package com.game_werewolf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ProgressBar;

import com.avos.avoscloud.PushService;
import com.game_werewolf.share.GameShareView;
import com.game_werewolf.share.ShareUtilsActivity;
import com.game_werewolf.share.VoiceDeedShareView;
import com.game_werewolf.share.VoiceShareActivity;
import com.game_werewolf.share.VoiceSpyShareView;
import com.game_werewolf.share.WereWolfShareActivity;

import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.GameShareFactory;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.model.VoiceDeedShareBean;
import orangelab.project.voice.model.VoiceSAFBean;

/**
 * game_werewolf
 * 2018/4/8 下午7:52
 * Mystery
 */

public class AppApplication extends MainApplication {

    @Override
    protected void onPreInitProjectConfig() {
        ProjectConfig.APP_NAME = getString(R.string.appName);
        ProjectConfig.APP_ICON = R.mipmap.ic_launcher;
        ProjectConfig.APP_SHARE_ICON = R.mipmap.icon_app_share;
        ProjectConfig.APP_FAMILY_MSG = getString(R.string.string_family_msg_hint);
        ProjectConfig.LEANCLOUD_SUPPORT_CUSTOM_SERVER = true;
        ProjectConfig.APP_API = BuildConfig.APP_API;
        ProjectConfig.APP_TYPE = BuildConfig.APP_TYPE;
        ProjectConfig.DEBUG_ABLE = BuildConfig.DEBUG_ABLE;
        ProjectConfig.LEANCLOUD_APPID = "BrjpIjqWyOpg07wHnPmmTAwh-gzGzoHsz";
        ProjectConfig.LEANCLOUD_APPKEY = "nTh0NC173njgegolAOnlY74D";
        ProjectConfig.LEANCLOUD_US_APPID = "fjckqKaLIXfrIqHMVFwzvzhx-MdYXbMMI";
        ProjectConfig.LEANCLOUD_US_APPKEY = "Yb24WAXknbANFVMYMjB8YUIG";
        ProjectConfig.APP_TYPE = BuildConfig.APP_TYPE;
        ProjectConfig.QQ_APPID = BuildConfig.qq_appid;
        ProjectConfig.QQ_APPKEY = BuildConfig.qq_appkey;
        ProjectConfig.WECHAT_APPID = BuildConfig.wechat_appid;
        ProjectConfig.WECHAT_APPKEY = BuildConfig.wechat_appkey;
        ProjectConfig.BG_SPLASH_NAME = BuildConfig.BG_SPLASH_NAME;
        ProjectConfig.NEED_SHOW_PRIVATE_ROOM_BUTTON_IN_CHAT = true;
        ProjectConfig.APP_CHANNEL = "google_play"; //由于国际版本狼人杀只有Google的渠道
        ProjectConfig.WATER_MARK = R.mipmap.ic_icon_water_mark; //狼人杀的水印
        ProjectConfig.QR_CODE_SHOW = R.mipmap.ic_qr_code_werewolf;//换装界面的二维码
        ProjectConfig.KTVAppId = "1400104761";            //狼人杀的ktv appid
        ProjectConfig.KTVAppKey = "viyrH8YrRyYY9A7X";     //狼人杀的ktv appkey
        ProjectConfig.KTVAccountType = "29712";           //狼人杀的ktv accounttype(目前没什么卵用)

        /**
         * 设置 狼人杀客服的 userId
         */
        ProjectConfig.EXCLUDE_USERID.add("5923d10843f4e7000773274b");

        /**
         * 分享类的注册
         */
        SharePoolManager.addProvider(ProjectConfig.APP_TYPE_WEREWOLF, new SharePoolManager.ShareProvider() {
            @Override
            public void onGameRoomShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, WereWolfShareActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onVoiceRoomShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, VoiceShareActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onPrivacyRoomShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, VoiceShareActivity.class);
                data.title = MessageUtils.getString(R.string.str_private_share_title);
                data.message = MessageUtils.getString(R.string.str_private_share_message, data.userName);
                data.url = MessageUtils.getString(R.string.str_private_share_url, ProjectConfig.APP_API, data.roomId, data.password);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onMiniGameShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, ShareUtilsActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onSettingShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, ShareUtilsActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onJsCallShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, ShareUtilsActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onDeedShare(Context context, VoiceDeedShareBean data) {
                Intent intent = new Intent(context, VoiceShareActivity.class);
                intent.putExtra(VoiceConstants.IS_FROM_DEED, true);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_DEED, data);
                context.startActivity(intent);
            }

            @Override
            public Bitmap getGameOverShareView(Context context, GameShareFactory.Builder builder) {
                GameShareView shareView = new GameShareView(builder, context);
                return shareView.createBitmap();
            }

            @Override
            public Bitmap getSpyShareView(Context context, VoiceSAFBean bean) {
                VoiceSpyShareView shareView = new VoiceSpyShareView(context);
                shareView.setData(bean);

                return shareView.createBitmap();
            }

            @Override
            public Bitmap getRoomDeedShareView(Context context, VoiceDeedShareBean bean) {
                VoiceDeedShareView shareView = new VoiceDeedShareView(context, bean);

                return shareView.createBitmap();
            }

            @Override
            public void onImageShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, ShareUtilsActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onSpyRoomShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, VoiceShareActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }

            @Override
            public void onLobbyRoomShare(Context context, ShareBridgeData data) {
                Intent intent = new Intent(context, VoiceShareActivity.class);
                intent.putExtra(ShareBridgeData.SHARE_COMMON_SINGLE, data);
                context.startActivity(intent);
            }
        });
    }

    @Override
    protected void onAfterApplicationCreate() {
        /**
         * 设定LeanCloud的推送启动Activity
         */
        PushService.setDefaultPushCallback(this, FakeLauncherActivity.class);

        //做一个检测处理
        if (TextUtils.isEmpty(ProjectConfig.KTVAppKey) || TextUtils.isEmpty(ProjectConfig.KTVAppId)) {
            throw new IllegalArgumentException("ktv id and ktv key must be set!!");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
