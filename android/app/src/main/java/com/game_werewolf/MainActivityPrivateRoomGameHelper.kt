package com.game_werewolf

import android.text.TextUtils
import com.datasource.GlobalUserState
import com.networktoolkit.transport.Constant
import com.toolkit.action.Destroyable
import orangelab.project.common.activity.LaunchActivity
import orangelab.project.common.model.EnterRoomPayload
import orangelab.project.common.utils.IntentDataHelper
import orangelab.project.minigame.model.GlobalSocketJoinRoom
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 *game_werewolf
 *2018/4/16 下午1:23
 *Mystery
 */
class MainActivityPrivateRoomGameHelper : Destroyable {

    private val mMainActivity: MainActivity

    constructor(mMainActivity: MainActivity) {
        this.mMainActivity = mMainActivity
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onJoinRoom(joinRoom: GlobalSocketJoinRoom) {
        /**
         * 处理私密房的进入
         */
        try {
            if (TextUtils.equals(joinRoom.data.level, Constant.GAME_TYPE_PRIVACY) && !GlobalUserState.getGlobalState().isGaming) {
                val enterRoomPayload = EnterRoomPayload()
                enterRoomPayload.invitee = joinRoom.data.extra.invitee
                enterRoomPayload.inviter = joinRoom.data.extra.inviter
                enterRoomPayload.gameType = Constant.GAME_TYPE_PRIVACY
                enterRoomPayload.password = joinRoom.data.password
                enterRoomPayload.token = GlobalUserState.getGlobalState().token
                enterRoomPayload.roomId = joinRoom.data.room_id
                enterRoomPayload.userId = GlobalUserState.getGlobalState().userId
                LaunchActivity.LaunchForRoom(mMainActivity, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, enterRoomPayload)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun destroy() {
        EventBus.getDefault().unregister(this)
    }
}