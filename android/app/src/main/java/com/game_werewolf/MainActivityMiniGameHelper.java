package com.game_werewolf;

import android.speech.tts.Voice;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.toolkit.action.Destroyable;

import orangelab.project.common.UserBehaviorObserver;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.db.entity.MiniGameAIRecordEntity;
import orangelab.project.common.model.ServerGet;
import orangelab.project.minigame.MiniGameActivity;
import orangelab.project.minigame.event.MiniGameReMatchEvent;
import orangelab.project.minigame.model.GlobalSocketJoinRoom;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.voice.config.VoiceRoomConfig;

/**
 * game_werewolf
 * 2018/2/1 下午2:55
 * Mystery
 */

public class MainActivityMiniGameHelper implements Destroyable {

    private MainActivity mActivity;

    public MainActivityMiniGameHelper(MainActivity activity) {
        mActivity = activity;

        RxToolKit.Build(this, GlobalSocketJoinRoom.class).action(value -> {
            if (mActivity != null) {
                mActivity.runOnUiThreadSafely(() -> {
                    if (GlobalUserState.getGlobalState().isGaming() && !VoiceRoomConfig.isLobby()) {
                        /**
                         * 如果在游戏中则不响应此次启动,此处限制了Activity 为了 保证 Activity的 单一性
                         */
                        return;
                    }

                    ServerGet serverGet = value.data;
                    ServerGet.ServerGetMiniGame miniGame = value.data.mini_game;
                    MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder()
                            /**
                             * 设定来源房间号,类型与密码,如果fromRoomId为空，则认为是leancloud进入的
                             */
                            .fromRoomId(VoiceRoomConfig.isLobby() ? VoiceRoomConfig.getRoomId() : "")
                            .fromRoomType(VoiceRoomConfig.isLobby() ? VoiceRoomConfig.getRoomType() : "")
                            .fromPassword(VoiceRoomConfig.isLobby() ? VoiceRoomConfig.getPsw() : "")

                            .type(serverGet.type)
                            .server(serverGet.server)
                            .roomId(serverGet.room_id)
                            .gameName(miniGame.name)
                            .gameIcon(miniGame.icon)
                            .gameUrl(miniGame.url)
                            .gameDownloadUrl(miniGame.download)
                            .landscape(miniGame.landscape)
                            .extraData(serverGet.extraData)
                            .password(serverGet.password)
                            .user_images(serverGet.user_images)
                            .tips(miniGame.tips);
                    try {
                        builder.oppoIcon(value.data.oppo.avatar);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MiniGameLaunch launch = builder.build();

                    if (!UserBehaviorObserver.enterMiniGameIntentInterrupt(mActivity, launch)) {
                        GlobalUserState.getGlobalState().setGaming(true);
                        MiniGameActivity.Launch(mActivity, launch);
                    }
                });
            }
        }).register();

        RxToolKit.Build(this, MiniGameReMatchEvent.class).action(value -> {
            if (mActivity != null) {
                mActivity.runOnUiThreadSafely(() -> {
                    LaunchActivity.LaunchForAmusementRandom(mActivity, value.getMiniGameLaunch().getType());
                });
            }
        }).register();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (mActivity != null) {
            mActivity = null;
        }
    }
}
