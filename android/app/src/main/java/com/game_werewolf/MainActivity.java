package com.game_werewolf;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.androidtoolkit.DeviceToolKit;
import com.androidtoolkit.Environment;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.emulator.EmulatorCheckUtil;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.networktoolkit.transport.Constant;
import com.androidtoolkit.PLog;

import cn.intviu.orbit.manager.OrbitGlobalConfig;
import cn.intviu.service.IntviuApplication;
import cn.intviu.support.ConstInfo;

import com.datasource.GlobalUserState;
import com.networktoolkit.transport.TransportConfig;
import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.RNActivityManager;
import orangelab.project.common.activity.HintActivity;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.dialog.MsgDialog;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.GlobalEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.pay.google.GooglePayTask;
import orangelab.project.common.pay.google.v2.GooglePayInAppV2Task;
import orangelab.project.common.pay.google.v2.GooglePaySubsV2Task;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

public class MainActivity extends ReactActivity {


    private static final String TAG = "MainActivity";

    private static final int IMAGE_PICKER_REQUEST = 61110;//图片选取
    private static final int CAMERA_PICKER_REQUEST = 61111;//相机
    private static final int CANCEL_PICKER_REQUEST = 69;//取消选取照片
    //    private static final int QQ_REQUEST = 11101;//QQ登录
//    private static final int ADV_TIMEOUT = 10000;
    private MsgDialog msgDialog;
    private MsgDialog mContinueDialog = null;
    private boolean ReactNativeInitFinish = false;
    private boolean ReactNativeDataInitFinish = false;

    private GooglePayInAppV2Task mGooglePayInAppV2Task = null;
    private GooglePaySubsV2Task mGooglePaySubsV2Task = null;

    private SafeHandler mSafeHandler = new SafeHandler();
    private List<Destroyable> mMainActivityHelpers = new ArrayList<>();

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected Bundle getLaunchOptions() {
                Bundle opts = new Bundle();
                opts.putString("did", ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
                opts.putString("sv", TransportConfig.SV_VALUE);
                opts.putString("version", BuildConfig.VERSION_NAME);
                opts.putString("pid", DeviceToolKit.GetDeviceId());
                opts.putString("host", Constant.HTTP_URL);
                opts.putString("sdk_version",  String.valueOf(Build.VERSION.SDK_INT));
                opts.putString("em", EmulatorCheckUtil.getEmCode(Environment.GetAppContext()));
                return opts;
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        splashDialogShow();

        super.onCreate(savedInstanceState);

        RNActivityManager.INSTANCE.BindRNActivity(this);
        EventBus.getDefault().register(this);

        Utils.setWindowStatusBarColor(this, android.R.color.transparent);

        mMainActivityHelpers.add(new MainActivityLCIMHelper(this));
        mMainActivityHelpers.add(new MainActivityFloatWindowHelper(this));
        mMainActivityHelpers.add(new MainActivityMiniGameHelper(this));
        mMainActivityHelpers.add(new MainActivityPrivateRoomGameHelper(this));

        mGooglePayInAppV2Task = new GooglePayInAppV2Task(this);
        mGooglePaySubsV2Task = new GooglePaySubsV2Task(this);

        initListener();
        
        if (PLog.needLog) {
            ToastToolKit.showLong("当前是否是模拟器（1：是，0：不是）" + EmulatorCheckUtil.getEmCode(this));
        }
    }

    private void initListener() {
        RxToolKit.Build(this, ActivityEvent.ReactNativeInitFinishEvent.class).action(v -> {
            runOnUiThreadSafely(() -> {
                ReactNativeInitFinish = true;
                PLog.i(TAG, "initReactNative Finish: ");
                checkLeanCloudNode();
            });
        }).register();
        RxToolKit.Build(this, ActivityEvent.StartPayTaskEvent.class).action(v -> startGoogleTask()).register();

        RxToolKit.Build(this, ActivityEvent.ReactNativeInitDataFinishEvent.class).action(v -> {
            runOnUiThreadSafely(() -> {
                ReactNativeDataInitFinish = true;
                startGoogleTask();
                startBlackList();
                showRestoreGameInfoIfNeed();
                EffectsManager.RefreshGiftsManifest(null);
                EffectsManager.RefreshHeadDecorManiFest(null);
                EffectsManager.RefreshMsgDecorManifest(null);
                EffectsManager.RefreshEmotionManiFest(null);
            });
        }).register();
        RxToolKit.Build(this, ActivityEvent.ReactNativeLogOutEvent.class).action(v -> runOnUiThreadSafely(() -> ReactNativeDataInitFinish = false)).register();
        initInnerMessageEvent();
    }

    private void startBlackList() {
        BlackListManager.Refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PLog.i(TAG, "onResume");
        ReportEventUtils.reportActivityResume(this);
        if (ReactNativeInitFinish) {
            checkLeanCloudNode();
            todDoRecordLogin();
            RoomSocketEngineHelper.userLeaveFromWeb();
        }

        if (ReactNativeDataInitFinish) {
            startGoogleTask();
        }

        if (ReactNativeDataInitFinish) {
            showRestoreGameInfoIfNeed();
        }

        if (ReactNativeDataInitFinish) {
            EffectsManager.RefreshGiftsManifest(null);
            EffectsManager.RefreshHeadDecorManiFest(null);
            EffectsManager.RefreshEmotionManiFest(null);
        }
        /**
         * 通知Rn 在 Top上
         */
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_REACT_NATIVE_ON_TOP);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PLog.i(TAG, "onPause");
        ReportEventUtils.reportActivityPause(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        PLog.i(TAG, "onStart");
        AppManager.ActivityOnStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        PLog.i(TAG, "onStop");
        AppManager.ActivityOnStop();
    }

    private void startGoogleTask() {
        if (mGooglePayInAppV2Task != null) {
            mGooglePayInAppV2Task.startTask();
        }
        if (mGooglePaySubsV2Task != null) {
            mGooglePaySubsV2Task.startTask();
        }
    }

    /**
     * 上报连续登录
     */
    private void todDoRecordLogin() {
        //通知进行登录上报
        WritableMap params = Arguments.createMap();
        RoomSocketEngineHelper.sendEventToRn(Constant.CONTINUE_LOGIN, params);
    }

    private void checkLeanCloudNode() {
        ApiManager.CheckLeanCloudNode((config, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    try {
                        /**
                         * 处理节点问题
                         */
                        if (!TextUtils.isEmpty(config.host)) {
                            String host = config.host;
                            boolean unmute_send = config.unmute_send;
                            OrbitGlobalConfig.forceConfigUnMuteSendPackage(unmute_send);
                            if (!TextUtils.isEmpty(host)) {
                                String configHost = Utils.getLeanCloudNode(MainActivity.this);
                                PLog.i(TAG, "onSuccess: configHost=" + configHost);
                                if (TextUtils.isEmpty(configHost)) {
                                    /**
                                     * 如果上次为空那么先存入默认值然后获取
                                     */
                                    Utils.saveLeanCloudNode(MainActivity.this, Constant.LEANCLOUD_CN);
                                    configHost = Utils.getLeanCloudNode(MainActivity.this);
                                    PLog.i(TAG, "onSuccess: configHost=" + configHost);
                                }
                                PLog.i(TAG, "onSuccess: host=" + host + " lastHost=" + configHost);
                                //判断配置是否相同
                                if (TextUtils.equals(configHost, host)) {
                                    /**
                                     * 配置相同则忽略
                                     */
                                } else {
                                    WritableMap map = Arguments.createMap();
                                    map.putString(Constant.LEANCLOUD_NODE_RN_CONFIG, host);
                                    RoomSocketEngineHelper.sendEventToRn(Constant.LEANCLOUD_NODE_RN, map);
                                    /**
                                     * 更改配置，并且重新启动应用
                                     */
                                    Utils.saveLeanCloudNode(MainActivity.this, host);
                                    HintActivity.showHint(MainActivity.this);
                                }
                            }
                        }

                        /**
                         * 处理LeanCloud server代理地址问题
                         */
                        if (Utils.targetIsNotNull(config.leancloud)) {

                            boolean configHasChange = false;

                            String api = config.leancloud.api;
                            String rtm = config.leancloud.rtm;

                            PLog.i(TAG, "new leancloud config-> api:" + api + ",rtm:" + rtm);

                            String lastApi = Utils.getLeanCloudServerApi(MainActivity.this);
                            String lastRtm = Utils.getLeanCloudServerRtm(MainActivity.this);

                            PLog.i(TAG, "old leancloud config-> api:" + lastApi + ",rtm:" + lastRtm);

                            if (TextUtils.equals(api, lastApi)) {
                                /**
                                 * 地址相同不作处理
                                 */
                            } else {
                                configHasChange = true;
                                /**
                                 * 保存新地址
                                 */
                                Utils.saveLeanCloudServerApi(MainActivity.this, api);
                            }

                            if (TextUtils.equals(rtm, lastRtm)) {
                                /**
                                 * 地址相同不作处理
                                 */
                            } else {
                                configHasChange = true;
                                /**
                                 * 保存新地址
                                 */
                                Utils.saveLeanCloudServerRtm(MainActivity.this, rtm);
                            }

                            if (configHasChange) {
                                /**
                                 * 发生了地址改变
                                 */
                                HintActivity.showHint(MainActivity.this);
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
        });
    }

    private void initInnerMessageEvent() {
        RxToolKit.Build(this, ViewEvent.LeaveMessageEvent.class).action(leaveMessageEvent -> {
            if (msgDialog != null && msgDialog.isShowing()) {
                return;
            }
            msgDialog = new MsgDialog(MainActivity.this,
                    MessageUtils.getString(R.string.dialog_hint),
                    MessageUtils.getString(R.string.leave_warning_string),
                    v -> msgDialog.dismiss()
            );
            msgDialog.show();
        }).register();
    }

    @Override
    protected String getMainComponentName() {
        return BuildConfig.app_module;
    }

    @Override
    public void finish() {
        super.finish();
        PLog.i(TAG, "finish");
        release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PLog.i(TAG, "onDestroy");
        release();
    }

    private void release() {
        RxToolKit.UnRegister(this);
        EventBus.getDefault().unregister(this);

        mSafeHandler.release();

        if (mGooglePayInAppV2Task != null) {
            mGooglePayInAppV2Task.destroy();
            mGooglePayInAppV2Task = null;
        }
        if (mGooglePaySubsV2Task != null) {
            mGooglePaySubsV2Task.destroy();
            mGooglePaySubsV2Task = null;
        }

        ToolKit.travelCollection(mMainActivityHelpers, value -> {
            value.destroy();
        });
        mMainActivityHelpers.clear();

        try {
            MainApplication.getInstance().getNativeJSModule().unRegisterBridgeFilter();
        } catch (Exception e) {
            e.printStackTrace();
            //in case NativeJSModule is null
        }

        RNActivityManager.INSTANCE.UnRegisterRNActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //TODO wangxu react-native-qq 需要实现onActivityResult requestCode=11101
        //TODO 但是react-native-image-crop-picker 自身以及处理如果是选取头像一定要实现picker的onActivityResult requestCode=61110
        // 或requestCode=61111或requestCode=69
        if (requestCode == CAMERA_PICKER_REQUEST || requestCode == IMAGE_PICKER_REQUEST || requestCode == CANCEL_PICKER_REQUEST) {
            if (MainApplication.getInstance().getPickerModule() != null) {
                MainApplication.getInstance().getPickerModule().onActivityResult(this, requestCode, resultCode, data);
            } else {
                PLog.i(TAG, "onActivityResult: getPickerModule==null");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            ReactInstanceManager mReactInstanceManager = MainApplication.getInstance().getReactNativeHost()
                    .getReactInstanceManager();
            if (mReactInstanceManager != null) {
                mReactInstanceManager.onActivityResult(this, requestCode, resultCode, data);
            }
            MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
    }

    public void runOnUiThreadSafely(Runnable runnable) {
        mSafeHandler.postSafely(runnable);
    }

    private void showRestoreGameInfoIfNeed() {
        if (mContinueDialog != null && mContinueDialog.isShowing()) {
            return;
        }
        ApiManager.DisConnectInfoApi(value -> {
            runOnUiThreadSafely(() -> {
                if (value != null && !TextUtils.isEmpty(value.room_id) && !TextUtils.isEmpty(value.type)) {
                    MsgDialog.MsgDialogBuilder builder = MsgDialog.MsgDialogBuilder.build().setAutoDismiss(true)
                            .setLeftButtonText(MessageUtils.getString(R.string.werewolf_continue_game_cancel))
                            .setRightButtonText(MessageUtils.getString(R.string.werewolf_continue_game_confirm))
                            .setLeftClick(v -> {
                                ApiManager.EscapeGameApi();
                                if (mContinueDialog != null && mContinueDialog.isShowing()) {
                                    mContinueDialog.dismiss();
                                }
                            })
                            .setRightClick(v -> {
                                if (mContinueDialog != null && mContinueDialog.isShowing()) {
                                    mContinueDialog.dismiss();
                                }
                                EnterRoomPayload payload = new EnterRoomPayload();
                                payload.password = value.password;
                                payload.gameType = value.type;
                                payload.roomId = value.room_id;
                                payload.userId = GlobalUserState.getGlobalState().getUserId();
                                payload.token = GlobalUserState.getGlobalState().getToken();
                                LaunchActivity.LaunchForRoom(this, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
                            })
                            .setMessage(MessageUtils.getString(R.string.werewolf_continue_game))
                            .setTitle(MessageUtils.getString(R.string.dialog_hint));
                    mContinueDialog = new MsgDialog(this, builder);
                    mContinueDialog.show();
                }
            });
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSplashDialogDismissEvent(ActivityEvent.SplashDialogDismissEvent event) {
        splashDialogDismiss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPrivateRoomInvite(GlobalEvent.PrivateRoomInviteForNormal event) {
        /**
         * 处理
         * 私密房邀请的信息
         */
        EnterRoomPayload payload = new EnterRoomPayload();
        payload.roomId = event.getRoomId();
        payload.gameType = event.getRoomType();
        payload.password = event.getRoomPassword();
        payload.userId = GlobalUserState.getGlobalState().getUserId();
        payload.token = GlobalUserState.getGlobalState().getToken();
        LaunchActivity.LaunchForRoom(this, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
    }

    private void splashDialogShow() {
        try {
            SplashDialogFragment splashDialogFragment = new SplashDialogFragment();
            splashDialogFragment.show(getFragmentManager(), "SplashDialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void splashDialogDismiss() {
        try {
            ((SplashDialogFragment) getFragmentManager().findFragmentByTag("SplashDialog")).dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static class SplashDialogFragment extends DialogFragment {
        private static final String TAG = "SplashDialogFragment";

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            PLog.i(TAG, "onCreate");
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            PLog.i(TAG, "onCreateDialog");
            Dialog dialog = new Dialog(getActivity(), com.R.style.SplashScreen_Fullscreen);
            ImageView view = (ImageView) View.inflate(getActivity(), com.R.layout.launch_screen, null);
            int resoure = ApkResourceManager.GetApkImageResource(ProjectConfig.BG_SPLASH_NAME);
            if (resoure > 0) {
                view.setImageResource(resoure);
            }
            dialog.setContentView(view);
            dialog.setCancelable(false);
            return dialog;
        }
    }
}
