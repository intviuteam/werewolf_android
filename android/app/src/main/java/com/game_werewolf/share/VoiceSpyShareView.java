package com.game_werewolf.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.game_werewolf.R;
import com.toolkit.action.Keepable;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.model.VoiceSAFBean;
import orangelab.project.voice.model.VoiceSimpleUserBean;
import orangelab.project.voice.utils.PositionHelper;

/**
 * Created by lloydfinch on 29/03/2018.
 */

public class VoiceSpyShareView extends RelativeLayout implements Keepable {


    private ImageView ivIcon;//顶部胜利失败图标

    private TextView tvWordSpy;
    private TextView tvWordGood;

    private ImageView ivVafFirst;
    private ImageView ivVafSecond;
    private TextView tvVafFirst;
    private TextView tvVafSecond;

    private LinearLayout llFirstContainer;
    private LinearLayout llSecondContainer;

    private View llSpyShareBg;
    private View rlSpyShareBgPart;
    private ImageView ivSpyShareQRCode;

    private Context mContext;
    int defaultHeight = ScreenUtils.getScreenHeight();
    int defaultWidth = ScreenUtils.getScreenWidth();


    public VoiceSpyShareView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public VoiceSpyShareView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public VoiceSpyShareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    public void initView() {
        addView(inflate(getContext(), R.layout.layout_voice_spy_share_view, null));

        ivIcon = (ImageView) findViewById(R.id.iv_flag_icon);
        tvWordSpy = (TextView) findViewById(R.id.tv_spy_word);
        tvWordGood = (TextView) findViewById(R.id.tv_good_word);
        ivVafFirst = (ImageView) findViewById(R.id.iv_vaf_spy);
        ivVafSecond = (ImageView) findViewById(R.id.iv_vaf_good);
        tvVafFirst = (TextView) findViewById(R.id.tv_vaf_spy);
        tvVafSecond = (TextView) findViewById(R.id.tv_vaf_good);
        llFirstContainer = (LinearLayout) findViewById(R.id.ll_spy_container);
        llSecondContainer = (LinearLayout) findViewById(R.id.ll_good_container);
        llSpyShareBg = findViewById(R.id.ll_spy_share_bg);
        rlSpyShareBgPart = findViewById(R.id.rl_spy_share_content_view);
        ivSpyShareQRCode = (ImageView) findViewById(R.id.iv_qr_code);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(defaultWidth, defaultHeight);
        setLayoutParams(layoutParams);

    }

    //设置数据
    public void setData(VoiceSAFBean data) {
        if (data == null) {
            return;
        }
        setSpyWord(data.spyWord);
        setGoodWord(data.goodWord);
        if (data.isSpy) {
            if (data.victory) {
                spyVictory();
            } else {
                spyFailure();
            }
        } else {
            if (data.victory) {
                goodVictory();
            } else {
                goodFailure();
            }
        }

        addToFirstRow(data.first);
        addToSecondRow(data.second);
    }

    //设置卧底词
    public void setSpyWord(CharSequence spyWord) {
        tvWordSpy.setText(spyWord);
    }

    //设置好人词
    public void setGoodWord(CharSequence goodWord) {
        tvWordGood.setText(goodWord);
    }

    //自己是卧底且胜利
    public void spyVictory() {

        ivIcon.setImageResource(R.mipmap.ico_voice_spy_victory);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_victory);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_spy_victory));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_failure);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_good_failure));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        setVictoryBg();
    }

    //自己是卧底且失败
    public void spyFailure() {
        ivIcon.setImageResource(R.mipmap.ico_voice_spy_failure);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_failure);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_spy_failure));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_victory);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_good_victory));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        setFailureBg();
    }

    //自己是好人且胜利
    public void goodVictory() {

        ivIcon.setImageResource(R.mipmap.ico_voice_good_victory);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_victory);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_good_victory));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_failure);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_spy_failure));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        setVictoryBg();
    }

    //自己是好人且失败
    public void goodFailure() {
        ivIcon.setImageResource(R.mipmap.ico_voice_good_failure);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_failure);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_good_failure));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_victory);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_spy_victory));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        setFailureBg();
    }


    //添加第一行用户,自己的角色类型
    public void addToFirstRow(List<VoiceSimpleUserBean> userBeans) {
        llFirstContainer.removeAllViews();
        for (VoiceSimpleUserBean userBean : userBeans) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_voice_head_simple, null);
            CircleImageView ivHead = (CircleImageView) itemView.findViewById(R.id.iv_head_member);
            TextView tvName = (TextView) itemView.findViewById(R.id.tv_name_member);
            TextView tvNumber = (TextView) itemView.findViewById(R.id.tv_head_tag);
            if (PositionHelper.isSelf(userBean.userId)) {
                ivHead.setBorderColor(Color.YELLOW);
                TextView tvTag = (TextView) itemView.findViewById(R.id.tv_head_tag);
                tvTag.setBackgroundResource(R.drawable.bg_voice_vote_num_yellow);
                tvTag.setTextColor(Color.parseColor("#1f0064"));
            }
            llFirstContainer.addView(itemView);

            PicassoUtils.loadImage(mContext, userBean.headUrl, ivHead, R.mipmap.default_head);
            tvName.setText(userBean.userName);
            tvNumber.setText(String.valueOf(userBean.position));
        }
    }

    //添加第二行用户,跟自己对立的角色类型
    public void addToSecondRow(List<VoiceSimpleUserBean> userBeans) {
        llSecondContainer.removeAllViews();

        for (VoiceSimpleUserBean userBean : userBeans) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_voice_head_simple, null);
            CircleImageView ivHead = (CircleImageView) itemView.findViewById(R.id.iv_head_member);
            TextView tvName = (TextView) itemView.findViewById(R.id.tv_name_member);
            TextView tvNumber = (TextView) itemView.findViewById(R.id.tv_head_tag);
            if (PositionHelper.isSelf(userBean.userId)) {
                ivHead.setBorderColor(Color.YELLOW);
                TextView tvTag = (TextView) itemView.findViewById(R.id.tv_head_tag);
                tvTag.setBackgroundResource(R.drawable.bg_voice_vote_num_yellow);
                tvTag.setTextColor(Color.parseColor("#1f0064"));
            }
            llSecondContainer.addView(itemView);

            PicassoUtils.loadImage(mContext, userBean.headUrl, ivHead, R.mipmap.default_head);
            tvName.setText(userBean.userName);
            tvNumber.setText(String.valueOf(userBean.position));
        }
    }

    //自己胜利展示的背景图
    public void setVictoryBg() {
        llSpyShareBg.setBackgroundResource(R.mipmap.ic_voice_spy_share_bg_victory);
        rlSpyShareBgPart.setBackgroundResource(R.mipmap.ic_voice_spy_share_bg_victory_part);
        ivSpyShareQRCode.setImageResource(R.mipmap.ic_voice_spy_share_qr_code_victory);
    }

    //自己失败展示的背景图
    public void setFailureBg() {
        llSpyShareBg.setBackgroundResource(R.mipmap.ic_voice_spy_share_bg_failure);
        rlSpyShareBgPart.setBackgroundResource(R.mipmap.ic_voice_spy_share_bg_failure_part);
        ivSpyShareQRCode.setImageResource(R.mipmap.ic_voice_spy_share_qr_code_failure);
    }


    //获取当前绘制的bitmap
    public Bitmap createBitmap() {
        measure(getLayoutParams().width, getLayoutParams().height);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        return bitmap;
    }
}
