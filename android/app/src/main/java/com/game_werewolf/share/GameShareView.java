package com.game_werewolf.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.game_werewolf.R;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Keepable;

import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.common.utils.GameShareFactory;

/**
 * game_werewolf
 * 2017/6/13 下午4:14
 * Mystery
 */

public class GameShareView extends FrameLayout implements Keepable {
    private Context context;

    private int defaultWidth = ScreenUtils.dip2px(270);
    private int defaultHeight = ScreenUtils.dip2px(380);

    public GameShareView(GameShareFactory.Builder builder, @NonNull Context context) {
        super(context);
        this.context = context;
        initView(builder);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(defaultWidth, defaultHeight);
        setLayoutParams(layoutParams);
    }

    public void initView(GameShareFactory.Builder builder) {
        View root = View.inflate(context, R.layout.layout_game_share, null);
        View bg = root.findViewById(R.id.share_bg);
        View messageBg = root.findViewById(R.id.share_message_bg);

        ImageView gameTitle = (ImageView) root.findViewById(R.id.gameover_share_title);
        TextView userName = (TextView) root.findViewById(R.id.gameover_share_name);
        TextView gameType = (TextView) root.findViewById(R.id.gameover_game_type);
        TextView gameMessage = (TextView) root.findViewById(R.id.gameover_game_message);
        CircleImageView imageView = (CircleImageView) root.findViewById(R.id.gameover_share_iv);
        userName.setTextColor(getResources().getColor(R.color.game_share_name_color));
        userName.setText(builder.getUserName());
        gameType.setText(builder.getGameType());
        gameMessage.setText(builder.getGameMessage());
        if (builder.isWin()) {
            bg.setBackgroundResource(R.mipmap.gameover_share_win_bg);
            messageBg.setBackgroundResource(R.mipmap.gameover_share_message_win);
            gameTitle.setBackgroundResource(R.mipmap.gameover_share_title_win);
        } else {
            bg.setBackgroundResource(R.mipmap.gameover_share_lose_bg);
            messageBg.setBackgroundResource(R.mipmap.gameover_share_message_lose);
            gameTitle.setBackgroundResource(R.mipmap.gameover_share_title_lose);
        }
        PicassoUtils.loadImage(context, builder.getImageUrl(), imageView, R.mipmap.default_head);
        addView(root);
    }

    public Bitmap createBitmap() {
        measure(getLayoutParams().width, getLayoutParams().height);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        return bitmap;
    }
}
