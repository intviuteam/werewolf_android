package com.game_werewolf.share;

/**
 * game_werewolf
 * 2017/12/1 下午3:38
 * Mystery
 */

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;

import com.R;
import com.androidtoolkit.FileToolKit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.URLManager;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URLEncoder;
import java.util.ArrayList;

import cn.intviu.support.AppUtil;
import cn.intviu.support.ShareTools;
import orangelab.project.MainApplication;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.share.PublicShareCase;
import orangelab.project.common.share.PublicShareManager;
import orangelab.project.common.share.ShareAttributes;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.ShareDialogAdapter;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.dialog.WereWolfBaseDialog;
import orangelab.project.game.dialog.WereWolfIntviuFriendDialog;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class WereWolfShareActivity extends SafeActivity {

    private static final String TAG = "WereWolfShareActivity";

    private String mShareMessage;
    private String mShareTitle;
    private String mShareUrl;
    private String icon = "";

    private RxPermissions rxPermissions;
    private WereWolfBaseDialog shareDialog;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        if (intent != null) {
            try {
                ShareBridgeData data = (ShareBridgeData) intent.getSerializableExtra(ShareBridgeData.SHARE_COMMON_SINGLE);
                if (data != null) {
                    shareByType(data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据不同调起端分享
     *
     * @param data
     */
    private void shareByType(ShareBridgeData data) {
        switch (data.shareOrigin) {
            case ShareBridgeData.SHARE_FROM_GANE:
                shareFromGameOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_VOICE:
                shareFromVoiceOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_MINIGAME:
                shareFromMiniGameOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_SETTING:
                shareFromSettingOut(data);
                break;
            case ShareBridgeData.SHARE_FROM_JS:
                shareFromJS(data);
                break;
            case ShareBridgeData.SHARE_FROM_CUSTOM:
                shareFromCustom(data);
                break;
            default:
                break;

        }
    }

    private boolean filterAppExit(String app) {
        if (ShareTools.existsApp(WereWolfShareActivity.this, app)) {
            return true;
        } else {
            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.share_error)));
            return false;
        }
    }

    /**
     * 游戏房分享
     *
     * @param data
     */
    public void shareFromGameOut(ShareBridgeData data) {
        if (TextUtils.isEmpty(data.userName)) {
            return;
        }
        //标记是分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.INVITE);
        StringBuilder newUrl = new StringBuilder();
        String newUserName = data.userName;
        try {
            newUserName = URLEncoder.encode(newUserName, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        String flavor = "/invite";
        String url = URLManager.GetDefaultURL() + flavor + "?id=" + data.roomId + "&inviter=" + newUserName
                + "&channel=" + AppUtil.getApkChannel(this);
        newUrl.append(url);
        if (!TextUtils.isEmpty(data.password)) {
            newUrl.append("&password=" + data.password);
        }
        String[] messagesId = this.getResources().getStringArray(R.array.share_message_from_game);
        String[] titlesId = this.getResources().getStringArray(R.array.share_game_title);
        int id = (int) Math.round(Math.random() * (titlesId.length - 1));//随机产生一个index索引
        String title = titlesId[id];
        String message = String.format(messagesId[id], data.roomId, "");
        if (!TextUtils.isEmpty(data.password)) {
            message = String.format(messagesId[id], data.roomId, WereWolfString.getString(R.string.str_psw, data.password));
        }

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = TextUtils.isEmpty(data.url) ? newUrl.toString() : data.url;


        //后置处理，需要优化
        PublicShareCase.ShareBean shareContent = PublicShareManager.INSTANCE.getGameRoomShareContent();
        if (shareContent != null) {
            String localeTitle = shareContent.getLocaleTitle();
            if (!TextUtils.isEmpty(localeTitle)) {
                mShareTitle = localeTitle;
            }
            String localeContent = shareContent.getLocaleContent();
            if (!TextUtils.isEmpty(localeContent)) {
                try {
                    mShareMessage = localeContent.replace(ShareBridgeData.$ROOMID, data.roomId).
                            replace(ShareBridgeData.$PASSWORD, data.password);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!TextUtils.isEmpty(shareContent.link)) {
                //游客可以不传
                mShareUrl = shareContent.link + String.format(ShareAttributes.SHARE_GAME_ATTRIBUTES,
                        AppUtil.getApkChannel(this), data.roomId, data.password, data.userName,
                        shareContent.share_case_id, GlobalUserState.getGlobalState().getUserUid());
            }
            icon = shareContent.share_icon;
        }

        showShareDialog();
    }

    /**
     * 语音房分享
     *
     * @param data
     */
    public void shareFromVoiceOut(ShareBridgeData data) {

        ShareMedia.getShareMedia().setType(Constant.INVITE);
        String url = String.format(URLManager.GetDefaultURL() + Constant.SHARE_AUDIO_URL, data.roomId, data.password);

        String pswString = TextUtils.isEmpty(data.password) ? "" : MessageUtils.getString(R.string.str_psw, data.password);
        String message = MainApplication.getInstance().getString(R.string.share_message_from_voice_wolf, data.roomId, pswString);
        String title = MainApplication.getInstance().getString(R.string.share_title_voice_wolf);

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = TextUtils.isEmpty(data.url) ? url : data.url;


        //后置处理，需要优化
        PublicShareCase.ShareBean shareContent = PublicShareManager.INSTANCE.getVoiceRoomShareContent();
        if (shareContent != null) {
            String localeTitle = shareContent.getLocaleTitle();
            if (!TextUtils.isEmpty(localeTitle)) {
                mShareTitle = localeTitle;
            }
            String localeContent = shareContent.getLocaleContent();
            if (!TextUtils.isEmpty(localeContent)) {
                try {
                    mShareMessage = localeContent.replace(ShareBridgeData.$ROOMID, data.roomId).
                            replace(ShareBridgeData.$PASSWORD, data.password);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!TextUtils.isEmpty(shareContent.link)) {
                //游客可以不传
                mShareUrl = shareContent.link + String.format(ShareAttributes.SHARE_VOICE_ATTRIBUTES, data.roomId, data.password, shareContent.share_case_id, GlobalUserState.getGlobalState().getUserUid());
            }
            icon = shareContent.share_icon;
        }

        showShareDialog();
    }

    /**
     * 设置页面分享
     *
     * @param data
     */
    public void shareFromSettingOut(ShareBridgeData data) {

        //标记是分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        String url = URLManager.GetDefaultURL() + "/share?channel=" + AppUtil.getApkChannel(this);
        String[] messages = getResources().getStringArray(R.array.share_message_from_setting);
        String[] titles = getResources().getStringArray(R.array.share_title);
        int id = (int) Math.round(Math.random() * (titles.length - 1));//随机产生一个index索引
        String message = messages[id];
        String title = titles[id];

        mShareMessage = message;
        mShareTitle = title;
        mShareUrl = TextUtils.isEmpty(data.url) ? url : data.url;


        //后置处理，需要优化
        PublicShareCase.ShareBean shareContent = PublicShareManager.INSTANCE.getAppShareContent();
        if (shareContent != null) {
            String localeTitle = shareContent.getLocaleTitle();
            if (!TextUtils.isEmpty(localeTitle)) {
                mShareTitle = localeTitle;
            }
            String localeContent = shareContent.getLocaleContent();
            if (!TextUtils.isEmpty(localeContent)) {
                try {
                    mShareMessage = localeContent.replace(ShareBridgeData.$ROOMID, data.roomId).
                            replace(ShareBridgeData.$PASSWORD, data.password);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!TextUtils.isEmpty(shareContent.link)) {
                //游客可以不传
                mShareUrl = shareContent.link + String.format(ShareAttributes.SHARE_APP_ATTRIBUTES,
                        AppUtil.getApkChannel(this),
                        shareContent.share_case_id, GlobalUserState.getGlobalState().getUserUid());
            }
            icon = shareContent.share_icon;
        }

        showShareDialog();
    }

    /**
     * 小游戏分享
     */
    public void shareFromMiniGameOut(ShareBridgeData data) {

    }

    /**
     * js分享
     *
     * @param data
     */
    public void shareFromJS(ShareBridgeData data) {

    }

    /**
     * 自定义分享，直接使用传递数据进行
     *
     * @param data
     */
    public void shareFromCustom(ShareBridgeData data) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);

        mShareMessage = data.message;
        mShareTitle = data.title;
        mShareUrl = data.url;

        showShareDialog();
    }

    private View getDialogContentView() {
        ArrayList<Integer> text = new ArrayList<>();
        ArrayList<Integer> image = new ArrayList<>();
        text.add(R.string.share_we_chat);
        text.add(R.string.share_moments);
        text.add(R.string.share_qq);
        text.add(R.string.share_facebook);
        text.add(R.string.share_line);

        image.add(R.mipmap.share_through_weixin);
        image.add(R.mipmap.share_through_moments);
        image.add(R.mipmap.share_through_qq);
        image.add(R.mipmap.icon_facebook);
        image.add(R.mipmap.icon_line);

        if (GlobalUserState.getGlobalState().isGaming() && !GlobalUserState.getGlobalState().isTourist()) {
            text.add(R.string.share_intviu);
            image.add(R.mipmap.invite_icon_friends);
        }

        if (FamilyMemo.iMemo.isHasJoinFamily()) {
            text.add(R.string.string_intviu_into_family);
            image.add(R.mipmap.ico_family_smaller);
        }

        ListView view = new ListView(this);
        view.setSelector(android.R.color.transparent);
        view.addFooterView(new View(this));
        view.setFooterDividersEnabled(true);
        view.setVerticalScrollBarEnabled(false);
        view.setDividerHeight(0);

        final ShareDialogAdapter adapter = new ShareDialogAdapter(this, text, image, R.color.werewolf_dialog_normal_text_color, R.mipmap.ico_werewolf_user_dialog_arrow);
        view.setAdapter(adapter);
        view.setOnItemClickListener((parent, view1, position, id) -> {

            boolean invalid = position >= 0 && position < text.size();
            if (!invalid) {
                return;
            }

            int itemId = 0;
            try {
                itemId = (Integer) adapter.getItem(position);
            } catch (IndexOutOfBoundsException e) {
                PLog.e(TAG, e.getMessage());
            }

            if (itemId == R.string.share_we_chat) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
                if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.WEIXIN));
                }
            } else if (itemId == R.string.share_moments) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
                if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.WEIXIN_CIRCLE));
                }
            } else if (itemId == R.string.share_qq) {
                ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
                if (filterAppExit(ShareTools.SHARE_QQ)) {
                    rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Boolean>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Boolean aBoolean) {
                                    if (aBoolean) {
                                        if (!TextUtils.isEmpty(mShareMessage)) {
                                            if (mShareMessage.length() > 60) {
                                                StringBuilder message = new StringBuilder();
                                                message.append(mShareMessage.substring(0, 57));
                                                message.append("...");
                                                mShareMessage = message.toString();
                                            }
                                        }
                                        shareSoftware(SHARE_MEDIA.QQ);
                                        if (shareDialog != null && shareDialog.isShowing()) {
                                            shareDialog.dismiss();
                                        }
                                        String category = ShareMedia.getShareMedia().getType();
                                        String type = Constant.SHARE_TO_QQ;
                                        if (TextUtils.equals(category, Constant.INVITE)) {
                                            type = Constant.INVITE_TO_QQ;
                                        }
                                        //shareSuccessReported(type);
                                    } else {
                                        RunnableFactory.createMsgToastRunnable(WereWolfString.getString(R.string.permission_read_store_error)).run();
                                    }
                                }
                            });
                }
            } else if (itemId == R.string.share_intviu) {
                WereWolfIntviuFriendDialog friendDialog = new WereWolfIntviuFriendDialog(UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST, WereWolfShareActivity.this);
                friendDialog.show();
                dismissDialogOnly();
            } else if (itemId == R.string.share_facebook) {
                if (filterAppExit(ShareTools.SHARE_FACEBOOK)) {
                    ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
                    dismissDialogOnly();
                    ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.FACEBOOK));
                }
            } else if (itemId == R.string.share_line) {
                if (filterAppExit(ShareTools.SHARE_LINE)) {
                    rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Boolean>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Boolean aBoolean) {
                                    if (aBoolean) {
                                        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
                                        dismissDialogOnly();
                                        ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.LINE));
                                    } else {
                                        RunnableFactory.createMsgToastRunnable(WereWolfString.getString(R.string.permission_read_store_error)).run();
                                    }
                                }
                            });
                }
            } else if (itemId == R.string.string_intviu_into_family) {
                Utils.runSafely(() -> {
                    LeanCloudChatHelper.SendIntviuToConversationForFamily(
                            GlobalUserState.getGlobalState().getCurRoomId(),
                            GlobalUserState.getGlobalState().getPassword(),
                            FamilyMemo.iMemo.getFamilyLcId()
                    );
                });
            }
        });
        view.setPadding(ScreenUtils.dip2px(8), ScreenUtils.dip2px(8), ScreenUtils.dip2px(8), ScreenUtils.dip2px(8));
        return view;
    }

    private void showShareDialog() {
        if (shareDialog == null) {
            shareDialog = new WereWolfBaseDialog(this);
            String title = getString(R.string.title_share);
            shareDialog.setDialogContentView(getDialogContentView());
            shareDialog.setTitle(title);
            shareDialog.setButtonType(WereWolfBaseDialog.ONE_BUTTON);
            shareDialog.getOneButton().setOnClickListener(v -> {
                dismissDialogAndFinish();
            });
            shareDialog.showDialog(0);
        }
    }

    private void dismissDialogOnly() {
        if (shareDialog != null) {
            shareDialog.dismiss();
        }
    }

    private void dismissDialogAndFinish() {
        dismissDialogOnly();
        finish();
    }

    private void shareSoftware(SHARE_MEDIA share_media) {
        Bitmap shareIcon = BitmapFactory.decodeResource(getResources(), com.game_werewolf.R.mipmap.icon_app_share);
        try {
            if (!TextUtils.isEmpty(icon)) {
                byte[] bytes = FileToolKit.loadFile(icon);
                shareIcon = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        UMImage image = new UMImage(WereWolfShareActivity.this.getApplicationContext(), shareIcon);
        UMWeb umWeb = new UMWeb((mShareUrl));
        umWeb.setTitle(mShareTitle);
        umWeb.setThumb(image);
        umWeb.setDescription(mShareMessage);

        new ShareAction(this)
                .withMedia(umWeb)
                .setPlatform(share_media)
                .setCallback(mUmShareListener)
                .share();
    }

    private UMShareListener mUmShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            PLog.i(TAG, "share-->onStart" + share_media);
        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            PLog.i(TAG, "share-->success" + platform);
            //TODO qq,weixin,line,facebook统一为QQ微信的任务
            String category = ShareMedia.getShareMedia().getType();
            //分享类
            String type = Constant.SHARE_TO_QQ;
            if (TextUtils.equals(category, Constant.INVITE)) {
                //邀请类
                type = Constant.INVITE_TO_QQ;
            }
            shareSuccessReported(type);
            finish();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            finish();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            PLog.i(TAG, "share-->cancel");
            finish();
        }
    };

    private void shareSuccessReported(String type) {
        ApiManager.DoShareApi(type, 1);
    }

    //如果使用的是qq或者新浪精简版jar，需要在您使用分享或授权的Activity（fragment不行）中添加如下回调代码：
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this.getApplicationContext()).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void finish() {
        destroy();
        super.finish();
    }

    private void destroy() {
        if (rxPermissions != null) {
            rxPermissions = null;
        }

        if (shareDialog != null) {
            if (shareDialog.isShowing()) {
                shareDialog.dismiss();
            }
            shareDialog = null;
        }

        RxToolKit.UnRegister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.VoiceShareActivityFinishEvent event) {
        finish();
    }
}
