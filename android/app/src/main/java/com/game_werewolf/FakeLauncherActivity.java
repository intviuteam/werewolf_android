package com.game_werewolf;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;

import com.datasource.GlobalUserState;

import orangelab.project.common.RNActivityManager;
import orangelab.project.common.activity.SafeActivity;

/**
 * game_werewolf
 * 2018/6/14 下午5:22
 * Mystery
 */
public class FakeLauncherActivity extends SafeActivity {

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindow();
        if (RNActivityManager.INSTANCE.GetRNActivity() == null) {
            runOnUiThreadSafely(() -> {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            });
            return;
        } else {
            finish();
            return;
        }
    }

    private void initWindow() {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);// 隐藏标题
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);// 设置全屏
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
