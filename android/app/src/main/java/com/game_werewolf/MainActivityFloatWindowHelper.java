package com.game_werewolf;

import android.content.Context;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.toolkit.action.Destroyable;

import orangelab.project.MainApplication;
import orangelab.project.common.event.SmallWindowEvent;
import orangelab.project.common.floatwindow.FloatWindowCommander;
import orangelab.project.common.floatwindow.event.RestoreEvent;
import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2017/12/27 下午3:02
 * Mystery
 */

public class MainActivityFloatWindowHelper implements Destroyable {

    private Context mContext;
    private SafeHandler mSafeHandler = new SafeHandler();

    public MainActivityFloatWindowHelper(Context context) {
        mContext = context;
        initListener();
    }

    private void initListener() {
        RxToolKit.Build(this, RestoreEvent.class)
                .action(v -> {
                    if (mContext != null) {
                        MainApplication.getInstance().getSocketEngineHelper().restoreRoom(
                                mContext,
                                v.isNeedHandOver(),
                                value -> {
                                    if (mSafeHandler != null) {
                                        mSafeHandler.postSafely(() -> {
                                            ToastToolKit.showShort(value.getMessage());
                                        });
                                    }
                                }
                        );
                    }
                }).register();
        RxToolKit.Build(this, SmallWindowEvent.NotifyDesTorySW.class).action(value -> {
            if (mSafeHandler != null) {
                mSafeHandler.postSafely(() -> {
                    if (mContext != null) {
                        FloatWindowCommander.DestroyFloatWindow(mContext, null);
                    }
                });
            }
        }).register();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (mSafeHandler != null) {
            mSafeHandler.release();
            mSafeHandler = null;
        }
        if (mContext != null) {
            mContext = null;
        }
    }
}
