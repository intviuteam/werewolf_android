package com.androidtoolkit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.toolkit.action.Action;

import java.io.File;
import java.io.IOException;


public class PicassoUtils {

    public static void fetchImage(Context context, String url, Callback callback) {
        Picasso.get().load(url).fetch(callback);
    }

    /**
     * 不应在主线程调用
     *
     * @param context
     * @param resourceId
     * @return
     * @throws IOException
     */
    public static Bitmap getImageBitmap(Context context, int resourceId) throws IOException {
        return Picasso.get().load(resourceId).get();
    }

    /**
     * 不应在主线程调用
     *
     * @param context
     * @return
     * @throws IOException
     */
    public static Bitmap getImageBitmap(Context context, String url) throws IOException {
        return Picasso.get().load(url).get();
    }

    /**
     * 不应在主线程调用
     *
     * @param context
     * @return
     * @throws IOException
     */
    public static Bitmap getImageBitmap(Context context, File file) throws IOException {
        return Picasso.get().load(file).get();
    }

    public static void getImage(Context context, int resourceId, Action<Bitmap> bitmapAction) {
        ThreadToolKit.Async(() -> {
            try {
                Bitmap bitmap = Picasso.get().load(resourceId).get();
                if (bitmapAction != null) {
                    bitmapAction.func(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (bitmapAction != null) {
                    bitmapAction.func(null);
                }
            }
        });
    }

    public static void getImage(Context context, int resourceId, int height, int width, Action<Bitmap> bitmapAction) {
        ThreadToolKit.Async(() -> {
            try {
                Bitmap bitmap = Picasso.get().load(resourceId).resize(height, width).get();
                if (bitmapAction != null) {
                    bitmapAction.func(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (bitmapAction != null) {
                    bitmapAction.func(null);
                }
            }
        });
    }

    public static void getImage(Context context, String url, int height, int width, Action<Bitmap> bitmapAction) {
        ThreadToolKit.Async(() -> {
            try {
                Bitmap bitmap = Picasso.get().load(url).resize(height, width).get();
                if (bitmapAction != null) {
                    bitmapAction.func(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (bitmapAction != null) {
                    bitmapAction.func(null);
                }
            }
        });
    }

    public static void getImage(Context context, String url, Action<Bitmap> bitmapAction) {
        ThreadToolKit.Async(() -> {
            try {
                Bitmap bitmap = Picasso.get().load(url).get();
                if (bitmapAction != null) {
                    bitmapAction.func(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (bitmapAction != null) {
                    bitmapAction.func(null);
                }
            }
        });
    }

    public static void loadImage(Context context, String url, ImageView target) {
        Picasso.get().load(url).into(target);
    }

    public static void loadImage(Context context, String url, ImageView target, Callback callback) {
        Picasso.get().load(url).into(target, callback);
    }

    public static void loadImage(Context context, String url, ImageView target, int width, int height, Callback callback) {
        Picasso.get().load(url).resize(width, height).into(target, callback);
    }

    public static void loadImage(Context context, File file, ImageView target) {
        Picasso.get().load(file).resize(150, 150).centerInside().into(target);
    }

    public static void loadImage(Context context, String url, ImageView target, int defaultId) {
        loadImage(context
                , url
                , 150, 150
                , target
                , defaultId);
    }

    public static void loadImageByDefault(Context context, String url, ImageView target, int defaultId) {
        try {
            if (TextUtils.isEmpty(url)) {
                target.setImageResource(defaultId);
            } else {
                Picasso.get().load(url).placeholder(defaultId).error(defaultId).into(target);
            }
        } catch (Exception e) {
            //防止图片加载回来target为null的情况
            e.printStackTrace();
        }
    }

    public static void loadImageByDefaultNoPlaceHolder(Context context, String url, ImageView target, int defaultId) {
        try {
            if (TextUtils.isEmpty(url)) {
                target.setImageResource(defaultId);
            } else {
                Picasso.get().load(url).noPlaceholder().error(defaultId).into(target);
            }
        } catch (Exception e) {
            //防止图片加载回来target为null的情况
            e.printStackTrace();
        }
    }

    public static void loadImage(Context context, String url, int width, int height, ImageView
            target) {
        if (TextUtils.isEmpty(url)) {

        } else {
            Picasso.get()
                    .load(url)
                    .centerCrop()
                    .resize(width, height)
                    .into(target);
        }
    }

    public static void loadImage(Context context, String url, int width, int height, ImageView
            target, int defaultId) {
        if (TextUtils.isEmpty(url)) {
            target.setImageResource(defaultId);
        } else {
            Picasso.get()
                    .load(url)
                    .resize(width, height)
                    .centerCrop()
                    .placeholder(defaultId)
                    .error(defaultId)
                    .into(target);
        }
    }

    public static void loadImage(Context context, String url, int defaultId, ImageView imageView, Callback callback) {
        if (TextUtils.isEmpty(url)) {
            imageView.setImageResource(defaultId);
        } else {
            Picasso.get()
                    .load(url)
                    .resize(150, 150)
                    .centerCrop()
                    .placeholder(defaultId)
                    .error(defaultId)
                    .into(imageView, callback);
        }
    }

    public static void loadImage(Context context, String url, int width, int height, ImageView
            target, int defaultId, Callback callback) {
        Picasso.get()
                .load(url)
                .resize(width, height)
                .centerCrop()
                .placeholder(defaultId)
                .error(defaultId)
                .into(target, callback);
    }

    public static void loadImage(Context context, String path, ImageView imageView, int width, int
            height, int defaultId) {
        Picasso.get()
                .load(path)
                .resize(width, height)
                .centerCrop()
                .placeholder(defaultId)
                .error(defaultId)
                .into(imageView);
    }

    public static void loadImage(Context context, String path, ImageView imageView, int width, int
            height) {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        Picasso.get()
                .load(path)
                .resize(width, height)
                .centerCrop()
                .into(imageView);
    }

    public static void loadImageForGift(Context context, int resourceId, ImageView imageView) {
        Picasso.get().load(resourceId).resize(150, 150).centerInside().into(imageView);
    }

    public static void loadImage(Context context, int resourceId, ImageView imageView, int width, int
            height) {
        Picasso.get()
                .load(resourceId)
                .resize(width, height)
                .centerCrop()
                .into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView, int width, int
            height) {
        Picasso.get()
                .load(file)
                .resize(width, height)
                .centerCrop()
                .into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView, int width, int
            height, int defaultId) {
        Picasso.get()
                .load(file)
                .resize(width, height)
                .centerCrop()
                .placeholder(defaultId)
                .error(defaultId)
                .into(imageView);
    }
}
