package com.androidtoolkit.emulator;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.TextUtils;

import com.androidtoolkit.PLog;

/**
 * game_werewolf
 * 2018/7/31 下午1:59
 * Mystery
 */

public class EmulatorCheckUtil {

    public interface EmulatorCheckCallback {
        void findEmulator(String emulatorInfo);
    }

    private EmulatorCheckUtil() {

    }

    private static class SingletonHolder {
        private static final EmulatorCheckUtil INSTANCE = new EmulatorCheckUtil();
    }

    public static final EmulatorCheckUtil getSingleInstance() {
        return SingletonHolder.INSTANCE;
    }

    private EmulatorCheckCallback emulatorCheckCallback;
    private boolean hasCheck = false;
    private boolean isEmulator = false;

    public boolean readSysProperty() {
        return readSysProperty(null);
    }

    public boolean readSysProperty(EmulatorCheckCallback callback) {
        this.emulatorCheckCallback = callback;
        int suspectCount = 0;


        String systemServerIsEmulator = CommandUtil.getSingleInstance().getProperty("ro.kernel.qemu");
        if (!TextUtils.isEmpty(systemServerIsEmulator) && TextUtils.equals(systemServerIsEmulator, "1")) {
            ++suspectCount;
        }

        String baseBandVersion = CommandUtil.getSingleInstance().getProperty("gsm.version.baseband");
        if (TextUtils.isEmpty(baseBandVersion) || (baseBandVersion != null && baseBandVersion.contains("1.0.0.0")))
            ++suspectCount;

        String buildFlavor = CommandUtil.getSingleInstance().getProperty("ro.build.flavor");
        if (TextUtils.isEmpty(buildFlavor) || (buildFlavor != null && buildFlavor.contains("vbox")))
            ++suspectCount;

        String productBoard = CommandUtil.getSingleInstance().getProperty("ro.product.board");
        if (TextUtils.isEmpty(productBoard) || (productBoard != null && productBoard.contains("android")))
            ++suspectCount;

        String boardPlatform = CommandUtil.getSingleInstance().getProperty("ro.board.platform");
        if (TextUtils.isEmpty(boardPlatform) || (boardPlatform != null && boardPlatform.contains("android")))
            ++suspectCount;

        if (!TextUtils.isEmpty(productBoard)
                && !TextUtils.isEmpty(boardPlatform)
                && !productBoard.equals(boardPlatform))
            ++suspectCount;

        String filter = CommandUtil.getSingleInstance().exec("cat /proc/self/cgroup");
        if (TextUtils.isEmpty(filter)) ++suspectCount;

        if (emulatorCheckCallback != null) {
            StringBuffer stringBuffer = new StringBuffer("Emulator Check Result\n")
                    .append("systemServerIsEmulator:" + systemServerIsEmulator)
                    .append("\n")
                    .append("baseBandVersion:" + baseBandVersion)
                    .append("\n")
                    .append("buildFlavor:" + buildFlavor)
                    .append("\n")
                    .append("productBoard:" + productBoard)
                    .append("\n")
                    .append("boardPlatform:" + boardPlatform)
                    .append("\n")
                    .append(filter)
                    .append("\nsuspectCount:" + suspectCount);

            emulatorCheckCallback.findEmulator(stringBuffer.toString());
            emulatorCheckCallback = null;
        }
        return suspectCount > 2;
    }

    public boolean hasGyroscopeSensor(Context context) {
        return getSystemSensor(context, Sensor.TYPE_GYROSCOPE);
    }

    public boolean hasLightSensor(Context context) {
        return getSystemSensor(context, Sensor.TYPE_LIGHT);
    }

    private boolean getSystemSensor(Context context, int type) {
        if (context == null) return false;
        SensorManager manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (manager == null) return false;
        Sensor sensor = manager.getDefaultSensor(type);
        if (sensor == null) return false;
        manager.registerListener(new MySensorEventListener(manager), sensor, SensorManager.SENSOR_DELAY_NORMAL);
        return true;
    }

    private class MySensorEventListener implements SensorEventListener {
        SensorManager sensorManager;

        MySensorEventListener(SensorManager sensorManager) {
            this.sensorManager = sensorManager;
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (sensorManager != null) {
                sensorManager.unregisterListener(this);
                sensorManager = null;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    @Deprecated
    public String readBuildInfo() {
        StringBuffer sb = new StringBuffer();
        sb.append("-\n")
                .append("BOARD-")
                .append(android.os.Build.BOARD)
                .append("\nBOOTLOADER-")
                .append(android.os.Build.BOOTLOADER)
                .append("\nBRAND-")
                .append(android.os.Build.BRAND)
                .append("\nDEVICE-")
                .append(android.os.Build.DEVICE)
                .append("\nHARDWARE-")
                .append(android.os.Build.HARDWARE)
                .append("\nMODEL-")
                .append(android.os.Build.MODEL)
                .append("\nPRODUCT-")
                .append(android.os.Build.PRODUCT);
        return sb.toString();
    }

    public static String getEmCode(Context context) {
        if (isEmulator(context)) {
            return "1";
        } else {
            return "0";
        }
    }

    public static boolean isEmulator(Context context) {
        PLog.i("Checker", Thread.currentThread().getName() + "checking");
        try {
            if (getSingleInstance().hasCheck) {
                return getSingleInstance().isEmulator;
            } else {

                long startTime = System.currentTimeMillis();
                /**
                 * 先进行检测是否是模拟器
                 *
                 */
                boolean isEmulator = getSingleInstance().readSysProperty();

                if (isEmulator) {
                    getSingleInstance().hasCheck = true;
                    getSingleInstance().isEmulator = isEmulator;
                } else {
                    /**
                     * 在进行传感器检测
                     */
                    boolean hasSensor = getSingleInstance().hasLightSensor(context);
                    if (hasSensor) {
                        isEmulator = false;
                        getSingleInstance().hasCheck = true;
                        getSingleInstance().isEmulator = isEmulator;
                    } else {
                        isEmulator = true;
                        getSingleInstance().hasCheck = true;
                        getSingleInstance().isEmulator = isEmulator;
                    }
                }

                long endTime = System.currentTimeMillis();
                PLog.i("Checker", "Em cost " + (endTime - startTime));
                return isEmulator;


            }
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
}