package com.androidtoolkit.transport;

/**
 * game_werewolf
 * 2018/3/5 下午1:12
 * Mystery
 */

public interface onTransport<T> {

    T get();
}
