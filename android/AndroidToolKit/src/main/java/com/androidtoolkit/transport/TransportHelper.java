package com.androidtoolkit.transport;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2018/3/5 下午1:13
 * Mystery
 */

public enum TransportHelper {

    INSTANCE;

    private Map<String, Object> mMap;

    private TransportHelper() {
        mMap = new HashMap<>();
    }

    public static TransportHelper getInstance() {
        return INSTANCE;
    }

    public static <T> void putTransportable(String key, T data) {
        getInstance().mMap.put(key, data);
    }

    public boolean hasTransportable(String key) {
        return getInstance().mMap.containsKey(key);
    }

    public static <T> T getTransport(String key) {
        try {
            T result = (T) getInstance().mMap.remove(key);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
