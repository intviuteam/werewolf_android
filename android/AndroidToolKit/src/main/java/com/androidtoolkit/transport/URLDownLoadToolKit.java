package com.androidtoolkit.transport;

import android.util.Log;

import com.toolkit.action.Action;
import com.toolkit.action.Filter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * game_werewolf
 * 2018/4/3 下午2:03
 * Mystery
 */

public class URLDownLoadToolKit {

    public static File DownloadFile(String urlPath, String downloadDir, Action<Float> percentListener, Filter isCancel) throws Exception {
        File file = null;
        try {
            if (isCancel == null) {
                isCancel = () -> false;
            }
            // 统一资源
            URL url = new URL(urlPath);
            // 连接类的父类，抽象类
            URLConnection urlConnection = url.openConnection();
            // http的连接类
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;

            TransportCompat.compatURLConnection(httpURLConnection);
            // 设定请求的方法，默认是GET
//            httpURLConnection.setRequestMethod("GET");
            // 设置字符编码
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            // 打开到此 URL 引用的资源的通信链接（如果尚未建立这样的连接）。
            httpURLConnection.connect();

            // 文件大小
            int fileLength = httpURLConnection.getContentLength();

            // 文件名
            String filePathUrl = httpURLConnection.getURL().getFile();
            String fileFullName = filePathUrl.substring(filePathUrl.lastIndexOf(File.separatorChar) + 1);

            BufferedInputStream bin = new BufferedInputStream(httpURLConnection.getInputStream());

            String path = downloadDir + File.separatorChar + fileFullName;
            file = new File(path);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            OutputStream out = new FileOutputStream(file);
            int size = 0;
            int len = 0;
            byte[] buf = new byte[1024];
            int lastpercent = 0;
            while (((size = bin.read(buf)) != -1) && !isCancel.filter()) {
                len += size;
                out.write(buf, 0, size);
                if (fileLength != 0) {
                    float percent = (len * 100 / fileLength);
                    if (percentListener != null && ((int) percent) != lastpercent) {
                        percentListener.func(percent);
                    }
                    lastpercent = (int) percent;
                }
            }
            bin.close();
            out.close();
            httpURLConnection.disconnect();
            if (file.length() == fileLength) {
                Log.i("MiniGame", "DownloadFile: success");
                return file;
            } else {
                /**
                 * 加载失败了需要吧这个文件删除
                 */
                Log.i("MiniGame", "DownloadFile: Error may be cancel or error or else");
                file.delete();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw e;
        }
        return null;
    }
}
