package com.androidtoolkit.transport;

import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

import com.facebook.react.modules.network.OkHttpClientProvider;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TransportCompat {


    public static void compatOkHttp(OkHttpClient.Builder builder) {
        OkHttpClientProvider.enableTls12OnPreLollipop(builder);
    }

    public static WebResourceResponse compatWebViewInterceptRequest(WebView view, String url) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            return compatWebViewInterceptRequestOkHttp(view, url);
        } else {
            return null;
        }
    }

    public static WebResourceResponse compatWebViewInterceptRequestURL(WebView view, String url) {
        try {
            URL urlConnection = new URL(url);
            URLConnection connection = urlConnection.openConnection();
            TransportCompat.compatURLConnection(connection);
            return new WebResourceResponse(URLConnection.guessContentTypeFromStream(connection.getInputStream()), connection.getHeaderField("encoding"), connection.getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static WebResourceResponse compatWebViewInterceptRequestOkHttp(WebView view, String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }

        if (url.startsWith("https")) {
            Request request = new Request.Builder().url(url).build();
            try {
                Response response = HttpTransportUtils.execute(request);
                InputStream inputStream = response.body().byteStream();
                String contentType = response.body().contentType().toString();
                String encoding = response.header("encoding", "UTF-8");
                return new WebResourceResponse(null, null, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void compatURLConnection(URLConnection connection) {
        if (connection instanceof HttpsURLConnection) {
            try {
                // 自定义一个信任所有证书的TrustManager，添加SSLSocketFactory的时候要用到
                final X509TrustManager trustAllCert =
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        };
                final SSLSocketFactory sslSocketFactory = new SSLSocketFactoryCompat(trustAllCert);
                ((HttpsURLConnection) connection).setSSLSocketFactory(sslSocketFactory);
            } catch (Exception e) {

            }
        }
    }
}
