package com.androidtoolkit;

import android.util.Log;

public class PLog {

    public static boolean needLog = true;

    private static boolean ON = true;//will be false automatically when release
    private static boolean INFO = true;
    private static boolean DEBUG = true;
    private static boolean VERBOSE = true;
    private static boolean ERROR = true;

    public static void debugAble(boolean needLog) {
        PLog.needLog = needLog;
    }

    public static void v(String TAG, String msg) {
        if (ON && VERBOSE && needLog) {
            Log.v(TAG, msg);
        }
    }

    public static void i(String TAG, String msg) {
        if (ON && INFO && needLog) {
            Log.i(TAG, msg);
        }
    }

    public static void d(String TAG, String msg) {
        if (ON && DEBUG && needLog) {
            Log.d(TAG, msg);
        }
    }

    public static void e(String TAG, String msg) {
        if (ON && ERROR && needLog) {
            Log.e(TAG, msg);
        }
    }

    public static void e(String TAG, String msg, Throwable e) {
        if (ON && ERROR && needLog) {
            Log.e(TAG, msg, e);
        }
    }
}
