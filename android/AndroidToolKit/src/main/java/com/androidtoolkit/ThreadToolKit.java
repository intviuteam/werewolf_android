package com.androidtoolkit;

import com.toolkit.action.ActionListener;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2017/5/19 上午10:25
 * Mystery
 */

public class ThreadToolKit {

    public static Executor singleThreadPool = null;

    public static void AsyncInSinglePool(Runnable runnable) {
        AsyncInSinglePool(runnable, null);
    }

    public static void AsyncInSinglePool(Runnable runnable, ActionListener listener) {
        if (singleThreadPool == null) {
            singleThreadPool = Executors.newSingleThreadExecutor();
        }
        singleThreadPool.execute(() -> {
            try {
                if (listener != null) {
                    listener.onStart();
                }
                runnable.run();
                if (listener != null) {
                    listener.onComplete();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (listener != null) {
                    listener.onError(e);
                }
            }
        });
    }

    public static void Async(Runnable runnable) {
        Async(runnable, null);
    }

    /**
     * 异步接口 在线程池中操作
     *
     * @param runnable
     * @param listener
     */
    public static void Async(Runnable runnable, ActionListener listener) {
        Observable.just(runnable)
                .observeOn(Schedulers.io())
                .onBackpressureBuffer()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Runnable>() {

                    @Override
                    public void onStart() {
                        if (listener != null) {
                            listener.onStart();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        if (listener != null) {
                            listener.onComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onError(e);
                        }
                    }

                    @Override
                    public void onNext(Runnable runnable) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
    }

    public static void Sync(Runnable runnable) {
        Sync(runnable, null);
    }

    public static void ExecuteInCurThread(Runnable runnable) {
//        Observable.just(runnable)
//                .onBackpressureBuffer()
//                .subscribe(runnable1 -> {
//                    runnable.run();
//                });
        if (runnable != null) {
            runnable.run();
        }
    }

    public static void Sync(Runnable runnable, ActionListener listener) {
        Observable.just(runnable)
                .onBackpressureBuffer()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Runnable>() {

                    @Override
                    public void onStart() {
                        if (listener != null) {
                            listener.onStart();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        if (listener != null) {
                            listener.onComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onError(e);
                        }
                    }

                    @Override
                    public void onNext(Runnable runnable) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
    }
}
