package com.androidtoolkit;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.text.TextUtils;

import com.toolkit.ToolKit;

import java.util.Locale;


/**
 * game_werewolf
 * 2017/8/31 下午12:15
 * Mystery
 */

public class ApkToolKit {

    private static final String TAG = "SystemUtils";

    private static Context sContext;

    private static String apkSign = "";
    private static String Language = "";

    public static void Bind(Context context) {
        sContext = context;
    }

    public static String GetApkSignString() {
        try {
            PackageInfo packageInfo = sContext.getPackageManager().getPackageInfo(sContext.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];
            apkSign = ToolKit.Encryption(sign.toByteArray(), "SHA");
            return apkSign;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String GetApkVerifyStringByMD5() {
        if (!TextUtils.isEmpty(apkSign)) {
            return apkSign;
        } else {
            return GetApkSignString();
        }
    }

    public static String GetApkLanguage() {
        if (!TextUtils.isEmpty(Language)) {
            return Language;
        }
        String curLanguage = "zh-CN";
        try {
            Locale locale;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                locale = sContext.getResources().getConfiguration().getLocales().get(0);
            } else {
                locale = sContext.getResources().getConfiguration().locale;
            }
            curLanguage = locale.getLanguage() + "-" + locale.getCountry();
            Language = curLanguage;
            return curLanguage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curLanguage;
    }
}
