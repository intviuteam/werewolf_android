package com.androidtoolkit;

import android.os.Build;
import android.text.TextUtils;

import com.androidtoolkit.transport.TransportCompat;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Created by lloydfinch on 21/10/2017.
 * <p>
 * 执行下载功能
 * 传入 url
 * 返回 byte数组
 */

public class HttpToolkit {

    private static final int TIMEOUT = 10000;

    private HttpToolkit() {

    }

    public static void closeConnection(HttpURLConnection connection) {
        if (connection != null) {
            try {
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 用于处理GET请求
     * 返回数据的字节数组
     */
    public static byte[] doGet(String path) {

        byte[] data = null;

        if (!TextUtils.isEmpty(path)) {

            HttpURLConnection connection = null;
            InputStream inputStream = null;

            try {
                URL url = new URL(path);
                connection = (HttpURLConnection) url.openConnection();
                TransportCompat.compatURLConnection(connection);
                connection.setRequestMethod("GET");

                connection.setRequestProperty("Charset", "UTF-8");
                connection.setRequestProperty("User-Agent", "ting_4.1.7(MI2, Android" + Build.VERSION.SDK_INT + ")");

                //Socket打开连接的时间
                connection.setConnectTimeout(TIMEOUT);

                //设置打开链接后读取的最大时间
                connection.setReadTimeout(TIMEOUT);

                //设置自动处理302/307错误的跳转,通常返回200
                connection.setInstanceFollowRedirects(true);

                //设置可读写
                //connection.setDoInput(true);
                //connection.setDoOutput(true);

                //进行连接
                connection.connect();

                if (connection.getResponseCode() == 200) {

                    inputStream = connection.getInputStream();
//                    //获取服务器头信息
//                    String encoding = connection.getHeaderField("Content-Encoding");
//                    if (encoding == null) {
//                        encoding = connection.getHeaderField("content-encoding");
//                    } else if (TextUtils.equals(encoding, "gzip")) {
//                        //表示数据经过压缩,需要解压缩
//                        inputStream = new GZIPInputStream(inputStream);
//                    }

                    //读取inputStream中的数据
                    data = StreamToolKit.readStream(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                StreamToolKit.close(inputStream);
                closeConnection(connection);
            }
        }
        return data;
    }
}