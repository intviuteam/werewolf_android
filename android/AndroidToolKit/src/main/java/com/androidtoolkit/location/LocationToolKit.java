package com.androidtoolkit.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import java.util.List;

/**
 * game_werewolf
 * 2018/3/1 下午1:43
 * Mystery
 */

public class LocationToolKit {
    /**
     * 尝试刷新地理位置，这里不进行权限的校验，默认赋予权限
     *
     * @param context
     */
    public static void UpdateGlobalLocation(Context context) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = locationManager.getProviders(true);
            String locationProvider = "";
            if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
                //如果是Network
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else if (providers.contains(LocationManager.GPS_PROVIDER)) {
                //如果是GPS
                locationProvider = LocationManager.GPS_PROVIDER;
            } else {
                /**
                 * 无可用的位置
                 */
                return;
            }
            // TODO: 2018/3/1 这里不进行权限校验，权限校验由方法调用者考虑
            Location location = locationManager.getLastKnownLocation(locationProvider);
            if (location != null) {
                GlobalLocation.INSTANCE.setLatitude(String.valueOf(location.getLatitude()));
                GlobalLocation.INSTANCE.setLongitude(String.valueOf(location.getLongitude()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
}
