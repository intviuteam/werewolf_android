package com.androidtoolkit.location;

/**
 * game_werewolf
 * 2018/3/1 上午11:48
 * Mystery
 */

public enum GlobalLocation {

    INSTANCE;

    private String Longitude;
    private String Latitude;

    private GlobalLocation() {

    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }
}
