package com.androidtoolkit;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2017/11/9 下午12:23
 * Mystery
 */

public enum CodeTimeRecord {


    iRecord;

    private static final String TAG = "CodeTimeRecord";

    private Map<String, Long> timeRecord;

    private CodeTimeRecord() {
        timeRecord = new HashMap<>();
    }


    public void recordCodeStart(String action) {
        if (!PLog.needLog)
            return;
        try {
            timeRecord.put(action, System.currentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recordCodeEnd(String action) {
        if (!PLog.needLog)
            return;
        try {
            Log.i(TAG, "[" + action + "] cost Time [" + Thread.currentThread() + "]" + (System.currentTimeMillis() - timeRecord.get(action)));
            timeRecord.remove(action);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
