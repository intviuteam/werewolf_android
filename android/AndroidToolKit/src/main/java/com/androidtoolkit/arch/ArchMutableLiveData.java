package com.androidtoolkit.arch;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

/**
 * game_werewolf
 * 2018/7/30 下午7:10
 * Mystery
 */
public class ArchMutableLiveData<T> extends MutableLiveData<T> {

    private static final String TAG = "ArchMutableLiveData";

    @Override
    protected void onActive() {
        super.onActive();
        Log.i(TAG, "onActive: ");
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        Log.i(TAG, "onInactive: ");
    }
}
