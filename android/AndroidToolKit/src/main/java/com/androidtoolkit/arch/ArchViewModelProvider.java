package com.androidtoolkit.arch;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelStore;
import android.arch.lifecycle.ViewModelStoreOwner;
import android.support.annotation.NonNull;

/**
 * game_werewolf
 * 2018/7/30 下午3:00
 * Mystery
 * <p>
 * ViewModelProvider 的封装
 */
public class ArchViewModelProvider extends ViewModelProvider {

    private ViewModelStore mViewModelStoreHolder;

    public ArchViewModelProvider(@NonNull ViewModelStoreOwner owner, @NonNull Factory factory) {
        this(owner.getViewModelStore(), factory);
    }

    public ArchViewModelProvider(@NonNull ViewModelStore store, @NonNull Factory factory) {
        super(store, factory);
        mViewModelStoreHolder = store;
    }

    public void destroy() {
        if (mViewModelStoreHolder != null) {
            mViewModelStoreHolder.clear();
            mViewModelStoreHolder = null;
        }
    }
}
