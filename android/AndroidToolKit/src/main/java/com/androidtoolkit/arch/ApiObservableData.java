package com.androidtoolkit.arch;

/**
 * game_werewolf
 * 2018/7/30 下午1:47
 * Mystery
 */
public class ApiObservableData<T> {

    public T data;
    public Exception e;

    public static <T> ApiObservableData<T> createApiObservableData(T data, Exception e) {
        ApiObservableData<T> result = new ApiObservableData<>();
        result.data = data;
        result.e = e;
        return result;
    }
}
