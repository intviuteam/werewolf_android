package com.androidtoolkit.arch;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.lifecycle.ViewModelStore;
import android.support.annotation.MainThread;

/**
 * game_werewolf
 * 2018/7/30 下午1:40
 * Mystery
 * 对ViewModelProviders的一层封装
 */
public class ArchViewModelProviders extends ViewModelProviders {

    @MainThread
    public static ArchViewModelProvider of() {
        return new ArchViewModelProvider(new ViewModelStore(), new ArchViewModelFactory());
    }
}
