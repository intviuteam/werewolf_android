package com.androidtoolkit.arch;

import android.arch.lifecycle.Observer;

/**
 * game_werewolf
 * 2018/7/30 下午1:42
 * Mystery
 */
public interface DataObserver<T> extends Observer<T> {
}
