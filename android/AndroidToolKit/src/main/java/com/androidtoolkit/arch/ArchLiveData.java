package com.androidtoolkit.arch;

import android.arch.lifecycle.LiveData;
import android.util.Log;

/**
 * game_werewolf
 * 2018/7/30 下午7:09
 * Mystery
 */
public class ArchLiveData<T> extends LiveData<T> {

    private static final String TAG = "ArchLiveData";

    @Override
    protected void onInactive() {
        super.onInactive();
        Log.i(TAG, "onInactive: ");
    }

    @Override
    protected void onActive() {
        super.onActive();
        Log.i(TAG, "onActive: ");
    }
}
