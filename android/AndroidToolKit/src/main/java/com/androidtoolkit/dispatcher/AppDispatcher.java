package com.androidtoolkit.dispatcher;


import android.text.TextUtils;

import java.util.*;

/**
 * game_werewolf
 * 2018/7/5 下午6:08
 * Mystery
 * <p>
 * App内部使用Dispatch
 * <p>
 * <p>
 * 2018-07-07 第一版本，测试版本
 * 1。目前只能在UI线程进行使用
 * 2。未支持一次性Observer
 */
public class AppDispatcher {

    private static final String TAG = "AppDispatcher";

    static volatile AppDispatcher defaultInstance;

    public static AppDispatcher get() {
        if (defaultInstance == null) {
            synchronized (AppDispatcher.class) {
                if (defaultInstance == null) {
                    defaultInstance = new AppDispatcher();
                }
            }
        }
        return defaultInstance;
    }

    private final Map<String, AppDispatcherObserver> mObserverMap = new HashMap<>();
    private final Map<Class<?>, AppDispatcherClassRecord> mClassRecordMap = new HashMap<>();

    private AppDispatcher() {

    }


    void register(Class clazz, AppDispatcherObserver observer) {
        AppDispatcherClassRecord record = mClassRecordMap.get(clazz);
        if (record == null) {
            record = new AppDispatcherClassRecord();
        }
        record.addRecord(observer.getObserverKey(), observer);
        mClassRecordMap.put(clazz, record);
    }

    public void register(AppDispatcherObserver observer) {
        mObserverMap.put(observer.getObserverKey(), observer);
    }

    public void post(final Object o) {

        if (o == null) return;

        Class targetClass = o.getClass();

        AppDispatcherClassRecord record = mClassRecordMap.get(targetClass);

        if (record == null) return;

        record.handleEvent(o);
    }

    public void unregister(Object o) {
        if (o == null) return;
        unregister(o.getClass().getName());
    }

    public void unregister(String key) {
        if (TextUtils.isEmpty(key)) {
            return;
        }

        /**
         * 从监听者列表删除
         */
        final AppDispatcherObserver observer = mObserverMap.remove(key);

        if (observer == null) return;

        observer.travelMethodMap(value -> {
            /**
             * 从类record中清理数据
             */
            AppDispatcherClassRecord record = mClassRecordMap.get(value);
            if (record != null && !record.isEmpty()) {
                record.removeRecord(observer.getObserverKey());
            }
            if (record.isEmpty()) {
                mClassRecordMap.remove(value);
            }
        });

    }

    public void printData() {
        System.out.println("ObserverMap " + mObserverMap.size());
        System.out.println("mClassRecordMap " + mClassRecordMap.size());
    }


    private static void TestCode() {
        AppDispatcher dispatcher = AppDispatcher.get();

        Object observerObject = new Object();

        dispatcher.register(new AppDispatcherObserver(observerObject) {
            @Override
            public void initMethodMap() {
                /**
                 * 注册监听事件
                 */
                recordMethod(String.class, s -> {
                    /**
                     * 处理事件
                     */
                });

                recordMethod(Integer.class, s -> {
                    /**
                     * 处理事件
                     */
                });
            }
        });

        /**
         * 发放事件
         */

        dispatcher.post(5);

        dispatcher.post("TestPost");

        /**
         * 解除注册事件
         */
        dispatcher.unregister(observerObject);
    }

}
