package com.androidtoolkit.dispatcher;

public interface EventMethod<T> {

    void onEvent(T t);
}
