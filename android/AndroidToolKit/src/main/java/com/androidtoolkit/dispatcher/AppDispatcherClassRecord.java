package com.androidtoolkit.dispatcher;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

class AppDispatcherClassRecord {

    private Map<String, AppDispatcherObserver> observerMap;

    AppDispatcherClassRecord() {
        observerMap = new LinkedHashMap<>();
    }

    void addRecord(String key, AppDispatcherObserver observer) {
        observerMap.put(key, observer);
    }

    void removeRecord(String key) {
        observerMap.remove(key);
    }

    boolean isEmpty() {
        return observerMap.isEmpty();
    }

    void handleEvent(final Object o) {
        Iterator<Map.Entry<String, AppDispatcherObserver>> iterator = observerMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, AppDispatcherObserver> entry = iterator.next();
            AppDispatcherObserver observer = entry.getValue();
            observer.handleEvent(o);
        }
    }

    @Override
    public String toString() {
        return "AppDispatcherClassRecord{" +
                "observerMap=" + observerMap +
                '}';
    }
}
