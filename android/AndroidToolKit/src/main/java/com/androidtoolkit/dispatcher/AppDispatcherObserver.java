package com.androidtoolkit.dispatcher;


import android.text.TextUtils;

import com.toolkit.action.TravelCollection;

import java.util.*;

public abstract class AppDispatcherObserver {

    private Map<Class<?>, EventMethodBean> mMethodMap;
    private String mObserverKey;

    public AppDispatcherObserver(String observerKey) {
        if (TextUtils.isEmpty(observerKey)) {
            throw new IllegalArgumentException("observerKey can not be empty");
        }
        mMethodMap = new LinkedHashMap<>();
        mObserverKey = observerKey;
        initMethodMap();
    }

    public AppDispatcherObserver(Object o) {
        this(o.getClass().getName());
    }

    public abstract void initMethodMap();

    public <T> void recordMethod(Class<T> clazz, EventMethod<T> eventMethod) {
        mMethodMap.put(clazz, new EventMethodBean(mObserverKey, eventMethod));
        AppDispatcher.get().register(clazz, this);
    }

    Map<Class<?>, EventMethodBean> getMethodMap() {
        return mMethodMap;
    }

    EventMethodBean getMethod(Class<?> clazz) {
        return mMethodMap.get(clazz);
    }

    public String getObserverKey() {
        return mObserverKey;
    }

    void handleEvent(Object o) {
        if (o == null) return;

        EventMethodBean method = getMethod(o.getClass());

        if (method != null) {
            method.onEvent(o);
        }
    }

    void travelMethodMap(TravelCollection<Class<?>> travelCollection) {
        if (!mMethodMap.isEmpty()) {
            Iterator<Map.Entry<Class<?>, EventMethodBean>> iterator = mMethodMap.entrySet().iterator();
            while (iterator.hasNext()) {
                if (travelCollection != null) {
                    travelCollection.onTravel(iterator.next().getKey());
                }
            }
        }
    }

    @Override
    public String toString() {
        return "AppDispatcherObserver{" +
                "mMethodMap=" + mMethodMap +
                ", mObserverKey='" + mObserverKey + '\'' +
                '}';
    }
}
