package com.androidtoolkit.dispatcher;

public class EventMethodBean<T> implements EventMethod<T> {

    private EventMethod<T> source;
    private String mParentKey;

    public EventMethodBean(String parentKey, EventMethod<T> source) {
        this.source = source;
        this.mParentKey = parentKey;
    }

    public String getParentKey() {
        return mParentKey;
    }

    @Override
    public void onEvent(T t) {
        if (source != null) {
            source.onEvent(t);
        }
    }
}
