package com.androidtoolkit;

import android.text.TextUtils;
import android.util.Log;

import com.toolkit.action.Action;
import com.toolkit.action.ActionFilter;
import com.toolkit.action.ActionListener;
import com.toolkit.action.Filter;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2017/11/25 下午1:17
 * Mystery
 */

public class RxToolKit {

    private static final String TAG = "RxToolKit";

    public static final int ANDROID_UI_THREAD = 0;
    public static final int ANOTHER_THREAD = 1;

    public static <T> RxBuilder<T> Build(Object object, Class<T> forceClass) {
        if (object == null) {
            throw new IllegalArgumentException();
        }
        return Build(object.getClass(), forceClass);
    }

    public static <T> RxBuilder<T> Build(Class target, Class<T> forceClass) {
        if (target == null) {
            throw new IllegalArgumentException();
        }
        return Build(target.getName(), forceClass);
    }

    public static <T> RxBuilder<T> Build(String target, Class<T> forceClass) {
        RxBuilder<T> rxBuilder = new RxBuilder<>();
        rxBuilder.target(target).focusClass(forceClass);
        return rxBuilder;
    }

    private static <T> void Register(RxBuilder<T> rxBuilder) {
        if (rxBuilder == null) {
            throw new IllegalArgumentException();
        }
        if (TextUtils.isEmpty(rxBuilder.target)) {
            throw new IllegalArgumentException();
        }
        Observable<T> observable = RxBus.turnToObservable(rxBuilder.focusClass);
        if (rxBuilder.backPressureAble) {
            observable = observable.onBackpressureBuffer();
        }
        if (rxBuilder.observeOn == ANDROID_UI_THREAD) {
            observable = observable.observeOn(AndroidSchedulers.mainThread());
        } else {
            observable = observable.observeOn(Schedulers.io());
        }
        if (rxBuilder.subscribeOn == ANDROID_UI_THREAD) {
            observable = observable.subscribeOn(AndroidSchedulers.mainThread());
        } else {
            observable = observable.subscribeOn(Schedulers.io());
        }

        if (rxBuilder.filter != null) {
            Subscription sb = observable.filter(t -> rxBuilder.filter.filter(t)).subscribe(new Subscriber<T>() {
                @Override
                public void onStart() {
                    super.onStart();
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onStart();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCompleted() {
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onComplete();
                        }
                        if (rxBuilder.destroyWhenFinish) {
                            if (isUnsubscribed()) {
                                unsubscribe();
                                Log.i(TAG, "onCompleted: unsubscribe");
                            }
                            RxBus.getInstance().unregister(rxBuilder.target);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onError(e);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onNext(T t) {
                    try {
                        if (rxBuilder.action != null) {
                            rxBuilder.action.func(t);
                        }
                        if (rxBuilder.destroyWhenActionDone && rxBuilder.destroyWhenFinish) {
                            onCompleted();
                        }
                    } catch (Throwable e) {
                        /**
                         * we catch all Exception when action is running
                         */
                        e.printStackTrace();
                    }
                }
            });
            RxBus.getInstance().register(rxBuilder.target, sb);
        } else {
            Subscription sb = observable.subscribe(new Subscriber<T>() {

                @Override
                public void onStart() {
                    super.onStart();
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onStart();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCompleted() {
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onComplete();
                        }
                        if (rxBuilder.destroyWhenFinish) {
                            if (isUnsubscribed()) {
                                unsubscribe();
                                Log.i(TAG, "onCompleted: unsubscribe");
                            }
                            RxBus.getInstance().unregister(rxBuilder.target);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        if (rxBuilder.actionListener != null) {
                            rxBuilder.actionListener.onError(e);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onNext(T t) {
                    try {
                        if (rxBuilder.action != null) {
                            rxBuilder.action.func(t);
                        }
                        if (rxBuilder.destroyWhenActionDone && rxBuilder.destroyWhenFinish) {
                            onCompleted();
                        }
                    } catch (Throwable e) {
                        /**
                         * we catch all Exception when action is running
                         */
                        e.printStackTrace();
                    }
                }
            });
            RxBus.getInstance().register(rxBuilder.target, sb);
        }
    }

    public static class RxBuilder<T> {


        private String target = null;
        private Class<T> focusClass = null;
        private Action<T> action = null;
        private ActionFilter<T> filter = null;
        private ActionListener actionListener = null;
        private int subscribeOn = ANDROID_UI_THREAD;
        private int observeOn = ANDROID_UI_THREAD;
        private boolean backPressureAble = true;
        private boolean destroyWhenFinish = false;
        private boolean destroyWhenActionDone = false;

        private RxBuilder() {

        }

        private RxBuilder<T> target(Class clazz) {
            target = clazz.getName();
            return this;
        }

        private RxBuilder<T> target(String val) {
            target = val;
            return this;
        }

        private RxBuilder<T> focusClass(Class<T> val) {
            focusClass = val;
            return this;
        }

        public RxBuilder<T> action(Action<T> val) {
            action = val;
            return this;
        }

        public RxBuilder<T> actionFilter(ActionFilter<T> val) {
            filter = val;
            return this;
        }

        public RxBuilder<T> actionListener(ActionListener val) {
            actionListener = val;
            return this;
        }

        public RxBuilder<T> subscribeOn(int val) {
            subscribeOn = val;
            return this;
        }

        public RxBuilder<T> observeOn(int val) {
            observeOn = val;
            return this;
        }

        public RxBuilder<T> backPressureAble(boolean val) {
            backPressureAble = val;
            return this;
        }

        public RxBuilder<T> destroyWhenFinish(boolean val) {
            destroyWhenFinish = val;
            return this;
        }

        public RxBuilder<T> destroyWhenActionDone(boolean val) {
            destroyWhenActionDone = val;
            return this;
        }

        public void register() {
            RxToolKit.Register(this);
        }
    }

    public static void Emit(Object object) {
        RxBus.emit(object);
    }

    public static void UnRegister(String targetKey) {
        RxBus.getInstance().unregister(targetKey);
    }

    public static void UnRegister(Object object) {
        RxBus.getInstance().unregister(object);
    }

}
