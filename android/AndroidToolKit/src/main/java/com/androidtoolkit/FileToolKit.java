package com.androidtoolkit;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.toolkit.ToolKit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created by lloydfinch on 29/10/2017.
 * 功能: 本地存取文件
 * byte数组，图片
 */

public class FileToolKit {

    private static final String TAG = "FileToolKit";

    private FileToolKit() {

    }

    /**
     * 将一个字符串写入 文件，写入不进行自动追加而是直接进行覆盖
     *
     * @param fileDir  文件目录结尾需要追加 「 ／ 」
     * @param fileName 文件的名字
     * @param data     需要写入的数据
     */
    public static boolean writeStringIntoFile(String fileDir, String fileName, String data) {
        File file = new File(fileDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        File realFile = new File(fileDir + fileName);
        if (!realFile.exists()) {
            try {
                realFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false; //文件创建失败
            }
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(realFile));
            bw.write(data);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            StreamToolKit.close(bw);
        }
    }

    /**
     * 从文件中读取字符串
     *
     * @param fileDir  文件目录结尾需要追加 「 ／ 」
     * @param fileName 文件名字
     * @return
     */
    public static String getStringFromFile(String fileDir, String fileName) {
        File file = new File(fileDir + fileName);
        BufferedReader br = null;
        StringBuilder result = new StringBuilder("");
        try {
            br = new BufferedReader(new FileReader(file));
            String s;
            while ((s = br.readLine()) != null) {
                result.append(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            StreamToolKit.close(br);
        }
        return result.toString();
    }


    /**
     * 根据url从本地取出字节流数据
     */
    public static byte[] loadFile(String url) {
        PLog.e(TAG, "loadFile: " + url);
        String cacheDir = StorageToolKit.GetStoragePath(com.androidtoolkit.Environment.GetAppContext(), "orange_wolf_file");
        byte[] ret = null;
        if (url != null) {
            String name = md5(url);
            File file = new File(cacheDir, name);
            if (file.exists()) {
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(file);
                    ret = StreamToolKit.readStream(fin);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    StreamToolKit.close(fin);
                }
            }
        }
        return ret;
    }

    /**
     * 将字节流保存保存在本地
     * 根据url命名
     */
    public static void saveFile(String url, byte[] data) {
        String cacheDir = StorageToolKit.GetStoragePath(com.androidtoolkit.Environment.GetAppContext(), "orange_wolf_file");
        if (url != null && data != null) {
            String name = md5(url);
            File file = new File(cacheDir, name);
            FileOutputStream fot = null;
            try {
                fot = new FileOutputStream(file);
                fot.write(data);
                PLog.e(TAG, "saveFile: " + name);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                StreamToolKit.close(fot);
            }
        }
    }

    /**
     * 将字节流保存在本地相册
     * 根据url命名
     */
    public static void saveFileToJpg(String url, byte[] data) {

        String galleryPath = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_DCIM
                + File.separator + "Camera" + File.separator;

        if (url != null && data != null) {
            String name = md5(url);
            FileOutputStream fot = null;
            try {
                File file = new File(galleryPath, name + ".jpg");
                fot = new FileOutputStream(file);
                fot.write(data);
                PLog.e(TAG, "saveFile: " + name);

                String s = MediaStore.Images.Media.insertImage(com.androidtoolkit.Environment.GetAppContext().getContentResolver(), file.getAbsolutePath(), file.toString(), null);
                PLog.e(TAG, "path: " + s);
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(file);
                intent.setData(uri);
                com.androidtoolkit.Environment.GetAppContext().sendBroadcast(intent);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                StreamToolKit.close(fot);
            }
        }
    }

    /**
     * 保存在本地成mp3格式
     */
    public static void saveToMp3(String url, byte[] data) {
        PLog.e(TAG, "saveToMp3: " + url);
        String cacheDir = StorageToolKit.GetStoragePath(com.androidtoolkit.Environment.GetAppContext(), "music");
        if (url != null && data != null) {
            String name = md5(url);
            File file = new File(cacheDir, name + ".mp3");
            FileOutputStream fot = null;
            try {
                fot = new FileOutputStream(file);
                fot.write(data);
                PLog.e(TAG, "saveFile: " + file.getAbsolutePath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                StreamToolKit.close(fot);
            }
        }
    }

    /**
     * 从本地加载
     */
    public static File loadFile2Mp3(String url) {
        PLog.e(TAG, "loadFile2Mp3: " + url);
        String cacheDir = StorageToolKit.GetStoragePath(com.androidtoolkit.Environment.GetAppContext(), "music");
        if (url != null) {
            String name = md5(url);
            File file = new File(cacheDir, name + ".mp3");
            if (file.exists()) {
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(file);
                    PLog.e(TAG, "saveFile: " + file.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    StreamToolKit.close(fin);
                }
            } else {
                file = null;
            }
            return file;
        }
        return null;
    }

    /**
     * 根据url进行md5转换，返回一个独一无二的字符串
     */
    private static String md5(String url) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        return ToolKit.Encryption(url.getBytes(), "md5");
    }

    /**
     * 获取指定路径文件或文件夹的大小
     */
    public static String getFileOrFilesSize(String filePath) {
        File file = new File(filePath);
        long blockSize = 0;
        try {
            if (file.isDirectory()) {
                blockSize = getFileSizes(file);
            } else {
                blockSize = getFileSize(file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FormatFileSize(blockSize);
    }

    /**
     * 获取指定文件大小
     */
    private static long getFileSize(File file) throws Exception {
        long size = 0;
        if (file.exists()) {
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            size = fis.available();
        } else {
            file.createNewFile();
        }
        return size;
    }

    /**
     * 获取指定文件夹大小
     */
    private static long getFileSizes(File f) throws Exception {
        long size = 0;
        File flist[] = f.listFiles();
        for (int i = 0; i < flist.length; i++) {
            if (flist[i].isDirectory()) {
                size = size + getFileSizes(flist[i]);
            } else {
                size = size + getFileSize(flist[i]);
            }
        }
        return size;
    }

    /**
     * 将文件的大小格式化成字符串展示
     */
    public static String FormatFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (fileS == 0) {
            return wrongSize;
        }
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format(((double) fileS) / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format(((double) (fileS)) / 1048576) + "MB";
        } else {
            fileSizeString = df.format(((double) (fileS)) / 1073741824) + "GB";
        }
        return fileSizeString;
    }
}
