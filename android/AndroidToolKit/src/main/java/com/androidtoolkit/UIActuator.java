package com.androidtoolkit;

import android.os.Handler;
import android.os.Looper;

/**
 * Author:1727
 * 2017/3/10
 */

public class UIActuator {

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static void remove(Runnable runnable) {
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    public static Handler getHandler() {
        return handler;
    }

    public static void post(Runnable runnable) {
        handler.post(runnable);
    }

    /**
     * 此方法会将Runnable里面的异常Catch住
     *
     * @param runnable
     */
    public static void postSafely(Runnable runnable) {
        postSafely(runnable, 0);
    }

    public static void postSafely(Runnable runnable, long dur) {
        handler.postDelayed(runnable, dur);
    }

    public static void postDelay(Runnable runnable, long dur) {
        handler.postDelayed(runnable, dur);
    }

}
