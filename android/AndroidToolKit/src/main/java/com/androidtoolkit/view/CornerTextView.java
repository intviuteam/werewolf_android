package com.androidtoolkit.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.support.R;

/**
 * game_werewolf
 * 2018/4/12 下午3:20
 * Mystery
 */
public class CornerTextView extends AppCompatTextView {

    private int mBorderWidth = 1;
    private int mBorderWidthColor = Color.WHITE;
    private int mCornersize = 8; //我们默认以px为单位
    private Paint mCornerPaint; //边框画笔   文字我们只用系统的 TextView

    @Override
    public boolean removeCallbacks(Runnable action) {
        return super.removeCallbacks(action);
    }

    public CornerTextView(Context context) {
        this(context, null);
    }

    public CornerTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CornerTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initAttrs(context, attrs);

    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CornerTextView);
        mBorderWidth = (int) array.getDimension(R.styleable.CornerTextView_borderWidth, mBorderWidth);
        mBorderWidthColor = array.getColor(R.styleable.CornerTextView_borderWidthColor, mBorderWidthColor);
        mCornersize = (int) array.getDimension(R.styleable.CornerTextView_cornerSize, mCornersize);
        array.recycle();

        mCornerPaint = new Paint();
        mCornerPaint.setAntiAlias(true);
        mCornerPaint.setDither(true);
        mCornerPaint.setStrokeWidth(mBorderWidth);
        mCornerPaint.setStyle(Paint.Style.FILL_AND_STROKE); //实心 只画边框也画心
        mCornerPaint.setColor(mBorderWidthColor);

    }


    @Override
    protected void onDraw(Canvas canvas) {

        RectF rectF = new RectF(mBorderWidth / 2, mBorderWidth / 2, getMeasuredWidth() - mBorderWidth, getMeasuredHeight() - mBorderWidth);
        canvas.drawRoundRect(rectF, mCornersize, mCornersize, mCornerPaint);


        super.onDraw(canvas);
    }

    public void setCornerSize(int size) { //设置的单位默认是px
        mCornersize = size;

    }
    //设置颜色

    public CornerTextView setTextBackGround(int color) {
        this.mBorderWidthColor = color;
        mCornerPaint.setColor(mBorderWidthColor);
        invalidate();
        return this;
    }
}
