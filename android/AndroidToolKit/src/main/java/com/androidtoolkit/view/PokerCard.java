package com.androidtoolkit.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.toolkit.action.Destroyable;

import java.util.List;

/**
 * game_werewolf
 * 2018/7/30 下午4:12
 * Mystery
 * <p>
 * 扑克牌View
 */
public class PokerCard extends AppCompatImageView {

    public interface PokerCardFlipListener {

        void onFlipStart();

        void onFlipEnd();

    }

    /**
     * 纸牌背面资源
     */
    private int backResource;

    /**
     * 纸牌正面资源
     */
    private int frontResource;

    private PokerCardFlipListener mListener;
    private ObjectAnimator oa1;
    private ObjectAnimator oa2;
    private DecelerateInterpolator oa1Interpolator = new DecelerateInterpolator();
    private AccelerateDecelerateInterpolator oa2Interpolator = new AccelerateDecelerateInterpolator();

    private long flipTime = 200;

    public PokerCard(@NonNull Context context) {
        this(context, null);
    }

    public PokerCard(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PokerCard(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setListener(PokerCardFlipListener listener) {
        mListener = listener;
    }

    /**
     * 设置卡牌背景
     *
     * @param backResource
     */
    public void setBack(int backResource) {
        this.backResource = backResource;
        setBackgroundResource(this.backResource);
    }

    /**
     * 设置纸牌正面
     *
     * @param frontResource
     */
    public void setCard(int frontResource) {
        this.frontResource = frontResource;
    }

    /**
     * 翻牌子
     */
    public void flip() {
        clearAnim();

        oa1 = ObjectAnimator.ofFloat(this, View.SCALE_X, 1f, 0f);
        oa2 = ObjectAnimator.ofFloat(this, View.SCALE_X, 0f, 1f);

        oa1.setInterpolator(oa1Interpolator);
        oa2.setInterpolator(oa2Interpolator);

        oa1.setDuration(flipTime / 2);
        oa2.setDuration(flipTime / 2);

        oa1.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (frontResource > 0) {
                    setBackgroundResource(frontResource);
                }
                oa2.start();
            }
        });
        oa1.start();

        oa2.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (mListener != null) {
                    mListener.onFlipEnd();
                }
            }

        });

        if (mListener != null) {
            mListener.onFlipStart();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnim();
    }

    private void clearAnim() {
        if (oa1 != null) {
            oa1.cancel();
        }
        if (oa2 != null) {
            oa2.cancel();
        }
    }

    public long getFlipTime() {
        return flipTime;
    }

    public void setFlipTime(long flipTime) {
        this.flipTime = flipTime;
    }
}
