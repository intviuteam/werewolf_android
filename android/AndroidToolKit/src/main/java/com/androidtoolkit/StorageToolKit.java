package com.androidtoolkit;

import android.content.Context;
import android.os.StatFs;
import android.os.storage.StorageManager;

import java.io.File;

/**
 * game_werewolf
 * 2018/4/3 下午2:10
 * Mystery
 */

public class StorageToolKit {

    private static final String TAG = "StorageToolKit";

    public static String getAppCachePath(Context context) {
        return context.getCacheDir().getPath();
    }

    /**
     * 得到一个存储路径
     *
     * @param context
     * @param dir
     * @return
     */
    public static String GetStoragePath(Context context, String dir) {
        String path;
        if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment.getExternalStorageState()) || !android.os.Environment.isExternalStorageRemovable()) {
            File externalFilesDir = context.getExternalFilesDir(dir);
            if (externalFilesDir != null) {
                path = externalFilesDir.getPath();
            } else {
                path = context.getFilesDir().getPath() + File.separator + dir;
            }
        } else {
            path = context.getFilesDir().getPath() + File.separator + dir;
        }
        PLog.i(TAG, "Get StoragePath " + path);
        return path;
    }


    /**
     * 获取主存
     */
    public static String getPrimaryStoragePath(Context context) {
        StorageManager sm = (StorageManager) context.getSystemService(context.STORAGE_SERVICE);
        String path = "";
        //emulated
        try {
            String[] paths = (String[]) sm.getClass().getMethod("getVolumePaths", new Class[0]).invoke(sm, new Object[]{});
            for (String p : paths) {
                if (p.contains("emulated")) {
                    path = p;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return path;
    }

    /**
     * 获取最大的存储空间
     */
    public static String getBiggestFreeStorage(Context context) {
        StorageManager sm = (StorageManager) context.getSystemService(context.STORAGE_SERVICE);
        String path = "";
        int biggest = 0;
        try {
            String[] paths = (String[]) sm.getClass().getMethod("getVolumePaths", new Class[0]).invoke(sm, new Object[]{});
            for (String p : paths) {
                StatFs sf = new StatFs(p);
                int availCount = sf.getAvailableBlocks();
                if (availCount > biggest) {
                    biggest = availCount;
                    path = p;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return path;
    }

    /**
     * 获取所有的存储空间列表
     */
    public static String[] getAllStorage(Context context) {
        StorageManager sm = (StorageManager) context.getSystemService(context.STORAGE_SERVICE);
        String[] paths = null;
        try {
            paths = (String[]) sm.getClass().getMethod("getVolumePaths", new Class[0]).invoke(sm, new Object[]{});
        } catch (Exception e) {
            e.printStackTrace();
        }

        return paths;
    }

}
