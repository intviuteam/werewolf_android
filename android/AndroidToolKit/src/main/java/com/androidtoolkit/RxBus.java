package com.androidtoolkit;

import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subscriptions.CompositeSubscription;

/**
 *
 */

public class RxBus {
    private static final String TAG = "RxToolKit";
    private static volatile RxBus mInstance;
    private SerializedSubject<Object, Object> mSubject;
    private ConcurrentHashMap<String, CompositeSubscription> mSubscriptionMap;

    private RxBus() {
        //mSubject = new SerializedSubject<>(PublishSubject.create());
        mSubject = PublishSubject.create().toSerialized();
    }

    public static RxBus getInstance() {
        if (mInstance == null) {
            synchronized (RxBus.class) {
                if (mInstance == null) {
                    mInstance = new RxBus();
                }
            }
        }
        return mInstance;
    }

    private void post(Object o) {
        mSubject.onNext(o);
    }

    public static void emit(Object o) {
        if (o == null) {
            return;
        }
        PLog.i(TAG, "post msg:" + o.toString());
        getInstance().post(o);
    }

    private <T> Observable<T> toObservable(final Class<T> type) {
        return mSubject.ofType(type);
    }

    public static <T> Observable<T> turnToObservable(final Class<T> type) {
        return getInstance().toObservable(type);
    }

    public void register(String key, Subscription subscription) {
        if (TextUtils.isEmpty(key)) {
            return;
        }
        if (mSubscriptionMap == null) {
            mSubscriptionMap = new ConcurrentHashMap<>();
        }
        if (mSubscriptionMap.get(key) != null) {
            mSubscriptionMap.get(key).add(subscription);
            PLog.i(TAG, "register [" + key + "]: success! add into a exist map");
        } else {
            CompositeSubscription compositeSubscription = new CompositeSubscription();
            compositeSubscription.add(subscription);
            mSubscriptionMap.put(key, compositeSubscription);
            PLog.i(TAG, "register [" + key + "]: success! add into a new map");
        }
    }

    public void register(Object o, Subscription subscription) {
        if (o == null) {
            return;
        }
        if (mSubscriptionMap == null) {
            mSubscriptionMap = new ConcurrentHashMap<>();
        }
        String key = o.getClass().getName();
        register(key, subscription);
    }

    public void unregister(Object object, Subscription subscription) {
        unregister(object.getClass().getName(), subscription);
    }

    public void unregister(String key, Subscription subscription) {
        if (mSubscriptionMap == null)
            return;
        CompositeSubscription compositeSubscription = mSubscriptionMap.get(key);
        if (compositeSubscription != null) {
            compositeSubscription.remove(subscription);
            PLog.i(TAG, "unregister [" + key + "]: success!");
        }
    }

    public void unregister(String key) {
        if (mSubscriptionMap == null) {
            return;
        }
        if (!mSubscriptionMap.containsKey(key)) {
            return;
        }
        if (mSubscriptionMap.get(key) != null) {
            mSubscriptionMap.get(key).unsubscribe();
        }
        mSubscriptionMap.remove(key);
        PLog.i(TAG, "unregister: [" + key + "]:unregister success");
    }

    public void unregister(Object o) {
        if (mSubscriptionMap == null) {
            return;
        }
        unregister(o.getClass().getName());
    }

    public static <T> Observable<T> wrapObservable(Observable<T> observable) {
        return wrapObservable(observable, AndroidSchedulers.mainThread(), Schedulers.io());
    }

    public static <T> Observable<T> wrapObservable(Observable<T> observable, Scheduler observerOn, Scheduler subscribeOn) {
        /**
         * 这里目前先采用onBackpressureBuffer 目前测试handle 1500条消息是没有问题，但是UI会强卡顿
         */
        observable = observable.onBackpressureBuffer()
                .observeOn(observerOn)
                .subscribeOn(subscribeOn);
        return observable;
    }

    public static <T> Subscription wrapObservableSafely(Observable<T> observable, Action1<T> onNext) {
        return wrapObservable(observable)
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        /**
                         * 这两个地方日志为强制打印，以便追寻RxJava的问题
                         */
                        Log.e(TAG, "onError: ", e);
                    }

                    @Override
                    public void onNext(T t) {
                        try {
                            onNext.call(t);
                        } catch (Throwable throwable) {
                            /**
                             * 这两个地方日志为强制打印，用于追寻RxJava的问题
                             */
                            Log.e(TAG, "wrapObservableSafely: onError", throwable);
                        }
                    }
                });
    }

}
