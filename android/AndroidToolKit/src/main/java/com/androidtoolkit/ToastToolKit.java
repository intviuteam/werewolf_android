package com.androidtoolkit;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/5/17 下午4:30
 * Mystery
 */

public class ToastToolKit {

    public static Context application;

    public static void bind(Context context) {
        application = context;
    }

    public static void showLong(String msg) {
        Toast.makeText(application, msg, Toast.LENGTH_LONG).show();
    }

    public static void showShort(String msg) {
        Toast.makeText(application, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showLong(int strId) {
        showLong(application.getString(strId));
    }

    public static void showShort(int strId) {
        showShort(application.getString(strId));
    }

    public static Toast build(ToastBuilder builder) {
        Toast mToastInstance = new Toast(application);
        mToastInstance.setView(LayoutInflater.from(application).inflate(builder.contentViewId, null));
        mToastInstance.setGravity(builder.gravity, ScreenUtils.dip2px(builder.xOffSet), ScreenUtils.dip2px(builder.yOffSet));
        mToastInstance.setDuration(builder.duration);
        return mToastInstance;
    }

    public static class ToastBuilder {
        private int contentViewId;
        private int gravity;
        private int xOffSet;
        private int yOffSet;
        private int duration;

        public void setContentViewId(int contentViewId) {
            this.contentViewId = contentViewId;
        }

        public void setGravity(int gravity) {
            this.gravity = gravity;
        }

        public void setxOffSet(int xOffSet) {
            this.xOffSet = xOffSet;
        }

        public void setyOffSet(int yOffSet) {
            this.yOffSet = yOffSet;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }
    }

}
