package com.androidtoolkit;

import android.content.Context;

/**
 * game_werewolf
 * 2017/11/23 下午2:45
 * Mystery
 */

public class Environment {

    private static Context mContext = null;


    public static void SetUpContext(Context context) {
        mContext = context;
        ApkToolKit.Bind(context);
    }

    public static Context GetAppContext() {
        if (mContext == null) {
            throw new IllegalStateException("Did you forget to SetUpContext ?");
        }
        return mContext;
    }
}
