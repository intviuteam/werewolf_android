package com.datasource.cache;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.androidtoolkit.StorageToolKit;
import com.androidtoolkit.transport.TransportCompat;
import com.jakewharton.disklrucache.DiskLruCache;
import com.androidtoolkit.PLog;
import com.toolkit.ToolKit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2017/11/7 下午4:31
 * Mystery
 * <p>
 * 用于做图片的存储管理
 */

public enum DiskLruCacheManager {

    aCache;
    private static final String TAG = "DiskLruCacheManager";
    private static final long MAX_CACHE_SIZE = 1 * 1024 * 1024 * 1024;

    /**
     * 礼物的图片缓存类型
     */
    public static final String GIFT_IMAGE_CACHE = "Gift";
    /**
     * 头像框图片缓存类型
     */
    public static final String HEAD_DECOR_CACHE = "HeadDecor";

    /**
     * 聊天框图片缓存
     */
    public static final String MSG_DECOR_CACHE = "MsgDecor";

    /**
     * 聊天框图片缓存
     */
    public static final String EMOTION_CACHE = "Emotion";

    private Map<String, DiskLruCache> mDiskLruCacheMap;
    private Map<String, String> mCacheDirMap;

    private DiskLruCacheManager() {
        mDiskLruCacheMap = new HashMap<>();
        mCacheDirMap = new HashMap<>();
    }

    public void createDiskLruCache(Context context, String cacheName) {
        if (TextUtils.isEmpty(cacheName)) {
            return;
        }
        DiskLruCache mDiskLruCache = null;
        try {
            File cacheDir = getDiskCacheDir(context, cacheName);
            if (!cacheDir.exists()) {
                cacheDir.mkdirs();
            }
            mDiskLruCache = DiskLruCache.open(cacheDir, 1, 1, MAX_CACHE_SIZE);
            mDiskLruCacheMap.put(cacheName, mDiskLruCache);
            PLog.i(TAG, "init [" + cacheName + "] Success !");
        } catch (Exception e) {
            PLog.i(TAG, "init [" + cacheName + "] Failed !");
            e.printStackTrace();
        }
    }

    public long getLengthFromCacheByUrl(String cacheType, String url) {
        if (TextUtils.isEmpty(url)) {
            return 0;
        }
        String key = ToolKit.GetHashKey(url);
        long result = 0;
        final DiskLruCache mCache = mDiskLruCacheMap.get(cacheType);
        try {
            DiskLruCache.Snapshot snapShot = mCache.get(key);
            if (snapShot != null) {
                result = snapShot.getLength(0);
                /**
                 * 拿到长度后 关闭
                 */
                try {
                    snapShot.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }

    public InputStream getInputStreamFromCacheByUrl(String cacheType, String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        String key = ToolKit.GetHashKey(url);
        final DiskLruCache mCache = mDiskLruCacheMap.get(cacheType);
        try {
            DiskLruCache.Snapshot snapShot = mCache.get(key);
            if (snapShot != null) {
                InputStream is = snapShot.getInputStream(0);
                return is;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean putImageUrlIntoCache(String cacheType, String url) {
        String key = ToolKit.GetHashKey(url);
        final DiskLruCache mCache = mDiskLruCacheMap.get(cacheType);
        try {
            DiskLruCache.Editor editor = mCache.edit(key);
            if (editor != null) {
                OutputStream outputStream = editor.newOutputStream(0);
                if (downloadUrlToStream(url, outputStream)) {
                    editor.commit();
                    PLog.i(TAG, "downloadUrl " + url + " finish");
                } else {
                    editor.abort();
                }
            }
            mCache.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean downloadUrlToStream(String urlString, OutputStream outputStream) {
        HttpURLConnection urlConnection = null;
        BufferedOutputStream out = null;
        BufferedInputStream in = null;
        try {
            final URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            TransportCompat.compatURLConnection(urlConnection);
            in = new BufferedInputStream(urlConnection.getInputStream(), 8 * 1024);
            out = new BufferedOutputStream(outputStream, 8 * 1024);
            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
            return true;
        } catch (final IOException e) {
            PLog.e(TAG,"FileNotFound: "+urlString);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath = context.getFilesDir().getPath();
        String cacheFilePath = cachePath + File.separator + uniqueName;
        mCacheDirMap.put(uniqueName, cacheFilePath);
        return new File(cacheFilePath);
    }

    public String getCacheDirPath(String cacheName) {
        return mCacheDirMap.get(cacheName);
    }


}
