package com.datasource;


import android.text.TextUtils;

import com.androidtoolkit.PLog;

public enum GlobalUserState {

    INSTANCE;

    private static final String TAG = "UserBehavior";

    private String curChatUserId = "";
    private boolean isGaming = false;

    private String userName;
    private String userId;
    private int userSex;
    private String userIcon;
    private int userExp;
    private String token;
    private String curGameType;
    private String curRoomId;
    private String userLocation = "";
    private String userUid = "";
    private int userLeveL = -1;
    private String curConversationId = "";
    private String password = "";
    private boolean actionBlock = false;

    public boolean isActionBlock() {
        return actionBlock;
    }

    public void setActionBlock(boolean actionBlock) {
        this.actionBlock = actionBlock;
    }

    public String getCurConversationId() {
        return curConversationId;
    }

    public void setCurConversationId(String curConversationId) {
        this.curConversationId = curConversationId;
    }

    public String getCurRoomId() {
        return curRoomId;
    }

    public void setCurRoomId(String curRoomId) {
        this.curRoomId = curRoomId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private boolean isTourist = false;

    private GlobalUserState() {

    }

    public String getCurGameType() {
        return curGameType;
    }

    public void setCurGameType(String curGameType) {
        if (TextUtils.isEmpty(curGameType)) {
            this.curGameType = "";
        } else {
            this.curGameType = curGameType;
        }
        PLog.i(TAG, "update cur Type " + this.curGameType);
    }

    public String getCurChatUserId() {
        return curChatUserId;
    }

    public void setCurChatUserId(String curChatUserId) {
        this.curChatUserId = curChatUserId;
    }

    public boolean isGaming() {
        return isGaming;
    }

    public void setGaming(boolean gaming) {
        isGaming = gaming;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserSex() {
        return userSex;
    }

    public void setUserSex(int userSex) {
        this.userSex = userSex;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public static GlobalUserState getGlobalState() {
        return INSTANCE;
    }

    public int getUserExp() {
        return userExp;
    }

    public void setUserExp(int userExp) {
        this.userExp = userExp;
    }

    public String getToken() {
        return token == null ? "" : token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isTourist() {
        return isTourist;
    }

    public void setTourist(boolean tourist) {
        isTourist = tourist;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public void setUserLeveL(int userLeveL) {
        this.userLeveL = userLeveL;
    }

    public int getUserLeveL() {
        return this.userLeveL;
    }
}
