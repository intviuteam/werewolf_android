# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt

#-dontobfuscate
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

#[For App]==================================================================================================
-keep class orangelab.project.common.model.**
-keep class orangelab.project.common.model.** {*;}
-keep class orangelab.project.common.event.**
-keep class orangelab.project.common.event.** {*;}
-keep class orangelab.project.common.db.**
-keep class orangelab.project.common.db.** {*;}
-keep class orangelab.project.common.bridge.**
-keep class orangelab.project.common.bridge.** {*;}
-keep class orangelab.project.common.dialog.**
-keep class orangelab.project.common.dialog.** {*;}
-keep class orangelab.project.common.view.**
-keep class orangelab.project.common.view.** {*;}
-keep class orangelab.project.common.activity.PersonalInfoActivity
-keep class orangelab.project.common.activity.PersonalInfoActivity {*;}
-keep class orangelab.project.common.activity.PersonalInfoActivity$*
-keep class * implements com.toolkit.action.Destroyable
-keep class * implements com.toolkit.action.Destroyable {*;}
-keep class * extends orangelab.project.common.component.BasicViewComponent
-keep class * extends orangelab.project.common.component.BasicViewComponent {*;}
-keep class * extends orangelab.project.common.dialog.SafeDialog
-keep class * extends orangelab.project.common.dialog.SafeDialog {*;}
-keep class orangelab.project.common.utils.** {*;}
-keep class orangelab.project.common.union.** {*;}

-dontwarn com.yanzhenjie.recyclerview.swipe.**
-dontwarn com.yanzhenjie.**
-keep class com.yanzhenjie.recyclerview.swipe.** {*;}
-keep class com.yanzhenjie.** {*;}

-keep class * implements com.toolkit.action.Keepable
-keep class * implements com.toolkit.action.Keepable {*;}

-keepclasseswithmembers class android.support.v7.widget.RecyclerView$ViewHolder {
   public final View *;
}

#[For QiNiu]=================================================================================================
-keep class com.qiniu.**{*;}
-keep class com.qiniu.**{public <init>();}
-ignorewarnings
#============================================================================================================
#[For Okio & http]===========================================================================================
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
#============================================================================================================

#[For Umeng]=================================================================================================
-dontshrink
-dontoptimize
-dontwarn com.google.android.maps.**
-dontwarn android.webkit.WebView
-dontwarn com.umeng.**
-dontwarn com.tencent.weibo.sdk.**
-keep public class javax.**
-keep public class android.webkit.**
-dontwarn android.support.v4.**
-keepattributes Exceptions,InnerClasses,Signature
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep class com.umeng.** {*;}
-keep public interface com.tencent.**
-keep public interface com.umeng.socialize.**
-keep public interface com.umeng.socialize.sensor.**
-keep public interface com.umeng.scrshot.**
-keep class com.android.dingtalk.share.ddsharemodule.** { *; }
-keep public class com.umeng.socialize.* {*;}
-keep class com.umeng.scrshot.**
-keep public class com.tencent.** {*;}
-keep class com.umeng.socialize.sensor.**
-keep class com.umeng.socialize.handler.**
-keep class com.umeng.socialize.handler.*
-keep class com.umeng.weixin.handler.**
-keep class com.umeng.weixin.handler.*
-keep class com.umeng.qq.handler.**
-keep class com.umeng.qq.handler.*
-keep class UMMoreHandler{*;}
-keep class com.tencent.mm.sdk.modelmsg.WXMediaMessage {*;}
-keep class com.tencent.mm.sdk.modelmsg.** implements   com.tencent.mm.sdk.modelmsg.WXMediaMessage$IMediaObject {*;}
-keep class im.yixin.sdk.api.YXMessage {*;}
-keep class im.yixin.sdk.api.** implements im.yixin.sdk.api.YXMessage$YXMessageData{*;}
-keep class com.tencent.mm.sdk.** {
 *;
}
-keep public class com.umeng.com.umeng.soexample.R$*{
public static final int *;
}
-keep public class com.linkedin.android.mobilesdk.R$*{
public static final int *;
    }
-keep class com.sina.** {*;}
-dontwarn com.sina.**
-keepattributes Signature
-dontwarn org.apache.http.**
#==============================================================================================================

#[For Google ]=================================================================================================
-dontwarn com.google.**
-keep class com.google.** { *;}
-keep class android.text {* ;}
-dontwarn android.text.*
-keep class android.**{*;}
-keepnames class * implements android.os.Parcelable {
public static final ** CREATOR;
}
-keepclassmembers enum * {
public static **[] values();
public static ** valueOf(java.lang.String);
}
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class com.android.vending.licensing.ILicensingService
#==============================================================================================================

#[For Alibaba]=================================================================================================
-keep class com.alibaba.fastjson.**{ *;}
-keep class  com.alipay.share.sdk.** {
   *;
}
-dontwarn com.alibaba.fastjson.**
-keep class com.linkedin.** { *; }
#==============================================================================================================

#[For Tencent]=================================================================================================
-keep class com.tencent.open.TDialog$*
-keep class com.tencent.open.TDialog$* {*;}
-keep class com.tencent.open.PKDialog
-keep class com.tencent.open.PKDialog {*;}
-keep class com.tencent.open.PKDialog$*
-keep class com.tencent.open.PKDialog$* {*;}
-keep class com.tencent.** {*;}
-dontwarn com.tencent.**
-keep class com.tencent.mm.sdk.** {
   *;
}
#==============================================================================================================

#[For Picasso]=================================================================================================
-keep class com.squareup.picasso.** {*;}
-dontwarn com.squareup.picasso.**
#==============================================================================================================

#[For Rx.... ]=================================================================================================
-dontwarn com.tbruyelle.**
-dontwarn java.lang.invoke.*
-keep class rx.internal.util.** {*;}
-dontwarn rx.internal.util.**
-keep class com.tbruyelle.** {*;}
-keep class de.greenrobot.** {*;}
#==============================================================================================================


#[For Avoscloud LeanCloud ]====================================================================================
-keep class orangelab.thirdparty.leancloud.** {*;}
-keep class com.avos.avoscloud.** {*;}
-keep class com.avos.avospush.notification.** { *; }
-keep class com.avos.** {*;}
-dontwarn com.avos.avospush.notification.**
#==============================================================================================================

#[For React Native & FaceBook]=================================================================================
-keep class com.facebook.** {*;}
-keep class com.facebook.ads.**{*;}
-dontwarn com.facebook.**
-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}
-keep @com.facebook.proguard.annotations.DoNotStrip class *
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
    @com.facebook.proguard.annotations.DoNotStrip *;
    @com.facebook.common.internal.DoNotStrip *;
}
-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
  void set*(***);
  *** get*();
}
-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
-keep class * extends com.facebook.react.bridge.NativeModule { *; }
-keepclassmembers,includedescriptorclasses class * { native <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }
-dontwarn com.facebook.react.**
#==============================================================================================================


#[Green Dao ]==================================================================================================
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties
# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**
#==============================================================================================================


#[For WebView JS]==============================================================================================
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}
#==============================================================================================================


#[For Orbit Sdk]===============================================================================================
-keep class cn.intviu.connect.model.**{*;}
-keep class cn.intviu.sdk.model.**{*;}
-keep class org.webrtc.**{*;}
-keep class org.appspot.apprtc.**{*;}
-keep class org.apache.http.**{*;}
-keep class de.tavendo.autobohn.**{*;}
-keep class cn.intviu.service.config.**{*;}
-keep class * extends cn.intviu.service.config.Config{public protected private *;}
-keep class * extends cn.intviu.sdk.BaseRequestExector{public protected *;}
#==============================================================================================================

#[For GifImageView]===============================================================================================
-keep class com.felipecsl.**{*;}
#==============================================================================================================

#[For Voice]==================================================================================================
-keep class orangelab.project.voice.activity.**
-keep class orangelab.project.voice.activity.** {*;}

-keep class orangelab.project.voice.adapter.**
-keep class orangelab.project.voice.adapter.** {*;}

-keep class orangelab.project.voice.basic.**
-keep class orangelab.project.voice.basic.** {*;}

-keep class orangelab.project.voice.cache.**
-keep class orangelab.project.voice.cache.** {*;}

-keep class orangelab.project.voice.component.**
-keep class orangelab.project.voice.component.** {*;}

-keep class orangelab.project.voice.config.**
-keep class orangelab.project.voice.config.** {*;}

-keep class orangelab.project.voice.dialog.**
-keep class orangelab.project.voice.dialog.** {*;}

-keep class orangelab.project.voice.ds.**
-keep class orangelab.project.voice.ds.** {*;}

-keep class orangelab.project.voice.element.**
-keep class orangelab.project.voice.element.** {*;}

-keep class orangelab.project.voice.gif.**
-keep class orangelab.project.voice.gif.** {*;}

-keep class orangelab.project.voice.gifimageview.**
-keep class orangelab.project.voice.gifimageview.** {*;}

-keep class orangelab.project.voice.manager.**
-keep class orangelab.project.voice.manager.** {*;}

-keep class orangelab.project.voice.model.**
-keep class orangelab.project.voice.model.** {*;}

-keep class orangelab.project.voice.share.**
-keep class orangelab.project.voice.share.** {*;}

-keep class orangelab.project.voice.utils.**
-keep class orangelab.project.voice.utils.** {*;}

-keep class orangelab.project.voice.view.**
-keep class orangelab.project.voice.view.** {*;}

-keep class orangelab.project.voice.observer.**
-keep class orangelab.project.voice.observer.** {*;}

-keep class orangelab.project.voice.interfaces.**
-keep class orangelab.project.voice.interfaces.** {*;}

-keep class orangelab.project.voice.data.**
-keep class orangelab.project.voice.data.** {*;}

-keep class * implements java.io.Serializable
-keep class * implements java.io.Serializable {*;}
#==============================================================================================================

#[For FloatWindow]==================================================================================================
-keep class orangelab.project.common.floatwindow.event.**
-keep class orangelab.project.common.floatwindow.event.** {*;}

-keep class orangelab.project.common.floatwindow.model.**
-keep class orangelab.project.common.floatwindow.model.** {*;}

-keep class orangelab.project.common.floatwindow.permission.**
-keep class orangelab.project.common.floatwindow.permission.** {*;}

-keep class orangelab.project.common.floatwindow.permission.rom.**
-keep class orangelab.project.common.floatwindow.permission.rom.** {*;}

-keep class orangelab.project.common.floatwindow.receiver.**
-keep class orangelab.project.common.floatwindow.receiver.** {*;}

-keep class orangelab.project.common.floatwindow.util.**
-keep class orangelab.project.common.floatwindow.util.** {*;}

-keep class orangelab.project.common.floatwindow.view.**
-keep class orangelab.project.common.floatwindow.view.** {*;}

-keep class orangelab.project.common.floatwindow.**
-keep class orangelab.project.common.floatwindow.** {*;}
#==============================================================================================================
