package orangelab.thirdparty.leancloud.chatkit.utils;


import com.tbruyelle.rxpermissions.RxPermissions;

/**
 * 系统名称:
 * 模块名称:
 * 功能说明:
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 */

public enum LCIMRxPermissionHelper {

    INSTANCE;

    private RxPermissions permissions;

    private LCIMRxPermissionHelper() {

    }

    public RxPermissions getPermissions() {
        return permissions;
    }

    public void setPermissions(RxPermissions permissions) {
        this.permissions = permissions;
    }

    public static LCIMRxPermissionHelper IMPL() {
        return INSTANCE;
    }
}
