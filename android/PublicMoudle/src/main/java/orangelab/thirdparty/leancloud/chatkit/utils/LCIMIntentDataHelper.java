package orangelab.thirdparty.leancloud.chatkit.utils;

import android.content.Intent;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;

/**
 * Author:1727
 * 2017/3/6
 */

public class LCIMIntentDataHelper {

    private static final String CHAT_LEFT_USER = "CHAT_LEFT_USER";
    private static final String CHAT_RIGHT_USER = "CHAT_RIGHT_USER";
    public static final String TYPE_ENTER_ROOM_FROM_RN = "TYPE_ENTER_ROOM_FROM_RN";
    public static final String TYPE_NOTIFACTION_FROM_INTVIU = "TYPE_NOTIFACTION_FROM_INTVIU";
    public static final String TYPE_EVENT_ROOM_FROM_CHAT = "TYPE_EVENT_ROOM_FROM_CHAT";
    private static final String PASSWORD = "password";
    public static final String INTENT_TYPE = "INTENT_TYPE";
    private static final String USER_ID = "USER_ID";
    private static final String GAME_TYPE = "GAME_TYPE";
    private static final String ROOM_ID = "ROOM_ID";
    private static final String IS_SINGLE = "IS_SINGLE";
    private static final String CHAT_CONFIG = "CHAT_CONFIG";

    public static void setChatConfig(Intent intent, ChatConfig chatConfig) {
        intent.putExtra(CHAT_CONFIG, chatConfig);
    }

    public static ChatConfig getChatConfig(Intent intent) {
        return intent.getParcelableExtra(CHAT_CONFIG);
    }

    public static void setRoomId(Intent intent, String roomId) {
        intent.putExtra(ROOM_ID, roomId);
    }

    public static String getRoomID(Intent intent) {
        return intent.getStringExtra(ROOM_ID);
    }

    public static UserData getChatLeftUserData(Intent intent) {
        return intent.getParcelableExtra(CHAT_LEFT_USER);
    }

    public static void setIsSingle(Intent intent, boolean isSingle) {
        intent.putExtra(IS_SINGLE, isSingle);
    }

    public static boolean IsSingle(Intent intent) {
        return intent.getBooleanExtra(IS_SINGLE, true);
    }

    public static String getUserID(Intent intent) {
        return intent.getStringExtra(USER_ID);
    }

    public static void setUserId(Intent intent, String userid) {
        intent.putExtra(USER_ID, userid);
    }

    public static void setChatLeftUser(Intent intent, UserData data) {
        intent.putExtra(CHAT_LEFT_USER, data);
    }

    public static void setChatRightUser(Intent intent, UserData data) {
        intent.putExtra(CHAT_RIGHT_USER, data);
    }

    public static UserData getChatRightUserData(Intent intent) {
        return intent.getParcelableExtra(CHAT_RIGHT_USER);
    }

    public static void setRoomPassword(Intent intent, String password) {
        intent.putExtra(PASSWORD, password);
    }

    public static String getRoomPassword(Intent intent) {
        return intent.getStringExtra(PASSWORD);
    }

    public static void setGameType(Intent intent, String gameType) {
        intent.putExtra(GAME_TYPE, gameType);
    }

    public static String getGameType(Intent intent) {
        return intent.getStringExtra(GAME_TYPE);
    }

    public static void setIntentType(Intent intent, String type) {
        intent.putExtra(INTENT_TYPE, type);
    }

    public static String getIntentType(Intent intent) {
        return intent.getStringExtra(INTENT_TYPE);
    }
}
