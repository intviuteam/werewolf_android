package orangelab.thirdparty.leancloud.chatkit.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.utils.MessageUtils;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;

import org.greenrobot.eventbus.EventBus;

import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import orangelab.thirdparty.leancloud.chatkit.event.ChatIntviuEvent;


/**
 * 系统名称:NotificationUtils
 * 模块名称:NotificationUtils
 * 功能说明:NotificationUtils
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 * 提示工具
 */

public enum NotificationUtils {

    INSTANCE;

    private static final String TAG = "NotificationUtils";

    private NotificationUtils() {
    }

    public static NotificationUtils IMPL() {
        return INSTANCE;
    }

    public void clearAllNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public void clearNotification(Context context, String userId) {
        int notificationId = userId.hashCode();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context
                .NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }

    public void onChatNotificationIntviu(
            String conversationId,
            String userId,
            String userName,
            int userSex,
            String userIvUrl,
            String message,
            String roomId,
            String roomType,
            String roomPassword,
            boolean isFamilyMsg
    ) {
        ChatIntviuEvent chatIntviuEvent = new ChatIntviuEvent();
        chatIntviuEvent.userId = userId;
        chatIntviuEvent.userName = userName;
        chatIntviuEvent.userSex = userSex;
        chatIntviuEvent.userIvUrl = userIvUrl;
        chatIntviuEvent.message = message;
        chatIntviuEvent.roomId = roomId;
        chatIntviuEvent.roomType = roomType;
        chatIntviuEvent.roomPassword = roomPassword;
        chatIntviuEvent.conversationId = conversationId;
        chatIntviuEvent.isFamilyMsg = isFamilyMsg;
        EventBus.getDefault().post(chatIntviuEvent);
    }

    public void onChatNotification(String conversationId, Context context, String userId, String userName, int userSex, String userIvUrl, String message, boolean isFamilyMsg) {
        if (TextUtils.equals(userId, GlobalUserState.getGlobalState().getCurChatUserId())) {
            return;
        }
        Intent startIntent = new Intent(context, LCIMConversationActivity.class);
        UserData leftUser = UserData.createUserData(userId, userName, userSex, userIvUrl);
        UserData rightUser =
                UserData.createUserData(
                        GlobalUserState.getGlobalState().getUserId(),
                        GlobalUserState.getGlobalState().getUserName(),
                        GlobalUserState.getGlobalState().getUserSex(),
                        GlobalUserState.getGlobalState().getUserIcon()
                );
        ChatConfig chatConfig = new ChatConfig();
        chatConfig.isSingleChat = !isFamilyMsg;
        chatConfig.leftUser = leftUser;
        chatConfig.rightUser = rightUser;
        chatConfig.conversationId = conversationId;
        chatConfig.peerId = leftUser.userId;
        LCIMIntentDataHelper.setChatConfig(startIntent, chatConfig);
        int notificationId = conversationId.hashCode();
        PendingIntent intent = PendingIntent.getActivity(context, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.notification_small_icon);
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.layout_notification_chat);
        if (isFamilyMsg) {
            contentView.setTextViewText(R.id.notification_title, ProjectConfig.APP_FAMILY_MSG);
        } else {
            contentView.setTextViewText(R.id.notification_title, ProjectConfig.APP_NAME);
        }
        contentView.setImageViewResource(R.id.notification_image, AppManager.GetAppIcon());
        contentView.setTextViewText(R.id.notification_text, "[" + userName + "]" + ":" + message);
        builder.setContent(contentView);
        builder.setContentIntent(intent);
        builder.setWhen(System.currentTimeMillis());
        Notification notification = builder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(notificationId, notification);
    }

}
