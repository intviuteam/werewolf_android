package orangelab.thirdparty.leancloud.chatkit.event;

/**
 * game_werewolf
 * 2018/3/1 下午3:40
 * Mystery
 */

public class LCIMSendTempTextMessageEvent {
    public String text;

    public LCIMSendTempTextMessageEvent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
