package orangelab.thirdparty.leancloud.chatkit.model;

import com.avos.avoscloud.im.v2.AVIMMessageCreator;
import com.avos.avoscloud.im.v2.AVIMMessageField;
import com.avos.avoscloud.im.v2.AVIMMessageType;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;

import java.util.Map;

/**
 * game_werewolf
 * 2017/8/3 上午11:55
 * Mystery
 */
@AVIMMessageType(
        type = 102
)
public class AVIMSystemMessage extends AVIMTextMessage {
    public static final int SystemMessageType = 102;
    @AVIMMessageField(
            name = "_lctext"
    )
    String text;
    @AVIMMessageField(
            name = "_lcattrs"
    )
    Map<String, Object> attrs;

    public static final Creator<AVIMSystemMessage> CREATOR = new AVIMMessageCreator(AVIMSystemMessage.class);

    public AVIMSystemMessage() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Object> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, Object> attr) {
        this.attrs = attr;
    }
}
