package orangelab.thirdparty.leancloud.chatkit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.File;

import com.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import com.androidtoolkit.view.ScreenUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMConstants;


/**
 * Created by wli on 16/2/29.
 * 图片详情页，聊天时点击图片则会跳转到此页面
 */
public class LCIMImageActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lcim_chat_image_brower_layout);
        imageView = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();
        String path = intent.getStringExtra(LCIMConstants.IMAGE_LOCAL_PATH);
        String url = intent.getStringExtra(LCIMConstants.IMAGE_URL);
        if (TextUtils.isEmpty(path)) {
            Picasso.get().load(url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                    .resize(
                            ScreenUtils.getScreenWidth(),
                            ScreenUtils.getScreenHeight()
                    ).centerInside().
                    into(imageView);
        } else {
            Picasso.get().load(new File(path))
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                    .resize(
                            ScreenUtils.getScreenWidth(),
                            ScreenUtils.getScreenHeight()
                    ).centerInside().into(imageView);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }
}
