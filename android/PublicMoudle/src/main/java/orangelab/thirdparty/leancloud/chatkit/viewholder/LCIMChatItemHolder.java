package orangelab.thirdparty.leancloud.chatkit.viewholder;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMLocationMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.R;
import com.androidtoolkit.PicassoUtils;
import com.networktoolkit.transport.Constant;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMGiftMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMIntviuMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMMiniGameMessage;

import org.greenrobot.eventbus.EventBus;

import orangelab.thirdparty.leancloud.chatkit.event.LCIMMessageResendEvent;
import orangelab.thirdparty.leancloud.chatkit.event.PersonalInfoEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMConstants;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * Created by wli on 15/9/17.
 * 聊天 item 的 holder
 */
public class LCIMChatItemHolder extends LCIMCommonViewHolder {

    protected boolean isLeft;

    protected AVIMMessage message;
    protected ImageView avatarView;
    protected ImageView sexView;
    protected TextView timeView;
    protected TextView nameView;
    protected LinearLayout conventLayout;
    protected FrameLayout statusLayout;
    protected ProgressBar progressBar;
    protected TextView statusView;
    protected ImageView errorView;
    private boolean isUnKnown = false;

    private UserData userData;
    private boolean isSingleChat = true;
    private ChatConfig mChatConfig;


    public LCIMChatItemHolder(ChatConfig chatConfig, Context context, ViewGroup root, boolean isLeft) {
        super(context, root, isLeft ? R.layout.lcim_chat_item_left_layout : R.layout.lcim_chat_item_right_layout);
        this.isLeft = isLeft;
        this.isSingleChat = chatConfig.isSingleChat;
        this.mChatConfig = chatConfig;
        if (isSingleChat) {
            if (isLeft) {
                userData = chatConfig.leftUser;
            } else {
                userData = chatConfig.rightUser;
            }
        } else {
            /**
             * 群聊
             */
        }

        initView();
    }

    public LCIMChatItemHolder(Context context, ViewGroup root) {
        super(context, root, R.layout.lcim_chat_item_unkown_layout);
        isUnKnown = true;
        timeView = (TextView) itemView.findViewById(R.id.chat_unkown_time);
    }

    public void initView() {
        if (isLeft) {
            avatarView = (ImageView) itemView.findViewById(R.id.chat_left_iv_avatar);
            timeView = (TextView) itemView.findViewById(R.id.chat_left_tv_time);
            nameView = (TextView) itemView.findViewById(R.id.chat_left_tv_name);
            conventLayout = (LinearLayout) itemView.findViewById(R.id.chat_left_layout_content);
            statusLayout = (FrameLayout) itemView.findViewById(R.id.chat_left_layout_status);
            statusView = (TextView) itemView.findViewById(R.id.chat_left_tv_status);
            progressBar = (ProgressBar) itemView.findViewById(R.id.chat_left_progressbar);
            errorView = (ImageView) itemView.findViewById(R.id.chat_left_tv_error);
            sexView = (ImageView) itemView.findViewById(R.id.chat_left_iv_sex);
        } else {
            avatarView = (ImageView) itemView.findViewById(R.id.chat_right_iv_avatar);
            timeView = (TextView) itemView.findViewById(R.id.chat_right_tv_time);
            nameView = (TextView) itemView.findViewById(R.id.chat_right_tv_name);
            conventLayout = (LinearLayout) itemView.findViewById(R.id.chat_right_layout_content);
            statusLayout = (FrameLayout) itemView.findViewById(R.id.chat_right_layout_status);
            progressBar = (ProgressBar) itemView.findViewById(R.id.chat_right_progressbar);
            errorView = (ImageView) itemView.findViewById(R.id.chat_right_tv_error);
            statusView = (TextView) itemView.findViewById(R.id.chat_right_tv_status);
            sexView = (ImageView) itemView.findViewById(R.id.chat_right_iv_sex);
        }
        setAvatarClickEvent();
        setResendClickEvent();
    }

    private void initUserData(String userName, String userImage, int userSex) {
        /**
         * 初始化用户姓名
         */
        if (TextUtils.isEmpty(userName)) {
            nameView.setText("");
        } else {
            nameView.setText(userName);
        }
        /**
         * 初始化用户头像
         */
        if (!TextUtils.isEmpty(userImage)) {
            PicassoUtils.loadImage(getContext(), userImage, avatarView, R.mipmap.default_head);
        }
        /**
         * 初始化性别
         */
        if (userSex == 1) {
            sexView.setImageResource(R.mipmap.ic_male);
        } else {
            sexView.setImageResource(R.mipmap.ic_female);
        }
    }

    @Override
    public void bindData(Object o) {
        message = (AVIMMessage) o;
        timeView.setText(millisecsToDateString(message.getTimestamp()));
        if (isUnKnown) {
            return;
        }
        String userName = "";
        int userSex = 1;
        String userImage = "";
        String userId = "";

        if (isSingleChat) {
            /**
             * 如果为强制锁定用户数据那么所有数据均采用UserData
             */
            userImage = userData.userImageUrl;
            userName = userData.userName;
            userSex = userData.userSex;
            userId = userData.userId;

            initUserData("", userImage, userSex);
        } else {
            /**
             * 如果不是强制锁定用户数据，那么就是根据信息集体数据而显示
             */
            if (message instanceof AVIMIntviuMessage) {
                AVIMIntviuMessage temp = (AVIMIntviuMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMTextMessage) {
                AVIMTextMessage temp = (AVIMTextMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMAudioMessage) {
                AVIMAudioMessage temp = (AVIMAudioMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMLocationMessage) {
                AVIMLocationMessage temp = (AVIMLocationMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMImageMessage) {
                AVIMImageMessage temp = (AVIMImageMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMGiftMessage) {
                AVIMGiftMessage temp = (AVIMGiftMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (message instanceof AVIMMiniGameMessage) {
                AVIMMiniGameMessage temp = (AVIMMiniGameMessage) message;
                try {
                    if (temp.getAttrs() != null) {
                        userId = (String) temp.getAttrs().get(Constant.USER_ID);
                        userName = (String) temp.getAttrs().get(Constant.USER_NAME);
                        userSex = Integer.parseInt((String) temp.getAttrs().get(Constant.USER_SEX));
                        userImage = (String) temp.getAttrs().get(Constant.USER_ICON);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            userData = UserData.createUserData(userId, userName, userSex, userImage);
            initUserData(userName, userImage, userSex);
        }


        switch (message.getMessageStatus()) {
            case AVIMMessageStatusFailed:
                statusLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                statusView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                break;
            case AVIMMessageStatusSent:
                statusLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                statusView.setVisibility(View.GONE);
                statusView.setVisibility(View.GONE);
                errorView.setVisibility(View.GONE);
                break;
            case AVIMMessageStatusSending:
                statusLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                statusView.setVisibility(View.GONE);
                errorView.setVisibility(View.GONE);
                break;
            case AVIMMessageStatusNone:
            case AVIMMessageStatusReceipt:
                statusLayout.setVisibility(View.GONE);
                break;
        }
    }

    public void setHolderOption(LCIMChatHolderOption option) {
        if (isUnKnown) {
            return;
        }
        if (null != option && !isLeft &&
                (AVIMMessage.AVIMMessageStatus.AVIMMessageStatusSent == message.getMessageStatus() ||
                        AVIMMessage.AVIMMessageStatus.AVIMMessageStatusReceipt == message.getMessageStatus())) {
            timeView.setVisibility(option.isShowTime() ? View.VISIBLE : View.GONE);
//            nameView.setVisibility(option.isShowName() ? View.VISIBLE : View.GONE);
            statusView.setVisibility(option.isShowDelivered() || option.isShowRead() ? View.GONE : View.GONE);
            statusLayout.setVisibility(option.isShowDelivered() || option.isShowRead() ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
            if (option.isShowRead()) {
                statusView.setText("已读");
            } else if (option.isShowDelivered()) {
                statusView.setText("已收到");
            }
        }
    }

    /**
     * 设置头像点击按钮的事件
     */
    private void setAvatarClickEvent() {
        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (mChatConfig.fromPersonalInfo()) {
                        return;
                    }
                    EventBus.getDefault().post(new PersonalInfoEvent(getContext(), userData.userId));
                } catch (ActivityNotFoundException exception) {
                    Log.i(LCIMConstants.LCIM_LOG_TAG, exception.toString());
                }
            }
        });
    }

    /**
     * 设置发送失败的叹号按钮的事件
     */
    private void setResendClickEvent() {
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LCIMMessageResendEvent event = new LCIMMessageResendEvent();
                event.message = message;
                EventBus.getDefault().post(event);
            }
        });
    }

    //TODO 展示更人性一点
    private static String millisecsToDateString(long timestamp) {
        SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
        return format.format(new Date(timestamp));
    }
}

