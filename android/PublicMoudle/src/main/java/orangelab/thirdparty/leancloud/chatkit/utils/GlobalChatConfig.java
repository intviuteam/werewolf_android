package orangelab.thirdparty.leancloud.chatkit.utils;

import com.R;

/**
 * game_werewolf
 * 2017/11/23 上午11:58
 * Mystery
 */

public class GlobalChatConfig {

    public static final int WEREWOLF_DEFAULT_USER_HEAD_FOR_CHAT = R.mipmap.lcim_default_head;
    public static final int WEREWOLF_DEFAULT_CONVERSATION_COLOR = R.color.leancloud_chat_title_bg_werewolf_color;
}
