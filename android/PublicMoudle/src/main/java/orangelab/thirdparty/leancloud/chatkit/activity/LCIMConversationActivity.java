package orangelab.thirdparty.leancloud.chatkit.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.avos.avoscloud.AVCallback;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

import orangelab.project.ProjectConfig;
import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.family.FamilyInfoActivity;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.tool.WrapImagePicker;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;

import com.R;
import com.google.gson.JsonSyntaxException;
import com.androidtoolkit.PLog;
import com.tbruyelle.rxpermissions.RxPermissions;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import orangelab.thirdparty.leancloud.chatkit.event.FromChatEvent;
import orangelab.thirdparty.leancloud.chatkit.event.IntviuRoomEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;
import orangelab.thirdparty.leancloud.chatkit.utils.GlobalChatConfig;

import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.utils.LCIMIntentDataHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMConversationUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMLogUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMRxPermissionHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

import org.greenrobot.eventbus.EventBus;

import orangelab.thirdparty.leancloud.chatkit.LCChatKit;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by wli on 16/2/29.
 * 会话详情页
 * 包含会话的创建以及拉取，具体的 UI 细节在 LCIMConversationFragment 中
 */
public class LCIMConversationActivity extends SafeActivity {

    private static final String TAG = "LCIMConversationActivit";

    protected LCIMConversationFragment conversationFragment;

    /**
     * Title部分
     */
    private ImageView mNotice;
    private TextView textTitle;
    private View titleBackGround;


    private ChatConfig mChatConfig;
    private View topContainer;

    private View userHeadContainer;
    private CircleImageView userHead;
    private ImageView userSex;
    private Button topContaienrAdd;
    private Button topContainerBlock;

    private boolean isSingleChat = true;//默认是单聊
    private String mFamilyId = "";

    private boolean isDestroy = false;

    private WrapImagePicker mWrapImagePicker = null;

    public static void Launch(Context c, String conversationId, String peerId, UserData left, UserData right, boolean isSingle, boolean needShowMinigame) {
        Intent intent = new Intent(c, LCIMConversationActivity.class);
        ChatConfig chatConfig = new ChatConfig();
        chatConfig.leftUser = left;
        chatConfig.rightUser = right;
        chatConfig.conversationId = conversationId;
        chatConfig.isSingleChat = isSingle;
        chatConfig.peerId = peerId;
        chatConfig.isNeedShowMiniGame = needShowMinigame;
        LCIMIntentDataHelper.setChatConfig(intent, chatConfig);
        c.startActivity(intent);
    }

    public static void Launch(Context c, String conversationId, String peerId, UserData left, UserData right, boolean isSingle) {
        Launch(c, conversationId, peerId, left, right, isSingle, false);
    }

    @Override
    protected void onActivityWindowInitFinish() {
        try {
            if (mChatConfig.isNeedShowMiniGame) {
                if (conversationFragment != null) {
                    conversationFragment.showMiniGamePopWindow();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void initTitleAndWindow() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(GlobalChatConfig.WEREWOLF_DEFAULT_CONVERSATION_COLOR));
            }
            textTitle = (TextView) findViewById(R.id.game_activity_title);

            titleBackGround = findViewById(R.id.lcim_chat_title);
            titleBackGround.setBackgroundColor(getResources().getColor(GlobalChatConfig.WEREWOLF_DEFAULT_CONVERSATION_COLOR));

            mNotice = (ImageView) findViewById(R.id.id_family_notice);
            mNotice.setOnClickListener(v -> {
                FamilyInfoActivity.Launch(this, mFamilyId);
            });
            mNotice.setVisibility(View.GONE);
            findViewById(R.id.left_room).setOnClickListener(v -> {
                if (conversationFragment != null) {
                    conversationFragment.sendTempMessage(MessageUtils.getString(R.string.has_leave));
                }
                finish();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChatConfig = LCIMIntentDataHelper.getChatConfig(getIntent());
        mWrapImagePicker = new WrapImagePicker();
        mWrapImagePicker.initImageConfig(500, 500);
        LCIMRxPermissionHelper.IMPL().setPermissions(new RxPermissions(this));
        setContentView(R.layout.lcim_conversation_activity);
        initTitleAndWindow();

        topContainer = findViewById(R.id.top_hint_container);
        topContaienrAdd = (Button) findViewById(R.id.user_add);
        topContainerBlock = (Button) findViewById(R.id.user_block);
        userHeadContainer = findViewById(R.id.user_head);
        userHead = (CircleImageView) findViewById(R.id.user_head_circle_image);
        userSex = (ImageView) findViewById(R.id.user_head_sex);
        dismissTopContainer();

        /**
         * 这里要区分一下是家族群聊还是单聊
         */
        isSingleChat = mChatConfig.isSingleChat;
        conversationFragment = (LCIMConversationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_chat);
        conversationFragment.setWrapImagePicker(mWrapImagePicker);
        GlobalUserState.getGlobalState().setCurConversationId(mChatConfig.conversationId);

        if (isSingleChat) {
            mNotice.setVisibility(View.GONE);
            /**
             * 是单聊
             */
            UserData leftUser = mChatConfig.leftUser;
            UserData rightUser = mChatConfig.rightUser;

            topContainer.setVisibility(View.GONE);
            textTitle.setText(leftUser.userName);
            GlobalUserState.getGlobalState().setCurChatUserId(leftUser.userId);
            /**
             * 不让用户看到客服的消息
             */
            try {
                if (!ProjectConfig.EXCLUDE_USERID.contains(leftUser.userId)) {
                    getUserInfo(leftUser.userId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (!TextUtils.equals(leftUser.userId, "5923d10843f4e7000773274b")) {
//                /**
//                 * 不让用户看到客服的消息
//                 */
//                getUserInfo(leftUser.userId);
//            }
        } else {
            /**
             * 群聊
             */
            textTitle.setText(R.string.string_family_conversation_title);
            getUserInfoForFamily();
        }
        initByIntent(getIntent());
        initListener();
        EventBus.getDefault().register(this);
    }


    private void initListener() {
        RxToolKit.Build(mActivityKey, FamilyEvent.QuitFamilyEvent.class).action(quitFamilyEvent -> {
            finish();
        }).register();
    }

    private void showTopContainer() {
        UserData leftUser = mChatConfig.leftUser;
        topContainer.setVisibility(View.VISIBLE);
        /**
         * 设置 头像
         */
        PicassoUtils.loadImage(this, leftUser.userImageUrl, userHead, GlobalChatConfig.WEREWOLF_DEFAULT_USER_HEAD_FOR_CHAT);
        /**
         * 设置 性别
         */
        setUserSex(leftUser.userSex);

        topContaienrAdd.setOnClickListener(v -> {
            ApiManager.AddFriendApi(
                    leftUser.userId,
                    value -> runOnUiThreadSafely(() -> {
                        dismissTopContainer();
                    })
                    , null, null
            );
        });
        topContainerBlock.setOnClickListener(v -> {
            blockFriend();
        });
        userHeadContainer.setOnClickListener(v -> {
            PersonalInfoActivity.Launch(this, leftUser.userId);
        });
    }

    public void setUserSex(int sex) {
        switch (sex) {
            case 1:
                userSex.setVisibility(View.VISIBLE);
                userSex.setImageResource(R.mipmap.lcim_ico_male);
                break;
            case -1:
                userSex.setVisibility(View.GONE);
                break;
            default:
                userSex.setVisibility(View.VISIBLE);
                userSex.setImageResource(R.mipmap.lcim_ico_female);
                break;
        }
    }

    private void blockFriend() {
        UserData leftUser = mChatConfig.leftUser;
        ApiManager.AddToBlackApi(leftUser.userId, (s, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.add_into_black_list_success)).run();
                    BlackListManager.Add(leftUser.userId);
                    UnifiedBridgeHelper.SendBlackListAddAction(leftUser.userId);
                    finish();
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void dismissTopContainer() {
        topContainer.setVisibility(View.GONE);
    }

    private void getUserInfoForFamily() {
        /**
         * 刷新一次自己的数据
         */
        ApiManager.GetUserInfoApi(GlobalUserState.getGlobalState().getUserId(), (userInfoResult, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    if (userInfoResult.group != null) {
                        FamilyMemo.UpdateMemo(userInfoResult.group);
                        if (mNotice.getVisibility() == View.VISIBLE) {
                        } else {
                            mNotice.setVisibility(View.VISIBLE);
                            mNotice.setImageResource(R.mipmap.ico_family_members_notice);
                        }
                    }
                }
            });
        });
    }

    private void getUserInfo(String userId) {
        ApiManager.GetUserInfoApi(userId, (userInfoResult, e) -> runOnUiThreadSafely(() -> {
            try {
                if (e == null) {
                    if (userInfoResult.is_friend) {
                        dismissTopContainer();
                    } else {
                        showTopContainer();
                    }
                }
            } catch (JsonSyntaxException e2) {
                e2.printStackTrace();
            }
        }));
    }

    private void release() {
        if (!isDestroy) {
            GlobalUserState.getGlobalState().setCurChatUserId("");
            GlobalUserState.getGlobalState().setCurConversationId("");

            if (mChatConfig.isSingleChat) {
                WritableMap map = Arguments.createMap();
                map.putString(Constant.USER_ID, mChatConfig.leftUser.userId);
                map.putString(Constant.CONVERSATION_ID, mChatConfig.conversationId);
                map.putString(Constant.CONVERSATION_TYPE, Constant.MSG_TYPE_SINGLE);
                RoomSocketEngineHelper.sendEventToRn(com.networktoolkit.transport.Constant.EVENT_LEAVE_CHAT, map);
            } else {
                WritableMap map = Arguments.createMap();
                map.putString(Constant.CONVERSATION_ID, mChatConfig.conversationId);
                map.putString(Constant.CONVERSATION_TYPE, Constant.MSG_TYPE_FAMILY);
                RoomSocketEngineHelper.sendEventToRn(com.networktoolkit.transport.Constant.EVENT_LEAVE_CHAT, map);
            }

            /**
             * 发送取消小游戏的邀请
             */
            GlobalSocketEngine.INSTANCE.sendInviteCancel();
            /**
             * 发送取消私密房的邀请
             */
            GlobalSocketEngine.INSTANCE.sendCancelPrivateRoomInvite();

            RxToolKit.UnRegister(mActivityKey);
            EventBus.getDefault().unregister(this);

            isDestroy = true;
            LCIMRxPermissionHelper.IMPL().setPermissions(null);
            if (conversationFragment != null) {
                conversationFragment = null;
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        release();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initByIntent(intent);
    }

    private void initByIntent(Intent intent) {
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken())) {
            showToast("请先登录终极狼人杀");
            finish();
            return;
        }
        if (null == LCChatKit.getInstance().getClient()) {
            showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
            finish();
            return;
        }

        if (mChatConfig.isSingleChat) {
            /**
             * 单聊这里要判断是否在黑名单中
             */
            Utils.runSafely(() -> {
                if (BlackListManager.Has(mChatConfig.leftUser.userId)) {
                    showToast(MessageUtils.getString(R.string.user_in_black_list));
                    finish();
                    return;
                }
            });
        }
        Bundle extras = intent.getExtras();
        if (null != extras) {

            if (!TextUtils.isEmpty(mChatConfig.conversationId)) {
                /**
                 * 根据会话ID进入聊天室
                 */
                String conversationId = mChatConfig.conversationId;
                if (TextUtils.isEmpty(conversationId)) {
                    /**
                     * do nothing
                     */
                } else {
                    getConversationByConvId(conversationId);
                    return;
                }
            }

            if (!isSingleChat) {
                /**
                 * 对于群聊不进行下面的进入方式,如果上面的没有执行那么直接提示错误
                 */
                showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                finish();
                return;
            }

            if (!TextUtils.isEmpty(mChatConfig.peerId)) {
                /**
                 * 根据memberId进入聊天室
                 */
                String peerId = mChatConfig.peerId;
                getConversationByPeerId(peerId);
            } else {
                showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                finish();
            }
        }
    }

    private void getConversationByConvId(String mConversationId) {
        LCChatKit.getInstance().getClient().getConversationsQuery().whereEqualTo("objectId", mConversationId)
                .findInBackground(new AVIMConversationQueryCallback() {
                    @Override
                    public void done(List<AVIMConversation> list, AVIMException e) {
                        runOnUiThreadSafely(() -> {
                            if (e == null) {
                                if (list != null && !list.isEmpty()) {
                                    AVIMConversation conversation = list.get(0);
                                    if (conversation == null) {
                                        showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                                        finish();
                                        return;
                                    }
                                    updateConversation(conversation);
                                } else {
                                    showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                                    finish();
                                    return;
                                }
                            } else {
                                /**
                                 * 这里要把日志打印出来
                                 */
                                e.printStackTrace();
                                showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                                finish();
                                return;
                            }
                        });
                    }
                });
    }

    private void getConversationByPeerId(String mPeerId) {
        getConversation(mPeerId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            /**
             * 家族情况下 刷一下数据
             */
            if (!isSingleChat) {
                FamilyApiManager.GetFamilyInfoByConversationId(mChatConfig.conversationId, (familyInfoApiResult, e) -> {
                    runOnUiThreadSafely(() -> {
                        if (e == null) {
                            mFamilyId = familyInfoApiResult.group._id;
                            try {
                                if (familyInfoApiResult.group.require_count > 0) {
                                    mNotice.setImageResource(R.mipmap.ico_family_members_notice_with_red);
                                } else {
                                    mNotice.setImageResource(R.mipmap.ico_family_members_notice);
                                }
                            } catch (Exception e1) {
                            }
                            mNotice.setVisibility(View.VISIBLE);
                            FamilyMemo.UpdateMemo(familyInfoApiResult);
                        }
                    });
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        finish();
    }

    /**
     * 设置 actionBar title 以及 up 按钮事件
     *
     * @param title
     */
    protected void initActionBar(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            if (null != title) {
                actionBar.setTitle(title);
            }
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            finishActivity(RESULT_OK);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        /**
         * 不再响应返回按钮
         */
        finish();
        return;
    }

    /**
     * 主动刷新 UI
     *
     * @param conversation
     */
    protected void updateConversation(AVIMConversation conversation) {
        if (null != conversation) {
            conversationFragment.setConversation(conversation);
//            LCIMConversationItemCache.create().insertConversation(conversation.getConversationId());
            LCIMConversationUtils.getConversationName(conversation, new AVCallback<String>() {
                @Override
                protected void internalDone0(String s, AVException e) {
                    if (null != e) {
                        LCIMLogUtils.logException(e);
                    } else {
                        initActionBar(s);
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(IntviuRoomEvent event) {
        if (event != null) {
            FromChatEvent fromChatEvent = new FromChatEvent();
            fromChatEvent.activity = this;
            fromChatEvent.event = event;
            EventBus.getDefault().post(fromChatEvent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mWrapImagePicker.onActivityResult(this, requestCode, resultCode, data);
    }

    /**
     * 获取 conversation
     * 为了避免重复的创建，createConversation 参数 isUnique 设为 true·
     */
    protected void getConversation(String memberId) {
        if (LCChatKit.getInstance().getClient() == null) {
            showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
            finish();
        } else {
            LCChatKit.getInstance().getClient().createConversation(
                    Arrays.asList(memberId), "", null, false, true, new AVIMConversationCreatedCallback() {
                        @Override
                        public void done(AVIMConversation avimConversation, AVIMException e) {
                            runOnUiThreadSafely(() -> {
                                if (null != e) {
                                    e.printStackTrace();
                                    showToast(MessageUtils.getString(R.string.str_leancloud_action_failed));
                                    finish();
                                } else {
                                    PLog.i(TAG, "Create AVIMConversation Id = " + avimConversation.getConversationId());
                                    updateConversation(avimConversation);
                                }
                            });
                        }
                    });
        }
    }
}