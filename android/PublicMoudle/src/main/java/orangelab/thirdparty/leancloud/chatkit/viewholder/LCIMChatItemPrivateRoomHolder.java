package orangelab.thirdparty.leancloud.chatkit.viewholder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.datasource.GlobalUserState;

import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.minigame.model.MiniGameItem;
import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMPrivateRoomMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;

/**
 * game_werewolf
 * 2018/4/16 上午10:29
 * Mystery
 */

public class LCIMChatItemPrivateRoomHolder extends LCIMChatItemHolder {

    private final ChatConfig mChatConfig;

    private View mContainer;

    private TextView mCountDown;
    private CountDownTaskManager.CountDownTask mCountDownTask;

    private View mRefuse;
    private ImageView mImageState;
    private View mBg;

    public LCIMChatItemPrivateRoomHolder(ChatConfig chatConfig, Context context, ViewGroup root, boolean isLeft) {
        super(chatConfig, context, root, isLeft);
        this.mChatConfig = chatConfig;
    }

    @Override
    public void initView() {
        super.initView();
        mContainer = View.inflate(getContext(), R.layout.lcim_chat_private_room_layout, null);
        conventLayout.addView(mContainer, new ViewGroup.LayoutParams(ScreenUtils.dip2px(180), ScreenUtils.dip2px(180)));
        mImageState = (ImageView) mContainer.findViewById(R.id.id_private_room_state);
        mCountDown = (TextView) mContainer.findViewById(R.id.id_private_room_countdown);
        mRefuse = mContainer.findViewById(R.id.id_private_room_cancel);
        mBg = mContainer.findViewById(R.id.id_private_room_bg);
    }

    private void turnIntoOver() {
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
        mCountDown.setText(MessageUtils.getString(R.string.mini_game_intviu_over));
        mBg.setBackgroundResource(R.mipmap.lcim_private_room_intviu_over_bg);
        mCountDown.setTextColor(mCountDown.getResources().getColor(R.color.color_mini_game_over));
        mCountDown.setBackgroundResource(android.R.color.white);
        mImageState.setBackgroundResource(R.color.color_mini_game_fight_over_bg);
        mImageState.setImageResource(R.mipmap.lcim_mini_game_fight_over);
        mRefuse.setVisibility(View.GONE);
    }

    private void turnIntoCountDown(int countDownSecond, int countDownTime) {
        mContainer.setClickable(false);
        mBg.setBackgroundResource(R.mipmap.lcim_private_room_intviu_bg);
        mImageState.setBackgroundResource(R.color.color_mini_game_fight_bg);
        mImageState.setImageResource(R.mipmap.lcim_private_room_intviu);
        if (countDownSecond >= countDownTime) {
            countDownSecond = countDownTime;
        }
        mCountDown.setVisibility(View.VISIBLE);
        mCountDownTask = new CountDownTaskManager.CountDownTask(countDownSecond, new CountDownTaskManager.CountDownTaskListener() {

            @Override
            public void onTick(int seconds) {
                mCountDown.setText(MessageUtils.getString(R.string.amusement_matching_counting, Integer.toString(seconds)));
                mCountDown.setBackgroundResource(R.drawable.drawable_mini_game_countdown_bg);
                mCountDown.setTextColor(mCountDown.getResources().getColor(android.R.color.white));
            }

            @Override
            public void onStart(int startSeconds) {

            }

            @Override
            public void onFinish() {
                turnIntoOver();
            }
        });
        mCountDownTask.startTask();
    }

    @Override
    public void bindData(Object o) {
        super.bindData(o);
        AVIMMessage message = (AVIMMessage) o;
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
        try {
            if (message instanceof AVIMPrivateRoomMessage) {
                final AVIMPrivateRoomMessage privateMessage = (AVIMPrivateRoomMessage) message;
                String userId = (String) privateMessage.getAttrs().get(Constant.USER_ID);

                long chatTime = 0;
                int countDownTime = 0;
                try {
                    chatTime = (long) privateMessage.getAttrs().get(Constant.CHAT_TIME);
                    countDownTime = (int) privateMessage.getAttrs().get(Constant.PRIVACY_COUNTDOWN_TIME);
                } catch (Exception e) {
                    e.printStackTrace();
                    //时间有问题直接成为结束
                    turnIntoOver();
                    return;
                }

                long curTime = GlobalSocketEngine.INSTANCE.getServerCurTime();
                int second = (int) (((chatTime + countDownTime * 1000) - curTime) / 1000);

                if (second <= 0) {
                    /**
                     * 已经过期了
                     */
                    turnIntoOver();
                } else {
                    /**
                     * 开始倒计时
                     */
                    turnIntoCountDown(second, countDownTime);
                    if (TextUtils.equals(userId, GlobalUserState.getGlobalState().getUserId())) {
                        mRefuse.setVisibility(View.GONE);
                        mContainer.setClickable(false);
                    } else {
                        mRefuse.setVisibility(View.VISIBLE);
                        mContainer.setOnClickListener(v -> {
                            if (GlobalUserState.getGlobalState().isGaming()) {
                                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_enter_error));
                                return;
                            } else {
                                try {
                                    GlobalSocketEngine.INSTANCE.sendAcceptPrivateRoomIntviu(mChatConfig.leftUser.userId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    /**
                                     * 代码保护
                                     */
                                }
                            }
                        });
                    }
                }
                mRefuse.setOnClickListener(v -> {
                    GlobalSocketEngine.INSTANCE.sendPrivateRoomRefuseInvite(mChatConfig.leftUser.userId);
                    //ToastToolKit.showLong(R.string.str_private_room_refused);
                    /**
                     * 已经过期了
                     */
//                    turnIntoOver();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void destroy() {
        super.destroy();
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
    }
}
