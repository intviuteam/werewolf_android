package orangelab.thirdparty.leancloud.chatkit.event;

/**
 * game_werewolf
 * 2017/5/17 下午5:56
 * Mystery
 */

public class IntviuRoomEvent {

    private String roomId;
    private String roomPassword;
    private String roomGameType;

    public IntviuRoomEvent(String roomId, String roomPassword, String roomGameType) {
        this.roomId = roomId;
        this.roomPassword = roomPassword;
        this.roomGameType = roomGameType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomPassword() {
        return roomPassword;
    }

    public void setRoomPassword(String roomPassword) {
        this.roomPassword = roomPassword;
    }

    public String getRoomGameType() {
        return roomGameType;
    }

    public void setRoomGameType(String roomGameType) {
        this.roomGameType = roomGameType;
    }
}
