package orangelab.thirdparty.leancloud.chatkit.viewholder;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMMessage;
import com.R;

import orangelab.thirdparty.leancloud.chatkit.model.AVIMTempTextMessage;

/**
 * game_werewolf
 * 2018/3/1 下午4:47
 * Mystery
 */

public class LCIMChatTempTextItemHolder extends LCIMCommonViewHolder {

    private TextView mTextView;

    public LCIMChatTempTextItemHolder(Context context, ViewGroup root) {
        super(context, root, R.layout.lcim_chat_item_temp_message_layout);
        mTextView = (TextView) itemView.findViewById(R.id.id_text);
    }

    @Override
    public void bindData(Object o) {
        AVIMMessage message = (AVIMMessage) o;
        if (message instanceof AVIMTempTextMessage) {
            AVIMTempTextMessage tempMessage = (AVIMTempTextMessage) message;
            mTextView.setText(tempMessage.getText());
        }
    }
}
