package orangelab.thirdparty.leancloud.chatkit.viewholder;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.R;

import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.utils.MessageUtils;

import java.util.Map;

import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.game.config.WereWolfConfig;
import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.event.IntviuRoomEvent;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMGiftMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMIntviuMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;

import com.datasource.GlobalUserState;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by wli on 15/9/17.
 * 聊天页面中的文本 item 对应的 holder
 */
public class LCIMChatItemTextHolder extends LCIMChatItemHolder {

    protected TextView contentView;

    private View inviteContainer;
    private TextView inviteRoomId;
    private TextView inviteRoomPassword;

    private ImageView ivIcon;

    private TextView tvTitle;

    private View giftContainer;
    private ImageView giftIv;
    private TextView giftName;
    private TextView giftPopular;
    private TextView giftRebate;
    private View mContentView;//整体布局

    private boolean isUnKnown = false;

    public LCIMChatItemTextHolder(ChatConfig chatConfig, Context context, ViewGroup root, boolean isLeft) {
        super(chatConfig, context, root, isLeft);
    }

    public LCIMChatItemTextHolder(Context context, ViewGroup root) {
        super(context, root);
        isUnKnown = true;
    }

    @Override
    public void initView() {
        super.initView();
        if (isLeft) {
            mContentView = View.inflate(getContext(), R.layout.lcim_chat_item_left_text_layout, null);
            conventLayout.addView(mContentView);
            contentView = (TextView) itemView.findViewById(R.id.chat_left_text_tv_content);
            inviteContainer = itemView.findViewById(R.id.chat_left_text_invite);
            inviteRoomId = (TextView) itemView.findViewById(R.id.chat_left_text_invite_roomid);
            inviteRoomPassword = (TextView) itemView.findViewById(R.id.chat_left_text_invite_roompw);
            giftContainer = itemView.findViewById(R.id.chat_left_gift_container);
            giftIv = (ImageView) itemView.findViewById(R.id.chat_left_gift_iv);
            giftName = (TextView) itemView.findViewById(R.id.chat_left_gift_text);
            ivIcon = (ImageView) itemView.findViewById(R.id.chat_left_text_invite_iv);
            tvTitle = (TextView) itemView.findViewById(R.id.chat_left_text_invite_title);
            giftPopular = (TextView) itemView.findViewById(R.id.chat_left_gift_popular);
            giftRebate = (TextView) itemView.findViewById(R.id.chat_left_gift_rebate);
        } else {
            mContentView = View.inflate(getContext(), R.layout.lcim_chat_item_right_text_layout, null);
            conventLayout.addView(mContentView);
            contentView = (TextView) itemView.findViewById(R.id.chat_right_text_tv_content);
            inviteContainer = itemView.findViewById(R.id.chat_right_text_invite);
            inviteRoomId = (TextView) itemView.findViewById(R.id.chat_right_text_invite_roomid);
            inviteRoomPassword = (TextView) itemView.findViewById(R.id.chat_right_text_invite_roompw);
            giftContainer = itemView.findViewById(R.id.chat_right_gift_container);
            giftIv = (ImageView) itemView.findViewById(R.id.chat_right_gift_iv);
            giftName = (TextView) itemView.findViewById(R.id.chat_right_gift_text);
            ivIcon = (ImageView) itemView.findViewById(R.id.chat_right_text_invite_iv);
            tvTitle = (TextView) itemView.findViewById(R.id.chat_right_text_invite_title);
            giftPopular = (TextView) itemView.findViewById(R.id.chat_right_gift_popular);
            giftRebate = (TextView) itemView.findViewById(R.id.chat_right_gift_rebate);
        }
    }

    @Override
    public void bindData(Object o) {
        super.bindData(o);
        if (isUnKnown) {
            return;
        }

        if (isLeft) {
            mContentView.setBackgroundResource(R.mipmap.chat_left_text_bg);
        } else {
            mContentView.setBackgroundResource(R.mipmap.chat_right_text_bg);
        }

        try {
            AVIMMessage message = (AVIMMessage) o;
            if (message instanceof AVIMTextMessage) {
                inviteContainer.setVisibility(View.GONE);
                contentView.setVisibility(View.VISIBLE);
                giftContainer.setVisibility(View.GONE);
                AVIMTextMessage textMessage = (AVIMTextMessage) message;
                contentView.setText(textMessage.getText());
            } else if (message instanceof AVIMIntviuMessage) {
                AVIMIntviuMessage textMessage = (AVIMIntviuMessage) message;
                contentView.setText(textMessage.getText());
                final Map<String, Object> map = textMessage.getAttrs();
                if (map != null) {
                    if (map.containsKey(Constant.ROOM_ID)) {
                        inviteContainer.setVisibility(View.VISIBLE);
                        contentView.setVisibility(View.GONE);
                        giftContainer.setVisibility(View.GONE);
                        try {
                            Log.i("LeanCloudChatHelper", "bindData: " + map.get(Constant.ROOM_ID));
                            contentView.setVisibility(View.GONE);
                            inviteContainer.setVisibility(View.VISIBLE);

                            inviteRoomId.setText(MessageUtils.getString(R.string.str_room_id) + map.get(Constant.ROOM_ID));
                            inviteRoomId.setVisibility(View.VISIBLE);

                            if (map.containsKey(Constant.ROOM_PASSWORD)) {
                                Log.i("LeanCloudChatHelper", "bindData: " + map.get(Constant.ROOM_PASSWORD));
                                if (TextUtils.isEmpty(map.get(Constant.ROOM_PASSWORD).toString())) {
                                    inviteRoomPassword.setVisibility(View.GONE);
                                } else {
                                    inviteRoomPassword.setVisibility(View.VISIBLE);
                                }
                                inviteRoomPassword.setText(MessageUtils.getString(R.string.str_room_psw) + map.get(Constant.ROOM_PASSWORD));
                            } else {
                                inviteRoomPassword.setVisibility(View.GONE);
                            }
                            if (map.containsKey(Constant.GAME_TYPE)) {
                                String gameType = map.get(Constant.GAME_TYPE).toString();
                                Log.i("LeanCloudChatHelper", "bindData: " + gameType);
                                if (TextUtils.equals(gameType, com.networktoolkit.transport.Constant.GAME_TYPE_AUDIO)) {
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.voice_share_left_bob);
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.voice_share_right_bob);
                                    }

                                    String type = "";
                                    Object typeObj = map.get(com.networktoolkit.transport.Constant.CHILD_TYPE);
                                    if (typeObj != null) {
                                        try {
                                            type = typeObj.toString();
                                        } catch (Exception e) {
                                            Log.i("LeanCloudChatHelper", "error: " + e.getMessage());
                                        }
                                    }

                                    tvTitle.setText(MessageUtils.getString(R.string.str_voice_invite_title, type));
                                    tvTitle.setTextColor(getContext().getResources().getColor(R.color.voice_share_pur));
                                    inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.voice_share_gray));
                                    inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.voice_share_gray));
                                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                            ScreenUtils.dip2px(50),
                                            ScreenUtils.dip2px(50)
                                    );
                                    ivIcon.setLayoutParams(params);
                                    ivIcon.setImageResource(R.mipmap.share_voice_logo);

                                } else if (TextUtils.equals(gameType, com.networktoolkit.transport.Constant.GAME_TYPE_PRIVACY)) {
                                    /**
                                     * 私密房的分享
                                     */
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.voice_share_left_bob);
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.voice_share_right_bob);
                                    }
                                    tvTitle.setText(MessageUtils.getString(R.string.str_private_room_chat_intviu_title));
                                    tvTitle.setTextColor(getContext().getResources().getColor(R.color.voice_share_pur));
                                    inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.voice_share_gray));
                                    inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.voice_share_gray));

                                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                            ScreenUtils.dip2px(50),
                                            ScreenUtils.dip2px(50)
                                    );

                                    ivIcon.setLayoutParams(params);
                                    ivIcon.setImageResource(R.mipmap.share_voice_logo);
                                } else if (WereWolfConfig.isWereWolfGame(gameType)) {
                                    //还原布局，防止下次使用上次的布局
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.chat_left_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.chat_right_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                    }
                                    tvTitle.setText(R.string.str_game_invite);
                                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                            ScreenUtils.dip2px(45),
                                            ScreenUtils.dip2px(35)
                                    );
                                    ivIcon.setLayoutParams(params);
                                    ivIcon.setImageResource(R.mipmap.chat_invite);
                                } else if (TextUtils.equals(gameType, com.networktoolkit.transport.Constant.GAME_TYPE_SPY)) {
                                    //谁是卧底二期的分享,布局同狼人杀
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.chat_left_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.chat_right_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                    }
                                    tvTitle.setText(R.string.str_spy_room_tag);
                                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                            ScreenUtils.dip2px(45),
                                            ScreenUtils.dip2px(35)
                                    );
                                    ivIcon.setLayoutParams(params);
                                    ivIcon.setImageResource(R.mipmap.chat_invite);
                                } else if (TextUtils.equals(gameType, com.networktoolkit.transport.Constant.GAME_TYPE_PAIR)) {
                                    //大厅的分享,布局同狼人杀
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.chat_left_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.chat_right_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                    }
                                    tvTitle.setText(R.string.str_lobby_share_chat_title);
                                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                            ScreenUtils.dip2px(45),
                                            ScreenUtils.dip2px(35)
                                    );
                                    ivIcon.setLayoutParams(params);
                                    ivIcon.setImageResource(R.mipmap.chat_invite);
                                } else {
                                    /**
                                     * 不支持的类型
                                     */
                                    if (isLeft) {
                                        mContentView.setBackgroundResource(R.mipmap.chat_left_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_left));
                                    } else {
                                        mContentView.setBackgroundResource(R.mipmap.chat_right_text_bg);
                                        tvTitle.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomId.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                        inviteRoomPassword.setTextColor(getContext().getResources().getColor(R.color.color_lcim_right));
                                    }
                                    tvTitle.setText(R.string.avoscloud_unkown_message);
                                    inviteRoomId.setVisibility(View.GONE);
                                    inviteRoomPassword.setVisibility(View.GONE);
                                    return;
                                }
                            } else {
                                /**
                                 * 数据不全 则结束
                                 */
                                return;
                            }
                            inviteContainer.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    /**
                                     * ......
                                     */
                                    if (GlobalUserState.getGlobalState().isGaming()) {
                                        /**
                                         * 游戏中，不响应
                                         */
                                        ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_enter_error));
                                        return;
                                    } else {
                                        try {
                                            if (map.containsKey(Constant.ROOM_PASSWORD)) {
                                                EventBus.getDefault().post(new IntviuRoomEvent(
                                                        map.get(Constant.ROOM_ID).toString(),
                                                        map.get(Constant.ROOM_PASSWORD).toString(),
                                                        map.get(Constant.GAME_TYPE).toString()
                                                ));
                                            } else {
                                                EventBus.getDefault().post(new IntviuRoomEvent(
                                                        map.get(Constant.ROOM_ID).toString(),
                                                        "",
                                                        map.get(Constant.GAME_TYPE).toString()
                                                ));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            /**
                                             * just in case
                                             */
                                        }
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            contentView.setVisibility(View.VISIBLE);
                            inviteContainer.setVisibility(View.GONE);
                        }
                    } else {
                        contentView.setVisibility(View.VISIBLE);
                        inviteContainer.setVisibility(View.GONE);
                    }
                }
            } else if (message instanceof AVIMGiftMessage) {
                AVIMGiftMessage giftMessage = (AVIMGiftMessage) message;
                inviteContainer.setVisibility(View.GONE);
                contentView.setVisibility(View.GONE);
                giftContainer.setVisibility(View.VISIBLE);
                final Map<String, Object> map = giftMessage.getAttrs();
                if (map != null && map.containsKey(Constant.GIFT_TYPE)) {
                    String giftType = (String) map.get(Constant.GIFT_TYPE);
                    EffectsManager.FillGiftImageView(giftType, giftIv);

                    giftName.setText(EffectsManager.GetSupportedTypeName(giftType));

                    if (!EffectsManager.IsCardByGiftType(giftType)) {
                        try {
                            /**
                             * 填充人气 和 返现
                             */
                            String rebate = (String) map.get(Constant.GIFT_REBATE);
                            if (!TextUtils.isEmpty(rebate)) {
                                this.giftRebate.setVisibility(View.VISIBLE);
                                this.giftRebate.setText(giftRebate.getContext().getString(R.string.gift_rebate_text, rebate));
                            }
                            int popular = EffectsManager.GetGiftPopular(giftType);
                            String popularString = null;
                            if (popular >= 0) {
                                popularString = "+" + Integer.toString(Math.abs(popular));
                            } else {
                                popularString = "-" + Integer.toString(Math.abs(popular));
                            }
                            giftPopular.setText(giftPopular.getContext().getString(R.string.gift_popular_text, popularString));
                            giftPopular.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        this.giftRebate.setVisibility(View.GONE);
                        giftPopular.setVisibility(View.GONE);
                    }
                }
            } else {
                conventLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            /**
             * 异常消息
             */
            conventLayout.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }
}
