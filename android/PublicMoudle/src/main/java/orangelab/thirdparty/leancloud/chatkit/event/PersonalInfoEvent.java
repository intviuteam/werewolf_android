package orangelab.thirdparty.leancloud.chatkit.event;

import android.content.Context;

/**
 * game_werewolf
 * 2017/6/17 下午6:17
 * Mystery
 */

public class PersonalInfoEvent {

    private Context context;
    private String userId;

    public PersonalInfoEvent(Context context, String userId) {
        this.context = context;
        this.userId = userId;
    }

    public Context getContext() {
        return context;
    }

    public String getUserId() {
        return userId;
    }
}
