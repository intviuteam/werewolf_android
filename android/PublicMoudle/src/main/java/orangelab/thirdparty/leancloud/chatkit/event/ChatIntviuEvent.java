package orangelab.thirdparty.leancloud.chatkit.event;

/**
 * game_werewolf
 * 2017/5/25 上午10:30
 * Mystery
 */

public class ChatIntviuEvent {

    public String userId;
    public String userName;
    public int userSex;
    public String userIvUrl;
    public String message;
    public String roomId;
    public String roomType;
    public String roomPassword;
    public String conversationId;
    public boolean isFamilyMsg;
}
