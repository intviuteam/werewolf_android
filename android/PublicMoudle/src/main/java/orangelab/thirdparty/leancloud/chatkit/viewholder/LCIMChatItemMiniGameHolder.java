package orangelab.thirdparty.leancloud.chatkit.viewholder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.datasource.GlobalUserState;
import com.R;
import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.minigame.model.MiniGameItem;
import orangelab.project.minigame.utils.MiniGameUtils;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.utils.MessageUtils;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMMiniGameMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;

/**
 * game_werewolf
 * 2018/1/19 下午3:44
 * Mystery
 */

public class LCIMChatItemMiniGameHolder extends LCIMChatItemHolder {

    private View mRefuse;
    private View mConfirm;
    private View mGameBg;

    private TextView mCountDown;

    private ImageView mImageState;
    private ImageView mImageView;

    private View mBottomView;
    private TextView mTextView;
    private CountDownTaskManager.CountDownTask mCountDownTask;

    private MiniGameItem mGameItem;
    private ChatConfig mChatConfig;

    public LCIMChatItemMiniGameHolder(ChatConfig chatConfig, Context context, ViewGroup root, boolean isLeft) {
        super(chatConfig, context, root, isLeft);
        this.mChatConfig = chatConfig;
    }

    @Override
    public void initView() {
        super.initView();
        mConfirm = View.inflate(getContext(), R.layout.lcim_chat_mini_game_layout, null);
//        view.findViewById(R.id.top).setBackgroundResource(isLeft ? R.mipmap.chat_left_text_bg : R.mipmap.chat_right_text_bg);
        mGameBg = mConfirm.findViewById(R.id.id_minigame_bg);
        conventLayout.addView(mConfirm, new ViewGroup.LayoutParams(ScreenUtils.dip2px(180), ScreenUtils.dip2px(180)));

        mTextView = (TextView) mConfirm.findViewById(R.id.id_mini_game_name);

        mImageState = (ImageView) mConfirm.findViewById(R.id.id_mini_game_state);
        mImageView = (ImageView) mConfirm.findViewById(R.id.ic_mini_game_iv);

        mBottomView = mConfirm.findViewById(R.id.id_minigame_bottom);
        mCountDown = (TextView) mConfirm.findViewById(R.id.id_minigame_countdown);
        mRefuse = mConfirm.findViewById(R.id.id_minigame_cancel);
    }

    private void turnIntoOver() {
        mConfirm.setClickable(false);
        countDownOver();
    }

    private void countDownOver() {
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
        mCountDown.setText(MessageUtils.getString(R.string.mini_game_intviu_over));
        mCountDown.setTextColor(mCountDown.getResources().getColor(R.color.color_mini_game_over));
        mCountDown.setBackgroundResource(android.R.color.white);
        mImageState.setBackgroundResource(R.color.color_mini_game_fight_over_bg);
        mImageState.setImageResource(R.mipmap.lcim_mini_game_fight_over);
        mGameBg.setBackgroundResource(R.mipmap.lcim_mini_game_fight_over_bg);
        mRefuse.setVisibility(View.GONE);
    }

    private void startCountDown(int second) {
        mCountDown.setText(MessageUtils.getString(R.string.amusement_matching_counting, Integer.toString(second)));
        mCountDown.setBackgroundResource(R.drawable.drawable_mini_game_countdown_bg);
        mCountDown.setTextColor(mCountDown.getResources().getColor(android.R.color.white));
    }

    private void turnIntoFight(int countDownSecond, int countDownTime) {
        mConfirm.setClickable(false);
        mImageState.setBackgroundResource(R.color.color_mini_game_fight_bg);
        mImageState.setImageResource(R.mipmap.lcim_mini_game_fight);
        mGameBg.setBackgroundResource(R.mipmap.lcim_mini_game_fight_bg);
        if (countDownSecond >= countDownTime) {
            countDownSecond = countDownTime;
        }
        mCountDown.setVisibility(View.VISIBLE);
        mCountDownTask = new CountDownTaskManager.CountDownTask(countDownSecond, new CountDownTaskManager.CountDownTaskListener() {

            @Override
            public void onTick(int seconds) {
                startCountDown(seconds);
            }

            @Override
            public void onStart(int startSeconds) {

            }

            @Override
            public void onFinish() {
                countDownOver();
            }
        });
        mCountDownTask.startTask();
    }

    @Override
    public void bindData(Object o) {
        super.bindData(o);
        AVIMMessage message = (AVIMMessage) o;
        message.getDeliveredAt();
        if (mGameItem == null) {
            mGameItem = new MiniGameItem();
        }
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
        try {
            if (message instanceof AVIMMiniGameMessage) {
                final AVIMMiniGameMessage gameMessage = (AVIMMiniGameMessage) message;
                String userId = (String) gameMessage.getAttrs().get(Constant.USER_ID);

                mGameItem.icon = (String) gameMessage.getAttrs().get(Constant.MINI_GAME_ICON);
                mGameItem.name = (String) gameMessage.getAttrs().get(Constant.MINI_GAME_NAME);
                mGameItem.type = (String) gameMessage.getAttrs().get(Constant.MINI_GAME_TYPE);

                mTextView.setText(mGameItem.name);

                PicassoUtils.loadImage(mImageView.getContext(), mGameItem.icon, mImageView);

                long chatTime = (long) gameMessage.getAttrs().get(Constant.CHAT_TIME);
                int countDownTime = (int) gameMessage.getAttrs().get(Constant.MINI_COUNTDOWN_TIME);

                long curTime = GlobalSocketEngine.INSTANCE.getServerCurTime();
                int second = (int) (((chatTime + countDownTime * 1000) - curTime) / 1000);

                if (second <= 0) {
                    turnIntoOver();
                } else {
                    turnIntoFight(second, countDownTime);
                    if (TextUtils.equals(userId, GlobalUserState.getGlobalState().getUserId())) {
                        mRefuse.setVisibility(View.GONE);
                        mConfirm.setClickable(false);
                    } else {
                        mRefuse.setVisibility(View.VISIBLE);
                        mConfirm.setOnClickListener(v -> {
                            if (GlobalUserState.getGlobalState().isGaming()) {
                                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_enter_error));
                                return;
                            } else {
                                try {
                                    MiniGameUtils.SendMiniGameAcceptInvite(mGameItem.type, mChatConfig.leftUser.userId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    /**
                                     * 代码保护
                                     */
                                }
                            }
                        });
                    }
                }
                mRefuse.setOnClickListener(v -> {
                    MiniGameUtils.SendMiniGameRefuseInvite(mGameItem.type, mChatConfig.leftUser.userId);
                    turnIntoOver();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return;
    }

    @Override
    public void destroy() {
        super.destroy();
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
    }
}
