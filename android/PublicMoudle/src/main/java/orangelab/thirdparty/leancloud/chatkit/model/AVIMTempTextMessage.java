package orangelab.thirdparty.leancloud.chatkit.model;

import com.avos.avoscloud.im.v2.AVIMMessageField;
import com.avos.avoscloud.im.v2.AVIMMessageType;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;

/**
 * game_werewolf
 * 2018/3/1 下午3:12
 * Mystery
 */
@AVIMMessageType(type = 120)
public class AVIMTempTextMessage extends AVIMTextMessage {

    public static final int AVIM_TEMP_TEXT_MESSAGE = 120;

    @AVIMMessageField(
            name = "_lctext"
    )
    String text;

    @AVIMMessageField(name = "op")
    private String op;

    public String getOp() {
        return op;
    }

    public AVIMTempTextMessage setOp(String op) {
        this.op = op;
        return this;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}
