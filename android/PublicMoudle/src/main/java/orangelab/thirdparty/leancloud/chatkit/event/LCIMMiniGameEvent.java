package orangelab.thirdparty.leancloud.chatkit.event;

import orangelab.project.minigame.model.MiniGameItem;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/19 下午7:17
 * Mystery
 */

public class LCIMMiniGameEvent implements Keepable {

    private MiniGameItem mMiniGameItem;
    private String targetUserId;

    public LCIMMiniGameEvent(MiniGameItem miniGameItem, String targetUserId) {
        mMiniGameItem = miniGameItem;
        this.targetUserId = targetUserId;
    }

    public MiniGameItem getMiniGameItem() {
        return mMiniGameItem;
    }

    public String getTargetUserId() {
        return targetUserId;
    }
}
