package orangelab.thirdparty.leancloud.chatkit.utils;

import android.content.Context;
import android.content.Intent;

import orangelab.thirdparty.leancloud.feedback.FeedBackActivity;

/**
 * game_werewolf
 * 2017/6/14 上午11:58
 * Mystery
 */

public class LeanCloudFeedbackAgent {


    public static void launchFeedBack(Context context) {
        Intent intent = new Intent(context, FeedBackActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }
}
