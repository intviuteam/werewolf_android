package orangelab.thirdparty.leancloud.chatkit.utils;

/**
 * 系统名称:
 * 模块名称:
 * 功能说明:
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 */

public class Constant {
    public static final String APP = "APP";
    public static final String USER_ID = "USER_ID";
    public static final String EVENT_CHAT = "EVENT_CHAT"; //聊天事件
    public static final String USER_NAME = "USER_NAME";  //用户名
    public static final String USER_ICON = "USER_ICON";  //用户头像
    public static final String USER_SEX = "USER_SEX";    //用户性别
    public static final String ROOM_ID = "ROOM_ID";//房间号
    public static final String GAME_TYPE = "GAME_TYPE";//游戏类型
    public static final String ROOM_PASSWORD = "ROOM_PASSWORD";//房间密码
    public static final String USER_CHAT_TYPE = "USER_CHAT_TYPE"; //消息类型
    public static final String CHAT_TIME = "CHAT_TIME";  //消息时间
    public static final String CHAT_MESSAGE = "CHAT_MESSAGE";//聊天的消息
    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";//文本
    public static final String MESSAGE_TYPE_CHAT = "MESSAGE_TYPE_CHAT";
    public static final String EVENT_LEAVE_CHAT = "LEAVE_EVENT_CHAT"; //离开聊天事件
    public static final String GIFT_TYPE = "GIFT_TYPE";
    public static final String GIFT_REBATE = "GIFT_REBATE";
    public static final String READ = "READ";
    public static final String INTENT = "INTENT";
    public static final String APPREOVE_FRIEND = "APPREOVE_FRIEND";
    public static final String CONVERSATION_ID = "CONVERSATION_ID";
    public static final String CONVERSATION_TYPE = "CONVERSATION_TYPE";
    public static final String IS_SINGLE_CHAT = "IS_SINGLE_CHAT";
    public static final String IS_FAMILY_CONVERSATION = "IS_FAMILY_CONVERSATION";
    public static final String MSG_TYPE_FAMILY = "FAMILY";
    public static final String MSG_TYPE_SINGLE = "SINGLE";
    public static final String MSG_TYPE = "MSG_TYPE";//SINGLE

    public static final String MINI_GAME_TYPE = "MINI_GAME_TYPE";
    public static final String MINI_GAME_NAME = "MINI_GAME_NAME";
    public static final String MINI_GAME_ICON = "MINI_GAME_ICON";
    public static final String MINI_COUNTDOWN_TIME = "MINI_COUNTDOWN_TIME";
    public static final String PRIVACY_COUNTDOWN_TIME = "PRIVACY_COUNTDOWN_TIME";
}
