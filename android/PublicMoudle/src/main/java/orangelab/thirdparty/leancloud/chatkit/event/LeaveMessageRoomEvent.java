package orangelab.thirdparty.leancloud.chatkit.event;

/**
 * Created by wangxu on 2017/5/4.
 * 退出聊天房间后的消息事件
 */

public class LeaveMessageRoomEvent {
    public String messageType;
    public String otherUserId;

    public LeaveMessageRoomEvent(String messageType, String otherUserId) {
        this.messageType = messageType;
        this.otherUserId = otherUserId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        this.otherUserId = otherUserId;
    }
}
