package orangelab.thirdparty.leancloud.chatkit.model;

import com.avos.avoscloud.im.v2.AVIMMessageCreator;
import com.avos.avoscloud.im.v2.AVIMMessageField;
import com.avos.avoscloud.im.v2.AVIMMessageType;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;

import java.util.Map;

/**
 * game_werewolf
 * 2018/4/16 上午10:27
 * Mystery
 */
@AVIMMessageType(
        type = 104
)
public class AVIMPrivateRoomMessage extends AVIMTextMessage {

    public static final int PRIVATE_ROOM_MESSAGE = 104;

    @AVIMMessageField(
            name = "_lctext"
    )
    String text;
    @AVIMMessageField(
            name = "_lcattrs"
    )
    Map<String, Object> attrs;
    public static final Creator<AVIMGiftMessage> CREATOR = new AVIMMessageCreator(AVIMMiniGameMessage.class);

    public AVIMPrivateRoomMessage() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Object> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, Object> attr) {
        this.attrs = attr;
    }
}
