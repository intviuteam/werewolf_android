package orangelab.thirdparty.leancloud.chatkit.model;

import com.avos.avoscloud.im.v2.AVIMMessageCreator;
import com.avos.avoscloud.im.v2.AVIMMessageField;
import com.avos.avoscloud.im.v2.AVIMMessageType;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;

import java.util.Map;

/**
 * game_werewolf
 * 2017/5/25 下午4:03
 * Mystery
 * 送礼物的消息
 */
@AVIMMessageType(
        type = 103
)
public class AVIMMiniGameMessage extends AVIMTextMessage {

    public static final int GiftMessageType = 103;
    @AVIMMessageField(
            name = "_lctext"
    )
    String text;
    @AVIMMessageField(
            name = "_lcattrs"
    )
    Map<String, Object> attrs;
    public static final Creator<AVIMGiftMessage> CREATOR = new AVIMMessageCreator(AVIMMiniGameMessage.class);

    public AVIMMiniGameMessage() {
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Object> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, Object> attr) {
        this.attrs = attr;
    }
}
