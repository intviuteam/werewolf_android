package orangelab.thirdparty.leancloud.chatkit.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMMessageOption;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMMessagesQueryCallback;
import com.avos.avoscloud.im.v2.messages.AVIMAudioMessage;
import com.avos.avoscloud.im.v2.messages.AVIMImageMessage;
import com.avos.avoscloud.im.v2.messages.AVIMLocationMessage;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.tool.WrapImagePicker;
import orangelab.project.minigame.MiniGameItemClickListener;
import orangelab.project.minigame.MiniGamesListPopWindow;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.exhibition.gift.GiftEvent;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.manager.AppManager;
import orangelab.project.minigame.model.MiniGameItem;
import orangelab.project.minigame.utils.MiniGameUtils;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.KeyBoardChangeListener;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.ClickProtector;
import com.toolkit.CloneUtils;

import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.adapter.LCIMChatAdapter;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMConversationReadStatusEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMIMTypeMessageEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMInputBottomBarEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMInputBottomBarRecordEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMInputBottomBarTextEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMMiniGameEvent;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMSendTempTextMessageEvent;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMGiftMessage;

import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.model.AVIMMiniGameMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMPrivateRoomMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMTempTextMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMIntentDataHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMLogUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMNotificationUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMPathUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import orangelab.thirdparty.leancloud.chatkit.event.LCIMMessageResendEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMAudioHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMRxPermissionHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;
import orangelab.thirdparty.leancloud.chatkit.view.LCIMInputBottomBar;

/**
 * Created by wli on 15/8/27.
 * 将聊天相关的封装到此 Fragment 里边，只需要通过 setConversation 传入 Conversation 即可
 */
public class LCIMConversationFragment extends Fragment {

    private static final String TAG = "LCIMConversationFragmen";

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_IMAGE_PICK = 2;

    protected AVIMConversation imConversation;

    /**
     * recyclerView 对应的 Adapter
     */
    protected LCIMChatAdapter itemAdapter;

    protected RecyclerView recyclerView;
    protected WrapContentLinearLayoutManager layoutManager;

    /**
     * 负责下拉刷新的 SwipeRefreshLayout
     */
    protected SwipeRefreshLayout refreshLayout;

    /**
     * 底部的输入栏
     */
    protected LCIMInputBottomBar inputBottomBar;

    // 记录拍照等的文件路径
    protected String localCameraPath;

    private HashMap<String, Object> mapData = new HashMap<>();
    private HashMap<String, Object> otherData = new HashMap<>();

    private Button presentButton;
    private Button gameButton;
    private Button privateRoomButton;

    private String key = hashCode() + "";

    private ChatConfig mChatConfig;

    private KeyBoardChangeListener mKeyBoardChangeListener;
    private MiniGamesListPopWindow mMiniGamesListPopWindow;

    private WrapImagePicker mWrapImagePicker = null;

    private boolean dataIsInit = false;

    private void initData() {
        try {
            if (dataIsInit) return;
            dataIsInit = true;
            if (mChatConfig.isSingleChat) {
                /**
                 * 单聊包装数据
                 */
                UserData leftUser = mChatConfig.leftUser;
                UserData rightUser = mChatConfig.rightUser;

                mapData.put(Constant.USER_ID, rightUser.userId);
                mapData.put(Constant.USER_ICON, rightUser.userImageUrl);
                mapData.put(Constant.USER_SEX, Integer.toString(rightUser.userSex));
                mapData.put(Constant.USER_NAME, rightUser.userName);
                mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
                if (imConversation != null)
                    mapData.put(Constant.CONVERSATION_ID, imConversation.getConversationId());
                mapData.put(Constant.APP, AppManager.GetApp());

                otherData.put(Constant.USER_ID, leftUser.userId);
                otherData.put(Constant.USER_ICON, leftUser.userImageUrl);
                otherData.put(Constant.USER_SEX, Integer.toString(leftUser.userSex));
                otherData.put(Constant.USER_NAME, leftUser.userName);
                otherData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
                if (imConversation != null)
                    otherData.put(Constant.CONVERSATION_ID, imConversation.getConversationId());
                otherData.put(Constant.READ, true);
                otherData.put(Constant.APP, AppManager.GetApp());

            } else {
                /**
                 * 群聊包装数据
                 */
                mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
                mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
                mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
                mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
                mapData.put(Constant.CONVERSATION_ID, imConversation.getConversationId());
                mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_FAMILY);

                otherData.putAll(mapData);
                otherData.put(Constant.READ, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addExtraIntoMapData(String key, String value) {
        mapData.put(key, value);
        otherData.put(key, value);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lcim_conversation_fragment, container, false);
        EventBus.getDefault().register(this);

        mChatConfig = LCIMIntentDataHelper.getChatConfig(getActivity().getIntent());
        /**
         * 初始化公共数据
         */
        initCommonView(view);
        /**
         * 初始化送按钮
         */
        initButton(view);
        /**
         * 初始化包装数据,包装数据以后只在Conversation创建之后才开始初始化
         */

        mKeyBoardChangeListener = new KeyBoardChangeListener(getActivity());
        mKeyBoardChangeListener.setKeyBoardListener((isShow, keyboardHeight) -> {
            if (isShow) {

            } else {
                if (mMiniGamesListPopWindow != null && mMiniGamesListPopWindow.isShowing()) {
                    mMiniGamesListPopWindow.dismiss();
                }
            }
        });
        return view;
    }

    private void initCommonView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.fragment_chat_rv_chat);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_chat_srl_pullrefresh);
        refreshLayout.setEnabled(false);
        inputBottomBar = (LCIMInputBottomBar) view.findViewById(R.id.fragment_chat_inputbottombar);
        layoutManager = new WrapContentLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        itemAdapter = getAdpter();
        itemAdapter.resetRecycledViewPoolSize(recyclerView);
        recyclerView.setAdapter(itemAdapter);
    }

    public void showMiniGamePopWindow() {
        try {
            mMiniGamesListPopWindow = new MiniGamesListPopWindow(getContext(), MiniGamesListPopWindow.FROM_OTHER, new MiniGameItemClickListener() {
                @Override
                public void onItemClicked(PopupWindow popupWindow, LCIMMiniGameEvent event) {
                    /**
                     * 发送一个游戏对战邀请信息
                     */
                    try {
                        if (GlobalUserState.getGlobalState().isGaming()) {
                            ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_intviu_error_room));
                            popupWindow.dismiss();
                            return;
                        }

                        if (mChatConfig.isSingleChat) {
                            if (TextUtils.equals(mChatConfig.leftUser.userId, event.getTargetUserId())) {
                                sendMiniGameMessage(event.getMiniGameItem());
                            }
                        }
                        popupWindow.dismiss();
                    } catch (Exception e) {
                        /**
                         * 出错则不再发送消息
                         */
                        e.printStackTrace();
                    }
                }
            }, null);
            mMiniGamesListPopWindow.setTargetUserId(mChatConfig.leftUser.userId);
            mMiniGamesListPopWindow.show(inputBottomBar);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initButton(View view) {
        presentButton = (Button) view.findViewById(R.id.fragment_chat_present_gift);
        gameButton = (Button) view.findViewById(R.id.fragment_chat_amusement);
        privateRoomButton = (Button) view.findViewById(R.id.fragment_private_room);

        if (mChatConfig.isSingleChat) {
            /**
             * 单聊这里要做一个区分，如果发现是客服的话就不展示 送礼物什么的按钮
             */
            try {
                if (ProjectConfig.EXCLUDE_USERID.contains(mChatConfig.leftUser.userId)) {
                    presentButton.setVisibility(View.GONE);
                    gameButton.setVisibility(View.GONE);
                    privateRoomButton.setVisibility(View.GONE);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            gameButton.setVisibility(View.VISIBLE);
            gameButton.setOnClickListener(v -> {
                showMiniGamePopWindow();
            });
            presentButton.setVisibility(View.VISIBLE);
            presentButton.setOnClickListener(v -> {
                ExhibitionsActivity.startActivityFromChat(getContext(), mChatConfig.leftUser.userId, mChatConfig.leftUser.userName, mChatConfig.leftUser.userSex, mChatConfig.leftUser.userImageUrl);
            });

            if (ProjectConfig.NEED_SHOW_PRIVATE_ROOM_BUTTON_IN_CHAT) {
                privateRoomButton.setVisibility(View.VISIBLE);
            } else {
                privateRoomButton.setVisibility(View.GONE);
            }
            ClickProtector clickProtector = new ClickProtector() {
                @Override
                public void onClickWithProtector(View v) {
                    /**
                     * 发送私密房的游戏邀请
                     */
                    if (GlobalUserState.getGlobalState().isGaming()) {

                    } else {
                        handleSendPrivateRoomIntvte();
                    }
                }
            };
            clickProtector.addIntercept(new ClickProtector.ClickTimeIntercept(1000));
            privateRoomButton.setOnClickListener(clickProtector);
        } else {
            presentButton.setVisibility(View.GONE);
            gameButton.setVisibility(View.GONE);
            privateRoomButton.setVisibility(View.GONE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        initListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != imConversation) {
            LCIMNotificationUtils.addTag(imConversation.getConversationId());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LCIMAudioHelper.getInstance().stopPlayer();
        if (null != imConversation) {
            LCIMNotificationUtils.removeTag(imConversation.getConversationId());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        RxToolKit.UnRegister(key);
    }

    private void initListener() {
        initGiftListener();
//        initMiniGameListener();
    }

//    private void initMiniGameListener() {
//        RxToolKit.Build(key, LCIMMiniGameEvent.class).action(event -> {
//
//        }).register();
//    }

    private void initGiftListener() {
        RxToolKit.Build(key, GiftEvent.GiftForChatWrapperEvent.class).action(giftWrapperEvent -> {
            String giftType = giftWrapperEvent.getGift().getGiftType();
            sendPresentMessage(
                    MessageUtils.getString(R.string.present_gift, EffectsManager.GetGiftNameByType(giftType)),
                    giftType,
                    giftWrapperEvent.getGift().getDisCount()
            );
        }).register();
        RxToolKit.Build(key, CardEvent.CardForChatWrapperEvent.class).action(cardForChatWrapperEvent -> {
            String cardType = cardForChatWrapperEvent.getCard().getRealType();
            sendPresentMessage(
                    MessageUtils.getString(R.string.present_gift, EffectsManager.GetCardNameByType(cardType)),
                    cardType
            );
        }).register();
    }

    private void sendPresentMessage(String msg, String type) {
        sendPresentMessage(msg, type, "");
    }

    private void sendPresentMessage(String msg, String type, String rebate) {
        AVIMGiftMessage message = new AVIMGiftMessage();
        addExtraIntoMapData(Constant.GIFT_TYPE, type);
        addExtraIntoMapData(Constant.GIFT_REBATE, rebate);
        message.setText(msg);
        sendMessage(message, true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        refreshLayout.setOnRefreshListener(() -> {
            AVIMMessage message = itemAdapter.getFirstMessage();
            if (null == message) {
                refreshLayout.setRefreshing(false);
            } else {
                imConversation.queryMessages(message.getMessageId(), message.getTimestamp(), 50, new
                        AVIMMessagesQueryCallback() {
                            @Override
                            public void done(List<AVIMMessage> list, AVIMException e) {
                                refreshLayout.setRefreshing(false);
                                if (filterException(e)) {
                                    if (null != list && list.size() > 0) {
                                        itemAdapter.addMessageList(list);
                                        itemAdapter.setDeliveredAndReadMark(imConversation.getLastDeliveredAt(),
                                                imConversation.getLastReadAt());
                                        itemAdapter.notifyDataSetChanged();
                                        layoutManager.scrollToPositionWithOffset(list.size() - 1, 0);
                                    }
                                }
                            }
                        });
            }
        });
    }

    protected LCIMChatAdapter getAdpter() {
        return new LCIMChatAdapter(mChatConfig);
    }

    private void release() {
        RxToolKit.UnRegister(key);
        EventBus.getDefault().unregister(this);
        if (itemAdapter != null) {
            itemAdapter.destroy();
        }
        if (mKeyBoardChangeListener != null) {
            mKeyBoardChangeListener.destroy();
            mKeyBoardChangeListener = null;
        }
        if (mMiniGamesListPopWindow != null) {
            if (mMiniGamesListPopWindow.isShowing()) {
                mMiniGamesListPopWindow.dismiss();
            }
            mMiniGamesListPopWindow = null;
        }
        if (mWrapImagePicker != null) {
            mWrapImagePicker.destroy();
            mWrapImagePicker = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        release();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        release();
    }

    public void setWrapImagePicker(WrapImagePicker wrapImagePicker) {
        mWrapImagePicker = wrapImagePicker;
        if (mWrapImagePicker != null) {
            mWrapImagePicker.bindCallBack(new WrapImagePicker.ImagePickerObserver() {
                @Override
                public void onPicked(String path) {
                    Log.i(TAG, "onPicked: " + path);
                    if (TextUtils.isEmpty(path)) {
                        return;
                    }
                    if (path.contains("file://")) {
                        path = path.replace("file://", "");
                    }
                    sendImage(path);
                }

                @Override
                public void onUploadStart(String path) {

                }

                @Override
                public void onUploadProgress(String path, double percent) {

                }

                @Override
                public void onUploadSuccess(String path, String key, String url) {

                }

                @Override
                public void onUploadError(String path, Exception e) {

                }
            });
        }
    }

    public void setConversation(final AVIMConversation conversation) {
        imConversation = conversation;
        refreshLayout.setEnabled(true);
        inputBottomBar.setTag(imConversation.getConversationId());
        fetchMessages();
        LCIMNotificationUtils.addTag(conversation.getConversationId());
        if (!conversation.isTransient()) {
            if (conversation.getMembers().size() == 0) {
                conversation.fetchInfoInBackground(new AVIMConversationCallback() {
                    @Override
                    public void done(AVIMException e) {
                        if (null != e) {
                            LCIMLogUtils.logException(e);
                        }
                        itemAdapter.showUserName(conversation.getMembers().size() > 2);
                    }
                });
            } else {
                itemAdapter.showUserName(conversation.getMembers().size() > 2);
            }
        } else {
            itemAdapter.showUserName(true);
        }
        initData();
    }

    /**
     * 拉取消息，必须加入 conversation 后才能拉取消息
     */
    private void fetchMessages() {
        imConversation.queryMessages(new AVIMMessagesQueryCallback() {
            @Override
            public void done(List<AVIMMessage> messageList, AVIMException e) {
                if (filterException(e)) {
                    itemAdapter.setMessageList(messageList);
                    recyclerView.setAdapter(itemAdapter);
                    itemAdapter.setDeliveredAndReadMark(imConversation.getLastDeliveredAt(),
                            imConversation.getLastReadAt());
                    itemAdapter.notifyDataSetChanged();
                    scrollToBottom();
                    clearUnreadConut();
                }
            }
        });
    }

    /**
     * 输入事件处理，接收后构造成 AVIMTextMessage 然后发送
     * 因为不排除某些特殊情况会受到其他页面过来的无效消息，所以此处加了 tag 判断
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMInputBottomBarTextEvent textEvent) {
        if (null != imConversation && null != textEvent) {
            if (!TextUtils.isEmpty(textEvent.sendContent) && imConversation.getConversationId().equals(textEvent.tag)) {
                sendText(textEvent.sendContent);
            }
        }
    }

    /**
     * 处理推送过来的消息
     * 同理，避免无效消息，此处加了 conversation id 判断
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMIMTypeMessageEvent messageEvent) {
        Utils.runSafely(() -> {
            if (messageEvent.isInner)
                return;
            if (null != imConversation && null != messageEvent && null != messageEvent.conversation &&
                    imConversation.getConversationId().equals(messageEvent.conversation.getConversationId())) {
                itemAdapter.addMessage(messageEvent.message);
                itemAdapter.notifyDataSetChanged();
                scrollToBottom();
                clearUnreadConut();
            }
        });
    }

    /**
     * 重新发送已经发送失败的消息
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMMessageResendEvent resendEvent) {
        if (null != imConversation && null != resendEvent &&
                null != resendEvent.message && imConversation.getConversationId().equals(resendEvent.message
                .getConversationId())) {
            if (AVIMMessage.AVIMMessageStatus.AVIMMessageStatusFailed == resendEvent.message.getMessageStatus()
                    && imConversation.getConversationId().equals(resendEvent.message.getConversationId())) {
                sendMessage(resendEvent.message, false);
            }
        }
    }

    /**
     * 处理输入栏发送过来的事件
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMInputBottomBarEvent event) {
        if (null != imConversation && null != event && imConversation.getConversationId().equals(event.tag)) {
            switch (event.eventAction) {
                case LCIMInputBottomBarEvent.INPUTBOTTOMBAR_IMAGE_ACTION:
                    /**
                     * 处理传送图片
                     */
                    RxPermissions permissions1 = LCIMRxPermissionHelper.IMPL().getPermissions();
                    if (permissions1 != null) {
                        permissions1.request(Manifest.permission.READ_EXTERNAL_STORAGE)
                                .subscribe(aBoolean -> {
                                    if (aBoolean) {
                                        //  dispatchPickPictureIntent();
                                        try {
                                            mWrapImagePicker.filesPicker(getActivity());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        ToastToolKit.showShort(R.string.permission_read_store_error);
                                    }
                                });
                    }
                    break;
                case LCIMInputBottomBarEvent.INPUTBOTTOMBAR_CAMERA_ACTION:
                    RxPermissions permissions2 = LCIMRxPermissionHelper.IMPL().getPermissions();
                    if (permissions2 != null) {
                        permissions2.request(Manifest.permission.CAMERA)
                                .subscribe(aBoolean -> {
                                    if (aBoolean) {
                                        //dispatchTakePictureIntent();
                                        try {
                                            mWrapImagePicker.cameraPicker(getActivity());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        ToastToolKit.showShort(R.string.string_permission_camera_error);
                                    }
                                });
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 处理录音事件
     *
     * @param recordEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMInputBottomBarRecordEvent recordEvent) {
        if (null != imConversation && null != recordEvent &&
                !TextUtils.isEmpty(recordEvent.audioPath) &&
                imConversation.getConversationId().equals(recordEvent.tag)) {
            sendAudio(recordEvent.audioPath);
        }
    }

    /**
     * 更新对方已读的位置事件
     *
     * @param readEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMConversationReadStatusEvent readEvent) {
        if (null != imConversation && null != readEvent &&
                imConversation.getConversationId().equals(readEvent.conversationId)) {
            itemAdapter.setDeliveredAndReadMark(imConversation.getLastDeliveredAt(),
                    imConversation.getLastReadAt());
            itemAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 发送 Intent 跳转到系统拍照页面
     */
    private void dispatchTakePictureIntent() {
        localCameraPath = LCIMPathUtils.getPicturePathByCurrentTime(getContext());
        File file = new File(localCameraPath);
        Uri imageUri = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            imageUri = Uri.fromFile(file);
        } else {
            imageUri = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".provider", file);
        }
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        =Uri.fromFile(new File(localCameraPath));
        takePictureIntent.putExtra("return-data", false);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * 发送 Intent 跳转到系统图库页面
     */
    private void dispatchPickPictureIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, null);
        photoPickerIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE_PICK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (Activity.RESULT_OK == resultCode) {
//            switch (requestCode) {
//                case REQUEST_IMAGE_CAPTURE:
//                    sendImage(localCameraPath);
//                    break;
//                case REQUEST_IMAGE_PICK:
//                    sendImage(getRealPathFromURI(getActivity(), data.getData()));
//                    break;
//                default:
//                    break;
//            }
//        }
    }

    /**
     * 滚动 recyclerView 到底部
     */
    private void scrollToBottom() {
        layoutManager.scrollToPositionWithOffset(itemAdapter.getItemCount() - 1, 0);
    }

    /**
     * 根据 Uri 获取文件所在的位置
     *
     * @param context
     * @param contentUri
     * @return
     */
    private String getRealPathFromURI(Context context, Uri contentUri) {
        if (contentUri.getScheme().equals("file")) {
            return contentUri.getEncodedPath();
        } else {
            Cursor cursor = null;
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                if (null != cursor) {
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    return cursor.getString(column_index);
                } else {
                    return "";
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    /**
     * 发送文本消息
     *
     * @param content
     */
    protected void sendText(String content) {
        AVIMTextMessage message = new AVIMTextMessage();
        message.setText(content);
        sendMessage(message);
    }

    /**
     * 发送图片消息
     * TODO 上传的图片最好要压缩一下
     *
     * @param imagePath
     */
    protected void sendImage(String imagePath) {
        try {
            sendMessage(new AVIMImageMessage(imagePath));
        } catch (IOException e) {
            LCIMLogUtils.logException(e);
        }
    }

    /**
     * 发送语音消息
     *
     * @param audioPath
     */
    protected void sendAudio(String audioPath) {
        try {
            AVIMAudioMessage audioMessage = new AVIMAudioMessage(audioPath);
            sendMessage(audioMessage);
        } catch (IOException e) {
            LCIMLogUtils.logException(e);
        }
    }

    public void sendMessage(AVIMMessage message) {
        sendMessage(message, true);
    }

    private void wrapperMapData(AVIMMessage message) {
        HashMap<String, Object> mTempMap = CloneUtils.clone(mapData);
        if (mTempMap != null) {
            mTempMap.put(Constant.CHAT_TIME, System.currentTimeMillis());
            if (message instanceof AVIMTextMessage) {
                ((AVIMTextMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMAudioMessage) {
                ((AVIMAudioMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMImageMessage) {
                ((AVIMImageMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMLocationMessage) {
                ((AVIMLocationMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMGiftMessage) {
                ((AVIMGiftMessage) message).setAttrs(mTempMap);
            }
        }
    }

    private void wrapperOtherData(AVIMMessage message) {
        HashMap<String, Object> mTempMap = CloneUtils.clone(otherData);
        if (mTempMap != null) {
            mTempMap.put(Constant.CHAT_TIME, System.currentTimeMillis());
            if (message instanceof AVIMTextMessage) {
                ((AVIMTextMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMAudioMessage) {
                ((AVIMAudioMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMImageMessage) {
                ((AVIMImageMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMLocationMessage) {
                ((AVIMLocationMessage) message).setAttrs(mTempMap);
            } else if (message instanceof AVIMGiftMessage) {
                ((AVIMGiftMessage) message).setAttrs(mTempMap);
            }
        }
    }

    private void wrapperMiniGameMessage(HashMap<String, Object> temp, MiniGameItem miniGameItem) {
        temp.put(Constant.CHAT_TIME, GlobalSocketEngine.INSTANCE.getServerCurTime());
        temp.put(Constant.MINI_GAME_TYPE, miniGameItem.type);
        temp.put(Constant.MINI_GAME_NAME, miniGameItem.name);
        temp.put(Constant.MINI_GAME_ICON, miniGameItem.icon);
        temp.put(Constant.MINI_COUNTDOWN_TIME, 30);
    }

    private void wrapperPrivateRoomMessage(HashMap<String, Object> temp) {
        temp.put(Constant.CHAT_TIME, GlobalSocketEngine.INSTANCE.getServerCurTime());
        temp.put(Constant.PRIVACY_COUNTDOWN_TIME, 30);
    }

    private void handleSendPrivateRoomIntvte() {
        /**
         * 处理发送私密房邀请功能
         */
        if (GlobalSocketEngine.INSTANCE.isSocketError()) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
            return;
        }

        /**
         * 这里有一定的情况当展示出对话框的时候 Conversation还没有初始化
         */
        if (imConversation == null) {
            return;
        }

        AVIMPrivateRoomMessage message = new AVIMPrivateRoomMessage();
        HashMap<String, Object> temp = CloneUtils.clone(mapData);
        wrapperPrivateRoomMessage(temp);
        message.setText(MessageUtils.getString(R.string.str_private_room_chat_intviu));
        message.setAttrs(temp);

        AVIMPrivateRoomMessage otherMessage = new AVIMPrivateRoomMessage();
        HashMap<String, Object> temp2 = CloneUtils.clone(otherData);
        wrapperPrivateRoomMessage(temp2);
        otherMessage.setText(MessageUtils.getString(R.string.str_private_room_chat_intviu));
        otherMessage.setAttrs(temp2);

        itemAdapter.addMessage(message);
        itemAdapter.notifyDataSetChanged();

        scrollToBottom();

        AVIMMessageOption option = new AVIMMessageOption();
        option.setReceipt(true);

        imConversation.sendMessage(message, option, new AVIMConversationCallback() {
            @Override
            public void done(AVIMException e) {
                runOnUIThreadSavely(() -> {
                    if (e == null) {
                        /**
                         * 这里需要区分一下聊天类型如果是单聊执行下面
                         */
                        try {
                            if (mChatConfig.isSingleChat) {
                                LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                                event.conversation = imConversation;
                                event.isInner = true;
                                event.message = otherMessage;
                                EventBus.getDefault().post(event);
                            }
                            GlobalSocketEngine.INSTANCE.sendPrivateRoomIntviu(mChatConfig.leftUser.userId, 30000);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    public void sendMiniGameMessage(MiniGameItem miniGameItem) {
        /**
         * we can't send message if NetWork is unavailable
         */
        if (GlobalSocketEngine.INSTANCE.isSocketError()) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
            return;
        }

        AVIMMiniGameMessage miniGameMessage = new AVIMMiniGameMessage();
        HashMap<String, Object> temp = CloneUtils.clone(mapData);
        wrapperMiniGameMessage(temp, miniGameItem);
        miniGameMessage.setText(MessageUtils.getString(R.string.mini_game_intviu));
        miniGameMessage.setAttrs(temp);

        AVIMMiniGameMessage otherMiniGameMessage = new AVIMMiniGameMessage();
        HashMap<String, Object> temp2 = CloneUtils.clone(otherData);
        wrapperMiniGameMessage(temp2, miniGameItem);
        otherMiniGameMessage.setText(MessageUtils.getString(R.string.mini_game_intviu));
        otherMiniGameMessage.setAttrs(temp2);

        itemAdapter.addMessage(miniGameMessage);

        itemAdapter.notifyDataSetChanged();

        scrollToBottom();

        AVIMMessageOption option = new AVIMMessageOption();
        option.setReceipt(true);
        imConversation.sendMessage(miniGameMessage, option, new AVIMConversationCallback() {
            @Override
            public void done(AVIMException e) {
                itemAdapter.notifyDataSetChanged();
                /**
                 * 这里需要区分一下聊天类型如果是单聊执行下面
                 */
                if (mChatConfig.isSingleChat) {
                    LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                    event.conversation = imConversation;
                    event.isInner = true;
                    event.message = (AVIMTypedMessage) otherMiniGameMessage;
                    EventBus.getDefault().post(event);

                    try {
                        MiniGameUtils.SendMiniGameInvite(miniGameItem.type, mChatConfig.leftUser.userId, 30000);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LCIMSendTempTextMessageEvent sendTempTextMessageEvent) {
        sendTempMessage(sendTempTextMessageEvent.getText());
    }

    public void sendTempMessage(String string) {
        AVIMTempTextMessage message = new AVIMTempTextMessage();
        message.setText(string);
        wrapperMapData(message);
        sendTempMessage(message);
    }

    public void sendTempMessage(final AVIMMessage message) {
        AVIMMessageOption messageOption = new AVIMMessageOption();
        messageOption.setTransient(true);
        if (imConversation != null) {
            imConversation.sendMessage(message, messageOption, new AVIMConversationCallback() {
                @Override
                public void done(AVIMException e) {
                    if (e == null) {
                        //send success
                    } else {

                    }
                }
            });
        }
    }

    /**
     * 发送消息
     *
     * @param message
     */
    public void sendMessage(final AVIMMessage message, boolean addToList) {
        if (addToList) {
            itemAdapter.addMessage(message);
        }
        itemAdapter.notifyDataSetChanged();

        scrollToBottom();

        wrapperMapData(message);

        AVIMMessageOption option = new AVIMMessageOption();
        option.setReceipt(true);
        imConversation.sendMessage(message, option, new AVIMConversationCallback() {
            @Override
            public void done(AVIMException e) {
                itemAdapter.notifyDataSetChanged();
                /**
                 * 这里需要区分一下聊天类型如果是单聊执行下面
                 */
                if (mChatConfig.isSingleChat) {
                    LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                    wrapperOtherData(message);
                    event.conversation = imConversation;
                    event.isInner = true;
                    event.message = (AVIMTypedMessage) message;
                    EventBus.getDefault().post(event);
                    if (null != e) {
                        LCIMLogUtils.logException(e);
                    }
                } else {
                    /**
                     * 对于群聊另作处理
                     */
                    LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                    wrapperOtherData(message);
                    event.conversation = imConversation;
                    event.isInner = true;
                    event.message = (AVIMTypedMessage) message;
                    EventBus.getDefault().post(event);
                    if (null != e) {
                        LCIMLogUtils.logException(e);
                    }
                    /**
                     * 汇报任务
                     */
                    FamilyApiManager.FamilyTaskDoApi();

                }

            }
        });
    }

    private boolean filterException(Exception e) {
        if (null != e) {
            LCIMLogUtils.logException(e);
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return (null == e);
    }

    private void clearUnreadConut() {
        if (imConversation.getUnreadMessagesCount() > 0) {
            imConversation.read();
        }
    }

    private void runOnUIThreadSavely(Runnable runnable) {
        try {
            ((SafeActivity) getActivity()).runOnUiThreadSafely(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
