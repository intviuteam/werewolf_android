package orangelab.thirdparty.leancloud.chatkit;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2017/10/20 下午2:46
 * Mystery
 */

public class ChatConfig implements Parcelable {

    public static final String PERSONAL_INFO = "personal_info";

    public UserData leftUser;
    public UserData rightUser;
    public boolean isSingleChat = true;
    public boolean isNeedShowMiniGame = false;
    public String conversationId = "";
    public String peerId = "";
    public String from = "";

    public ChatConfig() {
    }

    public boolean fromPersonalInfo() {
        return TextUtils.equals(PERSONAL_INFO, from);
    }

    protected ChatConfig(Parcel in) {
        leftUser = in.readParcelable(UserData.class.getClassLoader());
        rightUser = in.readParcelable(UserData.class.getClassLoader());
        isSingleChat = in.readByte() != 0;
        isNeedShowMiniGame = in.readByte() != 0;
        conversationId = in.readString();
        peerId = in.readString();
        from = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(leftUser, flags);
        dest.writeParcelable(rightUser, flags);
        dest.writeByte((byte) (isSingleChat ? 1 : 0));
        dest.writeByte((byte) (isNeedShowMiniGame ? 1 : 0));
        dest.writeString(conversationId);
        dest.writeString(peerId);
        dest.writeString(from);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatConfig> CREATOR = new Creator<ChatConfig>() {
        @Override
        public ChatConfig createFromParcel(Parcel in) {
            return new ChatConfig(in);
        }

        @Override
        public ChatConfig[] newArray(int size) {
            return new ChatConfig[size];
        }
    };
}
