package orangelab.thirdparty.leancloud.chatkit.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 系统名称:
 * 模块名称:
 * 功能说明:
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 */

public class UserData implements Parcelable {

    public String userId;
    public String userName;
    public int userSex;
    public String userImageUrl;

    protected UserData() {
    }

    protected UserData(Parcel in) {
        userId = in.readString();
        userName = in.readString();
        userSex = in.readInt();
        userImageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeInt(userSex);
        dest.writeString(userImageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserData> CREATOR = new Creator<UserData>() {
        @Override
        public UserData createFromParcel(Parcel in) {
            return new UserData(in);
        }

        @Override
        public UserData[] newArray(int size) {
            return new UserData[size];
        }
    };

    public static UserData createUserData(String userId, String userName, int userSex, String userImageUrl) {
        UserData userData = new UserData();
        userData.userId = userId;
        userData.userName = userName;
        userData.userSex = userSex;
        userData.userImageUrl = userImageUrl;
        return userData;
    }

}
