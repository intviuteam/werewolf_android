package orangelab.thirdparty.leancloud.chatkit.utils;

import com.androidtoolkit.ThreadToolKit;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMMessageOption;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;

import java.util.ArrayList;
import java.util.List;

import orangelab.thirdparty.leancloud.chatkit.LCChatKit;

/**
 * game_werewolf
 * 2017/11/23 下午4:11
 * Mystery
 */

public class LeanCloudToolKit {


    public static void SendMessageIntoConversation(String conversationId, AVIMMessage message, AVIMConversationQueryCallback callback) {
        ThreadToolKit.Async(() -> {
            LCChatKit.getInstance().getClient().getConversationsQuery().whereEqualTo("objectId", conversationId)
                    .findInBackground(new AVIMConversationQueryCallback() {

                        private void executeCallBack(AVIMConversation avimConversation, AVIMException e) {
                            if (callback != null) {
                                if (e != null) {
                                    e.printStackTrace();
                                }
                                List<AVIMConversation> conversations = null;
                                if (avimConversation != null) {
                                    conversations = new ArrayList<>();
                                    conversations.add(avimConversation);
                                }
                                if (conversations != null) {
                                    callback.done(conversations, e);
                                } else {
                                    callback.done(null, e);
                                }
                            }
                        }

                        @Override
                        public void done(List<AVIMConversation> list, AVIMException e) {
                            if (e != null) {
                                executeCallBack(null, e);
                            } else {
                                AVIMConversation conversation = list.get(0);
                                if (conversation != null) {
                                    AVIMMessageOption option = new AVIMMessageOption();
                                    option.setReceipt(true);
                                    conversation.sendMessage(message, option, new AVIMConversationCallback() {
                                        @Override
                                        public void done(AVIMException e) {
                                            if (e != null) {
                                                executeCallBack(null, e);
                                            } else {
                                                executeCallBack(conversation, null);
                                            }
                                        }
                                    });
                                } else {
                                    executeCallBack(null, new AVIMException(-1, "can not find conversation"));
                                }
                            }
                        }
                    });
        });
    }
}
