package orangelab.thirdparty.leancloud.chatkit.model;

/**
 * Author:1727
 * 2017/3/31
 */

public class UserInfoResult {

    public String id;
    public String name;
    public boolean is_friend;
    public int sex;
    public String image;
    public UserInfoGameResult game;

    public static class UserInfoGameResult {
        public int win;
        public int lose;
        public int experience;
        public int level;

        @Override
        public String toString() {
            return "UserInfoGameResult{" +
                    "win=" + win +
                    ", lose=" + lose +
                    ", experience=" + experience +
                    ", level=" + level +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UserInfoResult{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", is_friend=" + is_friend +
                ", sex=" + sex +
                ", image='" + image + '\'' +
                ", game=" + game +
                '}';
    }
}
