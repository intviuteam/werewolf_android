package orangelab.thirdparty.leancloud.chatkit.handler;

import com.avos.avoscloud.im.v2.AVIMTypedMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/11/27 上午11:21
 * Mystery
 */

public class LCIMMessageInterceptor {

    public interface Interceptor {
        boolean intercept(String conversationId, AVIMTypedMessage message);
    }

    private static List<Interceptor> mInterceptors = null;

    public static void Register(Interceptor interceptor) {
        if (mInterceptors == null) {
            mInterceptors = new ArrayList<>();
        }
        mInterceptors.add(interceptor);
    }

    public static void UnRegister(Interceptor interceptor) {
        if (mInterceptors != null) {
            mInterceptors.remove(interceptor);
        }
    }

    public static boolean TravelInterceptor(String conversationId, AVIMTypedMessage message) {
        if (mInterceptors != null) {
            boolean result = false;
            for (Interceptor interceptor : mInterceptors) {
                result = interceptor.intercept(conversationId, message);
            }
            return result;
        }
        return true;
    }

}
