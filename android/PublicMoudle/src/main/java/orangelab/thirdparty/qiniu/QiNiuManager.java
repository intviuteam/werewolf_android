package orangelab.thirdparty.qiniu;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/10/27 下午7:39
 * Mystery
 */

public enum QiNiuManager {

    QinNiu;

    private UploadManager mUploadManager;

    private QiNiuManager() {
        mUploadManager = new UploadManager();
    }

    private UploadManager getUploadManager() {
        return mUploadManager;
    }

    public static void UpLoadImage(String filePath, String key, String token, UpCompletionHandler completionHandler,
                                   final UploadOptions options) {

        QinNiu.getUploadManager().put(filePath, key, token, completionHandler, options);
    }

}
