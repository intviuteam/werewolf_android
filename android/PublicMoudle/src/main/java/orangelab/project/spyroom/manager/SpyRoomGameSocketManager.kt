package orangelab.project.spyroom.manager

import android.text.TextUtils
import android.view.MotionEvent
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.MainApplication
import orangelab.project.common.engine.SocketEngine
import orangelab.project.common.event.SocketEvent
import orangelab.project.common.event.ViewEvent
import orangelab.project.spyroom.event.SpyRoomEvent
import org.greenrobot.eventbus.EventBus

object SpyRoomGameSocketManager : Keepable, Destroyable {

    private const val TAG = "SpyRoomGameSocketManager"

    var isConnected: Boolean = true

    /**
     * 启动监听
     */
    fun start() {
        initGameSocketConnectedStateListener()
    }

    /**
     * 关闭游戏socket
     */
    fun shutDown(engineToken: String) {
        MainApplication.getInstance().socketEngineHelper.releaseEngine(engineToken)
        isConnected = false
        destroy()
    }

    /**
     * 启动游戏socket连接状态监听器
     */
    private fun initGameSocketConnectedStateListener() {
        RxToolKit.Build(this, SocketEvent.SocketConnectStateEvent::class.java)
                .actionFilter { value -> TextUtils.equals(value.intent, SocketEngine.SPY_ROOM) }
                .action { socketConnectStateEvent ->
                    when (socketConnectStateEvent.action) {
                        SocketEvent.SOCKET_CONNECT -> {
                            if (!isConnected) {
                                //重新连接
                                EventBus.getDefault().post(SpyRoomEvent.GameSocketReconnectEvent(true))
                            }
                            PLog.e(TAG, "game socket connect")
                            isConnected = true
                        }
                        SocketEvent.SOCKET_DISCONNECT -> {
                            if (isConnected) {
                                //断开连接
                                EventBus.getDefault().post(SpyRoomEvent.GameSocketReconnectEvent(false))
                                //断开语音通道
                                RxToolKit.Emit(ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP))
                            }
                            PLog.e(TAG, "game socket disconnect")
                            isConnected = false
                        }
                    }
                }.register()
    }

    override fun destroy() {
        RxToolKit.UnRegister(this)
        isConnected = true
    }

}