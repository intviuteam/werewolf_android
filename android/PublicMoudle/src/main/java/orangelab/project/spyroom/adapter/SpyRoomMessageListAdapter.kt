package orangelab.project.spyroom.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.view.ScreenUtils
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.model.SystemMessageItem
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.voice.ds.LimitList
import orangelab.project.voice.manager.GameScoreReflection
import kotlinx.android.synthetic.main.item_lobby_msg.view.*
import orangelab.project.common.effect.msgdecor.MsgDecorToolKit


class SpyRoomMessageListAdapter(private var context: Context, private var listMsg: RecyclerView)
    : RecyclerView.Adapter<SpyRoomMessageListAdapter.MyViewHolder>() {

    companion object {
        val PADDING = ScreenUtils.dip2px(4f)
    }

    private var messageList: LimitList<SystemMessageItem> = LimitList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(context).inflate(R.layout.item_spy_room_msg, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return messageList.size()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val message = messageList.get(position)
        when (message.type) {
            SystemMessageItem.TYPE_SYSTEM -> {
                holder.itemView.ll_member_tag.visibility = View.GONE
                holder.itemView.ll_score_tag.visibility = View.GONE
                holder.itemView.ll_spy_member_tag.visibility = View.GONE
                holder.itemView.ll_system_tag.visibility = View.VISIBLE
                holder.itemView.tv_item_msg.visibility = View.VISIBLE
                holder.itemView.tv_item_system_msg.text = message.msg
                holder.itemView.tv_item_system_msg.setTextColor(message.color)
            }
            SystemMessageItem.TYPE_MEMBER -> {
                /**
                 * 玩家信息
                 */
                holder.itemView.ll_system_tag.visibility = View.GONE
                holder.itemView.ll_score_tag.visibility = View.GONE
                holder.itemView.ll_spy_member_tag.visibility = View.GONE
                holder.itemView.ll_member_tag.visibility = View.VISIBLE
                holder.itemView.ll_msg_container.visibility = View.VISIBLE

                holder.itemView.tv_item_msg.visibility = View.VISIBLE
                holder.itemView.tv_item_name.text = message.name
                holder.itemView.tv_item_name.handleVipInfo(message.spyUser?.vipInfo)

                holder.itemView.tv_item_msg.text = message.msg
                holder.itemView.tv_item_msg.setTextColor(message.color)

                holder.itemView.tv_item_msg.setPadding(PADDING, PADDING, PADDING, PADDING)
                holder.itemView.tv_item_msg.setBackgroundResource(R.drawable.bg_msg)

                holder.itemView.iv_item_position.setImageResource(findIdByPosition(message.number))

                var isGiftMessage = false
                if (!TextUtils.isEmpty(message.exhibitionType)) {
                    if (EffectsManager.SupportThisGiftType(message.exhibitionType)) {
                        holder.itemView.iv_item_gift.visibility = View.VISIBLE
                        EffectsManager.FillGiftImageView(message.exhibitionType, holder.itemView.iv_item_gift)
                        isGiftMessage = true
                    } else {
                        holder.itemView.iv_item_gift.visibility = View.INVISIBLE
                        isGiftMessage = true
                    }
                } else {
                    holder.itemView.iv_item_gift.visibility = View.GONE
                    isGiftMessage = false
                }

                /**
                 * 设置聊天框
                 */
                if (!isGiftMessage) {
                    MsgDecorToolKit.setNinePatchForViewBackGround(
                            holder.itemView.tv_item_msg, holder.itemView.tv_item_msg, EffectsManager.GetMsgDecorItem(message.spyUser?.msgDecor?.message_box_type)?.animate
                    )
                }
            }
            SystemMessageItem.TYPE_SMALL_GAME -> {
                holder.itemView.ll_system_tag.visibility = View.GONE
                holder.itemView.ll_msg_container.visibility = View.GONE
                holder.itemView.ll_spy_member_tag.visibility = View.GONE
                holder.itemView.ll_member_tag.visibility = View.VISIBLE
                holder.itemView.ll_score_tag.visibility = View.VISIBLE
                holder.itemView.tv_item_msg.visibility = View.GONE
                holder.itemView.tv_item_name.text = message.name
                holder.itemView.iv_item_position.setImageResource(findIdByPosition(message.number))
                holder.itemView.iv_item_score.setImageResource(GameScoreReflection.scoreToImageId(message.score))
            }
            SystemMessageItem.TYPE_SPY_MSG -> {
                holder.itemView.ll_msg_container.visibility = View.GONE
                holder.itemView.ll_member_tag.visibility = View.GONE
                holder.itemView.ll_score_tag.visibility = View.GONE
                holder.itemView.tv_item_msg.visibility = View.GONE
                holder.itemView.ll_system_tag.visibility = View.GONE

                holder.itemView.ll_spy_member_tag.visibility = View.VISIBLE
                holder.itemView.tv_spy_item_name.text = message.name
                holder.itemView.tv_spy_item_msg.visibility = View.VISIBLE
                holder.itemView.tv_spy_item_msg.gravity = message.gravity
                holder.itemView.tv_spy_item_msg.text = message.msg
                holder.itemView.tv_spy_item_msg.setTextColor(message.color)

            }
            else -> {

            }
        }

    }

    /**
     * 根据座位号找到位置标号
     */
    private fun findIdByPosition(position: Int): Int {
        if (position >= 1 && position <= SpyRoomConfig.maxChair) {
            try {
                return R.mipmap::class.java.getDeclaredField("voice_tag_$position").getInt(null)
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            }

        }

        return R.mipmap.voice_tag_other
    }

    /**
     * 刷新消息列表
     */
    fun refreshMessageList(latestMessage: SystemMessageItem?, scrollToBottom: Boolean) {
        if (latestMessage != null) {

            messageList.add(latestMessage)
            notifyDataSetChanged()
            if (scrollToBottom) {
                listMsg.scrollToPosition(itemCount - 1)
            }

//            //已经在底部就刷新底部
//            if (isScrollToBottom()) {
//                listMsg.scrollToPosition(itemCount - 1)
//            }
//
//            //自己发的消息就滚动到底部
//            val user = latestMessage.user
//            val id = if (user == null) "" else user.id
//            val myId = GlobalUserState.getGlobalState().userId
//            if (!TextUtils.isEmpty(myId) && TextUtils.equals(myId, id)) {
//                listMsg.scrollToPosition(itemCount - 1)
//            }
        }

    }


    /**
     * 最小化回来后复现之前的消息
     */
    fun restoreMessage(cMessageList: Collection<SystemMessageItem>) {
        messageList.clear()
        messageList.addAll(cMessageList)
        notifyDataSetChanged()
        listMsg.scrollToPosition(itemCount - 1)
    }

    /**
     * 是否滑动到底部
     */
    private fun isScrollToBottom(): Boolean {
        val layoutManager = listMsg.layoutManager as LinearLayoutManager
        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val state = listMsg.scrollState
        return visibleItemCount > 0 && lastVisibleItemPosition == totalItemCount - 1 && state == RecyclerView.SCROLL_STATE_IDLE
    }

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)
}