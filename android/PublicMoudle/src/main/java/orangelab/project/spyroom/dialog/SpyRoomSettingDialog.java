package orangelab.project.spyroom.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.spyroom.SpyRoomConfig;
import orangelab.project.spyroom.api.SpyRoomSocketHelper;

/**
 * Created by lloydfinch on 14/10/2017.
 */

public class SpyRoomSettingDialog extends Dialog {

    private static final String TAG = "RoomPasswordDialog";

    private TextView tvSwitch;
    private CheckBox passwordSwitch;
    private EditText editText;
    private View passwordContainer;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TextView text4;
    private TextView tvPswTip;

    private RadioButton rbSpyNum1;
    private RadioButton rbSpyNum2;
    private RadioButton rbSpyCanBoom;
    private RadioButton rbSpyNoBoom;

    private View btnSure;
    private View btnCancel;
    private View ivClose;

    private int spyNumber = 1;
    private boolean canBoom = false;


    private Context mContext;
    private String password;

    public SpyRoomSettingDialog(Context context) {
        super(context, R.style.radius_dialog);
        mContext = context;
        this.password = SpyRoomConfig.INSTANCE.getPassword();
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_spy_room_setting, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        editText = (EditText) contentView.findViewById(R.id.editText);
        passwordContainer = contentView.findViewById(R.id.password_container);
        passwordSwitch = (CheckBox) contentView.findViewById(R.id.password_switch);
        text1 = (TextView) contentView.findViewById(R.id.text1);
        text2 = (TextView) contentView.findViewById(R.id.text2);
        text3 = (TextView) contentView.findViewById(R.id.text3);
        text4 = (TextView) contentView.findViewById(R.id.text4);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        tvSwitch = (TextView) contentView.findViewById(R.id.tv_switch);
        tvPswTip = (TextView) contentView.findViewById(R.id.tv_psw_tip);

        rbSpyNum1 = (RadioButton) contentView.findViewById(R.id.rb_spy_num_1);
        rbSpyNum2 = (RadioButton) contentView.findViewById(R.id.rb_spy_num_2);
        rbSpyCanBoom = (RadioButton) contentView.findViewById(R.id.rb_spy_can_boom);
        rbSpyNoBoom = (RadioButton) contentView.findViewById(R.id.rb_spy_no_boom);

        ivClose = contentView.findViewById(R.id.iv_spy_room_close);

        if (SpyRoomConfig.INSTANCE.getSpyNumber() == 2) {
            twoSpy();
        } else {
            oneSpy();
        }
        if (SpyRoomConfig.INSTANCE.getCanGuessWord()) {
            canBoom();
        } else {
            noBoom();
        }

        initEditText(editText);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);

        //不可触摸外部取消¬
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            String password = editText.getText().toString();
            if (!TextUtils.equals(password, SpyRoomConfig.INSTANCE.getPassword())) {
                RoomSocketEngineHelper.sendChangePassword(password);
            }
            if (spyNumber != SpyRoomConfig.INSTANCE.getSpyNumber() || canBoom != SpyRoomConfig.INSTANCE.getCanGuessWord()) {
                SpyRoomSocketHelper.INSTANCE.sendSpySettingUpdate(spyNumber == 2, spyNumber, canBoom);
            }
            hideKeyboard();
            dismiss();
        });

        btnCancel.setOnClickListener(v -> dismiss());

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null) {
                    return;
                }
                String input = s.toString();
                Editable editable = editText.getText();
                int len = input.length();
                if (len > 4) {
                    int selEndIndex = Selection.getSelectionEnd(editable);
                    String str = editable.toString();
                    input = str.substring(0, 4);
                    editText.setText(input);
                    editable = editText.getText();
                    int newLen = editable.length();
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length();
                    }
                    Selection.setSelection(editable, selEndIndex);

                }
                handleInput(input);
                PLog.i(TAG, "onTextChanged: " + input.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                PLog.i(TAG, "afterTextChanged: ");
            }
        });
        passwordSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                openSwitch();
            } else {
                closeSwitch();
            }
            handleInput("");
            editText.setText("");
        });
        passwordContainer.setOnClickListener(v -> {
            editText.requestFocus();
            showKeyboard();
        });
        if (TextUtils.isEmpty(password)) {
            closeSwitch();
            editText.setText("");
            handleInput("");
        } else {
            openSwitch();
            handleInput(password);
            editText.setText(password);
        }
        editText.requestFocus();

        rbSpyNum1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                oneSpy();
            }
        });

        rbSpyNum2.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                twoSpy();
            }
        });

        rbSpyCanBoom.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                canBoom();
            }
        });

        rbSpyNoBoom.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                noBoom();
            }
        });

        ivClose.setOnClickListener(v -> dismiss());

    }

    private void openSwitch() {
        passwordSwitch.setChecked(true);
        //passwordSwitch.setBackgroundResource(R.mipmap.ic_voice_switch_on);
        tvSwitch.setText(R.string.str_open_password);
        passwordContainer.setVisibility(View.VISIBLE);
        tvPswTip.setVisibility(View.VISIBLE);
    }

    private void closeSwitch() {
        passwordSwitch.setChecked(false);
        //passwordSwitch.setBackgroundResource(R.mipmap.ic_voice_switch_off);
        tvSwitch.setText(R.string.str_close_psw);
        passwordContainer.setVisibility(View.GONE);
        tvPswTip.setVisibility(View.INVISIBLE);
    }

    private void clearInput() {
        text1.setText("");
        text2.setText("");
        text3.setText("");
        text4.setText("");
    }

    private void handleInputInner(String input) {
        try {
            text1.setText(input.substring(0, 1));
            text2.setText(input.substring(1, 2));
            text3.setText(input.substring(2, 3));
            text4.setText(input.substring(3, 4));
        } catch (Exception e) {
            //password length < 4
            e.printStackTrace();
        }
    }

    private void handleInput(String input) {
        clearInput();
        if (TextUtils.isEmpty(input)) {
        } else {
            if (input.length() > 4) {
                String result = input.substring(0, 4);
                handleInputInner(result);
            } else {
                handleInputInner(input);
            }

        }
    }

    private void initEditText(EditText editText) {
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

        });
        editText.setLongClickable(false);
    }

    private void showKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {
            PLog.e(TAG, "hide keyboard without not open it!");
            e.printStackTrace();
        }
    }

    private void oneSpy() {
        rbSpyNum1.setChecked(true);
        rbSpyNum2.setChecked(false);
        spyNumber = 1;
    }

    private void twoSpy() {
        rbSpyNum1.setChecked(false);
        rbSpyNum2.setChecked(true);
        spyNumber = 2;
    }

    private void canBoom() {
        rbSpyCanBoom.setChecked(true);
        rbSpyNoBoom.setChecked(false);
        canBoom = true;
    }

    private void noBoom() {
        rbSpyCanBoom.setChecked(false);
        rbSpyNoBoom.setChecked(true);
        canBoom = false;
    }
}
