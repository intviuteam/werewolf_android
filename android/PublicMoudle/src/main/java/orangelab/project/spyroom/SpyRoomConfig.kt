package orangelab.project.spyroom

import com.androidtoolkit.PLog
import com.toolkit.action.Keepable

/**
 * 卧底房配置类
 */
object SpyRoomConfig : Keepable {

    var type: String = ""
    var roomId: String = ""
    var title: String = ""                    //房间话题
    var password: String = ""
    var background: String = ""
    var maxChair: Int = 6                     //最大座位数(可见的)
    var maxRealChair: Int = 6                 //最大座位数(初始化的)

    var isFromSW: Boolean = false             //是否从小窗进入
    var isSupportSw: Boolean = false          //是否支持小窗

    var hasGhost: Boolean = false             //是否设置卧底为幽灵
    var canGuessWord: Boolean = true          //默认可以爆词
    var moreSpy: Boolean = false              //是否加一个卧底
    var canInsertMic: Boolean = false         //是否可插麦
    var isGaming: Boolean = false             //是否在游戏中
    var configing: Boolean = false            //是否在设置中
    var configed: Boolean = false             //是否设置完成
    var firstWord: String = ""                //第一个词语
    var secondWord: String = ""               //第二个词语
    var goodNumber: Int = 0                   //好人数
    var spyNumber: Int = 1                    //卧底数
    var minChairPosition = 0                  //最小座位标号
    var masterPosition = 0                    //房主位置
    var voteDuration = 0                      //投票时长
    var startDuration = 0                     //开始时长
    var gameLen: Int = 0                      //游戏阶段，第几轮
    var speechPosition: Int = 0               //当前轮到谁发言，实际位置需要-1
    var lenShowTime: Long = 3000              //当前阶段描述的显示时间

    fun clearConfig() {
        PLog.e("SpyRoomConfig", "start clear room config...")
        type = ""
        roomId = ""
        title = ""
        password = ""
        background = ""
        maxChair = 6
        maxRealChair = 6

        isFromSW = false
        isSupportSw = false

        clearGameConfig()
    }

    fun clearGameConfig() {
        hasGhost = false
        canGuessWord = true
        moreSpy = false
        canInsertMic = false
        isGaming = false
        configing = false
        configed = false
        firstWord = ""
        secondWord = ""
        goodNumber = 0
        spyNumber = 1
        minChairPosition = 0
        masterPosition = 0
        voteDuration = 0
        startDuration = 0
        gameLen = 0
        speechPosition = 0
    }
}
