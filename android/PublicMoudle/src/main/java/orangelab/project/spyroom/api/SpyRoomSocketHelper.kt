package orangelab.project.spyroom.api

import com.androidtoolkit.PLog
import com.networktoolkit.transport.Constant
import orangelab.project.MainApplication
import orangelab.project.common.engine.task.SocketCallBack
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.voice.constants.VoiceConstants
import org.json.JSONException
import org.json.JSONObject

/**
 * 谁是卧底房消息发送器
 */
object SpyRoomSocketHelper {

    /**
     * 发送socket消息
     */
    fun nativeRequestMessage(type: String, payload: JSONObject) {
        nativeRequestMessage(type, payload, null, null)
    }

    /**
     * 发送socket消息，带有callback
     */
    fun nativeRequestMessage(type: String, payload: JSONObject?, success: SocketCallBack?, failed: SocketCallBack?) {
        MainApplication.getInstance().socketEngineHelper.nativeRequestMessage(type, payload, success, failed)
    }

    /**
     * "谁是卧底"开始设置
     */
    fun sendSpySettingStart() {
        val jsonObject = JSONObject()
        nativeRequestMessage(SpyRoomConstants.SPY_START_CONFIG, jsonObject)
    }

    /**
     * "谁是卧底"结束设置
     */
    fun sendSpySettingStop() {
        val jsonObject = JSONObject()
        nativeRequestMessage(SpyRoomConstants.SPY_STOP_CONFIG, jsonObject)
    }

    /**
     * "谁是卧底"更新设置
     */
    fun sendSpySettingUpdate(addSpy: Boolean, setSpyToGhost: Boolean, canInsertMic: Boolean, canGuessWord: Boolean, firstWord: String, secondWord: String) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.SPY_MORE_UNDERCOVER, addSpy)
            jsonObject.put(SpyRoomConstants.SPY_HAS_GHOST, setSpyToGhost)
            jsonObject.put(SpyRoomConstants.SPY_CAN_CUT_SPEAKER, canInsertMic)
            jsonObject.put(SpyRoomConstants.SPY_CAN_GUESS, canGuessWord)
            jsonObject.put(SpyRoomConstants.SPY_FIRST_WORD, firstWord)
            jsonObject.put(SpyRoomConstants.SPY_SECOND_WORD, secondWord)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_UPDATE_CONFIG, jsonObject)
    }

    /**
     * "谁是卧底"获取系统词库
     */
    fun sendSpyGetSystemWords() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.TYPE, SpyRoomConstants.SPY_SYSTEM_WORDS)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_GET_WORDS, jsonObject)
    }

    /**
     * "谁是卧底"获取系统词库
     */
    fun sendSpyGetCustomWords() {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.TYPE, SpyRoomConstants.SPY_CUSTOM_WORDS)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_GET_WORDS, jsonObject)
    }

    /**
     * "谁是卧底"游戏开始
     */
    fun sendSpyGameStart(successCallback: SocketCallBack, failedCallback: SocketCallBack) {
        nativeRequestMessage(SpyRoomConstants.SPY_GAME_START, null, successCallback, failedCallback)
    }

    /**
     * "谁是卧底"准备
     */
    fun sendSpyGamePrepare() {
        val jsonObject = JSONObject()
        nativeRequestMessage(SpyRoomConstants.SPY_GAME_PREPARE, jsonObject)
    }

    /**
     * "谁是卧底"取消准备
     */
    fun sendSpyGameUnPrepare() {
        val jsonObject = JSONObject()
        nativeRequestMessage(SpyRoomConstants.SPY_GAME_UNPREPARE, jsonObject)
    }

    /**
     * "谁是卧底"投票
     */
    fun sendSpyGameVote(position: Int) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.POSITION, position)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_GAME_VOTE, jsonObject)
    }

    /**
     * "谁是卧底"猜词
     */
    fun sendSpyGuessWord(word: String) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.SPY_GAME_WORD, word)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_GAME_GUESS_WORD, jsonObject)
    }

    /**
     * 评论题目
     */
    fun sendTopicComment(isGoodTopic: Boolean) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.SPY_GAME_TOPIC_ATTITUDE, isGoodTopic)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_GAME_TOPIC_COMMENT, jsonObject)
    }

    /**
     * 结束发言
     */
    fun sendEndSpeech() {
        val jsonObject = JSONObject()
        nativeRequestMessage(SpyRoomConstants.SPY_GAME_END_SPEECH, jsonObject)
    }

    /**
     * 去观战
     */
    fun sendToObserve(position: Int) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(Constant.POSITION, position)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(Constant.GAME_DOWN_SEAT, jsonObject)
    }

    /**
     * 上座成为玩家
     */
    fun sendGameUpSeat() {

        val jsonObject = JSONObject()
        nativeRequestMessage(Constant.GAME_UP_SEAT, jsonObject)
    }

    /**
     * 更改麦位数
     */
    fun changeSeatNumber(count: Int) {
        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.COUNT, count)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.UPDATE_SEAT_COUNT, jsonObject)
    }

    /**
     * 更改卧底设置
     */
    fun sendSpySettingUpdate(addSpy: Boolean, spyNum: Int, canGuessWord: Boolean) {

        val jsonObject = JSONObject()
        try {
            jsonObject.put(SpyRoomConstants.SPY_MORE_UNDERCOVER, addSpy)
            jsonObject.put(SpyRoomConstants.SPY_CAN_GUESS, canGuessWord)
            jsonObject.put(SpyRoomConstants.SPY_COUNT, spyNum)


            //下面四个属性不需要
//            jsonObject.put(SpyRoomConstants.SPY_HAS_GHOST, false)
//            jsonObject.put(SpyRoomConstants.SPY_CAN_CUT_SPEAKER, false)
//            jsonObject.put(SpyRoomConstants.SPY_FIRST_WORD, "")
//            jsonObject.put(SpyRoomConstants.SPY_SECOND_WORD, "")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(SpyRoomConstants.SPY_UPDATE_CONFIG, jsonObject)
    }
}