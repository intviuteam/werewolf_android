package orangelab.project.spyroom.model

import com.androidtoolkit.transport.Transportable
import com.toolkit.action.Keepable
import orangelab.project.common.model.EnterRoomResult

/**
 * 用于启动谁是卧底房的消息实体
 */
class SpyRoomBridgeBean : Keepable, Transportable {

    var gameType: String
    var password: String
    var result: EnterRoomResult
    var engineToken: String

    constructor(gameType: String, password: String, engineToken: String, result: EnterRoomResult) {
        this.gameType = gameType
        this.password = password
        this.engineToken = engineToken
        this.result = result
    }
}