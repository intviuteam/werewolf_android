package orangelab.project.spyroom.dialog

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.UIActuator.postSafely
import com.androidtoolkit.view.ScreenUtils
import com.datasource.GlobalUserState
import kotlinx.android.synthetic.main.layout_dialog_spy_room_player.*
import kotlinx.android.synthetic.main.layout_dialog_spy_room_player.view.*
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.activity.PersonalInfoActivity
import orangelab.project.common.dialog.ReportDialog
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.exhibition.ExhibitionsActivity
import orangelab.project.common.manager.ApiManager
import orangelab.project.common.manager.BlackListManager
import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.union.UnifiedBridgeHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.common.view.HeadDecorView
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.model.SpyRoomPlayer

/**
 * 谁是卧底房间点击座位弹出框
 */
class SpyRoomPlayerDialog(context: Context, player: SpyRoomPlayer) : SafeDialog(context, R.style.radius_dialog) {


    private val TAG = "SpyRoomPlayerDialog"

    private var mContext: Context = context
    private var mUser: SpyRoomPlayer = player

    init {
        init()

        loadUserInfo()
    }

    private fun init() {

        //initView
        val contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_spy_room_player, null)
        contentView.isFocusable = true
        contentView.isFocusableInTouchMode = true
        contentView.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss()
                return@setOnKeyListener true
            }
            false
        }
        setContentView(contentView)

        //initWindow
        val dialogMaxWidth = ScreenUtils.dip2px(360f)
        val params = window!!
                .attributes
        val dur = (ScreenUtils.getScreenWidth() * 0.25f).toInt()
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth
        } else {
            params.width = (ScreenUtils.getScreenWidth() * 0.95f).toInt()
        }
        window!!.attributes = params

        initChildView()
    }

    /**
     * 初始化子视图
     */
    private fun initChildView() {

        //点击自己，屏蔽：加好友、举报、拉黑和踢出
        if (SpyRoomHelper.isSelf(mUser.userId)) {
            this.ll_spy_room_top_btn_container.visibility = View.GONE
            this.btn_spy_room_kick_out.visibility = View.GONE
        } else {
            //点击别人，是房主才能看到踢出
            this.ll_spy_room_top_btn_container.visibility = View.VISIBLE
            if (SpyRoomHelper.isMaster()) {
                this.btn_spy_room_kick_out.visibility = View.VISIBLE
            } else {
                this.btn_spy_room_kick_out.visibility = View.GONE
            }
        }

        if (Utils.checkUserIdIsTourist(mUser.userId) || Utils.checkUserIdIsTourist(PersonalDataHelper.getPersonalData().userId)) {
            this.btn_spy_room_block.visibility = View.INVISIBLE
        }

        //进入个人详情页面
        this.rl_spy_room_player_info_container.setOnClickListener {
            if (Utils.checkUserIdIsTourist(mUser.userId)) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_info))
            } else {
                PersonalInfoActivity.Launch(mContext, mUser.userId)
            }
            dismiss()
        }

        //加好友
        this.btn_spy_room_add_friend.setOnClickListener {
            ApiManager.AddFriendApi(this.mUser.userId, { _ -> postSafely { dismiss() } },
                    { failure -> postSafely { ToastToolKit.showShort(failure) } })
            { error -> postSafely { ToastToolKit.showShort(error) } }
        }

        //送礼
        this.btn_spy_room_send_gift.setOnClickListener {
            when {
                GlobalUserState.getGlobalState().isTourist -> ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift))
                Utils.checkUserIdIsTourist(mUser.userId) -> ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift_2))
                else -> ExhibitionsActivity.startActivityFromGame(mContext, mUser.userId, mUser.userName, mUser.sex, mUser.headImageUrl, mUser.position)
            }
            dismiss()
        }

        //踢出
        this.btn_spy_room_kick_out.setOnClickListener {

            fun canKickOut(): Boolean {
                return !SpyRoomConfig.isGaming || !SpyRoomHelper.isPlayer(mUser.position)
            }

            if (canKickOut()) {
                RoomSocketEngineHelper.kickOut(mUser.position, mUser.userId)
            } else {
                //游戏中、玩家，不能被踢出
            }
            dismiss()
        }

        //拉黑
        this.btn_spy_room_block.setOnClickListener {
            try {
                fun canBlock(): Boolean {
                    return !SpyRoomHelper.isSelf(mUser.userId) &&
                            !mUser.isBlock &&
                            !Utils.checkUserIdIsTourist(mUser.userId)
                }

                if (canBlock()) {
                    block()
                }
                //dismiss()
            } catch (e: Exception) {
                ToastToolKit.showShort(R.string.str_spy_room_block_tip_message)
                e.printStackTrace()
            }
        }

        //举报
        this.btn_spy_room_report.setOnClickListener {
            val reportDialog = ReportDialog(context, mUser.userId, mUser.userName)
            reportDialog.show()
            dismiss()
        }

        this.iv_spy_room_close.setOnClickListener {
            dismiss()
        }
    }

    /**
     * 刷新视图，填充数据
     */
    @SuppressLint("SetTextI18n")
    private fun refreshView(userInfo: UserInfoResult?) {
        if (userInfo != null) {

            mUser.isFriend = userInfo.is_friend
            mUser.isBlock = userInfo.isBlock

            if (!TextUtils.isEmpty(userInfo.image)) {
                PicassoUtils.loadImageByDefault(mContext,
                        userInfo.image,
                        this.iv_spy_room_player_head.realHeadImage,
                        R.mipmap.default_head)
            }
            if (userInfo.uid != null) {
                this.tv_user_id.text = "ID:${userInfo.uid}"
            } else {
                this.tv_user_id.text = ""
            }
            this.iv_spy_room_player_head.setUserSex(userInfo.sex)
            this.iv_spy_room_player_head.setHeadDecorate(userInfo.avatar_box)
            this.tv_spy_room_player_name.text = userInfo.name
            this.tv_spy_room_player_name.handleVipInfo(userInfo.vipInfo, Color.BLACK)

            this.tv_spy_room_player_popularity.text = MessageUtils.getString(R.string.user_popular_text, Integer.toString(userInfo.popular))
            if (userInfo.undercover != null) {
                this.tv_spy_room_good_win_num.text = userInfo.undercover.win.toString()
                this.tv_spy_room_spy_win_num.text = userInfo.undercover.lose.toString()
            }

            this.ll_spy_room_top_btn_container.btn_spy_room_add_friend.visibility = if (userInfo.is_friend) View.GONE else View.VISIBLE

            try {
                this.ll_spy_room_top_btn_container.btn_spy_room_block.visibility =
                        if (userInfo.isBlock || Utils.checkUserIdIsTourist(userInfo.id) || Utils.checkUserIdIsTourist(PersonalDataHelper.getPersonalData().userId)) View.GONE
                        else View.VISIBLE
            } catch (e: Exception) {
                this.ll_spy_room_top_btn_container.btn_spy_room_block.visibility = View.GONE
            }
        }
    }

    /**
     * 加载玩家信息
     */
    private fun loadUserInfo() {
        if (!TextUtils.isEmpty(mUser.userId)) {
            try {
                ApiManager.GetUserInfoApi(mUser.userId)
                { userInfoResult, _ -> runSafely { refreshView(userInfoResult) } }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 拉黑
     */
    private fun block() {
        try {
            ApiManager.AddToBlackApi(mUser.userId) { result, e ->
                runSafely {
                    if (e == null) {
                        BlackListManager.Add(mUser.userId)
                        UnifiedBridgeHelper.SendBlackListAddAction(mUser.userId)
                    }
                    PLog.e(TAG, "result1 = $result")
                    ToastToolKit.showShort(result)
                    PLog.e(TAG, "result2 = $result")

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun release() {

    }

}