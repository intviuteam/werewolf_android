package orangelab.project.spyroom.action

import com.toolkit.action.Keepable

interface Refreshable : Keepable {

    /**
     * 刷新数据
     */
    fun refresh(data: Refreshable)

    /**
     * 恢复数据
     */
    fun restore(data: Refreshable)

    /**
     * 重置数据
     */
    fun reset()

    /**
     * 清除数据
     */
    fun clear()
}