package orangelab.project.spyroom.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.PicassoUtils
import orangelab.project.MainApplication
import orangelab.project.spyroom.model.SpyRoomPlayer
import java.util.ArrayList

/**
 * 谁是卧底在线列表
 */
class SpyRoomOnLineAdapter : RecyclerView.Adapter<SpyRoomOnLineAdapter.OnLineMembersViewHolder> {

    private var memberList: List<SpyRoomPlayer> = ArrayList()
    private val mContext: Context

    private var onLineMemberClickedListener: OnOnLineMemberClickedListener? = null

    constructor(memberList: List<SpyRoomPlayer>, mContext: Context) : super() {
        this.memberList = memberList
        this.mContext = mContext
    }

    fun setOnLineMemberClickedListener(onLineMemberClickedListener: OnOnLineMemberClickedListener) {
        this.onLineMemberClickedListener = onLineMemberClickedListener
    }

    fun updateMemberList(memberList: List<SpyRoomPlayer>) {
        this.memberList = memberList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnLineMembersViewHolder {
        val itemView = LayoutInflater.from(mContext).inflate(R.layout.item_game_online_member, null)
        return OnLineMembersViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: OnLineMembersViewHolder, position: Int) {
        val member = memberList[position]
        if (member != null) {
            PicassoUtils.loadImageByDefault(MainApplication.getInstance().applicationContext, member.headImageUrl, holder.ivHead, R.mipmap.default_head)
            holder.tvName.text = member.userName
            holder.tvMotto.text = member.signature
            holder.tvPosition.text = (member.position + 1).toString()
            if (member.isObserver) {
                holder.tvObserver.visibility = View.VISIBLE
                holder.tvUpSeat.visibility = View.GONE
            } else {
                holder.tvObserver.visibility = View.GONE
                holder.tvUpSeat.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int {
        return memberList.size
    }

    override fun getItemId(position: Int): Long {
        if (position > memberList.size) {
            return if (memberList.isNotEmpty()) {
                (memberList.size - 1).toLong()
            } else {
                0
            }
        }
        return position.toLong()
    }

    inner class OnLineMembersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvName: TextView
        var ivHead: ImageView
        var tvPosition: TextView//位置标号
        var tvMotto: TextView//个性签名
        var tvUpSeat: View//座上
        var tvObserver: View//观战

        init {
            tvName = itemView.findViewById(R.id.tv_name) as TextView
            ivHead = itemView.findViewById(R.id.iv_head) as ImageView
            tvPosition = itemView.findViewById(R.id.tv_position) as TextView
            tvMotto = itemView.findViewById(R.id.tv_motto) as TextView
            tvUpSeat = itemView.findViewById(R.id.tv_upseat)
            tvObserver = itemView.findViewById(R.id.tv_observe)

            if (onLineMemberClickedListener != null) {
                itemView.setOnClickListener { v -> onLineMemberClickedListener?.onMemberClick(position) }
            }
        }
    }

    interface OnOnLineMemberClickedListener {
        fun onMemberClick(position: Int)
    }
}