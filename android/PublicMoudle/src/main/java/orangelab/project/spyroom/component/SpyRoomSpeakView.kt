package orangelab.project.spyroom.component

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.support.annotation.Keep
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import kotlinx.android.synthetic.main.layout_spy_room_speak_view.view.*
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.event.DebugEvent
import orangelab.project.common.event.ViewEvent
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.SpyRoomContext
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.event.SpyRoomEvent
import orangelab.project.spyroom.manager.SpyRoomAudioSocketManager
import orangelab.project.spyroom.model.SpyRoomMessenger
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 语音/消息控制器
 */
class SpyRoomSpeakView : Destroyable, Keepable {

    private val TAG = "SpyRoomSpeakView"

    private var contentView: ViewGroup
    private var spyRoomContext: SpyRoomContext
    private var isSpeakMode: Boolean = true
    private var isConnected: Boolean = false

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.contentView = contentView
        this.spyRoomContext = spyRoomContext

        init()
    }

    private fun init() {
        EventBus.getDefault().register(this)
        initKeyboardListener()
        initListener()
        switchRole()
    }

    /**
     *
     */

    fun refreshConnected(isConnected: Boolean) {
        this.isConnected = isConnected
    }

    /**
     * 接收断网事件
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SpyRoomEvent.GameSocketReconnectEvent) {
        //this is useless
    }

    /**
     * 键盘布局监听
     */
    private fun initKeyboardListener() {
        contentView.viewTreeObserver.addOnGlobalLayoutListener(
                {
                    val r = Rect()
                    contentView.getWindowVisibleDisplayFrame(r)
                    val screenHeight = contentView.rootView.height
                    val heightDifference = screenHeight - r.bottom
                    if (heightDifference > 200) {
                        contentView.btn_spy_room_send_message.visibility = View.VISIBLE
                        //contentView.et_spy_room_speak_view_input.requestFocus()

                        contentView.iv_spy_room_speak_view_switch.visibility = View.GONE
                    } else {
                        contentView.btn_spy_room_send_message.visibility = View.GONE
                        contentView.et_spy_room_speak_view_input.clearFocus()

                        contentView.iv_spy_room_speak_view_switch.visibility = View.VISIBLE
                    }
                })
    }

    private fun initListener() {
        contentView.ll_spy_room_speak_view_voice.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (canSpeak()) {
                        onTouchDownSpeaking()
                        sendStartSpeak()
                    }
                }
                MotionEvent.ACTION_UP -> {
                    onTouchUpEndSpeak()
                    sendEndSpeak()
                }
            }
            true
        }

        contentView.btn_spy_room_send_message.setOnClickListener {
            hideKeyboard()
            sendMessage()
        }
    }

    /**
     * 切换视角
     */
    private fun switchRole() {
        if (canSpeak()) {
            switchToSpeak()
        } else {
            switchToInput()
        }
    }

    /**
     * 是否可以说话
     */
    private fun canSpeak(): Boolean {

        //非游戏中，在坐上，可说话
        val condition1: Boolean = !SpyRoomConfig.isGaming && SpyRoomHelper.isPlayer()

        //游戏中，在坐上，轮到自己说话
        val condition2: Boolean = SpyRoomConfig.isGaming && SpyRoomHelper.isPlayer() && SpyRoomHelper.canSpeech()

        PLog.e(TAG, "con1 = $condition1, con2 = $condition2 , speech = ${SpyRoomHelper.canSpeech()}")

        return condition1 || condition2
    }

    /**
     * 切换到按住说话
     */
    private fun switchToSpeak() {
        if (canSpeak()) {
            contentView.ll_spy_room_speak_view_voice.visibility = View.VISIBLE
            contentView.et_spy_room_speak_view_input.visibility = View.INVISIBLE
            contentView.iv_spy_room_speak_view_switch.setImageResource(R.drawable.bg_spy_room_message)
            contentView.iv_spy_room_speak_view_switch.setOnClickListener { switchToInput() }
            isSpeakMode = true
        }
    }

    /**
     * 切换到打字
     */
    private fun switchToInput() {
        contentView.ll_spy_room_speak_view_voice.visibility = View.INVISIBLE
        contentView.et_spy_room_speak_view_input.visibility = View.VISIBLE
        contentView.iv_spy_room_speak_view_switch.setImageResource(R.drawable.bg_spy_room_radio)
        contentView.iv_spy_room_speak_view_switch.setOnClickListener {
            if (canSpeak()) {
                switchToSpeak()
            } else {
                if (!SpyRoomHelper.isPlayer()) {
                    ToastToolKit.showShort(R.string.str_spy_room_observer_can_not_speak)
                }
            }
        }

        isSpeakMode = false
        sendEndSpeak()
    }

    /**
     * 切换到按下状态
     */
    private fun onTouchDownSpeaking() {
        contentView.tv_spy_room_speak_view_voice_text.setText(R.string.str_up_end)
        contentView.tv_spy_room_speak_view_voice_text.setTextColor(Color.parseColor("#e3e3e3"))
        contentView.ll_spy_room_speak_view_voice.setBackgroundResource(R.mipmap.ic_spy_room_speak_bg_press)
        contentView.iv_spy_room_speak_icon.setImageResource(R.mipmap.ic_spy_room_speak_press)
    }

    /**
     * 切换到抬起状态
     */
    private fun onTouchUpEndSpeak() {
        contentView.tv_spy_room_speak_view_voice_text.setText(R.string.str_press_speak)
        contentView.tv_spy_room_speak_view_voice_text.setTextColor(Color.parseColor("#32ab75"))
        contentView.ll_spy_room_speak_view_voice.setBackgroundResource(R.mipmap.ic_spy_room_speak_bg_up)
        contentView.iv_spy_room_speak_icon.setImageResource(R.mipmap.ic_spy_room_speak_up)
    }

    /**
     * 发送start speak
     */
    private fun sendStartSpeak() {
        if (this.isConnected) {
            RoomSocketEngineHelper.startSpeak()
        }
    }

    /**
     * 发送end speak
     */
    private fun sendEndSpeak() {
        RxToolKit.Emit(ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP)) //结束说话前先关闭语音通道
        RoomSocketEngineHelper.endSpeak()
    }

    /**
     * 隐藏键盘
     */
    private fun hideKeyboard() {
        try {
            val imm = contentView.ll_spy_room_speak_view_voice
                    .context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(contentView.ll_spy_room_speak_view_voice.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 发送消息
     */
    private fun sendMessage() {

        val message = contentView.et_spy_room_speak_view_input.text.toString().trim()
        if (TextUtils.equals(message, "/1727")) {
            RxToolKit.Emit(DebugEvent.DeveloperShowDebugEvent())
            return
        }

        if (!TextUtils.isEmpty(message)) {
            RoomSocketEngineHelper.sendChatMessage(message)
            contentView.et_spy_room_speak_view_input.setText("")
        }
    }

    override fun destroy() {
        EventBus.getDefault().unregister(this)
        hideKeyboard()
    }

    /**
     * view观察处理器
     */
    var observer = object : SpyRoomProcessable() {

        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }


        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
            //这里再次判断
            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            if (SpyRoomHelper.isSelf(position)) {
                RxToolKit.Emit(ViewEvent.SpeakViewEvent(MotionEvent.ACTION_DOWN))
            }
            return false
        }

        override fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
            //这里再次判断
            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            if (SpyRoomHelper.isSelf(position)) {
                RxToolKit.Emit(ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP))
            }
            return false
        }

        override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {

            switchRole()
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }
    }
}