package orangelab.project.spyroom.model

import android.text.TextUtils
import com.toolkit.action.Keepable
import orangelab.project.common.model.*
import orangelab.project.spyroom.action.Refreshable
import orangelab.project.spyroom.data.SpyRoomConstants

/**
 * 玩家实体
 */
class SpyRoomPlayer : Refreshable, Keepable {

    var userId: String = ""                                 //id
    var userName: String = ""                               //名字
    var position: Int = -1                                  //位置
    var headImageUrl: String = ""                           //头像
    var sex: Int = 0                                        //性别
    var prepare: Boolean = false                            //是否准备
    var speech: Boolean = false                             //是否轮到发言
    var speaking: Boolean = false                           //是否在说话
    var isMaster: Boolean = false                           //是否是房主
    var isObserver: Boolean = false                         //是否是观战者
    var disconnected: Boolean = false                       //是否掉线
    var isBanMic: Boolean = false                           //是否被禁言
    var isOut: Boolean = false                              //是否出局
    var isFriend: Boolean = false                           //是否是自己好友
    var isBlock: Boolean = false                            //是否在黑名单中
    var signature: String = ""                              //个性签名
    var uid: String = ""                                    //uid
    var speechTime: Int = 0                                 //剩余说话时间
    var role: String = ""                                   //词语
    var role2: String = ""                                  //角色:卧底/好人


    /**
     * vip信息
     */
    var msgDecor: UserInfoResult.MessageBox? = null         //聊天框
    var headDecor: UserInfoResult.AvatarBox? = null         //头像框
    var vipInfo: UserVipInfo? = null                        //Vip信息

    override fun refresh(data: Refreshable) {

    }

    override fun restore(data: Refreshable) {
    }

    override fun reset() {
    }

    override fun clear() {
    }

    /**
     * 填充数据
     */
    fun inflateData(data: EnterRoomResult.EnterRoomUserItem): SpyRoomPlayer {
        this.userId = data.id
        this.userName = data.name
        this.position = data.position
        this.headImageUrl = data.avatar
        this.prepare = data.prepared
        this.speaking = data.speaking
        this.isMaster = data.is_master
        this.isObserver = data.is_observer

        this.msgDecor = data.message_box
        this.headDecor = data.avatar_box
        this.vipInfo = data.vipInfo

        if (!TextUtils.isEmpty(data.signature)) {
            this.signature = data.signature
        }

        return this
    }

    /**
     * 填充数据
     */
    fun inflateData(data: ServerMessageEnterResult): SpyRoomPlayer {
        this.userId = data.user.id
        this.userName = data.user.name
        this.position = data.user.position
        this.headImageUrl = data.user.avatar
        this.prepare = data.user.prepared
        this.speaking = false
        this.isMaster = data.user.is_master
        this.isObserver = data.user.is_observer

        this.msgDecor = data.user.message_box
        this.headDecor = data.user.avatar_box
        this.vipInfo = data.user.vipInfo

        if (!TextUtils.isEmpty(data.user.signature)) {
            this.signature = data.user.signature
        }

        return this
    }


    /**
     * 转化为PersonalData
     */
    fun toPersonalData(): PersonalData {
        //只处理需要的
        val personalData = PersonalData()
        personalData.userId = this.userId
        personalData.userName = this.userName
        personalData.isOut = this.isOut
        personalData.userSex = this.sex
        personalData.selfPosition = this.position
        personalData.isOffline = this.disconnected
        personalData.isPrepared = this.prepare
        personalData.isSpeaking = this.speaking
        personalData.imageUrl = this.headImageUrl
        personalData.isCanSpeak = !this.isBanMic
        personalData.isBanMic = false
        personalData.isFreeStyle = false
        personalData.isMaster = this.isMaster
        personalData.isSpeech = this.speech

        return personalData
    }

    /**
     * 是否是卧底
     */
    fun isSpy(): Boolean {
        return TextUtils.equals(role2, SpyRoomConstants.SPY_TAG)
    }

    /**
     * 是否是好人
     */
    fun isPeople(): Boolean {
        return TextUtils.equals(role2, SpyRoomConstants.GOOD_TAG)
    }

    override fun toString(): String {
        return "SpyRoomPlayer(userId='$userId', userName='$userName', position=$position, headImageUrl='$headImageUrl', sex=$sex, prepare=$prepare, speech=$speech, speaking=$speaking, isMaster=$isMaster, isObserver=$isObserver, disconnected=$disconnected, isBanMic=$isBanMic, isOut=$isOut, isFriend=$isFriend, isBlock=$isBlock)"
    }


}