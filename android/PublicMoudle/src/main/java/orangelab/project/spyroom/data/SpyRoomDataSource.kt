package orangelab.project.spyroom.data

import android.content.Intent
import android.graphics.Color
import android.support.annotation.Keep
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PLog
import com.datasource.GlobalUserState
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.MainApplication
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.model.*
import orangelab.project.common.utils.IntentDataHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.action.Refreshable
import orangelab.project.spyroom.manager.SpyRoomAudioSocketManager
import orangelab.project.spyroom.model.SpyRoomMessenger
import orangelab.project.spyroom.model.SpyRoomPlayer
import orangelab.project.voice.api.VoiceSocketHelper
import orangelab.project.voice.ds.LimitMap
import orangelab.project.voice.model.*
import java.util.Collections.sort
import java.util.Map.Entry

/**
 * 谁是卧底数据源
 */
class SpyRoomDataSource(result: EnterRoomResult, intent: Intent) : Refreshable, Destroyable, Keepable {

    private val TAG = "SpyRoomDataSource"

    /**
     * 是否开启同步处理，若开启，则更新数据时会更具条件更新与之关联的数据
     */
    private var sync = true

    var users: ArrayList<SpyRoomPlayer> = ArrayList()                        //房间所有用户
    var players: ArrayList<SpyRoomPlayer> = ArrayList()                      //玩家
    var observers: ArrayList<SpyRoomPlayer> = ArrayList()                    //观战者
    var lockedSeatList: ArrayList<Int> = ArrayList()                         //加锁座位列表
    var messageMap: LimitMap<Long, SystemMessageItem> = LimitMap()           //消息列表
    var addFriendIds: ArrayList<VoiceAddFriendBean> = ArrayList()            //加好友的id列表
    var likeCount: Int = 0                                                   //点赞数量
    var audioConfigBean: SpyRoomAudioSocketManager.SpyRoomAudioConfigBean?   //语音配置信息

    init {

        //房间玩家初始化
        val iterator = result.room.users.entries.iterator()
        while (iterator.hasNext()) {
            val entry = iterator.next() as Entry<*, *>
            val member = entry.value as EnterRoomResult.EnterRoomUserItem
            if (member.is_master) {
                SpyRoomConfig.masterPosition = member.position //记录房主位置
            }
            addPlayer(SpyRoomPlayer().inflateData(member))
        }

        //观战者初始化
        val iterator1 = result.room.observers.entries.iterator()
        while (iterator1.hasNext()) {
            val entry = iterator1.next() as Entry<*, *>
            val member = entry.value as EnterRoomResult.EnterRoomUserItem
            if (member.is_master) {
                SpyRoomConfig.masterPosition = member.position //记录房主位置
            }
            addObserver(SpyRoomPlayer().inflateData(member))
        }

        //加锁座位初始化
        refreshLockedSeats(result.room.locked_positions)

        //音频配置信息初始化
        audioConfigBean = SpyRoomAudioSocketManager.SpyRoomAudioConfigBean(result.room.room_id, result.media_server)

        //配置信息初始化
        SpyRoomConfig.roomId = result.room.room_id
        SpyRoomConfig.title = result.room.title
        //SpyRoomConfig.type = result.room.child_type //TODO 这里需要server提供房间详细类型
        SpyRoomConfig.background = result.room.config.room_info.background.url //TODO 跟server确认一下背景图片
        SpyRoomConfig.password = result.room.password
        //SpyRoomConfig.maxChair = result.room.config.active_count //TODO 这里先不处理,需要serve根据房间类型修改
        SpyRoomConfig.isGaming = result.room.isPlaying
        SpyRoomConfig.canInsertMic = result.room.can_cut_speaker
        if (result.room.config.undercover != null) {
            SpyRoomConfig.configing = result.room.config.undercover.configing
            SpyRoomConfig.configed = result.room.config.undercover.configed
            SpyRoomConfig.canGuessWord = result.room.config.undercover.can_guess
            SpyRoomConfig.moreSpy = result.room.config.undercover.more_undercover
            SpyRoomConfig.spyNumber = result.room.config.undercover.undercover_count
            SpyRoomConfig.goodNumber = SpyRoomConfig.maxChair - SpyRoomConfig.spyNumber
        }

        //个人信息初始化
        GlobalUserState.getGlobalState().curRoomId = result.room.room_id
        GlobalUserState.getGlobalState().curGameType = IntentDataHelper.getGameType(intent)
        val myself = PersonalData()
        myself.userExp = IntentDataHelper.getUserExp(intent)
        myself.userName = IntentDataHelper.getUserName(intent)
        myself.userId = IntentDataHelper.getUserID(intent)
        myself.imageUrl = IntentDataHelper.getUserAvater(intent)
        myself.gameType = IntentDataHelper.getGameType(intent)
        myself.token = IntentDataHelper.getUserToken(intent)
        myself.userSex = IntentDataHelper.getUserSex(intent)
        myself.roomId = result.room.room_id
        myself.selfPosition = result.position
        if (myself.userId != null) {
            val player = findUserById(myself.userId)
            myself.isMaster = player?.isMaster ?: false
        } else {
            val player = findUserByPosition(myself.selfPosition)
            myself.isMaster = player?.isMaster ?: false
        }

        //如果是游客，重新设置id
        if (Utils.checkUserIdIsTourist(myself.userId)) {
            val player = findUserByPosition(myself.selfPosition)
            myself.isMaster = player?.isMaster ?: false
            myself.userId = player?.userId
        }
        PersonalDataHelper.setPersonalData(myself)

        //addWelcomeMessage()
    }

    /**
     * 添加欢迎信息
     */
    private fun addWelcomeMessage() {
        addMessage(SpyRoomConstants.CODE_WELCOME, SystemMessageItem.createSystemMessage(
                MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                MessageUtils.getString(R.string.str_spy_room_game_welcome)))
        addMessage(SpyRoomConstants.CODE_INTRODUCE, SystemMessageItem.createSystemMessage(
                MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                MessageUtils.getString(R.string.str_spy_room_game_introduce, SpyRoomConfig.maxChair)))
    }

    /**
     * 根据位置找人
     */
    fun findUserByPosition(position: Int): SpyRoomPlayer? {
        users.forEach {
            if (it.position == position) {
                return it
            }
        }

        return null
    }

    /**
     * 根据id找人
     */
    fun findUserById(id: String): SpyRoomPlayer? {
        users.forEach {
            if (TextUtils.equals(it.userId, id)) {
                return it
            }
        }

        return null
    }

    /**
     * 添加消息
     */
    fun addMessage(msgId: Long, message: SystemMessageItem) {
        messageMap.put(msgId, message)
    }

    /**
     * 添加一个用户
     */
    fun addUser(user: SpyRoomPlayer) {
        users.add(user)
    }

    /**
     * 添加一个玩家
     */
    fun addPlayer(user: SpyRoomPlayer) {
        players.add(user)
        if (sync) {
            addUser(user)
        }
    }

    /**
     * 添加一个观战者
     */
    fun addObserver(user: SpyRoomPlayer) {
        observers.add(user)
        if (sync) {
            addUser(user)
        }
    }

    /**
     * 移除一个用户
     */
    fun removeUser(user: SpyRoomPlayer?) {
        users.remove(user)
    }

    /**
     * 移除一个玩家
     */
    fun removePlayer(user: SpyRoomPlayer?) {
        players.remove(user)
        if (sync) {
            removeUser(user)
        }
    }

    /**
     * 移除一个观战者
     */
    fun removeObserver(user: SpyRoomPlayer?) {
        observers.remove(user)
        if (sync) {
            removeUser(user)
        }
    }

    /**
     * 刷新加锁座位
     */
    fun refreshLockedSeats(lockedSeats: List<Int>) {
        this.lockedSeatList.clear()
        this.lockedSeatList.addAll(lockedSeats)
    }

    /**
     * 清空用户列表
     */
    fun clearUsers() {
        players.clear()
        observers.clear()
        users.clear()
    }

    /**
     * 刷新点赞人数
     */
    fun refreshLikeCount(likeCount: Int) {
        this.likeCount = if (likeCount > 0) likeCount else 0
    }

    override fun refresh(data: Refreshable) {

    }

    override fun restore(data: Refreshable) {
    }

    override fun reset() {
    }

    override fun clear() {

    }

    override fun destroy() {
        clearUsers()
        messageMap.clear()
        lockedSeatList.clear()
        addFriendIds.clear()
        audioConfigBean = null
    }

    /**
     * 数据解析器，处理server消息解析数据
     */
    var dataSourceResolver = object : SpyRoomProcessable() {

        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //加人
            val item = GsonHelper.getGson().fromJson<ServerMessageEnterResult>(spyRoomMessenger.payload.toString(), ServerMessageEnterResult::class.java)
            val player = SpyRoomPlayer().inflateData(item)
            if (!SpyRoomHelper.isSelf(player.userId)) {
                addPlayer(player)
            }

            //加消息
            addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                    MessageUtils.getString(R.string.str_comein, player.userName)))

            spyRoomMessenger.bindPostPlayer(player)

            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //更新用户列表
            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            val user = findUserByPosition(position)
            if (user != null) {
                removePlayer(user)

                //更新系统消息列表
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                        MessageUtils.getString(R.string.str_leave, user.userName)))

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleLeave: user is null")
            }

            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //加人
            val item = GsonHelper.getGson().fromJson<ServerMessageEnterResult>(spyRoomMessenger.payload.toString(), ServerMessageEnterResult::class.java)
            val player = SpyRoomPlayer().inflateData(item)
            if (!SpyRoomHelper.isSelf(player.userId)) {
                addObserver(player)
            }

            //加消息
//            addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
//                    MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
//                    MessageUtils.getString(R.string.str_comein, player.userName)))

            spyRoomMessenger.bindPostPlayer(player)

            return false
        }

        override fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //更新用户列表
            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            val user = findUserByPosition(position)
            if (user != null) {
                removeObserver(user)

                //更新系统消息列表
//                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
//                        MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
//                        MessageUtils.getString(R.string.str_leave, user.userName)))

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleObserverLeave: user is null")
            }

            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //更新用户列表
            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            val user = findUserByPosition(position)
            if (user != null) {
                if (SpyRoomHelper.isSelf(user.userId)) {
                    PersonalDataHelper.personalData.selfPosition = SpyRoomConstants.OVER_POSITION
                }
                removePlayer(user)
                removeObserver(user)
                //更新消息列表
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                        MessageUtils.getString(R.string.str_leave, user.userName)))

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleKickOut: user is null")
            }

            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val result = GsonHelper.getGson().fromJson<RestoreResult>(spyRoomMessenger.payload.toString(), RestoreResult::class.java)
            if (result != null) {
                restoreConfig(result)
                restoreRoomInfo(result.room_info)
                restoreGameInfo(result.game_info)
            }

            return false
        }

        override fun handleDisconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.disconnected = true
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isOffline = true
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleDisconnect: user is null")
            }

            return false
        }

        override fun handleReconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.disconnected = false
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isOffline = false
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleReconnect: user is null")
            }

            return false
        }

        override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            val user = findUserByPosition(position)
            if (user != null) {
                user.isMaster = true
                SpyRoomConfig.masterPosition = user.position
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isMaster = true
                    }
                }

                //处理系统消息
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_new_master, user.userName)))

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleChangeRoomOwner: user is null")
            }

            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //还原之前的房主
            val oldMaster = findUserByPosition(SpyRoomConfig.masterPosition)
            if (oldMaster != null) {
                oldMaster.isMaster = false
            }
            if (SpyRoomHelper.isSelf(SpyRoomConfig.masterPosition)) {
                val personalData = PersonalDataHelper.getPersonalData()
                if (personalData != null) {
                    personalData.isMaster = false
                }
            }

            //设置新房主
            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            SpyRoomConfig.masterPosition = position

            val user = findUserByPosition(position)
            if (user != null) {
                user.isMaster = true
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isMaster = true
                    }
                }

                //处理系统消息
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_new_master, user.userName)))

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleUpdateMaster: user is null")
            }

            return false
        }

        override fun handleUpdateRoomConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            return false
        }

        override fun handleChangeRoomPassword(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val password = spyRoomMessenger.payload.optString(SpyRoomConstants.PASSWORD)
            SpyRoomConfig.password = password
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                personalData.curPassword = password
                GlobalUserState.getGlobalState().password = password
            }

            return false
        }

        override fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = RoomSocketEngineHelper.getPositionFromJSON(spyRoomMessenger.payload)
            val message = spyRoomMessenger.payload.optString(SpyRoomConstants.MESSAGE)
            val user = findUserByPosition(position)
            if (user != null) {
                val userName = if (!TextUtils.isEmpty(user.userName)) user.userName else "[" + (user.position + 1) + "]" + MessageUtils.getString(R.string.str_num_uer)
                val gift = spyRoomMessenger.payload.optJSONObject(SpyRoomConstants.GIFT_CHAT)
                val msgPosition = position
                if (gift != null) {
                    val type = gift.optString(SpyRoomConstants.GIFT_TYPE)
                    val messageItem = SystemMessageItem.createGiftMessage(
                            MainApplication.getInstance().resources.getColor(R.color.color_voice_gift),
                            msgPosition + 1,
                            message,
                            type,
                            userName)
                    messageItem.setUser(user)
                    addMessage(spyRoomMessenger.msg_id, messageItem)
                } else {
                    val messageItem = SystemMessageItem.createMemberMessage(
                            MainApplication.getInstance().resources.getColor(R.color.color_spy_room_chat),
                            msgPosition + 1,
                            userName,
                            message)
                    messageItem.setUser(user)
                    addMessage(spyRoomMessenger.msg_id, messageItem)
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleChat: user is null")
            }

            return false
        }

        override fun handleSystemMessage(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val content = spyRoomMessenger.payload.optString(SpyRoomConstants.CONTENT)
            addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                    content))

            return false
        }

        override fun handleStartConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {

            SpyRoomConfig.configing = true
            SpyRoomConfig.configed = false

            return false
        }

        override fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {

            SpyRoomConfig.firstWord = spyRoomMessenger.payload.optString("first")
            SpyRoomConfig.secondWord = spyRoomMessenger.payload.optString("second")
            if (spyRoomMessenger.payload.has("config")) {
                val configObject = spyRoomMessenger.payload.optJSONObject("config")
                if (configObject != null) {
                    val roomConfigBean = GsonHelper.getGson().fromJson<VoiceRoomConfigBean>(configObject.toString(), VoiceRoomConfigBean::class.java)
                    if (roomConfigBean != null) {
                        SpyRoomConfig.canInsertMic = roomConfigBean.can_cut_speaker
                        SpyRoomConfig.canGuessWord = roomConfigBean.undercover.can_guess
                        SpyRoomConfig.hasGhost = roomConfigBean.undercover.has_ghost
                        SpyRoomConfig.moreSpy = roomConfigBean.undercover.more_undercover
                        SpyRoomConfig.configing = roomConfigBean.undercover.configing
                        SpyRoomConfig.configed = roomConfigBean.undercover.configed
                        SpyRoomConfig.spyNumber = roomConfigBean.undercover.undercover_count
                    }
                }
            }

            if (SpyRoomConfig.canGuessWord) {
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                        MessageUtils.getString(R.string.str_spy_room_update_spy_count, SpyRoomConfig.spyNumber)))
            } else {
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_spy_room_system),
                        MessageUtils.getString(R.string.str_spy_room_update_spy_count_no, SpyRoomConfig.spyNumber)))
            }

            return false
        }

        override fun handleStopConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {

            SpyRoomConfig.configing = false
            SpyRoomConfig.configed = true

            return false
        }

        override fun handleGetConfigWords(spyRoomMessenger: SpyRoomMessenger): Boolean {

            if (!SpyRoomConfig.configing) {
                val words = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceWords::class.java)
                VoiceSocketHelper.sendSpySettingUpdate(
                        SpyRoomConfig.moreSpy,
                        SpyRoomConfig.hasGhost,
                        SpyRoomConfig.canInsertMic,
                        SpyRoomConfig.canGuessWord,
                        words.first,
                        words.second)
            }

            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val userId = spyRoomMessenger.payload.optString(SpyRoomConstants.USER_ID_LOWER)
            val user = findUserById(userId)
            if (user != null) {
                removeObserver(user)
                val newPosition = spyRoomMessenger.payload.optInt(SpyRoomConstants.DEST_POSITION)
                user.position = newPosition
                user.isObserver = false
                val personalData = PersonalDataHelper.getPersonalData()
                if (personalData != null) {
                    if (SpyRoomHelper.isSelf(user.userId)) {
                        personalData.selfPosition = user.position
                        personalData.isObserver = user.isObserver
                    }
                }

                //禁麦期间被转让成房主,就解除禁麦
                if (SpyRoomHelper.isMaster()) {
                    if (user.isBanMic) {
                        user.isBanMic = false
                        if (SpyRoomHelper.isSelf(user.position)) {
                            personalData.isBanMic = false
                        }
                    }
                }

                //更新消息列表
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_up_seat, user.userName, newPosition + 1)))

                spyRoomMessenger.bindPostPlayer(user)
                addPlayer(user)
            } else {
                PLog.e(TAG, "handleUpSeat: user is null")
            }

            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val userId = spyRoomMessenger.payload.optString(SpyRoomConstants.USER_ID_LOWER)
            val desPosition = spyRoomMessenger.payload.optInt(SpyRoomConstants.DEST_POSITION)
            val user = findUserById(userId)
            if (user != null) {
                removePlayer(user)
                //更新系统消息列表
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().resources.getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_down_seat, user.userName, user.position + 1)))

                //更改用户属性
                user.position = desPosition
                user.isObserver = true
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.selfPosition = desPosition
                        personalData.isObserver = true
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
                addObserver(user)

                PLog.e(TAG, "newuser=${spyRoomMessenger.newPlayer}")
            } else {
                PLog.e(TAG, "handleDownSeat: user is null")
            }

            return false
        }

        override fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.prepare = true
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isPrepared = true
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleSpyGamePrepare: user is null")
            }

            return false
        }

        override fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.prepare = false
                val personalData = PersonalDataHelper.getPersonalData()
                if (personalData != null) {
                    if (SpyRoomHelper.isSelf(user.userId)) {
                        personalData.isPrepared = false
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleSpyGameUnPrepare: user is null")
            }

            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {

            try {
                val spyStart = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyStart::class.java)
                SpyRoomConfig.goodNumber = spyStart.role_map.people
                SpyRoomConfig.spyNumber = spyStart.role_map.undercover
                SpyRoomConfig.startDuration = spyStart.duration
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(
                        MessageUtils.getString(R.string.str_spy_room_game_start),
                        MessageUtils.getString(R.string.str_spy_room_good_number) + SpyRoomConfig.goodNumber + "\n"
                                + MessageUtils.getString(R.string.str_spy_room_spy_number) + SpyRoomConfig.spyNumber
                        , Gravity.CENTER))
                SpyRoomConfig.isGaming = true //这一句放在最后，返回非法消息解析失败，就不能开始游戏
            } catch (e: Exception) {
                PLog.e(TAG, "handleSpyGameStart error:" + e.message)
            }

            return false
        }

        override fun handleAssignRole(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val role = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceAssignedRole::class.java)
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                personalData.selfRole = role.role
            }

            return false
        }

        override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //清除其他人的说话状态
            users.forEach {
                it.speech = false
                it.speechTime = 0
            }
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                personalData.isSpeech = false
                personalData.speechTime = 0
            }

            //处理你的说话人的信息
            val spySpeech = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpySpeech::class.java)
            SpyRoomConfig.gameLen = spySpeech.loop_index
            SpyRoomConfig.speechPosition = spySpeech.position + 1
            if (!spySpeech.dead) {
                val user = findUserByPosition(spySpeech.position)
                if (user != null) {
                    user.speech = true
                    user.speechTime = spySpeech.duration / 1000
                }
                if (SpyRoomHelper.isSelf(spySpeech.position)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isSpeech = true
                        personalData.speechTime = spySpeech.duration / 1000
                    }
                }
            }

            return false
        }

        override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val spySpeech = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpySpeech::class.java)
            if (!spySpeech.dead) {
                val user = findUserByPosition(spySpeech.position)
                if (user != null) {
                    user.speech = false
                    user.speechTime = 0
                }
                if (SpyRoomHelper.isSelf(spySpeech.position)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isSpeech = false
                        personalData.speechTime = 0
                    }
                }
            }

            return false
        }

        override fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.speaking = true
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isSpeaking = true
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleSpeak: user is null")
            }

            return false
        }

        override fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            val user = findUserByPosition(position)
            if (user != null) {
                user.speaking = false
                if (SpyRoomHelper.isSelf(user.userId)) {
                    val personalData = PersonalDataHelper.getPersonalData()
                    if (personalData != null) {
                        personalData.isSpeaking = false
                    }
                }

                spyRoomMessenger.bindPostPlayer(user)
            } else {
                PLog.e(TAG, "handleUnSpeak: user is null")
            }

            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val beforeVote = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyBeforeVote::class.java)
            SpyRoomConfig.voteDuration = beforeVote.duration
            users.forEach {
                it.speech = false
                it.speechTime = 0
            }
            if (PersonalDataHelper.personalData != null) {
                PersonalDataHelper.personalData.isSpeech = false
                PersonalDataHelper.personalData.speechTime = 0
            }

            return false
        }

        override fun handleVoteInfoTemporary(spyRoomMessenger: SpyRoomMessenger): Boolean {

            return false
        }

        override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val voteResult = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyVoteResult::class.java)
            if (TextUtils.equals(voteResult.type, "death")) {
                if (voteResult.finished) {
                    if (voteResult.positions != null && voteResult.positions.size > 0) {

                        //处理投票结果
                        val sbVoteResult = StringBuilder()
                        val beVoted = voteResult.vote_info.keys
                        beVoted.forEach {
                            val votedSb = StringBuilder()
                            voteResult.vote_info[it]?.forEach {
                                votedSb.append(it + 1).append("、")
                            }

                            if (it >= 0) {
                                //正常投票
                                val votedStr = votedSb.deleteCharAt(votedSb.length - 1).toString()
                                val voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg, votedStr, it + 1)
                                sbVoteResult.append(voteInfo).append("\n")
                            } else {
                                //弃票
                                val votedStr = votedSb.deleteCharAt(votedSb.length - 1).toString()
                                val voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg_give_up_number, votedStr)
                                sbVoteResult.append(voteInfo).append("\n")
                            }
                        }

                        //有平票
                        if (voteResult.need_pk) {
                            //处理PK消息
                            if (voteResult.positions != null && voteResult.positions.size >= 2) {
                                sort(voteResult.positions)
                                val message = if (voteResult.positions.size == 2) {
                                    Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold, voteResult.positions[0] + 1, voteResult.positions[1] + 1)).toString()
                                } else {
                                    val sb = StringBuilder()
                                    for (position in voteResult.positions) {
                                        sb.append(position + 1).append("、")
                                    }
                                    val votePositions = sb.deleteCharAt(sb.length - 1).toString()
                                    Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold_more, votePositions)).toString()
                                }
                                //添加PK信息
                                sbVoteResult.append(message)
                            }
                        } else {
                            //无PK，删除最后一个换行符
                            sbVoteResult.deleteCharAt(sbVoteResult.length - 1)
                        }
                        addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(MessageUtils.getString(R.string.str_voice_vote_result), sbVoteResult.toString(), Gravity.LEFT))
                    } else {
                        //所有玩家弃票
                        addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(MessageUtils.getString(R.string.str_voice_vote_result), MessageUtils.getString(R.string.str_voice_vote_msg_give_up), Gravity.CENTER))
                    }
                }
            }

            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val deathInfo = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGameDeathInfo::class.java)
            if (deathInfo.death_info != null && deathInfo.death_info.size > 0) {
                val info = deathInfo.death_info[0]
                if (info != null) {
                    val user = findUserByPosition(info.killed)
                    if (user != null) {
                        user.isOut = true
                        user.role2 = info.role
                        if (user.isSpy()) {
                            SpyRoomConfig.spyNumber--
                        } else if (user.isPeople()) {
                            SpyRoomConfig.goodNumber--

                        }
                        if (SpyRoomHelper.isSelf(user.userId)) {
                            val personalData = PersonalDataHelper.getPersonalData()
                            if (personalData != null) {
                                personalData.isOut = true
                                personalData.selfRole = user.role
                                personalData.role = user.role2
                            }
                        }
                    } else {
                        PLog.e(TAG, "handleDeathInfo: user is null")
                    }

                    //处理系统消息
                    val role = if (TextUtils.equals(SpyRoomConstants.SPY_TAG, info.role))
                        MessageUtils.getString(R.string.str_voice_role_spy)
                    else
                        MessageUtils.getString(R.string.str_voice_role_people)
                    val title = MessageUtils.getString(R.string.str_voice_death_info)
                    val msg = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_identity_bold, info.killed + 1, role)).toString()
                    addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(title, msg, Gravity.CENTER))
                }
            }

            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //1 重置游戏,放在2之前，防止2意外出错不能正常结束游戏
            resetGame()

            //2 处理游戏结束消息
            val gameOverBean = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGameOverBean::class.java)
            var spyWord = ""
            var goodWord = ""
            val undercover = gameOverBean.roles.undercover
            val people = gameOverBean.roles.people
            if (people != null && people.size > 0) {
                goodWord = people[0].detail
            }
            if (undercover != null && undercover.size > 0) {
                val roleInfo = undercover[0]
                if (roleInfo != null) {
                    spyWord = roleInfo.detail
                }

                //卧底为幽灵的情况
                if (TextUtils.isEmpty(spyWord)) {
                    if (gameOverBean.words != null) {
                        spyWord = if (TextUtils.equals(goodWord, gameOverBean.words.first)) {
                            gameOverBean.words.second
                        } else {
                            gameOverBean.words.first
                        }
                    }
                }
            }
            val strId = if (TextUtils.equals(SpyRoomConstants.SPY_TAG, gameOverBean.win_type)) R.string.str_voice_spy_victory else R.string.str_voice_good_victory
            addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(
                    MessageUtils.getString(R.string.str_voice_game_over),
                    MessageUtils.getString(strId) + "\n"
                            + MessageUtils.getString(R.string.str_voice_good_word) + ":" + goodWord + "\n"
                            + MessageUtils.getString(R.string.str_voice_spy_word) + ":" +
                            if (TextUtils.isEmpty(spyWord)) MessageUtils.getString(R.string.str_voice_word_is_ghost) else spyWord, Gravity.CENTER))

            return false
        }

        override fun handleAddFriend(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val userId = spyRoomMessenger.payload.optString(SpyRoomConstants.USER_ID_LOWER)
            if (!hasAdded(userId)) {
                addFriendIds.add(VoiceAddFriendBean(userId, false))
            }

            return false
        }

        override fun handleGuessWord(spyRoomMessenger: SpyRoomMessenger): Boolean {
            return false
        }

        override fun handleGuessWordResult(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val wordResult = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGuessWordResult::class.java)
            if (wordResult != null) {
                val title = MessageUtils.getString(R.string.str_voice_bomb_word_result)
                val msg = if (wordResult.success)
                    MessageUtils.getString(R.string.str_voice_bomb_word_success)
                else
                    MessageUtils.getString(R.string.str_voice_bomb_word_failure)
                addMessage(spyRoomMessenger.msg_id, SystemMessageItem.createSpyRoomGameMessage(title, msg, Gravity.CENTER))
            }

            return false
        }

        /**
         * 重置游戏
         */
        private fun resetGame() {
            SpyRoomConfig.isGaming = false           //游戏结束
            //SpyRoomConfig.goodNumber = 0           //保留上一次设置
            //SpyRoomConfig.spyNumber = 0

            //重置玩家信息
            users.forEach {
                it.prepare = false
                it.speech = false
                it.isOut = false
            }

            //重置自身信息
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                personalData.isPrepared = false
                personalData.isOut = false
                personalData.isSpeech = false
            }
        }

        /**
         * 判断是否添加过
         */
        private fun hasAdded(id: String): Boolean {
            addFriendIds.forEach {
                if (TextUtils.equals(id, it.id)) {
                    return true
                }
            }
            return false
        }

        //恢复配置信息
        private fun restoreConfig(restoreResult: RestoreResult) {
            val roomMessage = restoreResult.room_info
            val titleType = if (TextUtils.isEmpty(restoreResult.room_info.child_type)) "" else restoreResult.room_info.child_type
            //SpyRoomConfig.type = titleType
            SpyRoomConfig.title = roomMessage.title
            SpyRoomConfig.password = roomMessage.password
            try {
                SpyRoomConfig.background = roomMessage.config.room_info.background.url
            } catch (e: Exception) {
                PLog.e(TAG, "restoreConfig: " + e.message)
            }

            SpyRoomConfig.isGaming = roomMessage.isPlaying
            //SpyRoomConfig.maxChair = restoreResult.room_info.config.active_count
            if (roomMessage.config.undercover != null) {
                SpyRoomConfig.configing = roomMessage.config.undercover.configing
                SpyRoomConfig.configed = roomMessage.config.undercover.configed

                //四个布尔值还原
                SpyRoomConfig.moreSpy = roomMessage.config.undercover.more_undercover
                SpyRoomConfig.hasGhost = roomMessage.config.undercover.has_ghost
                SpyRoomConfig.canInsertMic = roomMessage.config.can_cut_speaker
                SpyRoomConfig.canGuessWord = roomMessage.config.undercover.can_guess
            }

            //两个词语还原
            if (restoreResult.game_info != null) {
                if (restoreResult.game_info.words != null) {
                    SpyRoomConfig.firstWord = restoreResult.game_info.words.first
                    SpyRoomConfig.secondWord = restoreResult.game_info.words.second
                }
            }

            //卧底配置人数还原
            if (SpyRoomConfig.moreSpy) {
                SpyRoomConfig.spyNumber = 2
            } else {
                SpyRoomConfig.spyNumber = 1
            }
            SpyRoomConfig.goodNumber = SpyRoomConfig.maxChair - SpyRoomConfig.spyNumber
        }

        //恢复房间信息
        private fun restoreRoomInfo(roomInfo: EnterRoomResult.EnterRoomMessage) {

            //恢复在线用户
            clearUsers()

            //玩家复原
            val iterator = roomInfo.users.entries.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next() as Entry<*, *>
                val member = SpyRoomPlayer().inflateData(entry.value as EnterRoomResult.EnterRoomUserItem)
                if (member.isMaster) {
                    SpyRoomConfig.masterPosition = member.position //记录房主位置
                }

                addPlayer(member)
                PLog.e(TAG, "addPlayer:$member")
            }

            //观战者复原
            val iterator1 = roomInfo.observers.entries.iterator()
            while (iterator1.hasNext()) {
                val entry = iterator1.next() as Entry<*, *>
                val member = SpyRoomPlayer().inflateData(entry.value as EnterRoomResult.EnterRoomUserItem)
                if (member.isMaster) {
                    SpyRoomConfig.masterPosition = member.position //记录房主位置
                }
                addObserver(member)
                PLog.e(TAG, "addObserver:$member")
            }

            //刷新加锁座位
            refreshLockedSeats(roomInfo.locked_positions)

            //恢复个人信息
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                val mySelf = findUserById(PersonalDataHelper.getPersonalData().userId)
                if (mySelf != null) {
                    //更新个人的所有信息
                    PersonalDataHelper.setPersonalData(mySelf.toPersonalData())
                } else {
                    //被踢了, 将自己位置设为极大
                    personalData.selfPosition = SpyRoomConstants.OVER_POSITION
                }
            } else {
                //已经离开房间
            }

            //恢复点赞数
            refreshLikeCount(roomInfo.likeCount)
        }

        //恢复游戏信息
        private fun restoreGameInfo(gameInfo: RestoreResult.RestoreRoleMessage?) {

            if (gameInfo != null) {

                //恢复个人词语
                val personalData = PersonalDataHelper.getPersonalData()
                if (personalData != null) {
                    personalData.selfRole = gameInfo.role
                }

                //恢复出局信息
                if (gameInfo.death_info != null && gameInfo.death_info.size > 0) {
                    for (deathInfo in gameInfo.death_info) {
                        val user = findUserByPosition(deathInfo.position)
                        if (user != null) {
                            user.isOut = true
                            user.role2 = deathInfo.type
                            if (SpyRoomHelper.isSelf(user.userId)) {
                                if (personalData != null) {
                                    personalData.isOut = true
                                    personalData.role = user.role2
                                }
                            }
                            if (user.isSpy()) {
                                SpyRoomConfig.spyNumber--
                            } else if (user.isPeople()) {
                                SpyRoomConfig.goodNumber--
                            }
                        }
                    }
                }

                //恢复speech信息
                users.forEach {
                    it.speech = false
                    it.speechTime = 0
                }
                if (gameInfo.speech_info != null && gameInfo.speech_info.current != null) {
                    val position = gameInfo.speech_info.current.position//此人正在说话
                    SpyRoomConfig.speechPosition = position + 1
                    val user = findUserByPosition(position)
                    if (user != null) {
                        user.speech = true
                        user.speechTime = gameInfo.speech_info.duration / 1000
                        if (SpyRoomHelper.isSelf(user.userId)) {
                            if (personalData != null) {
                                personalData.isSpeech = user.speech
                                personalData.speechTime = user.speechTime;
                            }
                        }
                    }
                }
            }
        }
    }

}
