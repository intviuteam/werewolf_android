package orangelab.project.spyroom.component

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.R
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.UIActuator
import com.datasource.GlobalUserState
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import kotlinx.android.synthetic.main.layout_spy_room_operation_view.view.*
import orangelab.project.ProjectConfig
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.engine.task.SocketCallBack
import orangelab.project.common.share.ShareBridgeData
import orangelab.project.common.share.SharePoolManager
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.SpyRoomContext
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.api.SpyRoomSocketHelper
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.model.SpyRoomMessenger

/**
 * 操作器
 */
class SpyRoomOperationView : Destroyable, Keepable {

    private var contentView: ViewGroup
    private var spyRoomContext: SpyRoomContext

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.contentView = contentView
        this.spyRoomContext = spyRoomContext

        switchRole()
        initListener()
    }

    private fun turnIntoStartBtn(imageView: ImageView) {
        imageView.setImageResource(R.drawable.bg_spy_room_start_selector)
        imageView.setOnClickListener {
            SpyRoomSocketHelper.sendSpyGameStart(SocketCallBack { }, SocketCallBack {
                UIActuator.post { ToastToolKit.showShort(it) }
            })
        }
    }

    private fun turnIntoPrepareBtn(imageView: ImageView) {
        imageView.setImageResource(R.drawable.bg_spy_room_prepare_selector)
        imageView.setOnClickListener {
            SpyRoomSocketHelper.sendSpyGamePrepare()
        }
    }

    private fun turnIntoUnPrepareBtn(imageView: ImageView) {
        imageView.setImageResource(R.drawable.bg_spy_room_cancel_selector)
        imageView.setOnClickListener {
            SpyRoomSocketHelper.sendSpyGameUnPrepare()
        }
    }

    private fun turnIntoUpSeatBtn(imageView: ImageView) {
        imageView.setImageResource(R.drawable.bg_spy_room_upseat_selector)
        imageView.setOnClickListener {
            if (!isFull()) {
                SpyRoomSocketHelper.sendGameUpSeat()
            } else {
                ToastToolKit.showShort(R.string.str_spy_room_full)
            }
        }
    }

    private fun turnIntoDownSeatBtn(imageView: ImageView) {
        imageView.setImageResource(R.drawable.bg_spy_room_observe_selector)
        imageView.setOnClickListener {
            SpyRoomSocketHelper.sendSpyGameUnPrepare()//取消准备
            SpyRoomSocketHelper.sendToObserve(PersonalDataHelper.personalData.selfPosition)//下座
        }
    }

    /**
     * 展示结束发言按钮
     */
    private fun turnIntoEndSpeechBtn() {
        val position = SpyRoomConfig.speechPosition - 1
        fun showEndSpeech(): Boolean {
            return position >= 0 && SpyRoomHelper.isSelf(position) && SpyRoomConfig.isGaming
        }
        if (showEndSpeech()) {
            val imageView: ImageView = contentView.findViewById(R.id.btn_spy_room_operation_mid) as ImageView
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.bg_spy_room_end_speech_selector)
            imageView.setOnClickListener {
                SpyRoomSocketHelper.sendEndSpeech()
            }
        } else {
            hideAllButton()
        }
    }

    /**
     * 座位是否坐满
     */
    private fun isFull(): Boolean {
        return spyRoomContext.datasource.players.size >= SpyRoomConfig.maxChair
    }

    /**
     * 根据视角展示按钮/游戏开始前
     */
    private fun switchRole() {

        //准备/取消准备/开始
        if (SpyRoomHelper.isMaster()) {
            //房主显示开始
            turnIntoStartBtn(contentView.findViewById(R.id.btn_spy_room_operation_left) as ImageView)
        } else {
            //玩家视角
            if (SpyRoomHelper.isPrepared()) {
                turnIntoUnPrepareBtn(contentView.findViewById(R.id.btn_spy_room_operation_left) as ImageView)
            } else {
                turnIntoPrepareBtn(contentView.findViewById(R.id.btn_spy_room_operation_left) as ImageView)
            }
        }

        //上座/观战
        if (SpyRoomHelper.isPlayer()) {
            turnIntoDownSeatBtn(contentView.findViewById(R.id.btn_spy_room_operation_mid) as ImageView)
        } else {
            turnIntoUpSeatBtn(contentView.findViewById(R.id.btn_spy_room_operation_mid) as ImageView)
        }

        //展示按钮
        if (SpyRoomConfig.isGaming) {
            contentView.btn_spy_room_operation_left.visibility = View.INVISIBLE
            contentView.btn_spy_room_operation_mid.visibility = View.INVISIBLE
            contentView.btn_spy_room_operation_right.visibility = View.INVISIBLE
        } else {
            if (SpyRoomHelper.isPlayer()) {
                contentView.btn_spy_room_operation_left.visibility = View.VISIBLE
                contentView.btn_spy_room_operation_right.visibility = View.VISIBLE
                if (SpyRoomHelper.isMaster()) {
                    contentView.btn_spy_room_operation_mid.visibility = View.INVISIBLE
                } else {
                    contentView.btn_spy_room_operation_mid.visibility = View.VISIBLE
                }
            } else {
                contentView.btn_spy_room_operation_left.visibility = View.INVISIBLE
                contentView.btn_spy_room_operation_right.visibility = View.INVISIBLE
                if (!isFull()) {
                    contentView.btn_spy_room_operation_mid.visibility = View.VISIBLE
                } else {
                    contentView.btn_spy_room_operation_mid.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun initListener() {
        contentView.btn_spy_room_operation_right.setOnClickListener {
            val data = ShareBridgeData(ShareBridgeData.SHARE_FROM_SPY_ROOM)
            data.userName = GlobalUserState.getGlobalState().userName
            data.roomId = SpyRoomConfig.roomId
            data.password = SpyRoomConfig.password
            if (PersonalDataHelper.personalData != null) {
                data.avatar = PersonalDataHelper.personalData.imageUrl
            }

            SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onSpyRoomShare(contentView.context, data)
        }
    }

    /**
     * 隐藏所有操作按钮
     */
    private fun hideAllButton() {
        contentView.btn_spy_room_operation_left.visibility = View.INVISIBLE
        contentView.btn_spy_room_operation_mid.visibility = View.INVISIBLE
        contentView.btn_spy_room_operation_right.visibility = View.INVISIBLE
    }

    /**
     * 根据视角和游戏阶段展示UI
     */
    private fun refreshUI() {
        if (SpyRoomConfig.isGaming) {
            hideAllButton()
        } else {
            switchRole()
        }
    }

    override fun destroy() {

    }

    /**
     * view观察处理器
     */
    var observer = object : SpyRoomProcessable() {

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshUI()
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshUI()
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshUI()
            return false
        }

        override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
            hideAllButton()
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshUI()
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshUI()
            turnIntoEndSpeechBtn()
            return false
        }

        override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            turnIntoEndSpeechBtn()
            return false
        }

        override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            hideAllButton()
            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {
            hideAllButton()
            return false
        }
    }
}