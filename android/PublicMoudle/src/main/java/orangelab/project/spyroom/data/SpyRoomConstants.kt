package orangelab.project.spyroom.data

import android.support.annotation.Keep
import com.toolkit.action.Keepable

object SpyRoomConstants : Keepable {


    val CODE_WELCOME: Long = 11001
    val CODE_INTRODUCE: Long = 11002


    val CODE_CONNECT: Long = 100011     //连接
    val CODE_DISCONNECT: Long = 100012  //断开连接


    //"谁是卧底相关"//
    val USER_STATUS_PREPARE = 10001//准备中
    val USER_STATUS_VOTE = 10002//投票中
    val USER_STATUS_OFFLINE = 10003//离线中
    val MASTER_STATUS_SETTING = 20001//设置中
    val MASTER_STATUS_SETTING_OVER = 20002//设置完成
    val SPY_TAG = "undercover"
    val GOOD_TAG = "people"
    val SPY_START_CONFIG = "uc_start_config"
    val SPY_STOP_CONFIG = "uc_stop_config"
    val SPY_UPDATE_CONFIG = "uc_update_config"
    val SPY_HAS_GHOST = "has_ghost"
    val SPY_CAN_CUT_SPEAKER = "can_cut_speaker"
    val SPY_CAN_GUESS = "can_guess"
    val SPY_COUNT = "undercover_count"

    val SPY_FIRST_WORD = "first"
    val SPY_SECOND_WORD = "second"
    val SPY_MORE_UNDERCOVER = "more_undercover"
    val SPY_GET_WORDS = "uc_get_words"
    val SPY_GET_WORDS_RESPONSE = "uc_get_words_result"
    val SPY_SYSTEM_WORDS = "system"
    val SPY_CUSTOM_WORDS = "custom"
    val SPY_ASSIGNED_ROLE = "assigned_role"
    val SPY_GAME_START = "start"
    val SPY_GAME_GUESS_WORD = "uc_guess_word"
    val SPY_GAME_GUESS_WORD_RESULT = "uc_guess_word_result"
    val SPY_GAME_WORD = "word"
    val SPY_GAME_VOTE = "vote"
    val SPY_GAME_VOTE_RESULT = "vote_result"
    val SPY_GAME_PREPARE = "prepare"
    val SPY_GAME_UNPREPARE = "unprepare"
    val SPY_GAME_SPEECH = "speech"
    val SPY_GAME_END_SPEECH = "end_speech"
    val SPY_GAME_OVER = "game_over"
    val SPY_GAME_TOPIC_COMMENT = "comment"
    val SPY_GAME_TOPIC_ATTITUDE = "approve"
    val SPY_GAME_DEATH_INFO = "death_info"
    val SPY_GAME_DISCONNECT = "disconnected"
    val SPY_GAME_CONNECT = "connected"
    //"谁是卧底相关"//


    //20180117添加 自由模式需要手动接受//
    val REQUEST_FREE_MODE = "request_free_mode"
    val ACCEPT_FREE_MODE = "accept_free_mode"
    val REJECT_FREE_MODE = "reject_free_mode"
    //20180117添加//

    //20180118添加 请求成为房主//
    val REQUEST_MASTER = "request_master"
    //20180118添加 请求成为房主//

    //20180119添加 已购买房间//
    val PURCHASED = "purchased"
    val PURCHASE_ROOM = "purchase_room"
    val PURCHASE_ROOM_RESULT = "purchase_room_result"
    val HAND_OVER_CREATOR = "handover_creator"
    val HAND_OVER_CREATOR_RESULT = "handover_creator_result"
    //20180119添加 已购买房间//

    //20180130 房契分享相关//
    val IS_FROM_DEED = "is_from_deed"
    val VOICE_DEED_BEAN = "voice_deed_bean"
    val SHARE_HOUSE_DEEDS = "SHARE_HOUSE_DEEDS"//房契分享与RN交互的action
    //20180130 房契分享相关//

    val EMPTY = ""
    val ADD_FRIEND_REQUEST = "/friend/add"
    val USER_DETAIL_REQUEST = "/user/info"
    val URI_APP_UPDATE = "/upgrade"
    val URI_TASK_DO = "/task/do"
    val GET_CARD_LIST = "/wealth/unusedCards"
    val REPORT_URL = "/account/report"
    val GOOGLE_PAY = "/pay/googlePay"
    val IS_NOPLAY = "/isnoplay"
    val FRIEND_DELETE = "/friend/delete"
    val ACCEPT_FRIEND = "/friend/accept?friend_id="
    val REJECT_FRIEND = "/friend/reject?friend_id="
    val FACEBOO_ADV_CONFIG = "/config/gg?type="

    val FRIEND_ID = "friend_id"
    val ALIVES = "alives"
    val TYPE_ENTER = "enter"
    val TYPE_JOIN = "join"
    val TYPE_UPDATE_MASTER = "update_master"
    val TYPE_PREPARE = "prepare"
    val TYPE_UNPREPARE = "unprepare"
    val ID = "id"
    val DURATION = "duration"
    val START = "start"
    val PREPARE_TIME = "prepare_time"
    val DEATH = "death"
    val PREPARE = "prepare"
    val UNPREPARE = "unprepare"
    val NAME_LOWER = "name"
    val AVATAR_LOWER = "avatar"
    val SPEECH_DIRECTION = "speech_direction"
    val DIRECTION = "direction"
    val LEFT = "left"
    val RIGHT = "right"
    val SIGNATURE = "signature"
    val SPEAK = "speak"
    val UNSPEAK = "unspeak"
    val POSITION = "position"
    val DEST_POSITION = "dest_position"
    val ROLE = "role"
    val CARD_TYPE = "card_type"
    val SPEECH = "speech"
    val SELF_SPEECH = "SELF_SPEECH"
    val SELF_SPEECH_DANGER = "SELF_SPEECH_DANGER"
    val VOTE = "vote"
    val SPEECH_DANGER_TIME = "SPEECH_DANGER_TIME"
    val STOP_MEDIA = "STOP_MEDIA"
    val ENDSPEECH = "end_speech"
    val GAMEOVER = "game_over"
    val TYPE_VOTE_RESULT_DEATH = "death"
    val TYPE_VOTE_RESULT_NO_DEATH = "no_death"
    val TYPE_VOTE_RESULT_EQUAL = "equal"
    val WIN_PEOPELE = "people"
    val WIN_THIRD = "third_party"
    val STATE_VOTED = "voted"
    val STATE_ALIVE = "alive"
    val STATE_LEAVED = "leaved"
    val SYSTEM_MSG = "system_msg"
    val CONTENT = "content"
    val RESET_MASTER = "reset_master"
    val LEAVE = "leave"
    val REMOVE = "remove"
    val DEAD = "dead"
    val TYPE_CHAT = "chat"
    val MESSAGE = "message"
    val TAKE_AWAY_RESULT = "take_away_result"
    val MUTE_ALL = "mute_all"
    val UNMUTE_ALL = "unmute_all"
    val APPLY_ROLE = "apply_role"
    val APPLY_ROLE_RESULT = "apply_role_result"
    val NOT_ENOUGH_CARD = "not_enough_card"
    val NOT_ENOUGH_ROLES = "not_enough_roles"
    val ILLEGAL_STATE = "illegal_state"
    val FAILED = "failed"
    val NONE = "none"
    val SUCCESS = "success"
    val APPEND_TIME = "append_time"
    val APPEND_TIME_RESULT = "append_time_result"
    val LEFT_TIME = "left_time"
    val APPEND_DURATION = "append_duration"
    val TYPE_ADD_FRIEND = "add_friend"
    val SKIPED = "skiped"
    val HAND_OVER = "hand_over"
    val VOTE_TYPE = "type"
    val LOCK = "lock"
    val UNLOCK = "unlock"
    val KICK_OUT = "kick_out"
    val TO = "to"
    val FROM = "from"
    val PING = "ping"
    val MSG_ID = "msg_id"
    val CHANGE_PASSWORD = "change_password"
    val PASSWORD = "password"
    val RESTORE_ROOM = "restore_room"
    val VOICE_TITLE = "title"
    val VOICE_TITLE_TYPE = "type"
    val UPDATE_TITLE = "update_title"
    val TITLE = "title"
    val FORCE_SEAT = "force_seat"
    val UP_SEAT = "up_seat"
    val DOWN_SEAT = "down_seat"
    val HAND_OVER_MASTER = "handover_master"//语音房转让房主
    val MASTER_POSITION = 0//房主座位号
    val CAN_ADD_SPY_NUM = 8
    val MAX_CHAIR = 8//最后一个麦号
    val MAX_CHAIR_12 = 12//最后一个麦号
    val MAX_CHAIR_16 = 16//最后一个麦号
    val MAX_UN_SEAT = 9//麦上最多人数
    val OVER_POSITION = 10086//退出房间就视为去到这个位置
    val LIKE_ROOM = "like_room"
    val TEMPORARY_POSITION = -101//上麦前不下麦的临时麦位
    val CHANGE_USER_STATE = "change_user_state"//语音房更新用户状态
    val SHOW_EMOTION = "show_emoticon"//语音房发表情
    val MODEL = "state"
    val MARK = "mark"
    val URL = "url"
    val THUMBNAIL = "thumbnail"
    val SHOW_GAME = "show_game_emoticon"
    val STAY_TIME = "stay_time"
    val SOUND = "sound"
    val RES = "res"
    val NO_RES = -1
    val MSG = "msg"
    val BALANCE_NO_ENOUGH = 1039//余额不足
    val NO_RECOGNIZED = 1020//无法识别
    val DICE = "dice"
    val LIGHT = "light"
    val HAND_UP = "hand_up"
    val FINGER_GUESSING = "finger_guessing"
    val POINTS = "points"
    val OCTOPUS = "octopus"
    val COINS = "coins"
    val MIC = "mic"
    val GIFTS = "gifts"
    val APPLAUSE = "applause"
    val CHILD_TYPE = "child_type"
    val SOUND_APPLAUSE = "play_applause"//鼓掌音乐id
    val VOICE_BLACK_LIST = "block_user_list"
    val ADD_BLACK = "block"
    val UN_BLACK = "unblock_user"
    val USERS = "users"
    val LIMIT = "limit"
    val FREE = "free"
    val SPACE_STR = ""
    val BACKGROUND = "background"
    val THEME = "theme"
    val VOICE_COUNT = "count"
    val USER_ID = "USER_ID"
    val USER_ID_LOWER = "user_id"
    val USER_ID_CAMEL = "userId"
    val UID = "uid"
    val EVENT_USER_MAIN_PAGE = "EVENT_USER_MAIN_PAGE"
    val EVENT_USER_LEAVE = "EVENT_USER_LEAVE"
    val EVENT_USER_START = "EVENT_USER_START"
    val EVENT_USER_LEAVE_FROM_WEB = "EVENT_USER_LEAVE_FROM_WEB"
    val PROTECT = "protect"
    val PROTECT_RESULT = "protect_result"

    //更新相关//
    val CHECK_UPGRADE = "check_upgrade"
    val DOWNLOAD_APK = "download_apk"
    val INITIATIVE_CHECK = "initiative_check"
    //更新相关//

    val UNACTIVE_WARNING = "unactive_warning"
    val ACTION_CONTINUE = "continue"

    //分享页面页面跳转进应用
    val ROOM_CODE = "room_code"
    val ROOM_PASSWORD = "password"
    val EVENT_START_BROWSABLE = "EVENT_START_BROWSABLE"
    val EVENT_CHAT = "EVENT_CHAT" //聊天事件
    val EVENT_LEAVE_CHAT = "LEAVE_EVENT_CHAT" //离开聊天事件
    val USER_NAME = "USER_NAME"  //用户名
    val USER_ICON = "USER_ICON"  //用户头像
    val IS_FRIEND = "IS_FRIEND"//是否是好友
    val USER_SEX = "USER_SEX"    //用户性别
    val USER_CHAT_TYPE = "USER_CHAT_TYPE" //消息类型
    val CHAT_TIME = "CHAT_TIME"  //消息时间
    val CHAT_MESSAGE = "CHAT_MESSAGE"//聊天的消息
    val PARAMETER_USER_ID = "user_id"
    val PARAMETER_ROOM_ID = "room_id"
    val PARAMETER_TITLE = "title"
    val PARAMETER_FRIEND_ID = "friend_id"
    val MESSAGE_TYPE = "MESSAGE_TYPE"
    val MESSAGE_TYPE_CHAT = "MESSAGE_TYPE_CHAT"
    val RX_BACK_PRESSURE = 500//rx 背压
    //分享页面页面跳转进应用


    //兼容IOS添加的消息字段
    val CONVERSATION_TYPE = "conversationType"//
    val USERNAME = "username"//
    val ACTION_ROBOT = "action_Robot"
    val ACTION_CHECK_TIME = 35
    val DANGEROUS_TIME = 5
    //兼容IOS添加的消息字段


    //日常任务分享/邀请
    val TYPE = "type"
    val COUNT = "count"
    val SHARE_TO_WEIXIN = "share_to_weixin_qq"
    val SHARE_TO_WEIXIN_CIRCLE = "share_to_wxwall"
    val SHARE_TO_QQ = "share_to_weixin_qq"
    val INVITE_TO_QQ = "invite"
    val INVITE_TO_WEIXIN = "invite"
    val INVITE_TO_WEIXIN_CIRCLE = "invite"
    val INVITE_TO_FRIEND = "invite"
    val INVITE = "invite"
    val SHARE = "share"
    //日常任务分享/邀请


    //送礼相关
    val GIFT_FROM = "from"
    val GIFT_TO = "to"
    val GIFT_TYPE = "type"
    val GIFT_CHAT = "gift"
    val GIFT_TICKET = "ticket"
    val SOCKET_SEND_GIFT = "send_gift"
    val SOCKET_SEND_CARD = "send_card"
    val SOCKET_SEND_GIFT_PEER_ID = "peer_id"
    val SOCKET_SEND_GIFT_GIFT_TYPE = "gift_type"
    val SOCKET_SEND_GIFT_CARD_TYPE = "card_type"
    //送礼相关


    //LeanCloud节点问题
    val LEANCLOUD_NODE = "LEANCLOUD_NODE"
    val LEANCLOUD_NODE_CONFIG = "LEANCLOUD_NODE_CONFIG"
    val LEANCLOUD_CN = "cn"
    val LEANCLOUD_US = "us"
    val HOST = "host"
    val LEANCLOUD_NODE_RN = "LEANCLOUD_NODE_RN"
    val LEANCLOUD_NODE_RN_CONFIG = "LEANCLOUD_NODE_RN_CONFIG"
    val LEANCLOUD_SERVER_API = "LEANCLOUD_SERVER_API"
    val LEANCLOUD_SERVER_RTM = "LEANCLOUD_SERVER_RTM"
    val LEANCLOUD_SERVER_NONE = "None"
    val LEANCLOUD_SERVER_API_DEFAULT = "https://intviu-api.leancloud.cn"
    val LEANCLOUD_SERVER_RTM_DEFAULT = "wss://intviu-rtm.leancloud.cn"
    //LeanCloud节点问题


    val UNMUTE_SEND = "unmute_send"
    val OLIVE_CHANGE = "olive_changed"
    val ENGINE_ORBIT = "orbit"
    val ENGINE_GVOICE = "gvoice"

    val CALL_TYPE_GAME = "game"
    val CALL_TYPE_LANGUAGE = "language"
    val CALL_TYPE_JOY = "joy"
    val CALL_TYPE_HIGH_LEVEL = "high_level"
    val EXPORT = "export"
    val EXPORT_PAY = "export_pay"

    /**
     * 权限
     */
    val CONFIG_DATA = "config_data"
    val PREMISSION_KEY = "premission_key"
    /**
     * 广告flag
     */
    val ADV_STATUS = "adv_status"
    val ADV_FAIL_COUNT = "adv_fail_count"
    val ADV_FAIL = "adv_fail"
    val ADV_SUCCESS = "adv_success"

    val REPORT_ESCAPE = "escape"
    val REPORT_AD = "ad"
    val REPORT_SEX = "sex"
    val REPORT_CHEAT = "cheat"
    val REPORT_ABUSE = "abuse"
    val REPORT_GANG_UP = "gang_up"
    val REPORT_TROUBLE = "trouble"

    val SERVER_GET_LOCATION = "lc"
    val CAN_SAVE = "can_save"

    /**
     * 连续登录
     */
    val CONTINUE_LOGIN = "CONTINUE_LOGIN"

    /**
     * 领取奖品
     */
    val MEDIA_RECEIVE_GOLD = "media_receive_gold"
    val OPEN_BOX = "open_box"
    val TYPE_ENTER_GAME_ADV = 1//进入游戏广告展
    val TYPE_DIE_ADV = 2//游戏内死亡后广告展示
    val TYPE_AWARD_ADV = 3//领取奖励后弹出的广告
    val TYPE_GAME_OVER_ADV = 4//游戏结束时，胜负板上的广告
    val TYPE_BANNER_ADV = 5//转盘抽奖，悬浮的广告位
    val TYPE_DISK_ADV = 6//转盘抽奖，领奖的广告

    val ERROR_CODE_NOT_ENOUGH_MONEY = 1008
    val ERROR_CODE_BUY_ERROR = 1001
    val MAX_RETRY = 6

    val LEVEL_LIMIT_NONE = "0"
    val LEVEL_LIMIT_15 = "15"
    val LEVEL_LIMIT_30 = "30"
    val LEVEL_LIMIT_50 = "50"

    val MEDIA_SEER_WOLF = "MEDIA_SEER_WOLF"
    val MEDIA_SEER_WITCH_WOLF = "MEDIA_SEER_WITCH_WOLF"
    val MEDIA_SEER_GUARD_WOLF = "MEDIA_SEER_GUARD_WOLF"
    val MEDIA_SEER_GUARD_WITCH_WOLF = "MEDIA_SEER_GUARD_WITCH_WOLF"

    val SPEECH_DIRECTION_RESULT = "speech_direction_result"
    val SYSTEM_MSG_TYPE_DIALOG = "dialog"
    val SYSTEM_MSG_TYPE_TOAST = "toast"
    val SYSTEM_MSG_TYPE_TEXT = "text"

    val UPDATE_CONFIG = "update_config"

    val RESET_MASTER_RESULT = "reset_master_result"
    val REQUEST_RESET_MASTER = "request_reset_master"
    val REJECT_RESET_MASTER = "reject_reset_master"
    val ALLOW = "allow"


    val CAN_SPEAK = "can_speak"
    val NEED_HAND_OVER = "need_hand_over"

    val ENTER_MODE = "enter_mode"//进入语音房的方式
    val ENTER_FROM_LAUNCH = "enter_from_launch"//从launch进入
    val ENTER_MODE_SMALLWINDOW = "enter_from_small_window"//从小窗进入

    //更换房间背景
    val UPDATE_BG = "update_background"
    val BG_URL = "url"
    val BG_BLUR = "blur_avatar"

    //更换麦位数量
    val UPDATE_SEAT_COUNT = "update_active_count"

    val OBSERVER_LEAVE = "observer_leave"
    val OBSERVER_JOIN = "observer_join"


}