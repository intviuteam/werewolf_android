package orangelab.project.spyroom

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.R
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.common.activity.PersonalInfoActivity
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.common.view.RecycleViewDivider
import orangelab.project.spyroom.adapter.SpyRoomOnLineAdapter
import orangelab.project.spyroom.model.SpyRoomMessenger
import orangelab.project.spyroom.model.SpyRoomPlayer
import orangelab.project.spyroom.model.SpyRoomPlayerComparator
import java.util.*
import kotlin.collections.ArrayList

class SpyRoomOnlineFragment : Fragment(), Destroyable, Keepable {

    private val TAG = "WereWolfOnlineMembersFragment"

    private lateinit var ivBack: View
    private lateinit var tvTitle: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var mContext: Context
    private lateinit var spyRoomContext: SpyRoomContext

    private var memberList: MutableList<SpyRoomPlayer> = ArrayList()
    private var adapter: SpyRoomOnLineAdapter? = null

    fun setContext(spyRoomContext: SpyRoomContext) {
        this.spyRoomContext = spyRoomContext
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.layout_spy_room_online_players, container, false)

        initView(view)
        initData()
        initListener()

        return view
    }

//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View {

//    }

    private fun initView(view: View) {
        ivBack = view.findViewById(R.id.iv_back)
        tvTitle = view.findViewById<TextView>(R.id.tv_title) as TextView
        recyclerView = view.findViewById<RecyclerView>(R.id.list_online_members) as RecyclerView
        recyclerView.layoutManager = WrapContentLinearLayoutManager(mContext)
        recyclerView.addItemDecoration(RecycleViewDivider(mContext, LinearLayoutManager.HORIZONTAL, R.drawable.drawable_split_line_gray))
    }

    private fun initData() {
        adapter = SpyRoomOnLineAdapter(memberList, mContext)
        adapter?.setOnLineMemberClickedListener(object : SpyRoomOnLineAdapter.OnOnLineMemberClickedListener {
            override fun onMemberClick(position: Int) {
                PersonalInfoActivity.Launch(mContext, memberList[position].userId)
            }
        })

        recyclerView.adapter = adapter
        updatePlayerList()
    }

    private fun initListener() {
        spyRoomContext.registerPostProcess(observerHandler)
        ivBack.setOnClickListener { onBackPressed() }
    }

    private fun updatePlayerList() {
        try {
            val allObserverMember: ArrayList<SpyRoomPlayer> = ArrayList()
            allObserverMember.addAll(spyRoomContext.datasource.users)
            Collections.sort(allObserverMember, SpyRoomPlayerComparator())
            this.memberList = allObserverMember
            if (adapter != null) {
                adapter?.updateMemberList(memberList)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val observerHandler = object : SpyRoomProcessable() {
        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            updatePlayerList()
            return false
        }
    }

    private fun onBackPressed() {
        activity?.supportFragmentManager?.popBackStack()
    }

    fun getTAG(): String {
        return TAG
    }

    override fun onDestroy() {
        super.onDestroy()
        destroy()
    }

    override fun destroy() {
        spyRoomContext.unRegisterProcess(observerHandler)
    }
}