package orangelab.project.spyroom.action

import com.toolkit.action.Keepable

/**
 * 进程切换器，根据信息切换到玩家/观战者视角
 */
interface ProcessSwitcher : Keepable {

    /**
     * 前置切换(更改数据前)
     */
    fun switchProcessPre()

    /**
     * 后置切换(更改数据后)
     */
    fun switchProcessPost()
}