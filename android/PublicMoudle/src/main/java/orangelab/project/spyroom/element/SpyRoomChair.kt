package orangelab.project.spyroom.element

import android.graphics.PointF
import android.graphics.drawable.AnimationDrawable
import android.os.Build
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.squareup.picasso.Callback
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.layout_spy_player_right.view.*
import kotlinx.android.synthetic.main.layout_spy_room_item_vote.view.*
import orangelab.project.MainApplication
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.utils.CountDownView
import orangelab.project.common.view.HeadDecorView
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.action.Refreshable
import orangelab.project.spyroom.api.SpyRoomSocketHelper
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.dialog.SpyRoomPlayerDialog
import orangelab.project.spyroom.event.SpyRoomEvent
import orangelab.project.spyroom.model.SpyRoomPlayer
import orangelab.project.voice.gif.BasicComponent
import orangelab.project.voice.utils.SystemScreenUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 座位
 */
class SpyRoomChair : BasicComponent, Refreshable, Destroyable, Keepable {

    companion object {
        private const val TAG = "SpyRoomChair"
    }

    private var position: Int                           //当前位置
    private var contentView: ViewGroup                  //当前UI
    private var player: SpyRoomPlayer? = null           //座上玩家
    private var locked: Boolean = false                 //是否加锁
    private var enable: Boolean = true                  //是否可用
    private var isLeft: Boolean = true                  //是否在左边
    private var countDownView: CountDownView? = null    //计时器

    constructor(position: Int, contentView: ViewGroup) {
        this.position = position
        this.contentView = contentView

        init()
    }

    fun init() {
//        this.enable = (contentView.visibility == View.VISIBLE)
//        this.isLeft = this.position <= (SpyRoomConfig.maxChair ushr 1)
//        if (enable) {
//            PLog.e(TAG, "position-before:$position")
//            updatePosition()
//            PLog.e(TAG, "position-after:$position")
//
//            initListener()
//
//        } else {
//            this.position = -1
//        }

        EventBus.getDefault().register(this)
        updatePosition()
        initListener()
        reset()
    }

    private fun initListener() {

        contentView.iv_spy_room_player_head.setOnClickListener {
            if (!isEmpty()) {
                val spyRoomDialog = SpyRoomPlayerDialog(contentView.context, if (player == null) SpyRoomPlayer() else player!!)
                spyRoomDialog.show()
            }
        }
    }

    /**
     * 接收断网事件
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SpyRoomEvent.GameSocketReconnectEvent) {
        if (!event.isReconnected) {
            //断网关掉speak动画
            endSpeak()
        }
    }

    override fun refresh(data: Refreshable) {

    }

    override fun restore(data: Refreshable) {

    }

    /**
     * 还原UI为最初状态
     */
    override fun reset() {
        contentView.iv_spy_room_player_head.setImageResource(R.mipmap.ic_spy_room_empty)
        contentView.iv_spy_room_player_head.clearHeadDecorate()
        contentView.tv_spy_room_player_name.text = ""
        contentView.tv_spy_room_state_tag.visibility = View.INVISIBLE
        contentView.rl_spy_room_speak_container.visibility = View.INVISIBLE

        turnIntoNameText()
        endVote()
        endSpeak()
        endSpeech()
        endTimer()
        hideVoteTag()
        clearVoteInfo()
        clearOut()
    }

    override fun clear() {
        player = null
        reset()
    }

    /**
     * 是否能上座
     */
    private fun canUpSeat(): Boolean {
        return enable && isEmpty() && !locked
    }

    /**
     * 座位是否是空的
     */
    private fun isEmpty(): Boolean {
        return this.player == null
    }

    /**
     * 切换为名字显示文本
     */
    private fun turnIntoNameText() {
        contentView.tv_spy_room_player_name.setOnClickListener { }
        contentView.tv_spy_room_player_name.isClickable = false
        contentView.tv_spy_room_player_name.setBackgroundResource(R.mipmap.ic_spy_room_bg_name)
        if (!isEmpty()) {
            contentView.tv_spy_room_player_name.text = player!!.userName
        }
    }

    /**
     * 切换为投票按钮
     */
    private fun turnIntoVoteButton() {
        contentView.tv_spy_room_player_name.setBackgroundResource(R.drawable.bg_spy_room_vote_selector)
        if (isAlive()) {
            contentView.tv_spy_room_player_name.setText(R.string.str_spy_room_vote)
            contentView.tv_spy_room_player_name.setOnClickListener {
                SpyRoomSocketHelper.sendSpyGameVote(this.position)
            }
        }
    }

    /**
     * 切换为结束发言按钮
     */
    private fun turnIntoEndSpeechButton() {
        contentView.tv_spy_room_player_name.setBackgroundResource(R.drawable.bg_spy_room_vote_selector)
        if (!isEmpty()) {
            contentView.tv_spy_room_player_name.setText(R.string.str_spy_room_end_speech)
            contentView.tv_spy_room_player_name.setOnClickListener {
                SpyRoomSocketHelper.sendEndSpeech()
            }
        }
    }

    /**
     * 获取在屏幕上的坐标
     */
    fun getPositionOnScreen(): PointF {
        val location = IntArray(2)
        contentView.iv_spy_room_player_head.getLocationOnScreen(location)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            location[1] -= SystemScreenUtils.getStatusBarHeight()
        }
        return PointF(location[0].toFloat(), location[1].toFloat())
    }

    fun getHeadWidth(): Int {
        return contentView.findViewById<HeadDecorView>(R.id.iv_spy_room_player_head).measuredWidth
    }

    fun getHeadHeight(): Int {
        return contentView.findViewById<HeadDecorView>(R.id.iv_spy_room_player_head).measuredHeight
    }

    /**
     * 清空投票信息
     */
    fun clearVoteInfo() {
        contentView.ll_spy_room_vote_num_layout.removeAllViews()
        contentView.ll_spy_room_vote_num_layout.visibility = View.GONE
        contentView.ll_spy_room_vote_container.visibility = View.GONE
    }

    /**
     * 被投票
     */
    fun beVote(player: SpyRoomPlayer) {
        try {
            val view = LayoutInflater.from(contentView.context).inflate(R.layout.layout_spy_room_item_vote, null)
            view.tv_spy_room_item_vote_position.text = (player.position + 1).toString()
            PicassoUtils.loadImageByDefault(contentView.context.applicationContext,
                    player.headImageUrl,
                    view.iv_spy_room_item_vote_head,
                    R.mipmap.default_head)
            contentView.ll_spy_room_vote_num_layout.addView(view)
            contentView.ll_spy_room_vote_num_layout.visibility = View.VISIBLE
            contentView.ll_spy_room_vote_container.visibility = View.VISIBLE
        } catch (e: Exception) {
            PLog.e(TAG, "error when be vote:${e.message}")
        }
    }

    /**
     * 出局
     */
    fun out() {
        endSpeak()
        endSpeech()
        endTimer()
        endVote()
        clearVoteInfo()
        contentView.iv_spy_room_tag_out.visibility = View.VISIBLE
        if (!isEmpty()) {
            if (player!!.isSpy()) {
                contentView.iv_spy_room_tag_role.setImageResource(R.mipmap.ic_spy_room_tag_spy)
            } else {
                contentView.iv_spy_room_tag_role.setImageResource(R.mipmap.ic_spy_room_tag_good)
            }
            contentView.iv_spy_room_tag_role.visibility = View.VISIBLE
            contentView.iv_spy_room_out_cover.visibility = View.VISIBLE
        }
    }

    /**
     * 清除死亡UI
     */
    fun clearOut() {
        contentView.iv_spy_room_tag_out.visibility = View.GONE
        contentView.iv_spy_room_tag_role.visibility = View.GONE
        contentView.iv_spy_room_out_cover.visibility = View.GONE
    }

    /**
     * 重开游戏
     */
    fun reOpenGame() {
        unPrepare()
        endSpeech()
        endSpeak()
        endVote()
        hideVoteTag()
        clearVoteInfo()
        clearOut()
    }

    //消息处理 start==================================================================================//
    fun upSeat(player: SpyRoomPlayer) {
        if (canUpSeat()) {
            this.player = player
            contentView.tv_spy_room_player_name.text = player.userName
            contentView.tv_spy_room_player_name.handleVipInfo(player.vipInfo)

            PicassoUtils.loadImageByDefault(MainApplication.getInstance(), player.headImageUrl, contentView.iv_spy_room_player_head.realHeadImage, R.mipmap.default_head)
            contentView.iv_spy_room_player_head.setHeadDecorate(player.headDecor)
            //发言状态检测
            if (player.speech) {
                startSpeech()
            } else {
                endSpeech()
            }

            //说话状态检测
            if (player.speaking) {
                speak()
            } else {
                endSpeak()
            }

            //房主状态检测
            if (player.isMaster) {
                changeToMaster()
            } else {
                changeToNormal()
            }

            //准备状态检测
            if (player.prepare) {
                prepare()
            } else {
                unPrepare()
            }

            //死亡状态检测
            if (player.isOut) {
                out()
            }
        }
    }

    fun downSeat() {
        this.player = null
        reset()
    }

    fun prepare() {
        if (!isEmpty() && !player!!.isMaster && !SpyRoomConfig.isGaming) {
            contentView.tv_spy_room_state_tag.setText(R.string.str_spy_room_prepare)
            contentView.tv_spy_room_state_tag.visibility = View.VISIBLE
        }
    }

    fun unPrepare() {
        if (!isEmpty() && !player!!.isMaster) {
            contentView.tv_spy_room_state_tag.text = ""
            contentView.tv_spy_room_state_tag.visibility = View.INVISIBLE
        }
    }

    fun startSpeech() {
        if (isAlive() && SpyRoomConfig.isGaming) {
            contentView.rl_spy_room_speak_container.visibility = View.VISIBLE
            contentView.iv_spy_room_speak_anim.setImageResource(R.mipmap.ic_spy_room_speak_layer1)
            contentView.iv_spy_room_speak_anim.visibility = View.VISIBLE

            PLog.e(TAG, "$position: ${player!!.speechTime}")
            startTimer(player!!.speechTime)

            //自己在说话，就展示"结束发言"按钮
            if (SpyRoomHelper.isSelf(position)) {
                turnIntoEndSpeechButton()
            }
        }
    }

    fun endSpeech() {
        endSpeak()
        endTimer()
        contentView.iv_spy_room_speak_anim.visibility = View.GONE
        contentView.rl_spy_room_speak_container.visibility = View.INVISIBLE

        //自己发言结束，关闭"结束发言"按钮
        if (SpyRoomHelper.isSelf(position)) {
            turnIntoNameText()
        }
    }

    fun speak() {
        if (!isEmpty()) {
            PLog.e(TAG, "speaking + $position")
            contentView.rl_spy_room_speak_container.visibility = View.VISIBLE
            contentView.iv_spy_room_speak_anim.visibility = View.VISIBLE
            try {
                contentView.rl_spy_room_speak_container.iv_spy_room_speak_anim.setImageResource(R.drawable.bg_spy_room_speak_anim)
                (contentView.rl_spy_room_speak_container.iv_spy_room_speak_anim.drawable as AnimationDrawable).start()
            } catch (e: Exception) {
                PLog.e(TAG, "do speak in spy room error, message is $e")
            }
        }
    }

    fun endSpeak() {
        try {
            (contentView.rl_spy_room_speak_container.iv_spy_room_speak_anim.drawable as AnimationDrawable).stop()
        } catch (e: Exception) {
            PLog.e(TAG, "do end speak in spy room error, message is $e")
        }
        contentView.iv_spy_room_speak_anim.setImageResource(R.mipmap.ic_spy_room_speak_layer1)
        if (isEmpty()) {
            contentView.iv_spy_room_speak_anim.visibility = View.GONE
            contentView.rl_spy_room_speak_container.visibility = View.INVISIBLE
        } else {
            if (!player!!.speech) {
                contentView.iv_spy_room_speak_anim.visibility = View.GONE
                contentView.rl_spy_room_speak_container.visibility = View.INVISIBLE
            }
        }
    }

    fun startVote() {
        //自己没死，要投的那个人没死，游戏在进行，才显示要投的那个人的按钮
        if (!SpyRoomHelper.isOut() && isAlive() && SpyRoomConfig.isGaming) {
            turnIntoVoteButton()
            showVoteTag()
        }
    }

    fun endVote() {
        turnIntoNameText()
    }

    fun showVoteTag() {
        if (SpyRoomConfig.isGaming && isAlive()) {
            contentView.iv_spy_room_tag_vote.visibility = View.VISIBLE
        }
    }

    fun hideVoteTag() {
        contentView.iv_spy_room_tag_vote.visibility = View.GONE
    }

    fun lock() {
        downSeat()
        contentView.iv_spy_room_player_head.setImageResource(R.mipmap.lock_voice_chair)
        contentView.iv_spy_room_player_head.clearHeadDecorate()
        locked = true
    }

    fun unLock() {
        reset()
        locked = false
    }

    fun changeToMaster() {
        if (!isEmpty() && player!!.isMaster) {
            contentView.tv_spy_room_state_tag.setText(R.string.str_spy_room_master)
            contentView.tv_spy_room_state_tag.visibility = View.VISIBLE
        }
    }

    fun changeToNormal() {
        if (!isEmpty() && !player!!.isMaster) {
            if (player!!.prepare) {
                prepare()
            } else {
                contentView.tv_spy_room_state_tag.text = ""
                contentView.tv_spy_room_state_tag.visibility = View.INVISIBLE
            }
        }
    }

    private fun startTimer(time: Int) {
        if (SpyRoomConfig.isGaming) {

            contentView.tv_spy_room_vote_timer.visibility = View.VISIBLE
            countDownView = CountDownView(contentView.tv_spy_room_vote_timer, time, CountDownView.CountDownFinish {
                contentView.tv_spy_room_vote_timer.visibility = View.INVISIBLE
                changeSpeech()
                endSpeak()
            })
            countDownView?.start()
        }
    }

    private fun endTimer() {
        if (countDownView != null) {
            countDownView?.cancel()
        }
        contentView.tv_spy_room_vote_timer.visibility = View.INVISIBLE
    }

    private fun changeSpeech() {
        if (!isEmpty()) {
            player!!.speech = false
            player!!.speechTime = 0
            if (SpyRoomHelper.isSelf(player!!.userId)) {
                val personalData = PersonalDataHelper.personalData
                if (personalData != null) {
                    personalData.isSpeech = false
                    personalData.speechTime = 0
                }
            }
        }
    }

    private fun isAlive(): Boolean {
        return !isEmpty() && !player!!.isOut
    }

    private fun updatePosition() {
        //展示的座位数和初始化的座位数不相同
//        if (SpyRoomConfig.maxChair < SpyRoomConfig.maxRealChair) {
//            //该座位在右侧
//            if (this.position > (SpyRoomConfig.maxRealChair ushr 1)) {
//                //座位号前移
//                this.position -= ((SpyRoomConfig.maxRealChair - SpyRoomConfig.maxChair) ushr 1)
//            }
//        }
        contentView.tv_spy_room_position.text = (position + 1).toString()

    }
    //消息处理 end====================================================================================//


    override fun destroy() {
        super.destroy()
        EventBus.getDefault().unregister(this)
        clear()
    }
}