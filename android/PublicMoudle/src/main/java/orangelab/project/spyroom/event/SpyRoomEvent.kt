package orangelab.project.spyroom.event

import android.support.annotation.Keep
import com.toolkit.action.Keepable

class SpyRoomEvent : Keepable {

    /**
     * 断线重连事件
     */
    class GameSocketReconnectEvent(var isReconnected: Boolean) : Keepable

    /**
     * 加好友dialog已处理事件
     */
    class SpyRoomFriendHandled(var id: String) : Keepable
}