package orangelab.project.spyroom.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.R;

import orangelab.project.game.dialog.WereWolfBaseDialog;

/**
 * game_werewolf
 * 2017/12/2 下午5:36
 * Mystery
 */

public class SpyRoomWarnLeaveDialog extends SpyRoomBaseDialog {

    private TextView mTextView;
    private boolean enableLeftWhenCounting;
    private boolean enableRightWhenCounting;


    public SpyRoomWarnLeaveDialog(@NonNull Context context) {
        super(context);
        View view = View.inflate(context, R.layout.layout_spy_room_dialog_hint, null);
        mTextView = (TextView) view.findViewById(R.id.tv_spy_room_warn_leave_hint);
        setDialogContentView(view);
        setButtonType(TWO_BUTTON);
    }

    public SpyRoomWarnLeaveDialog setHintTitle(String title) {
        setTitle(title);
        return this;
    }

    public SpyRoomWarnLeaveDialog setHintMessage(String msg) {
        mTextView.setText(msg);
        return this;
    }

    public SpyRoomWarnLeaveDialog setHintButtonType(int type) {
        setButtonType(type);
        return this;
    }

    public SpyRoomWarnLeaveDialog setHintLeftButtonText(String text) {
        if (getTwoButtonLeft() != null) {
            getTwoButtonLeft().setText(text);
        }
        return this;
    }

    public SpyRoomWarnLeaveDialog setHintRightButtonText(String text) {
        if (getTwoButtonRight() != null) {
            getTwoButtonRight().setText(text);
        }
        return this;
    }

    public SpyRoomWarnLeaveDialog setLeftOnClick(View.OnClickListener onClick) {
        if (getTwoButtonLeft() == null) {
            return this;
        }
        getTwoButtonLeft().setOnClickListener(v -> {
            if (isCounting() && !this.enableLeftWhenCounting) {
                return;
            }
            if (onClick != null) {
                onClick.onClick(v);
            }
        });
        return this;
    }

    public SpyRoomWarnLeaveDialog setRightOnClick(View.OnClickListener onClick) {
        if (getTwoButtonRight() == null) {
            return this;
        }
        getTwoButtonRight().setOnClickListener(v -> {
            if (isCounting() && !this.enableRightWhenCounting) {
                return;
            }
            if (onClick != null) {
                onClick.onClick(v);
            }
        });
        return this;
    }

    public SpyRoomWarnLeaveDialog setDismissWhenCountDownFinish(boolean flag) {
        setDismissWhenFinish(flag);
        return this;
    }

    public SpyRoomWarnLeaveDialog setEnableLeftWhenCounting(boolean flag) {
        this.enableLeftWhenCounting = flag;
        return this;
    }

    public SpyRoomWarnLeaveDialog setEnableRightWhenCounting(boolean flag) {
        this.enableRightWhenCounting = false;
        return this;
    }

    public SpyRoomWarnLeaveDialog setHintDismissWhenFinish(boolean flag) {
        setDismissWhenFinish(flag);
        return this;
    }

    public SpyRoomWarnLeaveDialog setActionWhenCountDownFinish(Runnable runnable) {
        setCountDownFinish(runnable);
        return this;
    }

    public SpyRoomWarnLeaveDialog(@NonNull Context context,
                                  String title,
                                  String message,
                                  View.OnClickListener right,
                                  View.OnClickListener left,
                                  boolean enableLeftWhenCounting,
                                  boolean enableRightWhenCounting) {
        this(context);

        setTitle(title);

        setHintMessage(message);

        setEnableLeftWhenCounting(enableLeftWhenCounting);

        setEnableRightWhenCounting(enableRightWhenCounting);

        setDismissWhenFinish(false);

        setLeftOnClick(left);

        setRightOnClick(right);

        if (!enableRightWhenCounting) {
            getTwoButtonRight().setBackgroundResource(R.drawable.bg_spy_room_common_orange);
            getTwoButtonRight().setEnabled(false);
        }

        if (!enableLeftWhenCounting) {
            getTwoButtonRight().setBackgroundResource(R.drawable.bg_spy_room_common_white);
            getTwoButtonRight().setEnabled(false);
        }

        setCountDownFinish(() -> {

            getTwoButtonRight().setBackgroundResource(R.drawable.bg_spy_room_common_orange);
            getTwoButtonRight().setEnabled(true);

            getTwoButtonLeft().setBackgroundResource(R.drawable.bg_spy_room_common_white);
            getTwoButtonLeft().setEnabled(true);

        });
    }

    public SpyRoomWarnLeaveDialog(@NonNull Context context,
                                  String title,
                                  String message,
                                  View.OnClickListener right,
                                  View.OnClickListener left
    ) {
        this(context, title, message, right, left, true, false);
    }
}
