package orangelab.project.spyroom

import android.text.TextUtils
import com.toolkit.action.Keepable
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.model.SpyRoomMessenger

/**
 * 消息处理器
 */
abstract class SpyRoomSocketMessageHandler : SpyRoomProcessable(), Keepable {

    /**
     * 消息分配
     */
    //逻辑处理
    fun handleSocketEvent(spyRoomMessenger: SpyRoomMessenger) {
        val type = spyRoomMessenger.type
        when {
            TextUtils.equals(SpyRoomConstants.TYPE_JOIN, type) -> handleJoin(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.LEAVE, type) -> handleLeave(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPEAK, type) -> handleSpeak(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.UNSPEAK, type) -> handleUnSpeak(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.KICK_OUT, type) -> handleKickOut(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.TYPE_CHAT, type) -> handleChat(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SYSTEM_MSG, type) -> handleSystemMessage(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.UP_SEAT, type) -> handleUpSeat(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.DOWN_SEAT, type) -> handleDownSeat(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.RESTORE_ROOM, type) -> handleRestore(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.HAND_OVER_MASTER, type) -> handleChangeRoomOwner(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.TYPE_UPDATE_MASTER, type) -> handleUpdateMaster(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.CHANGE_PASSWORD, type) -> handleChangeRoomPassword(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.TYPE_ADD_FRIEND, type) -> handleAddFriend(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_START_CONFIG, type) -> handleStartConfig(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_UPDATE_CONFIG, type) -> handleUpdateConfig(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_STOP_CONFIG, type) -> handleStopConfig(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_PREPARE, type) -> handlePrepare(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.UNPREPARE, type) -> handleUnPrepare(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_VOTE_RESULT, type) -> handleVoteResult(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_DEATH_INFO, type) -> handleDeathInfo(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_GUESS_WORD, type) -> handleGuessWord(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_GUESS_WORD_RESULT, type) -> handleGuessWordResult(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_ASSIGNED_ROLE, type) -> handleAssignRole(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_START, type) -> handleGameStart(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_SPEECH, type) -> handleStartSpeech(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_END_SPEECH, type) -> handleEndSpeech(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_VOTE, type) -> handleStartVote(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_OVER, type) -> handleGameOver(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_DISCONNECT, type) -> handleDisconnect(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.SPY_GAME_CONNECT, type) -> handleReconnect(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.UPDATE_CONFIG, type) -> handleUpdateConfig(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.OBSERVER_JOIN, type) -> handleObserverJoin(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.OBSERVER_LEAVE, type) -> handleObserverLeave(spyRoomMessenger)
            TextUtils.equals(SpyRoomConstants.UNACTIVE_WARNING, type) -> handleWarnLeave(spyRoomMessenger)
        }
    }
}