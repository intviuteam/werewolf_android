package orangelab.project.spyroom

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import cn.intviu.orbit.OrbitError
import cn.intviu.support.ReportRobot
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.transport.TransportHelper
import com.datasource.GlobalUserState
import com.networktoolkit.transport.Constant
import com.tbruyelle.rxpermissions.RxPermissions
import com.umeng.analytics.MobclickAgent
import com.umeng.socialize.UMShareAPI
import orangelab.project.MainApplication
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.activity.RoomSafeActivity
import orangelab.project.common.activity.SafeActivity
import orangelab.project.common.dialog.LoadingDialog
import orangelab.project.common.dialog.MsgDialog
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.event.ActivityEvent
import orangelab.project.common.event.ServerEvent
import orangelab.project.common.event.SocketEvent
import orangelab.project.common.event.ViewEvent
import orangelab.project.common.exhibition.card.CardEvent
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.exhibition.gift.GiftEvent
import orangelab.project.common.model.SystemMessageItem
import orangelab.project.common.union.UnifiedBridgeHelper
import orangelab.project.common.utils.IntentDataHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.ReportEventUtils
import orangelab.project.common.utils.Utils
import orangelab.project.game.dialog.WereWolfHintDialog
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.dialog.SpyRoomLeaveDialog
import orangelab.project.spyroom.event.SpyRoomEvent
import orangelab.project.spyroom.manager.SpyRoomAudioSocketManager
import orangelab.project.spyroom.manager.SpyRoomGameSocketManager
import orangelab.project.spyroom.model.SpyRoomBridgeBean
import orangelab.project.spyroom.model.SpyRoomMessenger
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rx.android.schedulers.AndroidSchedulers

class SpyRoomActivity : RoomSafeActivity() {


    private var mBridgeBean: SpyRoomBridgeBean? = null
    private var engineToken: String = ""

    private var isSocketReconnectedFailed = false
    private lateinit var spyRoomUIController: SpyRoomUIController
    private lateinit var spyRoomContext: SpyRoomContext

    //弹出式UI==============================================
    private lateinit var quitDialogNormal: SpyRoomLeaveDialog   //退出对话框，无小窗选项
    private lateinit var mExitLoadingDialog: LoadingDialog      //退出时的延迟对话框
    private lateinit var permissionDialog: MsgDialog            //提示权限对话框
    //==============================================

    companion object {
        private const val TAG = "SpyRoomActivity"

        fun launch(context: Context, bridgeBean: SpyRoomBridgeBean) {
            TransportHelper.putTransportable(TAG, bridgeBean)

            val intent = Intent(context, SpyRoomActivity::class.java)

            IntentDataHelper.setUserName(intent, GlobalUserState.getGlobalState().userName)
            IntentDataHelper.setUserId(intent, GlobalUserState.getGlobalState().userId)
            IntentDataHelper.setUserSex(intent, GlobalUserState.getGlobalState().userSex)
            IntentDataHelper.setUserToken(intent, GlobalUserState.getGlobalState().token)
            IntentDataHelper.setUserAvater(intent, GlobalUserState.getGlobalState().userIcon)
            IntentDataHelper.setGameType(intent, bridgeBean.gameType)
            IntentDataHelper.setRoomPassword(intent, bridgeBean.password)
            IntentDataHelper.setUserExp(intent, GlobalUserState.getGlobalState().userExp)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //检测socket是否恶意中断
        checkStateAndClearResidualData()

        //检测数据是否恶意销毁
        val temp = TransportHelper.getTransport<SpyRoomBridgeBean>(TAG)
        if (temp != null) {
            mBridgeBean = temp
            engineToken = temp.engineToken
        }
        if (mBridgeBean == null) {
            sendLeaveSignal()
            return
        }

        GlobalUserState.getGlobalState().curGameType = IntentDataHelper.getGameType(intent)
        ReportEventUtils.reportGameEnter()
        GlobalUserState.getGlobalState().isGaming = true

        //初始化并显示UI
        init()

        //检测权限是否开启
        requestPermission()
    }

    /**
     * 检察socket连接状态，未连接就退出
     */
    private fun checkStateAndClearResidualData() {
        if (!MainApplication.getInstance().socketIsExit()) {
            finish()
            return
        }
        NotificationUtils.IMPL().clearAllNotification(this)
    }

    /**
     * 检测麦克风权限，未授权就退出
     */
    private fun requestPermission() {
        val rxPermissions = RxPermissions(this)
        rxPermissions.request(Manifest.permission.RECORD_AUDIO)
                .onBackpressureBuffer(SpyRoomConstants.RX_BACK_PRESSURE.toLong())
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { aBoolean ->
                    if (aBoolean!!) {
                        initVoiceEngine()
                    } else {
                        RxToolKit.Emit(OrbitError(OrbitError.AUDIO_RECORD_NO_PERMISSION))
                    }
                }
    }

    fun init() {
        spyRoomContext = SpyRoomContext(mBridgeBean!!, intent)

        initWindow()
        initView()
        initListener()

    }

    /**
     * 屏幕初始化
     */
    private fun initWindow() {
        //状态栏一体化
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val decorView = window.decorView
                val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                decorView.systemUiVisibility = option
                window.statusBarColor = Color.TRANSPARENT
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        //屏幕常亮
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
    }

    fun initView() {
        setContentView(R.layout.activity_spy_room)
        val contentView = findViewById<RelativeLayout>(R.id.rl_spy_room_content_view)
        spyRoomUIController = SpyRoomUIController(contentView as ViewGroup, spyRoomContext)
    }

    private fun initListener() {
        EventBus.getDefault().register(this)
        initServerMessageEvent()
        initGameSocketConnectedStateListener()
        initAudioSocketConnectedStateListener()
        initOrbitErrorListener()
        initViewListener()
        initSocketSendGiftListener()
        initGiftEvent()

        spyRoomContext.registerPostProcess(observer)
        spyRoomContext.startProcess()
        SpyRoomGameSocketManager.start()
    }

    //处理socket消息 start==========================================================//
    private fun initServerMessageEvent() {
        //VoiceServerMessageHandler.getInstance().registerUIHandler(messageHandler)
    }
    //处理socket消息 end==========================================================//

    //游戏服务器连接状态监听
    private fun initGameSocketConnectedStateListener() {
        //Socket断开重连的回调，失败就强制退出
        RxToolKit.Build(this, SocketEvent.ReConnectedFailedEvent::class.java)
                .action {
                    isSocketReconnectedFailed = true
                    exitRoomCompletely()
                }.register()
    }

    /**
     * 游戏socket连接状态的监听
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SpyRoomEvent.GameSocketReconnectEvent) {
        if (event.isReconnected) {
            reconnect()
        } else {
            disconnect()
        }
    }

    //语音服务器连接状态监听
    private fun initAudioSocketConnectedStateListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent::class.java)
                .action { event ->
                    Utils.runSafely {
                        when (event.action) {
                            ServerEvent.RECONNECT_FROM_AUDIO_SERVER -> spyRoomUIController.changeAudioSocketConnectedState(true)
                            ServerEvent.DISCONNECT_FROM_AUDIO_SERVER -> spyRoomUIController.changeAudioSocketConnectedState(false)
                        }
                    }
                }.register()
    }

    //view相关处理的回调
    private fun initViewListener() {
        //点击送礼按钮展示送礼dialog
//        RxToolKit.Build(this, ExhibitionsEvent.ExhibitionsByGameEvent::class.java)
//                .action { event ->
//                    runOnUiThreadSafely {
//                        val dialog = VoiceDialogSendGift(this@SpyRoomActivity, VoiceRoomDataSourceManager.getInstance().giftUserList)
//                        dialog.show()
//                    }
//                }.register()

        spyRoomUIController.onBackPressed(View.OnClickListener { onBack() })
    }

    /**
     * orbit出错回调
     */
    private fun initOrbitErrorListener() {
        RxToolKit.Build(this, OrbitError::class.java)
                .action { error ->
                    runOnUiThreadSafely {
                        when (error.errorCode) {
                            OrbitError.AUDIO_RECORD_BEEN_RACE, OrbitError.AUDIO_RECORD_NO_PERMISSION -> {
                                run {
                                    buildPermissionDialogCaseNoPermission()
                                    permissionDialog.show()
                                }
                                ReportRobot.robot.report("AUDIO_PERMISSION_FAIL_ALL")
                            }
                        }
                    }
                }.register()
    }

    /**
     * 权限提示
     */
    private fun buildPermissionDialogCaseNoPermission() {
        permissionDialog = MsgDialog(
                this,
                MessageUtils.getString(R.string.dialog_hint),
                MessageUtils.getString(R.string.audio_permission_error_in_gaming)
        ) {
            permissionDialog.dismiss()
            sendLeaveSignal()
        }
    }

    //送礼相关 start======================================================
    private fun initSocketSendGiftListener() {
        RxToolKit.Build(this, SocketEvent.SocketSendGiftEvent::class.java)
                .action { event ->
                    runOnUiThreadSafely {
                        RoomSocketEngineHelper.SendGiftMessage(event.peer_id, event.gift_type,
                                { result1 ->
                                    runOnUiThreadSafely {
                                        if (event.successCallBack != null) {
                                            event.successCallBack.func(result1)
                                        }
                                    }
                                }
                        ) { result12 ->
                            runOnUiThreadSafely {
                                if (event.failedCallBack != null) {
                                    event.failedCallBack.func(result12)
                                }
                            }
                        }
                    }
                }.register()

        RxToolKit.Build(this, SocketEvent.SocketSendCardEvent::class.java)
                .action { event ->
                    runOnUiThreadSafely {
                        RoomSocketEngineHelper.SendCardMessage(event.peer_id, event.gift_type,
                                { result1 ->
                                    runOnUiThreadSafely {
                                        if (event.successCallBack != null) {
                                            event.successCallBack.func(result1)
                                        }
                                    }
                                }
                        ) { result2 ->
                            runOnUiThreadSafely {
                                if (event.failedCallBack != null) {
                                    event.failedCallBack.func(result2)
                                }
                            }
                        }
                    }
                }.register()
    }

    private fun initGiftEvent() {
        //送礼
        RxToolKit.Build(this, GiftEvent.GiftForGameWrapperEvent::class.java)
                .action { event ->
                    val position = event.exhibitionPackage.toPosition
                    val user = spyRoomContext.datasource.findUserByPosition(position)
                    RoomSocketEngineHelper.sendGiftChatMessage(
                            MessageUtils.getString(
                                    R.string.str_send_gift_msg,
                                    user!!.userName,
                                    EffectsManager.GetGiftNameByType(event.gift.giftType)
                            ),
                            event.exhibitionPackage.fromPosition,
                            event.exhibitionPackage.toPosition,
                            event.gift.giftType,
                            event.gift.giftTicket
                    )
                }.register()

        //送卡片
        RxToolKit.Build(this, CardEvent.CardForGameWrapperEvent::class.java)
                .action { event ->
                    val position = event.exhibitionPackage.toPosition
                    val user = spyRoomContext.datasource.findUserByPosition(position)
                    RoomSocketEngineHelper.sendGiftChatMessage(
                            MessageUtils.getString(
                                    R.string.str_send_gift_msg,
                                    user!!.userName,
                                    EffectsManager.GetCardNameByType(event.card.realType)
                            ),
                            event.exhibitionPackage.fromPosition,
                            event.exhibitionPackage.toPosition,
                            event.card.realType,
                            event.card.cardTicket
                    )
                }.register()
    }
    //送礼相关 end======================================================


    /**
     * 启动音频服务器
     */
    private fun initVoiceEngine() {
        if (SpyRoomConfig.isFromSW) {
            //小窗模式直接通知一下即可
            MainApplication.getInstance().socketEngineHelper.startPollMessage()
            //更新一下语音连接状态
            //changeNetWorkState(SpyRoomAudioSocketManager.isConnected())
        } else {
            if (spyRoomContext.datasource.audioConfigBean != null) {
                SpyRoomAudioSocketManager.start(spyRoomContext.datasource.audioConfigBean!!)
            }
        }
    }


    /**
     * 发送离开信号
     */
    private fun sendLeaveSignal() {
        SpyRoomAudioSocketManager.shutDown(Runnable {
            runOnUiThreadSafely { this.exitGameRoom() }
        })
    }

    //正常退出房间，收到语音服务器断开的消息才调用
    private fun exitGameRoom() {
        try {
            if (quitDialogNormal.isShowing) {
                quitDialogNormal.dismiss()
            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }

        quitToMain()
        finish()
    }

    //正常退出 1释放资源 2关闭两个socket
    private fun quitToMain() {
        try {
            destroyResource()
            shutDownSocket()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //正常退出 销毁资源
    private fun destroyResource() {
        PLog.i(TAG, "Exec Destroy: ")
        UnifiedBridgeHelper.SendLeaveAudioRoomAction()
        RoomSocketEngineHelper.userLeaveReportRN()
        RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(SpyRoomConstants.LEAVE, null, null, null)
        ReportEventUtils.reportGameLeaveIsGaming()
        releaseMessage()
        showExitLoadingDialog()
        GlobalUserState.getGlobalState().isGaming = false
        SpyRoomConfig.clearConfig()//退出房间，清除配置信息
        GlobalUserState.getGlobalState().curGameType = ""
        EventBus.getDefault().post(FinishConversationEvent())
        RxToolKit.Emit(FinishConversationEvent())
        ReportEventUtils.reportGameLeave()
        PersonalDataHelper.setPersonalData(null)
        spyRoomContext.datasource.destroy()
    }

    //正常退出 关闭两个socket
    private fun shutDownSocket() {

        //停止消息监听
        spyRoomContext.destroy()

        //关闭游戏socket
        SpyRoomGameSocketManager.shutDown(engineToken)
        SpyRoomAudioSocketManager.destroy()
    }

    //彻底退出房间
    private fun exitRoomCompletely() {
        if (isSocketReconnectedFailed) {
            //socket断开重连失败的提示dialog
            if (!isFinishing) {
                SpyRoomAudioSocketManager.shutDown(Runnable {
                    runOnUiThreadSafely {
                        quitDialogNormal = SpyRoomLeaveDialog(
                                this@SpyRoomActivity,
                                MessageUtils.getString(R.string.dialog_hint),
                                MessageUtils.getString(R.string.str_voice_leave_room),
                                { sendLeaveSignal() },
                                { sendLeaveSignal() })
                        quitDialogNormal.negative(false)
                        quitDialogNormal.setCanceledOnTouchOutside(false)
                        quitDialogNormal.show()
                    }
                })
            }
        } else {
            sendLeaveSignal()
        }
    }

    //展示退出dialog
    private fun showExitLoadingDialog() {
        try {
            if (mExitLoadingDialog.isShowing) {
                mExitLoadingDialog.dismiss()
            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }

        mExitLoadingDialog = LoadingDialog(this)
        mExitLoadingDialog.setCanceledOnTouchOutside(false)
        mExitLoadingDialog.setMessage(MessageUtils.getString(R.string.gvoice_quit_room))
        mExitLoadingDialog.show()
    }

    //关闭退出dialog
    private fun dismissExitLoadingDialog() {
        try {
            if (mExitLoadingDialog.isShowing) {
                mExitLoadingDialog.dismiss()
            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    //重新连接
    private fun reconnect() {
        spyRoomContext.datasource.addMessage(
                SpyRoomConstants.CODE_CONNECT,
                SystemMessageItem.createSystemMessage(
                        resources.getColor(R.color.color_voice_net_status),
                        MessageUtils.getString(R.string.socket_reconnect)))
        spyRoomUIController.changeGameSocketConnectedState(true)
        spyRoomUIController.enableTouch()
    }

    //断开连接
    private fun disconnect() {
        spyRoomContext.datasource.addMessage(
                SpyRoomConstants.CODE_DISCONNECT,
                SystemMessageItem.createSystemMessage(
                        resources.getColor(R.color.color_voice_net_status),
                        MessageUtils.getString(R.string.socket_disconnect)))
        spyRoomUIController.changeGameSocketConnectedState(false)
        spyRoomUIController.disableTouch()
    }

    //不支持小窗模式下按回退键
    private fun popQuitDialogNormal() {
        val text: String = if (canQuitRoomWhilePlayingFilter()) {
            MessageUtils.getString(R.string.leave_the_room)
        } else {
            MessageUtils.getString(R.string.str_spy_room_leave_msg)
        }
        quitDialogNormal = SpyRoomLeaveDialog(this,
                MessageUtils.getString(R.string.dialog_hint),
                text,
                { sendLeaveSignal() },
                { quitDialogNormal.dismiss() }
        )
        quitDialogNormal.negative(true)
        quitDialogNormal.show()
    }

    //是否需要转让房主的过滤器
    private fun needHandOverMasterFilter(): Boolean {
        //房间人数大于1，自己是房主，网络连接，才提示转让房主
        return spyRoomContext.datasource.users.size > 1 &&
                SpyRoomHelper.isMaster() &&
                SpyRoomGameSocketManager.isConnected
    }

    //游戏中是否能离开房间
    private fun canQuitRoomWhilePlayingFilter(): Boolean {
        //正在游戏中，自己是玩家，网络连接，就不能退出房间
        PLog.e(TAG, "isGaming = ${SpyRoomConfig.isGaming}")
        PLog.e(TAG, "isPlayer = ${SpyRoomHelper.isPlayer()}")
        PLog.e(TAG, "isConnected = ${SpyRoomGameSocketManager.isConnected}")
        return !(SpyRoomConfig.isGaming &&
                SpyRoomHelper.isPlayer() &&
                SpyRoomGameSocketManager.isConnected)
    }

    //反注册RxBus，防止内存泄露
    private fun unRegisterRxBus() {
        RxToolKit.UnRegister(this)
        EventBus.getDefault().unregister(this)
    }

    //移除消息观察器
    private fun unRegisterUIMessageHandler() {
        try {
            spyRoomContext.unRegisterProcess(observer)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //销毁所有视图
    private fun destroyView() {
        try {
            spyRoomUIController.destroy()
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    //尝试隐藏键盘,退出房间时强制调用
    private fun hideKeyBoard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        } catch (e: Exception) {
            PLog.e("hideKeyBoard:", "error:" + e.message)
        }
    }

    //点击回退键
    private fun onBack() {
//        if (canQuitRoomWhilePlayingFilter()) {
//            popQuitDialogNormal()
//        } else {
//            ToastToolKit.showShort(R.string.str_voice_playing_can_not_leave)
//        }
        popQuitDialogNormal()
    }

    override fun onActivityWindowInitFinish() {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        PLog.i(TAG, "onResume: ")
        MobclickAgent.onResume(this)
        RxToolKit.Emit(ActivityEvent.GameActivityEvent(ActivityEvent.onResume))

        //用于重现由于onPause丢失的动画
        spyRoomUIController.onResume()
    }

    override fun onPause() {
        super.onPause()
        PLog.i(TAG, "onPause: ")
        MobclickAgent.onPause(this)
        RxToolKit.Emit(ActivityEvent.GameActivityEvent(ActivityEvent.onPause))
    }

    override fun onStop() {
        super.onStop()
        PLog.i(TAG, "onStop: ")
        RxToolKit.Emit(ActivityEvent.GameActivityEvent(ActivityEvent.onStop))
    }

    override fun onDestroy() {
        super.onDestroy()
        PLog.i(TAG, "onDestroy: ")
        //这里强制发送断开说话操作
        RxToolKit.Emit(ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP))
        unRegisterRxBus()
        unRegisterUIMessageHandler()
        destroyView()
        hideKeyBoard()
        dismissExitLoadingDialog()
    }

    override fun onBackPressed() {
        //warnLeaveRoom(20)
        //onBack()
    }

    /**
     * 自己是否离开了房间
     */
    fun judgeShouldExit() {
        if (SpyRoomHelper.isLeave()) {
            sendLeaveSignal()
            ToastToolKit.showShort(R.string.you_have_been_kick_out)
        }
    }

    /**
     * 长时间未操作系统提示
     */
    fun warnLeaveRoom(duration: Int) {
        val dialog = WereWolfHintDialog(this, MessageUtils.getString(R.string.dialog_hint), MessageUtils.getString(R.string.active_warning_string), null, null, true, true)
        val button1 = dialog.twoButtonLeft
        button1.text = MessageUtils.getString(R.string.werewolf_game_leave)
        button1.setOnClickListener { v ->
            dialog.dismiss()
            sendLeaveSignal()
        }

        val button2 = dialog.twoButtonRight
        button2.text = MessageUtils.getString(R.string.werewolf_game_continue)
        button2.setOnClickListener { v ->
            dialog.dismiss()
            RoomSocketEngineHelper.sendEmptyType(Constant.ACTION_CONTINUE)
        }

        dialog.setCountDownFinish {
            dialog.dismiss()
            sendLeaveSignal()
        }

        dialog.showDialog(duration)
    }


    private var observer = object : SpyRoomProcessable() {

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {

            judgeShouldExit()
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {

            judgeShouldExit()
            return false
        }

        override fun handleWarnLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            val duration = RoomSocketEngineHelper.getOptDurTime(spyRoomMessenger.payload)
            warnLeaveRoom(duration)
            return false
        }
    }
}
