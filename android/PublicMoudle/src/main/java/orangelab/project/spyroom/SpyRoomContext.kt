package orangelab.project.spyroom

import android.content.Intent
import android.text.TextUtils
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.event.ViewEvent
import orangelab.project.common.model.ServerMessageEvent
import orangelab.project.common.model.ServerResponseEvent
import orangelab.project.spyroom.action.ProcessSwitcher
import orangelab.project.spyroom.data.SpyRoomDataSource
import orangelab.project.spyroom.event.SpyRoomEvent
import orangelab.project.spyroom.model.SpyRoomBridgeBean
import orangelab.project.spyroom.model.SpyRoomMessenger
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 谁是卧底消息处理器
 */
class SpyRoomContext(spyRoomBridgeBean: SpyRoomBridgeBean, intent: Intent) : SpyRoomSocketMessageHandler(), Destroyable, Keepable {

    private val TAG = "SpyRoomContext"

    var datasource: SpyRoomDataSource = SpyRoomDataSource(spyRoomBridgeBean.result, intent)    //数据

    private var preProcesses: ArrayList<SpyRoomProcessable> = ArrayList()                      //前置处理
    private var postProcesses: ArrayList<SpyRoomProcessable> = ArrayList()                     //后置处理
    private var processSwitcher: ArrayList<ProcessSwitcher> = ArrayList()                      //切换器

    //好友处理过就标记为已处理
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SpyRoomEvent.SpyRoomFriendHandled) {
        datasource.addFriendIds.forEach {
            if (TextUtils.equals(it.id, event.id)) {
                it.isHandle = true
            }
        }
    }

    //收到此请求就表示http已处理成功，就发起socket请求
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ViewEvent.RequestAddFriendEvent) {
        val player = datasource.findUserById(event.toUserId)
        if (player != null) {
            val position = player.position
            RoomSocketEngineHelper.sendRequestFriendInGame(event.toUserId, position)
        }
    }

    /**
     * 注册消息前置监听器
     */
    fun registerPreProcess(processable: SpyRoomProcessable) {
        if (!preProcesses.contains(processable)) {
            preProcesses.add(processable)
        }
    }

    /**
     * 注册消息后置监听器
     */
    fun registerPostProcess(processable: SpyRoomProcessable) {
        if (!postProcesses.contains(processable)) {
            postProcesses.add(processable)
        }
    }

    /**
     * 移除消息监听器
     */
    fun unRegisterProcess(processable: SpyRoomProcessable) {
        preProcesses.remove(processable)
        postProcesses.remove(processable)
    }

    /**
     * 开始监听并解析消息
     */
    fun startProcess() {

        //message消息监听
        RxToolKit.Build(this, ServerMessageEvent::class.java).action({
            postSafely(Runnable {
                try {
                    if (it.isSpyRoom) {
                        handleSocketEvent(SpyRoomMessenger().inflateData(event = it))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    PLog.e(TAG, "error when parse ServerMessageEvent ,error is ${e.message}")
                }
            })
        }).register()

        //response消息监听
        RxToolKit.Build(this, ServerResponseEvent::class.java).action({
            postSafely(Runnable {
                try {
                    if (it.isSpyRoom) {
                        //TODO 不处理response类型的消息，后面视情况打开
                        //handleSocketEvent(SpyRoomMessenger().inflateData(event = it))
                    }
                } catch (e: Exception) {
                    PLog.e(TAG, "error when parse ServerResponseEvent ,error is ${e.message}")
                }
            })
        }).register()

        EventBus.getDefault().register(this)
    }

    override fun destroy() {
        super.destroy()
        RxToolKit.UnRegister(this)
        EventBus.getDefault().unregister(this)
        preProcesses.clear()
        postProcesses.clear()
        processSwitcher.clear()
        datasource.destroy()
    }

    //消息分发 =============================================================================//
    override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleJoin(spyRoomMessenger) }
        datasource.dataSourceResolver.handleJoin(spyRoomMessenger)
        postProcesses.forEach { it.handleJoin(spyRoomMessenger) }
        return false
    }

    override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleLeave(spyRoomMessenger) }
        datasource.dataSourceResolver.handleLeave(spyRoomMessenger)
        postProcesses.forEach { it.handleLeave(spyRoomMessenger) }
        return false
    }

    override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleKickOut(spyRoomMessenger) }
        datasource.dataSourceResolver.handleKickOut(spyRoomMessenger)
        postProcesses.forEach { it.handleKickOut(spyRoomMessenger) }
        return false
    }

    override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleRestore(spyRoomMessenger) }
        datasource.dataSourceResolver.handleRestore(spyRoomMessenger)
        postProcesses.forEach { it.handleRestore(spyRoomMessenger) }
        return false
    }

    override fun handleDisconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleDisconnect(spyRoomMessenger) }
        datasource.dataSourceResolver.handleDisconnect(spyRoomMessenger)
        postProcesses.forEach { it.handleDisconnect(spyRoomMessenger) }
        return false
    }

    override fun handleReconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleReconnect(spyRoomMessenger) }
        datasource.dataSourceResolver.handleReconnect(spyRoomMessenger)
        postProcesses.forEach { it.handleReconnect(spyRoomMessenger) }
        return false
    }

    override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleChangeRoomOwner(spyRoomMessenger) }
        datasource.dataSourceResolver.handleChangeRoomOwner(spyRoomMessenger)
        postProcesses.forEach { it.handleChangeRoomOwner(spyRoomMessenger) }
        return false
    }

    override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {

        preProcesses.forEach { it.handleUpdateMaster(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUpdateMaster(spyRoomMessenger)
        postProcesses.forEach { it.handleUpdateMaster(spyRoomMessenger) }
        return false
    }

    override fun handleUpdateRoomConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleUpdateRoomConfig(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUpdateRoomConfig(spyRoomMessenger)
        postProcesses.forEach { it.handleUpdateRoomConfig(spyRoomMessenger) }
        return false
    }

    override fun handleChangeRoomPassword(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleChangeRoomPassword(spyRoomMessenger) }
        datasource.dataSourceResolver.handleChangeRoomPassword(spyRoomMessenger)
        postProcesses.forEach { it.handleChangeRoomPassword(spyRoomMessenger) }
        return false
    }

    override fun handleStartConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleStartConfig(spyRoomMessenger) }
        datasource.dataSourceResolver.handleStartConfig(spyRoomMessenger)
        postProcesses.forEach { it.handleStartConfig(spyRoomMessenger) }
        return false
    }

    override fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleUpdateConfig(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUpdateConfig(spyRoomMessenger)
        postProcesses.forEach { it.handleUpdateConfig(spyRoomMessenger) }
        return false
    }

    override fun handleStopConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleStopConfig(spyRoomMessenger) }
        datasource.dataSourceResolver.handleStopConfig(spyRoomMessenger)
        postProcesses.forEach { it.handleStopConfig(spyRoomMessenger) }
        return false
    }

    override fun handleGetConfigWords(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleGetConfigWords(spyRoomMessenger) }
        datasource.dataSourceResolver.handleGetConfigWords(spyRoomMessenger)
        postProcesses.forEach { it.handleGetConfigWords(spyRoomMessenger) }
        return false
    }

    override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleUpSeat(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUpSeat(spyRoomMessenger)
        postProcesses.forEach { it.handleUpSeat(spyRoomMessenger) }
        return false
    }

    override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleDownSeat(spyRoomMessenger) }
        datasource.dataSourceResolver.handleDownSeat(spyRoomMessenger)
        postProcesses.forEach { it.handleDownSeat(spyRoomMessenger) }
        return false
    }

    override fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handlePrepare(spyRoomMessenger) }
        datasource.dataSourceResolver.handlePrepare(spyRoomMessenger)
        postProcesses.forEach { it.handlePrepare(spyRoomMessenger) }
        return false
    }

    override fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleUnPrepare(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUnPrepare(spyRoomMessenger)
        postProcesses.forEach { it.handleUnPrepare(spyRoomMessenger) }
        return false
    }

    override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleGameStart(spyRoomMessenger) }
        datasource.dataSourceResolver.handleGameStart(spyRoomMessenger)
        postProcesses.forEach { it.handleGameStart(spyRoomMessenger) }
        return false
    }

    override fun handleAssignRole(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleAssignRole(spyRoomMessenger) }
        datasource.dataSourceResolver.handleAssignRole(spyRoomMessenger)
        postProcesses.forEach { it.handleAssignRole(spyRoomMessenger) }
        return false
    }

    override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleStartSpeech(spyRoomMessenger) }
        datasource.dataSourceResolver.handleStartSpeech(spyRoomMessenger)
        postProcesses.forEach { it.handleStartSpeech(spyRoomMessenger) }
        return false
    }

    override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleEndSpeech(spyRoomMessenger) }
        datasource.dataSourceResolver.handleEndSpeech(spyRoomMessenger)
        postProcesses.forEach { it.handleEndSpeech(spyRoomMessenger) }
        return false
    }

    override fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleSpeak(spyRoomMessenger) }
        datasource.dataSourceResolver.handleSpeak(spyRoomMessenger)
        postProcesses.forEach { it.handleSpeak(spyRoomMessenger) }
        return false
    }

    override fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleUnSpeak(spyRoomMessenger) }
        datasource.dataSourceResolver.handleUnSpeak(spyRoomMessenger)
        postProcesses.forEach { it.handleUnSpeak(spyRoomMessenger) }
        return false
    }

    override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleStartVote(spyRoomMessenger) }
        datasource.dataSourceResolver.handleStartVote(spyRoomMessenger)
        postProcesses.forEach { it.handleStartVote(spyRoomMessenger) }
        return false
    }

    override fun handleVoteInfoTemporary(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleVoteInfoTemporary(spyRoomMessenger) }
        datasource.dataSourceResolver.handleVoteInfoTemporary(spyRoomMessenger)
        postProcesses.forEach { it.handleVoteInfoTemporary(spyRoomMessenger) }
        return false
    }

    override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleVoteResult(spyRoomMessenger) }
        datasource.dataSourceResolver.handleVoteResult(spyRoomMessenger)
        postProcesses.forEach { it.handleVoteResult(spyRoomMessenger) }
        return false
    }

    override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleDeathInfo(spyRoomMessenger) }
        datasource.dataSourceResolver.handleDeathInfo(spyRoomMessenger)
        postProcesses.forEach { it.handleDeathInfo(spyRoomMessenger) }
        return false
    }

    override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleGameOver(spyRoomMessenger) }
        datasource.dataSourceResolver.handleGameOver(spyRoomMessenger)
        postProcesses.forEach { it.handleGameOver(spyRoomMessenger) }
        return false
    }

    override fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleChat(spyRoomMessenger) }
        datasource.dataSourceResolver.handleChat(spyRoomMessenger)
        postProcesses.forEach { it.handleChat(spyRoomMessenger) }
        return false
    }

    override fun handleAddFriend(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleAddFriend(spyRoomMessenger) }
        datasource.dataSourceResolver.handleAddFriend(spyRoomMessenger)
        postProcesses.forEach { it.handleAddFriend(spyRoomMessenger) }
        return false
    }

    override fun handleGuessWord(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleGuessWord(spyRoomMessenger) }
        datasource.dataSourceResolver.handleGuessWord(spyRoomMessenger)
        postProcesses.forEach { it.handleGuessWord(spyRoomMessenger) }
        return false
    }

    override fun handleGuessWordResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleGuessWordResult(spyRoomMessenger) }
        datasource.dataSourceResolver.handleGuessWordResult(spyRoomMessenger)
        postProcesses.forEach { it.handleGuessWordResult(spyRoomMessenger) }
        return false
    }

    override fun handleSystemMessage(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleSystemMessage(spyRoomMessenger) }
        datasource.dataSourceResolver.handleSystemMessage(spyRoomMessenger)
        postProcesses.forEach { it.handleSystemMessage(spyRoomMessenger) }
        return false
    }

    override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleObserverJoin(spyRoomMessenger) }
        datasource.dataSourceResolver.handleObserverJoin(spyRoomMessenger)
        postProcesses.forEach { it.handleObserverJoin(spyRoomMessenger) }
        return false
    }

    override fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleObserverLeave(spyRoomMessenger) }
        datasource.dataSourceResolver.handleObserverLeave(spyRoomMessenger)
        postProcesses.forEach { it.handleObserverLeave(spyRoomMessenger) }
        return false
    }

    override fun handleWarnLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        preProcesses.forEach { it.handleWarnLeave(spyRoomMessenger) }
        datasource.dataSourceResolver.handleWarnLeave(spyRoomMessenger)
        postProcesses.forEach { it.handleWarnLeave(spyRoomMessenger) }
        return false
    }
    //消息分发 =============================================================================//

}