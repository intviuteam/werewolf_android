package orangelab.project.spyroom.component

import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import kotlinx.android.synthetic.main.layout_spy_room_title_view.view.*
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.utils.CountDownView
import orangelab.project.common.utils.MessageUtils
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.SpyRoomContext
import orangelab.project.spyroom.SpyRoomOnlineFragment
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.dialog.SpyRoomSettingDialog
import orangelab.project.spyroom.dialog.SpyRoomVoteResultDialog
import orangelab.project.spyroom.model.SpyRoomMessenger
import orangelab.project.voice.model.VoiceSpyBeforeVote
import orangelab.project.voice.model.VoiceSpyGuessWord
import orangelab.project.voice.model.VoiceSpyVoteResult

/**
 * 头部布局
 */
class SpyRoomTitleView : Destroyable, Keepable {

    private val TAG: String = "SpyRoomTitleView"

    private lateinit var contentView: ViewGroup
    private var spyRoomContext: SpyRoomContext
    private var countDownView: CountDownView? = null

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.contentView = contentView
        this.spyRoomContext = spyRoomContext

        init()
    }

    private fun init() {
        contentView.tv_spy_room_type.text = MessageUtils.getString(R.string.str_spy_room_game_type, SpyRoomConfig.maxChair)      //房间类型
        contentView.tv_spy_room_id.text = MessageUtils.getString(R.string.str_spy_room_room, SpyRoomConfig.roomId)                              //房间号
        contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_spy_room_status_prepare)                                           //房间阶段栏
        //contentView.tv_spy_room_message.text = SpyRoomConfig.title                             //房间消息栏

        contentView.tv_spy_room_observer.visibility = View.VISIBLE                              //观战视图
        contentView.tv_spy_room_good_number.visibility = View.VISIBLE                           //好人数
        contentView.tv_spy_room_spy_number.visibility = View.VISIBLE                            //卧底数
        contentView.tv_spy_room_good_number.text = "0"
        contentView.tv_spy_room_spy_number.text = "0"

        contentView.iv_spy_room_setting.setOnClickListener {
            if (canSetting()) {
                SpyRoomSettingDialog(contentView.context).show()
            } else {
                ToastToolKit.showShort(R.string.str_spy_room_vote_no_op)
            }
        }

        contentView.tv_spy_room_observer.setOnClickListener {
            //直接强转
            val fragmentManager = (contentView.context as AppCompatActivity).supportFragmentManager
            val fragment = SpyRoomOnlineFragment()
            fragment.setContext(spyRoomContext)
            fragmentManager.beginTransaction().replace(R.id.rl_fragment_container, fragment, fragment.getTAG()).addToBackStack(fragment.getTAG()).commit()
        }

        endTimer()

        switchRole()

        refreshLockState()

        refreshRoleNumber()
    }

    fun changeStageBg() {
        if (SpyRoomHelper.isPlayer()) {
            contentView.ll_spy_room_message_bg.setBackgroundResource(R.mipmap.ic_spy_room_bg_message_start)
        }
    }

    /**
     * 视角切换
     */
    fun switchRole() {
        contentView.iv_spy_room_setting.visibility =
                if (canShowSetting()) View.VISIBLE
                else View.INVISIBLE
    }

    /**
     * 设置按钮是否可见
     */
    private fun canShowSetting(): Boolean {
        return SpyRoomHelper.isMaster() && SpyRoomHelper.isPlayer()
    }

    /**
     * 能否进行设置
     */
    private fun canSetting(): Boolean {
        return SpyRoomHelper.isMaster() && SpyRoomHelper.isPlayer() && !SpyRoomConfig.isGaming
    }

    /**
     * 返回键点击事件
     */
    fun onBack(onClickListener: View.OnClickListener) {
        contentView.rl_spy_room_back_container.setOnClickListener(onClickListener)
    }

    private fun startTimer(time: Int) {
        contentView.ll_spy_room_timer_container.visibility = View.VISIBLE
        countDownView = CountDownView(contentView.tv_spy_room_timer, time, CountDownView.CountDownFinish {
            contentView.ll_spy_room_timer_container.visibility = View.GONE
        })
        countDownView?.start()
    }

    private fun endTimer() {
        if (countDownView != null) {
            countDownView?.cancel()
        }
        contentView.ll_spy_room_timer_container.visibility = View.GONE
    }

    fun refreshNetWifiState(connected: Boolean) {
        if (connected) {
            contentView.iv_spy_room_wifi.setImageResource(R.mipmap.ic_spy_room_tag_wifi_con)
        } else {
            contentView.iv_spy_room_wifi.setImageResource(R.mipmap.ic_spy_room_tag_wifi_dis)
        }
    }

    fun refreshLockState() {
        if (!TextUtils.isEmpty(SpyRoomConfig.password)) {
            contentView.iv_spy_room_lock.visibility = View.VISIBLE
        } else {
            contentView.iv_spy_room_lock.visibility = View.INVISIBLE
        }
    }

    fun refreshRoleNumber() {
        if (SpyRoomConfig.isGaming) {
            contentView.tv_spy_room_good_number.text = SpyRoomConfig.goodNumber.toString()
            contentView.tv_spy_room_spy_number.text = SpyRoomConfig.spyNumber.toString()
        } else {
            contentView.tv_spy_room_good_number.text = "0"
            contentView.tv_spy_room_spy_number.text = "0"
        }

    }

    override fun destroy() {

    }

    /**
     * view观察处理器，玩家视角
     */
    var observer = object : SpyRoomProcessable() {

        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            refreshLockState()
            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            refreshLockState()
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshLockState()
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_spy_room_game_start)
            postDelaySafely(Runnable {
                contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_voice_moment_describe)
            }, SpyRoomConfig.startDuration.toLong())

            changeStageBg()
            refreshRoleNumber()
            return false
        }

        override fun handleAssignRole(spyRoomMessenger: SpyRoomMessenger): Boolean {
            contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_voice_moment_describe)
            val personalData = PersonalDataHelper.getPersonalData()
            if (personalData != null) {
                contentView.tv_spy_room_message.text = personalData.selfRole
            }
            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {
            contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_voice_moment_vote)
            startTimer(SpyRoomConfig.voteDuration / 1000)
            PLog.e(TAG, "handleStartVote title: ${contentView.tv_spy_room_stage.text},time = ${SpyRoomConfig.voteDuration}")
            return false
        }

        override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            val voteResult = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyVoteResult::class.java)
            if (TextUtils.equals(voteResult.type, "death")) {
                if (voteResult.finished) {
                    contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_voice_moment_describe)
                    endTimer()
                }
            }
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            contentView.ll_spy_room_message_bg.setBackgroundResource(R.mipmap.ic_spy_room_message_bg)
            contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_spy_room_status_prepare)                              //房间阶段栏
            contentView.tv_spy_room_message.text = ""
            refreshRoleNumber()
            endTimer()
            return false
        }

        override fun handleGuessWord(spyRoomMessenger: SpyRoomMessenger): Boolean {
            contentView.tv_spy_room_stage.text = MessageUtils.getString(R.string.str_voice_moment_bomb_word)
            val guessWord = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGuessWord::class.java)
            startTimer(guessWord.duration / 1000)
            if (SpyRoomHelper.isSelf(guessWord.position)) {
                val dialog = SpyRoomVoteResultDialog(contentView.context)
                dialog.showGuessWord()
                dialog.startCount(guessWord.duration / 1000) {
                    if (dialog.isShowing) {
                        dialog.dismiss()
                    }
                }
            }

            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchRole()
            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshRoleNumber()
            return false
        }

        override fun handleChangeRoomPassword(spyRoomMessenger: SpyRoomMessenger): Boolean {

            refreshLockState()
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            if (SpyRoomConfig.isGaming) {
                changeStageBg()
                val personalData = PersonalDataHelper.getPersonalData()
                if (personalData != null) {
                    contentView.tv_spy_room_message.text = personalData.selfRole
                }
            } else {
                contentView.ll_spy_room_message_bg.setBackgroundResource(R.mipmap.ic_spy_room_message_bg)
                contentView.tv_spy_room_message.text = ""
            }
            switchRole()
            refreshRoleNumber()
            refreshLockState()
            return false
        }
    }
}