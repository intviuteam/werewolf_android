package orangelab.project.spyroom

import com.androidtoolkit.RxToolKit
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.common.utils.SafeHandler
import orangelab.project.spyroom.model.SpyRoomMessenger

/**
 * 谁是卧底消息处理器
 */
abstract class SpyRoomProcessable : Destroyable, Keepable {

    private var safeHandler = SafeHandler()

    protected open fun postSafely(runnable: Runnable) {
        postDelaySafely(runnable, 0)
    }

    protected open fun postDelaySafely(runnable: Runnable, delay: Long) {
        safeHandler.postDelaySafely(runnable, delay)
    }

    override fun destroy() {
        RxToolKit.UnRegister(this)
        safeHandler.release()
    }

    /**
     * 刷新数据
     */
    open fun refresh(key: Long) {

    }

    /**
     * 进入房间/玩家
     */
    open fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 离开房间/玩家
     */
    open fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 进入房间/观战者
     */
    open fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 离开房间/观战者
     */
    open fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 踢出房间
     */
    open fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 房间复原
     */
    open fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 断开连接
     */
    open fun handleDisconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 重新连接
     */
    open fun handleReconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 转让房主
     */
    open fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 更换房主
     */
    open fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 更改房间设置
     */
    open fun handleUpdateRoomConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 更改密码
     */
    open fun handleChangeRoomPassword(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 打字聊天
     */
    open fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 系统消息
     */
    open fun handleSystemMessage(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 开始设置
     */
    open fun handleStartConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 更新设置
     */
    open fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 完成设置
     */
    open fun handleStopConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 获取词语
     */
    open fun handleGetConfigWords(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 上座
     */
    open fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 下座观战
     */
    open fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 准备
     */
    open fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 取消准备
     */
    open fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 游戏开始
     */
    open fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 分配角色
     */
    open fun handleAssignRole(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 可以发言
     */
    open fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 不能发言
     */
    open fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 按住说话
     */
    open fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 停止说话
     */
    open fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 开始投票
     */
    open fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 投票信息
     */
    open fun handleVoteInfoTemporary(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 投票结果
     */
    open fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 死亡信息
     */
    open fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 游戏结束
     */
    open fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 房间内加好友
     */
    open fun handleAddFriend(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 开始猜词
     */
    open fun handleGuessWord(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 猜词结果
     */
    open fun handleGuessWordResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

    /**
     * 提示离开
     */
    open fun handleWarnLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
        return false
    }

}