package orangelab.project.spyroom.data

import android.text.TextUtils
import com.androidtoolkit.PLog
import com.toolkit.action.Keepable
import orangelab.project.common.PersonalDataHelper
import orangelab.project.spyroom.SpyRoomConfig

object SpyRoomHelper : Keepable {

    private val TAG: String = "SpyRoomHelper"

    /**
     * 位置映射,会自动根据当前房间的总座位数映射到对应的位置
     */
    fun computePosition(position: Int): Int {
        var newPosition = position
//        if (SpyRoomConfig.maxChair < SpyRoomConfig.maxRealChair) {
//            //该座位在右侧
//            if (position > (SpyRoomConfig.maxRealChair ushr 1)) {
//                //座位号右移
//                newPosition += ((SpyRoomConfig.maxRealChair - SpyRoomConfig.maxChair) ushr 1)
//            }
//        }

        return newPosition
    }

    /**
     * 是否是房主
     */
    fun isMaster(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return personalData.isMaster
    }

    /**
     * 是否是合法座位
     */
    fun isLegalPosition(position: Int): Boolean {
        return position >= SpyRoomConfig.minChairPosition && position < SpyRoomConfig.maxChair
    }

    /**
     * 是否是玩家
     */
    fun isPlayer(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData()
        if (personalData != null) {
            val selfPosition = personalData.selfPosition
            return selfPosition < SpyRoomConfig.maxChair && selfPosition >= SpyRoomConfig.minChairPosition
        }
        return false
    }

    /**
     * 是否是玩家
     */
    fun isPlayer(position: Int): Boolean {
        return position < SpyRoomConfig.maxChair && position >= SpyRoomConfig.minChairPosition
    }

    /**
     * 是否是自己
     */
    fun isSelf(id: String): Boolean {
        return try {
            val selfId: String = PersonalDataHelper.personalData.userId
            PLog.e(TAG, "selfId: $selfId, id $id")
            TextUtils.equals(selfId, id)
        } catch (e: Exception) {
            false
        }
    }

    /**
     * 是否是自己
     */
    fun isSelf(position: Int): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        val selfPosition = personalData.selfPosition
        return selfPosition == position
    }

    /**
     * 自己是否出局
     */
    fun isOut(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return personalData.isOut
    }

    /**
     * 自己是否准备
     */
    fun isPrepared(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return personalData.isPrepared
    }

    /**
     * 自己是否能说话
     */
    fun canSpeech(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return personalData.isSpeech
    }

    /**
     * 自己是否离开了房间
     */
    fun isLeave(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return true
        return personalData.selfPosition == SpyRoomConstants.OVER_POSITION
    }

    /**
     * 自己是否在发言
     */
    fun isSpeaking(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return personalData.isSpeaking
    }

    /**
     * 自己是否是卧底
     */
    fun isSpy(): Boolean {
        val personalData = PersonalDataHelper.getPersonalData() ?: return false
        return TextUtils.equals(personalData.role, SpyRoomConstants.SPY_TAG)
    }
}

