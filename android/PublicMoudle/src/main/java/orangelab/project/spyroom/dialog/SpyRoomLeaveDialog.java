package orangelab.project.spyroom.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 13/10/2017.
 */

public class SpyRoomLeaveDialog extends Dialog {

    private TextView tvTitle;
    private TextView tvMsg;
    private View btnSure;
    private View btnCancel;

    private Context mContext;
    private String title;
    private String msg;
    private View.OnClickListener confirmClickListener;
    private View.OnClickListener cancelClickListener;

    public SpyRoomLeaveDialog(@NonNull Context context, String title, String msg, View.OnClickListener confirm) {
        this(context, title, msg, confirm, null);
    }

    public SpyRoomLeaveDialog(@NonNull Context context, String title, String msg,
                              View.OnClickListener confirm,
                              View.OnClickListener cancel) {
        super(context, R.style.DarkDialog);
        this.mContext = context;
        this.title = title;
        this.msg = msg;
        this.confirmClickListener = confirm;
        this.cancelClickListener = cancel;

        setCancelable(false);
        setCanceledOnTouchOutside(false);

        init();
    }

    public SpyRoomLeaveDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_spy_room_leave, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        tvMsg = (TextView) contentView.findViewById(R.id.tv_msg);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        tvTitle.setText(title);
        tvMsg.setText(msg);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(340);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(confirmClickListener == null ? v -> dismiss() : confirmClickListener);
        btnCancel.setOnClickListener(cancelClickListener == null ? v -> dismiss() : cancelClickListener);
    }

    public void negative(boolean need) {
        if (need) {
            btnCancel.setVisibility(View.VISIBLE);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
    }
}
