package orangelab.project.spyroom.component

import android.graphics.PointF
import android.text.TextUtils
import android.util.SparseArray
import android.view.ViewGroup
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.view.ScreenUtils
import com.datasource.GlobalUserState
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.effect.gifts.GiftAnimConstant
import orangelab.project.common.model.RestoreResult
import orangelab.project.common.utils.LeanCloudChatHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.game.model.WereWolfGiftRebate
import orangelab.project.spyroom.SpyRoomConfig
import orangelab.project.spyroom.SpyRoomContext
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.data.SpyRoomHelper
import orangelab.project.spyroom.dialog.SpyRoomAddFriendDialog
import orangelab.project.spyroom.dialog.SpyRoomGameOverDialog
import orangelab.project.spyroom.dialog.SpyRoomVoteResultDialog
import orangelab.project.spyroom.element.SpyRoomChair
import orangelab.project.spyroom.model.SpyRoomMessenger
import orangelab.project.voice.model.*
import org.json.JSONObject
import java.util.*

/**
 * 舞台主布局
 */
class SpyRoomStageView : Destroyable, Keepable {

    private var spyRoomContext: SpyRoomContext
    private var contentView: ViewGroup
    private var chairs = SparseArray<SpyRoomChair>()

    companion object {
        private const val TAG = "SpyRoomStageView"
    }

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.spyRoomContext = spyRoomContext
        this.contentView = contentView
        initChairs(contentView)
        initPlayer()
    }

    /**
     * 初始化房间座位
     */
    private fun initChairs(stageView: ViewGroup) {
        for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxRealChair) {
            try {
                val chairId = R.id::class.java.getField("spy_player_$position").getInt(null)
                PLog.e(TAG, "chairId = $chairId, position = $position")
                val chairContentView = stageView.findViewById<ViewGroup>(chairId)
                chairs.put(position, SpyRoomChair(position, chairContentView as ViewGroup))
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 初始化房间成员
     */
    private fun initPlayer() {

        //处理加锁座位
        for (position in spyRoomContext.datasource.lockedSeatList) {
            if (SpyRoomHelper.isLegalPosition(position)) {
                if (chairs[computePosition(position)] != null) {
                    chairs[computePosition(position)].lock()
                }
            }
        }

        //处理房间成员
        for (user in spyRoomContext.datasource.players) {
            if (chairs[computePosition(user.position)] != null) {
                chairs[computePosition(user.position)].upSeat(user)
            }
        }
    }

    /**
     * 上人
     */
    private fun switchSeatToBeSeated(position: Int) {
        try {
            if (SpyRoomHelper.isPlayer(position)) {
                val player = spyRoomContext.datasource.findUserByPosition(position)
                if (player != null) {
                    chairs[computePosition(position)]?.upSeat(player)
                }
            }
        } catch (e: Exception) {
            PLog.e(TAG, "switchSeatToBeSeated : ${e.message}")
        }
    }

    /**
     * 走人
     */
    private fun switchSeatToEmpty(position: Int) {
        try {
            if (SpyRoomHelper.isPlayer(position)) {
                chairs[computePosition(position)]?.downSeat()
            }
        } catch (e: Exception) {
            PLog.e(TAG, "switchSeatToEmpty : ${e.message}")
        }
    }

    /**
     * 送礼
     */
    private fun sendGift(gift: JSONObject) {
        val type = gift.optString(SpyRoomConstants.GIFT_TYPE)
        /**
         * 如果送的是卡片，直接结束
         */
        if (EffectsManager.IsCardByGiftType(type)) {
            return
        }

        val from = gift.optInt(SpyRoomConstants.GIFT_FROM)
        val to = gift.optInt(SpyRoomConstants.GIFT_TO)

        var start: PointF? = null
        var end: PointF? = null

        //麦上给麦上送
        if (from <= SpyRoomConfig.maxChair && to <= SpyRoomConfig.maxChair) {
            start = getGiftPosition(from)
            end = getGiftPosition(to)
        }

        //麦下给麦上送
        //麦下不能给麦上送,按游戏房逻辑走
//        if (SpyRoomConfig.maxChair in to..(from - 1)) {
//            start = getGiftPosition(from)
//            end = getGiftPosition(to)
//        }

        if (start == null || end == null) {
            return
        }

        //这里是处理送礼物特效,处理默认数据
        val fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT
        val toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT

        val chair = chairs.get(to)//默认取终点
        var chairWidth = 0
        var chairHeight = 0
        if (chair != null) {
            chairWidth = chair.getHeadWidth()
            chairHeight = chair.getHeadHeight()

            //送礼的坐标处理，返现的不处理
            val start1 = PointF(start.x, start.y)
            val end1 = PointF(end.x, end.y)

            EffectsManager.DispatchAnimEvent(contentView.context, GiftAnimConstant.GIFT_FROM_TYPE_VOICE, type, start1, end1, fromO, toO, chairWidth, chairHeight, from, to)
        }

        //额外处理
        try {
            if (EffectsManager.SupportThisGiftType(type)) {
                if (gift.has("rebate")) {
                    //这里需要处理返现
                    val rebate = GsonHelper.getGson().fromJson(gift.optJSONObject("rebate").toString(), WereWolfGiftRebate::class.java)
                    if (rebate != null) {
                        val dim = rebate.value
                        val dimString = Integer.toString(dim)
                        if (TextUtils.isEmpty(dimString)) {
                            return
                        }
                        if (SpyRoomHelper.isPlayer(to)) {//终点在麦上才展示
                            PLog.e("gift-rebate", "to:$to")
                            EffectsManager.DispatchRebateAnimEvent(contentView.context, dimString, PointF(start.x, start.y), PointF(end.x, end.y), fromO, toO, chairWidth, chairHeight)
                        }
                        val player = spyRoomContext.datasource.findUserByPosition(to)
                        if (player != null) {
                            if (!TextUtils.equals(player.userId, GlobalUserState.getGlobalState().userId) && !Utils.checkUserIdIsTourist(player.userId)) {
                                //不能为游客，不能给自己
                                PLog.e("gift-rebate", "is not self and tourist")
                                LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(type, player.userId, player.userName, player.headImageUrl, player.sex, dimString)
                            }
                        }
                    }
                } else {
                    //ignore
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * 获取送礼位置
     */
    private fun getGiftPosition(position: Int): PointF? {
        //在麦上的就取chair的位置
        if (position <= SpyRoomConfig.maxChair) {
            if (chairs[computePosition(position)] != null) {
                return chairs[computePosition(position)].getPositionOnScreen()
            }
        } else {
            //不在麦上的取屏幕右下角位置
            val location = intArrayOf(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight())
            return PointF(location[0].toFloat(), location[1].toFloat())
        }

        return null
    }

    /**
     * 展示游戏结果
     */
    private fun showGameOver(gameOverBean: VoiceSpyGameOverBean) {

        var spyWord = ""
        var goodWord = ""
        var isSpy = false//房主和麦下人都视为好人
        val undercover = gameOverBean.roles.undercover
        val people = gameOverBean.roles.people

        if (people != null && people.size > 0) {
            goodWord = people[0].detail
            people.forEach {
                if (SpyRoomHelper.isSelf(it.position)) {
                    isSpy = false
                    return@forEach
                }
            }
        }

        if (undercover != null && undercover.size > 0) {
            spyWord = undercover[0].detail
            if (TextUtils.isEmpty(spyWord)) {
                //说明卧底为幽灵
                if (gameOverBean.words != null) {
                    spyWord = if (TextUtils.equals(goodWord, gameOverBean.words.first)) {
                        gameOverBean.words.second
                    } else {
                        gameOverBean.words.first
                    }
                }
            }
            undercover.forEach {
                if (SpyRoomHelper.isSelf(it.position)) {
                    isSpy = true
                    return@forEach
                }
            }
        }

        var isVictory = false
        val personalData = PersonalDataHelper.getPersonalData()
        //游戏结束的一瞬间被挤出就不再处理
        if (gameOverBean.winners != null) {
            isVictory = gameOverBean.winners.contains(personalData.selfPosition)
        }

        val selfWord = personalData.selfRole
        if (TextUtils.equals(spyWord, selfWord)) {
            //自己是卧底
            isSpy = true
        } else if (TextUtils.equals(goodWord, selfWord)) {
            //自己是好人
            isSpy = false
        }

        PLog.e(TAG, "isSpy = $isSpy, isVictory = $isVictory")

        try {
            val spyGameOverBeans = ArrayList<VoiceSimpleUserBean>()
            for (bean in gameOverBean.roles.undercover) {
                val item = spyRoomContext.datasource.findUserByPosition(bean.position)
                if (item != null) {
                    if (SpyRoomHelper.isSelf(item.userId)) {
                        spyGameOverBeans.add(0, VoiceSimpleUserBean(item.userId, item.userName, item.headImageUrl, item.position))
                    } else {
                        spyGameOverBeans.add(VoiceSimpleUserBean(item.userId, item.userName, item.headImageUrl, item.position))
                    }
                }
            }
            val goodGameOverBeans = ArrayList<VoiceSimpleUserBean>()
            for (bean in gameOverBean.roles.people) {
                val item = spyRoomContext.datasource.findUserByPosition(bean.position)
                if (item != null) {
                    if (SpyRoomHelper.isSelf(item.userId)) {
                        goodGameOverBeans.add(0, VoiceSimpleUserBean(item.userId, item.userName, item.headImageUrl, item.position))
                    } else {
                        goodGameOverBeans.add(VoiceSimpleUserBean(item.userId, item.userName, item.headImageUrl, item.position))
                    }
                }

            }

            val bean = VoiceSAFBean()
            bean.isSpy = isSpy
            bean.victory = isVictory
            bean.spyWord = spyWord
            bean.goodWord = goodWord
            if (isSpy) {
                bean.first = spyGameOverBeans
                bean.second = goodGameOverBeans
            } else {
                bean.first = goodGameOverBeans
                bean.second = spyGameOverBeans
            }

            //只有玩家才可以看到胜利/失败界面
            if (SpyRoomHelper.isPlayer()) {
                val dialog = SpyRoomGameOverDialog(contentView.context)
                dialog.setOnConfirmClickedListener { dialog.dismiss() }
                dialog.setData(bean)

                dialog.show()
            }
        } catch (e: Exception) {
            PLog.e(TAG, "no user message!")
        }
    }

    /**
     * 重开游戏
     */
    private fun reOpenGame() {
        for (position in SpyRoomConfig.minChairPosition..SpyRoomConfig.maxChair) {
            chairs[computePosition(position)]?.reOpenGame()
        }
    }

    /**
     * 加好友
     */
    private fun addFriend() {
        val addFriendBean = spyRoomContext.datasource.addFriendIds.last()
        if (!addFriendBean.isHandle) {
            val dialog = SpyRoomAddFriendDialog(contentView.context, addFriendBean.id)
            dialog.show()
        }
    }

    private fun downSeat(id: String) {

        val player = spyRoomContext.datasource.findUserById(id)
        if (player != null) {
            switchSeatToEmpty(player.position)
        }
    }

    /**
     * 处理投票结果
     */
    private fun showVote(spyRoomMessenger: SpyRoomMessenger) {

        fun endVote() {
            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)].endVote()
            }
        }

        val voteResult = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyVoteResult::class.java)
        if (TextUtils.equals(voteResult.type, "death")) {
            //已投票/投票结束就隐藏投票按钮
            endVote()
            //玩家本次投票的结果
            if (!voteResult.finished) {
                //隐藏自己的"投票中"标签
                chairs[computePosition(PersonalDataHelper.personalData.selfPosition)].hideVoteTag()
                //被投票的人的集合
                val beVoted = voteResult.vote_info.keys
                for (v in beVoted) {
                    if (SpyRoomHelper.isPlayer(v)) {
                        val roomChair = chairs.get(v)
                        if (roomChair != null) {
                            //清除上一次的投票信息
                            roomChair.clearVoteInfo()
                            //投给此人的人的集合
                            val votes = voteResult.vote_info[v]
                            //展示本次投票信息
                            votes?.forEach {
                                roomChair.beVote(spyRoomContext.datasource.findUserByPosition(it)!!)
                            }
                        }
                        //未弃票的人
                        if (v != -1) {
                            //隐藏所有已经投过票的人的"投票中"标签
                            voteResult.vote_info[v]?.forEach {
                                chairs[computePosition(it)].hideVoteTag()
                            }
                        }
                    }
                }
            } else {
                //投票完毕

                //清空座位的投票消息
                for (i in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                    val roomChair = chairs.get(i)
                    roomChair?.clearVoteInfo()
                    roomChair?.hideVoteTag()
                }

                //展示pk对话框
                val duration = voteResult.duration / 1000
                if (voteResult.positions != null && voteResult.positions.size > 0) {
                    if (voteResult.need_pk) {
                        if (SpyRoomHelper.isPlayer()) {
                            val spySystemMsgDialog = SpyRoomVoteResultDialog(contentView.context)
                            spySystemMsgDialog.showPK(voteResult.positions)
                            spySystemMsgDialog.startCount(duration) { spySystemMsgDialog.dismiss() }
                        }
                    }
                } else {
                    //展示弃票对话框
                    if (SpyRoomHelper.isPlayer()) {
                        val spySystemMsgDialog = SpyRoomVoteResultDialog(contentView.context)
                        spySystemMsgDialog.showGiveUp()
                        spySystemMsgDialog.startCount(duration) { spySystemMsgDialog.dismiss() }
                    }
                }
            }
        }
    }

    /**
     * 处理死亡消息
     */
    private fun showDeathInfo(deathInfo: VoiceSpyGameDeathInfo) {
        if (deathInfo.death_info != null && deathInfo.death_info.size > 0) {
            val info = deathInfo.death_info[0]
            val outPosition = info.killed
            if (SpyRoomHelper.isPlayer(outPosition)) {
                val chair = chairs.get(outPosition)
                chair?.out()
            }

            if (SpyRoomHelper.isPlayer()) {
                //处理系统消息
                val role = if (TextUtils.equals(SpyRoomConstants.SPY_TAG, info.role))
                    MessageUtils.getString(R.string.str_voice_role_spy)
                else
                    MessageUtils.getString(R.string.str_voice_role_people)
                val spySystemMsgDialog = SpyRoomVoteResultDialog(contentView.context)
                spySystemMsgDialog.showOut(outPosition, role)
                spySystemMsgDialog.startCount(deathInfo.duration / 1000) { spySystemMsgDialog.dismiss() }
            }
        }
    }

    /**
     * 位置计算
     */
    private fun computePosition(position: Int): Int {
        return SpyRoomHelper.computePosition(position)
    }

    /**
     * 销毁处理
     */
    override fun destroy() {
        for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
            chairs[computePosition(position)]?.destroy()
        }
    }

    /**
     * 前置处理器
     */
    var preObserver = object : SpyRoomProcessable() {

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val userId = spyRoomMessenger.payload.optString(SpyRoomConstants.USER_ID_LOWER)
            downSeat(userId)
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val userId = spyRoomMessenger.payload.optString(SpyRoomConstants.USER_ID_LOWER)
            downSeat(userId)
            return false
        }
    }

    /**
     * view观察处理器
     */
    var observer = object : SpyRoomProcessable() {

        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //不处理自己，自己在房间收到自己加入，乃非法消息
//            if (!SpyRoomHelper.isSelf(spyRoomMessenger.newPlayer.userId)) {
//                switchSeatToBeSeated(spyRoomMessenger.newPlayer.position)
//            }
            switchSeatToBeSeated(spyRoomMessenger.newPlayer.position)
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            switchSeatToEmpty(spyRoomMessenger.newPlayer.position)
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {

            switchSeatToEmpty(spyRoomMessenger.newPlayer.position)
            return false
        }

        override fun handleAddFriend(spyRoomMessenger: SpyRoomMessenger): Boolean {

            addFriend()
            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)]?.changeToNormal()
            }
            chairs[SpyRoomConfig.masterPosition]?.changeToMaster()
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.DEST_POSITION)
            switchSeatToBeSeated(position)

            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.DEST_POSITION)
            switchSeatToEmpty(position)

            return false
        }

        override fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val gift = spyRoomMessenger.payload.optJSONObject(SpyRoomConstants.GIFT_CHAT)
            if (gift != null) {
                sendGift(gift)
            }
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {

            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)]?.unPrepare()
            }
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val gameOverBean = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGameOverBean::class.java)
            showGameOver(gameOverBean)

            reOpenGame()
            return false
        }

        override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {

            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)]?.endSpeech()
            }

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            chairs[computePosition(position)]?.startSpeech()
            return false
        }

        override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val position = spyRoomMessenger.payload.optInt(SpyRoomConstants.POSITION)
            chairs[computePosition(position)]?.endSpeech()
            return false
        }

        override fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {

            PLog.e(TAG, "speaking position = ${spyRoomMessenger.newPlayer.position}")
            chairs[spyRoomMessenger.newPlayer.position]?.speak()
            return false
        }

        override fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {

            chairs[spyRoomMessenger.newPlayer.position]?.endSpeak()
            return false
        }

        override fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {

            chairs[spyRoomMessenger.newPlayer.position]?.prepare()
            return false
        }

        override fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {

            chairs[spyRoomMessenger.newPlayer.position]?.unPrepare()
            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {

            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)]?.endSpeech()
                chairs[computePosition(position)]?.showVoteTag()
            }

            val beforeVote = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyBeforeVote::class.java)
            if (beforeVote.alives != null && beforeVote.alives.size > 0) {
                beforeVote.alives.forEach {
                    chairs[computePosition(it)]?.startVote()
                }
            }

            return false
        }

        override fun handleVoteInfoTemporary(spyRoomMessenger: SpyRoomMessenger): Boolean {

            return false
        }

        override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            showVote(spyRoomMessenger)
            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {

            val deathInfo = GsonHelper.parseJsonToBean(spyRoomMessenger.payload.toString(), VoiceSpyGameDeathInfo::class.java)
            showDeathInfo(deathInfo)
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {

            //座位重置
            for (position in SpyRoomConfig.minChairPosition until SpyRoomConfig.maxChair) {
                chairs[computePosition(position)]?.clear()
            }

            //座位复原
            initPlayer()

            //发言信息复原
            val result = GsonHelper.getGson().fromJson<RestoreResult>(spyRoomMessenger.payload.toString(), RestoreResult::class.java)
            val game_info = result.game_info
            if (game_info.speech_info != null && game_info.speech_info.current != null) {
                val position = game_info.speech_info.current.position//此人正在说话
                if (SpyRoomHelper.isPlayer(position)) {
                    chairs[computePosition(position)]?.startSpeech()
                }
            }

            //出局信息
            if (game_info.death_info != null && game_info.death_info.size > 0) {
                for (deathInfo in game_info.death_info) {
                    if (SpyRoomHelper.isPlayer(deathInfo.position)) {
                        chairs[deathInfo.position]?.out()
                    }
                }
            }

            return false
        }
    }

}