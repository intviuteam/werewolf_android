package orangelab.project.spyroom.component

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.R
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.spyroom.SpyRoomContext
import orangelab.project.spyroom.SpyRoomProcessable
import orangelab.project.spyroom.adapter.SpyRoomMessageListAdapter
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.model.SpyRoomMessenger

/**
 * 消息展示器
 */
class SpyRoomMessageView : Destroyable, Keepable {

    private val TAG = "SpyRoomMessageView"

    private var messageList: RecyclerView
    private var spyRoomContext: SpyRoomContext
    private var msgAdapter: SpyRoomMessageListAdapter

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.messageList = contentView.findViewById(R.id.rlist_spy_room_message) as RecyclerView
        val linearLayoutManager = WrapContentLinearLayoutManager(contentView.context)
        linearLayoutManager.stackFromEnd = false
        this.messageList.layoutManager = linearLayoutManager
        this.spyRoomContext = spyRoomContext
        msgAdapter = SpyRoomMessageListAdapter(contentView.context, messageList)
        messageList.adapter = msgAdapter

        //showWelcomeMessage()
    }

    /**
     * 展示欢迎信息
     */
    private fun showWelcomeMessage() {
        refreshMsg(SpyRoomConstants.CODE_WELCOME)
        refreshMsg(SpyRoomConstants.CODE_INTRODUCE)
    }

    /**
     * 刷新消息列表，自动滚动到底部
     */
    private fun refreshMsg(key: Long) {
        msgAdapter.refreshMessageList(spyRoomContext.datasource.messageMap.get(key), true)
    }

    /**
     * 刷新消息列表，不滚动
     */
    private fun refreshMsgNotScroll(key: Long) {
        msgAdapter.refreshMessageList(spyRoomContext.datasource.messageMap.get(key), false)
    }

    /**
     * 复原消息列表
     */
    private fun restoreMsg() {
        msgAdapter.restoreMessage(spyRoomContext.datasource.messageMap.values())
    }

    override fun destroy() {

    }

    /**
     * view观察处理器
     */
    var observer = object : SpyRoomProcessable() {
        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            //refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            //refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {

            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleGuessWordResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsg(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleSystemMessage(spyRoomMessenger: SpyRoomMessenger): Boolean {
            refreshMsgNotScroll(spyRoomMessenger.msg_id)
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            restoreMsg()
            return false
        }

        override fun refresh(key: Long) {
            refreshMsg(key)
        }

    }
}