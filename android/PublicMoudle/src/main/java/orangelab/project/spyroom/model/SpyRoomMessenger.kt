package orangelab.project.spyroom.model

import com.androidtoolkit.transport.Transportable
import com.toolkit.action.Keepable
import orangelab.project.common.model.ServerMessageEvent
import orangelab.project.common.model.ServerResponseEvent
import org.json.JSONObject

/**
 * 谁是卧底房信使
 */
class SpyRoomMessenger : Keepable, Transportable {

    var code: Int = 0                                                   //状态码
    var msg_id: Long = 0                                                //消息编号
    lateinit var message: String                                        //提示信息
    lateinit var type: String                                           //消息类型
    lateinit var payload: JSONObject                                    //消息json数据实体
    var newPlayer: SpyRoomPlayer = SpyRoomPlayer()       //此条消息操作的用户,更改之后

    fun inflateData(event: ServerMessageEvent): SpyRoomMessenger {
        this.code = event.code
        this.msg_id = event.msg_id
        this.message = event.message
        this.type = event.type
        this.payload = event.payload

        return this
    }

    fun inflateData(event: ServerResponseEvent): SpyRoomMessenger {
        this.code = event.code
        this.msg_id = event.msg_id
        this.message = event.message
        this.type = event.type
        this.payload = event.payload

        return this
    }

    fun bindPostPlayer(player: SpyRoomPlayer): SpyRoomMessenger {
        this.newPlayer = player

        return this
    }

    override fun toString(): String {
        return "SpyRoomMessenger(code=$code, msg_id=$msg_id, message='$message', type='$type', payload=$payload)"
    }

}