package orangelab.project.spyroom

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.R
import com.androidtoolkit.PicassoUtils
import com.toolkit.action.Destroyable
import kotlinx.android.synthetic.main.activity_spy_room.view.*
import kotlinx.android.synthetic.main.layout_spy_room_len_stage.view.*
import orangelab.project.MainApplication
import orangelab.project.common.utils.MessageUtils
import orangelab.project.spyroom.action.Refreshable
import orangelab.project.spyroom.component.*
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.spyroom.model.SpyRoomMessenger

/**
 * UI控制器
 */
class SpyRoomUIController : Refreshable, Destroyable, SpyRoomProcessable {

    private var contentView: ViewGroup                                   //整体UI
    private var spyRoomTitleView: SpyRoomTitleView                       //顶部标题栏
    private var spyRoomStageView: SpyRoomStageView                       //舞台场景(8个座位)
    private var spyRoomMessageView: SpyRoomMessageView                   //消息栏
    private var spyRoomOperationView: SpyRoomOperationView               //下方操作栏
    private var spyRoomSpeakView: SpyRoomSpeakView                       //底部说话/打字操作烂

    private var observers: ArrayList<SpyRoomProcessable> = ArrayList()   //view观察器

    private var spyRoomContext: SpyRoomContext

    constructor(contentView: ViewGroup, spyRoomContext: SpyRoomContext) {
        this.contentView = contentView
        this.spyRoomContext = spyRoomContext

        spyRoomTitleView = SpyRoomTitleView(contentView.findViewById(R.id.rl_spy_room_title_view) as ViewGroup, spyRoomContext)
        spyRoomStageView = SpyRoomStageView(contentView.findViewById(R.id.rl_spy_room_stage_view) as ViewGroup, spyRoomContext)
        spyRoomMessageView = SpyRoomMessageView(contentView.findViewById(R.id.rl_spy_room_message_view) as ViewGroup, spyRoomContext)
        spyRoomOperationView = SpyRoomOperationView(contentView.findViewById(R.id.rl_spy_room_operate_view) as ViewGroup, spyRoomContext)
        spyRoomSpeakView = SpyRoomSpeakView(contentView.findViewById(R.id.rl_spy_room_speak_view) as ViewGroup, spyRoomContext)

        initKeyBoardHideEvent()
        initViewObservers()
    }

    //TODO 优化
    private fun initViewObservers() {
        observers.add(spyRoomTitleView.observer)
        observers.add(spyRoomStageView.observer)
        observers.add(spyRoomMessageView.observer)
        observers.add(spyRoomOperationView.observer)
        observers.add(spyRoomSpeakView.observer)

        spyRoomContext.registerPreProcess(this.spyRoomStageView.preObserver)//TODO 优化
        spyRoomContext.registerPostProcess(this.observer)
    }

    fun onResume() {

    }

    fun initKeyBoardHideEvent() {
        contentView.setOnClickListener {
            try {
                val imm = contentView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(contentView.windowToken, 0)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun onBackPressed(onBackPressedListener: View.OnClickListener) {
        spyRoomTitleView.onBack(onBackPressedListener)
    }

    /**
     * 更改房间背景
     */
    private fun updateBackGround() {
        PicassoUtils.loadImageByDefaultNoPlaceHolder(MainApplication.getInstance(),
                SpyRoomConfig.background,
                contentView.iv_spy_room_background,
                R.mipmap.ic_spy_room_background)
    }

    /**
     * 断网后UI的处理
     */
    fun disableTouch() {
        //contentView.view_spy_room_disconnect.visibility = View.VISIBLE
        //contentView.view_spy_room_disconnect_top.visibility = View.VISIBLE
        spyRoomMessageView.observer.refresh(SpyRoomConstants.CODE_DISCONNECT)
    }

    /**
     * 重连后UI的处理
     */
    fun enableTouch() {
        //contentView.view_spy_room_disconnect.visibility = View.GONE
        //contentView.view_spy_room_disconnect_top.visibility = View.GONE
        spyRoomMessageView.observer.refresh(SpyRoomConstants.CODE_CONNECT)
    }

    //更新音频socket连接状态
    fun changeAudioSocketConnectedState(isConnected: Boolean) {
        spyRoomTitleView.refreshNetWifiState(isConnected)
        spyRoomSpeakView.refreshConnected(isConnected)
    }

    //更新游戏socket连接状态
    fun changeGameSocketConnectedState(isConnected: Boolean) {
    }

    override fun refresh(data: Refreshable) {
    }

    override fun restore(data: Refreshable) {
    }

    override fun reset() {
    }

    override fun clear() {
    }

    /**
     * 展示开始图片
     */
    private fun showStart() {
        contentView.iv_spy_room_status_view.visibility = View.VISIBLE
        postDelaySafely(Runnable { hideStart() }, SpyRoomConfig.startDuration.toLong())
    }

    /**
     * 隐藏开始图片
     */
    private fun hideStart() {
        contentView.iv_spy_room_status_view.visibility = View.GONE
    }

    /**
     * 显示第几轮
     */
    private fun showCurrentLen() {
        if (SpyRoomConfig.isGaming && SpyRoomConfig.gameLen > 0) {
            contentView.fl_spy_room_len.tv_spy_room_len_index.text = MessageUtils.getString(R.string.str_spy_room_len, SpyRoomConfig.gameLen)
            contentView.fl_spy_room_len.tv_spy_room_len_intro.text = MessageUtils.getString(R.string.str_spy_room_len_intro, SpyRoomConfig.speechPosition)
            contentView.fl_spy_room_len.visibility = View.VISIBLE
            postDelaySafely(Runnable { hideCurrentLen() }, SpyRoomConfig.lenShowTime)
        }
    }

    /**
     * 隐藏显示
     */
    private fun hideCurrentLen() {
        contentView.fl_spy_room_len.visibility = View.GONE
    }

    override fun destroy() {
        spyRoomContext.unRegisterProcess(this.observer)
        spyRoomTitleView.destroy()
        spyRoomStageView.destroy()
        spyRoomMessageView.destroy()
        spyRoomOperationView.destroy()
        spyRoomSpeakView.destroy()

        contentView.giftAnimationView_spy_room.destroy()
        contentView.secondGiftAnimation_spy_room.destroy()
        contentView.removeAllViews()
    }

    /**
     * 消息处理
     */
    private var observer = object : SpyRoomProcessable() {
        override fun handleJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleJoin(spyRoomMessenger) }
            return false
        }

        override fun handleLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleLeave(spyRoomMessenger) }
            return false
        }

        override fun handleKickOut(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleKickOut(spyRoomMessenger) }
            return false
        }

        override fun handleRestore(spyRoomMessenger: SpyRoomMessenger): Boolean {
            hideStart()
            observers.forEach { it.handleRestore(spyRoomMessenger) }
            return false
        }

        override fun handleDisconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleDisconnect(spyRoomMessenger) }
            return false
        }

        override fun handleReconnect(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleReconnect(spyRoomMessenger) }
            return false
        }

        override fun handleChangeRoomOwner(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleChangeRoomOwner(spyRoomMessenger) }
            return false
        }

        override fun handleUpdateMaster(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUpdateMaster(spyRoomMessenger) }
            return false
        }

        override fun handleUpdateRoomConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUpdateRoomConfig(spyRoomMessenger) }
            return false
        }

        override fun handleChangeRoomPassword(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleChangeRoomPassword(spyRoomMessenger) }
            return false
        }

        override fun handleStartConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleStartConfig(spyRoomMessenger) }
            return false
        }

        override fun handleUpdateConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUpdateConfig(spyRoomMessenger) }
            return false
        }

        override fun handleStopConfig(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleStopConfig(spyRoomMessenger) }
            return false
        }

        override fun handleGetConfigWords(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleGetConfigWords(spyRoomMessenger) }
            return false
        }

        override fun handleUpSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUpSeat(spyRoomMessenger) }
            return false
        }

        override fun handleDownSeat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleDownSeat(spyRoomMessenger) }
            return false
        }

        override fun handlePrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handlePrepare(spyRoomMessenger) }
            return false
        }

        override fun handleUnPrepare(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUnPrepare(spyRoomMessenger) }
            return false
        }

        override fun handleGameStart(spyRoomMessenger: SpyRoomMessenger): Boolean {
            showStart()
            observers.forEach { it.handleGameStart(spyRoomMessenger) }
            return false
        }

        override fun handleAssignRole(spyRoomMessenger: SpyRoomMessenger): Boolean {
            hideStart()
            observers.forEach { it.handleAssignRole(spyRoomMessenger) }
            return false
        }

        override fun handleStartSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            showCurrentLen()
            observers.forEach { it.handleStartSpeech(spyRoomMessenger) }
            return false
        }

        override fun handleEndSpeech(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleEndSpeech(spyRoomMessenger) }
            return false
        }

        override fun handleSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleSpeak(spyRoomMessenger) }
            return false
        }

        override fun handleUnSpeak(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleUnSpeak(spyRoomMessenger) }
            return false
        }

        override fun handleStartVote(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleStartVote(spyRoomMessenger) }
            return false
        }

        override fun handleVoteInfoTemporary(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleVoteInfoTemporary(spyRoomMessenger) }
            return false
        }

        override fun handleVoteResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleVoteResult(spyRoomMessenger) }
            return false
        }

        override fun handleDeathInfo(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleDeathInfo(spyRoomMessenger) }
            return false
        }

        override fun handleGameOver(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleGameOver(spyRoomMessenger) }
            return false
        }

        override fun handleChat(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleChat(spyRoomMessenger) }
            return false
        }

        override fun handleAddFriend(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleAddFriend(spyRoomMessenger) }
            return false
        }

        override fun handleGuessWord(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleGuessWord(spyRoomMessenger) }
            return false
        }

        override fun handleGuessWordResult(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleGuessWordResult(spyRoomMessenger) }
            return false
        }

        override fun handleSystemMessage(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleSystemMessage(spyRoomMessenger) }
            return false
        }

        override fun handleObserverJoin(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleObserverJoin(spyRoomMessenger) }
            return false
        }

        override fun handleObserverLeave(spyRoomMessenger: SpyRoomMessenger): Boolean {
            observers.forEach { it.handleObserverLeave(spyRoomMessenger) }
            return false
        }
    }


}