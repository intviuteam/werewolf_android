package orangelab.project.spyroom.manager

import android.os.Build
import cn.intviu.sdk.model.User
import cn.intviu.support.AppUtil
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import com.datasource.GlobalUserState
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import orangelab.project.MainApplication
import orangelab.project.common.PersonalDataHelper
import orangelab.project.common.engine.OrbitEngineManager
import orangelab.project.common.event.ServerEvent
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.common.model.PersonalData
import orangelab.project.common.utils.Utils

/**
 * 谁是卧底房语音socket管理器
 */
object SpyRoomAudioSocketManager : Keepable, Destroyable {

    private const val TAG = "SpyRoomAudioSocketManager"
    private var isAudioSocketConnected = false

    init {
        initAudioSocketConnectedStateListener()
    }

    /**
     * 启动音频连接
     */
    fun start(configBean: SpyRoomAudioConfigBean) {
        initAndStartAudioEngine(configBean)
        //初始化完毕，可以说话
        MainApplication.getInstance().socketEngineHelper.startPollMessage()
    }

    /**
     * 关闭音频连接
     */
    fun shutDown(callback: Runnable) {
        releaseAndCloseAudioEngine(callback)
    }

    /**
     * 获取连接状态
     */
    fun isConnected(): Boolean {
        return isAudioSocketConnected
    }

    /**
     * 初始化并启动
     */
    private fun initAndStartAudioEngine(configBean: SpyRoomAudioConfigBean?) {
        if (configBean?.mediaServer != null) {
            val user = User()
            var versionName = AppUtil.getVersionName(MainApplication.getInstance())
            if (Build.CPU_ABI.contains("86")) {
                versionName = "$versionName-x86"
            }
            user.name = versionName + "_" + GlobalUserState.getGlobalState().userName
            user.setID(GlobalUserState.getGlobalState().userId)
            //自己是游客
            try {
                if (Utils.checkUserIdIsTourist(GlobalUserState.getGlobalState().userId)) {
                    user.setID(PersonalDataHelper.getPersonalData().userId)
                }
            } catch (e: Exception) {
                PLog.e(TAG, "initAndStartAudioEngine error : " + e.message)
            }

            //启动orbit
            OrbitEngineManager.INSTANCE.start(MainApplication.getInstance(), configBean.roomId, configBean.mediaServer, user)
        }
    }

    /**
     * 关闭orbit
     */
    private fun releaseAndCloseAudioEngine(callback: Runnable) {
        OrbitEngineManager.INSTANCE.closeRecord()
        OrbitEngineManager.INSTANCE.shutDown(callback)
    }

    /**
     * 音频socket连接状态监听器
     */
    private fun initAudioSocketConnectedStateListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent::class.java).action { audioServerEvent ->
            Utils.runSafely {
                when (audioServerEvent.action) {
                    ServerEvent.RECONNECT_FROM_AUDIO_SERVER -> {
                        isAudioSocketConnected = true
                        PLog.e(TAG, "RECONNECT_FROM_AUDIO_SERVER:$isAudioSocketConnected")
                    }
                    ServerEvent.DISCONNECT_FROM_AUDIO_SERVER -> {
                        isAudioSocketConnected = false
                        PLog.e(TAG, "DISCONNECT_FROM_AUDIO_SERVER:$isAudioSocketConnected")
                    }
                }
            }
        }.register()
    }


    override fun destroy() {
        RxToolKit.UnRegister(this)
        isAudioSocketConnected = false
    }

    /**
     * 谁是卧底房间语音配置类
     */
    class SpyRoomAudioConfigBean {
        var roomId: String
        var mediaServer: EnterRoomResult.RoomServerMessage

        constructor(roomId: String, mediaServer: EnterRoomResult.RoomServerMessage) {
            this.roomId = roomId
            this.mediaServer = mediaServer
        }
    }
}