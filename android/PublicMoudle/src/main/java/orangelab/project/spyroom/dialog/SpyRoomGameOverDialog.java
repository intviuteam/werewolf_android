package orangelab.project.spyroom.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.URLManager;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Destroyable;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import cn.intviu.support.ShareTools;
import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.ProjectConfig;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.spyroom.SpyRoomConfig;
import orangelab.project.spyroom.api.SpyRoomApiDefines;
import orangelab.project.spyroom.data.SpyRoomHelper;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.dialog.BasicFormatDialog;
import orangelab.project.voice.model.VoiceSAFBean;
import orangelab.project.voice.model.VoiceSimpleUserBean;
import orangelab.project.voice.share.ShareBuilder;
import orangelab.project.voice.share.ShareToolKit;
import orangelab.project.voice.share.ShareType;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by lloydfinch on 28/11/2017.
 * <p>
 * 目前逻辑:自己总是在第一行(需求)
 */

public class SpyRoomGameOverDialog extends BasicFormatDialog implements Destroyable {

    private ImageView ivIcon;//顶部胜利失败图标

    private TextView tvWordSpy;
    private TextView tvWordGood;

    private View btnGoodTopic;
    private View btnBadTopic;

    private ImageView ivVafFirst;
    private ImageView ivVafSecond;
    private TextView tvVafFirst;
    private TextView tvVafSecond;

    private TextView tvRevenge;

    private View ivShareToWechat;
    private View ivShareToMoments;
    private View ivShareToQQ;
    private View ivShareToLine;
    private View ivShareToFaceBook;

    private View btnConfirm;
    private LinearLayout llFirstContainer;
    private LinearLayout llSecondContainer;

    private View llCommentContainer;
    private ImageView ivGoodComment;
    private ImageView ivBadComment;

    private RxPermissions permissions;
    private VoiceSAFBean bean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public SpyRoomGameOverDialog(@NonNull Context context) {
        this(context, R.style.DarkDialog);
    }

    public SpyRoomGameOverDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    public void initView() {
        setContentView(R.layout.layout_dialog_spy_room_game_over);

        ivIcon = (ImageView) findViewById(R.id.iv_flag_icon);

        tvWordSpy = (TextView) findViewById(R.id.tv_spy_word);
        tvWordGood = (TextView) findViewById(R.id.tv_good_word);

        btnGoodTopic = findViewById(R.id.btn_good_topic);
        btnBadTopic = findViewById(R.id.btn_bad_topic);

        ivVafFirst = (ImageView) findViewById(R.id.iv_vaf_spy);
        ivVafSecond = (ImageView) findViewById(R.id.iv_vaf_good);
        tvVafFirst = (TextView) findViewById(R.id.tv_vaf_spy);
        tvVafSecond = (TextView) findViewById(R.id.tv_vaf_good);
        tvRevenge = (TextView) findViewById(R.id.tv_revenge);

        ivShareToWechat = findViewById(R.id.iv_share_wechat);
        ivShareToMoments = findViewById(R.id.iv_share_moments);
        ivShareToQQ = findViewById(R.id.iv_share_qq);
        ivShareToLine = findViewById(R.id.iv_share_line);
        ivShareToFaceBook = findViewById(R.id.iv_share_fb);

        btnConfirm = findViewById(R.id.btn_confirm);
        llFirstContainer = (LinearLayout) findViewById(R.id.ll_spy_container);
        llSecondContainer = (LinearLayout) findViewById(R.id.ll_good_container);

        llCommentContainer = findViewById(R.id.ll_comment_container);
        ivGoodComment = (ImageView) findViewById(R.id.iv_good_comment);
        ivBadComment = (ImageView) findViewById(R.id.iv_bad_comment);
    }

    @Override
    public void initWindow() {
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ScreenUtils.getScreenWidth();
        params.height = ScreenUtils.getScreenHeight();
        getWindow().setAttributes(params);
    }

    @Override
    public void initListener() {
        initShare();
        btnConfirm.setOnClickListener(v -> dismiss());
        btnGoodTopic.setOnClickListener(v -> {
            goodTopic();
            disableComment();
            tipAlreadyComment(true);
        });
        btnBadTopic.setOnClickListener(v -> {
            badTopic();
            disableComment();
            tipAlreadyComment(false);
        });
    }

    private void tipAlreadyComment(boolean isGood) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_voice_topic_comment);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llCommentContainer.setVisibility(View.INVISIBLE);
                ivGoodComment.setVisibility(View.INVISIBLE);
                ivBadComment.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        llCommentContainer.setVisibility(View.VISIBLE);
        if (isGood) {
            ivGoodComment.setVisibility(View.VISIBLE);
            ivGoodComment.startAnimation(animation);
        } else {
            ivBadComment.setVisibility(View.VISIBLE);
            ivBadComment.startAnimation(animation);
        }
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_comment_success));
    }

    private void disableComment() {
        btnGoodTopic.setEnabled(false);
        btnBadTopic.setEnabled(false);
    }

    //好题
    private void goodTopic() {
        VoiceSocketHelper.sendTopicComment(true);
    }

    //坏题
    private void badTopic() {
        VoiceSocketHelper.sendTopicComment(false);
    }

    //设置卧底词
    public void setSpyWord(CharSequence spyWord) {
        tvWordSpy.setText(spyWord);
    }

    //设置好人词
    public void setGoodWord(CharSequence goodWord) {
        tvWordGood.setText(goodWord);
    }

    private void changeMessage(CharSequence content) {
        tvRevenge.setText(content);
    }

    //分享
    private void initShare() {
        if (ProjectConfig.isKuaiWan()) {
            initShareCn();
        } else {
            initShareNation();
        }
    }

    //国际分享
    private void initShareNation() {

        String name = PersonalDataHelper.getPersonalData().getUserName();
        String roomId = SpyRoomConfig.INSTANCE.getRoomId();
        String password = SpyRoomConfig.INSTANCE.getPassword();
        ShareBuilder builder = new ShareBuilder(ShareType.FROM_TYPE_SPY_ROOM, name, roomId, password);

        String avatar = PersonalDataHelper.getPersonalData().getImageUrl();
        String url = String.format(URLManager.GetDefaultURL() + SpyRoomApiDefines.SPY_ROOM_SHARE, roomId, avatar, name, password);

        try {
            String avatar1 = URLEncoder.encode(avatar, "UTF-8");
            String name1 = URLEncoder.encode(name, "UTF-8");
            url = String.format(URLManager.GetDefaultURL() + SpyRoomApiDefines.SPY_ROOM_SHARE, roomId, avatar1, name1, password);
        } catch (UnsupportedEncodingException e) {
            PLog.e("initShareNation", "error occur when encode spy share url, the message is: " + e.getMessage());
            e.printStackTrace();
        }

        builder.setShareUrl(url);

        String pswString = TextUtils.isEmpty(password) ? "" : MessageUtils.getString(R.string.str_psw, password);
        String title = MessageUtils.getString(R.string.str_spy_room_share_title);
        String message = MessageUtils.getString(R.string.str_spy_room_share_message, roomId, pswString);
        builder.setShareTitle(title);
        builder.setShareMsg(message);

        ivShareToWechat.setOnClickListener(v -> {
            ShareToolKit.build(builder).shareToWeChat((Activity) mContext);
        });

        ivShareToMoments.setOnClickListener(v -> {
            ShareToolKit.build(builder).shareToMoments((Activity) mContext);
        });

        ivShareToQQ.setOnClickListener(v -> {
            ShareToolKit.build(builder).shareToQQ((Activity) mContext);
        });

        ivShareToLine.setOnClickListener(v -> {
            ShareToolKit.build(builder).shareToLine((Activity) mContext);
        });

        ivShareToFaceBook.setOnClickListener(v -> {
            ShareToolKit.build(builder).shareToFaceBook((Activity) mContext);
        });
    }

    //国内分享
    private void initShareCn() {
        ivShareToWechat.setOnClickListener(v -> shareToWechat());
        ivShareToMoments.setOnClickListener(v -> shareToMoments());
        ivShareToQQ.setOnClickListener(v -> shareToQQ());
        ivShareToLine.setOnClickListener(v -> shareToLine());
        ivShareToFaceBook.setOnClickListener(v -> shareToFaceBook());

        //隐藏入口
        ivShareToFaceBook.setVisibility(View.GONE);
        ivShareToLine.setVisibility(View.GONE);
    }


    private void shareToWechat() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.WEIXIN));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToMoments() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.WEIXIN_CIRCLE));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToQQ() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_QQ)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                doShare(SHARE_MEDIA.QQ);
                                orangelab.project.common.utils.ShareTools.shareSuccessReported(Constant.SHARE_TO_QQ);
                            } else {
                                ToastToolKit.showShort(R.string.permission_read_store_error);
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToFaceBook() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_FACEBOOK)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.FACEBOOK));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToLine() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_LINE)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.LINE));
                            } else {
                                ToastToolKit.showShort(R.string.permission_read_store_error);
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void doShare(SHARE_MEDIA share_media) {
        if (mContext instanceof Activity) {
            ShareToolKit.create().shareSpyGameOverImage((Activity) mContext, bean, share_media);
        } else {
            PLog.e("VoiceVictoryOrFailureDialog", "share need a Activity not a Context!!!");
        }
    }


    @Override
    public void initEvent() {

    }

    @Override
    public void initData() {
        if (mContext instanceof Activity) {
            this.permissions = new RxPermissions((Activity) mContext);
        } else {
            ToastToolKit.showShort(R.string.permission_read_store_error);
        }
    }

    public void setData(VoiceSAFBean data) {
        if (data == null) {
            return;
        }
        bean = data;
        setSpyWord(data.spyWord);
        setGoodWord(data.goodWord);
        if (data.isSpy) {
            if (data.victory) {
                spyVictory();
            } else {
                spyFailure();
            }
        } else {
            if (data.victory) {
                goodVictory();
            } else {
                goodFailure();
            }
        }

        addToFirstRow(data.first);
        addToSecondRow(data.second);
    }

    @Override
    public void destroy() {
        llFirstContainer.removeAllViews();
        llSecondContainer.removeAllViews();

        ivGoodComment.clearFocus();
        ivBadComment.clearAnimation();
    }

    @Override
    protected void release() {

    }

    //自己是卧底且胜利
    public void spyVictory() {

        ivIcon.setImageResource(R.mipmap.ico_voice_spy_victory);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_victory);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_spy_victory));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_failure);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_good_failure));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        changeMessage(MessageUtils.getString(R.string.str_voice_show));
    }

    //自己是卧底且失败
    public void spyFailure() {
        ivIcon.setImageResource(R.mipmap.ico_voice_spy_failure);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_failure);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_spy_failure));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_victory);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_good_victory));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        changeMessage(MessageUtils.getString(R.string.str_voice_revenge));
    }

    //自己是好人且胜利
    public void goodVictory() {

        ivIcon.setImageResource(R.mipmap.ico_voice_good_victory);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_victory);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_good_victory));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_failure);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_spy_failure));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        changeMessage(MessageUtils.getString(R.string.str_voice_show));
    }

    //自己是好人且失败
    public void goodFailure() {
        ivIcon.setImageResource(R.mipmap.ico_voice_good_failure);

        ivVafFirst.setImageResource(R.mipmap.ico_voice_failure);
        tvVafFirst.setText(MessageUtils.getString(R.string.str_voice_good_failure));
        tvVafFirst.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_failure));

        ivVafSecond.setImageResource(R.mipmap.ico_voice_victory);
        tvVafSecond.setText(MessageUtils.getString(R.string.str_voice_spy_victory));
        tvVafSecond.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_victory));

        changeMessage(MessageUtils.getString(R.string.str_voice_revenge));
    }

    //添加第一行用户,自己的角色类型
    public void addToFirstRow(List<VoiceSimpleUserBean> userBeans) {
        llFirstContainer.removeAllViews();
        for (VoiceSimpleUserBean userBean : userBeans) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_voice_head_simple, null);
            CircleImageView ivHead = (CircleImageView) itemView.findViewById(R.id.iv_head_member);
            TextView tvName = (TextView) itemView.findViewById(R.id.tv_name_member);
            TextView tvNumber = (TextView) itemView.findViewById(R.id.tv_head_tag);
            if (SpyRoomHelper.INSTANCE.isSelf(userBean.userId)) {
                ivHead.setBorderColor(Color.YELLOW);
                TextView tvTag = (TextView) itemView.findViewById(R.id.tv_head_tag);
                tvTag.setBackgroundResource(R.drawable.bg_voice_vote_num_yellow);
                tvTag.setTextColor(Color.parseColor("#1f0064"));
            }
            llFirstContainer.addView(itemView);

            PicassoUtils.loadImage(mContext, userBean.headUrl, ivHead, R.mipmap.default_head);
            tvName.setText(userBean.userName);
            tvNumber.setText(String.valueOf(userBean.position + 1));
        }
    }

    //添加第二行用户,跟自己对立的角色类型
    public void addToSecondRow(List<VoiceSimpleUserBean> userBeans) {
        llSecondContainer.removeAllViews();

        for (VoiceSimpleUserBean userBean : userBeans) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_voice_head_simple, null);
            CircleImageView ivHead = (CircleImageView) itemView.findViewById(R.id.iv_head_member);
            TextView tvName = (TextView) itemView.findViewById(R.id.tv_name_member);
            TextView tvNumber = (TextView) itemView.findViewById(R.id.tv_head_tag);
            if (SpyRoomHelper.INSTANCE.isSelf(userBean.userId)) {
                ivHead.setBorderColor(Color.YELLOW);
                TextView tvTag = (TextView) itemView.findViewById(R.id.tv_head_tag);
                tvTag.setBackgroundResource(R.drawable.bg_voice_vote_num_yellow);
                tvTag.setTextColor(Color.parseColor("#1f0064"));
            }
            llSecondContainer.addView(itemView);

            PicassoUtils.loadImage(mContext, userBean.headUrl, ivHead, R.mipmap.default_head);
            tvName.setText(userBean.userName);
            tvNumber.setText(String.valueOf(userBean.position + 1));
        }
    }

    public void setOnConfirmClickedListener(OnConfirmClickedListener onConfirmClickedListener) {
        if (onConfirmClickedListener != null) {
            btnConfirm.setOnClickListener(v -> onConfirmClickedListener.onConfirmClick(v));
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        destroy();
    }

    public interface OnConfirmClickedListener {
        void onConfirmClick(View view);
    }
}
