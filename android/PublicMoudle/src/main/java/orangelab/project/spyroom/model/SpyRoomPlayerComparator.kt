package orangelab.project.spyroom.model

import com.toolkit.action.Keepable

class SpyRoomPlayerComparator : Comparator<SpyRoomPlayer>, Keepable {

    override fun compare(o1: SpyRoomPlayer?, o2: SpyRoomPlayer?): Int {
        if (o1 == null || o2 == null) {
            return 0
        }
        return o1.position - o2.position
    }

}