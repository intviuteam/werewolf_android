package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:13
 * Mystery
 */

public class MiniGameInviteBeRefused implements Keepable {
    public String type = "invite_be_refused";
    public String invite_user_id;
    public String target_user_id;
    public String game_type;
    public MiniGameUser target_user;
}
