package orangelab.project.minigame.model;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/5/22 下午2:15
 * Mystery
 */
public class MiniGameOverForConsole implements Keepable {

    public String game_state;
    public String score_mode;
    public String game_result;
    public long game_result_value;
    public String game_mode;

    public int ai_level;
    public int ai_level_change = -1;
    public int winning_streak_change = 0;


    public boolean isAiLevelChange() {
        return ai_level_change != -1;
    }

    public boolean isWin() {
        return TextUtils.equals(game_state, "win");
    }

    public boolean isLose() {
        return TextUtils.equals(game_state, "lose");
    }

    public boolean isPeace() {
        return TextUtils.equals(game_state, "peace");
    }

    public boolean isNone() {
        return TextUtils.equals(game_state, "none") || TextUtils.isEmpty(game_state);
    }


}
