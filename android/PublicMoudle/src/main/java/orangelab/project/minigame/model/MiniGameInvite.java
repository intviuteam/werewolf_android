package orangelab.project.minigame.model;

import orangelab.project.minigame.MiniGameConstant;
import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午1:53
 * Mystery
 */

public class MiniGameInvite implements Keepable {

    public String type = MiniGameConstant.INVITE;
    public String game_type;
    public String target_user_id;
    public long wait_timer;

}
