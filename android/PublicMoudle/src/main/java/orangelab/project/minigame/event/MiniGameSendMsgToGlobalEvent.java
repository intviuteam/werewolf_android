package orangelab.project.minigame.event;

import orangelab.project.minigame.MiniGameConstant;
import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:58
 * Mystery
 */

public class MiniGameSendMsgToGlobalEvent implements Keepable {

    public String type = MiniGameConstant.PLAY_MINI_GAME;
    public String payload;

    public MiniGameSendMsgToGlobalEvent(String payload) {
        this.payload = payload;
    }
}
