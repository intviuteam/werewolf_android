package orangelab.project.minigame;

import android.widget.PopupWindow;

import orangelab.thirdparty.leancloud.chatkit.event.LCIMMiniGameEvent;

/**
 * 小游戏点击的回调
 */
public interface MiniGameItemClickListener {

    void onItemClicked(PopupWindow popupWindow, LCIMMiniGameEvent event);
}
