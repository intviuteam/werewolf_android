package orangelab.project.minigame.event;

import orangelab.project.minigame.model.MiniGameLaunch;

/**
 * game_werewolf
 * 2018/2/1 下午2:59
 * Mystery
 */

public class MiniGameReMatchEvent {

    private MiniGameLaunch mMiniGameLaunch;

    public MiniGameReMatchEvent(MiniGameLaunch miniGameLaunch) {
        mMiniGameLaunch = miniGameLaunch;
    }

    public MiniGameLaunch getMiniGameLaunch() {
        return mMiniGameLaunch;
    }
}
