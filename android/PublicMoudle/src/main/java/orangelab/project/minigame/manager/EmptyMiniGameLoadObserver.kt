package orangelab.project.minigame.manager

import com.androidtoolkit.PLog

/**
 *game_werewolf
 *2018/4/3 下午3:22
 *Mystery
 */
open class EmptyMiniGameLoadObserver : MiniGameManager.MiniGameLoadObserver {

    constructor()

    companion object {
        val TAG = "MiniGameManager"
    }

    override fun onBusy() {
        printLog("onBusy")
    }

    override fun onDownloadingStart() {
        printLog("onDownloadingStart")
    }

    override fun onDownloading(percent: Float) {
        printLog("onDownloading ${percent}")
    }

    override fun onDownloadingFinish() {
        printLog("onDownloadingFinish ")
    }

    override fun onLoadStart() {
        printLog("onLoadStart ")
    }

    override fun onChecking() {
        printLog("onChecking")
    }

    override fun onCheckSuccess() {
        printLog("onCheckSuccess")
    }

    override fun onCheckFailed() {
        printLog("onCheckFailed")
    }

    override fun onError(reason: String) {
        printLog("onError${reason}")
    }

    override fun onUnZipDone() {
        printLog("onUnZipDone")
    }

    override fun onUnZipping() {
        printLog("onUnZipping")
    }

    override fun onLoadFinish(url: String) {
        printLog("onLoadFinish ${url}")
    }

    override fun onGenerateManifest() {
        printLog("onGenerateManifest")
    }

    override fun onGenerateManifestDone() {
        printLog("onGenerateManifestDone")
    }

    override fun onGenerateZipRecord() {
        printLog("onGenerateZipRecord")
    }

    override fun onGenerateZipRecordDone() {
        printLog("onGenerateZipRecordDone")
    }

    private fun printLog(info: String) {
        PLog.i(TAG, info)
    }

    override fun isCancel(): Boolean {
        return false
    }


}