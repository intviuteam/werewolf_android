package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:12
 * Mystery
 */

public class MiniGameRefuseInvite implements Keepable {

    public String type = "refuse_invite";
    public String game_type;
    public String invite_user_id;
}
