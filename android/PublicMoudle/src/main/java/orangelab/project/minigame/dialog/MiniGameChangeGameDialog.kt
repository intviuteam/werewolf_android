package orangelab.project.minigame.dialog

import android.content.Context
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.R
import com.androidtoolkit.ToastToolKit
import com.toolkit.ToolKit
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.minigame.MiniGamesApiManager
import orangelab.project.minigame.adapter.MiniGamesListAdapter
import orangelab.project.minigame.adapter.MiniGamesViewPagerAdapter
import orangelab.project.minigame.manager.MiniGameDownloadManager
import orangelab.project.minigame.model.MiniGameItem
import orangelab.project.voice.basic.BasicViewHolder
import java.util.ArrayList

/**
 *game_werewolf
 *2018/5/18 上午10:58
 *Mystery
 */
class MiniGameChangeGameDialog : SafeDialog, View.OnClickListener {

    interface MiniGameChangeGameObserver {

        fun onSelectGame(miniGameItem: MiniGameItem)
    }

    private val TAG = "MiniGameChangeGameDialog"

    private var mContext: Context
    private var mTotalPages = 0
    private var mCurPages = 0
    private val mPageMaxIndex = 6
    private var mGameViewPager: ViewPager
    private var mGameIndex: LinearLayout
    private var mGameDataLoading: ProgressBar

    private val mGridViewList = ArrayList<GridView>(3)
    private lateinit var mGameIndexPoints: Array<ImageView?>

    private var mObserver: MiniGameChangeGameObserver? = null

    constructor(context: Context, observer: MiniGameChangeGameObserver?) : super(context, R.style.DarkDialogAllowDismiss) {
        this.mContext = context
        this.mObserver = observer
        setContentView(R.layout.layout_mini_game_finish_change_game)

        mGameViewPager = findViewById<ViewPager>(R.id.game_viewpager) as ViewPager
        mGameIndex = findViewById<LinearLayout>(R.id.game_index) as LinearLayout
        mGameDataLoading = findViewById<ProgressBar>(R.id.game_loading) as ProgressBar

        findViewById<FrameLayout>(R.id.container).setOnClickListener(this)

        resizeDialog(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        setCancelable(true)
    }

    override fun show() {

        super.show()

        turnIntoLoading()
        getMiniGameList()
    }

    private fun turnIntoLoading() {
        mGameViewPager.visibility = View.INVISIBLE
        mGameDataLoading.visibility = View.VISIBLE
    }

    private fun turnIntoShowData() {
        mGameViewPager.visibility = View.VISIBLE
        mGameDataLoading.visibility = View.GONE
    }

    private fun getMiniGameList() {
        MiniGamesApiManager.GetMiniGamesList(true, { result, e ->
            runSafely({
                if (e == null) {
                    /**
                     * 展示数据
                     */
                    val list = result.games

                    if (ToolKit.isEmpty(list)) {
                        ToastToolKit.showShort(R.string.mini_game_no_games)
                        dismiss()
                        return@runSafely
                    } else {

                        /**
                         * 更新下小游戏的各个进度
                         */
                        MiniGameDownloadManager.decorMiniGameList(list)

                        turnIntoShowData()
                        /**
                         * 计算总页数
                         */
                        mTotalPages = Math.ceil(list.size * 1.0 / mPageMaxIndex).toInt()
                        mGameIndex.removeAllViews()
                        mGameIndexPoints = arrayOfNulls(mTotalPages)

                        /**
                         * 初始化页码布局
                         */
                        for (i in 0 until mTotalPages) {
                            mGameIndexPoints[i] = ImageView(mContext)
                            if (i == mCurPages) {
                                mGameIndexPoints[i]!!.setImageResource(R.mipmap.ico_mini_game_list_index_select)
                            } else {
                                mGameIndexPoints[i]!!.setImageResource(R.mipmap.ico_mini_game_list_index_not_select)
                            }
                            mGameIndexPoints[i]!!.setPadding(5, 5, 5, 5)
                            mGameIndex.addView(mGameIndexPoints[i]!!)
                        }

                        /**
                         * 初始化GridView数组
                         */
                        for (i in 0 until mTotalPages) {
                            val gridView = View.inflate(mContext, R.layout.fragment_gift_viewpage_item, null) as GridView
                            gridView.numColumns = 3
                            gridView.adapter = MiniGamesListAdapter(mContext, list, i, mPageMaxIndex)
                            gridView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                                override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    val game = gridView.getItemAtPosition(position) as MiniGameItem

                                    val gameType = game.type
                                    val gameDownload = game.download
                                    if (TextUtils.isEmpty(gameType)) {
                                        return
                                    }
                                    if (!TextUtils.isEmpty(gameDownload)) {
                                        if (MiniGameDownloadManager.checkIsDownloading(gameType)) {
                                            return
                                        }

                                        /**
                                         * 这里要加一个判断，如果本地已经下载了这个游戏，那么就执行下面的方法
                                         */
                                        if (MiniGameDownloadManager.checkDownloaded(gameType, gameDownload)) {
                                            /**
                                             * 下载正确
                                             */


                                        } else {
                                            /**
                                             * 执行下载操作
                                             * 1.更新数据
                                             * 2.开始下载
                                             * 3.刷新UI
                                             */
                                            game.isDownloading = true
                                            game.isFinish = false
                                            game.progress = 0

                                            MiniGameDownloadManager.beginDownload(gameType, gameDownload)
                                            (gridView.adapter as MiniGamesListAdapter).notifyDataSetChanged()
                                            return
                                        }
                                    }


                                    mObserver?.onSelectGame(game)
                                    dismiss()
                                }
                            })
                            mGridViewList.add(gridView)
                        }

                        /**
                         * 初始化ViewPager
                         */
                        mGameViewPager.adapter = MiniGamesViewPagerAdapter(mGridViewList)
                        mGameViewPager.offscreenPageLimit = 0
                        mGameViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                            }

                            override fun onPageSelected(position: Int) {
                                for (i in 0 until mTotalPages) {
                                    if (i == position) {
                                        mGameIndexPoints[i]!!.setImageResource(R.mipmap.ico_mini_game_list_index_select)
                                    } else {
                                        mGameIndexPoints[i]!!.setImageResource(R.mipmap.ico_mini_game_list_index_not_select)
                                    }
                                }
                                mCurPages = position
                            }

                            override fun onPageScrollStateChanged(state: Int) {

                            }
                        })
                    }
                } else {
                    ToastToolKit.showShort(e.message)
                    dismiss()
                }
            })
        })
    }

    override fun onClick(v: View?) {
        v ?: return

        when (v.id) {
            R.id.container -> {
                dismiss()
            }
        }
    }

    override fun dismiss() {
        super.dismiss()
        for (temp in mGridViewList) {
            (temp.adapter as MiniGamesListAdapter).destroy()
        }
    }

    override fun release() {
        mObserver = null
    }
}