package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/4/3 下午5:07
 * Mystery
 */

public class MiniGameZipRecord implements Keepable {

    public String game_type;
    public long fileLongth = 0;
    public String zipMd5;
    public String urlMd5;
}
