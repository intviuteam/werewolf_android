package orangelab.project.minigame.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.transport.TransportCompat;
import com.datasource.GlobalUserState;
import com.BuildConfig;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.engine.OrbitEngine;
import orangelab.project.common.engine.IOrbitEngine;
import orangelab.project.common.engine.OrbitEngineManager;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.minigame.MiniGameConstant;
import orangelab.project.minigame.MiniGameContext;
import orangelab.project.minigame.MiniGameProcess;
import orangelab.project.minigame.model.MiniGameGameOverServer;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameReadyServer;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;

import com.google.gson.JsonSyntaxException;
import com.networktoolkit.transport.GlobalDecorateManager;
import com.networktoolkit.transport.TransportConfig;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Action;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cn.intviu.sdk.model.User;
import cn.intviu.service.IntviuApplication;
import cn.intviu.support.ConstInfo;
import cn.intviu.support.GsonHelper;
import rx.functions.Action1;

/**
 * game_werewolf
 * 2018/1/18 下午6:00
 * Mystery
 */

public class MiniGameView extends MiniGameProcess implements Destroyable {

    private static final String TAG = "MiniGameView";

    private MiniGameContext mMiniGameContext;

    private RxPermissions mRxPermissions;

    private ViewGroup mRoot;
    private WebView mWebView;

    private Object JSBridge = new Object() {
        //todo 添加Object方法
        @JavascriptInterface
        public void postNativeDirectlyCommend(String type, String json) {
            PLog.i(TAG, "postNativeDirectlyCommend: [" + type + "]," + json);
            if (mMiniGameContext != null) {
                mMiniGameContext.post(() -> {
                    try {
                        if (TextUtils.equals(type, MiniGameConstant.NATIVE_DESTROY)) {
                            if (mMiniGameContext != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(json);
                                    String reason = jsonObject.optString("reason");
                                    mMiniGameContext.forceDestroy(reason);
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    mMiniGameContext.forceDestroy("");
                                    return;
                                }
                            }
                            return;
                        }

                        if (TextUtils.equals(type, MiniGameConstant.ENTER_INIT)) {
                            try {
                                MiniGameGameOverServer server = GsonHelper.getGson().fromJson(json, MiniGameGameOverServer.class);
                                if (mMiniGameContext != null) {
                                    mMiniGameContext.setAgainAble(server.again);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.START_GAME_SERVER)) {
                            if (mMiniGameContext != null) {
                                mMiniGameContext.startGameReady(json);
                            }
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.DOM_SHOW)) {
                            if (mMiniGameContext != null) {
                                mMiniGameContext.forceIntoGame();
                            }
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.GAME_OVER_SERVER)) {
                            if (mMiniGameContext != null) {
                                mMiniGameContext.gameOverReady(json);
                            }
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.DOM_INIT)) {
                            if (mMiniGameContext != null) {
                                mMiniGameContext.loadUrlFinish(json);
                            }
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_CLOSE_RECORD)) {
                            OrbitEngineManager.INSTANCE.closeRecord();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_CLOSE_SPEAKER)) {
                            OrbitEngineManager.INSTANCE.closeSpeaker();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_OPEN_RECORD)) {
                            OrbitEngineManager.INSTANCE.openRecord();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_OPEN_SPEAKER)) {
                            OrbitEngineManager.INSTANCE.openSpeaker();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_EXIT)) {
                            orbitExit();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.ORBIT_ENTER)) {
                            orbitEnter();
                            return;
                        }
                        if (TextUtils.equals(type, MiniGameConstant.REPORT_ERROR_TO_UMENG)) {
                            try {
                                ReportEventUtils.reportError(new Exception("JSError" + new JSONObject(json).opt("error")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                        if (mMiniGameContext != null) {
                            mMiniGameContext.sendMessage(type, json);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    };

    public MiniGameView(MiniGameContext miniGameContext, RxPermissions rxPermissions) {
        mRxPermissions = rxPermissions;
        mMiniGameContext = miniGameContext;
        mMiniGameContext.registerProcess(this);
        mRoot = (ViewGroup) mMiniGameContext.getRootView().findViewById(R.id.id_amusement_game);
        mWebView = (WebView) mMiniGameContext.getRootView().findViewById(R.id.id_amusement_webview);
        mWebView.setDrawingCacheEnabled(true);

        WebSettings settings = mWebView.getSettings();
        String ua = settings.getUserAgentString();
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        settings.setJavaScriptEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setBlockNetworkImage(false);
        settings.setSupportZoom(false);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setAppCacheEnabled(true);
        settings.setDomStorageEnabled(true);

        mWebView.addJavascriptInterface(JSBridge, "Game");
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        mWebView.setBackgroundColor(0); // 设置背景色

        if (Build.VERSION.SDK_INT >= 19 && ProjectConfig.DEBUG_ABLE) {
            mWebView.setWebContentsDebuggingEnabled(true);
        }

        mWebView.setWebChromeClient(new WebChromeClient());

        mWebView.setWebViewClient(new WebViewClient() {

            @TargetApi(21)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                return super.shouldInterceptRequest(view, request);
            }

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                Log.i(TAG, "shouldInterceptRequest: WebView view, String url[" + Thread.currentThread() + "]");
                WebResourceResponse resourceResponse = TransportCompat.compatWebViewInterceptRequest(view, url);
                if (resourceResponse == null) {
                    return super.shouldInterceptRequest(view, url);
                } else {
                    return resourceResponse;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mMiniGameContext.loadUrlError(MessageUtils.getString(R.string.mini_game_resource_loading_error));
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (mMiniGameContext != null) {
                    mMiniGameContext.post(() -> {
                        if (mWebView != null) {
                            mWebView.loadUrl(url);
                        }
                    });
                }
                return true;
            }
        });

        initListener();
    }

    private void initListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent.class).action(value -> {
            if (mMiniGameContext != null) {
                mMiniGameContext.post(() -> {
                    switch (value.getAction()) {
                        case ServerEvent.RECONNECT_FROM_AUDIO_SERVER:
                            postJSDirectlyCommend(MiniGameConstant.ORBIT_CONNECT, MiniGameConstant.EMPTY_JSON_STRING);
                            break;
                        case ServerEvent.DISCONNECT_FROM_AUDIO_SERVER:
                            postJSDirectlyCommend(MiniGameConstant.ORBIT_DISCONNECT, MiniGameConstant.EMPTY_JSON_STRING);
                            break;
                    }
                });
            }
        }).register();
    }

    @Override
    public boolean onMiniGameServerMessage(String type, String json) {
//        if (TextUtils.equals(type, MiniGameConstant.READY_SERVER)) {
//            try {
//                MiniGameReadyServer server = GsonHelper.getGson().fromJson(json, MiniGameReadyServer.class);
//                if (TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), server.user_id)) {
//                    PLog.i(TAG, "myself is ready");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        postJSDirectlyCommend(type, json);
        return false;
    }

    @Override
    public boolean onMiniGameMessage(String type, String json) {
        postJSDirectlyCommend(type, json);
        return false;
    }

    @Override
    public boolean onMiniGameStartServer(String type, String json) {
        postJSDirectlyCommend(type, json);
        return false;
    }

    public void load(String url) {
        Map<String, String> extraHeaders = new HashMap<String, String>();
        if (GlobalDecorateManager.INSTANCE.getHeaderDecorate() != null) {
            GlobalDecorateManager.INSTANCE.getHeaderDecorate().decorateWebViewHeader(extraHeaders);
            PLog.i(TAG, "mini game view header : " + extraHeaders);
            url = GlobalDecorateManager.INSTANCE.getHeaderDecorate().decorateUrl(url);
            PLog.i(TAG, "mini game view url " + url);
            mWebView.loadUrl(url, extraHeaders);
        } else {
            mWebView.loadUrl(url);
        }
    }

    public void reload() {
        try {
            if (mWebView != null) {
                mWebView.reload();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        mRoot.setVisibility(View.VISIBLE);
    }

    public void gone() {
        mRoot.setVisibility(View.GONE);
    }

    public void postJSDirectlyCommend(String type, String json) {
        if (mMiniGameContext != null) {
            mMiniGameContext.post(() -> {
                postJSDirectlyCommendInner(type, json);
            });
        }
    }

    private void postJSDirectlyCommendInner(String type, String json) {
        if (mWebView != null) {
            PLog.i(TAG, "postJSDirectlyCommend: [" + type + "]," + json);
            mWebView.loadUrl("javascript:postJSDirectlyCommend('" + type + "','" + json + "')");
        }
    }

    private void orbitEnter() {
        /**
         * 这里要获取一下权限
         */
        if (mRxPermissions != null) {
            mRxPermissions.request(Manifest.permission.RECORD_AUDIO).subscribe(aBoolean -> {
                if (aBoolean) {
                    if (mMiniGameContext != null && mMiniGameContext.getRoomServerMessage() != null) {
                        EnterRoomResult.RoomServerMessage roomServerMessage = mMiniGameContext.getRoomServerMessage();
                        User user = new User();
                        String versionName = BuildConfig.VERSION_NAME;
                        if (Build.CPU_ABI.contains("86")) {
                            versionName = versionName + "-x86";
                        }
                        user.setName(versionName + "_" + GlobalUserState.getGlobalState().getUserName());
                        user.setID(GlobalUserState.getGlobalState().getUserId());
                        OrbitEngineManager.INSTANCE.start(mRoot.getContext(), mMiniGameContext.getRoomId(), roomServerMessage, user, true);
                    }
                } else {
                    postJSDirectlyCommend(MiniGameConstant.ORBIT_PERMISSION_DINED, "{}");
                }
            });
        }
    }

    private void orbitExit() {
        OrbitEngineManager.INSTANCE.shutDown(() -> {
            postJSDirectlyCommend(MiniGameConstant.ORBIT_EXITED, MiniGameConstant.EMPTY_JSON_STRING);
        });
    }

    public void loadBlank() {
        mWebView.loadUrl("about:blank");
    }

    @Override
    public void destroy() {
        mRxPermissions = null;
        mMiniGameContext.unRegisterProcess(this);
        RxToolKit.UnRegister(this);
        if (mWebView != null) {
            if (mRoot != null) {
                mRoot.removeAllViews();
            }
            mWebView.clearHistory();
            // NOTE: clears RAM cache, if you pass true, it will also clear the disk cache.
            // Probably not a great idea to pass true if you have other WebViews still alive.
            //mWebView.clearCache(true);

            // Loading a blank page is optional, but will ensure that the WebView isn't doing anything when you destroy it.
            loadBlank();
            mWebView.onPause();
            mWebView.removeAllViews();
            mWebView.destroyDrawingCache();

            // NOTE: This pauses JavaScript execution for ALL WebViews,
            // do not use if you have other WebViews still alive.
            // If you create another WebView after calling this,
            // make sure to call mWebView.resumeTimers().
//            mWebView.pauseTimers();

            // NOTE: This can occasionally cause a segfault below API 17 (4.2)
            mWebView.destroy();

            // Null out the reference so that you don't end up re-using it.
            mWebView = null;
        }
        if (mMiniGameContext != null) {
            mMiniGameContext = null;
        }
        if (mRoot != null) {
            mRoot = null;
        }
    }
}
