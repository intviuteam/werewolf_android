package orangelab.project.minigame;

import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameNotify;
import orangelab.project.minigame.model.MiniGameReplay;
import orangelab.project.common.model.EnterRoomResult;

import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2018/1/24 上午10:41
 * Mystery
 */

public class MiniGameProcessManager extends MiniGameProcess implements Destroyable {

    private List<MiniGameProcess> mMiniGameProcesses;

    public MiniGameProcessManager() {
        mMiniGameProcesses = new ArrayList<>();
    }

    public void add(MiniGameProcess miniGameProcess) {
        mMiniGameProcesses.add(miniGameProcess);
    }

    public void remove(MiniGameProcess miniGameProcess) {
        mMiniGameProcesses.remove(miniGameProcess);
    }

    @Override
    public boolean onMiniGameChangeRequired(String gameIcon, String gameName, String gameType, String fromUser) {
        try {
            for (MiniGameProcess process : mMiniGameProcesses) {
                process.onMiniGameChangeRequired(gameIcon, gameName, gameType, fromUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameMessage(String type, String json) {
        try {
            for (MiniGameProcess process : mMiniGameProcesses) {
                process.onMiniGameMessage(type, json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameMatched() {
        try {
            for (MiniGameProcess process : mMiniGameProcesses) {
                process.onMiniGameMatched();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onLoadUrlProgress(String userId, int process) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onLoadUrlProgress(userId, process);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onLoadUrlFinish(String json) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onLoadUrlFinish(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onLoadUrlError(String reason) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onLoadUrlError(reason);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameEnterServer(EnterRoomResult enterRoomResult) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameEnterServer(enterRoomResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameStartServer(String type, String json) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameStartServer(type, json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameStartServerReady(String json) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameStartServerReady(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameDomShow() {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameDomShow();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameOverServer(String type, String json) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameOverServer(type, json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameOverServerReady(String payload) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameOverServerReady(payload);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameUserLeaveServer() {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameUserLeaveServer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameUserReturnServer() {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameUserReturnServer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameForceDestroy(String reason) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameForceDestroy(reason);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameServerMessage(String type, String json) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameServerMessage(type, json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameKickOutServer() {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameKickOutServer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameReplayServer(MiniGameReplay miniGameReplay) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameReplayServer(miniGameReplay);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameNotifyServer(MiniGameNotify notify) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameNotifyServer(notify);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onMiniGameSwitchLaunch(MiniGameLaunch miniGameLaunch) {
        try {
            for (MiniGameProcess p : mMiniGameProcesses) {
                p.onMiniGameSwitchLaunch(miniGameLaunch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void destroy() {
        mMiniGameProcesses.clear();
    }
}
