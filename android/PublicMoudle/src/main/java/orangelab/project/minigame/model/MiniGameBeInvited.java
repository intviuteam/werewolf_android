package orangelab.project.minigame.model;


import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:02
 * Mystery
 */

public class MiniGameBeInvited implements Keepable {

    public String type = "be_invited";
    public String invite_user_id;
    public String target_user_id;
    public String game_type;
}
