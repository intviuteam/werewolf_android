package orangelab.project.minigame.dialog

import android.graphics.Point
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView

import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.RxToolKit
import com.androidtoolkit.view.ScreenUtils
import com.datasource.GlobalUserState
import com.R
import com.androidtoolkit.PLog
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.engine.MiniGameSocketEngine
import orangelab.project.minigame.MiniGameConstant
import orangelab.project.minigame.MiniGameContext
import orangelab.project.minigame.event.*
import orangelab.project.minigame.model.MiniGameOver
import orangelab.project.minigame.model.MiniGameReplay
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.common.utils.CountDownTaskManager
import orangelab.project.common.utils.LeanCloudChatHelper
import orangelab.project.common.utils.MessageUtils
import it.sephiroth.android.library.tooltip.Tooltip
import orangelab.project.common.manager.ApiManager
import orangelab.project.game.dialog.WereWolfHintDialog
import orangelab.project.minigame.model.MiniGameItem
import orangelab.project.minigame.model.MiniGameNotify
import orangelab.project.minigame.view.MiniGameChangeGameView

/**
 * game_werewolf
 * 2018/1/31 下午6:45
 * Mystery
 */

class MiniGameFinishDialog : SafeDialog, View.OnClickListener {

    private final val COUNT_DOWN = 5;

    private val mRoot: ViewGroup;

    private val mClose: View

    private val mResultImageView: ImageView
    private val mResultReason: TextView

    private val mTodayRecord: TextView
    private val mTodayScore: TextView
    private val mLeftUserHead: ImageView
    private val mRightUserHead: ImageView
    private val mLeftName: TextView
    private val mRightName: TextView

    private var mMiniGameContext: MiniGameContext?
    private var mMiniGameOverData: MiniGameOver?
    private var mMiniGameScoreData: MiniGameOver.ScoreData?

    private val mPlayAgainView: View

    private val mMatchAgainView: View

    private val mChangeGameView: View

    private val mAddFriendView: View

    private var mOpponentReady: Boolean = false;
    private var mSelfReady: Boolean = false;

    private var mMiniGameReplay: MiniGameReplay? = null
    private var mMiniGameTooltip: Tooltip.TooltipView? = null

    private var mMiniGameActionLeftTooltip: Tooltip.TooltipView? = null
    private var mMiniGameActionRightTooltip: Tooltip.TooltipView? = null

    private var mCountDownTask: CountDownTaskManager.CountDownTask? = null

    private var mOppoLeave = false;

    private var mScoreText: TextView
    private var mLeveUpText: TextView

    private var mLeftTag: ImageView
    private var mRightTag: ImageView

    private var mActionLike: View
    private var mActionCiao: View
    private var mActionEncourage: View
    private var mActionPraise: View

    private var mUserId: String = ""
    private var mOppoId: String = ""

    private var mChangeGameDialog: MiniGameChangeGameDialog? = null
    private var mChangeGameIntviuView: MiniGameChangeGameView? = null
    //    private var mReMatchHintDialog: WereWolfHintDialog? = null
    private var mIntviuGameType: String = ""


    constructor(miniGameContext: MiniGameContext, miniGameOver: MiniGameOver?, miniGameScore: MiniGameOver.ScoreData?, isOppoLeave: Boolean) : super(miniGameContext.context, R.style.DarkDialogAllowDismiss) {

        mMiniGameContext = miniGameContext
        mMiniGameOverData = miniGameOver
        mMiniGameScoreData = miniGameScore

        setContentView(R.layout.layout_mini_game_game_finish_dialog)

        mRoot = findViewById(R.id.id_dialog_root_parent) as ViewGroup

        mClose = findViewById(R.id.id_dialog_close)
        mClose.setOnClickListener(this)

        mResultImageView = findViewById(R.id.id_mini_game_result) as ImageView
        mResultReason = findViewById(R.id.id_mini_game_over_reason) as TextView

        mTodayRecord = findViewById(R.id.id_mini_game_today_record) as TextView
        mTodayScore = findViewById(R.id.id_mini_game_today_score) as TextView

        mLeftUserHead = findViewById(R.id.id_mini_game_left_user) as ImageView
        mRightUserHead = findViewById(R.id.id_mini_game_right_user) as ImageView
        mLeftName = findViewById(R.id.id_mini_game_left_name) as TextView
        mRightName = findViewById(R.id.id_mini_game_right_name) as TextView

        mLeftTag = findViewById(R.id.id_mini_game_left_tag) as ImageView
        mRightTag = findViewById(R.id.id_mini_game_right_tag) as ImageView

        mScoreText = findViewById(R.id.id_mini_game_score) as TextView
        mLeveUpText = findViewById(R.id.id_mini_game_levelup) as TextView

        mMatchAgainView = findViewById(R.id.action_change_oppo)
        mMatchAgainView.setOnClickListener(this)

        mPlayAgainView = findViewById(R.id.action_again)
        mPlayAgainView.setOnClickListener(this)

        mChangeGameView = findViewById(R.id.action_change_game)
        mChangeGameView.setOnClickListener(this)

        mAddFriendView = findViewById(R.id.action_add_friend)
        mAddFriendView.setOnClickListener(this)

        mActionLike = findViewById(R.id.action_like)
        mActionLike.setOnClickListener(this)

        mActionCiao = findViewById(R.id.action_bye)
        mActionCiao.setOnClickListener(this)

        mActionEncourage = findViewById(R.id.action_encourage)
        mActionEncourage.setOnClickListener(this)

        mActionPraise = findViewById(R.id.action_praise)
        mActionPraise.setOnClickListener(this)

        initView()

        resizeDialog(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        setCancelable(false)

        mOppoLeave = isOppoLeave
        if (mOppoLeave) {
            if (isMatch()) {
                mMatchAgainView.post {
                    turnIntoChangeOppo(COUNT_DOWN)
                }
            } else {
                mChangeGameView.post {
                    turnIntoChangeGame(COUNT_DOWN)
                }
            }
        }
    }

    private fun updateAndShowLeftActionToolTip(message: String?) {

        message ?: return

        val location = IntArray(2)
        val viewWidth = mLeftUserHead.width
        val delta = ScreenUtils.dip2px(10f);
        mLeftUserHead.getLocationOnScreen(location)

        val point = Point(location[0] + viewWidth / 2, location[1] - delta)

        mMiniGameActionLeftTooltip?.remove()
        mMiniGameActionLeftTooltip = Tooltip.make(
                context, Tooltip.Builder(100)
                .anchor(point, Tooltip.Gravity.TOP)
                .text(message)
                .maxWidth(ScreenUtils.dip2px(200f))
                .withArrow(true)
                .withOverlay(false)
                .withStyleId(R.style.ToolTipLayoutMiniGameAction)
                .build())
        mMiniGameActionLeftTooltip!!.setTextSize(ScreenUtils.dip2px(10f).toFloat())
        mMiniGameActionLeftTooltip!!.setTextColor(context.resources.getColor(android.R.color.white))
        mMiniGameActionLeftTooltip!!.requestLayout()
        mMiniGameActionLeftTooltip!!.show(mRoot)
    }

    private fun updateAndShowRightActionToolTip(message: String?) {

        message ?: return

        val location = IntArray(2)
        val viewWidth = mRightUserHead.width
        val viewHeight = mRightUserHead.height
        val delta = ScreenUtils.dip2px(10f);
        mRightUserHead.getLocationOnScreen(location)

        val point = Point(location[0] + viewWidth / 2, location[1] + viewHeight * 2 / 3 - delta)

        mMiniGameActionRightTooltip?.remove()
        mMiniGameActionRightTooltip = Tooltip.make(
                context, Tooltip.Builder(100)
                .anchor(point, Tooltip.Gravity.BOTTOM)
                .text(message)
                .maxWidth(ScreenUtils.dip2px(200f))
                .withArrow(true)
                .withOverlay(false)
                .withStyleId(R.style.ToolTipLayoutMiniGameAction)
                .build())
        mMiniGameActionRightTooltip!!.setTextSize(ScreenUtils.dip2px(10f).toFloat())
        mMiniGameActionRightTooltip!!.setTextColor(context.resources.getColor(android.R.color.white))
        mMiniGameActionRightTooltip!!.requestLayout()
        mMiniGameActionRightTooltip!!.show(mRoot)
    }

    private fun updateAndShowToolTipView(view: View, message: String) {
        val location = IntArray(2)
        val viewWidth = view.width
        val delta = ScreenUtils.dip2px(10f);
        view.getLocationOnScreen(location)

        val point = Point(location[0] + viewWidth / 2, location[1] - delta)
//        dismissChangeGame()
        mMiniGameTooltip?.remove()
        mMiniGameTooltip = Tooltip.make(
                context, Tooltip.Builder(100)
                .anchor(point, Tooltip.Gravity.TOP)
                .text(message)
                .maxWidth(500)
                .withArrow(true)
                .withOverlay(false)
                .withStyleId(R.style.ToolTipLayoutMiniGameFinish)
                .build())
        mMiniGameTooltip!!.setTextSize(ScreenUtils.dip2px(10f).toFloat())
        mMiniGameTooltip!!.setTextColor(context.resources.getColor(R.color.mini_game_finish_pop_textcolor))
        mMiniGameTooltip!!.requestLayout()
        mMiniGameTooltip!!.show(mRoot)
    }

    private fun initView() {
        /**
         * 初始化输赢图标
         */
        val myUserId = GlobalUserState.getGlobalState().userId;
        mUserId = myUserId
        val oppoId = mMiniGameContext!!.opponent.id;
        mOppoId = oppoId

        if (mMiniGameOverData!!.isTie) {
            mResultImageView.setImageResource(R.mipmap.ico_mini_game_even)
        } else {
            if (mMiniGameOverData!!.isWin(myUserId)) {
                mResultImageView.setImageResource(R.mipmap.ico_mini_game_win)
                /**
                 * 初始化结果提示信息
                 */
                val overReason = mMiniGameOverData!!.overReason();
                if (TextUtils.isEmpty(overReason)) {
                    mResultReason.visibility = View.GONE
                } else {
                    mResultReason.visibility = View.VISIBLE
                    mResultReason.setText(overReason)
                }
            } else {
                mResultImageView.setImageResource(R.mipmap.ico_mini_game_lose)
            }
        }

        /**
         * 初始化左右两个头像及名称信息 左边的是自己 右边的是对手
         */
        val leftUserHead = GlobalUserState.getGlobalState().userIcon;
        val rightUserHead = mMiniGameContext!!.opponent.avatar;
        val leftUserName = GlobalUserState.getGlobalState().userName;
        val rightUserName = mMiniGameContext!!.opponent.name

        PicassoUtils.loadImage(context, leftUserHead, mLeftUserHead, R.mipmap.default_head)
        PicassoUtils.loadImage(context, rightUserHead, mRightUserHead, R.mipmap.default_head)

        mLeftName.setText(leftUserName)
        mRightName.setText(rightUserName)

        /**
         * 初始化今日对局总数
         */
        try {
            val count = mMiniGameOverData!!.history_data.totalCount;
            mTodayRecord.setText(MessageUtils.getString(R.string.mini_game_v2_today_number, count.toString()))
        } catch (e: Exception) {
            e.printStackTrace()
            mTodayRecord.visibility = View.GONE
        }
        mTodayRecord.visibility = View.GONE

        /**
         * 初始化比分数
         */
        try {
            val leftScore = mMiniGameOverData!!.getScoreFromHistory(myUserId)
            val rightScore = mMiniGameOverData!!.getScoreFromHistory(oppoId)
            mTodayScore.setText(MessageUtils.getString(R.string.mini_game_v2_game_score, leftScore.toString(), rightScore.toString()))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val againAble: Boolean = mMiniGameContext!!.isAgainAble;
        if (againAble) {
            mPlayAgainView.visibility = View.VISIBLE
            mPlayAgainView.isEnabled = true
        } else {
            mPlayAgainView.visibility = View.VISIBLE
            mPlayAgainView.isEnabled = false
        }

        if (isMatch()) {
            mMatchAgainView.visibility = View.VISIBLE
        } else {
            mMatchAgainView.visibility = View.GONE
        }

        /**
         * Title 信息
         */
        //val leftUser=mMiniGameOverData?.users?.g
        fun getUser(userId: String): EnterRoomResult.EnterRoomUserItem? {
            var result: EnterRoomResult.EnterRoomUserItem? = null
            mMiniGameOverData?.users?.forEach {
                if (TextUtils.equals(it.value?.id, userId)) {
                    result = it.value
                }
            }
            return result
        }

        try {
            val leftUser = getUser(myUserId)
            val rightUser = getUser(oppoId)

            if (leftUser == null) {
                return
            }
            if (rightUser == null) {
                return
            }

            if (!TextUtils.isEmpty(leftUser.mini_game_title)) {
                PicassoUtils.loadImage(context, leftUser.mini_game_title, mLeftTag)
            }

            if (!TextUtils.isEmpty(rightUser.mini_game_title)) {
                PicassoUtils.loadImage(context, rightUser.mini_game_title, mRightTag)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /**
         * 用户分数信息
         */
        val scoreData = mMiniGameScoreData
        try {
            if (scoreData == null) {
                return
            } else {
                Log.i("MiniGameFinish", scoreData?.toString())
                /**
                 * 拿到自己的得分数据
                 */
                if (TextUtils.isEmpty(scoreData.msg)) {
                    if (scoreData.changeVal == 0) {
                        /**
                         * 分数为0
                         */
                        mScoreText.visibility = View.INVISIBLE
                    } else {
                        /**
                         * 展示分数
                         */
                        if (scoreData.changeVal > 0) {
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                mScoreText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_win_score, scoreData.changeVal.toString()), Html.FROM_HTML_MODE_LEGACY))
                            } else {
                                mScoreText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_win_score, scoreData.changeVal.toString())))
                            }
                        } else {
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                mScoreText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_lose_score, scoreData.changeVal.toString()), Html.FROM_HTML_MODE_LEGACY))
                            } else {
                                mScoreText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_lose_score, scoreData.changeVal.toString())))
                            }
                        }
                    }
                } else {
                    mScoreText.visibility = View.VISIBLE
                    mScoreText.setText(scoreData.msg)
                }

                /**
                 * 展示自己是否升级了
                 */
                val oldLevel = scoreData.oldLevelInfo.`val`
                val newLevel = scoreData.levelInfo.level.`val`

                if (newLevel > oldLevel) {
                    mLeveUpText.visibility = View.VISIBLE
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        mLeveUpText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_level_up), Html.FROM_HTML_MODE_LEGACY))
                    } else {
                        mLeveUpText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_level_up)))
                    }
                } else {
                    try {
                        if (TextUtils.isEmpty(scoreData.otherMsg)) {
                            mLeveUpText.visibility = View.GONE
                        } else {
                            mLeveUpText.visibility = View.VISIBLE
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                mLeveUpText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_other_msg, scoreData.otherMsg), Html.FROM_HTML_MODE_LEGACY))
                            } else {
                                mLeveUpText.setText(Html.fromHtml(MessageUtils.getString(R.string.mini_game_v2_other_msg, scoreData.otherMsg)))
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        mLeveUpText.visibility = View.GONE
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            /**
             * 报错则不显示
             */
            mScoreText.visibility = View.INVISIBLE
            mLeveUpText.visibility = View.GONE
        }

        /**
         * 判断是否是好友
         */
        ApiManager.GetUserInfoApi(oppoId, { result, e ->
            runSafely({
                if (e == null) {
                    try {
                        PLog.i("MiniGame", "${result.is_friend}")
                        if (result.is_friend) {
                            runSafely {
                                mAddFriendView.visibility = View.GONE
                            }
                        } else {
                            runSafely {
                                mAddFriendView.visibility = View.VISIBLE
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        })

    }

    private fun isFromChat(): Boolean {
        if (!TextUtils.isEmpty(mMiniGameContext!!.miniGameLaunch.password) && !isFromLobby()) {
            /**
             * Leancloud进入
             */
            return true
        }
        return false
    }

    private fun isFromMatch(): Boolean {
        return !isFromLobby() && !isFromChat()
    }

    private fun isFromLobby(): Boolean {
        /**
         * 判断是大厅
         */
        if (!TextUtils.isEmpty(mMiniGameContext!!.miniGameLaunch.fromRoomId)) {
            /**
             * 从大厅进入
             */
            return true
        }
        return false
    }

    private fun isMatch(): Boolean {
        var result = false
        try {
            /**
             * 判断是大厅
             */
            if (isFromLobby()) {
                return true
            }
            /**
             * 从聊天进入
             */
            if (isFromChat()) {
                return false
            }
            /**
             * 从匹配进入
             */
            if (isFromMatch()) {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    private fun sendMessageToServer(type: String, payload: String) {
        mMiniGameContext?.sendMessage(type, payload)
    }

    fun updateNotify(miniGameNotify: MiniGameNotify) {
        mAddFriendView.post {
            if (TextUtils.equals(miniGameNotify.serverType, MiniGameConstant.FIREND_REQUIRE_SERVER)) {

                if (TextUtils.equals(miniGameNotify.type, MiniGameConstant.REQUIRE_ADD)) {
                    /**
                     * 收到了对方请求加的好友消息
                     */
                    updateAndShowToolTipView(mAddFriendView, MessageUtils.getString(R.string.string_mini_game_finish_button_require_friend))
                    return@post
                }

                if (TextUtils.equals(miniGameNotify.type, MiniGameConstant.ADD_FIREND_SUCCESS)) {
                    /**
                     * 收到了加好友成功的消息
                     */
                    updateAndShowToolTipView(mAddFriendView, MessageUtils.getString(R.string.string_mini_game_finish_button_has_been_friend))
                    mAddFriendView.isEnabled = false
                    return@post
                }

                if (TextUtils.equals(miniGameNotify.type, MiniGameConstant.REQUIRE_SENDED)) {
                    if (miniGameNotify.is_friend) {
                        updateAndShowToolTipView(mAddFriendView, MessageUtils.getString(R.string.string_mini_game_finish_button_has_been_friend))
                        mAddFriendView.isEnabled = false
                    }
                    if (TextUtils.equals(miniGameNotify.from, GlobalUserState.getGlobalState().userId)) {
                        updateAndShowToolTipView(mAddFriendView, MessageUtils.getString(R.string.mini_game_replay_success))
                    }
                }
                return@post
            }

            if (TextUtils.equals(miniGameNotify.serverType, MiniGameConstant.NOTIFY_SERVER)) {
                /**
                 *  这里暂时 只处理 Go1 Go2 Go3 Go4 Go5
                 */
                if (TextUtils.equals(miniGameNotify.type, MiniGameConstant.NOTIFY_GO1)
                        || TextUtils.equals(miniGameNotify.type, MiniGameConstant.NOTIFY_GO2)
                        || TextUtils.equals(miniGameNotify.type, MiniGameConstant.NOTIFY_GO3)
                        || TextUtils.equals(miniGameNotify.type, MiniGameConstant.NOTIFY_GO4)
                        || TextUtils.equals(miniGameNotify.type, MiniGameConstant.NOTIFY_GO5)) {
                    if (TextUtils.equals(miniGameNotify.from, mUserId)) {
                        /**
                         * 发送者是自己
                         */
                        updateAndShowLeftActionToolTip(miniGameNotify.msg)
                    } else {
                        updateAndShowRightActionToolTip(miniGameNotify.msg)
                    }

                }
                return@post
            }
        }
    }

    fun updateState(miniGameReplay: MiniGameReplay) {
        mMiniGameReplay = miniGameReplay;
        runSafely({
            try {
                if (TextUtils.equals(miniGameReplay.user_id, GlobalUserState.getGlobalState().userId)) {
                    if (mMiniGameReplay!!.replay) {
                        mSelfReady = true;
                    }
                    return@runSafely;
                } else {
                    if (mMiniGameReplay!!.replay) {
                        mOpponentReady = true;
                    }
                }
                if (mMiniGameReplay != null) {
                    if (mMiniGameReplay!!.change_game) {
                        turnIntoChangeGame(COUNT_DOWN)
                        return@runSafely
                    }
                    if (mMiniGameReplay!!.change_oppo) {
                        turnIntoChangeOppo(COUNT_DOWN)
                        return@runSafely
                    }
                    if (mMiniGameReplay!!.replay) {
                        turnIntoReplay()
                        return@runSafely
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun updateTipTextCountDown(text: String) {
        mMiniGameTooltip?.setText(text)
    }

    private fun turnIntoChangeGame(startCountDown: Int) {
        mChangeGameView.post {
            mPlayAgainView.isEnabled = false;
            mCountDownTask?.cancelTask()
            mCountDownTask?.destroyTask()
            mCountDownTask = CountDownTaskManager.CountDownTask(startCountDown.toInt(), object : CountDownTaskManager.CountDownTaskListener {
                override fun onTick(seconds: Int) {
                    runSafely({
                        updateTipTextCountDown(MessageUtils.getString(R.string.mini_game_v2_opponent_change_game, seconds.toString()))
                    })
                }

                override fun onStart(startSeconds: Int) {

                }

                override fun onFinish() {
                    runSafely({
                        close()
                    })
                }
            })
            updateAndShowToolTipView(mChangeGameView, MessageUtils.getString(R.string.mini_game_v2_opponent_change_game, startCountDown.toString()))
            mCountDownTask!!.startTask()
        }
    }

    private fun turnIntoChangeOppo(startCountDown: Int) {
        mMatchAgainView.post {
            mPlayAgainView.isEnabled = false;
            mChangeGameView.isEnabled = false;
            mCountDownTask?.cancelTask()
            mCountDownTask?.destroyTask()
            mCountDownTask = CountDownTaskManager.CountDownTask(startCountDown.toInt(), object : CountDownTaskManager.CountDownTaskListener {
                override fun onTick(seconds: Int) {
                    runSafely({
                        updateTipTextCountDown(MessageUtils.getString(R.string.mini_game_v2_opponent_change_opponent, seconds.toString()))
                    })
                }

                override fun onStart(startSeconds: Int) {
                }

                override fun onFinish() {
                    runSafely({
                        close()
                    })
                }
            })
            updateAndShowToolTipView(mMatchAgainView, MessageUtils.getString(R.string.mini_game_v2_opponent_change_opponent, startCountDown.toString()))
            mCountDownTask!!.startTask()
        }
    }

    private fun turnIntoReplay() {
        mPlayAgainView.post {
            if (!mSelfReady) {
                updateAndShowToolTipView(mPlayAgainView, MessageUtils.getString(R.string.mini_game_v2_opponent_again))
            }
        }
    }

    override fun release() {
        mMiniGameContext = null
        mMiniGameTooltip = null
        mCountDownTask?.cancelTask()
        mCountDownTask?.destroyTask()
        RxToolKit.UnRegister(this)
        if (mChangeGameDialog != null && mChangeGameDialog!!.isShowing) {
            mChangeGameDialog!!.dismiss()
        }
//        if (mReMatchHintDialog != null && mReMatchHintDialog!!.isShowing) {
//            mReMatchHintDialog!!.dismiss()
//        }
    }

    private fun dismissChangeGame() {
        mChangeGameIntviuView ?: return
        mChangeGameIntviuView!!.dismiss()
    }

    fun turnIntoChangeGame(gameIcon: String?, gameName: String?, gameType: String?, fromUser: String?) {
        mChangeGameView?.post {

            if (TextUtils.equals(fromUser, GlobalUserState.getGlobalState().userId)) {
                /**
                 * 记录邀请的游戏类型
                 */
                mIntviuGameType = gameType ?: "";
                return@post
            }

            if (TextUtils.equals(gameType, mIntviuGameType)) {
                /**
                 * 对方也发送了一个 同样游戏类型的邀请
                 */
                return@post
            }

            mMiniGameTooltip?.remove()

            if (mChangeGameIntviuView == null) {
                mChangeGameIntviuView = MiniGameChangeGameView(context)
                mChangeGameIntviuView!!.fillData(gameIcon, gameName)
                mChangeGameIntviuView!!.setObserver(object : MiniGameChangeGameView.MiniGameChangeGameViewObserver {
                    override fun onAccept() {
                        /**
                         * 点击了同意
                         */
                        mMiniGameContext?.changeGameAtGameOver(gameType)
                        dismissChangeGame()
                    }
                })
                mChangeGameIntviuView!!.show(mChangeGameView, mRoot)
            } else {
                mChangeGameIntviuView!!.fillData(gameIcon, gameName)
                mChangeGameIntviuView!!.setObserver(object : MiniGameChangeGameView.MiniGameChangeGameViewObserver {
                    override fun onAccept() {
                        /**
                         * 点击了同意
                         */
                        mMiniGameContext?.changeGameAtGameOver(gameType)
                        dismissChangeGame()
                    }
                })
                if (mChangeGameIntviuView!!.isShowing()) {

                } else {
                    mChangeGameIntviuView!!.show(mChangeGameView, mRoot)
                }
            }
        }
    }

    private fun changeGameOld() {
        sendMessageToServer(MiniGameConstant.REPLAY, MiniGameSocketEngine.CreateChangeGame())
        if (isMatch()) {
            RxToolKit.Emit(MiniGameChangeGameEvent())
        } else {
            RxToolKit.Emit(MiniGameFinishEvent())
        }
    }

    /**
     * 换个游戏
     */
    private fun changeGameNew() {
        if (isFromChat()) {
            changeGameOld()
        } else {
            if (mChangeGameDialog == null) {

            } else {
                mChangeGameDialog?.dismiss()
            }
            mChangeGameDialog = MiniGameChangeGameDialog(context, object : MiniGameChangeGameDialog.MiniGameChangeGameObserver {

                override fun onSelectGame(miniGameItem: MiniGameItem) {
                    PLog.i("MiniGame", "select game->${miniGameItem}")
                    mMiniGameContext?.changeGameAtGameOver(miniGameItem.type)
                    /**
                     * 展示等待对方确认
                     */
                    mChangeGameView.post {
                        dismissChangeGame()
                        updateAndShowToolTipView(mChangeGameView, MessageUtils.getString(R.string.mini_game_v2_opponent_me))
                    }
                }
            })
            mChangeGameDialog!!.show()
        }
    }

    private fun changeOppoOld() {
        sendMessageToServer(MiniGameConstant.REPLAY, MiniGameSocketEngine.CreateChangeOppo())
        RxToolKit.Emit(MiniGameFinishEvent())
        sendLeaveMessage()
    }

    /**
     * 换个对手
     */
    private fun changeOppoRematch() {

        if (isFromLobby()) {
            changeOppoOld()
        } else {
            mMiniGameContext?.changeOppoAtGameOver()
        }

//        mReMatchHintDialog = WereWolfHintDialog(
//                context,
//                MessageUtils.getString(R.string.dialog_hint),
//                MessageUtils.getString(R.string.mini_game_rematch_hint),
//                { v1 ->
//
//                },
//                { v2 ->
//                    mReMatchHintDialog?.dismiss()
//                }
//        )
//        mReMatchHintDialog?.showDialog(0)


    }

    private fun sendLeaveMessage() {
        val opponentId = (mMiniGameContext?.opponent?.id) ?: ("")
        LeanCloudChatHelper.sendTempMessageIntoConversation(MessageUtils.getString(R.string.has_leave), opponentId)
    }

    private fun changeAgain() {
        if (!mOpponentReady) {
            updateAndShowToolTipView(mPlayAgainView, MessageUtils.getString(R.string.mini_game_v2_opponent_me))
        }
        sendMessageToServer(MiniGameConstant.REPLAY, MiniGameSocketEngine.CreateReplay())
    }

    private fun close() {
        if (isMatch()) {
            changeOppoOld()
        } else {
            changeGameOld()
        }
    }

    override fun dismiss() {
        mMiniGameTooltip?.remove()
        mMiniGameActionRightTooltip?.remove()
        mMiniGameActionLeftTooltip?.remove()
        mRoot.removeAllViews()
        super.dismiss()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.action_change_game
                /**换个游戏**/
            -> {
                changeGameNew()
            }
            R.id.action_change_oppo
                /**换个对手**/
            -> {
                changeOppoRematch()
            }
            R.id.action_again
                /**再玩一次**/
            -> {
                changeAgain()
            }
            R.id.id_dialog_close
                /**关闭**/
            -> {
                close()
            }
            R.id.action_add_friend
                /**加好友**/
            -> {
                sendMessageToServer(MiniGameConstant.FIREND_REQUIRE, MiniGameSocketEngine.CreateRequireFriend())
            }
            R.id.action_like -> {
                sendMessageToServer(MiniGameConstant.NOTIFY, MiniGameSocketEngine.CreateGo(MiniGameConstant.NOTIFY_GO1))
            }
            R.id.action_encourage -> {
                sendMessageToServer(MiniGameConstant.NOTIFY, MiniGameSocketEngine.CreateGo(MiniGameConstant.NOTIFY_GO5))
            }
            R.id.action_bye -> {
                sendMessageToServer(MiniGameConstant.NOTIFY, MiniGameSocketEngine.CreateGo(MiniGameConstant.NOTIFY_GO2))
            }
            R.id.action_praise -> {
                sendMessageToServer(MiniGameConstant.NOTIFY, MiniGameSocketEngine.CreateGo(MiniGameConstant.NOTIFY_GO4))
            }
        }
    }
}
