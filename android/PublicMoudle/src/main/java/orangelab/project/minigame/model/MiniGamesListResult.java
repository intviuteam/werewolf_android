package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2018/1/19 下午1:49
 * Mystery
 */

public class MiniGamesListResult implements Keepable{
    public ArrayList<MiniGameItem> games;
}
