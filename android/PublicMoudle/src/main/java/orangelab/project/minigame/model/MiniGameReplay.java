package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/7 下午5:06
 * Mystery
 */

public class MiniGameReplay implements Keepable {

    public boolean replay = false;
    public boolean change_game = false;
    public boolean change_oppo = false;
    public String user_id;

    public static MiniGameReplay CreateReplay() {
        MiniGameReplay replay = new MiniGameReplay();
        replay.replay = true;
        return replay;
    }

    public static MiniGameReplay CreateChangeOppo() {
        MiniGameReplay replay = new MiniGameReplay();
        replay.change_oppo = true;
        return replay;
    }

    public static MiniGameReplay CreateChangeGame() {
        MiniGameReplay replay = new MiniGameReplay();
        replay.change_game = true;
        return replay;
    }
}
