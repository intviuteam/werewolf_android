package orangelab.project.minigame.view;

import java.util.List;

import orangelab.project.minigame.model.MiniGameItem;

public interface MiniGameLoadOverCallBack {
    void onLoadOver(List<MiniGameItem> miniGameItems);
}
