package orangelab.project.minigame.manager

import android.text.TextUtils
import com.androidtoolkit.PLog
import com.androidtoolkit.ThreadToolKit
import com.androidtoolkit.UIActuator
import com.androidtoolkit.transport.URLDownLoadToolKit
import com.toolkit.action.Action
import com.toolkit.action.Filter
import orangelab.project.minigame.model.MiniGameItem
import java.util.ArrayList

/**
 *game_werewolf
 *2018/8/1 下午1:38
 *Mystery
 * 小游戏下载管理器
 */
object MiniGameDownloadManager {

    val TAG = "MiniGameDownloadManager"

    init {

    }

    private var mMiniGameRecord: MutableMap<String, MiniGameDownloadRecord> = HashMap()
    private var mObserver: MutableList<MiniGameDownloadObserver> = ArrayList()

    fun decorMiniGameList(list: List<MiniGameItem>?) {
        list ?: return
        for (item in list) {
            val progress = checkProgress(item.type)
            if (progress == -1) {
                item.isDownloading = false
                item.isFinish = true
            } else {
                item.isDownloading = true
                item.isFinish = false
            }
        }
    }

    fun addObserver(observer: MiniGameDownloadObserver?) {
        observer ?: return
        mObserver.add(observer)
    }

    fun removeObserver(observer: MiniGameDownloadObserver?) {
        observer ?: return
        mObserver.remove(observer)
    }

    fun beginDownload(gameType: String, gameDownloadUrl: String) {

        if (checkIsDownloading(gameType)) {
            /**
             * 说明当前正在有小游戏正在下载
             */
            printLog("this ${gameType} is downloading")
            return
        }

        if (checkDownloaded(gameType, gameDownloadUrl)) {
            /**
             * 如果该小游戏已经下载则不进行下载
             */
            printLog("this ${gameType} has been download")
            return
        }

        /**
         * 清空这个小游戏的所有本地文件
         */
        MiniGameManager.DeleteMiniGameFile(gameType)

        /**
         * 该小游戏未下载过
         *
         *
         * 1。重新创建一个Record
         * 2。创建并执行一个小游戏下载任务
         */
        val record = MiniGameDownloadRecord(gameType, gameDownloadUrl)  //1.
        mMiniGameRecord.put(gameType, record)
        record.downloadProgress = 0
        ThreadToolKit.Async(MiniGameDownloadTask(gameType, gameDownloadUrl)) //2.
    }

    fun checkDownloaded(gameType: String, gameDownloadUrl: String): Boolean {
        /**
         * 判断小游戏是否已经下载
         */
        return MiniGameManager.CheckZipDataIntegrity(gameType, gameDownloadUrl)
    }

    fun checkIsDownloading(gameType: String): Boolean {
        /**
         * 判断小游戏是否正在下载
         */
        return mMiniGameRecord.get(gameType) != null
    }

    fun checkProgress(gameType: String): Int {
        return mMiniGameRecord.get(gameType)?.downloadProgress ?: -1
    }

    interface MiniGameDownloadObserver {

        fun onDownloadStart(gameType: String)

        fun onDownloadProgress(gameType: String, progress: Int)

        fun onDownloadFinish(gameType: String)

        fun onDownloadError(gameType: String, e: Exception)

    }

    class MiniGameDownloadTask(var mType: String, var mDownloadUrl: String) : Runnable, Action<Float>, Filter {

        override fun func(value: Float?) {
            value ?: return

            if (value.toInt() % 10 == 0) {
                UIActuator.post {

                    mMiniGameRecord.get(mType)?.downloadProgress = value.toInt()

                    mObserver.forEach {
                        it.onDownloadProgress(mType, value.toInt())
                    }
                }
            }
        }

        override fun filter(): Boolean {
            return false
        }


        override fun run() {

            try {
                UIActuator.post {
                    /**
                     * 通知小游戏下载任务
                     */
                    mObserver.forEach {
                        it.onDownloadStart(mType)
                    }
                }

                val path = MiniGameManager.getMiniGameDownloadZipDir(mType)


                /**
                 * 执行下载
                 */
                val file = URLDownLoadToolKit.DownloadFile(mDownloadUrl, path, this, this)

                /**
                 * 下载完毕进行Zip包的清单生成
                 */
                if (file == null) {
                    reportError(NullPointerException("Download file is null"))
                    return
                }

                if (!MiniGameManager.generateZipRecordJSONFile(mType, mDownloadUrl, file)) {
                    reportError(IllegalArgumentException("resource check failed"))
                    return
                }

                UIActuator.post {
                    /**
                     * 通知小游戏下载结束
                     */
                    mObserver.forEach {
                        it.onDownloadFinish(mType)
                    }
                    mMiniGameRecord.remove(mType)

                }
            } catch (e: Exception) {
                e.printStackTrace()
                reportError(e)
            }
        }

        private fun reportError(e: Exception) {
            UIActuator.post {
                /**
                 * 通知小游戏下载出现异常
                 */
                mObserver.forEach {
                    it.onDownloadError(mType, e)
                }
                mMiniGameRecord.remove(mType)
            }
        }
    }

    /**
     * 小游戏下载器Record
     */
    class MiniGameDownloadRecord(var mType: String, var mDownloadUrl: String) {

        var downloadProgress = -1

        init {

        }

    }

    class SimpleDownloadObserver : MiniGameDownloadObserver {
        override fun onDownloadStart(gameType: String) {
            printLog("onDownloadStart")
        }

        override fun onDownloadProgress(gameType: String, progress: Int) {
            printLog("onDownloadProgress:${progress}")
        }

        override fun onDownloadFinish(gameType: String) {
            printLog("onDownloadFinish")
        }

        override fun onDownloadError(gameType: String, e: Exception) {
            printLog("onDownloadError")
        }

    }


    private fun printLog(info: String) {
        PLog.i(TAG, info)
    }

}