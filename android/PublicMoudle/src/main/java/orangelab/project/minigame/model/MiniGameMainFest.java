package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

import java.util.HashMap;

/**
 * game_werewolf
 * 2018/4/3 下午4:55
 * Mystery
 */

public class MiniGameMainFest implements Keepable {

    public HashMap<String, MiniGameMainFestItem> itemList;

    public static class MiniGameMainFestItem implements Keepable {
        public String fileName;
        public String fileMD5;
    }
}
