package orangelab.project.minigame;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;

import orangelab.project.MainApplication;
import orangelab.project.common.engine.MiniGameSocketEngine;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.model.ServerGet;
import orangelab.project.minigame.model.MiniGameChangeUserAtGameOver;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameNotify;
import orangelab.project.minigame.model.MiniGameReplay;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.SafeHandler;

import com.networktoolkit.transport.Constant;
import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/1/24 上午10:11
 * Mystery
 */

public class MiniGameContext implements Destroyable {
    protected static final int MINI_PROCESS_NONE = -2;
    protected static final int MINI_PROCESS_LOADING_RESOURCE = -1;
    protected static final int MINI_PROCESS_LOADING_RESOURCE_SUCCESS = 0;
    protected static final int MINI_PROCESS_LOADING = 1;
    protected static final int MINI_PROCESS_MATCHING = 2;
    protected static final int MINI_PROCESS_MATCHED = 3;
    protected static final int MINI_PROCESS_GAMING = 4;
    protected static final int MINI_PROCESS_GAME_OVER = 5;

    protected static final int MINI_PROCESS_GAME_NONE = 8;
    protected static final int MINI_PROCESS_GAME_SWITCHING = 6;
    protected static final int MINI_PROCESS_GAME_SWITCH_DONE = 7;

    private static final String TAG = "MiniGameContext";
    private String mRoomId;
    private Context mContext;
    private View mRootView;
    private List<EnterRoomResult.EnterRoomUserItem> mEnterUsers = new ArrayList<>(2);
    private MiniGameProcessManager mProcessManager;
    private MiniGameContextController mMiniGameContextController;
    private SafeHandler mSafeHandler = new SafeHandler();
    private MiniGameLaunch mMiniGameLaunch;
    private EnterRoomResult.RoomServerMessage mRoomServerMessage;

    private long mProcessMatchingStartTime = 0L;
    private long mProcessLoadingStartTime = 0L;
    private long mProcessResourceLoadingStartTime = 0L;

    private int mCurProcess = MINI_PROCESS_NONE;
    private int mSwitchProcess = MINI_PROCESS_GAME_NONE;
    private int mPosition = -1;
    private String mSwitchType = "";
    private boolean mAgainAble = true;

    private List<ServerMessageEvent> mServerMessageEventCache = new ArrayList<>();


    public MiniGameContext(Context context, View rootView, MiniGameLaunch miniGameLaunch, MiniGameContextController contextController) {
        mContext = context;
        mRootView = rootView;
        mProcessManager = new MiniGameProcessManager();
        mMiniGameContextController = contextController;
        mMiniGameLaunch = miniGameLaunch;
        initListener();
    }

    private void initListener() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(value -> post(() -> {
            try {
                if (!value.isMiniGame()) {
                    return;
                }

                if (isSwitchProcessGameSwitching()) {
                    /**
                     * 如果当前是切换游戏状态，则将所有消息缓存
                     */
                    mServerMessageEventCache.add(value);
                    return;
                }

                handleServerMessageEvent(value);

            } catch (Exception e) {
                e.printStackTrace();
            }
        })).register();

        RxToolKit.Build(this, ServerResponseEvent.class).actionFilter(value -> TextUtils.equals(value.getFromSocketType(), SocketEngine.MINI_GAME)).action(value -> post(() -> {
            try {
                mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        })).register();
    }

    private void handleServerMessageEvent(ServerMessageEvent value) {
        if (TextUtils.equals(value.type, MiniGameConstant.JOIN_SERVER)) {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            ServerMessageEnterResult item = GsonHelper.getGson().fromJson(value.payload.toString(), ServerMessageEnterResult.class);
            EnterRoomResult.EnterRoomUserItem userItem = item.user;
            mEnterUsers.add(userItem);
            if (checkGameMember()) {
                /**
                 * 已经可以进入匹配页面
                 */
                mCurProcess = MINI_PROCESS_MATCHED;
                mProcessManager.onMiniGameMatched();
            }
            return;

        } else if (TextUtils.equals(value.type, MiniGameConstant.LEAVE_SERVER)) {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            mProcessManager.onMiniGameUserLeaveServer();
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.USER_RETURN_SERVER)) {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            mProcessManager.onMiniGameUserReturnServer();
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.START_GAME_SERVER)) {
            mCurProcess = MINI_PROCESS_GAMING;
            mProcessManager.onMiniGameStartServer(value.type, value.payload.toString());
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.GAME_OVER_SERVER)) {
            mCurProcess = MINI_PROCESS_GAME_OVER;
            mProcessManager.onMiniGameOverServer(value.type, value.payload.toString());
            /**
             * 向RN传送数据
             */
            UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_MINI_GAME_OVER, value.payload);
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.REPLAY_SERVER)) {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            MiniGameReplay replay = GsonHelper.getGson().fromJson(value.payload.toString(), MiniGameReplay.class);
            mProcessManager.onMiniGameReplayServer(replay);
            return;
        } else if (TextUtils.equals(value.type, Constant.KICK_OUT)) {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            int position = RoomSocketEngineHelper.getPositionFromJSON(value.payload);
            if (position == mPosition) {
                mProcessManager.onMiniGameKickOutServer();
            }
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.FORCE_LEAVE_SERVER)) {
            /**
             * 受到此消息会强制退出
             */
            String msg = "";
            try {
                msg = value.payload.optString(Constant.MESSAGE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mMiniGameContextController != null) {
                mMiniGameContextController.forceDestroy(msg);
            }
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.NOTIFY_SERVER)) {
            MiniGameNotify notify = GsonHelper.getGson().fromJson(value.payload.toString(), MiniGameNotify.class);
            notify.serverType = value.type;
            mProcessManager.onMiniGameNotifyServer(notify);
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.FIREND_REQUIRE_SERVER)) {
            MiniGameNotify notify = GsonHelper.getGson().fromJson(value.payload.toString(), MiniGameNotify.class);
            notify.serverType = value.type;
            mProcessManager.onMiniGameNotifyServer(notify);
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
            return;
        } else if (TextUtils.equals(value.type, MiniGameConstant.CHANGE_AT_GAMEOVER_SERVER)) {
            /**
             * 这个消息不传给H5
             */
            handleChangeAtGameOverServer(value.payload.toString());
            return;
        } else if (TextUtils.equals(value.type, Constant.TYPE_ENTER_SERVER)) {
            if (isSwitchProcessGameSwitchDone()) {
                EnterRoomResult enterRoomResult = GsonHelper.getGson().fromJson(value.payload.toString(), EnterRoomResult.class);
                handleEnterServer(enterRoomResult);
                return;
            }
        }

        try {
            mProcessManager.onMiniGameServerMessage(value.type, value.payload.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isAgainAble() {
        return mAgainAble;
    }

    public void setAgainAble(boolean againAble) {
        mAgainAble = againAble;
    }

    public void sendMessage(String type, String json) {
        try {
            sendMessage(type, new JSONObject(json));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String type, JSONObject jsonObject) {
        post(() -> {
            MiniGameSocketEngine mEngineHelper = MainApplication.getInstance().getMiniGameSocketEngineHelper();
            if (mEngineHelper != null && mEngineHelper.engineExist()) {
                try {
                    mEngineHelper.nativeRequestMessage(type, jsonObject, null, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendMiniGameMessage(String type, String json) {
        post(() -> {
            mProcessManager.onMiniGameMessage(type, json);
        });
    }

    public String getRoomId() {
        return mRoomId;
    }

    public void initSocketEngine() {
        /**
         * 创建Engine 并进入
         */
        MiniGameSocketEngine mEngineHelper = MainApplication.getInstance().getMiniGameSocketEngineHelper();
        if (mEngineHelper != null) {
            mEngineHelper.initEngineAndEnter(mMiniGameLaunch, (enterRoomResult, e) -> {
                post(() -> {
                    if (e != null) {
                        e.printStackTrace();
                        if (mMiniGameContextController != null) {
                            mMiniGameContextController.forceDestroy(e.getMessage());
                        }
                        return;
                    } else {
                        handleEnterServer(enterRoomResult);
                    }
                });
            });
            mEngineHelper.startPollMessage();
        }
    }

    private void handleEnterServer(EnterRoomResult enterRoomResult) {
        /**
         * 初始化自己的位置
         */
        try {
            mPosition = enterRoomResult.position;
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        /**
         * 初始化mini配置
         */
        mRoomId = enterRoomResult.room.room_id;
        Log.i(TAG, "Room Id=" + mRoomId);
        mRoomServerMessage = enterRoomResult.media_server;

        mMiniGameLaunch.setRoomId(mRoomId);
        mMiniGameLaunch.setPassword(enterRoomResult.room.password);

        try {

            GlobalUserState.getGlobalState().setGaming(true);
            GlobalUserState.getGlobalState().setCurRoomId(enterRoomResult.room.room_id);
            GlobalUserState.getGlobalState().setCurGameType(mMiniGameLaunch.getType());

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        /**
         * 进入房间成功
         */
        mProcessManager.onMiniGameEnterServer(enterRoomResult);

        /**
         * 等待匹配结果
         */
        if (checkGameMember(enterRoomResult)) {
            /**
             * 进入了匹配页面
             */
            ToolKit.travelMap(enterRoomResult.room.users, (key, value) -> {
                addEnterUsers(value);
            });

            mCurProcess = MINI_PROCESS_MATCHED;
            mProcessManager.onMiniGameMatched();
        } else {
            /**
             * 还需要等待,这里先把自己取出来
             */
            try {
                int position = enterRoomResult.position;
                EnterRoomResult.EnterRoomUserItem userItem = enterRoomResult.room.users.get(position);
                addEnterUsers(userItem);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (mMiniGameLaunch != null) {
                if (TextUtils.isEmpty(mMiniGameLaunch.getRoomId())) {
                    mCurProcess = MINI_PROCESS_MATCHING;
                    mProcessMatchingStartTime = System.currentTimeMillis();
                } else {
                    mCurProcess = MINI_PROCESS_LOADING;
                    mProcessLoadingStartTime = System.currentTimeMillis();
                }
            }
        }
    }

    public boolean isSocketExist() {
        MiniGameSocketEngine mEngineHelper = MainApplication.getInstance().getMiniGameSocketEngineHelper();
        if (mEngineHelper != null && mEngineHelper.engineExist()) {
            return true;
        } else {
            return false;
        }
    }

    public Context getContext() {
        return mContext;
    }

    public View getRootView() {
        return mRootView;
    }

    public EnterRoomResult.EnterRoomUserItem getOpponent() {
        for (EnterRoomResult.EnterRoomUserItem item : mEnterUsers) {
            if (TextUtils.equals(item.id, GlobalUserState.getGlobalState().getUserId())) {
                continue;
            } else {
                return item;
            }
        }
        return null;
    }

    public void clearOpponent() {
        Iterator<EnterRoomResult.EnterRoomUserItem> iterator = mEnterUsers.iterator();
        while (iterator.hasNext()) {
            EnterRoomResult.EnterRoomUserItem item = iterator.next();
            if (TextUtils.equals(item.id, GlobalUserState.getGlobalState().getUserId())) {
                continue;
            } else {
                iterator.remove();
            }
        }
    }

    public void reportProcessLoadingResource() {
        mCurProcess = MINI_PROCESS_LOADING_RESOURCE;
        mProcessResourceLoadingStartTime = System.currentTimeMillis();
    }

    public void startGameReady(String json) {
        post(() -> {
            mCurProcess = MINI_PROCESS_GAMING;
            mProcessManager.onMiniGameStartServerReady(json);
        });
    }

    public void forceIntoGame() {
        post(() -> {
            mCurProcess = MINI_PROCESS_GAMING;
            mProcessManager.onMiniGameDomShow();
        });
    }

    public void forceDestroy(String reason) {
        post(() -> {
            mProcessManager.onMiniGameForceDestroy(reason);
        });
    }

    public void switchLaunch(MiniGameLaunch miniGameLaunch) {
        post(() -> {
            mProcessManager.onMiniGameSwitchLaunch(miniGameLaunch);
        });
    }

    public void gameOverReady(String payload) {
        post(() -> {
            mCurProcess = MINI_PROCESS_GAME_OVER;
            mProcessManager.onMiniGameOverServerReady(payload);
        });
    }

    public void loadUrlError(String reason) {
        post(() -> {
            mProcessManager.onLoadUrlError(reason);
        });
    }

    public void loadUrlFinish(String json) {
        post(() -> {
            mCurProcess = MINI_PROCESS_LOADING_RESOURCE_SUCCESS;
            mProcessManager.onLoadUrlFinish(json);
        });
    }

    public void addEnterUsers(EnterRoomResult.EnterRoomUserItem item) {
        mEnterUsers.add(item);
    }

    public void addAllEnterUsers(List<EnterRoomResult.EnterRoomUserItem> items) {
        mEnterUsers.addAll(items);
    }

    public void registerProcess(MiniGameProcess process) {
        mProcessManager.add(process);
    }

    public void unRegisterProcess(MiniGameProcess process) {
        mProcessManager.remove(process);
    }

    public MiniGameLaunch getMiniGameLaunch() {
        return mMiniGameLaunch;
    }

    public EnterRoomResult.RoomServerMessage getRoomServerMessage() {
        return mRoomServerMessage;
    }

    public boolean checkGameMember() {
        try {
            if (mEnterUsers.size() == 2) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkGameMember(EnterRoomResult enterRoomResult) {
        try {
            if (enterRoomResult.room.users.size() == 2) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isCurProcessResourceLoading() {
        return mCurProcess == MINI_PROCESS_LOADING_RESOURCE;
    }

    public boolean isCurProcessLoading() {
        return mCurProcess == MINI_PROCESS_LOADING;
    }

    public boolean isCurProcessMatching() {
        return mCurProcess == MINI_PROCESS_MATCHING;
    }

    public boolean isCurProcessMatched() {
        return mCurProcess == MINI_PROCESS_MATCHED;
    }

    public boolean isCurProcessGaming() {
        return mCurProcess == MINI_PROCESS_GAMING;
    }

    public boolean isCurProcessGameOver() {
        return mCurProcess == MINI_PROCESS_GAME_OVER;
    }

    public boolean isSwitchProcessGameSwitching() {
        return mSwitchProcess == MINI_PROCESS_GAME_SWITCHING;
    }

    public boolean isSwitchProcessGameSwitchDone() {
        return mSwitchProcess == MINI_PROCESS_GAME_SWITCH_DONE;
    }

    public void gameSwitchDone() {
        mSwitchProcess = MINI_PROCESS_GAME_SWITCH_DONE;

        /**
         * 开始将缓存的消息都吐出去
         */
        for (ServerMessageEvent event : mServerMessageEventCache) {
            handleServerMessageEvent(event);
        }
        mServerMessageEventCache.clear();
    }

    public long getProcessMatchingStartTime() {
        return mProcessMatchingStartTime;
    }

    public long getProcessLoadingStartTime() {
        return mProcessLoadingStartTime;
    }

    public void setProcessMatchingStartTime(long processMatchingStartTime) {
        mProcessMatchingStartTime = processMatchingStartTime;
    }

    public long getProcessResourceLoadingStartTime() {
        return mProcessResourceLoadingStartTime;
    }

    public void setProcessResourceLoadingStartTime(long processResourceLoadingStartTime) {
        mProcessResourceLoadingStartTime = processResourceLoadingStartTime;
    }

    public void post(Runnable runnable) {
        post(runnable, 0);
    }

    public void removePost(Runnable runnable) {
        mSafeHandler.removeCallbacks(runnable);
    }

    public void post(Runnable runnable, long delay) {
        mSafeHandler.postDelaySafely(runnable, delay);
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        GlobalUserState.getGlobalState().setGaming(false);
        GlobalUserState.getGlobalState().setCurGameType("");
        GlobalUserState.getGlobalState().setCurRoomId("");
        if (mProcessManager != null) {
            mProcessManager.destroy();
            mProcessManager = null;
        }
        if (mMiniGameLaunch != null) {
            mMiniGameLaunch = null;
        }
        try {
            MiniGameSocketEngine mEngineHelper = MainApplication.getInstance().getMiniGameSocketEngineHelper();
            if (mEngineHelper != null && mEngineHelper.engineExist()) {
                mEngineHelper.nativeRequestMessage(Constant.LEAVE, null, null, null);
                RoomSocketEngineHelper.userLeaveReportRN();
                mEngineHelper.releaseDelay();
                Log.i(TAG, "destroy: Engine release");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSafeHandler.release();
        mEnterUsers.clear();
    }

    /**
     * 游戏结束，换个对手
     */
    public void changeOppoAtGameOver() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.TYPE, MiniGameConstant.CHANGE_USER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(MiniGameConstant.CHANGE_AT_GAMEOVER, jsonObject);
    }

    public void changeGameAtGameOver(String gameType) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.TYPE, MiniGameConstant.CHANGE_GAME);
            jsonObject.put(MiniGameConstant.GAME_TYPE, gameType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(MiniGameConstant.CHANGE_AT_GAMEOVER, jsonObject);
    }

    public boolean isSwitchGame() {
        return TextUtils.equals(mSwitchType, MiniGameConstant.CHANGE_GAME);
    }

    public boolean isSwitchOpponent() {
        return TextUtils.equals(mSwitchType, MiniGameConstant.CHANGE_USER);
    }

    private void handleChangeAtGameOverServer(String payload) {
        try {

            /**
             * 更新的Launch数据
             */
            MiniGameChangeUserAtGameOver over = GsonHelper.getGson().fromJson(payload, MiniGameChangeUserAtGameOver.class);

            if (TextUtils.equals(over.type, MiniGameConstant.CHANGE_GAME_REQUIRED)) {
                /**
                 * 请求换一个游戏
                 */
                mProcessManager.onMiniGameChangeRequired(over.icon, over.name, over.game_type, over.from_user);
                return;
            }

            mSwitchType = over.change_type;

            if (isSwitchOpponent()) {
                /**
                 * 更换对手
                 */
                mSwitchProcess = MINI_PROCESS_GAME_SWITCHING; //开始切换

                /**
                 * 更换对手，代表着重新匹配
                 */
                MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder();
                ServerGet.ServerGetMiniGame getMiniGame = over.data.mini_game_cfg;
                builder.gameName(getMiniGame.name);
                builder.gameUrl(getMiniGame.url);
                builder.gameDownloadUrl(getMiniGame.download);
                builder.gameIcon(getMiniGame.icon);
                builder.landscape(getMiniGame.landscape);
                builder.tips(getMiniGame.tips);
                builder.user_images(mMiniGameLaunch.getUser_images());
                builder.type(mMiniGameLaunch.getType());
                builder.server(mMiniGameLaunch.getServer());

                builder.fromRoomType(mMiniGameLaunch.getFromRoomType());
                builder.fromPassword(mMiniGameLaunch.getFromPassword());
                builder.fromRoomId(mMiniGameLaunch.getFromRoomId());
                mMiniGameLaunch = builder.build();

                /**
                 * 先清房间内的用户的信息
                 */
                mEnterUsers.clear();

                /**
                 * 执行切换
                 */
                switchLaunch(mMiniGameLaunch);
            } else {
                /**
                 * 更换游戏
                 */
                mSwitchProcess = MINI_PROCESS_GAME_SWITCHING; //开始切换
                /**
                 * 更换游戏，重新配置Launch
                 */
                MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder();
                ServerGet.ServerGetMiniGame getMiniGame = over.data.mini_game_cfg;
                builder.gameName(getMiniGame.name);
                builder.gameUrl(getMiniGame.url);
                builder.gameDownloadUrl(getMiniGame.download);
                builder.gameIcon(getMiniGame.icon);
                builder.landscape(getMiniGame.landscape);
                builder.tips(getMiniGame.tips);
                builder.user_images(mMiniGameLaunch.getUser_images());
                builder.type(over.data.game_type);
                builder.server(mMiniGameLaunch.getServer());
                builder.oppoIcon(getOpponent().avatar);
                builder.fromRoomType(mMiniGameLaunch.getFromRoomType());
                builder.fromPassword(mMiniGameLaunch.getFromPassword());
                builder.fromRoomId(mMiniGameLaunch.getFromRoomId());
                mMiniGameLaunch = builder.build();

                /**
                 * 清理房间内的用户信息
                 */
                mEnterUsers.clear();

                /**
                 * 执行切换
                 */
                switchLaunch(mMiniGameLaunch);
            }
        } catch (Exception e) {

            e.printStackTrace();
            mSwitchProcess = MINI_PROCESS_GAME_SWITCH_DONE;   //切换结束

        }
    }


}
