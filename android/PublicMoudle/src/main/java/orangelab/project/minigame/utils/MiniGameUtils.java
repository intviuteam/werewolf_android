package orangelab.project.minigame.utils;

import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ThreadToolKit;
import com.datasource.GlobalUserState;
import com.toolkit.action.ApiResult;

import orangelab.project.MainApplication;
import orangelab.project.common.db.MiniGameAIRecordDaoHelper;
import orangelab.project.common.db.MiniGameRecordDaoHelper;
import orangelab.project.common.db.entity.MiniGameRecordEntity;
import orangelab.project.common.db.model.MiniGameAiRecord;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.minigame.MiniGameConstant;
import orangelab.project.minigame.MiniGamesApiManager;
import orangelab.project.minigame.event.MiniGameSendMsgToGlobalEvent;
import orangelab.project.minigame.model.MiniGameAcceptInvite;
import orangelab.project.minigame.model.MiniGameInvite;
import orangelab.project.minigame.model.MiniGameRecordReport;
import orangelab.project.minigame.model.MiniGameRefuseInvite;

import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/1/24 下午4:19
 * Mystery
 */

public class MiniGameUtils {

    public static void SendMiniGameInvite(String game_type, String userId, long wait_timer) {
        MiniGameInvite invite = new MiniGameInvite();
        invite.game_type = game_type;
        invite.target_user_id = userId;
        invite.wait_timer = wait_timer;
        RxToolKit.Emit(new MiniGameSendMsgToGlobalEvent(GsonHelper.getGson().toJson(invite)));
    }

    public static void SendMiniGameAcceptInvite(String game_type, String invite_user_id) {
        MiniGameAcceptInvite invite = new MiniGameAcceptInvite();
        invite.game_type = game_type;
        invite.invite_user_id = invite_user_id;
        RxToolKit.Emit(new MiniGameSendMsgToGlobalEvent(GsonHelper.getGson().toJson(invite)));
    }

    public static void SendMiniGameRefuseInvite(String game_type, String invite_user_id) {
        MiniGameRefuseInvite invite = new MiniGameRefuseInvite();
        invite.game_type = game_type;
        invite.invite_user_id = invite_user_id;
        RxToolKit.Emit(new MiniGameSendMsgToGlobalEvent(GsonHelper.getGson().toJson(invite)));
    }

    public static MiniGameAiRecord getMiniGameAiRecord(String gameType) {
        if (TextUtils.isEmpty(gameType)) return null;
        MiniGameAIRecordDaoHelper mHelper = MainApplication.getInstance().getMiniGameAIRecordDaoHelper();
        if (mHelper == null) {
            return null;
        } else {
            MiniGameAiRecord miniGameAiRecord = mHelper.getAIRecord(GlobalUserState.getGlobalState().getUserId(), gameType);
            if (miniGameAiRecord == null) {
                /**
                 * 本地无记录,生成一个新的礼物
                 */
                miniGameAiRecord = new MiniGameAiRecord();
                miniGameAiRecord.ai_level = MiniGameConstant.MINI_GAME_AI_DEFAULT_LEVEL;
                miniGameAiRecord.winning_streak = 0;
                mHelper.insertNewRecord(
                        GlobalUserState.getGlobalState().getUserId(),
                        GlobalUserState.getGlobalState().getToken(),
                        gameType,
                        miniGameAiRecord.ai_level,
                        miniGameAiRecord.winning_streak
                );
            }
            return miniGameAiRecord;
        }
    }

    public static void reportMiniGameRecord(String userId, String gameType, long scoreValue, long costTime, ApiResult<Integer> result) {
        ThreadToolKit.Async(() -> {

            MiniGameRecordDaoHelper helper = MainApplication.getInstance().getMiniGameRecordDaoHelper();

            if (helper != null) {
                /**
                 * 将本次的成绩添加到数据库中
                 */
                helper.addMiniGameRecord(GlobalUserState.getGlobalState().getToken(), userId, gameType, scoreValue, costTime);

                /**
                 * 取出所有的这个人的小游戏成绩
                 */
                List<MiniGameRecordEntity> entities = helper.getMiniGameRecord(userId, gameType);

                /**
                 * 汇报这个人的数据
                 */
                MiniGameRecordReport recordReport = MiniGameRecordReport.createReport(gameType, entities);
                MiniGamesApiManager.GetGameRank(GlobalUserState.getGlobalState().getToken(), recordReport, (integer, e) -> {

                    if (result != null) {
                        result.onResult(integer, e);
                    }

                    if (e == null) {
                        /**
                         * 汇报成功，删除汇报的所有数据
                         */
                        if (entities != null) {
                            for (MiniGameRecordEntity entity : entities) {
                                helper.removeRecord(entity);
                            }
                        }
                    }

                });
            } else {
                /**
                 * 数据库连接出问题了
                 */
            }
        });
    }
}
