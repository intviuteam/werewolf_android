package orangelab.project.minigame.model;

import android.text.TextUtils;

import com.R;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.utils.MessageUtils;

import com.toolkit.ToolKit;
import com.toolkit.action.Keepable;
import com.toolkit.action.TravelCollection;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

/**
 * game_werewolf
 * 2018/3/7 下午1:45
 * Mystery
 */

public class MiniGameOver implements Keepable {

    private static final String USER_GIVE_IP = "user give up";

    public HashMap<Integer, EnterRoomResult.EnterRoomUserItem> users;
    public MiniGameOverGameData game_data;
    public MiniGameOverHistoryData history_data;

    public boolean isWin(String userId) {
        boolean result = false;
        try {
            if (TextUtils.equals(userId, game_data.gameOver.winUserId)) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    public boolean isTie() {
        boolean result = false;
        try {
            if (TextUtils.isEmpty(game_data.gameOver.winUserId)) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getScoreFromHistory(String userId) {
        final int[] result = {0};
        ToolKit.travelCollection(history_data.winCounts, value -> {
            if (TextUtils.equals(value.user_id, userId)) {
                result[0] = value.count;
            }
        });
        return result[0];
    }

    public String overReason() {
        String reason = null;
        try {
            if (TextUtils.equals(game_data.gameOver.winReason, USER_GIVE_IP)) {
                reason = MessageUtils.getString(R.string.mini_game_v2_opponent_give_up);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reason;
    }

    public static class MiniGameOverGameData implements Keepable {
        public String gameState;
        public int game_id;
        public boolean is_replay;
        public MiniGameOverDetail gameOver;
    }

    public static class MiniGameOverDetail implements Keepable {
        public String winUserId;
        public String winReason;
    }

    public static class MiniGameOverHistoryData implements Keepable {
        public int totalCount;
        public ArrayList<MiniGameOverHistoryDataItem> winCounts;
    }

    public static class MiniGameOverHistoryDataItem implements Keepable {
        public String user_id;
        public int count;
    }

    public static class ScoreData implements Keepable {
        public LevelInfo levelInfo;
        public OldLevel oldLevelInfo;
        public int changeVal;
        public String msg = "";
        public String otherMsg = "";
    }

    public static class LevelInfo implements Keepable {
        public String levelInfo;
        public String gameType;
        public String score;
        public Level level;
    }

    public static class Level implements Keepable {
        public int val;
        public int star;
        public int score;
    }

    public static class OldLevel implements Keepable {
        public int val;
        public int star;
    }
}
