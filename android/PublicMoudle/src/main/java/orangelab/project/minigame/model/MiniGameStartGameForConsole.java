package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/5/22 下午2:14
 * Mystery
 */
public class MiniGameStartGameForConsole implements Keepable {

    public String user_id;
    public String user_name;
    public String user_icon;
    public int user_sex;
    public int ai_level;
    public int winning_streak;
}
