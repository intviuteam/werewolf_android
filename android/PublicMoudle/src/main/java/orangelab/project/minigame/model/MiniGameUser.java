package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/2/5 上午11:31
 * Mystery
 */

public class MiniGameUser implements Keepable {

    public String id;
    public String name;
    public String avatar;
    public int sex;
}
