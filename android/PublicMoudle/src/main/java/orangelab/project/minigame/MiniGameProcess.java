package orangelab.project.minigame;

import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameNotify;
import orangelab.project.minigame.model.MiniGameReplay;
import orangelab.project.common.model.EnterRoomResult;

/**
 * game_werewolf
 * 2018/1/24 上午10:29
 * Mystery
 */

public abstract class MiniGameProcess {


    public boolean onLoadUrlFinish(String json) {
        return true;
    }

    public boolean onLoadUrlError(String reason) {
        return true;
    }

    public boolean onLoadUrlProgress(String userId, int process) {
        return true;
    }

    public boolean onMiniGameLoading() {
        return true;
    }

    public boolean onMiniGameKickOutServer() {
        return true;
    }

    public boolean onMiniGameEnterServer(EnterRoomResult enterRoomResult) {
        return true;
    }

    public boolean onMiniGameMatched() {
        return true;
    }

    public boolean onMiniGameStartServer(String type, String json) {
        return true;
    }

    public boolean onMiniGameStartServerReady(String json) {
        return true;
    }

    public boolean onMiniGameDomShow() {
        return true;
    }

    public boolean onMiniGameServerMessage(String type, String json) {
        return true;
    }

    public boolean onMiniGameMessage(String type, String json) {
        return true;
    }

    public boolean onMiniGameOverServer(String type, String json) {
        return true;
    }

    public boolean onMiniGameOverServerReady(String json) {
        return true;
    }

    public boolean onMiniGameUserLeaveServer() {
        return true;
    }

    public boolean onMiniGameUserReturnServer() {
        return true;
    }

    public boolean onMiniGameNotifyServer(MiniGameNotify notify) {
        return true;
    }

    public boolean onMiniGameForceDestroy(String reason) {
        return true;
    }

    public boolean onMiniGameReplayServer(MiniGameReplay miniGameReplay) {
        return true;
    }

    public boolean onMiniGameSwitchLaunch(MiniGameLaunch miniGameLaunch) {
        return true;
    }

    public boolean onMiniGameChangeRequired(String gameIcon, String gameName, String gameType, String fromUser) {
        return true;
    }


}
