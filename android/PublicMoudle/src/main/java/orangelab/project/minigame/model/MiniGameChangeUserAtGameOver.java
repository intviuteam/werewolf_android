package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

import orangelab.project.common.model.ServerGet;

/**
 * game_werewolf
 * 2018/5/18 下午2:09
 * Mystery
 */
public class MiniGameChangeUserAtGameOver implements Keepable {

    public String type;
    public MiniGameChangeUserAtGameOverData data;
    public String change_type;
    public String from_user;
    public String game_type;
    public String name;
    public String icon;

    public static class MiniGameChangeUserAtGameOverData implements Keepable {

        public ServerGet.ServerGetMiniGame mini_game_cfg;
        public String game_type;

    }

}
