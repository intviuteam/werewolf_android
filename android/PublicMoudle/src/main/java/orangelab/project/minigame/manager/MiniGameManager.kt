package orangelab.project.minigame.manager

import android.text.TextUtils
import cn.intviu.support.GsonHelper

import com.androidtoolkit.Environment
import com.androidtoolkit.PLog
import com.androidtoolkit.StorageToolKit
import com.androidtoolkit.ThreadToolKit
import orangelab.project.minigame.model.MiniGameZipRecord
import com.toolkit.action.Action

import java.io.File
import com.R
import com.androidtoolkit.transport.URLDownLoadToolKit
import orangelab.project.minigame.model.MiniGameMainFest
import com.koushikdutta.async.http.server.AsyncHttpServer
import com.toolkit.*
import com.toolkit.action.Filter
import orangelab.project.common.utils.MessageUtils
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.HashMap


/**
 * game_werewolf
 * 2018/4/3 下午1:53
 * Mystery
 *
 *
 * 小游戏本地的统一管理器
 */

object MiniGameManager {

    interface MiniGameLoadObserver {

        fun onBusy()

        fun onLoadStart()

        fun onChecking()

        fun onCheckSuccess()

        fun onCheckFailed()

        fun onDownloadingStart()

        fun onDownloading(percent: Float)

        fun onDownloadingFinish()

        fun onGenerateZipRecord()

        fun onGenerateZipRecordDone()

        fun onUnZipping()

        fun onUnZipDone()

        fun onGenerateManifest()

        fun onGenerateManifestDone()

        fun onLoadFinish(storgePath: String)

        fun onError(reason: String)

        fun isCancel(): Boolean
    }

    val TAG = "MiniGameManager"
    val MINI_GAME_ROOT_DIR = "mini_game"
    val MINI_GAME_FILE_ZIP = ".zip"
    val MINI_GAME_FILE_JSON = ".json"
    val MINI_GAME_FILE_MANIFEST_JSON = "_manifest"
    val MINI_GAME_FILE_RECORD_JSON = "_record"

    private var mLocked: Boolean = false
    private var mLocalHttpServer: AsyncHttpServer? = null

    private fun checkMiniGameMainfest(gameType: String): Boolean {
        /**
         * 校验清单文件
         */
        val manifestAll = GsonHelper.getGson().fromJson(
                JSONFileToolKit.getDatafromFile(getMiniGameManifestPath(gameType), getMiniGameManiFestName(gameType)), MiniGameMainFest::class.java)

        val gameDataPath = getMiniGameDataMainDir(gameType)
        val file = File(gameDataPath)
        val mainfestName = getMiniGameManiFestNameFull(gameType)

        var checkResult = false
        if (file.exists()) {

            val list = LinkedList<File>()
            var files = file.listFiles()
            for (file2 in files) {
                if (file2.isDirectory) {
                    list.add(file2)
                } else {
                    val curPath = file2.absolutePath?.replace(gameDataPath, "")
                    if (TextUtils.equals(mainfestName, curPath)) {
                        /**
                         * 跳过清单文件
                         */
                        continue
                    }
                    if (manifestAll.itemList.containsKey(curPath)) {
                        /**
                         *  有这个path，先认为校验成功了，后续可以添加完整性的校验
                         */

                    } else {
                        printLog("${curPath} check failed")
                        checkResult = false
                        return checkResult
                    }
                }
            }
            var temp_file: File
            while (!list.isEmpty()) {
                temp_file = list.removeFirst()
                files = temp_file.listFiles()
                for (file2 in files) {
                    if (file2.isDirectory) {
                        list.add(file2)
                    } else {
                        val curPath = file2.absolutePath?.replace(gameDataPath, "")
                        if (TextUtils.equals(mainfestName, curPath)) {
                            /**
                             * 跳过清单文件
                             */
                            continue
                        }
                        if (manifestAll.itemList.containsKey(curPath)) {
                            /**
                             *  有这个path，先认为校验成功了，后续可以添加完整性的校验
                             */

                        } else {
                            printLog("${curPath} check failed")
                            checkResult = false
                            return checkResult
                        }
                    }
                }
            }
        } else {
            throw FileNotFoundException()
        }
        checkResult = true
        return checkResult
    }

    private fun generateMiniGameMainfest(gameType: String) {
        val manifest = MiniGameMainFest()
        val manifestItems = HashMap<String, MiniGameMainFest.MiniGameMainFestItem>()
        manifest.itemList = manifestItems

        val gameDataPath = getMiniGameDataMainDir(gameType)
        val file = File(gameDataPath)
        if (file.exists()) {

            val list = LinkedList<File>()
            var files = file.listFiles()
            for (file2 in files) {
                if (file2.isDirectory) {
                    list.add(file2)
                } else {
                    printLog("Find a File ${file2.absolutePath}")
                    val item = MiniGameMainFest.MiniGameMainFestItem()
                    item.fileName = file2.absolutePath?.replace(gameDataPath, "")
                    item.fileMD5 = MD5ToolKit.getFileMD5(file2)
                    manifestItems.put(item.fileName, item)
                }
            }
            var temp_file: File
            while (!list.isEmpty()) {
                temp_file = list.removeFirst()
                files = temp_file.listFiles()
                for (file2 in files) {
                    if (file2.isDirectory) {
                        list.add(file2)
                    } else {
                        printLog("Find a File ${file2.absolutePath}")
                        val item = MiniGameMainFest.MiniGameMainFestItem()
                        item.fileName = file2.absolutePath?.replace(gameDataPath, "")
                        item.fileMD5 = MD5ToolKit.getFileMD5(file2)
                        manifestItems.put(item.fileName, item)
                    }
                }
            }
            JSONFileToolKit.saveDataToFile(getMiniGameManifestPath(gameType), getMiniGameManiFestName(gameType), GsonHelper.getGson().toJson(manifest))
        } else {
            throw FileNotFoundException()
        }
    }

    private fun unZipMiniGameData(gameType: String) {
        val zipPath = getMiniGameZipAbsolutePath(gameType)
        val unZipPath = getMiniGameUnZipDir(gameType)
        printLog("zipPath=${zipPath}")
        printLog("unZipPath=${unZipPath}")
        ZipFileToolKit.unPackZip(zipPath, unZipPath)
    }

    /**
     * 下载Zip文件
     */
    private fun downLoadZipFile(gameType: String, gameUrl: String, savePathDir: String, observer: MiniGameLoadObserver?): File? {
        var file: File? = URLDownLoadToolKit.DownloadFile(gameUrl, savePathDir, Action { observer?.onDownloading(it) }, Filter { observer?.isCancel() ?: false })
        return file
    }

    fun generateZipRecordJSONFile(gameType: String, gameUrl: String, file: File): Boolean {
        /**
         * 下载完成 生成一个Zip的记录文件 json
         */
        try {
            val zipRecord = MiniGameZipRecord()
            zipRecord.fileLongth = file.length()
            zipRecord.game_type = gameType
            zipRecord.urlMd5 = MD5ToolKit.getStringMD5(gameUrl)
            zipRecord.zipMd5 = MD5ToolKit.getFileMD5(file) //记录MD5值
            JSONFileToolKit.saveDataToFile(getMiniGameDownloadMainDir(gameType), getMiniGameZipRecordFileName(gameType), GsonHelper.getGson().toJson(zipRecord))
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    fun LoadMiniGame(gameType: String, gameUrl: String, observer: MiniGameLoadObserver?) {
        if (!mLocked) {
            ThreadToolKit.AsyncInSinglePool {
                val begin = System.currentTimeMillis()
                LoadMiniGameInner(gameType, gameUrl, observer)
                val end = System.currentTimeMillis()
                printLog("Cost ${end - begin}")
                mLocked = false
            }
        } else {
            reportBusy(observer)
        }
    }

    fun ReleaseLocalServer() {
        mLocalHttpServer?.stop()
        mLocalHttpServer = null
    }

    private fun upLoadLocalFileToServer(gameType: String, baseUrl: String, server: AsyncHttpServer) {

        val gameDataPath = getMiniGameDataMainDir(gameType)
        val file = File(gameDataPath)
        if (file.exists()) {

            val list = LinkedList<File>()
            var files = file.listFiles()
            for (file2 in files) {
                if (file2.isDirectory) {
                    list.add(file2)
                } else {
                    val uploadpath = file2.absolutePath.replace(gameDataPath, baseUrl)
                    server.directory(uploadpath, file2)
                    printLog("uploadPath ${uploadpath}")
                }
            }
            var temp_file: File
            while (!list.isEmpty()) {
                temp_file = list.removeFirst()
                files = temp_file.listFiles()
                for (file2 in files) {
                    if (file2.isDirectory) {
                        list.add(file2)
                    } else {
                        val uploadpath = file2.absolutePath.replace(gameDataPath, baseUrl)
                        server.directory(uploadpath, file2)
                        printLog("uploadPath ${uploadpath}")
                    }
                }
            }
        } else {
            throw FileNotFoundException()
        }
    }

    private fun reportSuccess(gameType: String, observer: MiniGameLoadObserver?) {
        /**
         * 这里说明本地资源已经校验成功，启动本地服务器
         */
        ReleaseLocalServer()

        val baseUrl = "/${gameType}/"
        val point = SocketToolKit.getAvailablePointRandom(9000, 10000)
        try {
            mLocalHttpServer = AsyncHttpServer()
            upLoadLocalFileToServer(gameType, baseUrl, mLocalHttpServer!!)
            mLocalHttpServer!!.listen(point)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        observer?.onCheckSuccess()
        observer?.onLoadFinish("http://localhost:${point}${baseUrl}")
    }

    /**
     * 汇报本次加载任务失败
     */
    private fun reportError(gameType: String, reason: String, observer: MiniGameLoadObserver?) {
        /**
         * 释放Sever资源
         */
        ReleaseLocalServer()

        /**
         * 汇报给 observer 失败了
         */
        observer?.onError(reason)

        /**
         * 删除主目录文件
         */
        DeleteMiniGameFile(gameType)
    }

    private fun LoadMiniGameInner(gameType: String, gameUrl: String, observer: MiniGameLoadObserver?) {

        val path = getMiniGameDownloadZipDir(gameType)

        observer?.onLoadStart()

        /**
         * 判断是否需要更新
         */
        if (OverallUpdate(gameType, gameUrl)) {
            /**
             * 需要全部更新
             */
            printLog("need Refresh All")
            ForceRefreshMiniGameTypeData(path, gameType, gameUrl, observer)
        } else {
            printLog("don't need Refresh All")
            /**
             * 不需要全部更新 执行文件的校验
             */
            observer?.onChecking()
            var checkResult = false
            try {
                checkResult = checkMiniGameMainfest(gameType)
            } catch (e: Exception) {
                e.printStackTrace()
                /**
                 * 由于校验失败会进行Zip包的校验，所以这里的这个异常不进行处理
                 */
            }

            if (checkResult) {
                reportSuccess(gameType, observer)
                return
            } else {
                /**
                 * 清单文件校验失败了,开始校验Zip包的完整性
                 */
                var zipCheckResult = false

                try {
                    zipCheckResult = CheckZipDataIntegrity(gameType, gameUrl)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (zipCheckResult) {

                    /**
                     * zip包是完整的,删除之前解压的所有数据然后重新进行解压
                     */
                    try {
                        deleteFile(File(getMiniGameUnZipDir(gameType)))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    UnZipAndCheck(path, gameType, gameUrl, observer)
                } else {
                    /**
                     * zip包也不完整,强制重新刷新
                     */
                    ForceRefreshMiniGameTypeData(path, gameType, gameUrl, observer)
                }
            }
        }
    }

    /**
     * 强制刷新所有的小游戏的数据
     */
    private fun ForceRefreshMiniGameTypeData(path: String, gameType: String, gameUrl: String, observer: MiniGameLoadObserver?) {

        val file: File = File(path)

        if (file.exists()) {
            /**
             * 删除所有的数据文件
             */
            deleteFile(file)
        } else {
            /**
             * 不存在重新创建一个数据
             */
            if (file.isDirectory) {
                file.mkdirs()
            }
        }

        /**
         * 下载文件
         */
        var zipFile: File? = null
        observer?.onDownloadingStart()
        try {
            zipFile = downLoadZipFile(gameType, gameUrl, path, observer)
        } catch (e: Exception) {
            e.printStackTrace()
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            return
        }
        if (zipFile == null) {
            /**
             * 文件下载失败直接结束
             */
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            return
        }
        observer?.onDownloadingFinish()

        /**
         * 生成Zip record json 数据
         */
        observer?.onGenerateZipRecord()
        if (!generateZipRecordJSONFile(gameType, gameUrl, zipFile!!)) {
            /**
             * 创建Zip record 失败 直接结束
             */
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            return
        }
        observer?.onGenerateZipRecordDone()

        UnZipAndCheck(path, gameType, gameUrl, observer)
    }

    private fun UnZipAndCheck(path: String, gameType: String, gameUrl: String, observer: MiniGameLoadObserver?) {
        /**
         * 开始解压文件
         */
        observer?.onUnZipping()
        try {
            unZipMiniGameData(gameType)
        } catch (e: Exception) {
            e.printStackTrace()
            /**
             * 解压失败了需要通知上层，需要重新开始
             */
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            return
        }
        observer?.onUnZipDone()

        /**
         * 开始生成清单文件
         */
        observer?.onGenerateManifest()
        try {
            generateMiniGameMainfest(gameType)
        } catch (e: Exception) {
            e.printStackTrace()
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            return
        }
        observer?.onGenerateManifestDone()

        /**
         * 开始校验文件
         */
        observer?.onChecking()
        var checkResult = true
        try {
            checkResult = checkMiniGameMainfest(gameType)
        } catch (e: Exception) {
            e.printStackTrace()
            reportError(gameType, MessageUtils.getString(R.string.mini_game_resource_loading_error), observer)
            observer?.onCheckFailed()
            return
        }
        if (checkResult) {
            reportSuccess(gameType, observer)
        } else {
            observer?.onCheckFailed()
        }
    }

    private fun OverallUpdate(gameType: String, gameUrl: String): Boolean {
        try {
            val jsonFile = File(getMiniGameZipRecordFilePath(gameType) + getMiniGameZipRecordFileNameFull(gameType)) //json 清单文件
            if (jsonFile.exists()) {
                val gameZipRecord: MiniGameZipRecord? = GsonHelper.getGson().fromJson(
                        JSONFileToolKit.getDatafromFile(getMiniGameZipRecordFilePath(gameType), getMiniGameZipRecordFileName(gameType)), MiniGameZipRecord::class.java)
                if (gameZipRecord == null) {
                    return true
                }
                val newUrlMd5 = MD5ToolKit.getStringMD5(gameUrl)
                val oldUrlMd5 = gameZipRecord.urlMd5
                if (TextUtils.equals(newUrlMd5, oldUrlMd5)) {
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return true
        }
    }

    fun CheckZipDataIntegrity(gameType: String, gameUrl: String): Boolean {
        /**
         * 校验Zip文件的数据完整性
         */
        val zipFile = File(getMiniGameZipAbsolutePath(gameType)) //zip
        val jsonFile = File(getMiniGameZipRecordFilePath(gameType) + getMiniGameZipRecordFileNameFull(gameType)) //json 清单文件

        if (zipFile.exists() && jsonFile.exists()) {
            val gameZipRecord: MiniGameZipRecord? = GsonHelper.getGson().fromJson(
                    JSONFileToolKit.getDatafromFile(getMiniGameZipRecordFilePath(gameType), getMiniGameZipRecordFileName(gameType)), MiniGameZipRecord::class.java)
            if (gameZipRecord == null) {
                return false
            }
            /**
             * 开始校验长度与MD5值
             */
            val zipFileMd5 = MD5ToolKit.getFileMD5(zipFile)
            val oldMd5 = gameZipRecord.zipMd5

            val newUrlMd5 = MD5ToolKit.getStringMD5(gameUrl)
            val oldUrlMd5 = gameZipRecord.urlMd5

            if (TextUtils.equals(zipFileMd5, oldMd5) && TextUtils.equals(newUrlMd5, oldUrlMd5) && zipFile.length() == gameZipRecord.fileLongth) {
                return true
            } else {
                return false
            }
        } else {
            /**
             * 两个文件有任何一个不存在则认为失败
             */
            return false
        }
    }

    @Deprecated("For now")
    private fun getMiniGameLoadUrl(gameType: String): String {
        return getMiniGameDataMainDir(gameType)     // 由于使用本地服务器 ;//"file://" + getMiniGameDataMainDir(gameType) + "index.html"
    }

    fun getMiniGameManiFestName(gameType: String): String {
        return gameType + MINI_GAME_FILE_MANIFEST_JSON
    }

    fun getMiniGameManiFestNameFull(gameType: String): String {
        return getMiniGameManiFestName(gameType) + MINI_GAME_FILE_JSON
    }

    fun getMiniGameManifestPath(gameType: String): String {
        return getMiniGameDataMainDir(gameType)
    }

    fun getMiniGameDataMainDir(gameType: String): String {
        return getMiniGameUnZipDir(gameType)
    }

    fun getMiniGameZipRecordFileName(gameType: String): String {
        return gameType + MINI_GAME_FILE_RECORD_JSON
    }

    fun getMiniGameZipRecordFileNameFull(gameType: String): String {
        return gameType + MINI_GAME_FILE_RECORD_JSON + MINI_GAME_FILE_JSON
    }

    /**
     * zip record 的文件目录
     */
    fun getMiniGameZipRecordFilePath(gameType: String): String {
        return getMiniGameDownloadMainDir(gameType)
    }

    /**
     * 解压后的目录
     */
    fun getMiniGameUnZipDir(gameType: String): String {
        return getMiniGameDownloadMainDir(gameType) + "data" + File.separator + gameType + File.separator
    }

    /**
     * Zip包的绝对路径
     */
    fun getMiniGameZipAbsolutePath(gameType: String): String {
        return getMiniGameDownloadMainDir(gameType) + gameType + MINI_GAME_FILE_ZIP
    }

    /**
     * 不同小游戏主要的存储目录
     */
    fun getMiniGameDownloadMainDir(gameType: String): String {
        return getMiniGameRootDir() + File.separator + gameType + File.separator
    }

    /**
     * 不同小游戏本地存储Zip目录
     */
    fun getMiniGameDownloadZipDir(gameType: String): String {
        return getMiniGameRootDir() + File.separator + gameType
    }

    /**
     * 小游戏存储的根目录
     */
    fun getMiniGameRootDir(): String {
        return StorageToolKit.GetStoragePath(Environment.GetAppContext(), MINI_GAME_ROOT_DIR)
    }

    private fun reportBusy(observer: MiniGameLoadObserver?) {
        observer?.onBusy()
        mLocked = false
    }

    private fun printLog(info: String) {
        PLog.i(TAG, info)
    }

    private fun deleteFile(file: File) {
        if (file.isDirectory) {
            val files = file.listFiles()
            for (i in files.indices) {
                val f = files[i]
                deleteFile(f)
            }
            file.delete()
        } else if (file.exists()) {
            file.delete()
        }
    }

    fun DeleteMiniGameFile(gameType: String) {
        try {
            val path = getMiniGameDownloadZipDir(gameType)
            val file = File(path)
            deleteFile(file)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

