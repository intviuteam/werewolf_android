package orangelab.project.minigame;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.transport.TransportHelper;
import com.datasource.GlobalUserState;
import com.R;
import com.tbruyelle.rxpermissions.RxPermissions;

import orangelab.project.MainApplication;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.activity.RoomSafeActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.db.MiniGameAIRecordDaoHelper;
import orangelab.project.common.db.model.MiniGameAiRecord;
import orangelab.project.common.engine.OrbitEngineManager;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.event.SocketEvent;

import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.dialog.WereWolfHintDialog;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.minigame.dialog.MiniGameConsoleFinishDialog;
import orangelab.project.minigame.dialog.MiniGameFinishDialog;
import orangelab.project.minigame.event.MiniGameChangeGameEvent;
import orangelab.project.minigame.event.MiniGameFinishEvent;
import orangelab.project.minigame.event.MiniGameLaunchFinishEvent;
import orangelab.project.minigame.manager.EmptyMiniGameLoadObserver;
import orangelab.project.minigame.manager.MiniGameManager;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameNotify;
import orangelab.project.minigame.model.MiniGameOver;
import orangelab.project.minigame.model.MiniGameOverForConsole;
import orangelab.project.minigame.model.MiniGameReplay;
import orangelab.project.minigame.model.MiniGameStartGameForConsole;
import orangelab.project.minigame.utils.MiniGameUtils;
import orangelab.project.minigame.view.MiniGamePrepareView;
import orangelab.project.minigame.view.MiniGameView;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.utils.MessageUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;
import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2018/1/18 上午11:35
 * Mystery
 */

public class MiniGameActivity extends RoomSafeActivity implements MiniGameContextController {

    private static final String TAG = "MiniGameActivity";
    private MiniGameContext mMiniGameContext;

    private MiniGamePrepareView mGamePrepareView;
    private MiniGameView mGameView;
    private MiniGameLaunch mMiniGameLaunch;

    private MiniGameFinishDialog mMiniGameFinishDialog;
    private MiniGameConsoleFinishDialog mMiniGameConsoleFinishDialog;

    private View mBackView;
    private View mExitView;


    private int mRetryCount = 0;
    private final int RETRY_COUNT_MAX = 3;

    private WereWolfHintDialog mExitDialog;

    private boolean urlHasLoadFinish = false;
    private boolean hasTurnIntoGame = false;

    private boolean mOppoLeave = false;
    private boolean mLoadCancel = false;
    private String mGameOverJson = "";
    private RxPermissions mRxPermissions;

    private long startTimeRecord = 0;
    private long endTimeRecord = 0;

    public static void Launch(Context context, MiniGameLaunch launch) {
        Intent intent = new Intent(context, MiniGameActivity.class);
        TransportHelper.putTransportable(TAG, launch);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    private void turnIntoLoadingResource() {
        if (mMiniGameContext != null) {
            mMiniGameContext.reportProcessLoadingResource();
        }
        mGamePrepareView.turnIntoLoadingResource();
    }

    private void startLoadingCount() {
        mGamePrepareView.startCounting();
    }

    private void turnIntoLoading() {
        mGamePrepareView.turnIntoLoading();
    }

    private void turnIntoMatching() {
        mBackView.setVisibility(View.VISIBLE);
        mGamePrepareView.startRandomAnim();
        mGamePrepareView.turnIntoMatching();
    }

    private void turnIntoGame() {
        if (!hasTurnIntoGame) {
            mGamePrepareView.releaseBackGroundAnim();
            mGamePrepareView.releaseRandomAnim();
            mGamePrepareView.release();
            mGamePrepareView.gone();
            if (mMiniGameLaunch.isLandscape()) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {

            }
            mGameView.show();
            hasTurnIntoGame = true;
        }
    }

    private void turnIntoMatched(String avatar) {
        mBackView.setVisibility(View.VISIBLE);
        mGamePrepareView.turnIntoMatched(avatar);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void initWindow() {
        try {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View decorView = getWindow().getDecorView();
                int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                decorView.setSystemUiVisibility(option);
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MiniGameLaunch temp = TransportHelper.getTransport(TAG);

        mRxPermissions = new RxPermissions(this);

        if (temp != null) {
            mMiniGameLaunch = temp;
        }

        if (temp == null) {
            /**
             * 数据为空直接结束,先强制Destroy 然后 强制 Finish
             */
            destroy();
            return;
        }

        View rootView = View.inflate(this, R.layout.layout_amusement_activity, null);
        mBackView = rootView.findViewById(R.id.id_mini_game_back);
        mExitView = rootView.findViewById(R.id.id_mini_game_exit);
        mExitView.setOnClickListener(v -> {
            mExitDialog = new WereWolfHintDialog(
                    this,
                    WereWolfString.getString(R.string.dialog_hint),
                    WereWolfString.getString(R.string.leave_notdead_not_in_game),
                    v1 -> {
                        if (mExitDialog != null) {
                            mExitDialog.dismiss();
                        }
                        destroy();
                    },
                    v2 -> {
                        if (mExitDialog != null) {
                            mExitDialog.dismiss();
                        }
                    }
            );
            mExitDialog.showDialog(0);
        });
        mBackView.setOnClickListener(v -> {
            destroy();
        });
        mMiniGameContext = new MiniGameContext(this, rootView, mMiniGameLaunch, this);

        mMiniGameContext.registerProcess(new MiniGameProcess() {

            @Override
            public boolean onMiniGameSwitchLaunch(MiniGameLaunch miniGameLaunch) {
                Log.i(TAG, "onMiniGameSwitchLaunch: " + mMiniGameLaunch);
                /**
                 * 切换Launch，用于小游戏切换对手，重新随机匹配
                 */
                if (miniGameLaunch == null) {
                    return false;
                }

                mOppoLeave = false;

                if (mMiniGameContext.isSwitchOpponent()) {
                    /**
                     * 换个对手需要关闭Orbit
                     */
                    OrbitEngineManager.INSTANCE.shutDown(() -> {
                        /**
                         * 关闭Orbit结束
                         */
                    });
                }

                mGameView.loadBlank();

                /**
                 * 切换成竖屏
                 */
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                mMiniGameLaunch = miniGameLaunch;

                if (mMiniGameFinishDialog != null && mMiniGameFinishDialog.isShowing()) {
                    mMiniGameFinishDialog.dismiss();
                }

                /**
                 * 标示重制
                 */
                urlHasLoadFinish = false;
                hasTurnIntoGame = false;

                /**
                 * 这里要进行重新的加载资源操作
                 */
                initDataAndView();

                /**
                 * 这里需要判断是否已知得到对手与游戏
                 */
                if (mMiniGameContext.getOpponent() != null) {
                    turnIntoMatched(mMiniGameContext.getOpponent().avatar);
                }
                return true;
            }

            @Override
            public boolean onMiniGameEnterServer(EnterRoomResult enterRoomResult) {
                Log.i(TAG, "onMiniGameEnterServer: ");
                mGameView.postJSDirectlyCommend(MiniGameConstant.ENTER_INIT, GsonHelper.getGson().toJson(enterRoomResult));
                return true;
            }

            @Override
            public boolean onMiniGameMatched() {
                Log.i(TAG, "onMiniGameMatched: ");
                /**
                 * 匹配到人员
                 */
                EnterRoomResult.EnterRoomUserItem item = mMiniGameContext.getOpponent();

                mGamePrepareView.goneLoadingText();

                if (item != null) {
                    turnIntoMatched(item.avatar);
                } else {
                    turnIntoMatched("");
                }
                return true;
            }

            @Override
            public boolean onMiniGameUserReturnServer() {
                mOppoLeave = false;
                return true;
            }

            @Override
            public boolean onMiniGameKickOutServer() {
                forceDestroy(MessageUtils.getString(R.string.you_have_been_kick_out));
                return true;
            }

            @Override
            public boolean onMiniGameUserLeaveServer() {
                Log.i(TAG, "onMiniGameUserLeaveServer: ");
                mOppoLeave = true;
                if (mMiniGameContext.isCurProcessGaming() || mMiniGameContext.isCurProcessGameOver()) {
                    /**
                     * 游戏中包含游戏结束，不进行强制关闭操作
                     */
                    return true;
                }
                forceDestroy(getString(R.string.mini_game_opponent_leave));
                return true;
            }

            @Override
            public boolean onMiniGameStartServerReady(String json) {
                Log.i(TAG, "onMiniGameStartServerReady: ");
                /**
                 * 游戏开始了 这里要进行计时
                 */
                String gameMode = "";
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    gameMode = jsonObject.optString("game_mode");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return true;
                }
                if (TextUtils.equals(gameMode, MiniGameLaunch.CONSOLE_GAME)) {
                    /**
                     * 单机版本
                     */

                    /**
                     * 记录游戏开始时间
                     */
                    startTimeRecord = System.nanoTime();

                    if (mMiniGameConsoleFinishDialog != null && mMiniGameConsoleFinishDialog.isShowing()) {
                        mMiniGameConsoleFinishDialog.dismiss();
                    }

                } else {
                    /**
                     * 联机版本
                     */
                    if (mMiniGameFinishDialog != null && mMiniGameFinishDialog.isShowing()) {
                        mMiniGameFinishDialog.dismiss();
                    }
                }
                turnIntoGame();
                return true;
            }

            @Override
            public boolean onMiniGameDomShow() {
                Log.i(TAG, "onMiniGameDomShow: ");
                turnIntoGame();
                return true;
            }

            @Override
            public boolean onMiniGameOverServer(String type, String json) {
                Log.i(TAG, "onMiniGameOverServer: ");
                /**
                 * 这里 依然继续给 JS 传递数据 但是 客户端需要自己去处理胜利失败问题
                 */
                mGameView.postJSDirectlyCommend(type, json);
                mGameOverJson = json;
                return true;
            }

            @Override
            public boolean onMiniGameOverServerReady(String json) {
                Log.i(TAG, "onMiniGameOverServerReady: ");
                String gameMode = "";
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    gameMode = jsonObject.optString("game_mode");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return true;
                }
                if (TextUtils.equals(gameMode, MiniGameLaunch.CONSOLE_GAME)) {
                    /**
                     * 单机模式
                     */

                    /**
                     * 记录游戏结束时间
                     */
                    try {
                        endTimeRecord = System.nanoTime();

                        MiniGameOverForConsole console = GsonHelper.getGson().fromJson(json, MiniGameOverForConsole.class);

                        if (console == null) {
                            return false;
                        }

                        long costTime = (endTimeRecord - startTimeRecord) / 1000000;

                        Log.i(TAG, "onMiniGameOverServerReady: Time Cost " + costTime);

                        if (mMiniGameConsoleFinishDialog != null && mMiniGameConsoleFinishDialog.isShowing()) {
                            mMiniGameConsoleFinishDialog.dismiss();
                        }

                        mMiniGameConsoleFinishDialog = new MiniGameConsoleFinishDialog(mMiniGameContext, console, costTime);
                        mMiniGameConsoleFinishDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    /**
                     * 联机模式
                     */
                    try {
                        JSONObject payload = new JSONObject(mGameOverJson);
                        MiniGameOver miniGameOver = GsonHelper.getGson().fromJson(mGameOverJson, MiniGameOver.class);
                        MiniGameOver.ScoreData myselfData = null;
                        try {
                            myselfData = GsonHelper.getGson().fromJson(
                                    payload.optJSONObject("score_data").optJSONObject(GlobalUserState.getGlobalState().getUserId()).toString(), MiniGameOver.ScoreData.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                            myselfData = null;
                        }
                        mMiniGameFinishDialog = new MiniGameFinishDialog(mMiniGameContext, miniGameOver, myselfData, mOppoLeave);
                        mMiniGameFinishDialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }

            @Override
            public boolean onLoadUrlFinish(String json) {
                Log.i(TAG, "onLoadUrlFinish: ");
                if (!urlHasLoadFinish) {
                    urlHasLoadFinish = true;

                    String game_mode = null;
                    try {
                        /**
                         * 获取游戏模式
                         */
                        JSONObject jsonObject = new JSONObject(json);
                        game_mode = jsonObject.optString("game_mode");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /**
                     * 加载url数据完成了
                     * 这里需要加一个判断，如果是单机模式则不需要连接SocketEngine，如果是联网模式则
                     * 是需要连接SocketEngine
                     */
                    if (TextUtils.equals(game_mode, MiniGameLaunch.CONSOLE_GAME)) {
                        /**
                         * 单机模式
                         *
                         * Step 1。通过数据库查询此玩家记录的AI等级与连胜记录，如果本地有记录则取本地，否则初始化一条新的记录
                         * Step 2。通知JS start——game——server 消息
                         */
                        /**
                         * Step 1.
                         */
                        MiniGameAiRecord miniGameAiRecord = MiniGameUtils.getMiniGameAiRecord(mMiniGameLaunch.getType());

                        if (miniGameAiRecord == null) {
                            forceDestroy(MessageUtils.getString(R.string.mini_game_resource_loading_error));
                        } else {
                            /**
                             * Step 2.
                             */
                            MiniGameStartGameForConsole forConsole = new MiniGameStartGameForConsole();
                            forConsole.ai_level = miniGameAiRecord.ai_level;
                            forConsole.winning_streak = miniGameAiRecord.winning_streak;
                            forConsole.user_icon = GlobalUserState.getGlobalState().getUserIcon();
                            forConsole.user_id = GlobalUserState.getGlobalState().getUserId();
                            forConsole.user_name = Utils.filterStringForJS(GlobalUserState.getGlobalState().getUserName());
                            forConsole.user_sex = GlobalUserState.getGlobalState().getUserExp();
                            mGameView.postJSDirectlyCommend(MiniGameConstant.START_GAME_SERVER, GsonHelper.getGson().toJson(forConsole));
                        }
                    } else {
                        /**
                         * 联机模式
                         */
                        if (mMiniGameContext.isSwitchProcessGameSwitching()) {
                            /**
                             * 当前是切换游戏状态
                             */
                            if (mMiniGameContext.isSwitchOpponent()) {
                                /**
                                 * 切换对手
                                 */
                                turnIntoMatching();
                            } else {
                                /**
                                 * 换一个游戏，此时是知道对手是谁
                                 */
                                turnIntoLoading();
                                turnIntoMatched(mMiniGameLaunch.getOppoIcon());
                            }

                            /**
                             * 通知切换游戏结束
                             */
                            mMiniGameContext.gameSwitchDone();
                        } else {
                            /**
                             * 当前不是切换游戏状态
                             */
                            initSocketEngine();
                        }
                    }
                    mRetryCount = 0;
                } else {
                    Log.i(TAG, "onLoadUrlFinish: url has been finished");
                }
                return true;
            }

            @Override
            public boolean onLoadUrlError(String reason) {
                Log.i(TAG, "onLoadUrlError: ");
                /**
                 * 如果资源加载过程已经成功通过则不在响应urlError事件
                 */
                if (mMiniGameContext.isCurProcessMatching() ||
                        mMiniGameContext.isCurProcessLoading() ||
                        mMiniGameContext.isCurProcessMatched() ||
                        mMiniGameContext.isCurProcessGaming() ||
                        mMiniGameContext.isCurProcessGameOver()) {
                    return true;
                }
                /**
                 * 加载数据出现了问题
                 */
                if (mRetryCount >= RETRY_COUNT_MAX) {
                    forceDestroy(MessageUtils.getString(R.string.mini_game_resource_loading_error));
                    mRetryCount = 0;
                } else {
                    mGameView.reload();
                    mRetryCount++;
                }
                return true;
            }

            @Override
            public boolean onMiniGameReplayServer(MiniGameReplay miniGameReplay) {
                if (mMiniGameFinishDialog != null) {
                    mMiniGameFinishDialog.updateState(miniGameReplay);
                }
                return true;
            }

            @Override
            public boolean onMiniGameNotifyServer(MiniGameNotify notify) {
                if (mMiniGameFinishDialog != null) {
                    mMiniGameFinishDialog.updateNotify(notify);
                }
                return true;
            }

            @Override
            public boolean onMiniGameForceDestroy(String reason) {
                forceDestroy(reason);
                return true;
            }

            @Override
            public boolean onMiniGameChangeRequired(String gameIcon, String gameName, String gameType, String fromUser) {
                if (mMiniGameFinishDialog != null && mMiniGameFinishDialog.isShowing()) {
                    mMiniGameFinishDialog.turnIntoChangeGame(gameIcon, gameName, gameType, fromUser);
                }
                return true;
            }
        });

        mGamePrepareView = new MiniGamePrepareView(mMiniGameContext, mMiniGameLaunch);

        mGameView = new MiniGameView(mMiniGameContext, mRxPermissions);

        initWindow();

        setContentView(rootView);

        initListener();

        initDataAndView();

        /**
         * 通知小游戏Launch结束
         */
        EventBus.getDefault().post(new MiniGameLaunchFinishEvent());

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void runOnUiThreadSafely(Runnable runnable) {
        runOnUiThreadSafely(runnable, 0);
    }

    @Override
    public void runOnUiThreadSafely(Runnable runnable, long delay) {
        if (mMiniGameContext != null) {
            mMiniGameContext.post(runnable, delay);
        }
    }

    private void initListener() {
        RxToolKit.Build(this, MiniGameFinishEvent.class).action(value -> {
            runOnUiThreadSafely(() -> {
                destroy();
            });
        }).register();
        RxToolKit.Build(this, SocketEvent.SocketConnectStateEvent.class).actionFilter(
                value -> TextUtils.equals(value.getIntent(), SocketEngine.MINI_GAME)
        ).action(value -> {
            runOnUiThreadSafely(() -> {
                switch (value.getAction()) {
                    case SocketEvent.SOCKET_CONNECT:
                        if (mGameView != null) {
                            mGameView.postJSDirectlyCommend(MiniGameConstant.SERVER_CONNECT, MiniGameConstant.EMPTY_JSON_STRING);
                        }
                        break;
                    case SocketEvent.SOCKET_DISCONNECT:
                        if (mGameView != null) {
                            mGameView.postJSDirectlyCommend(MiniGameConstant.SERVER_DISCONNECT, MiniGameConstant.EMPTY_JSON_STRING);
                        }
                        break;
                }
            });
        }).register();

        RxToolKit.Build(this, SocketEvent.ReConnectedFailedEvent.class)
                .actionFilter(value -> TextUtils.equals(value.getIntent(), SocketEngine.MINI_GAME))
                .action(value -> {
                    runOnUiThreadSafely(() -> {
                        ToastToolKit.showShort(MessageUtils.getString(R.string.you_have_been_disconnected));
                        destroy();
                    });
                }).register();
        RxToolKit.Build(this, MiniGameChangeGameEvent.class)
                .action(v -> {
                    runOnUiThreadSafely(() -> {
                        try {
                            UserData rightUser = UserData.createUserData(
                                    GlobalUserState.getGlobalState().getUserId(),
                                    GlobalUserState.getGlobalState().getUserName(),
                                    GlobalUserState.getGlobalState().getUserSex(),
                                    GlobalUserState.getGlobalState().getUserIcon()
                            );
                            EnterRoomResult.EnterRoomUserItem item = mMiniGameContext.getOpponent();
                            UserData leftUser = UserData.createUserData(
                                    item.id,
                                    item.name,
                                    item.sex,
                                    item.avatar
                            );
                            LCIMConversationActivity.Launch(this, "", leftUser.userId, leftUser, rightUser, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        destroy();
                    });
                }).register();
    }

    private void initDataAndView() {
        /**
         * 重制准备数据
         */
        mGamePrepareView.reSet();

        /**
         * 设为可见
         */
        mGamePrepareView.show();

        /**
         * 进入加载资源
         */
        turnIntoLoadingResource();

        /**
         * 开始计时
         */
        startLoadingCount();

        /**
         * 开始提示随机
         */
        mGamePrepareView.startTips();

        /**
         * 预加载WebView资源
         */

        if (mMiniGameLaunch.isConsoleGame()) {
            if (TextUtils.isEmpty(mMiniGameLaunch.getGameDownloadUrl())) {
                mGameView.load(mMiniGameLaunch.getGameConsoleUrl());
                PLog.i(TAG, "load console use url");
            } else {
                localWebViewResource();
                PLog.i(TAG, "load console use local url");
            }
        } else {
            if (TextUtils.isEmpty(mMiniGameLaunch.getGameDownloadUrl())) {
                mGameView.load(mMiniGameLaunch.getGameUrl());
                PLog.i(TAG, "load use url");
            } else {
                PLog.i(TAG, "load use local url");
                localWebViewResource();
            }
        }
    }

    private void localWebViewResource() {
        MiniGameManager.INSTANCE.LoadMiniGame(mMiniGameLaunch.getType(), mMiniGameLaunch.getGameDownloadUrl(), new EmptyMiniGameLoadObserver() {
            @Override
            public void onLoadFinish(String url) {
                runOnUiThreadSafely(() -> {

                    if (mMiniGameLaunch.isConsoleGame()) {
                        /**
                         * 单机模式加载
                         */
                        mGameView.load(url + "index_single.html");
                    } else {
                        /**
                         * 联机模式加载
                         */
                        mGameView.load(url + "index.html");
                    }
                });
            }

            @Override
            public void onCheckFailed() {
                super.onCheckFailed();
            }

            @Override
            public void onError(String reason) {
                runOnUiThreadSafely(() -> {
                    /**
                     * 加载失败，采用返回的url
                     */
                    if (mMiniGameLaunch.isConsoleGame()) {
                        mGameView.load(mMiniGameLaunch.getGameConsoleUrl());
                    } else {
                        mGameView.load(mMiniGameLaunch.getGameUrl());
                    }
                    PLog.i(TAG, "load use url");
                });
            }

            @Override
            public boolean isCancel() {
                return mLoadCancel;
            }
        });
    }


    private void initSocketEngine() {
        if (mMiniGameContext.isSocketExist()) {
            /**
             * socket存在直接返回
             */
            return;
        }
        if (TextUtils.isEmpty(mMiniGameLaunch.getRoomId())) {
            turnIntoMatching();
        } else {
            turnIntoLoading();
        }
        mMiniGameContext.initSocketEngine();
    }

    private void destroy() {
        mRxPermissions = null;
        mLoadCancel = true;

        if (mGamePrepareView != null) {
            mGamePrepareView.destroy();
            mGamePrepareView = null;
        }
        if (mGameView != null) {
            mGameView.destroy();
            mGameView = null;
        }
        if (mMiniGameContext != null) {
            mMiniGameContext.destroy();
            mMiniGameContext = null;
        }
        try {
            if (mMiniGameFinishDialog != null && mMiniGameFinishDialog.isShowing()) {
                mMiniGameFinishDialog.dismiss();
            }
            if (mMiniGameConsoleFinishDialog != null && mMiniGameConsoleFinishDialog.isShowing()) {
                mMiniGameConsoleFinishDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        OrbitEngineManager.INSTANCE.shutDown(() -> {
            runOnUiThread(() -> {
                finish();
                /**
                 * 如果来源为含有房间等信息则说明还要回到房间去
                 */
                if (mMiniGameLaunch == null) return;
                if (TextUtils.isEmpty(mMiniGameLaunch.getFromRoomId())
                        || TextUtils.isEmpty(mMiniGameLaunch.getFromRoomType())
                        ) {
                    return;
                }

                String roomId = mMiniGameLaunch.getFromRoomId();
                String password = mMiniGameLaunch.getFromPassword();
                String roomType = mMiniGameLaunch.getFromRoomType();

                LaunchActivity.LaunchForRoom(this, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, roomId, roomType);
            });
        });
        urlHasLoadFinish = false;
        MiniGameManager.INSTANCE.ReleaseLocalServer();
    }

    @Override
    public void finish() {
        super.finish();
        RxToolKit.UnRegister(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void forceDestroy(String reason) {
        if (!TextUtils.isEmpty(reason)) {
            showToast(reason);
        } else {
            /**
             * 如果没有reason则直接退出
             */
            //showToast(MessageUtils.getString(R.string.string_unkown_error));
        }
        runOnUiThreadSafely(() -> {
            destroy();
        });
    }
}
