package orangelab.project.minigame;

import com.datasource.GlobalUserState;

import orangelab.project.common.exception.ApiFailedException;
import orangelab.project.common.exception.NetWorkException;
import orangelab.project.minigame.model.MiniGameRecordReport;
import orangelab.project.minigame.model.MiniGamesListResult;

import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.toolkit.action.ApiResult;

import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/1/19 上午10:53
 * Mystery
 */

public class MiniGamesApiManager {

    public static void GetMiniGamesList(boolean inLobby, ApiResult<MiniGamesListResult> apiResult) {
        String url = "/game/list";
        if (inLobby) {
            url += "?in_lobby=1";
        }
        RequestTask.create().isGet(false).withToken(GlobalUserState.getGlobalState().getToken())
                .request(url).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                MiniGamesListResult result = GsonHelper.getGson().fromJson(response, MiniGamesListResult.class);
                if (result != null && apiResult != null) {
                    apiResult.onResult(result, null);
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (apiResult != null) {
                    apiResult.onResult(null, new Exception(failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (apiResult != null) {
                    apiResult.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

    public static void GetMiniGamesList(ApiResult<MiniGamesListResult> apiResult) {
        GetMiniGamesList(false, apiResult);
    }

    public static void GetGameRank(String token, MiniGameRecordReport miniGameRecordReport, ApiResult<Integer> apiResult) {
        try {
            RequestTask.create().isGet(false).withToken(token).request("/game/get_game_rank").load(new JSONObject(GsonHelper.getGson().toJson(miniGameRecordReport))).action(new RequestTaskCallBack() {
                @Override
                public void onSuccess(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int rank = jsonObject.optInt("rank");
                        if (apiResult != null) {
                            apiResult.onResult(rank, null);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }

                @Override
                public void onFailed(int code, String failed) {
                    if (apiResult != null) {
                        apiResult.onResult(null, new ApiFailedException(code, failed));
                    }
                }

                @Override
                public void onError(Exception e) {
                    if (apiResult != null) {
                        apiResult.onResult(null, new NetWorkException());
                    }
                }
            }).build().execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
