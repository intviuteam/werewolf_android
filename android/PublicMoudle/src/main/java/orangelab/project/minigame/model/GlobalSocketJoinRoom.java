package orangelab.project.minigame.model;

import orangelab.project.common.model.ServerGet;
import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:05
 * Mystery
 */

public class GlobalSocketJoinRoom implements Keepable {

    public String type = "join_room";
    public ServerGet data;

}
