package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午1:58
 * Mystery
 */

public class MiniGameInviteSuccess implements Keepable {

    public String type = "invited_success";
    public String uuid;
    public String target_user_id;
    public String game_type;
}
