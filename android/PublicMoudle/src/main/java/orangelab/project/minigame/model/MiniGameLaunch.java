package orangelab.project.minigame.model;


import android.text.TextUtils;

import com.androidtoolkit.transport.Transportable;
import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2018/1/23 下午5:02
 * Mystery
 */

public class MiniGameLaunch implements Keepable, Transportable {

    public static final String ONLINE_GAME = "online";
    public static final String CONSOLE_GAME = "console";


    private String type;
    private String server;
    private String roomId;
    private String gameName;
    private String gameUrl;
    private String gameDownloadUrl;
    private String gameIcon;
    private String extraData;
    private String password;
    private ArrayList<String> user_images = new ArrayList<>(0);
    private ArrayList<MiniGameTip> tips = new ArrayList<>(0);
    private boolean landscape;
    private String oppoIcon = "";
    private String launchMode = ONLINE_GAME;
    private String gameConsoleUrl;


    private String fromRoomId = "";
    private String fromRoomType = "";
    private String fromPassword = "";

    private MiniGameLaunch(Builder builder) {
        setType(builder.type);
        setServer(builder.server);
        setRoomId(builder.roomId);
        setGameName(builder.gameName);
        setGameUrl(builder.gameUrl);
        setGameDownloadUrl(builder.gameDownloadUrl);
        setGameIcon(builder.gameIcon);
        setExtraData(builder.extraData);
        setPassword(builder.password);
        setUser_images(builder.user_images);
        setTips(builder.tips);
        setLandscape(builder.landscape);
        setOppoIcon(builder.oppoIcon);
        setLaunchMode(builder.launchMode);
        setGameConsoleUrl(builder.gameConsoleUrl);
        setFromRoomId(builder.fromRoomId);
        setFromRoomType(builder.fromRoomType);
        fromPassword = builder.fromPassword;
    }

    public String getFromPassword() {
        return fromPassword;
    }

    public void setFromPassword(String fromPassword) {
        this.fromPassword = fromPassword;
    }

    public String getFromRoomId() {
        return fromRoomId;
    }

    public void setFromRoomId(String fromRoomId) {
        this.fromRoomId = fromRoomId;
    }

    public String getFromRoomType() {
        return fromRoomType;
    }

    public void setFromRoomType(String fromRoomType) {
        this.fromRoomType = fromRoomType;
    }

    public String getGameConsoleUrl() {
        return gameConsoleUrl;
    }

    public void setGameConsoleUrl(String gameConsoleUrl) {
        this.gameConsoleUrl = gameConsoleUrl;
    }

    public String getLaunchMode() {
        return launchMode;
    }

    public void setLaunchMode(String launchMode) {
        this.launchMode = launchMode;
    }

    public String getGameDownloadUrl() {
        return gameDownloadUrl;
    }

    public void setGameDownloadUrl(String gameDownloadUrl) {
        this.gameDownloadUrl = gameDownloadUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }

    public String getGameIcon() {
        return gameIcon;
    }

    public void setGameIcon(String gameIcon) {
        this.gameIcon = gameIcon;
    }

    public String getExtraData() {
        return extraData;
    }

    public void setExtraData(String extraData) {
        this.extraData = extraData;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<String> getUser_images() {
        return user_images;
    }

    public void setUser_images(ArrayList<String> user_images) {
        this.user_images = user_images;
    }

    public ArrayList<MiniGameTip> getTips() {
        return tips;
    }

    public void setTips(ArrayList<MiniGameTip> tips) {
        this.tips = tips;
    }

    public boolean isLandscape() {
        return landscape;
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }

    public String getOppoIcon() {
        return oppoIcon;
    }

    public void setOppoIcon(String oppoIcon) {
        this.oppoIcon = oppoIcon;
    }

    public boolean isConsoleGame() {
        return TextUtils.equals(this.launchMode, CONSOLE_GAME);
    }

    public boolean isOnlineGame() {
        return TextUtils.equals(this.launchMode, ONLINE_GAME);
    }

    public static final class Builder {
        private String type;
        private String server;
        private String roomId;
        private String gameName;
        private String gameUrl;
        private String gameDownloadUrl;
        private String gameIcon;
        private String extraData;
        private String password;
        private ArrayList<String> user_images;
        private ArrayList<MiniGameTip> tips;
        private boolean landscape;
        private String oppoIcon;
        private String launchMode;
        private String gameConsoleUrl;
        private String fromRoomId;
        private String fromRoomType;
        private String fromPassword;

        public Builder() {
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder server(String val) {
            server = val;
            return this;
        }

        public Builder roomId(String val) {
            roomId = val;
            return this;
        }

        public Builder gameName(String val) {
            gameName = val;
            return this;
        }

        public Builder gameUrl(String val) {
            gameUrl = val;
            return this;
        }

        public Builder gameDownloadUrl(String val) {
            gameDownloadUrl = val;
            return this;
        }

        public Builder gameIcon(String val) {
            gameIcon = val;
            return this;
        }

        public Builder extraData(String val) {
            extraData = val;
            return this;
        }

        public Builder password(String val) {
            password = val;
            return this;
        }

        public Builder user_images(ArrayList<String> val) {
            user_images = val;
            return this;
        }

        public Builder tips(ArrayList<MiniGameTip> val) {
            tips = val;
            return this;
        }

        public Builder landscape(boolean val) {
            landscape = val;
            return this;
        }

        public Builder oppoIcon(String val) {
            oppoIcon = val;
            return this;
        }

        public Builder launchMode(String val) {
            launchMode = val;
            return this;
        }

        public Builder gameConsoleUrl(String val) {
            gameConsoleUrl = val;
            return this;
        }

        public Builder fromRoomId(String val) {
            fromRoomId = val;
            return this;
        }

        public Builder fromRoomType(String val) {
            fromRoomType = val;
            return this;
        }

        public Builder fromPassword(String val) {
            fromPassword = val;
            return this;
        }

        public MiniGameLaunch build() {
            return new MiniGameLaunch(this);
        }
    }
}
