package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:03
 * Mystery
 */

public class MiniGameAcceptInvite implements Keepable {

    public String type = "accept_invite";
    public String game_type;
    public String invite_user_id;
}
