package orangelab.project.minigame.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.LogHelper;
import orangelab.project.common.utils.SafeHandler;

import com.squareup.picasso.Callback;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2018/2/11 下午4:29
 * Mystery
 */

public class MiniGameRandomAnimView implements Destroyable {

    private static final String TAG = "MiniGameRandomAnimView";

    private FrameLayout mRoot;

    private List<String> mRandomUsers;
    private List<View> mRandomImageView;
    private List<Animator> mAnimators;

    private RectF mRandomArea;
    private RectF mRandomBlockArea;
    private PointF mRandomAreaPoint;

    private int mRandomUserHeadWidth = 0;
    private Random mRandom;

    private long alphaOut = 500l;
    private long alphaIn = 500;
    private long randomTaskDelay = 1000l;

    private SafeHandler mActionHandler = new SafeHandler();

    public MiniGameRandomAnimView(FrameLayout root, List<String> randomUsers) {
        mRoot = root;
        mRandomUsers = randomUsers;
        mRandom = new Random();
        mRandomImageView = new ArrayList<>();
        mAnimators = new ArrayList<>();
    }

    public void reSet() {
        mActionHandler = new SafeHandler();
    }

    public void init(View blockView) {

        int statusBarHeight = ScreenUtils.dip2px(25);

        mRandomUserHeadWidth = ScreenUtils.dip2px(42);

        int[] location1 = new int[2];
        mRoot.getLocationInWindow(location1);

        mRandomAreaPoint = new PointF(location1[0], location1[1]);

        int[] location2 = new int[2];
        blockView.getLocationInWindow(location2);

        int defaultWidth = ScreenUtils.dip2px(75);
        int x = (int) (location2[0] - defaultWidth - mRandomAreaPoint.x);
        int y = (int) (location2[1] - mRandomAreaPoint.y);
        int blockWidth = blockView.getWidth() + 2 * defaultWidth;
        int blockHeight = blockView.getHeight();

        mRandomBlockArea = new RectF(x, y, x + blockWidth, y + blockHeight);


        int randomAreaWidth = mRoot.getWidth();
        int randomAreaHeight = mRoot.getHeight();
        int randomAreaX = 0;
        int randomAreaY = 0;
        mRandomArea = new RectF(randomAreaX, randomAreaY, randomAreaX + randomAreaWidth, randomAreaY + randomAreaHeight);
    }

    public void startRandomAnim() {
        startRandomAnim(false);
    }

    private void startRandomAnim(boolean imm) {
        if (mActionHandler != null) {
            if (imm) {
                mActionHandler.postSafely(() -> {
                    createRandomUIItem();
                });
            } else {
                mActionHandler.postDelaySafely(() -> {
                    createRandomUIItem();
                }, randomTaskDelay);
            }
        }
    }

    private void createRandomUIItem() {

        String userIcon = getRandomUserIcon();

        if (TextUtils.isEmpty(userIcon)) {
            startRandomAnim(true);
            return;
        }

        PointF pointF = getRandomPoint();
        PointF judgePointF = new PointF(pointF.x + mRandomUserHeadWidth, pointF.y + mRandomUserHeadWidth);
        if (!(pointIsAvailable(judgePointF) && pointIsAvailable(pointF))) {
            startRandomAnim(true);
            return;
        }

        ImageView imageView = createRandomView(pointF);
        Animator animator = createRandomAnimator(imageView);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                LogHelper.miniGameLog("Random Anim onAnimationCancel");
                super.onAnimationCancel(animation);
                removeViewFromRandomArea(imageView);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                LogHelper.miniGameLog("Random Anim onAnimationEnd");
                super.onAnimationEnd(animation);
                removeViewFromRandomArea(imageView);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                LogHelper.miniGameLog("Random Anim onAnimationStart");
                super.onAnimationStart(animation);
            }
        });

        PicassoUtils.loadImage(mRoot.getContext(), userIcon, imageView, new Callback() {
            @Override
            public void onSuccess() {
                if (mActionHandler != null) {
                    mActionHandler.postSafely(() -> {
                        addViewFromRandomArea(imageView, animator);
                        startRandomAnim(false);
                    });
                }
            }

            @Override
            public void onError(Exception e) {
                if (mActionHandler != null) {
                    mActionHandler.postSafely(() -> {
                        startRandomAnim(true);
                    });
                }
            }
        });
    }


    private String getRandomUserIcon() {
        if (mRandomUsers.size() == 0) {
            return "";
        }
        if (mRandomUsers.size() == 1) {
            return mRandomUsers.get(0);
        }
        int x = mRandom.nextInt(mRandomUsers.size() - 1);
        if (x > mRandomUsers.size()) {
            return mRandomUsers.get(mRandomUsers.size() - 1);
        }
        return mRandomUsers.get(x);
    }

    private void addViewFromRandomArea(View view, Animator animator) {
        mRandomImageView.add(view);
        mAnimators.add(animator);
        mRoot.addView(view);
        animator.start();
    }

    private void removeViewFromRandomArea(View view) {
        try {
            mRoot.removeView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean pointIsAvailable(PointF pointF) {
        if (pointF.x >= mRandomArea.left && pointF.x <= mRandomArea.right
                && pointF.y >= mRandomArea.top && pointF.y <= mRandomArea.bottom) {
            if (pointF.x >= mRandomBlockArea.left && pointF.x <= mRandomBlockArea.right
                    && pointF.y >= mRandomBlockArea.top && pointF.y <= mRandomBlockArea.bottom) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private ImageView createRandomView(PointF randomPoint) {
        CircleImageView targetView = new CircleImageView(mRoot.getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                mRandomUserHeadWidth,
                mRandomUserHeadWidth
        );
        targetView.setLayoutParams(layoutParams);
        targetView.setX(randomPoint.x);
        targetView.setY(randomPoint.y);
        return targetView;
    }

    private Animator createRandomAnimator(View view) {
        AnimatorSet animatorSet = new AnimatorSet();
        Animator animatorOut = MiniGamePrepareView.Helper.createAlphaOut(view, 1f, alphaOut);
        Animator animatorIn = MiniGamePrepareView.Helper.createAlphaIn(view, 1f, alphaIn);
        animatorIn.setStartDelay(alphaIn);
        animatorSet.play(animatorOut).before(animatorIn);
        return animatorSet;
    }

    private PointF getRandomPoint() {
        int x = (int) (mRandom.nextInt((int) (mRandomArea.right)));
        int y = (int) (mRandom.nextInt((int) (mRandomArea.bottom)));
        return new PointF(x, y);
    }

    @Override
    public void destroy() {
        if (mActionHandler != null) {
            mActionHandler.release();
            mActionHandler = null;
        }
        for (Animator animator : mAnimators) {
            animator.cancel();
        }
        mAnimators.clear();
        mRandomImageView.clear();
        mRoot.removeAllViews();
        Log.i(TAG, "destroy: random Anim release");
    }
}
