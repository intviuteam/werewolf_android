package orangelab.project.minigame.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;
import com.androidtoolkit.ToastToolKit;
import com.toolkit.action.Destroyable;

import org.jetbrains.annotations.NotNull;

import orangelab.project.game.view.MiniGameRoundProgressBar;
import orangelab.project.game.view.RoundProgressBar;
import orangelab.project.minigame.manager.MiniGameDownloadManager;
import orangelab.project.minigame.model.MiniGameItem;

import java.util.List;

/**
 * game_werewolf
 * 2018/1/19 下午2:30
 * Mystery
 */

public class MiniGamesListAdapter extends BaseAdapter implements Destroyable, MiniGameDownloadManager.MiniGameDownloadObserver {

    private List<MiniGameItem> mData;
    private int mCurIndex = 0;
    private int mMaxSize;
    private Context mContext;

    public MiniGamesListAdapter(Context context, List<MiniGameItem> data, int curIndex, int maxSize) {
        mData = data;
        mCurIndex = curIndex;
        mMaxSize = maxSize;
        mContext = context;
        MiniGameDownloadManager.INSTANCE.addObserver(this);
    }

    @Override
    public int getCount() {
        return mData.size() > (mCurIndex + 1) * mMaxSize ? mMaxSize : (mData.size() - getCurIndexStartIndex());
    }

    @Override
    public MiniGameItem getItem(int position) {
        return mData.get(position + getCurIndexStartIndex());
    }

    @Override
    public long getItemId(int position) {
        return position + getCurIndexStartIndex();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.layout_mini_game_item, null);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.id_minigame_iv);
            holder.mTextView = (TextView) convertView.findViewById(R.id.id_minigame_tv);
            holder.mProgressBar = convertView.findViewById(R.id.id_mini_game_progress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        MiniGameItem item = getItem(position);
        PicassoUtils.loadImage(mContext, item.icon, holder.mImageView);
        holder.mTextView.setText(item.name);

        if (!item.isFinish && item.isDownloading) {
            /**
             * 下载中
             */
            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mProgressBar.setProgress(item.progress);
        }
        if (item.isFinish && !item.isDownloading) {
            /**
             * 下载成功
             */
            holder.mProgressBar.setVisibility(View.GONE);
        }
        if (item.progress < 0 && item.isFinish && !item.isDownloading) {
            /**
             * 下载失败
             */
            holder.mProgressBar.setVisibility(View.GONE);
        }

        return convertView;
    }

    private int getCurIndexStartIndex() {
        return mCurIndex * mMaxSize;
    }

    @Override
    public void destroy() {
        MiniGameDownloadManager.INSTANCE.removeObserver(this);
    }

    private void updateProgress(String gameType, int progress) {
        int index = 0;

        MiniGameItem miniGameItem = null;
        for (MiniGameItem temp : mData) {
            if (TextUtils.equals(temp.type, gameType)) {
                miniGameItem = temp;
                break;
            }
            index++;
        }
        if (miniGameItem == null) return;

        if (progress > 100) {
            /**
             * 下载完成
             */
            miniGameItem.progress = 100;
            miniGameItem.isDownloading = false;
            miniGameItem.isFinish = true;
        } else if (progress >= 0 && progress <= 100) {
            /**
             * 下载中
             */
            miniGameItem.progress = progress;
            miniGameItem.isDownloading = true;
            miniGameItem.isFinish = false;
        } else {
            /**
             * 下载出错了
             */
            miniGameItem.progress = -1;
            miniGameItem.isDownloading = false;
            miniGameItem.isFinish = false;
        }

        int pageStart = mCurIndex * mMaxSize;
        int pageEnd = pageStart + mMaxSize;

        if (index >= pageStart && index < pageEnd) {
            notifyDataSetChanged();
        }
    }

    @Override
    public void onDownloadStart(@NotNull String gameType) {
        updateProgress(gameType, 0);
    }

    @Override
    public void onDownloadProgress(@NotNull String gameType, int progress) {
        updateProgress(gameType, progress);
    }

    @Override
    public void onDownloadFinish(@NotNull String gameType) {
        updateProgress(gameType, 101);
    }

    @Override
    public void onDownloadError(@NotNull String gameType, @NotNull Exception e) {
        updateProgress(gameType, -1);
        ToastToolKit.showShort(R.string.network_is_unavailable);
    }

    private static class ViewHolder {
        public ImageView mImageView;
        public TextView mTextView;
        public MiniGameRoundProgressBar mProgressBar;
    }
}
