package orangelab.project.minigame.view

import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.view.ScreenUtils
import orangelab.project.common.manager.AppManager
import orangelab.project.common.utils.MessageUtils

/**
 *game_werewolf
 *2018/5/18 下午5:21
 *Mystery
 */
class MiniGameChangeGameView : FrameLayout {

    interface MiniGameChangeGameViewObserver {

        fun onAccept()

    }

    private lateinit var mGameIcon: ImageView
    private lateinit var mGameText: TextView
    private lateinit var mAcceptButton: Button
    private var mObserver: MiniGameChangeGameViewObserver? = null

    private var mWidth = ScreenUtils.dip2px(200f)
    private var mHeight = ScreenUtils.dip2px(60f)

    private var mParent: ViewGroup? = null
    private var isShow = false

    constructor(context: Context?) : this(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    fun setObserver(observer: MiniGameChangeGameViewObserver) {
        mObserver = observer
    }

    fun isShowing(): Boolean {
        return isShow
    }

    private fun init() {
        val view = View.inflate(context, R.layout.layout_mini_game_finish_change_game_view, null)
        addView(view, ViewGroup.LayoutParams(mWidth, mHeight))

        mGameIcon = findViewById(R.id.game_icon) as ImageView
        mGameText = findViewById(R.id.game_text) as TextView
        mAcceptButton = findViewById(R.id.game_accept) as Button

        mAcceptButton.setOnClickListener {
            mObserver?.onAccept()
        }

    }

    fun fillData(gameIcon: String?, gameName: String?) {
        if (TextUtils.isEmpty(gameIcon)) {
            return
        }
        if (TextUtils.isEmpty(gameName)) {
            return
        }

        PicassoUtils.loadImage(context, gameIcon, mGameIcon)
        mGameText.setText(MessageUtils.getString(R.string.string_mini_game_change_game_intviu, gameName))
    }

    fun show(referenceObject: View?, parent: ViewGroup?) {
        referenceObject ?: return
        parent ?: return

        if (isShow) return

        val location = IntArray(2)
        referenceObject.getLocationInWindow(location)

        val x = location[0]
        val y = location[1]

        val referenceObjectWidth = referenceObject.width
        val referenceObjectHeight = referenceObject.height

        val startX = x + referenceObjectWidth / 2 - mWidth / 2
        val startY = y - mHeight

        setX(startX.toFloat())
        setY(startY.toFloat())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            elevation = 6f
        }

        parent.addView(this)

        mParent = parent
        isShow = true


    }

    fun dismiss() {

        mParent ?: return

        try {
            mParent!!.removeView(this)
            isShow = false
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}