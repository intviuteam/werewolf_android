package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/4/18 下午4:02
 * Mystery
 */

public class MiniGameNotify implements Keepable {
    public String serverType;
    public String from = "";
    public String to = "";
    public String type = "";
    public String msg = "";
    public boolean is_friend;
}
