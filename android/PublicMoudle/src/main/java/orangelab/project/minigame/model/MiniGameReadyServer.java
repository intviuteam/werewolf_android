package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/26 上午11:54
 * Mystery
 */

public class MiniGameReadyServer implements Keepable {

    public String user_id;
    public boolean ready = false;
}
