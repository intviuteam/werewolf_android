package orangelab.project.minigame.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import orangelab.project.common.model.UserInfoResult;

import java.util.List;

/**
 * game_werewolf
 * 2018/1/20 下午5:01
 * Mystery
 */
public class MiniGameUserInfoViewPagerAdapter extends PagerAdapter {
    private List<UserInfoResult.PhotoItem> items;

    public MiniGameUserInfoViewPagerAdapter(List<UserInfoResult.PhotoItem> items) {
        this.items = items;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(container.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        UserInfoResult.PhotoItem item = items.get(position);
        PicassoUtils.loadImage(container.getContext(), item.url, imageView);
        container.addView(imageView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}