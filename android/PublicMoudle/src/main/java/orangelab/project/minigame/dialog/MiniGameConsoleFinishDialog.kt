package orangelab.project.minigame.dialog

import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PicassoUtils
import com.datasource.GlobalUserState
import com.toolkit.MathToolKit
import orangelab.project.MainApplication
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.utils.MessageUtils
import orangelab.project.minigame.MiniGameContext
import orangelab.project.minigame.model.MiniGameOverForConsole
import orangelab.project.minigame.utils.MiniGameUtils
import org.json.JSONObject

/**
 *game_werewolf
 *2018/5/17 下午3:21
 *Mystery
 */
class MiniGameConsoleFinishDialog : SafeDialog, View.OnClickListener {

    private var mMiniGameContext: MiniGameContext?

    private var mUserHead: ImageView
    private var mUserText: TextView
    private var mScore: TextView
    private var mPlaceHolder1: View
    private var mRange: TextView
    private var mPlaceHolder2: View

    private var mReturn: View
    private var mReplay: View
    private var miniGameOverForConsole: MiniGameOverForConsole

    constructor(mMiniGameContext: MiniGameContext, miniGameOverForConsole: MiniGameOverForConsole, timeCost: Long) : super(mMiniGameContext.context, R.style.DarkDialogAllowDismiss) {
        this.mMiniGameContext = mMiniGameContext
        setContentView(R.layout.layout_mini_game_console_game_finish_dialog)

        this.miniGameOverForConsole = miniGameOverForConsole;

        mUserHead = findViewById(R.id.user_head) as ImageView
        mUserText = findViewById(R.id.user_name) as TextView

        mScore = findViewById(R.id.user_score) as TextView
        mPlaceHolder1 = findViewById(R.id.placeholder1)
        mRange = findViewById(R.id.user_range) as TextView
        mPlaceHolder2 = findViewById(R.id.placeholder2)

        mReturn = findViewById(R.id.action_return)
        mReturn.setOnClickListener(this)

        mReplay = findViewById(R.id.action_again)
        mReplay.setOnClickListener(this)

        PicassoUtils.loadImage(mUserHead.context, GlobalUserState.getGlobalState().userIcon, mUserHead, R.mipmap.default_head)
        mUserText.setText(GlobalUserState.getGlobalState().userName)

        /**
         * 展示分数与排名
         */
        val showScore = miniGameOverForConsole.game_result
        val showResultScore = miniGameOverForConsole.game_result_value
        val scoreMode = miniGameOverForConsole.score_mode

        var reporScore = 0L

        if (miniGameOverForConsole.isNone) {
            /**
             * 非对战模式
             */
            if (TextUtils.equals(scoreMode, "time")) {
                reporScore = timeCost
                mScore.setText(MessageUtils.getString(R.string.string_mini_game_console_score_time, MathToolKit.twoDigitsAfterDecimalPoint(timeCost)))
            } else {
                reporScore = showResultScore
                mScore.setText(MessageUtils.getString(R.string.string_mini_game_console_score, showScore))
            }
            /**
             * 排名先展示计算中...
             */
            mRange.setText(MessageUtils.getString(R.string.string_mini_game_console_range_counting))
            /**
             * 准备汇报服务器
             */
            MiniGameUtils.reportMiniGameRecord(
                    GlobalUserState.getGlobalState().userId,
                    mMiniGameContext?.miniGameLaunch?.type,
                    reporScore,
                    timeCost,
                    { value, e ->
                        if (e == null) {
                            runSafely {
                                mRange.setText(MessageUtils.getString(R.string.string_mini_game_console_range, value.toString()))
                            }
                        }
                    }
            )
        } else {
            /**
             * 对战模式
             */
            mPlaceHolder1.visibility = View.VISIBLE
            mPlaceHolder2.visibility = View.GONE
            if (miniGameOverForConsole.isWin) {
                mScore.setText(MessageUtils.getString(R.string.string_mini_game_console_score, MessageUtils.getString(R.string.user_temp_dialog_win)))
            }

            if (miniGameOverForConsole.isLose) {
                mScore.setText(MessageUtils.getString(R.string.string_mini_game_console_score, MessageUtils.getString(R.string.user_temp_dialog_lose)))
            }

            if (miniGameOverForConsole.isPeace) {
                mScore.setText(MessageUtils.getString(R.string.string_mini_game_console_score, MessageUtils.getString(R.string.string_mini_game_console_peace)))
            }

            /**
             * 更新Ai等级和连胜记录
             */
            val mHelper = MainApplication.getInstance().miniGameAIRecordDaoHelper
            var api_level = miniGameOverForConsole.ai_level
            if (miniGameOverForConsole.isAiLevelChange) {
                api_level = miniGameOverForConsole.ai_level_change
            }
            mHelper?.updateAiRecord(GlobalUserState.getGlobalState().userId, mMiniGameContext?.miniGameLaunch?.type, api_level, miniGameOverForConsole.winning_streak_change)
        }

        resizeDialog(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        setCancelable(false)
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v.id) {
            R.id.action_return -> {
                mMiniGameContext?.forceDestroy("")
            }
            R.id.action_again -> {
                if (miniGameOverForConsole.isNone) {
                    mMiniGameContext?.sendMiniGameMessage("replay", "{}")
                } else {
                    val miniGameAiRecord = MiniGameUtils.getMiniGameAiRecord(mMiniGameContext?.miniGameLaunch?.type)
                    miniGameAiRecord ?: return
                    mMiniGameContext?.sendMiniGameMessage("replay", GsonHelper.getGson().toJson(miniGameAiRecord))
                }
            }
        }
    }


    override fun release() {

    }

}