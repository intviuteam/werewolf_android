package orangelab.project.minigame;

import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.common.BaseGlobalServiceHandler;
import orangelab.project.common.IGlobalService;
import orangelab.project.common.engine.GlobalSocketConstant;
import orangelab.project.minigame.event.MiniGameSendMsgToGlobalEvent;
import orangelab.project.minigame.model.MiniGameInviteBeRefused;
import orangelab.project.minigame.model.GlobalSocketJoinRoom;
import orangelab.project.common.utils.MessageUtils;

import cn.intviu.support.GsonHelper;
import orangelab.project.voice.config.VoiceRoomConfig;

/**
 * game_werewolf
 * 2018/1/30 下午2:55
 * Mystery
 */

public class MiniGameGlobalServiceHandler extends BaseGlobalServiceHandler {

    private static final String TAG = "MiniGameServiceHandler";

    public MiniGameGlobalServiceHandler(IGlobalService globalService) {
        super(globalService);
        initListener();
    }

    private void initListener() {
        RxToolKit.Build(this, MiniGameSendMsgToGlobalEvent.class).action(value -> {
            if (mGlobalService != null) {
                mGlobalService.sendMessage(value.type, value.payload);
            }
        }).register();
    }

    @Override
    public boolean isCareType(String type) {
        return TextUtils.equals(type, GlobalSocketConstant.MINI_GAME_PLAY_SERVER_TYPE) || TextUtils.equals(type, GlobalSocketConstant.MINI_GAME_PLAY_TYPE);
    }

    @Override
    public void handleMessage(String type, String payload) {
        Log.i(TAG, "handleMessage: " + type + ", payload " + payload);
        if (TextUtils.equals(type, GlobalSocketConstant.JOIN_ROOM)) {
            if (GlobalUserState.getGlobalState().isGaming()&&!VoiceRoomConfig.isLobby()) {
                PLog.e(TAG, "is in gaming, so can't join mini game room ");
                /**
                 * 正在游戏中无法处理游戏邀请
                 */
            } else {
                GlobalSocketJoinRoom room = GsonHelper.getGson().fromJson(payload, GlobalSocketJoinRoom.class);
                RxToolKit.Emit(room);
            }
            return;
        }
        if (TextUtils.equals(type, MiniGameConstant.INVITE_BE_REFUSED)) {
            try {
                MiniGameInviteBeRefused inviteBeRefused = GsonHelper.getGson().fromJson(payload, MiniGameInviteBeRefused.class);
                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_intviu_be_refused, inviteBeRefused.target_user.name));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        if (TextUtils.equals(type, GlobalSocketConstant.INVITE_VAILD)) {
            UIActuator.post(() -> {
                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_intviu_vaild));
            });
            return;
        }
        if (TextUtils.equals(type, GlobalSocketConstant.INVITED_SUCCESS)) {
//            GlobalSocketEngine.INSTANCE.reportSendInvite();
            UIActuator.post(() -> {
                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_intviu_success));
            });
            return;
        }
//        if (TextUtils.equals(type, GlobalSocketConstant.MG_WT_ST)) {
//            try {
//                MiniGameRoomDestroy destroy = GsonHelper.getGson().fromJson(payload, MiniGameRoomDestroy.class);
//                RxToolKit.Emit(destroy);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return;
//        }
    }

    @Override
    public void destroy() {
        super.destroy();
        RxToolKit.UnRegister(this);
    }
}
