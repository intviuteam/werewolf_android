package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.db.entity.MiniGameRecordEntity;

/**
 * game_werewolf
 * 2018/5/19 下午1:43
 * Mystery
 */
public class MiniGameRecordReport implements Keepable {


    public String gtype;
    public ArrayList<MiniGameRecordReportItem> ginfo;

    public static class MiniGameRecordReportItem implements Keepable {

        public long time;
        public long score;
    }

    public static MiniGameRecordReport createReport(String gameType, List<MiniGameRecordEntity> entities) {
        MiniGameRecordReport recordReport = new MiniGameRecordReport();
        recordReport.gtype = gameType;
        ArrayList<MiniGameRecordReportItem> list = new ArrayList<>();
        if (entities != null) {
            for (MiniGameRecordEntity entity : entities) {
                MiniGameRecordReportItem item = new MiniGameRecordReportItem();
                item.time = entity.getGameCostTime();
                item.score = entity.getGameScoreRecordValue();
                list.add(item);
            }
        }
        recordReport.ginfo = list;
        return recordReport;
    }
}
