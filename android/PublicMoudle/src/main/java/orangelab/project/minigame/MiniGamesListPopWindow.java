package orangelab.project.minigame;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.arch.ArchViewModelProvider;
import com.androidtoolkit.arch.ArchViewModelProviders;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.utils.MessageUtils;
import orangelab.project.minigame.adapter.MiniGamesListAdapter;
import orangelab.project.minigame.adapter.MiniGamesViewPagerAdapter;
import orangelab.project.minigame.manager.MiniGameDownloadManager;
import orangelab.project.minigame.model.MiniGameItem;
import orangelab.project.common.utils.SafeHandler;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.minigame.view.MiniGameLoadOverCallBack;
import orangelab.project.minigame.viewmodel.MiniGameListViewModel;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMMiniGameEvent;

/**
 * game_werewolf
 * 2018/1/19 上午10:02
 * Mystery
 */

public class MiniGamesListPopWindow extends PopupWindow implements LifecycleOwner {
    private static final String TAG = "MiniGamesListPopWindow";

    private final int DEFAULT_POP_WINDOW_HEIGHT = ScreenUtils.dip2px(190);
    private final int PAGE_MAX_SIZE = 8;

    public static final int FROM_LOBBY = 1;
    public static final int FROM_OTHER = 0;

    private Context mContext;

    private ViewPager mGameViewPager;
    private LinearLayout mGameIndex;
    private ProgressBar mGameDataLoading;

    private List<GridView> mGridViewList = new ArrayList<>(0);
    private ImageView[] mGameIndexPoints;
    private List<MiniGameItem> mMiniGameItems;

    private SafeHandler mSafeHandler = new SafeHandler();
    private int mTotalPages = 0;
    private int mCurPages = 0;
    private String mTargetUserId = "";
    private MiniGameItemClickListener mItemClickListener; //item的点击回调
    private MiniGameLoadOverCallBack mLoadOverCallBack; //小游戏加载完毕的回调
    private int fromType = FROM_OTHER;  // 0 非游戏大厅，1 游戏大厅

    private LifecycleRegistry mLifecycleRegistry;
    private ArchViewModelProvider mViewModelProvider;

    public void setTargetUserId(String targetUserId) {
        mTargetUserId = targetUserId;
    }

    private MiniGamesListPopWindow(Context context) {
        super(context);

        mLifecycleRegistry = new LifecycleRegistry(this);
        mViewModelProvider = ArchViewModelProviders.of();
        mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        mContext = context;
        View view = View.inflate(mContext, R.layout.layout_amusement_popwindow, null);
        initView(view);
        setContentView(view);
        initPopWindow();

    }

    public MiniGamesListPopWindow(Context context, int from, MiniGameItemClickListener listener, MiniGameLoadOverCallBack callBack) {
        this(context);
        this.mItemClickListener = listener;
        this.mLoadOverCallBack = callBack;
        this.fromType = from;
    }

    private void initPopWindow() {
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(false);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initView(View view) {
        mGameViewPager = (ViewPager) view.findViewById(R.id.game_viewpager);
        mGameIndex = (LinearLayout) view.findViewById(R.id.game_index);
        mGameDataLoading = (ProgressBar) view.findViewById(R.id.game_loading);
    }

    private void startLoading() {
        mGameViewPager.setVisibility(View.GONE);
        mGameIndex.setVisibility(View.GONE);
        mGameDataLoading.setVisibility(View.VISIBLE);
    }

    private void endLoading() {
        mGameViewPager.setVisibility(View.VISIBLE);
        mGameIndex.setVisibility(View.VISIBLE);
        mGameDataLoading.setVisibility(View.GONE);
    }

    public void show(View parent) {
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
        startLoading();
        showInner(parent);
        mViewModelProvider.get(MiniGameListViewModel.class).getApiGameListLiveData((fromType == FROM_LOBBY)).observe(this, miniGamesListResultApiResultData -> {
            mSafeHandler.postSafely(() -> {
                Log.i(TAG, miniGamesListResultApiResultData + "");
                if (miniGamesListResultApiResultData.e != null) {
                    ToastToolKit.showLong(miniGamesListResultApiResultData.e.getMessage());
                    dismiss();
                    return;
                } else {
                    /**
                     * prepare load game
                     */
                    endLoading();
                    mMiniGameItems = miniGamesListResultApiResultData.data.games;
                    if (mMiniGameItems == null || mMiniGameItems.size() == 0) {
                        ToastToolKit.showLong(MessageUtils.getString(R.string.mini_game_no_games));
                        dismiss();
                        return;
                    } else {

                        /**
                         * 更新下小游戏的各个进度
                         */
                        MiniGameDownloadManager.INSTANCE.decorMiniGameList(mMiniGameItems);
                        /**
                         * we have some mini-games
                         */
                        updateTotalPages();

                        loadGameIndexView();

                        loadGridViewList();

                        loadGameViewPager();
                    }

                    try {
                        //保存小游戏列表
                        if (mLoadOverCallBack != null) {
                            mLoadOverCallBack.onLoadOver(mMiniGameItems);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
        });
    }

    private void loadGameViewPager() {
        mGameViewPager.setAdapter(new MiniGamesViewPagerAdapter<GridView>(mGridViewList));
        mGameViewPager.setOffscreenPageLimit(0);
        mGameViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mTotalPages; i++) {
                    if (i == position) {
                        mGameIndexPoints[i].setImageResource(R.mipmap.gift_index_selected);
                    } else {
                        mGameIndexPoints[i].setImageResource(R.mipmap.gift_index_normal);
                    }
                }
                mCurPages = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void loadGridViewList() {
        for (int i = 0; i < mTotalPages; i++) {
            final GridView gridView = (GridView) View.inflate(mContext, R.layout.fragment_gift_viewpage_item, null);
            final MiniGamesListAdapter adapter = new MiniGamesListAdapter(mContext, mMiniGameItems, i, PAGE_MAX_SIZE);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener((parent, view, position, id) -> {
                MiniGameItem game = (MiniGameItem) gridView.getItemAtPosition(position);

                String gameType = game.type;
                String gameDownload = game.download;
                if (TextUtils.isEmpty(gameType)) {
                    return;
                }
                if (!TextUtils.isEmpty(gameDownload)) {

                    if (MiniGameDownloadManager.INSTANCE.checkIsDownloading(gameType)) {
                        return;
                    }

                    /**
                     * 这里要加一个判断，如果本地已经下载了这个游戏，那么就执行下面的方法
                     */
                    if (MiniGameDownloadManager.INSTANCE.checkDownloaded(gameType, gameDownload)) {
                        /**
                         * 下载正确
                         */


                    } else {
                        /**
                         * 执行下载操作
                         * 1.更新数据
                         * 2.开始下载
                         * 3.刷新UI
                         */
                        game.isDownloading = true;
                        game.isFinish = false;
                        game.progress = 0;

                        MiniGameDownloadManager.INSTANCE.beginDownload(gameType, gameDownload);
                        ((MiniGamesListAdapter) gridView.getAdapter()).notifyDataSetChanged();
                        return;
                    }
                }


                try {
                    if (this.mItemClickListener != null) {
                        this.mItemClickListener.onItemClicked(this, new LCIMMiniGameEvent(game, mTargetUserId));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            mGridViewList.add(gridView);
        }
    }

    private void updateTotalPages() {
        mTotalPages = (int) Math.ceil(mMiniGameItems.size() * 1.0 / PAGE_MAX_SIZE);
    }

    private void loadGameIndexView() {
        mGameIndex.removeAllViews();
        mGameIndexPoints = new ImageView[mTotalPages];
        for (int i = 0; i < mTotalPages; i++) {
            mGameIndexPoints[i] = new ImageView(mContext);
            if (i == mCurPages) {
                mGameIndexPoints[i].setImageResource(R.mipmap.gift_index_selected);
            } else {
                mGameIndexPoints[i].setImageResource(R.mipmap.gift_index_normal);
            }
            mGameIndexPoints[i].setPadding(3, 3, 3, 3);
            mGameIndex.addView(mGameIndexPoints[i]);
        }
    }

    private void showInner(View parent) {
        int[] location = new int[2];
        parent.getLocationOnScreen(location);
        showAtLocation(parent, Gravity.NO_GRAVITY, location[0], location[1] - DEFAULT_POP_WINDOW_HEIGHT);
        Log.i(TAG, "show: ");
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        Log.i(TAG, "dismiss: ");

        mSafeHandler.release();
        mLoadOverCallBack = null;
        mItemClickListener = null;
        mViewModelProvider.destroy();

        for (GridView temp : mGridViewList) {
            ((MiniGamesListAdapter) temp.getAdapter()).destroy();
        }

        mLifecycleRegistry.markState(Lifecycle.State.DESTROYED);
    }
}
