package orangelab.project.minigame;

/**
 * game_werewolf
 * 2018/1/25 下午12:00
 * Mystery
 */

public class MiniGameConstant {

    public static final String JOIN_SERVER = "join_server";
    public static final String LEAVE_SERVER = "leave_server";
    public static final String READY = "ready";
    public static final String MOVE = "move";
    public static final String REPLAY = "replay";
    public static final String CHANGE_GAME = "change_game";
    public static final String CHANGE_OPPO = "change_oppo";
    public static final String REPLAY_SERVER = "replay_server";
    public static final String READY_SERVER = "ready_server";
    public static final String START_GAME_SERVER = "start_game_server";
    public static final String GAME_CLOSE_SERVER = "game_close_server";
    public static final String GAME_OVER_SERVER = "game_over_server";
    public static final String ENTER_INIT = "enter_init";
    public static final String INVITE = "invite";
    public static final String PLAY_MINI_GAME = "play_mini_game";
    public static final String PLAY_MINI_GAME_SERVER = "play_mini_game_server";
    public static final String DOM_INIT = "dom_init";
    public static final String DOM_SHOW = "dom_show";
    public static final String ORBIT_CONNECT = "orbit_connect";
    public static final String ORBIT_DISCONNECT = "orbit_disconnect";
    public static final String EMPTY_JSON_STRING = "{}";
    public static final String SERVER_CONNECT = "server_connect";
    public static final String SERVER_DISCONNECT = "server_disconnect";
    public static final String ORBIT_EXITED = "orbit_exited";
    public static final String ORBIT_EXIT = "orbit_exit";
    public static final String ORBIT_ENTER = "orbit_enter";
    public static final String ORBIT_CLOSE_SPEAKER = "orbit_close_speaker"; //关闭音频播放
    public static final String ORBIT_OPEN_SPEAKER = "orbit_open_speaker"; //打开音频播放
    public static final String ORBIT_OPEN_RECORD = "orbit_open_record";// 打开录音
    public static final String ORBIT_CLOSE_RECORD = "orbit_close_record";//关闭录音

    public static final String INVITE_BE_REFUSED = "invite_be_refused";
    public static final String CANCEL_INVITE = "cancel_invite";
    public static final String USER_RETURN_SERVER = "user_return_server";
    public static final String NATIVE_DESTROY = "native_destroy";
    public static final String FORCE_LEAVE_SERVER = "force_leave_server";

    public static final String NOTIFY_GO1 = "go1";
    public static final String NOTIFY_GO2 = "go2";
    public static final String NOTIFY_GO3 = "go3";
    public static final String NOTIFY_GO4 = "go4";
    public static final String NOTIFY_GO5 = "go5";

    public static final String NOTIFY = "notify";
    public static final String NOTIFY_SERVER = "notify_server";

    public static final String REQUIRE = "require";
    public static final String REQUIRE_ADD = "require_add";
    public static final String REQUIRE_SENDED = "require_sended";
    public static final String FIREND_REQUIRE_SERVER = "friend_require_server";
    public static final String FIREND_REQUIRE = "friend_require";
    public static final String ADD_FIREND_SUCCESS = "add_friend_success";

    public static final String ORBIT_PERMISSION_DINED = "orbit_permission_dined";

    public static final String CHANGE_AT_GAMEOVER = "change_at_gameover";
    public static final String CHANGE_AT_GAMEOVER_SERVER = "change_at_gameover_server";
    public static final String CHANGE_USER = "change_user";
    public static final String GAME_TYPE = "game_type";
    public static final String CHANGE_GAME_REQUIRED = "change_game_required";

    public static final String REPORT_ERROR_TO_UMENG = "report_error_to_umeng";

    public static final int MINI_GAME_AI_DEFAULT_LEVEL = 3;

}
