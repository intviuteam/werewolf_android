package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/19 下午1:49
 * Mystery
 */

public class MiniGameItem implements Keepable {

    public String name;
    public String icon;
    public String type;
    public String download;

    public boolean isDownloading = false;
    public int progress = 0;
    public boolean isFinish = true;

    @Override
    public String toString() {
        return "MiniGameItem{" +
                "name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
