package orangelab.project.minigame.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.LogHelper;
import orangelab.project.minigame.MiniGameContext;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.minigame.model.MiniGameTip;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;

import com.squareup.picasso.Callback;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2018/1/31 上午11:20
 * Mystery
 */

public class MiniGamePrepareView implements Destroyable {

    private static final String TAG = "MiniGamePrepareView";

    private View mRoot;

    private TextView mCountDownView;
    private int mCount = 0;
    private int mLoadingResourceWaitingTime = 35 * 1000;
    private int mMatchingWaitingTime = 60 * 1000;
    private int mLoadingWaitingTime = 30 * 1000;
    private long mTipsRandomTime = 5 * 1000l;
    private SafeHandler mSafeHandler = new SafeHandler();
    private boolean loadingResourceErrorShow = false;
    private boolean matchingErrorShow = false;
    private boolean loadingErrorShow = false;
    private Runnable mCountingRunnable = new Runnable() {
        @Override
        public void run() {
            mCount++;
            mCountDownView.setText(MessageUtils.getString(R.string.amusement_matching_counting, Integer.toString(mCount)));
            try {
                if (mMiniGameContext != null) {
                    if (mMiniGameContext.isCurProcessResourceLoading()) {
                        if (!loadingResourceErrorShow) {
                            if (System.currentTimeMillis() - mMiniGameContext.getProcessResourceLoadingStartTime() > mLoadingResourceWaitingTime) {
                                ToastToolKit.showLong(MessageUtils.getString(R.string.mini_game_loading_resource_too_long));
                                loadingResourceErrorShow = true;
                            }
                        }
                    } else if (mMiniGameContext.isCurProcessMatching()) {
                        if (!matchingErrorShow) {
                            if (System.currentTimeMillis() - mMiniGameContext.getProcessMatchingStartTime() > mMatchingWaitingTime) {
                                ToastToolKit.showLong(MessageUtils.getString(R.string.mini_game_matching_too_long));
                                matchingErrorShow = true;
                            }
                        }
                    } else if (mMiniGameContext.isCurProcessLoading()) {
                        if (!loadingErrorShow) {
                            if (System.currentTimeMillis() - mMiniGameContext.getProcessLoadingStartTime() > mLoadingWaitingTime) {
                                ToastToolKit.showLong(MessageUtils.getString(R.string.mini_game_loading_too_long));
                                loadingErrorShow = true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            startCounting();
        }
    };

    private View mBackGroundRound1;
    private View mBackGroundRound2;
    private View mBackGroundRound3;
    private boolean mBackGroundAnimRunning = false;


    private FrameLayout mBackGroundRound4;
    private View mBackGroundPk;

    private ImageView mLeftImageView;
    private ImageView mRightImageView;
    private ImageView mMidImageView;

    private TextView mLoadingText;
    private ImageView mGameIcon;
    private TextView mGameTv;

    private TextView mGameTips;

    private MiniGameContext mMiniGameContext;
    private List<String> mOtherImagePlaceHolder;
    private ArrayList<MiniGameTip> mTips;
    private Random mRandom;
    private Runnable mTipsRunnable = () ->
    {
        fillTips();
        startTips();
    };

    private AnimatorSet mBackGroundAnim;
    private List<Animator> mAnimator;

    private MiniGameRandomAnimView mRandomAnimView;

    public MiniGamePrepareView(MiniGameContext miniGameContext, MiniGameLaunch miniGameLaunch) {

        mMiniGameContext = miniGameContext;

        mOtherImagePlaceHolder = mMiniGameContext.getMiniGameLaunch().getUser_images();
        mRandom = new Random();
        mTips = mMiniGameContext.getMiniGameLaunch().getTips();
        mAnimator = new ArrayList<>();

        mRoot = mMiniGameContext.getRootView().findViewById(R.id.id_minigame_prepare);

        mBackGroundRound1 = mRoot.findViewById(R.id.id_mini_game_round_1);
        mBackGroundRound2 = mRoot.findViewById(R.id.id_mini_game_round_2);
        mBackGroundRound3 = mRoot.findViewById(R.id.id_mini_game_round_3);
        mBackGroundRound4 = (FrameLayout) mRoot.findViewById(R.id.id_mini_game_round_4);
        mBackGroundPk = mRoot.findViewById(R.id.imageView15);

        mLeftImageView = (ImageView) mRoot.findViewById(R.id.id_mini_game_left_user);
        mRightImageView = (ImageView) mRoot.findViewById(R.id.id_mini_game_right_user);
        mMidImageView = (ImageView) mRoot.findViewById(R.id.id_mini_game_mid_user);

        mGameTips = (TextView) mRoot.findViewById(R.id.id_minigame_tips);

        mCountDownView = (TextView) mRoot.findViewById(R.id.id_minigame_countdown);

        mLoadingText = (TextView) mRoot.findViewById(R.id.id_minigame_loading_text);

        mGameIcon = (ImageView) mRoot.findViewById(R.id.id_minigame_match_loading_iv);
        mGameTv = (TextView) mRoot.findViewById(R.id.id_minigame_match_loading_tv);
        if (mOtherImagePlaceHolder == null) {
            mOtherImagePlaceHolder = new ArrayList<>();
        }
        mRandomAnimView = new MiniGameRandomAnimView(mBackGroundRound4, mOtherImagePlaceHolder);


        /**
         * 初始化时需要判断，单机游戏还是联机游戏
         */
        if (miniGameLaunch.isConsoleGame()) {
            alphaOutImageView(mMidImageView, GlobalUserState.getGlobalState().getUserIcon());
        } else {
            alphaOutImageView(mLeftImageView, GlobalUserState.getGlobalState().getUserIcon());
        }
        // PicassoUtils.loadImage(mRoot.getContext(), GlobalUserState.getGlobalState().getUserIcon(), mLeftImageView, R.mipmap.default_head);

        fillTips();
    }

    public void startTips() {
        mSafeHandler.postDelaySafely(mTipsRunnable, mTipsRandomTime);
    }

    private void startMatchedAnim(String userIcon) {
        releaseRandomAnim();
        alphaOutImageView(mRightImageView, userIcon);
    }

    private void alphaOutImageView(ImageView imageView, String userIcon) {
        Animator animator = Helper.createAlphaOut(imageView, 1f, 100l);
        mAnimator.add(animator);
        PicassoUtils.loadImage(mRoot.getContext(), userIcon, imageView, R.mipmap.default_head);
        animator.start();
    }

    public void startRandomAnim() {
        mBackGroundRound4.post(() -> {
            mSafeHandler.postSafely(() -> {
                if (mBackGroundPk == null) {
                    return;
                }
                if (mRandomAnimView == null) {
                    return;
                }
                try {
                    mRandomAnimView.init(mBackGroundPk);
                    mRandomAnimView.startRandomAnim();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            });
        });
    }


    public void show() {
        mRoot.setVisibility(View.VISIBLE);
    }

    public void gone() {
        mRoot.setVisibility(View.GONE);
    }

    public void startCounting() {
        mSafeHandler.postDelaySafely(mCountingRunnable, 1000);
    }

    public void reSet() {
        try {
            mTips = mMiniGameContext.getMiniGameLaunch().getTips();
            fillTips();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSafeHandler = new SafeHandler();
        mBackGroundAnimRunning = false;
        mCount = 0;
        mCountDownView.setText(MessageUtils.getString(R.string.amusement_matching_counting, Integer.toString(mCount)));
        mRandomAnimView.reSet();
    }

    public void turnIntoLoadingResource() {
        startBackGroundAnim();
        mLoadingText.setVisibility(View.VISIBLE);
        mLoadingText.setText(MessageUtils.getString(R.string.mini_game_resource_loading));
        mRightImageView.setAlpha(0f);
        mGameTv.setVisibility(View.GONE);
        mGameIcon.setVisibility(View.GONE);

        MiniGameLaunch launch = mMiniGameContext.getMiniGameLaunch();
        if (launch != null) {
            if (TextUtils.isEmpty(launch.getOppoIcon())) {
                /**
                 * 匹配类型
                 */
            } else {
                /**
                 * 对战类型
                 */
                turnIntoMatched(launch.getOppoIcon());
            }
        }
    }

    public void turnIntoLoading() {
        mLoadingText.setText(MessageUtils.getString(R.string.mini_game_waiting_other));
    }

    public void turnIntoMatching() {
        mLoadingText.setText(MessageUtils.getString(R.string.amusement_matching));
    }

    public void goneLoadingText() {
        mLoadingText.setVisibility(View.INVISIBLE);
    }

    public void turnIntoMatched(String mMatchUserIcon) {
        turnIntoShowGameWaiting(mMiniGameContext.getMiniGameLaunch().getGameName(), mMiniGameContext.getMiniGameLaunch().getGameIcon());
        startMatchedAnim(mMatchUserIcon);
    }

    public void turnIntoShowGameWaiting(String gameName, String gameIcon) {
        mGameIcon.setVisibility(View.VISIBLE);
        mGameTv.setVisibility(View.VISIBLE);
        PicassoUtils.loadImage(mRoot.getContext(), gameIcon, mGameIcon);
        mGameTv.setText(MessageUtils.getString(R.string.amusement_game_loading, gameName));
    }

    private void startBackGroundAnim() {
        if (mBackGroundAnimRunning) {
            return;
        }
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator round3Anim = ObjectAnimator.ofFloat(mBackGroundRound3, View.ROTATION, 0f, 360f);
        round3Anim.setRepeatCount(ValueAnimator.INFINITE);
        round3Anim.setDuration(8000);
        round3Anim.setInterpolator(new LinearInterpolator());

        ObjectAnimator round2Anim = ObjectAnimator.ofFloat(mBackGroundRound2, View.ROTATION, 0f, 360f);
        round2Anim.setRepeatCount(ValueAnimator.INFINITE);
        round2Anim.setDuration(6000);
        round2Anim.setInterpolator(new LinearInterpolator());

        ObjectAnimator round1Anim = ObjectAnimator.ofFloat(mBackGroundRound1, View.ROTATION, 0f, 360f);
        round1Anim.setRepeatCount(ValueAnimator.INFINITE);
        round1Anim.setDuration(4000);
        round1Anim.setInterpolator(new LinearInterpolator());

        AnimatorSet set = new AnimatorSet();
        set.play(round2Anim).with(round1Anim);
        animatorSet.play(set).with(round3Anim);
        mBackGroundAnim = animatorSet;
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                LogHelper.miniGameLog("Bg Anim onAnimationCancel");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                LogHelper.miniGameLog("Bg Anim onAnimationEnd");
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                LogHelper.miniGameLog("Bg Anim onAnimationStart");
            }
        });
        animatorSet.start();
        mBackGroundAnimRunning = true;
    }

    public void release() {
        for (Animator animator : mAnimator) {
            if (animator.isRunning()) {
                animator.cancel();
            }
        }
        mAnimator.clear();
        mSafeHandler.release();
    }

    public void releaseBackGroundAnim() {
        if (mBackGroundAnim != null && mBackGroundAnim.isRunning()) {
            mBackGroundAnim.cancel();
            mBackGroundAnim = null;
        }
    }

    public void releaseRandomAnim() {
        if (mRandomAnimView != null) {
            mRandomAnimView.destroy();
        }
    }

    private void fillTips() {
        String msg = getRandomTips();
        mGameTips.setText(msg);
    }

    private String getRandomTips() {
        try {
            if (mTips.size() == 0) {
                return "";
            }
            if (mTips.size() == 1) {
                return mTips.get(0).msg;
            }
            int x = mRandom.nextInt(mTips.size() - 1);
            if (x > mTips.size()) {
                return mTips.get(mTips.size() - 1).msg;
            }
            return mTips.get(x).msg;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void destroy() {
        if (mMiniGameContext != null) {
            mMiniGameContext = null;
        }
        releaseBackGroundAnim();
        releaseRandomAnim();
        release();
    }

    public static class Helper {

        public static Animator createAlphaOut(View targetView, float scale, long time) {
            ObjectAnimator animatorAlphaOut = ObjectAnimator.ofFloat(targetView, "alpha", 0, 1);
            animatorAlphaOut.setDuration(time);
            PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 0f, scale);
            PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 0f, scale);
            Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(time);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(animatorAlphaOut).with(animatorScale);
            return animatorSet;
        }

        public static Animator createAlphaIn(View targetView, float scale, long time) {
            ObjectAnimator animatorAlphaIn = ObjectAnimator.ofFloat(targetView, "alpha", 1, 0);
            animatorAlphaIn.setDuration(time);
            PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", scale, 0f);
            PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", scale, 0f);
            Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(time);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(animatorAlphaIn).with(animatorScale);
            return animatorSet;
        }
    }
}
