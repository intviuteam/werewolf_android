package orangelab.project.minigame.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.androidtoolkit.UIActuator;
import com.androidtoolkit.arch.ApiObservableData;
import com.androidtoolkit.arch.ArchMutableLiveData;

import orangelab.project.minigame.MiniGamesApiManager;
import orangelab.project.minigame.model.MiniGamesListResult;

/**
 * game_werewolf
 * 2018/7/30 上午10:52
 * Mystery
 */
public class MiniGameListViewModel extends ViewModel {

    private static final String TAG = "MiniGameListViewModel";

    private MutableLiveData<ApiObservableData<MiniGamesListResult>> mResultLiveData;

    public MutableLiveData<ApiObservableData<MiniGamesListResult>> getApiGameListLiveData(boolean isInLobby) {
        if (mResultLiveData == null) {
            mResultLiveData = new ArchMutableLiveData<>();
        }
        /**
         * 获取小游戏列表数据
         */
        MiniGamesApiManager.GetMiniGamesList(isInLobby, (miniGamesListResult, e) -> {
            mResultLiveData.postValue(ApiObservableData.createApiObservableData(miniGamesListResult, e));
        });
        return mResultLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i(TAG, "onCleared: ");
    }
}
