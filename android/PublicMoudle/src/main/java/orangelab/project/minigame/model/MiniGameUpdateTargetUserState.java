package orangelab.project.minigame.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/30 下午2:00
 * Mystery
 */

public class MiniGameUpdateTargetUserState implements Keepable {

    public String type = "update_target_user_status";
    public boolean is_online = false;
    public String target_user_id;
    public String game_type;
    public String uuid;
}
