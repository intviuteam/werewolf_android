package orangelab.project;

import android.text.TextUtils;

import com.androidtoolkit.ApkToolKit;
import com.androidtoolkit.DeviceToolKit;
import com.androidtoolkit.Environment;
import com.androidtoolkit.TimeZoneToolKit;
import com.androidtoolkit.emulator.EmulatorCheckUtil;
import com.androidtoolkit.location.GlobalLocation;
import com.datasource.GlobalUserState;
import com.networktoolkit.BuildConfig;
import com.networktoolkit.transport.IHeaderDecorate;
import com.networktoolkit.transport.RequestTaskUtils;
import com.networktoolkit.transport.TransportConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cn.intviu.service.IntviuApplication;
import cn.intviu.support.ConstInfo;
import okhttp3.Request;

/**
 * game_werewolf
 * 2018/3/1 上午11:59
 * Mystery
 */

public class HeaderDecorate implements IHeaderDecorate {

    @Override
    public Request.Builder decorateHttpRequestHeader(Request.Builder builder) {
        try {
            builder.addHeader("lg", ApkToolKit.GetApkLanguage());
            builder.addHeader("v", Integer.toString(BuildConfig.VERSION_CODE));
            builder.addHeader(TransportConfig.SV_KEY, TransportConfig.SV_VALUE);
            builder.addHeader("tz", TimeZoneToolKit.GetCurrentTimeZone());
            builder.addHeader("pt", "android");
            builder.addHeader("sign", ApkToolKit.GetApkVerifyStringByMD5());
            builder.addHeader("channel", ProjectConfig.APP_CHANNEL);
            builder.addHeader("em", EmulatorCheckUtil.getEmCode(Environment.GetAppContext()));
            try {
                builder.addHeader("did", ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
                builder.addHeader("pid", DeviceToolKit.GetDeviceId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            // TODO: 2018/3/1 如果含有地理位置 则传输地理位置 否则不传输
            if (!TextUtils.isEmpty(GlobalLocation.INSTANCE.getLatitude()) && !TextUtils.isEmpty(GlobalLocation.INSTANCE.getLongitude())) {
                builder.addHeader("lng", GlobalLocation.INSTANCE.getLongitude());
                builder.addHeader("lag", GlobalLocation.INSTANCE.getLatitude());
            }
            builder.addHeader("share_version", "3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder;
    }

    @Override
    public Map<String, List<String>> decorateSocketHeader(Map<String, List<String>> headers) {
        try {
            headers.put("x-access-token", Arrays.asList(GlobalUserState.getGlobalState().getToken()));
            headers.put("lg", Arrays.asList(ApkToolKit.GetApkLanguage()));
            headers.put("v", Arrays.asList(Integer.toString(BuildConfig.VERSION_CODE)));
            headers.put(TransportConfig.SV_KEY, Arrays.asList(TransportConfig.SV_VALUE));
            headers.put("tz", Arrays.asList(TimeZoneToolKit.GetCurrentTimeZone()));
            headers.put("pt", Arrays.asList("android"));
            headers.put("sign", Arrays.asList(ApkToolKit.GetApkVerifyStringByMD5()));
            headers.put("app", Arrays.asList(RequestTaskUtils.APP_TYPE));
            headers.put("channel", Arrays.asList(ProjectConfig.APP_CHANNEL));
            headers.put("em", Arrays.asList(EmulatorCheckUtil.getEmCode(Environment.GetAppContext())));
            try {
                headers.put("did", Arrays.asList(ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance())));
                headers.put("pid", Arrays.asList(DeviceToolKit.GetDeviceId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            headers.put("share_version", Arrays.asList("3"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return headers;
    }

    @Override
    public Map<String, String> decorateWebViewHeader(Map<String, String> extraHeaders) {
        try {
            extraHeaders.put("x-access-token", GlobalUserState.getGlobalState().getToken());
            extraHeaders.put("lg", ApkToolKit.GetApkLanguage());
            extraHeaders.put("v", Integer.toString(com.support.BuildConfig.VERSION_CODE));
            extraHeaders.put(TransportConfig.SV_KEY, TransportConfig.SV_VALUE);
            extraHeaders.put("tz", TimeZoneToolKit.GetCurrentTimeZone());
            extraHeaders.put("pt", "android");
            extraHeaders.put("sign", ApkToolKit.GetApkVerifyStringByMD5());
            extraHeaders.put("app", TransportConfig.APP_TYPE);
            extraHeaders.put("channel", ProjectConfig.APP_CHANNEL);
            extraHeaders.put("em", EmulatorCheckUtil.getEmCode(Environment.GetAppContext()));
//            extraHeaders.put("share_version", "3");
            try {
                extraHeaders.put("did", ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
                extraHeaders.put("pid", DeviceToolKit.GetDeviceId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return extraHeaders;
    }

    @Override
    public String decorateUrl(String url) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("?access_token=" + GlobalUserState.getGlobalState().getToken());
            sb.append("&lg=" + ApkToolKit.GetApkLanguage());
            sb.append("&v=" + Integer.toString(com.support.BuildConfig.VERSION_CODE));
            sb.append("&" + TransportConfig.SV_KEY + "=" + TransportConfig.SV_VALUE);
            sb.append("&tz=" + TimeZoneToolKit.GetCurrentTimeZone());
            sb.append("&pt=android");
            sb.append("&sign=" + ApkToolKit.GetApkVerifyStringByMD5());
            sb.append("&app=" + TransportConfig.APP_TYPE);
            sb.append("&channel=" + ProjectConfig.APP_CHANNEL);
            sb.append("&em=" + EmulatorCheckUtil.getEmCode(Environment.GetAppContext()));
            try {
                sb.append("&did=" + ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
                sb.append("&pid=" + DeviceToolKit.GetDeviceId());
            } catch (Exception e) {
                e.printStackTrace();
            }
            sb.append("&_t=" + System.currentTimeMillis());
            sb.append("&minigame_version=2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url + sb.toString();
    }
}
