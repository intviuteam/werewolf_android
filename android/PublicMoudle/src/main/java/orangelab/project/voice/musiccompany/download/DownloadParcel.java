package orangelab.project.voice.musiccompany.download;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 公有下载实体
 */
public class DownloadParcel implements Parcelable {

    //出错状态码
    public static final int RIGHT_CODE = 0; //下载顺利
    public static final int ERROR_CODE = 1; //下载出错

    public int _id;
    public String id;
    public int progress = 0;
    public String type = "music";                     //下载的类型，目前只有music，为了防止后面添加其他类型添加
    public String url = "";
    public int fileSize = 0;
    public int status = 0;
    public String imageUrl = "";
    public String title = "";
    public int isError = 0;//0没错 1有错
    public String speed = "";
    public String local_url = "";                //本地存储路径
    public String fileName = "";
    public String packageName = "";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(url);
        dest.writeInt(progress);
        dest.writeInt(fileSize);
        dest.writeInt(status);
        dest.writeString(imageUrl);
        dest.writeString(title);
        dest.writeInt(isError);
        dest.writeString(speed);
        dest.writeString(local_url);
        dest.writeString(fileName);
        dest.writeString(packageName);
    }

    public static final Creator<DownloadParcel> CREATOR = new Creator<DownloadParcel>() {
        public DownloadParcel createFromParcel(Parcel in) {
            return new DownloadParcel(in);
        }

        public DownloadParcel[] newArray(int size) {
            return new DownloadParcel[size];
        }
    };

    public DownloadParcel(Parcel in) {
        _id = in.readInt();
        id = in.readString();
        type = in.readString();
        url = in.readString();
        progress = in.readInt();
        fileSize = in.readInt();
        status = in.readInt();
        imageUrl = in.readString();
        title = in.readString();
        isError = in.readInt();
        speed = in.readString();
        local_url = in.readString();
        fileName = in.readString();
        packageName = in.readString();
    }

    public DownloadParcel() {
        super();
    }

    public DownloadParcel(String id, String url, String title, String fileName) {
        this.id = id;
        this.type = "music";
        this.url = url;
        this.title = title;
        this.fileName = fileName;
    }

    public DownloadParcel(String id, String type, String url, String imageUrl, String title, String packageName, String fileName) {
        this.id = id;
        this.type = type;
        this.url = url;
        this.imageUrl = imageUrl;
        this.title = title;
        this.packageName = packageName;
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "id=" + id + ",type=" + type + ",progress=" + progress + ",url=" + url + ",fileSize=" + fileSize + ",status" + status + ",title=" + title + " ,packageName=" + packageName;
    }
}
