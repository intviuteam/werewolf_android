package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * Created by hwx on 13/07/2018.
 */

public class VoiceRedPacketListBean implements Keepable {
    public int page;
    public int pages;
    public List<Package> packages;

    public static class Package implements Keepable{
        public String _id;
        public int count;
        public int left;
        public boolean grabbed;
        public Sender sender;
    }
    public static class Sender implements Keepable{
        public String name;
    }
}
