package orangelab.project.voice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.voice.model.VoiceAdminListMode;

/**
 * Created by hwx on 07/08/2018.
 */

public class VoiceRoomAdminListAdapter extends RecyclerView.Adapter<VoiceRoomAdminListAdapter.AdminListViewHolder> implements Destroyable {

    private List<VoiceAdminListMode> mAdminList = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public VoiceRoomAdminListAdapter(Context context, List<VoiceAdminListMode> adminList) {
        this.mContext = context;
        this.mAdminList = adminList;
    }

    public void updateAdminList(List<VoiceAdminListMode> adminlist) {
        this.mAdminList.clear();
        this.mAdminList.addAll(adminlist);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public AdminListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_voice_room_admin_list, null);
        return new AdminListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdminListViewHolder holder, int position) {
        VoiceAdminListMode user = mAdminList.get(position);
        if (user != null) {
            holder.tvName.setText(user.name);
            if (!TextUtils.isEmpty(user.image)) {
                PicassoUtils.loadImage(mContext, user.image, holder.ivHead);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAdminList.size();
    }

    class AdminListViewHolder extends RecyclerView.ViewHolder {

        ImageView ivHead;
        TextView tvName;
        View btnAdd;
        View btnRemove;
        View btnEdit;

        public AdminListViewHolder(View itemView) {
            super(itemView);

            ivHead = (ImageView) itemView.findViewById(R.id.iv_admin_list_user);
            tvName = (TextView) itemView.findViewById(R.id.tv_admin_list_name);
            btnAdd = itemView.findViewById(R.id.btn_admin_list_add);
            btnRemove = itemView.findViewById(R.id.btn_admin_list_remove);
            btnEdit = itemView.findViewById(R.id.btn_admin_list_edit);

            btnRemove.setOnClickListener(v -> {
                VoiceAdminListMode userItem = mAdminList.get(getPosition());
                if (userItem != null) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(userItem);
                    }
                }
            });
            itemView.setOnClickListener(v -> {
                VoiceAdminListMode userItem = mAdminList.get(getPosition());
                if (userItem != null) {
                    PersonalInfoActivity.Launch(mContext, userItem.id);
                }
            });
        }
    }

    @Override
    public void destroy() {
        mAdminList.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(VoiceAdminListMode mode);
    }
}
