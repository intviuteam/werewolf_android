package orangelab.project.voice.privateroom.holder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.R
import com.androidtoolkit.PicassoUtils
import orangelab.project.common.tool.DialogManager
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.privateroom.PrivateRoomTools
import orangelab.project.voice.privateroom.model.TempPictureMessageItem
import com.toolkit.action.Destroyable
import de.hdodenhof.circleimageview.CircleImageView
import me.drakeet.multitype.ItemViewBinder
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.SafeHandler
import orangelab.project.common.view.VipNameView
import orangelab.project.voice.privateroom.PrivateRoomTempPictureDialog

/**
 *game_werewolf
 *2018/4/1 下午6:11
 *Mystery
 */
class TempPictureMsgItemViewBinder : ItemViewBinder<TempPictureMessageItem, TempPictureMsgItemViewBinder.TempPictureMsgViewHolder>, Destroyable {

    companion object {
        val TAG = "TempPictureItem"
    }

    private var mContext: Context
    private var mDialogType: String
    private var mDialogManager: DialogManager? = null

    private var mSafeHandler: SafeHandler = SafeHandler()

    constructor(mContext: Context, mDialogType: String, mDialogManager: DialogManager?) : super() {
        this.mContext = mContext
        this.mDialogType = mDialogType
        this.mDialogManager = mDialogManager
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): TempPictureMsgViewHolder {
        val view = inflater.inflate(R.layout.layout_private_room_temp_picutre_msg, parent, false)
        val temp = TempPictureMsgViewHolder(view, mSafeHandler)
        return temp
    }

    override fun onBindViewHolder(holder: TempPictureMsgViewHolder, item: TempPictureMessageItem) {

        mSafeHandler.removeCallbacks(holder.mCountDownTimerRunnable)
        holder.mCountDownTimerRunnable.clearMessage()

        /**
         * 初始化点击事件
         */
        holder.enableClick = View.OnClickListener {
            val dialog = PrivateRoomTempPictureDialog(mContext, item.picture, (item.countdown / 1000).toInt(), false)
            mDialogManager?.putAndShow(mDialogType, dialog)
        }
        holder.unEnableClick = null
        holder.name.setText(item.name)
        holder.name.handleVipInfo(item.vipInfo)
        holder.position.setImageResource(PrivateRoomTools.MessageItemPositionIv(item.position))

        /**
         * 如果是自己上传的图片，并且没有100% 则不开始进行倒计时
         */
        if (item.isSelfUpload) {
            if (item.progress == 100) {
                /**
                 * 第一次到达100,开始倒计时
                 */
                item.progress = item.progress + 1
                holder.progress.progress = 100
                item.time = RoomSocketEngineHelper.serverTime()
                holder.mCountDownTimerRunnable.updateItem(item)
                mSafeHandler.postDelaySafely({
                    holder.progress.visibility = View.GONE
                }, 100)
            } else if (item.progress < 100) {
                /**
                 * 未到达100
                 */
                holder.progress.visibility = View.VISIBLE
                holder.message.setText(MessageUtils.getString(R.string.str_private_room_temp_picture_msg_text, (item.countdown / 1000).toString()))
                holder.progress.progress = item.progress
            } else {
                /**
                 * 已经达到过100
                 */
                holder.mCountDownTimerRunnable.updateItem(item)
                holder.progress.visibility = View.GONE
            }
        } else {
            /**
             * 开始倒计时
             */
            holder.progress.visibility = View.GONE
            holder.mCountDownTimerRunnable.updateItem(item)
        }

        /**
         * 展示缩略图
         */
        PicassoUtils.loadImage(holder.image.context, item.picture, 150, 150, holder.image, R.mipmap.ico_private_room_toolbox_sendimage)
    }

    override fun destroy() {
        mDialogManager = null
        mSafeHandler.release()
    }

    class TempPictureMsgViewHolder : RecyclerView.ViewHolder, Destroyable {

        var click: View
        var message: TextView
        var position: CircleImageView
        var name: VipNameView
        var image: ImageView

        var progress: ProgressBar

        var enableClick: View.OnClickListener? = null
        var unEnableClick: View.OnClickListener? = null

        var mCountDownTimerRunnable: CountDownTimerRunnable

        constructor(itemView: View?, safeHandler: SafeHandler) : super(itemView) {
            click = itemView!!.findViewById(R.id.id_click)
            message = itemView!!.findViewById(R.id.tv_item_msg) as TextView
            name = itemView!!.findViewById(R.id.tv_item_name) as VipNameView
            position = itemView!!.findViewById(R.id.iv_item_position) as CircleImageView
            image = itemView!!.findViewById(R.id.temp) as ImageView
            progress = itemView!!.findViewById(R.id.upload_progress) as ProgressBar
            progress.max = 100
            mCountDownTimerRunnable = CountDownTimerRunnable(this, safeHandler)
        }

        override fun destroy() {

        }
    }

    /**
     * 倒计时的Runnable
     */
    class CountDownTimerRunnable : Runnable {

        private var mHolder: TempPictureMsgViewHolder
        private var mItem: TempPictureMessageItem? = null

        private var mSafeHandler: SafeHandler

        private var mRealTime: Long = 0  //实际的时间

        constructor(mHolder: TempPictureMsgViewHolder, mSafeHandler: SafeHandler) {
            this.mHolder = mHolder
            this.mSafeHandler = mSafeHandler
        }

        /**
         * 根据添加时间与倒计时时间，得到实际该显示的时间
         */
        fun updateItem(item: TempPictureMessageItem?) {

            item ?: return

            try {
                mItem = item
                updateTimer()
            } catch (e: Exception) {
                forcefinish()
            }
        }

        private fun updateTimer() {
            try {
                /**
                 * 计算的实际时间
                 */
                val curTime = RoomSocketEngineHelper.serverTime()
                mRealTime = (mItem!!.countdown + mItem!!.time - curTime)
                run()
            } catch (e: Exception) {
                /**
                 * 如果这里除了任何异常则先强制结束
                 */
                forcefinish()
            }
        }

        fun forcefinish() {
            mSafeHandler.removeCallbacks(this)
            mHolder?.message?.setText(MessageUtils.getString(R.string.str_private_room_temp_picture_msg_text_crash))
            mHolder?.click?.setOnClickListener(mHolder?.unEnableClick)
        }

        fun clearMessage() {
            mHolder?.message?.setText("")
        }


        override fun run() {
            /**
             * 倒计时的秒数
             */
            val second = Math.ceil((mRealTime / 1000).toDouble()).toInt()

            /**
             * 处理显示
             */
            if (second > 0) {
                /**
                 * 还有倒计时
                 */
                mHolder?.message?.setText(MessageUtils.getString(R.string.str_private_room_temp_picture_msg_text, second.toString()))
                mHolder?.click?.setOnClickListener(mHolder?.enableClick)
            } else {
                /**
                 * 时间已经过期了
                 */
                mHolder?.message?.setText(MessageUtils.getString(R.string.str_private_room_temp_picture_msg_text_crash))
                mHolder?.click?.setOnClickListener(mHolder?.unEnableClick)
                return
            }

            /**
             * 倒计时时间减1s
             */
            mRealTime -= 1000

            /**
             * 执行下一次计时
             */
            mSafeHandler.postDelaySafely(this, 1000)

        }
    }
}