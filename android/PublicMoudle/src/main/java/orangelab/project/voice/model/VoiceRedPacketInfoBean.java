package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * Created by hwx on 11/07/2018.
 */

public class VoiceRedPacketInfoBean implements Keepable {
    public int left;
    public int count;
    public List<Getter> getters;
    public Sender sender;

    public static class Sender implements Keepable{
        public String name;
    }
    public static class Getter implements Keepable{
        public String image;
        public String name;
        public int count;
        public boolean best;
    }
}
