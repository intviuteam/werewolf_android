package orangelab.project.voice.basic;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by lloydfinch on 27/10/2017.
 * <p>
 * 用于记录员是否首次进入的share
 */

public class ShareIsFirstComeIn {

    private static final String SP_FIRST_COME_IN = "SP_FIRST_COME_IN";

    private static final String SP_FIRST_COME_IN_TAG = "SP_FIRST_COME_IN_TAG";
    private static final String SP_FIRST_OPEN_TAG = "SP_FIRST_OPEN_TAG";
    private static SharedPreferences spFirstComein;

    public static void build(Context context) {
        if (spFirstComein == null) {
            spFirstComein = context.getApplicationContext().getSharedPreferences(SP_FIRST_COME_IN, Context.MODE_PRIVATE);
        }
    }

    //记录进入
    public static void recordComeIn() {
        spFirstComein.edit().putBoolean(SP_FIRST_COME_IN_TAG, true).apply();
    }

    //判断是否进入过
    public static boolean hadComeIn() {
        return spFirstComein.getBoolean(SP_FIRST_COME_IN_TAG, false);
    }

    //记录是否打开过
    public static void recordOpen() {
        spFirstComein.edit().putBoolean(SP_FIRST_OPEN_TAG, true).apply();
    }

    //判断是否打开过
    public static boolean hadOpened() {
        return spFirstComein.getBoolean(SP_FIRST_OPEN_TAG, false);
    }
}
