package orangelab.project.voice.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * game_werewolf
 * 2018/3/27 下午3:49
 * Mystery
 */

public class FlexibleRecycleView extends RecyclerView {

    private int mChangeSize = 200;

    private OnScrollListener mInnerListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE: {
                    //recyclerview已经停止滚动
                    if (mObserver != null) {
                        mObserver.onScrollStopped();
                    }
                }
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    //recyclerview正在被拖拽
                case RecyclerView.SCROLL_STATE_SETTLING: {
                    //recyclerview正在依靠惯性滚动
                    if (mObserver != null) {
                        mObserver.onScrolled();
                    }
                }
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (isSlideToBottom()) {
                if (mObserver != null) {
                    mObserver.onSlideToBottom();
                }
            }
        }
    };

    private FlexibleRecycleViewObserver mObserver;

    public FlexibleRecycleView(Context context) {
        this(context, null);
    }

    public FlexibleRecycleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlexibleRecycleView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        addOnScrollListener(mInnerListener);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (mObserver != null) {
                    mObserver.onTouched();
                }
            }
            break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP: {
                if (mObserver != null) {
                    mObserver.onEndTouch();
                }
            }
            break;
        }
        return super.onTouchEvent(e);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (oldw == 0 || oldh == 0)
            return;

        Log.i("PrivateRoom", "onSizeChanged: w=" + w + ", h=" + h + ",oldw=" + oldw + ",oldh=" + oldh);

        if (oldw != 0 && h - oldh < -mChangeSize) {
            Log.i("PrivateRoom", "onSizeChanged: keyboard show");
            if (mObserver != null) {
                mObserver.onKeyBoardShow();
            }
        }

        if (oldw != 0 && h - oldh > mChangeSize) {
            Log.i("PrivateRoom", "onSizeChanged: keyboard dismiss");
            if (mObserver != null) {
                mObserver.onKeyBoardDismiss();
            }
        }
    }

    public void setObserver(FlexibleRecycleViewObserver observer) {
        mObserver = observer;
    }

    public void moveTo(int position) {
        scrollToPosition(position);
    }

    public void release() {
        removeOnScrollListener(mInnerListener);
    }

    public boolean isSlideToBottom() {
        if (computeVerticalScrollExtent() + computeVerticalScrollOffset() >= computeVerticalScrollRange()) {
            return true;
        } else {
            return false;
        }
    }

    public interface FlexibleRecycleViewObserver {

        void onSlideToBottom();//滚动到底部

        void onScrolled();//正在滚动

        void onScrollStopped();//停止滚动

        void onTouched();//被触摸

        void onEndTouch();//结束触摸

        void onKeyBoardShow();

        void onKeyBoardDismiss();
    }
}
