package orangelab.project.voice.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.view.viewpagergroup.GridViewPagerGroup;
import com.androidtoolkit.view.viewpagergroup.GridViewPagerGroupHolderCreator;
import com.toolkit.action.Destroyable;

import java.util.Locale;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.smallgame.SmallGameManiFest;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.manager.SmallGameListManager;

public class SmallGameViewPagerGroup extends GridViewPagerGroup<SmallGameManiFest.SmallGame, SmallGameViewHolder> implements Destroyable {

    private Context mContext;
    private OnItemClickedListener mOnItemClickedListener;

    public SmallGameViewPagerGroup(@NonNull Context context) {
        this(context, null);
    }

    public SmallGameViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SmallGameViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

        setViewPagerGroupHolderCreator(new GridViewPagerGroupHolderCreator<SmallGameManiFest.SmallGame, SmallGameViewHolder>() {
            @Override
            public SmallGameViewHolder createViewHolder(View view) {
                SmallGameViewHolder holder = new SmallGameViewHolder();
                holder.mImageView = (ImageView) view.findViewById(R.id.iv_voice_small_game);
                holder.mTextView = (TextView) view.findViewById(R.id.tv_voice_game_name);
                return holder;
            }

            @Override
            public void onBindViewHolder(SmallGameManiFest.SmallGame smallGame, SmallGameViewHolder holder) {
                if (smallGame != null && holder != null) {
                    if (Locale.getDefault().toString().contains("CN")) {
                        holder.mTextView.setText(smallGame.mark.j);
                    } else {
                        holder.mTextView.setText(smallGame.mark.f);
                    }
                    holder.mImageView.setImageResource(ApkResourceManager.GetApkImageResource(smallGame.thumbnail));
                    holder.mImageView.setOnClickListener((v) -> {
                        if (mOnItemClickedListener != null) {
                            mOnItemClickedListener.onItemClicked(smallGame);
                        }
                    });
                }
            }

            @Override
            public void onDestroy() {

            }

            @Override
            public View createViewHolderView() {
                return View.inflate(mContext, R.layout.item_small_game, null);
            }
        });
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        mOnItemClickedListener = onItemClickedListener;
    }

    @Override
    public void destroy() {
    }

    public interface OnItemClickedListener {
        void onItemClicked(SmallGameManiFest.SmallGame smallGame);
    }
}
