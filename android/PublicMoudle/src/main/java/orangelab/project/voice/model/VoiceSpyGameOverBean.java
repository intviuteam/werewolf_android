package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lloydfinch on 06/12/2017.
 */

public class VoiceSpyGameOverBean implements Serializable, Keepable {

    public String win_type;//谁胜利
    public Roles roles;//阵营分配
    public List<Integer> winners;
    public Experience experience;//经验问题
    public VoiceWords words;//返回的词语

    public static class Roles {
        public List<RoleInfo> undercover;
        public List<RoleInfo> people;
    }

    public static class RoleInfo implements Serializable {
        public int position;
        public String role;
        public String detail;
        public String status;
    }

    public static class Experience {
        public int win;
        public int lose;
        public int espace;
    }


}