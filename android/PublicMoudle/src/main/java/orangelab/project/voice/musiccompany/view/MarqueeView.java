package orangelab.project.voice.musiccompany.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

public class MarqueeView extends android.support.v7.widget.AppCompatTextView {

    public MarqueeView(Context context) {
        super(context);
    }

    public MarqueeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean isFocused() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {

    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {

    }
}
