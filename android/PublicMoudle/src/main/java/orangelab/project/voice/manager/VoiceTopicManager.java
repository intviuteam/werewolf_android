package orangelab.project.voice.manager;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.project.voice.api.VoiceApiManager;
import orangelab.project.voice.config.VoiceRoomConfig;

/**
 * Created by lloydfinch on 18/10/2017.
 */

public class VoiceTopicManager implements Destroyable {

    private static final String TAG = "VoiceTopicManager";

    private static volatile VoiceTopicManager mInstance;
    private static Params topic = null;

    private VoiceTopicManager() {
    }

    public static VoiceTopicManager getInstance() {
        if (mInstance == null) {
            synchronized (VoiceTopicManager.class) {
                if (mInstance == null) {
                    mInstance = new VoiceTopicManager();
                }
            }
        }
        return mInstance;
    }

    public Params getTopic() {
        return topic;
    }

    public void init() {
        initReceiveTopicTypeListener();
    }

    public void initTopic(String value) {
        try {
            topic = GsonHelper.parseJsonToBean(value, Params.class);
            if (topic != null) {
                VoiceRoomConfig.setIsSupportSW(topic.isMini_mode());
                VoiceRoomConfig.setRoomPrice(topic.room_price);
                if (topic.active != null) {
                    VoiceRoomConfig.setIsSupportChangeSeat(topic.active.enable_change);
                } else {
                    VoiceRoomConfig.setIsSupportChangeSeat(false);
                }
                VoiceRoomConfig.setMaxPacket(topic.red_packet.max_user);
                VoiceRoomConfig.setMinPacket(topic.red_packet.min_user);
                VoiceRoomConfig.setMinDiamond(topic.red_packet.min_money);
                VoiceRoomConfig.setRemainTime(topic.red_packet.remain_time);
                //这里不再提供purchase_room
                //VoiceRoomConfig.setIsSupportDeed(topic.isPurchase_room());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getTopicList(String roomId) {
        VoiceApiManager.GetTopicList(roomId, (response, e) -> {
            if (e == null) {
                if (!TextUtils.isEmpty(response)) {
                    initTopic(response);
                }
            }
        });
    }

    private void initReceiveTopicTypeListener() {

    }

    public int findTopicPositionByType(String type) {
        PLog.e(TAG, "type: " + type);
        int position = 0;

        if (topic == null) {
            return position;
        }
        if (topic.audio_room_types == null) {
            return position;
        }
        for (int i = 0; i < topic.audio_room_types.size(); i++) {
            Params.VoiceTopic topic = VoiceTopicManager.topic.audio_room_types.get(i);
            if (topic != null && TextUtils.equals(topic.getType(), type)) {
                return i;
            }
        }
        return position;
    }

    public String findTopicDesByType(String type) {
        String des = "";
        if (TextUtils.isEmpty(type)) {
            return des;
        }
        for (Params.VoiceTopic t : topic.audio_room_types) {
            if (TextUtils.equals(t.getType(), type)) {
                return t.getDescription();
            }
        }
        PLog.e(TAG, "des:" + des);
        return des;
    }

    public String findTopicBgUrlByType(String type) {
        String url = "";
        if (topic == null) {
            return url;
        }
        for (Params.VoiceTopic t : topic.audio_room_types) {
            if (TextUtils.equals(t.getType(), type)) {
                return t.getImageUrl();
            }
        }
        return url;
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
    }

    public static class TopicBean {
        public int code;
        public Params data;

    }

    public static class Params implements Keepable {
        private boolean mini_mode = false;
        private boolean purchase_room = false;
        private int room_price = 0;
        private List<VoiceTopic> audio_room_types;
        private Active active;
        public String room_reset_duration = "7天";
        private Packet red_packet;
        public KTV ktv;

        public boolean isMini_mode() {
            return mini_mode;
        }

        public boolean isPurchase_room() {
            return purchase_room;
        }

        public int getRoom_price() {
            return room_price;
        }

        public List<VoiceTopic> getTopics() {
            return audio_room_types;
        }

        public static class VoiceTopic implements Keepable {
            private String k;
            private String t;
            private String i;
            private String ai;

            public VoiceTopic() {
                super();
            }

            public VoiceTopic(String k, String t, String i, String ai) {
                super();
                this.k = k;
                this.t = t;
                this.i = i;
                this.ai = ai;
            }

            public String getType() {
                return k;
            }

            public String getDescription() {
                return t;
            }

            public String getImageUrl() {
                return i;
            }

            //小语app首页图标
            public String getAi() {
                return ai;
            }
        }

        public List<VoiceTopic> getAudio_room_types() {
            return audio_room_types;
        }

        public Active getActive() {
            return active;
        }

        /**
         * 扩麦相关
         */
        public static class Active implements Keepable {
            public boolean enable_change;        //是否可切换麦位
            public List<Integer> active_options; //可选择麦位数
        }

        static class Packet implements Keepable {
            int min_money;
            int min_user;
            int max_user;
            int remain_time;
        }

        /**
         * ktv 相关
         */
        public static class KTV {
            public int max_count = 30;      //最大可点歌曲数
            public int per_user_count = 5;  //没人最大可点歌曲数
        }
    }
}
