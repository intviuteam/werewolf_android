package orangelab.project.voice.dialog;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.dialog.ReportDialog;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.tool.WrapContentGridLayoutManager;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.FlowLayout;
import orangelab.project.common.view.HeadDecorView;
import orangelab.project.common.view.VipNameView;
import orangelab.project.voice.adapter.VoiceSeatPhotoAdapter;
import orangelab.project.voice.basic.ShareIsFirstComeIn;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;
import orangelab.project.voice.utils.PositionHelper;

/**
 * Created by lloydfinch on 11/10/2017.
 */

public class VoiceSeatDialog extends SafeDialog {


    private static final String TAG = "VoiceSeatDialog";

    private View llUserInfoContainer;
    private View llUserReportContainer;
    private View llButtonContainer;
    private View llSeatControlContainer;
    private View rlBtnMasterFreeContainer;

    private View llCarryUpSeat;
    private View llBanSeat;
    private View llUnBanSeat;
    private View llFreeStyle;
    private View llFreeContent;
    private View llUnFreeStyle;
    private View llDownSeat;
    private View llPersonalInfo;
    private View llSetDownSeat;
    private View llLockSeat;
    private View llUnLockSeat;
    private View llSendGift;
    private View llAddFriend;
    private View llKickOut;
    private View llCancel;
    private Button btnMasterFree;
    private View btnMasterUpFree;
    private View llOpenKtv;
    private View llCloseKtv;

    private View btnAddBlackList;
    private View btnReport;
    private HeadDecorView mHeadView;
    private VipNameView userName;
    private TextView userLevel;
    private FlowLayout userRoles;
    private TextView userPopularity;
    private TextView userRate;
    private TextView userWins;
    private TextView userLoses;
    private TextView userEscapeRate;

    private View viewOpenFreeTag;
    private View viewBanMicTag;
    private View viewMasterFreeTag;

    private TextView tvUserId;

    private RelativeLayout rlPhotoContainer;
    private RecyclerView listPhotos;

    private View.OnClickListener onCarryUpSeatListener;//抱用户上麦
    private View.OnClickListener onDownSeatListener;//下麦
    private View.OnClickListener onBanSeatListener;//禁麦
    private View.OnClickListener onUnBanSeatListener;//解除禁麦
    private View.OnClickListener onFreeStyleListener;//自由模式
    private View.OnClickListener onUnFreeStyleListener;//解除自由模式
    private View.OnClickListener onPersonalInfoListener;//查看个人信息
    private View.OnClickListener onSetDownSeatListener;//设为旁听
    private View.OnClickListener onLockSeatListener;//加锁
    private View.OnClickListener onUnLockSeatListener;//解锁
    private View.OnClickListener onSendGiftListener;//送礼物
    private View.OnClickListener onAddFriendListener;//加好友
    private View.OnClickListener onKickOutListener;//踢人
    private View.OnClickListener onCancelClickListener;//取消
    private View.OnClickListener onOpenKtvListener;//打开伴奏
    private View.OnClickListener onCloseKtvListener;//关闭伴奏

    private Context mContext;
    private boolean isMaster;//是否是房主
    private boolean isLocked;
    private boolean isSeated;
    private boolean isBanMic;
    private boolean isFreeStyle;
    private boolean isSelf;
    private boolean isKtv; //当前玩家是否是ktv模式
    private int position;//点击的chair的位置

    private String mUserId;
    private String mUserName;

    private void init() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_user_voice, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llUserInfoContainer = contentView.findViewById(R.id.user_info_container);
        llUserReportContainer = contentView.findViewById(R.id.ll_user_report);
        llButtonContainer = contentView.findViewById(R.id.ll_button_container);
        llSeatControlContainer = contentView.findViewById(R.id.ll_seat_controller);
        rlBtnMasterFreeContainer = contentView.findViewById(R.id.btn_master_free_container);

        btnAddBlackList = contentView.findViewById(R.id.tv_user_add_black_list);
        btnReport = contentView.findViewById(R.id.ll_report_user);
        llCarryUpSeat = contentView.findViewById(R.id.voice_rl_force_seat);
        llBanSeat = contentView.findViewById(R.id.voice_rl_ban_seat);
        llUnBanSeat = contentView.findViewById(R.id.voice_rl_un_ban_seat_mic);
        llFreeStyle = contentView.findViewById(R.id.voice_rl_free_style);
        llFreeContent = contentView.findViewById(R.id.voice_ll_free_content);
        llUnFreeStyle = contentView.findViewById(R.id.voice_rl_un_free_style);
        llDownSeat = contentView.findViewById(R.id.btn_down_seat);
        llPersonalInfo = contentView.findViewById(R.id.rl_user_detail_container);
        llSetDownSeat = contentView.findViewById(R.id.voice_rl_set_down_seat);
        llLockSeat = contentView.findViewById(R.id.voice_rl_close_seat);
        llUnLockSeat = contentView.findViewById(R.id.voice_rl_unlock_seat);
        llSendGift = contentView.findViewById(R.id.btn_send_gift);
        llAddFriend = contentView.findViewById(R.id.btn_add_friend);
        llKickOut = contentView.findViewById(R.id.voice_rl_kick_out);
        llCancel = contentView.findViewById(R.id.btn_cancel);
        btnMasterFree = (Button) contentView.findViewById(R.id.btn_master_open_free);
        btnMasterUpFree = contentView.findViewById(R.id.btn_master_close_free);
        llOpenKtv = contentView.findViewById(R.id.voice_rl_open_ktv);
        llCloseKtv = contentView.findViewById(R.id.voice_rl_close_ktv);

        mHeadView = (HeadDecorView) findViewById(R.id.user_head_view);
        userName = (VipNameView) findViewById(R.id.voice_user_name);
        userLevel = (TextView) findViewById(R.id.voice_user_level);
        userPopularity = (TextView) findViewById(R.id.voice_user_popularity);
        userRate = (TextView) findViewById(R.id.user_win_rate);
        userWins = (TextView) findViewById(R.id.wins);
        userLoses = (TextView) findViewById(R.id.lose);
        userEscapeRate = (TextView) findViewById(R.id.user_escape_rate);
        userRoles = (FlowLayout) findViewById(R.id.id_user_detail_flowlayout);
        userRoles.setVisibility(View.GONE);
        tvUserId = (TextView) findViewById(R.id.tv_user_id);
        tvUserId.setVisibility(View.GONE);

        viewOpenFreeTag = findViewById(R.id.view_tag_open_free);
        viewBanMicTag = findViewById(R.id.view_tag_ban_mic);
        viewMasterFreeTag = findViewById(R.id.view_tag_open_free_master);

        //照片墙
        rlPhotoContainer = (RelativeLayout) findViewById(R.id.rl_photo_container);
        listPhotos = (RecyclerView) findViewById(R.id.rl_photos_list);
        //拦截滑动
        WrapContentGridLayoutManager layoutManager = new WrapContentGridLayoutManager(mContext, 4) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        //layoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
        listPhotos.setLayoutManager(layoutManager);
        //默认不可见
        rlPhotoContainer.setVisibility(View.GONE);

        llUserReportContainer.setVisibility(View.GONE);
        //llButtonContainer.setVisibility(View.GONE);
        llAddFriend.setVisibility(View.GONE);

        ShareIsFirstComeIn.build(mContext);
        if (ShareIsFirstComeIn.hadOpened()) {
            viewOpenFreeTag.setVisibility(View.GONE);
            viewBanMicTag.setVisibility(View.GONE);
            viewMasterFreeTag.setVisibility(View.GONE);
        } else {
            viewOpenFreeTag.setVisibility(View.VISIBLE);
            viewBanMicTag.setVisibility(View.VISIBLE);
            viewMasterFreeTag.setVisibility(View.VISIBLE);
            ShareIsFirstComeIn.recordOpen();
        }
        RxToolKit.Emit(new VoiceEvent.FirstComeIn());

        initDialogParams();
        initListener();
    }

    private void initDialogParams() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnReport.setOnClickListener((v -> {
            ReportDialog reportDialog = new ReportDialog(getContext(), mUserId, mUserName);
            reportDialog.show();
        }));

        btnAddBlackList.setOnClickListener(v -> {
            addBlackToList();
        });
    }

    public void setMaster(boolean master) {
        isMaster = master;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public void setSeated(boolean seated) {
        isSeated = seated;
    }

    public void setBanMic(boolean banMic) {
        isBanMic = banMic;
    }

    public void setFreeStyle(boolean freeStyle) {
        isFreeStyle = freeStyle;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setKtv(boolean ktv) {
        isKtv = ktv;
    }

    public void setOnCarryUpSeatListener(View.OnClickListener onCarryUpSeatListener) {
        this.onCarryUpSeatListener = onCarryUpSeatListener;
        llCarryUpSeat.setOnClickListener(onCarryUpSeatListener);
    }

    public void setOnDownSeatListener(View.OnClickListener onDownSeatListener) {
        this.onDownSeatListener = onDownSeatListener;
        llDownSeat.setOnClickListener((view) -> {
            //ktv房间并且自己正在唱歌,弹出提示
            if (VoiceRoomConfig.isIsKTV() && PositionHelper.isSinger()) {
                VoiceLeaveDialog dialog = new VoiceLeaveDialog(mContext,
                        MessageUtils.getString(R.string.str_music_tip),
                        MessageUtils.getString(R.string.str_music_singer_warn_down_seat));
                dialog.setConfirmClickListener((v) -> {
                    this.onDownSeatListener.onClick(v);
                    if (VoiceRoomConfig.isIsKTV() && PositionHelper.isSinger()) {
                        MusicSocketHelper.INSTANCE.sendStopSing();
                    }
                    dialog.dismiss();
                });
                dialog.negative(true);
                dialog.show();
            } else {
                onDownSeatListener.onClick(view);
            }
        });
    }

    public void setOnPersonalInfoListener(View.OnClickListener onPersonalInfoListener) {
        this.onPersonalInfoListener = onPersonalInfoListener;
        llPersonalInfo.setOnClickListener(onPersonalInfoListener);
    }

    public void setOnSetDownSeatListener(View.OnClickListener onSetDownSeatListener) {
        this.onSetDownSeatListener = onSetDownSeatListener;
        llSetDownSeat.setOnClickListener(onSetDownSeatListener);
    }

    public void setOnLockSeatListener(View.OnClickListener onLockSeatListener) {
        this.onLockSeatListener = onLockSeatListener;
        llLockSeat.setOnClickListener(onLockSeatListener);
    }

    public void setOnUnLockSeatListener(View.OnClickListener onUnLockSeatListener) {
        this.onUnLockSeatListener = onUnLockSeatListener;
        llUnLockSeat.setOnClickListener(onUnLockSeatListener);
    }

    public void setOnSendGiftListener(View.OnClickListener onSendGiftListener) {
        this.onSendGiftListener = onSendGiftListener;
        llSendGift.setOnClickListener(onSendGiftListener);
    }

    public void setOnAddFriendListener(View.OnClickListener onAddFriendListener) {
        this.onAddFriendListener = onAddFriendListener;
        llAddFriend.setOnClickListener(onAddFriendListener);
    }

    public void setOnKickOutListener(View.OnClickListener onKickOutListener) {
        this.onKickOutListener = onKickOutListener;
        llKickOut.setOnClickListener(onKickOutListener);
    }

    public void setOnCancelClickListener(View.OnClickListener onCancelClickListener) {
        this.onCancelClickListener = onCancelClickListener;
        llCancel.setOnClickListener(onCancelClickListener);
    }

    public void setOnBanSeatListener(View.OnClickListener onBanSeatListener) {
        this.onBanSeatListener = onBanSeatListener;
        llBanSeat.setOnClickListener(onBanSeatListener);
    }

    public void setOnUnBanSeatListener(View.OnClickListener onUnBanSeatListener) {
        this.onUnBanSeatListener = onUnBanSeatListener;
        llUnBanSeat.setOnClickListener(onUnBanSeatListener);
    }

    public void setOnFreeStyleListener(View.OnClickListener onFreeStyleListener) {
        this.onFreeStyleListener = onFreeStyleListener;
        llFreeContent.setOnClickListener(onFreeStyleListener);
        btnMasterFree.setOnClickListener(onFreeStyleListener);
    }

    public void setOnUnFreeStyleListener(View.OnClickListener onUnFreeStyleListener) {
        this.onUnFreeStyleListener = onUnFreeStyleListener;
        llUnFreeStyle.setOnClickListener(onUnFreeStyleListener);
        btnMasterUpFree.setOnClickListener(onUnFreeStyleListener);
    }

    public void setOnOpenKtvListener(View.OnClickListener onOpenKtvListener) {
        this.onOpenKtvListener = onOpenKtvListener;
        llOpenKtv.setOnClickListener(onOpenKtvListener);
    }

    public void setOnCloseKtvListener(View.OnClickListener onCloseKtvListener) {
        this.onCloseKtvListener = onCloseKtvListener;
        llCloseKtv.setOnClickListener(onCloseKtvListener);
    }

    public VoiceSeatDialog build() {
        llPersonalInfo.setVisibility(View.GONE);
        //llSendGift.setVisibility(View.GONE);
        llAddFriend.setVisibility(View.GONE);
        llSetDownSeat.setVisibility(View.GONE);

        llBanSeat.setVisibility(View.GONE);
        llUnBanSeat.setVisibility(View.GONE);
        llFreeStyle.setVisibility(View.GONE);
        llUnFreeStyle.setVisibility(View.GONE);
        rlBtnMasterFreeContainer.setVisibility(View.GONE);
        btnMasterUpFree.setVisibility(View.GONE);
        llOpenKtv.setVisibility(View.GONE);
        llCloseKtv.setVisibility(View.GONE);
        if (isMaster) {
            llDownSeat.setVisibility(View.GONE);
            if (isLocked) {
                llUnLockSeat.setVisibility(View.VISIBLE);
                llLockSeat.setVisibility(View.GONE);
                //llCarryUpSeat.setVisibility(View.GONE);//新需求:加锁的座位也可以"抱用户上麦"
                llKickOut.setVisibility(View.GONE);
            } else {
                llUnLockSeat.setVisibility(View.GONE);
                llLockSeat.setVisibility(View.VISIBLE);
                if (isSeated) {
                    llPersonalInfo.setVisibility(View.VISIBLE);
                    llCarryUpSeat.setVisibility(View.GONE);
                    if (isSelf) {
                        llLockSeat.setVisibility(View.GONE);
                        llKickOut.setVisibility(View.GONE);
                        llSeatControlContainer.setVisibility(View.GONE);

                        if (isFreeStyle) {
                            rlBtnMasterFreeContainer.setVisibility(View.GONE);
                            btnMasterUpFree.setVisibility(View.VISIBLE);
                        } else {
                            rlBtnMasterFreeContainer.setVisibility(View.VISIBLE);
                            btnMasterUpFree.setVisibility(View.GONE);
                        }
                    } else {
                        llLockSeat.setVisibility(View.VISIBLE);
                        llKickOut.setVisibility(View.VISIBLE);
                        llSendGift.setVisibility(View.VISIBLE);
                        llAddFriend.setVisibility(View.VISIBLE);
                        llSetDownSeat.setVisibility(View.VISIBLE);
                        llSeatControlContainer.setVisibility(View.VISIBLE);

                        if (isBanMic) {
                            llBanSeat.setVisibility(View.GONE);
                            llUnBanSeat.setVisibility(View.VISIBLE);
                        } else {
                            llBanSeat.setVisibility(View.VISIBLE);
                            llUnBanSeat.setVisibility(View.GONE);
                        }
                        if (isFreeStyle) {
                            llFreeStyle.setVisibility(View.GONE);
                            llUnFreeStyle.setVisibility(View.VISIBLE);
                        } else {
                            if (VoiceRoomConfig.isIsKTV()) {
                                llFreeStyle.setVisibility(View.GONE);
                                if (isKtv) {
                                    llCloseKtv.setVisibility(View.VISIBLE);
                                    llOpenKtv.setVisibility(View.GONE);
                                } else {
                                    llOpenKtv.setVisibility(View.VISIBLE);
                                    llCloseKtv.setVisibility(View.GONE);
                                }
                            } else {
                                llOpenKtv.setVisibility(View.GONE);
                                llCloseKtv.setVisibility(View.GONE);
                                llFreeStyle.setVisibility(View.VISIBLE);
                            }
                            llUnFreeStyle.setVisibility(View.GONE);
                        }
                    }
                } else {
                    llPersonalInfo.setVisibility(View.GONE);
                    llKickOut.setVisibility(View.GONE);
                    llCarryUpSeat.setVisibility(View.VISIBLE);
                    llOpenKtv.setVisibility(View.GONE);
                    llCloseKtv.setVisibility(View.GONE);
                }
            }
        } else {
            llKickOut.setVisibility(View.GONE);
            llCarryUpSeat.setVisibility(View.GONE);
            llLockSeat.setVisibility(View.GONE);
            llBanSeat.setVisibility(View.GONE);
            llUnLockSeat.setVisibility(View.GONE);
            llSeatControlContainer.setVisibility(View.GONE);
            llOpenKtv.setVisibility(View.GONE);
            llCloseKtv.setVisibility(View.GONE);
            if (isLocked) {
                llDownSeat.setVisibility(View.GONE);
            } else {
                if (isSeated) {
                    llPersonalInfo.setVisibility(View.VISIBLE);
                    if (isSelf) {
                        llDownSeat.setVisibility(View.VISIBLE);
                    } else {
                        llDownSeat.setVisibility(View.GONE);
                        llSendGift.setVisibility(View.VISIBLE);
                        llAddFriend.setVisibility(View.VISIBLE);
                    }
                } else {
                    llDownSeat.setVisibility(View.GONE);
                }
            }
        }

        if (!isSeated) {
            llUserInfoContainer.setVisibility(View.GONE);
        } else {
            llUserInfoContainer.setVisibility(View.VISIBLE);
        }

        //"谁是卧底"房并且游戏已经开始，就不能执行麦位相关操作
        if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
            llSeatControlContainer.setVisibility(View.GONE);
            llDownSeat.setVisibility(View.GONE);
        }

        //房主视角的"自由模式"按钮转换
        if (VoiceRoomConfig.isIsKTV()) {
            if (isKtv) {
                toCloseKtvBtn();
            } else {
                toOpenKtvBtn();
            }
        } else {
            toFreeBtn();
        }

        return this;
    }

    /**
     * 自由模式按钮
     */
    private void toFreeBtn() {
        btnMasterFree.setText(R.string.str_open_free_style);
        btnMasterFree.setOnClickListener(onFreeStyleListener);
    }

    /**
     * 开伴奏按钮
     */
    private void toOpenKtvBtn() {
        btnMasterFree.setText(R.string.str_music_open_ktv);
        btnMasterFree.setOnClickListener(onOpenKtvListener);
    }

    /**
     * 关伴奏权限
     */
    private void toCloseKtvBtn() {
        btnMasterFree.setText(R.string.str_music_close_ktv);
        btnMasterFree.setOnClickListener(onCloseKtvListener);
    }

    @Override
    public void show() {
        super.show();
        PLog.e("Status--", isMaster + "," + isLocked + "," + isSeated + "," + isSelf);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public VoiceSeatDialog(@NonNull Context context, String userId) {
        this(context, R.style.radius_dialog);
        mUserId = userId;
        getUserInfo(userId);
    }

    public VoiceSeatDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
    }

    @Override
    protected void release() {

    }

    public void destroy() {
        release();
    }


    private void getUserInfo(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        try {
            ApiManager.GetUserInfoApi(userId, (userInfoResult, e)
                    -> runSafely(() -> initData(userInfoResult)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData(UserInfoResult result) {

        if (result != null) {

            PLog.e(TAG, "UserInfoResult = " + result.toString());
            mUserName = result.name;
            if (!TextUtils.isEmpty(result.image)) {
                PicassoUtils.loadImage(mContext, result.image, mHeadView.getRealHeadImage(), R.mipmap.default_head);
            }
            userName.setText(result.name);
            userName.handleVipInfo(result.vipInfo, Color.BLACK);
            mHeadView.setUserSex(result.sex);
            mHeadView.setHeadDecorate(result.avatar_box);
            tvUserId.setVisibility(View.VISIBLE);
            tvUserId.setText("ID:" + result.uid);
            userLevel.setText(
                    MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level))
            );
            userPopularity.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));

            float total = (result.game.lose + result.game.win + result.game.escape);
            float rate = total > 0 ? (result.game.win / (total)) * 100f : 0;
            float escapeRate = total > 0 ? (result.game.escape / (total)) * 100f : 0;
            String rateString = new BigDecimal(rate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            String escapeRateString = new BigDecimal(escapeRate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
            userRate.setText(rateString + "%");
            userEscapeRate.setText(escapeRateString + "%");
            userWins.setText(Integer.toString(result.game.win));
            userLoses.setText(Integer.toString(result.game.lose));
            try {
                List<String> roleData = new ArrayList<>();
                if (result.role != null && result.role.customs != null && result.role.customs.size() > 0) {
                    roleData.addAll(result.role.customs);
                }
                if (roleData.size() > 0) {
                    userRoles.setVisibility(View.VISIBLE);
                    userRoles.addTextViewsWithVipInfo(roleData, Color.parseColor("#2d1c4c"), 10, true, result.vipInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //照片墙问题
            if (result.photos != null && result.photos.size() > 0) {
                rlPhotoContainer.setVisibility(View.VISIBLE);
                listPhotos.setAdapter(new VoiceSeatPhotoAdapter(mContext, result.photos));
            } else {
                rlPhotoContainer.setVisibility(View.GONE);
            }

            controlView(result);
        }
    }

    private void controlView(UserInfoResult result) {
        if (isSelf) {
            //llButtonContainer.setVisibility(View.GONE);
            llAddFriend.setVisibility(View.GONE);
        } else {
            //llButtonContainer.setVisibility(View.VISIBLE);
            llAddFriend.setVisibility(View.VISIBLE);
        }

        if (isSelf || Utils.checkUserIdIsTourist(mUserId)) {
            llUserReportContainer.setVisibility(View.GONE);
        } else {
            llUserReportContainer.setVisibility(View.VISIBLE);
        }

        if (Utils.checkUserIdIsTourist(GlobalUserState.getGlobalState().getUserId())) {
            btnAddBlackList.setVisibility(View.INVISIBLE);
        }

        if (result.is_friend) {
            llAddFriend.setVisibility(View.GONE);
        }

        if (result.isBlock) {
            btnAddBlackList.setVisibility(View.GONE);
        }
    }

    private void addBlackToList() {
        ApiManager.AddToBlackApi(mUserId, (result, e) -> runSafely(() -> {
            if (e == null) {
                BlackListManager.Add(mUserId);
                UnifiedBridgeHelper.SendBlackListAddAction(mUserId);
            }
            RunnableFactory.createMsgToastRunnable(result).run();
        }));
    }
}
