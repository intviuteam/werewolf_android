package orangelab.project.voice.privateroom.view

import android.graphics.PointF
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.R

/**
 *game_werewolf
 *2018/4/1 下午12:01
 *Mystery
 */
class PrivateRoomToolBoxViewImpl : PrivateRoomToolBoxView, View.OnClickListener {

    private var mToolBoxContainer: View

    private var mToolBoxPaperPlaneContainer: View
    private var mToolBoxPaperPlaneIco: ImageView

    private var mToolBoxSendImageContainer: View
    private var mToolBoxSendImageIco: ImageView
    private var mToolBoxSendImageFuncOpen = false // 此功能是否打开

    private var mToolBoxChangeBackGroundContainer: View
    private var mToolBoxChangeBackGroundIcon: ImageView

    private var mToolBoxSwitch: ImageView

    private var isClosed = true

    private var mToolBoxItemContainer: View
    private var mToolBoxItemIcon: ImageView
    private var mToolBoxItemText: TextView

    private var mObserver: PrivateRoomToolBoxViewObserver? = null

    private var isMaster = false
    private var isUpSeat = false

    private var mPoint: PointF? = null
    private var mContainerWidth = 0
    private var mContainerHeight = 0

    constructor(rootView: View, observer: PrivateRoomToolBoxViewObserver?) {

        mObserver = observer

        mToolBoxContainer = rootView.findViewById(R.id.toolbox)

        mToolBoxPaperPlaneContainer = rootView.findViewById(R.id.toolbox_paperplane)
        mToolBoxPaperPlaneIco = rootView.findViewById<ImageView>(R.id.toolbox_paperplane_ico) as ImageView
        mToolBoxPaperPlaneContainer.setOnClickListener(this)

        mToolBoxSendImageContainer = rootView.findViewById(R.id.toolbox_sendimage)
        mToolBoxSendImageIco = rootView.findViewById<ImageView>(R.id.toolbox_sendimage_ico) as ImageView
        mToolBoxSendImageContainer.setOnClickListener(this)

        mToolBoxChangeBackGroundContainer = rootView.findViewById(R.id.toolbox_background)
        mToolBoxChangeBackGroundIcon = rootView.findViewById<ImageView>(R.id.toolbox_background_ico) as ImageView
        mToolBoxChangeBackGroundContainer.setOnClickListener(this)

        mToolBoxItemContainer = rootView.findViewById(R.id.toolbox_item)
        mToolBoxItemText = rootView.findViewById<TextView>(R.id.toolbox_item_text) as TextView
        mToolBoxItemIcon = rootView.findViewById<ImageView>(R.id.toolbox_item_ico) as ImageView
        mToolBoxItemContainer.setOnClickListener(this)

        mToolBoxSwitch = rootView.findViewById<ImageView>(R.id.toolbox_switch) as ImageView
        mToolBoxSwitch.setOnClickListener(this)

        mToolBoxContainer.post {
            val intArray = IntArray(2)
            mToolBoxContainer.getLocationOnScreen(intArray)
            mPoint = PointF(intArray[0].toFloat(), intArray[1].toFloat())

            mContainerWidth = mToolBoxContainer.width
            mContainerHeight = mToolBoxContainer.height
        }
    }

    override fun closeToolBox() {
        if (!isClosed) {
            mToolBoxContainer.visibility = View.INVISIBLE

//            mToolBoxPaperPlaneContainer.visibility = View.GONE
//            mToolBoxSendImageContainer.visibility = View.GONE
//            mToolBoxChangeBackGroundContainer.visibility = View.GONE

            mToolBoxSwitch.visibility = View.VISIBLE
            mToolBoxSwitch.setImageResource(R.drawable.drawable_private_room_toolbox_switch)
            isClosed = true
        }
    }

    override fun showToolBox() {
//        mToolBoxContainer.visibility = View.VISIBLE
        mToolBoxSwitch.visibility = View.VISIBLE
        mToolBoxSwitch.setImageResource(R.drawable.drawable_private_room_toolbox_switch)
    }

    override fun openToolBox() {
        if (isClosed) {
            mToolBoxContainer.visibility = View.VISIBLE

//            if (isMaster) {
//                mToolBoxPaperPlaneContainer.visibility = View.VISIBLE
//                mToolBoxSendImageContainer.visibility = View.VISIBLE
//                mToolBoxChangeBackGroundContainer.visibility = View.VISIBLE
//            }
//
//            if (isUpSeat) {
//                mToolBoxPaperPlaneContainer.visibility = View.VISIBLE
//                mToolBoxSendImageContainer.visibility = View.GONE
//                mToolBoxChangeBackGroundContainer.visibility = View.GONE
//            }

            mToolBoxSwitch.visibility = View.GONE
            mToolBoxSwitch.setImageResource(R.drawable.drawable_private_room_toolbox_switch)

            isClosed = false
        }
    }

    override fun showCloseSendTempImage() {
//        mToolBoxSendImageFuncOpen = true
//        mToolBoxSendImageIco.setImageResource(R.mipmap.ico_private_room_toobox_close_func)
    }

    override fun showOpenSendTempImage() {
//        mToolBoxSendImageFuncOpen = false
//        mToolBoxSendImageIco.setImageResource(R.mipmap.ico_private_room_toolbox_sendimage)
    }

    private fun sendOpenSendImage() {
//        RoomSocketEngineHelper.sendEnableUploadImage(true)
    }

    private fun sendCloseSendImage() {
//        RoomSocketEngineHelper.sendEnableUploadImage(false)
    }

    override fun goneToolBox() {
        mToolBoxContainer.visibility = View.INVISIBLE
        mToolBoxSwitch.visibility = View.GONE
    }

    override fun turnIntoUpSeatToolBox() {
        isUpSeat = true
        isMaster = false
        mToolBoxChangeBackGroundContainer.visibility = View.GONE
    }

    override fun turnIntoMasterToolBox() {
        isUpSeat = false
        isMaster = true
        mToolBoxChangeBackGroundContainer.visibility = View.VISIBLE
    }

    override fun turnIntoOutSeatToolBox() {
        isUpSeat = false
        isMaster = false
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {


        ev ?: return false

        mPoint ?: return false

        if (ev?.action == MotionEvent.ACTION_DOWN) {
            /**
             * 点击事件
             */
            val y = ev?.y

            /**
             * 判断点击位置是否在Container范围内
             */
            if (y >= mPoint!!.y && y <= mPoint!!.y + mContainerHeight) {
                /**
                 * 处于范围内
                 */

            } else {
                /**
                 * 不在范围内
                 */
                closeToolBox()
            }

        }
        return false
    }

    override fun openToolBoxItemSendTempImage() {
        /**
         * 打开发送图片功能
         */
//        mToolBoxSendImageFuncOpen = true
//        mToolBoxItemContainer.visibility = View.VISIBLE
//        mToolBoxItemText.setText(R.string.str_private_room_toolbox_send_image)
//        mToolBoxItemIcon.setImageResource(R.mipmap.ico_private_room_toolbox_sendimage)
    }

    override fun closeToolBoxItem() {
        /**
         * 关闭显示外部显示Item的功能
         */
//        mToolBoxSendImageFuncOpen = false
//        mToolBoxItemContainer.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.toolbox_switch -> {
                if (isClosed) {
                    openToolBox()
                } else {
                    closeToolBox()
                }
            }
            R.id.toolbox_sendimage -> {
//                if (mToolBoxSendImageFuncOpen) {
//                    sendCloseSendImage()
//                } else {
//                    sendOpenSendImage()
//                }
//                closeToolBox()
                mObserver?.onSendImage()
            }
            R.id.toolbox_background -> {
                mObserver?.onChangeBackGround()
            }
            R.id.toolbox_paperplane -> {
                mObserver?.onSendPaperPlane()
            }
            R.id.toolbox_item -> {
                if (mToolBoxSendImageFuncOpen) {
                    mObserver?.onSendImage()
                }
            }
        }
    }

    override fun destroy() {
        mObserver = null
    }
}