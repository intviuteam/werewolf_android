package orangelab.project.voice.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 01/11/2017.
 */

public class OctopusView extends RelativeLayout implements Destroyable {

    private TextView tvBai;
    private TextView tvShi;
    private TextView tvGe;

    private Context mContext;

    public OctopusView(Context context) {
        super(context);
        init(context);
    }

    public OctopusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public OctopusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        initView();
    }

    public void resetView(int layoutId) {
        removeAllViews();
        addView(inflate(mContext, layoutId, null));
        tvBai = (TextView) findViewById(R.id.tv_zyj_bai);
        tvShi = (TextView) findViewById(R.id.tv_zyj_shi);
        tvGe = (TextView) findViewById(R.id.tv_zyj_ge);
    }

    private void initView() {
        addView(inflate(mContext, R.layout.layout_voice_game_zhangyuji, null));

        tvBai = (TextView) findViewById(R.id.tv_zyj_bai);
        tvShi = (TextView) findViewById(R.id.tv_zyj_shi);
        tvGe = (TextView) findViewById(R.id.tv_zyj_ge);
    }

    public void setResult(int result) {
        int bai = result / 100;
        int shi = (result % 100) / 10;
        int ge = result % 10;

        tvBai.setText(String.valueOf(bai));
        tvShi.setText(String.valueOf(shi));
        tvGe.setText(String.valueOf(ge));
    }

    //显示出现动画
    public void jump() {
        Animation animationBai = AnimationUtils.loadAnimation(mContext, R.anim.anim_small_game_chou_mic);
        animationBai.setFillAfter(true);
        tvBai.startAnimation(animationBai);

        Animation animationShi = AnimationUtils.loadAnimation(mContext, R.anim.anim_small_game_chou_mic);
        animationShi.setFillAfter(true);
        animationShi.setStartOffset(500);
        tvShi.startAnimation(animationShi);

        Animation animationGe = AnimationUtils.loadAnimation(mContext, R.anim.anim_small_game_chou_mic);
        animationGe.setFillAfter(true);
        animationGe.setStartOffset(1000);
        tvGe.startAnimation(animationGe);
    }

    @Override
    public void destroy() {
        tvBai.clearAnimation();
        tvShi.clearAnimation();
        tvGe.clearAnimation();
    }
}
