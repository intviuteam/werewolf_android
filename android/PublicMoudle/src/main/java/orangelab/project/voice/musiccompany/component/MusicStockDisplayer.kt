package orangelab.project.voice.musiccompany.component

import android.text.TextUtils
import android.view.View
import com.R
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import kotlinx.android.synthetic.main.fragment_music_stock.view.*
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.common.dialog.EnterLoadingDialog
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.voice.musiccompany.adapter.MusicStockAdapter
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource
import orangelab.project.voice.musiccompany.concrete.MusicManager
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig

/**
 * 曲库展示器，处理正在下载的音乐的UI展示
 */
class MusicStockDisplayer(viewGroup: View) : MusicStateSwitcher(), Destroyable {


    private var prePageIndex = 0    //上次页数
    private var pageIndex = 0       //当前页数
    private var keyword = ""        //搜索的关键词
    private var contentView = viewGroup

    private var musics = ArrayList<MusicDataEntity>()
    private var musicStockAdapter = MusicStockAdapter(musics)

    private var commendPrePageIndex = 1     //上次推荐页数
    private var commendPageIndex = 1        //当前推荐页数

    /**
     * 当前处理的音乐
     */
    private lateinit var musicData: MusicDataEntity

    init {
        contentView.list_music.layoutManager = WrapContentLinearLayoutManager(contentView.context)
        contentView.list_music.adapter = musicStockAdapter

        contentView.search_bar.changeStock(musics)
        contentView.search_bar.setCallBack { musicStockAdapter.updateStock(it) }
        contentView.search_bar.onSearchClick = {
            keyword = it
            pageIndex = 1
            prePageIndex = 1
            loadMusic()
        }

        contentView.tv_music_stock_load_more.setOnClickListener {
            if (pageIndex <= 0) {
                commendPageIndex++
                loadCommendMusic()
            } else {
                pageIndex++
                loadMusic()
            }

        }
    }

    /**
     * 加载推荐音乐
     */
    fun loadCommendMusic() {
        MusicManager.instance.refreshStock(commendPageIndex, MusicCompanyConfig.PAGE_STEP)
    }

    private fun loadMusic() {
        MusicManager.instance.refreshStock(keyword, pageIndex, MusicCompanyConfig.PAGE_STEP)
    }

    override fun switchMusic(musicData: MusicDataEntity): MusicStockDisplayer {
        this.musicData = musicData

        return this
    }

    override fun switchToOrder() {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun switchToDownLoading() {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun switchToDownPause() {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun switchToDownLoaded() {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun switchToDownFailed() {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun switchProgress(progress: Int) {
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun refresh(musicData: MusicDataEntity, position: Int) {
        musics.forEach {
            if (TextUtils.equals(musicData.id, it.id)) {
                it.refresh(musicData)
            }
        }
        musicStockAdapter.notifyItemChanged(position, musicData.progress)
    }

    /**
     * 刷新曲库
     */
    fun refreshStock() {
        if (prePageIndex == pageIndex) {
            musics.clear()
        }
        musics.addAll(MusicCompanyDataSource.musicList)
        if (!musics.isEmpty()) {
            musicStockAdapter.updateStock(musics)
            contentView.tv_music_stock_load_more.visibility = View.VISIBLE
        } else {
            ToastToolKit.showShort(R.string.str_music_stock_empty)
            contentView.tv_music_stock_load_more.visibility = View.GONE
        }
        prePageIndex = pageIndex
    }

    /**
     * 刷新曲库(推荐歌曲)
     */
    fun refreshStockByCommend() {
        if (commendPrePageIndex == commendPageIndex) {
            musics.clear()
        }
        musics.addAll(MusicCompanyDataSource.musicList)
        if (!musics.isEmpty()) {
            musicStockAdapter.updateStock(musics)
            contentView.tv_music_stock_load_more.visibility = View.VISIBLE
        } else {
            ToastToolKit.showShort(R.string.str_music_stock_empty)
            contentView.tv_music_stock_load_more.visibility = View.GONE
        }
        commendPrePageIndex = commendPageIndex
    }

    private var mEnterLoadingDialog: EnterLoadingDialog? = null
    fun showLoading() {
        //mEnterLoadingDialog = EnterLoadingDialog(contentView.context)
        //mEnterLoadingDialog?.show()
    }

    fun dismissLoading() {
//        if (mEnterLoadingDialog != null) {
//            mEnterLoadingDialog?.dismiss()
//        }
    }

    override fun destroy() {
        prePageIndex = 1
        pageIndex = 1
        dismissLoading()
    }
}
