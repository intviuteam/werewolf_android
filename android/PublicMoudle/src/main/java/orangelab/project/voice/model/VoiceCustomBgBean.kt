package orangelab.project.voice.model

import android.text.TextUtils
import com.toolkit.action.Keepable
import java.io.Serializable

/**
 * Created by lloydfinch on 19/03/2018.
 */
data class VoiceCustomBgBean(var path: String = "") : Serializable, Keepable {

    var title: String? = ""//显示的标题
    var clickable: Boolean = false//是否对点击作出相应,默认的背景图不相应

    val isEmpty: Boolean//是否已经放置有图片
        get() = TextUtils.isEmpty(path)

    constructor(title: String, path: String) : this(path) {
        this.title = title
        this.path = path
    }

    constructor(title: String, path: String, clickable: Boolean) : this(title, path) {
        this.title = title
        this.path = path
        this.clickable = clickable
    }

    override fun toString(): String {
        return "VoiceCustomBgBean(path='$path', title=$title, clickable=$clickable"
    }
}