package orangelab.project.voice.cache;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.androidtoolkit.StreamToolKit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by lloydfinch on 29/10/2017.
 */
public class FileCache {

    private static final String TAG = "FileCache";

    //用来获取CacheDir
    private Context context;

    private static FileCache ourInstance;

    public static FileCache createInstance(Context context) {

        if (context != null) {
            if (ourInstance == null) {
                ourInstance = new FileCache(context.getApplicationContext());
            }
            return ourInstance;
        } else {
            throw new IllegalArgumentException("Context must be set");
        }
    }

    //用于通知需要context加载
    public static FileCache getInstance() {
        if (ourInstance != null) {
            return ourInstance;
        } else {
            throw new IllegalArgumentException("You must invoke createInstance(Context) before this");
        }
    }

    private FileCache(Context context) {
        this.context = context;
    }

    public byte[] loadFile(String url) {
        PLog.e(TAG, "loadFile: " + url);
        File cacheDir = null;
        byte[] ret = null;
        String state = Environment.getExternalStorageState();
        if (TextUtils.equals(Environment.MEDIA_MOUNTED, state)) {
            cacheDir = context.getExternalCacheDir();
        } else {
            cacheDir = context.getCacheDir();
        }
        if (url != null) {
            String name = MD5(url);
            File file = new File(cacheDir, name);
            if (file.exists()) {
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(file);
                    ret = StreamToolKit.readStream(fin);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    StreamToolKit.close(fin);
                }
            }
        }
        return ret;
    }

    public File loadFile2Mp3(String url) {
        PLog.e(TAG, "loadFile2Mp3: " + url);
        File cacheDir = null;
        String state = Environment.getExternalStorageState();
        if (TextUtils.equals(Environment.MEDIA_MOUNTED, state)) {
            cacheDir = context.getExternalCacheDir();
        } else {
            cacheDir = context.getCacheDir();
        }
        if (url != null) {
            String name = MD5(url);
            File file = new File(cacheDir, name + ".mp3");
            if (file.exists()) {
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(file);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    StreamToolKit.close(fin);
                }
            }
            return file;
        }
        return null;
    }

    public void saveFile(String url, byte[] data) {
        File cacheDir = null;
        String state = Environment.getExternalStorageState();
        if (TextUtils.equals(Environment.MEDIA_MOUNTED, state)) {
            cacheDir = context.getExternalCacheDir();
        } else {
            cacheDir = context.getCacheDir();
        }
        if (url != null && data != null) {
            String name = MD5(url);
            File file = new File(cacheDir, name);
            FileOutputStream fot = null;
            try {
                fot = new FileOutputStream(file);
                fot.write(data);
                PLog.e(TAG, "saveFile: " + name);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                StreamToolKit.close(fot);
            }
        }
    }

    //MD5转换
    private String MD5(String url) {
        String ret = null;
        if (url != null) {
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] data = md5.digest(url.getBytes());
                StringBuilder sb = new StringBuilder();
                for (byte b : data) {
                    int h = (b >> 4) & 0x0f;
                    int l = b & 0x0f;
                    sb.append(Integer.toHexString(h)).append(Integer.toHexString(l));
                }
                ret = sb.toString();
                sb = null;
                data = null;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
