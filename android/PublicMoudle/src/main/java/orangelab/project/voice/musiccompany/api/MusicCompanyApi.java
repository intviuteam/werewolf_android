package orangelab.project.voice.musiccompany.api;

import com.androidtoolkit.PLog;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.toolkit.action.ApiResult;

import cn.intviu.support.GsonHelper;
import orangelab.project.voice.musiccompany.model.MusicStockParseBean;

public class MusicCompanyApi {

    /**
     * 获取推荐歌曲列表
     */
    public static void getRecommendSong(String url, ApiResult<MusicStockParseBean.Data> callback) {

        RequestTask.create()
                .isGet(true)
                .request(url)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {

                        PLog.e("getRecommendSong", response);
                        MusicStockParseBean.Data bean = GsonHelper.parseJsonToBean(response, MusicStockParseBean.Data.class);
                        callback.onResult(bean, null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        PLog.e("getRecommendSong", "null");
                        callback.onResult(null, null);
                    }

                    @Override
                    public void onError(Exception e) {
                        PLog.e("getRecommendSong", "null");
                        callback.onResult(null, e);
                    }
                }).build().execute();
    }

    /**
     * 获取曲库列表
     */
    public static void getSongStockByStep(String url, ApiResult<MusicStockParseBean.Data> callback) {

        RequestTask.create()
                .isGet(true)
                .request(url)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {

                        PLog.e("getSongStockByStep", response);
                        MusicStockParseBean.Data bean = GsonHelper.parseJsonToBean(response, MusicStockParseBean.Data.class);
                        callback.onResult(bean, null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        PLog.e("getSongStockByStep", "null");
                        callback.onResult(null, null);
                    }

                    @Override
                    public void onError(Exception e) {
                        PLog.e("getSongStockByStep", "null");
                        callback.onResult(null, null);
                    }
                }).build().execute();
    }

    /**
     * 获取歌曲详情
     */
    public static void getSongDetail(String url, ApiResult<MusicStockParseBean.Data> callback) {
        RequestTask.create()
                .isGet(true)
                .request(url)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        MusicStockParseBean.Data bean = GsonHelper.parseJsonToBean(response, MusicStockParseBean.Data.class);
                        callback.onResult(bean, null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        PLog.e("getSongDetail", "null");
                        callback.onResult(null, null);
                    }

                    @Override
                    public void onError(Exception e) {
                        PLog.e("getSongDetail", "null");
                        callback.onResult(null, null);
                    }
                }).build().execute();
    }

    /**
     * 获取歌词
     */
    public static void getLyric(String url, ApiResult<String> callback) {

        try {
            RequestTask.create()
                    .isGet(true)
                    .urlMode(RequestTask.RequestTaskBuilder.URL_CUSTOM)
                    .customUrl(url)
                    .interceptor(data -> {
                                PLog.e("getLyric", "lyric: " + data);
                                if (data != null) {
                                    callback.onResult(data, null);
                                } else {
                                    callback.onResult("", null);
                                }
                                return true;
                            }
                    ).action(new RequestTask.RequestTaskCallBackAdapter() {
                @Override
                public void onFailed(int code, String failed) {
                    callback.onResult("", null);
                }

                @Override
                public void onError(Exception e) {
                    callback.onResult("", null);
                }
            }).build().execute();
        } catch (Exception e) {
            callback.onResult("", null);
        }
    }
}
