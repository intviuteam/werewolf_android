package orangelab.project.voice.share;

import android.text.TextUtils;

import com.BuildConfig;
import com.R;

import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.api.VoiceApiDefines;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.manager.VoiceTopicManager;

import com.networktoolkit.transport.URLManager;

/**
 * Created by lloydfinch on 29/11/2017.
 */

public class ShareBuilder {
    private ShareType shareType;//分享类型,根据类型设置icon

    private String name;
    private String roomId;
    private String psw;

    private String shareTitle;
    private String shareMsg;
    private String shareUrl;
    private String avatar;

    public ShareBuilder(ShareType shareType, String name, String roomId, String psw) {
        this.shareType = shareType;
        this.name = name;
        this.roomId = roomId;
        this.psw = psw;
    }

    public ShareBuilder setShareType(ShareType shareType) {
        this.shareType = shareType;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public ShareBuilder setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
        return this;
    }

    public ShareBuilder setShareMsg(String shareMsg) {
        this.shareMsg = shareMsg;
        return this;
    }

    public ShareBuilder setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
        return this;
    }

    public ShareBuilder setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    //根据不同的shareType进行转换处理
    public String getShareTitle() {
        if (ShareType.FROM_TYPE_VOICE.equals(shareType)) {
            String type = VoiceRoomConfig.getRoomType();
            String title = type;//不是"谁是卧底"房，直接显示主题
            if (!TextUtils.isEmpty(type)) {
                if (TextUtils.equals(VoiceConstants.SPY_TAG, type)) {
                    //"谁是卧底"房，显示特殊分享文案
                    title = MessageUtils.getString(R.string.share_title_voice_wolf_spy);
                } else {
                    String typeDes = VoiceTopicManager.getInstance().findTopicDesByType(VoiceRoomConfig.getRoomType());
                    title = typeDes;
                }
            }
            shareTitle = title;
        }
        return shareTitle;
    }

    //根据不同的shareType进行转换处理
    public String getShareMsg() {
        if (ShareType.FROM_TYPE_VOICE.equals(shareType)) {
            String pswString = TextUtils.isEmpty(psw) ? "" : MessageUtils.getString(R.string.str_psw, psw);
            String message = "";
            message = MessageUtils.getString(R.string.share_message_from_voice_wolf, roomId, pswString, name);
            String type = VoiceRoomConfig.getRoomType();
            if (!TextUtils.isEmpty(type)) {
                if (TextUtils.equals(VoiceConstants.SPY_TAG, type)) {
                    //"谁是卧底"房，显示特殊分享文案
                    message = MessageUtils.getString(R.string.share_message_from_voice_wolf_spy, roomId, pswString);
                }
            }
            shareMsg = message;
        }
        return shareMsg;
    }

    //根据不同的shareType进行转换处理
    public String getShareUrl() {
        if (ShareType.FROM_TYPE_VOICE.equals(shareType)) {
            shareUrl = String.format(URLManager.GetDefaultURL() + VoiceApiDefines.SHARE_AUDIO_URL_WEREWOLF, roomId, psw);
        }
        return shareUrl;
    }
}
