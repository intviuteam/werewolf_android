package orangelab.project.voice.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.R;

/**
 * Created by lloydfinch on 30/10/2017.
 * 点击旋转，再点还原的按钮
 */

public class PassView extends android.support.v7.widget.AppCompatImageView {

    private static final String TAG = "PassView";
    private Animation animation;

    public PassView(Context context) {
        super(context);
        init();
    }

    public PassView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        animation = AnimationUtils.loadAnimation(getContext(), R.anim.anim_rotate_pass_view);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(v -> {
            PassView.this.onClick();
            l.onClick(v);
        });
    }

    private void onClick() {
        startAnimation(animation);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnimation();
        animation = null;
    }
}
