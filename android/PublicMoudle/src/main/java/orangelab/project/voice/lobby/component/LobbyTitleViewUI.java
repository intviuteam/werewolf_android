package orangelab.project.voice.lobby.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.toolkit.action.Destroyable;

import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.utils.PositionHelper;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public class LobbyTitleViewUI implements Destroyable {

    private ViewGroup contentView;

    private View llOnLineContainer;
    private TextView tvTitle;
    private TextView tvComeIn;
    private View ivBack;
    private ImageView ivWifi;
    private TextView tvRoomId;
    private TextView tvOnLineNumber;
    private View ivSetting;
    private View ivShare;
    private ImageView ivTypeIcon;

    private Context mContext;

    LobbyTitleViewUI(Context context, ViewGroup contentView) {
        this.mContext = context;
        this.contentView = contentView;
        init();
    }

    private void init() {
        llOnLineContainer = contentView.findViewById(R.id.ll_online_container);
        tvComeIn = (TextView) contentView.findViewById(R.id.tv_come_in);
        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        ivBack = contentView.findViewById(R.id.iv_back);
        ivWifi = (ImageView) contentView.findViewById(R.id.iv_wifi_icon);
        tvRoomId = (TextView) contentView.findViewById(R.id.tv_room_id);
        tvOnLineNumber = (TextView) contentView.findViewById(R.id.tv_online_number);
        ivSetting = contentView.findViewById(R.id.iv_setting);
        ivShare = contentView.findViewById(R.id.iv_share);
        ivTypeIcon = (ImageView) contentView.findViewById(R.id.iv_voice_type_icon);
    }

    public void updateTitle(String title, String type) {
        tvTitle.setText(title);
        updateTypeIcon(VoiceTopicManager.getInstance().findTopicBgUrlByType(type));
    }

    public void setRoomId(String roomId) {
        tvRoomId.setText(roomId);
    }

    public void updateOnLineNumber(int number) {
        tvOnLineNumber.setText(String.valueOf(number));
    }

    public void updateWifiState(boolean isConnected) {
        if (isConnected) {
            ivWifi.setImageResource(R.mipmap.wifi_connect);
        } else {
            ivWifi.setImageResource(R.mipmap.wifi_disconnect);
        }
    }

    public void showComeIn(String name) {
        if (TextUtils.isEmpty(name)) {
            //防止名字为空
            tvComeIn.setText(MessageUtils.getString(R.string.str_comein, "..."));
        } else {
            tvComeIn.setText(MessageUtils.getString(R.string.str_comein, name.length() <= 5 ? name : name.substring(0, 5) + "..."));
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.voice_member_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                tvComeIn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (tvComeIn != null) {
                    tvComeIn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tvComeIn.startAnimation(animation);
    }

    public View getLlOnLineContainer() {
        return llOnLineContainer;
    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public View getIvBack() {
        return ivBack;
    }

    public ImageView getIvWifi() {
        return ivWifi;
    }

    public TextView getTvRoomId() {
        return tvRoomId;
    }

    public TextView getTvOnLineNumber() {
        return tvOnLineNumber;
    }

    public View getIvSetting() {
        return ivSetting;
    }

    public View getIvShare() {
        return ivShare;
    }

    public void showMasterUI() {
        if (showMasterUIFilter()) {
            showSetting();
        } else {
            hideSetting();
        }
    }

    private boolean showMasterUIFilter() {

        //是房主或者owner_id是自己，就展示设置按钮
        if (PositionHelper.isMaster() || VoiceRoomConfig.isIsOwnerRoom()) {
            return true;
        }
        return false;
    }

    public void showSetting() {
        ivSetting.setVisibility(View.VISIBLE);
    }

    public void hideSetting() {
        ivSetting.setVisibility(View.GONE);
    }

    public void updateTypeIcon(String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            PicassoUtils.loadImage(mContext, imageUrl, ivTypeIcon);
        }
    }

    @Override
    public void destroy() {
        if (tvComeIn != null) {
            tvComeIn.clearAnimation();
            tvComeIn = null;
        }
    }
}
