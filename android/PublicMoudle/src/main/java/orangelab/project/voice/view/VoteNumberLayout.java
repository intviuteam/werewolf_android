package orangelab.project.voice.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.androidtoolkit.PLog;
import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lloydfinch on 27/11/2017.
 * <p>
 * 投票容器,从下往上，从左往右添加
 */

public class VoteNumberLayout extends LinearLayout implements Destroyable {

    private int col = 4;//默认4列
    private List<LinearLayout> childrenView = new ArrayList<>();
    private int itemChildCount = 0;

    private Context mContext;

    public VoteNumberLayout(Context context) {
        super(context);
        init(context);
    }

    public VoteNumberLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VoteNumberLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
    }

    public void addVoteView(View view) {
        if (view == null) {
            return;
        }
        itemChildCount++;
        int row = itemChildCount / (col + 1);//加在第几行

        LinearLayout rowLayout = null;
        if (childrenView.size() > 0) {
            try {
                rowLayout = childrenView.get(row);
            } catch (IndexOutOfBoundsException e) {
                //下表越界,说明还没有添加这一行
                rowLayout = null;
            }
        }
        if (rowLayout == null) {
            rowLayout = new LinearLayout(mContext);
            LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            layoutParams.topMargin = ScreenUtils.dip2px(2);
            rowLayout.setLayoutParams(layoutParams);
            rowLayout.setOrientation(HORIZONTAL);
            rowLayout.setGravity(Gravity.CENTER);
            addView(rowLayout, 0);
            childrenView.add(rowLayout);
        }
        rowLayout.addView(view);
        PLog.e("VoteNumberLayout", "bevote: " + itemChildCount);

    }

    @Override
    public void removeAllViews() {
        super.removeAllViews();
        childrenView.clear();
        itemChildCount = 0;
    }

    @Override
    public void destroy() {
        childrenView.clear();
    }
}
