package orangelab.project.voice.musiccompany.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.R

/**
 * ktv房间右侧，"歌单"点击的tag
 */
class SongMenuViewTag(context: Context?, attributeSet: AttributeSet) : RelativeLayout(context, attributeSet) {

    private lateinit var tvSongNumber: TextView

    init {
        init(context)
    }

    private fun init(context: Context?) {
        val contentView = View.inflate(context, R.layout.layout_music_menu_tag, null)

        initView(contentView)

        addView(contentView)
    }

    private fun initView(contentView: View) {
        tvSongNumber = contentView.findViewById(R.id.tv_music_song_menu) as TextView
    }

    fun refreshSongNumber(number: Int) {
        tvSongNumber.text = if (number < 0) {
            " x0"
        } else {
            " x$number"
        }
    }
}