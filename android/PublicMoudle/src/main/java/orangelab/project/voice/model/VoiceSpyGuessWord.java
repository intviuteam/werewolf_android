package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 11/12/2017.
 */

public class VoiceSpyGuessWord implements Serializable, Keepable {

    public int position = -10086;
    public int duration;

    @Override
    public String toString() {
        return "VoiceSpyGuessWord{" +
                "position=" + position +
                ", duration=" + duration +
                '}';
    }
}
