package orangelab.project.voice.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.R;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.NetworkHelpers;
import orangelab.project.MainApplication;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.SpaceItemDecoration;
import orangelab.project.voice.adapter.VoiceRoomAdminListAdapter;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.model.AdminListParserMode;
import orangelab.project.voice.model.VoiceAdminListMode;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

/**
 * Created by hwx on 07/08/2018.
 */

public class VoiceRoomAdminListActivity extends SafeActivity implements Destroyable {

    private View mBack;
    private RecyclerView mRecyclerView;
    private VoiceRoomAdminListAdapter mAdapter;
    private List<VoiceAdminListMode> mAdminList;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_room_admin_list);
        init();
        getAdminList();
    }

    private void init() {
        initWindow();
        initView();
        initData();
        initListener();
    }

    private void initWindow() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        Utils.setWindowStatusBarColor(this, R.color.vocie_status_bar);
    }

    private void initView() {
        mBack = this.findViewById(R.id.iv_back);
        mRecyclerView = (RecyclerView) this.findViewById(R.id.list_admin_list);
        mRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SpaceItemDecoration(ScreenUtils.dip2px(4)));
    }

    private void initData() {
        mAdminList = new ArrayList<>();
        mAdapter = new VoiceRoomAdminListAdapter(this, mAdminList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initListener() {
        mBack.setOnClickListener(v -> onBackPressed());
        initServerMessageEvent();
        initKickOutListener();

        mAdapter.setOnItemClickListener(userItem -> {
            if (judgeSocketConnect()) {
                RoomSocketEngineHelper.removeVoiceAdmin(userItem.id);
            }
        });
    }

    private void getAdminList() {
        RoomSocketEngineHelper.getVoiceAdminList();
    }

    //接受socket推送的消息
    private void initServerMessageEvent() {
        VoiceServerMessageHandler.getInstance().registerUIHandler(mMessageHandler);
    }

    private void initKickOutListener() {
        RxToolKit.Build(this, FinishConversationEvent.class)
                .action(event -> runOnUiThreadSafely(() -> finish()))
                .register();
    }

    private boolean judgeSocketConnect() {
        //网络断开或者socket没连上
        boolean isConnect = NetworkHelpers.isNetworkAvailable(MainApplication.getInstance()) &&
                VoiceRoomGameSocketManager.getInstance().isConnected();
        if (!isConnect) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
            return false;
        }
        return true;
    }

    @Override
    public void destroy() {

        RxToolKit.UnRegister(this);
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(mMessageHandler);

        if (mAdminList != null) {
            mAdminList.clear();
            mAdapter.notifyDataSetChanged();
        }
        if (mAdapter != null) {
            mAdapter.destroy();
        }
    }

    @Override
    public void finish() {
        destroy();
        super.finish();
    }

    public static void launch(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, VoiceRoomAdminListActivity.class));
        }
    }

    private AbstractSocketMessageHandler mMessageHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handleKickOut(VoiceMessageBean messageBean) {
            RoomSocketEngineHelper.getVoiceAdminList();
        }

        @Override
        public void handleAdminList(VoiceMessageBean messageBean) {
            AdminListParserMode mode = GsonHelper.getGson().fromJson(messageBean.payload.toString(), AdminListParserMode.class);
            mAdminList = mode.users;
            mAdapter.updateAdminList(mAdminList);
        }

        @Override
        public void handleUnAdmin(VoiceMessageBean messageBean) {
            RoomSocketEngineHelper.getVoiceAdminList();
        }

        @Override
        public void destroy() {
        }
    };
}
