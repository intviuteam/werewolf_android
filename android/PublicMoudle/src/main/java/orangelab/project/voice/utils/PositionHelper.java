package orangelab.project.voice.utils;

import android.text.TextUtils;

import com.datasource.GlobalUserState;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;

/**
 * Created by lloydfinch on 05/12/2017.
 */

public final class PositionHelper {

    //是否是房主
    public static boolean isMaster() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData == null) {
            return false;
        }
        return personalData.getSelfPosition() == VoiceConstants.MASTER_POSITION;
    }

    //是否是玩家
    public static boolean isPlayer() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData == null) {
            return false;
        } else {
            int selfPosition = personalData.getSelfPosition();
            return selfPosition <= VoiceRoomConfig.getMaxChairNumber() && selfPosition > VoiceConstants.MASTER_POSITION;
        }
    }

    //是否在麦上
    public static boolean isUpSeat() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            int selfPosition = personalData.getSelfPosition();
            return selfPosition <= VoiceRoomConfig.getMaxChairNumber() && selfPosition >= VoiceConstants.MASTER_POSITION;
        }
        return false;
    }

    //是否是房主 0
    public static boolean isMaster(int position) {
        return position == VoiceConstants.MASTER_POSITION;
    }

    //是否是玩家 1-8
    public static boolean isPlayer(int position) {
        return position <= VoiceRoomConfig.getMaxChairNumber() && position > VoiceConstants.MASTER_POSITION;
    }

    //是否是麦上位置
    public static boolean isUpSeat(int position) {
        return position <= VoiceRoomConfig.getMaxChairNumber() && position >= VoiceConstants.MASTER_POSITION;
    }

    //是否是自己
    public static boolean isSelf(String id) {
        //游客直接返回false
        if (Utils.checkUserIdIsTourist(id)) {
            return false;
        }
        String selfId = GlobalUserState.getGlobalState().getUserId();
        return TextUtils.equals(selfId, id);
    }

    //是否是自己
    public static boolean isSelfPosition(int position) {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData == null) {
            return false;
        }
        int selfPosition = personalData.getSelfPosition();
        return selfPosition == position;
    }

    //是否未投票
    public static boolean isGiveUpPosition(int position) {
        return -1 == position;
    }

    //是否是非法位置
    public static boolean isIllegalPosition() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData == null) {
            return true;
        }
        int selfPosition = personalData.getSelfPosition();
        return selfPosition < VoiceConstants.MASTER_POSITION;
    }

    //是否是非法位置
    public static boolean isIllegalPosition(int position) {
        return position < VoiceConstants.MASTER_POSITION;
    }

    /**
     * 自己是否有伴奏权限
     */
    public static boolean canSing() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (personalData.isCanSing()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 自己是否是唱歌的人
     */
    public static boolean isSinger() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (personalData.isSing()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否在房间
     */
    public static boolean inRoom(String id) {
        EnterRoomResult.EnterRoomUserItem user =
                VoiceRoomDataSourceManager.getInstance().findUserById(id);

        return user != null;
    }

    /**
     * 是否在麦上
     */
    public static boolean isUpSeat(String id) {
        EnterRoomResult.EnterRoomUserItem user =
                VoiceRoomDataSourceManager.getInstance().findUserById(id);
        if (user != null) {
            return isUpSeat(user.position);
        }
        return false;
    }

    /*
     * 自己是否管理员
     */
    public static boolean isAdmin() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (personalData.isAdmin()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 自己是否是客人
     */
    public static boolean isGuest() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (personalData.isGuest()) {
                return true;
            }
        }
        return false;
    }

}
