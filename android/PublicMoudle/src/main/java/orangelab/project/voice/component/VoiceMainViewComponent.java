package orangelab.project.voice.component;

import android.view.View;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEventOver;
import com.toolkit.action.Destroyable;

import java.util.List;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public interface VoiceMainViewComponent extends Destroyable {

    //展示数据
    void displayData();

    void initLikeCount(int likeCount);

    void initMembers(EnterRoomResult.EnterRoomMessage enterRoomResult);

    void handleServerEvent(ServerMessageEventOver event);

    void handleServerResponseEvent(ServerResponseEventOver event);

    void showChangeTitleUI(String title, String type);

    void hideChangeTitleUI();

    void updateMasterUI();

    void saveTitle(String title, String titleType);

    void updateSocketState(boolean isConnected);

    void setOnChangeTitleClickedListener(View.OnClickListener onChangeTitleClickedListener);

    List<EnterRoomResult.EnterRoomUserItem> getOnLineMembers();

    EnterRoomResult.EnterRoomUserItem findUserByPosition(int position);

    EnterRoomResult.EnterRoomUserItem findUserById(String id);

    void onResume();

    void onPause();

    void onReEnter();//重新进入调用,复现聊天消息

    void hideChatMessage(boolean status);

    void hideGiftMessage(boolean status);

    void hideJoinMessage(boolean status);
}
