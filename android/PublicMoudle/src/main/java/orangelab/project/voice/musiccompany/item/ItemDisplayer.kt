package orangelab.project.voice.musiccompany.item

import android.view.View
import com.R
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import kotlinx.android.synthetic.main.layout_voice_music_stock_item.view.*
import orangelab.project.MainApplication
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.musiccompany.api.MusicSocketHelper
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource
import orangelab.project.voice.musiccompany.concrete.MusicManager
import orangelab.project.voice.musiccompany.concrete.MusicState
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import orangelab.project.voice.musiccompany.model.MusicOrderSong
import orangelab.project.voice.utils.TransferClassHelper

/**
 * 列表条目展示器
 */
class ItemDisplayer {

    companion object {

        val TAG = "ItemDisplayer"

        fun updateState(musicData: MusicDataEntity) {
        }

        /**
         * 发送"点歌"消息
         */
        fun orderSong(musicData: MusicDataEntity) {
            //点歌，构造点歌实体
            val song: MusicOrderSong = TransferClassHelper.MusicDataEntity2MusicOrderSong(musicData)
            if (MusicCompanyDataSource.hasAddToMenu(song)) {
                ToastToolKit.showShort(R.string.str_music_song_has_ordered)

                //点的歌曲是自己正在唱的歌曲，也不能添加
                if (MusicCompanyConfig.currentSong != null) {
                    if (MusicCompanyDataSource.hasAddToMenu(MusicCompanyConfig.currentSong)) {
                        ToastToolKit.showShort(R.string.str_music_song_is_singing)
                    }
                }
            } else {
                MusicSocketHelper.INSTANCE.sendOrderSong(song)
            }
        }

        //音乐下载界面条目显示器
        fun displayMusicItem(itemView: View, musicData: MusicDataEntity, position: Int) {

            /**
             * 点歌
             */
            fun toOrder() {
                itemView.btn_order_or_play.text = MessageUtils.getString(R.string.str_music_song_down_load)
                itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_down_load)
                itemView.btn_order_or_play.setOnClickListener {
                    MusicManager.instance.orderSong(musicData, position)
                }
            }

            /**
             * 取消
             */
            fun toPause() {
                itemView.btn_order_or_play.text = "${musicData.progress}%"
                itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_down_load)
                itemView.btn_order_or_play.setOnClickListener {

                }
            }

            /**
             * 继续
             */
            fun toContinue() {
                itemView.btn_order_or_play.text = MessageUtils.getString(R.string.str_music_continue)
                itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_down_load)
                itemView.btn_order_or_play.setOnClickListener {
                    MusicManager.instance.orderSong(musicData, position)
                }
            }

            /**
             * 播放
             */
            fun toPlay() {
                itemView.btn_order_or_play.text = MessageUtils.getString(R.string.str_music_song_order)
                itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_order)
                itemView.btn_order_or_play.setOnClickListener {
                    orderSong(musicData)
                }
            }

            /**
             * 等待
             */
            fun toWait() {
                itemView.btn_order_or_play.text = MessageUtils.getString(R.string.str_music_down_load_wait)
                itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_down_load)
                itemView.btn_order_or_play.isClickable = false
            }

            //按钮展示
            when (musicData.state) {
                MusicState.STATE_DOWN_ING -> {
                    toPause()
                }
                MusicState.STATE_DOWN_PAUSE -> {
                    toContinue()
                    updateState(musicData)
                }
                MusicState.STATE_DOWN_FINISH -> {
                    toPlay()
                }
                MusicState.STATE_DOWN_NON -> {
                    toOrder()
                    updateState(musicData)
                }
                MusicState.STATE_DOWN_FAILED -> {
                    toOrder()
                    updateState(musicData)
                }
                MusicState.STATE_DOWN_WAIT -> {
                    toWait()
                    updateState(musicData)
                }
            }

            //静态信息展示
            itemView.tv_music_name.text = musicData.song_name
            itemView.tv_music_size.text = musicData.sizeDes()
            itemView.tv_music_singer.text = musicData.author_name
            itemView.progress_music_stock.progress = musicData.progress

            PicassoUtils.loadImageByDefault(MainApplication.getInstance(), musicData.img, itemView.tv_music_img, R.mipmap.default_head)
        }

        //已点界面item展示
        fun displayOrderedItem(itemView: View, musicData: MusicDataEntity, position: Int) {
            //静态信息展示
            itemView.tv_music_name.text = musicData.song_name
            itemView.tv_music_size.text = musicData.sizeDes()
            itemView.tv_music_singer.text = musicData.author_name
            PicassoUtils.loadImageByDefault(MainApplication.getInstance(), musicData.img, itemView.tv_music_img, R.mipmap.default_head)

            itemView.btn_order_or_play.text = MessageUtils.getString(R.string.str_music_song_order)
            itemView.btn_order_or_play.setBackgroundResource(R.drawable.bg_music_btn_order)
            itemView.btn_order_or_play.setOnClickListener {
                orderSong(musicData)
            }
        }
    }
}