package orangelab.project.voice.privateroom.model

import com.toolkit.action.Keepable
import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.model.UserVipInfo

/**
 * game_werewolf
 * 2018/3/19 下午4:11
 * Mystery
 */

class PrivateRoomMemberDataImpl : Keepable, PrivateRoomMemberData {

    var position: Int = -1

    var id: String? = null
    var name: String? = null
    var sex: Int = 0
    var url: String? = null
    var level: Int = 0
    var signature: String? = ""

    var isSpeaking: Boolean = false
    var isFreeStyle: Boolean = false
    var isMaster: Boolean = false
    var isSelf: Boolean = false
    var isDeputy: Boolean = false
    var isBanMic: Boolean = false
    var isUpSeat: Boolean = false

    var isNeedRefresh: Boolean = false

    var memberVipInfo: UserVipInfo? = null
    var memberHeadDecor: UserInfoResult.AvatarBox? = null
    var memberMsgDecor: UserInfoResult.MessageBox? = null

    override fun getMemberPosition(): Int {
        return position
    }

    override fun getMemberId(): String? {
        return id
    }

    override fun getMemberIcon(): String? {
        return url
    }

    override fun getMemberName(): String? {
        return name
    }

    override fun getMemberSex(): Int {
        return sex
    }

    override fun getMemberLevel(): Int {
        return level
    }

    override fun getMemberSignature(): String? {
        return signature
    }

    override fun memberIsSpeaking(): Boolean {
        return isSpeaking
    }

    override fun memberIsMaster(): Boolean {
        return isMaster
    }

    override fun memberIsSelf(): Boolean {
        return isSelf
    }

    override fun memberIsDeputy(): Boolean {
        return isDeputy
    }

    override fun memberIsBanMic(): Boolean {
        return isBanMic
    }

    override fun memberIsUpSeat(): Boolean {
        return isUpSeat
    }

    override fun memberIsFreeStyle(): Boolean {
        return isFreeStyle
    }

    override fun memberNeedRefresh(): Boolean {
        return isNeedRefresh
    }

    override fun memberRefreshDone() {
        isNeedRefresh = false
    }

    override fun memberMsgDecor(): UserInfoResult.MessageBox? {
        return memberMsgDecor
    }

    override fun memberHeadDecor(): UserInfoResult.AvatarBox? {
        return memberHeadDecor
    }

    override fun memberVipInfo(): UserVipInfo? {
        return memberVipInfo
    }

    override fun toString(): String {
        return "PrivateRoomMemberDataImpl(position=$position, id=$id, name=$name, isSpeaking=$isSpeaking, isFreeStyle=$isFreeStyle, isMaster=$isMaster, isSelf=$isSelf, isDeputy=$isDeputy, isBanMic=$isBanMic, isUpSeat=$isUpSeat, isNeedRefresh=$isNeedRefresh)\n"
    }


}
