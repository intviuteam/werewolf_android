package orangelab.project.voice.musiccompany.download;

import com.androidtoolkit.PLog;

/**
 * 下载回调
 */
public abstract class AjaxCallBack<T> {

    private boolean progress = true;
    private int rate = 1000 * 1;//每秒

    public boolean isProgress() {
        return progress;
    }

    public int getRate() {
        return rate;
    }

    /**
     * 设置进度,而且只有设置了这个了以后，onLoading才能有效。
     */
    public AjaxCallBack<T> progress(boolean progress, int rate) {
        this.progress = progress;
        this.rate = rate;
        return this;
    }

    public void onStart() {
    }

    /**
     * 进度更新
     */
    public void onLoading(long count, long current, T result) {
    }

    /**
     * 成功回调
     */
    public void onSuccess(T t) {
    }

    /**
     * 失败回调
     */
    public void onFailure(Throwable t, int errorNo, String msg, T result) {
    }

}
