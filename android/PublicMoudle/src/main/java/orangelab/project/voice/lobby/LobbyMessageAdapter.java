package orangelab.project.voice.lobby;

import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import orangelab.project.MainApplication;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.effect.msgdecor.MsgDecorToolKit;
import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.view.VipNameView;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.ds.LimitList;
import orangelab.project.voice.lobby.api.LobbyRoomConstants;
import orangelab.project.voice.lobby.api.LobbyRoomSocketHelper;
import orangelab.project.voice.lobby.data.LobbyRoomConfig;
import orangelab.project.voice.lobby.model.LobbyAcceptGameBean;
import orangelab.project.voice.manager.GameScoreReflection;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.utils.PositionHelper;

/**
 * 大厅房间消息适配器。比语音房多了游戏类型消息的处理
 */

public class LobbyMessageAdapter extends BaseAdapter {

    private static final int PADDING = ScreenUtils.dip2px(4);

    private final String TAG = "LobbyMessageAdapter";


    private Context mContext;
    private ListView listMsg;
    private LimitList<SystemMessageItem> messageList = new LimitList<>();
    private boolean isScrollToBottom = true;
    private OnNameClickListener onNameClickListener;

    public LobbyMessageAdapter(Context mContext, ListView listMsg, List<SystemMessageItem> messageList) {
        this.mContext = mContext;
        this.listMsg = listMsg;
        this.messageList.clear();
        this.messageList.addAll(messageList);

        initListScrollListener();
    }

    private void initListScrollListener() {
        listMsg.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        isScrollToBottom = true;
                    } else {
                        isScrollToBottom = false;
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    public void setOnNameClickListener(OnNameClickListener onNameClickListener) {
        this.onNameClickListener = onNameClickListener;
    }

    //刷新消息列表:一般消息
    public void refreshMessageList(Long key) {
        if (VoiceRoomConfig.isIsHiddenMsg()) {
            showLobbyMessage(key);
        } else {
            showAllMessage(key);
        }

    }

    /**
     * 展示所有消息
     */
    private void showAllMessage(long key) {
        //这里清空所有消息，添加新的，考虑更好策略
        messageList.clear();
        messageList.addAll(VoiceRoomDataSourceManager.getInstance().getMessageList());
        notifyDataSetChanged();

        //已经在底部就刷新底部
        if (isScrollToBottom) {
            listMsg.setSelection(getCount() - 1);
        }

        //自己发的消息就滚动到底部
        SystemMessageItem latestMessage = VoiceRoomDataSourceManager.getInstance().getLatestMessage(key);
        //这里需要额外添加判空，因为大厅有些消息不展示(导致没添加)
        if (latestMessage != null) {
            EnterRoomResult.EnterRoomUserItem user = latestMessage.getUser();
            String id = user == null ? "" : user.id;
            String myId = GlobalUserState.getGlobalState().getUserId();
            if (!TextUtils.isEmpty(myId) && TextUtils.equals(myId, id)) {
                listMsg.setSelection(getCount() - 1);
            }
        }
    }

    /**
     * 展示大厅消息
     */
    private void showLobbyMessage(long key) {
        //这里清空所有消息，添加新的，考虑更好策略
        messageList.clear();
        this.messageList.addAll(getLobbyMsg());
        notifyDataSetChanged();

        //已经在底部就刷新底部
        if (isScrollToBottom) {
            listMsg.setSelection(getCount() - 1);
        }

        //自己发的消息就滚动到底部
        SystemMessageItem latestMessage = VoiceRoomDataSourceManager.getInstance().getLatestMessage(key);
        //额外判空处理
        if (latestMessage != null) {
            EnterRoomResult.EnterRoomUserItem user = latestMessage.getUser();
            String id = user == null ? "" : user.id;
            String myId = GlobalUserState.getGlobalState().getUserId();
            if (!TextUtils.isEmpty(myId) && TextUtils.equals(myId, id)) {
                listMsg.setSelection(getCount() - 1);
            }
        }
    }

    /**
     * 获取大厅邀请消息列表
     */
    private Collection<SystemMessageItem> getLobbyMsg() {
        Collection<SystemMessageItem> lobbyMsg = new ArrayList<>();
        Collection<SystemMessageItem> messageList = VoiceRoomDataSourceManager.getInstance().getMessageList();
        for (SystemMessageItem item : messageList) {
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                lobbyMsg.add(item);
            }
        }
        return lobbyMsg;
    }

    /**
     * 屏蔽聊天消息，只显示游戏邀请消息
     */
    public void hideChatMessage() {
        restoreMessage(getLobbyMsg());
    }

    /**
     * 展示所有消息
     */
    public void showAllMessage() {
        restoreMessage(VoiceRoomDataSourceManager.getInstance().getMessageList());
    }

    //小窗回来时复现之前的消息
    public void restoreMessage(Collection<SystemMessageItem> cMessageList) {
        messageList.clear();
        messageList.addAll(cMessageList);
        notifyDataSetChanged();
        listMsg.setSelection(getCount() - 1);
    }

    @Override
    public int getCount() {
        return messageList == null ? 0 : messageList.size();
    }

    @Override
    public Object getItem(int i) {
        return messageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View container, ViewGroup viewGroup) {

        View contentView = container == null ? LayoutInflater.from(mContext).inflate(R.layout.item_lobby_msg, null) : container;

        ViewHolder holder = (ViewHolder) contentView.getTag();
        if (holder == null) {
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        }

        SystemMessageItem message = messageList.get(i);
        switch (message.getType()) {
            case SystemMessageItem.TYPE_SYSTEM:
                holder.llMemberContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llSystemContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.VISIBLE);
                holder.tvSystemMsg.setText(message.getMsg());
                holder.tvSystemMsg.setTextColor(message.getColor());

                holder.llLobbyMessageContainer.setVisibility(View.GONE);
                break;
            case SystemMessageItem.TYPE_MEMBER:
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.VISIBLE);
                holder.llMsgContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.VISIBLE);
                holder.tvName.setText(message.getName());
                holder.tvMsg.setText(message.getMsg());
                holder.tvMsg.setTextColor(message.getColor());
                holder.tvMsg.setPadding(PADDING, PADDING, PADDING, PADDING);
                holder.tvMsg.setBackgroundResource(R.drawable.bg_msg);
                holder.ivPosition.setImageResource(findIdByPosition(message.getNumber() - 1));
                holder.llLobbyMessageContainer.setVisibility(View.GONE);
                boolean isGift = false;
                if (!TextUtils.isEmpty(message.getExhibitionType())) {
                    if (EffectsManager.SupportThisGiftType(message.getExhibitionType())) {
                        holder.ivGift.setVisibility(View.VISIBLE);
                        EffectsManager.FillGiftImageView(message.getExhibitionType(), holder.ivGift);
                        isGift = true;
                    } else {
                        holder.ivGift.setVisibility(View.INVISIBLE);
                        isGift = true;
                    }
                } else {
                    holder.ivGift.setVisibility(View.GONE);
                    isGift = false;
                }
                if (!isGift) {
                    EnterRoomResult.EnterRoomUserItem userItem = message.getUser();
                    if (userItem != null) {
                        holder.tvName.handleVipInfo(userItem.vipInfo);
                        if (userItem != null && userItem.message_box != null && !TextUtils.isEmpty(userItem.message_box.message_box_type)) {
                            MsgDecorManiFest.MsgDecorManiFestItem item = EffectsManager.GetMsgDecorItem(userItem.message_box.message_box_type);
                            if (item != null) {
                                MsgDecorToolKit.setNinePatchForViewBackGround(holder.tvMsg, holder.tvMsg, item.animate);
                            }
                        }
                    }
                }
                break;
            case SystemMessageItem.TYPE_SMALL_GAME:
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llMsgContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.VISIBLE);
                holder.llScoreContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.tvName.setText(message.getName());
                holder.ivPosition.setImageResource(findIdByPosition(message.getNumber() - 1));
                holder.ivScore.setImageResource(GameScoreReflection.scoreToImageId(message.getScore()));

                holder.llLobbyMessageContainer.setVisibility(View.GONE);
                break;
            case SystemMessageItem.TYPE_SPY_MSG:
                holder.llMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.VISIBLE);

                holder.tvSpyMsgTitle.setText(message.getName());
                holder.tvSpyMsg.setText(message.getMsg());
                holder.tvSpyMsg.setGravity(message.getGravity());

                holder.llLobbyMessageContainer.setVisibility(View.GONE);
                break;
            case SystemMessageItem.TYPE_LOBBY:
                //大厅消息
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);

                holder.llMemberContainer.setVisibility(View.VISIBLE);
                holder.llMsgContainer.setVisibility(View.GONE);
                holder.llLobbyMessageContainer.setVisibility(View.VISIBLE);

                //默认按照没有leave type的走
                holder.llLobbyMessageContainer.setBackgroundResource(R.drawable.bg_msg);
                holder.tvName.setText(message.getName());
                holder.ivPosition.setImageResource(findIdByPosition(message.getNumber()));
                holder.tvGameMessage.setText(message.getMsg());

                holder.btnAccept.setVisibility(View.VISIBLE);
                holder.ivSender.setVisibility(View.GONE);
                holder.ivAccepter.setVisibility(View.GONE);

                //去参加小游戏了，刷新UI为对战中
                if (TextUtils.equals(message.getLeaveType(), LobbyRoomConstants.PLAY_GAME)) {
                    holder.llLobbyMessageContainer.setBackgroundResource(R.drawable.bg_msg_lobby_player_leave);
                    holder.tvGameMessage.setText(R.string.str_lobby_pk_ing);
                    EnterRoomResult.EnterRoomUserItem user = message.getUser();
                    String sendAvatar = user == null ? "" : user.avatar;
                    PicassoUtils.loadImageByDefault(MainApplication.getInstance(), sendAvatar, holder.ivSender, R.mipmap.default_head);
                    LobbyAcceptGameBean.Acceptor acceptor = message.getAcceptor();
                    String acceptAvatar = acceptor == null ? "" : acceptor.avatar;
                    PicassoUtils.loadImageByDefault(MainApplication.getInstance(), acceptAvatar, holder.ivAccepter, R.mipmap.default_head);

                    holder.btnAccept.setVisibility(View.GONE);
                    holder.ivSender.setVisibility(View.VISIBLE);
                    holder.ivAccepter.setVisibility(View.VISIBLE);
                } else {
                    //此人进入了游戏大厅
                    if (TextUtils.equals(message.getLeaveType(), LobbyRoomConstants.JOIN_LOBBY)) {
                        //do nothing
                    }
                }

                //添加默认图片
                PicassoUtils.loadImageByDefault(MainApplication.getInstance(), message.getIcon(), holder.ivGameType, R.mipmap.ic_default_mini_game_bg);

                break;
            default:
                break;
        }
//        if (!TextUtils.isEmpty(message.getExhibitionType())) {
//            if (EffectsManager.SupportThisGiftType(message.getExhibitionType())) {
//                holder.ivGift.setVisibility(View.VISIBLE);
//                EffectsManager.FillGiftImageView(message.getExhibitionType(), holder.ivGift);
//            } else {
//                holder.ivGift.setVisibility(View.INVISIBLE);
//            }
//        } else {
//            holder.ivGift.setVisibility(View.GONE);
//        }

        if (onNameClickListener != null) {
            EnterRoomResult.EnterRoomUserItem user = message.getUser();
            final String id = user == null ? "" : user.id;
            holder.llNameContainer.setOnClickListener(v -> onNameClickListener.onNameClick(id));
        }

        //接受观战
        EnterRoomResult.EnterRoomUserItem user = message.getUser();
        final String id = user == null ? "" : user.id;
        if (PositionHelper.isSelf(id)) {
            holder.btnAccept.setBackgroundResource(R.drawable.bg_lobby_accept_button_ban);
            holder.btnAccept.setClickable(false);
        } else {
            holder.btnAccept.setBackgroundResource(R.drawable.bg_lobby_accept_button);
            holder.btnAccept.setOnClickListener(v -> {
                //点击的时候再加个判断，当前用户是否在大厅
                if (LobbyRoomHelper.isInRoom(id)) {
                    //通知大厅server此消息被接受
                    LobbyRoomSocketHelper.INSTANCE.sendAcceptGame(message.getInvite_id(), message.getGameType(), id, message.getUuid());
                    //通知小游戏server接受并进行小游戏
                    GlobalSocketEngine.INSTANCE.sendAccept(message.getGameType(), id, message.getUuid());
                } else {
                    ToastToolKit.showShort(R.string.str_voice_already_leave);
                }
            });
        }

        return contentView;
    }

    private int findIdByPosition(int position) {
        if (position == VoiceConstants.MASTER_POSITION) {
            return R.mipmap.voice_tag_master;
        }

        if (position <= LobbyRoomConfig.maxSeatPosition) {
            try {
                return R.mipmap.class.getDeclaredField("voice_tag_" + position).getInt(null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return R.mipmap.voice_tag_other;
    }

    public void destroy() {
        messageList.clear();
        messageList = null;
    }

    class ViewHolder {
        View llMemberContainer;
        View llNameContainer;
        ImageView ivPosition;
        VipNameView tvName;

        View llMsgContainer;
        TextView tvMsg;
        ImageView ivGift;

        View llSystemContainer;
        TextView tvSystemMsg;

        View llScoreContainer;
        ImageView ivScore;

        View llSpyMsgContainer;
        TextView tvSpyMsgTitle;
        TextView tvSpyMsg;

        View llLobbyMessageContainer;
        ImageView ivGameType;
        TextView tvGameMessage;
        Button btnAccept;
        ImageView ivSender;
        ImageView ivAccepter;

        public ViewHolder(View contentView) {
            llMemberContainer = contentView.findViewById(R.id.ll_member_tag);
            llNameContainer = contentView.findViewById(R.id.ll_name_container);
            ivPosition = (ImageView) contentView.findViewById(R.id.iv_item_position);
            tvName = (VipNameView) contentView.findViewById(R.id.tv_item_name);

            llMsgContainer = contentView.findViewById(R.id.ll_msg_container);
            tvMsg = (TextView) contentView.findViewById(R.id.tv_item_msg);
            ivGift = (ImageView) contentView.findViewById(R.id.iv_item_gift);

            llSystemContainer = contentView.findViewById(R.id.ll_system_tag);
            tvSystemMsg = (TextView) contentView.findViewById(R.id.tv_item_system_msg);

            llScoreContainer = contentView.findViewById(R.id.ll_score_tag);
            ivScore = (ImageView) contentView.findViewById(R.id.iv_item_score);

            llSpyMsgContainer = contentView.findViewById(R.id.ll_spy_member_tag);
            tvSpyMsgTitle = (TextView) contentView.findViewById(R.id.tv_spy_item_name);
            tvSpyMsg = (TextView) contentView.findViewById(R.id.tv_spy_item_msg);

            llLobbyMessageContainer = contentView.findViewById(R.id.ll_lobby_msg_container);
            ivGameType = (ImageView) contentView.findViewById(R.id.iv_lobby_icon);
            tvGameMessage = (TextView) contentView.findViewById(R.id.tv_lobby_message);
            btnAccept = (Button) contentView.findViewById(R.id.btn_lobby_accept);
            ivSender = (ImageView) contentView.findViewById(R.id.iv_lobby_head_sender);
            ivAccepter = (ImageView) contentView.findViewById(R.id.iv_lobby_head_accepter);

            tvMsg.setOnLongClickListener(v -> {
                ClipboardManager cmb = (ClipboardManager) MainApplication.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
                cmb.setText(tvMsg.getText().toString().trim());
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_copy));

                return true;
            });
        }
    }

    public interface OnNameClickListener {
        void onNameClick(String id);
    }

}
