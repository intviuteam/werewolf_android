package orangelab.project.voice.gif;

/**
 * Created by lloydfinch on 31/10/2017.
 */

public class CommentExtension {
    public byte[] bytes;
    public int size;

    public CommentExtension(byte[] bytes, int offset) {
        int blockSize;
        // get size
        size = 0x02;

        blockSize = bytes[offset + size] & 0xFF;
        size += 1;
        while (blockSize != 0x00) {
            size += blockSize;
            blockSize = bytes[offset + size] & 0xFF;
            size += 1;
        }

        this.bytes = new byte[size];
        System.arraycopy(bytes, offset, this.bytes, 0, size);
    }
}
