package orangelab.project.voice.component;

import android.content.Context;
import android.graphics.PointF;
import android.text.Html;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import orangelab.project.common.effect.EffectsUtils;
import orangelab.project.voice.activity.RedpacketActivity;
import orangelab.project.common.effect.AnimationEvent;

import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.game.model.WereWolfGiftRebate;
import cn.intviu.support.GsonHelper;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.LockResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.adapter.MessageAdapter;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceClickMsgDialog;
import orangelab.project.voice.dialog.VoiceFreeStyleDialog;
import orangelab.project.voice.dialog.VoiceFriendDialog;
import orangelab.project.voice.dialog.VoiceSystemDialog;
import orangelab.project.voice.dialog.VoiceVictoryOrFailureDialog;
import orangelab.project.voice.element.RoomChair;
import orangelab.project.voice.element.VoiceChair;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.SmallGameManager;
import orangelab.project.voice.manager.VoiceRoomManager;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.model.VoiceSAFBean;
import orangelab.project.voice.model.VoiceSimpleUserBean;
import orangelab.project.voice.model.VoiceSpyBeforeVote;
import orangelab.project.voice.model.VoiceSpyGameDeathInfo;
import orangelab.project.voice.model.VoiceSpyGameOverBean;
import orangelab.project.voice.model.VoiceSpySpeech;
import orangelab.project.voice.model.VoiceSpyVoteResult;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;
import orangelab.project.voice.musiccompany.concrete.MusicManager;
import orangelab.project.voice.dialog.RedpacketDialog;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public class VoiceMainView extends AbstractSocketMessageHandler implements VoiceMainViewComponent {

    private static final String TAG = "VoiceMainView";
    private VoiceMainViewUI voiceMainViewUI;
    private SparseArray<RoomChair> voiceChairs = new SparseArray<>();

    private List<SystemMessageItem> messageList;
    private MessageAdapter adapter;

    private Context mContext;
    private String type;
    private RedpacketDialog redpacketDialog;

    private boolean mIsChatMessageHidden;
    private boolean mIsGiftMessageHidden;
    private boolean mIsJoinMessageHidden;

    //预处理器
    private AbstractSocketMessageHandler preHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handlePreDownSeat(int position) {
            if (PositionHelper.isUpSeat(position)) {
                RoomChair oldChair = voiceChairs.get(position);
                if (oldChair != null) {
                    oldChair.downSeat(VoiceConstants.TEMPORARY_POSITION);
                    EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
                }
            }
        }

        @Override
        public void destroy() {

        }
    };

    public VoiceMainView(Context context, ViewGroup contentView) {
        this.mContext = context;
        voiceMainViewUI = new VoiceMainViewUI(context, contentView);
        init();
    }

    private void init() {
        initChairs();
        initUIEvent();
        initData();

        //添加预处理器，可以在更改数据前进行特殊操作
        VoiceServerMessageHandler.getInstance().registerPreHandler(preHandler);
    }

    private void initChairs() {

        for (int position = 0; position <= VoiceConstants.MAX_CHAIR_16; position++) {
            try {
                int chairId = R.id.class.getField("voice_member" + position).getInt(null);
                PLog.e(TAG, "" + chairId);
                ViewGroup chairContentView = voiceMainViewUI.findChairUIById(chairId);
                voiceChairs.put(position, new VoiceChair(chairContentView, position));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private void initUIEvent() {
        voiceMainViewUI.setOnSaveTitleClickedListener(v -> {
            String title = ((EditText) v).getText().toString().trim();
            if (!TextUtils.isEmpty(title) && title.length() >= 4) {
                saveTitle(title, type);
                hideChangeTitleUI();
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_tip_title_length));
            }
        });

        RxToolKit.Build(this, ViewEvent.RequestAddFriendEvent.class)
                .action(requestAddFriendEvent -> {
                    int position = findUserById(requestAddFriendEvent.getToUserId()).position;
                    RoomSocketEngineHelper.sendRequestFriendInGame(requestAddFriendEvent.getToUserId(), position);
                })
                .register();

        RxToolKit.Build(this, VoiceEvent.FirstComeIn.class)
                .action(firstComeIn -> {
                    voiceMainViewUI.onResume();
                })
                .register();
    }

    private void initData() {
        messageList = new ArrayList<>();
        adapter = new MessageAdapter(mContext, voiceMainViewUI.getListMsg(), messageList);
        adapter.setOnNameClickListener(id -> {
            //只有房主点击不是自己的人才有效
            if (PositionHelper.isMaster() && !PositionHelper.isSelf(id)) {
                EnterRoomResult.EnterRoomUserItem user = findUserById(id);
                //点击的人没走才有效
                if (user != null) {
                    new VoiceClickMsgDialog(mContext, user).show();
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
                }
            } else {
                PLog.e(TAG, "useless click!");
            }
        });
        voiceMainViewUI.getListMsg().setAdapter(adapter);

        voiceMainViewUI.getBtnRedPacket().setOnClickListener(v -> {
            redpacketDialog = new RedpacketDialog(mContext);
            redpacketDialog.show();
        });
    }

    @Override
    public void displayData() {

        //处理加锁位置
        List<Integer> lockedSeatList = VoiceRoomDataSourceManager.getInstance().getLockedSeatList();
        if (lockedSeatList != null) {
            for (Integer position : lockedSeatList) {
                if (PositionHelper.isUpSeat(position)) {
                    voiceChairs.get(position).lockSeat();
                }
            }
        }

        //处理麦上用户
        List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        if (onLineUsers != null) {
            for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
                if (PositionHelper.isUpSeat(user.position)) {
                    voiceChairs.get(user.position).upSeat(user);
                }
            }
        }

        int likeCount = VoiceRoomDataSourceManager.getInstance().getLikeCount();
        initLikeCount(likeCount);
    }

    @Override
    public void initLikeCount(int likeCount) {
        voiceMainViewUI.initLikeCount(likeCount);
    }

    @Override
    public void initMembers(EnterRoomResult.EnterRoomMessage enterRoomResult) {
        for (Integer i : enterRoomResult.locked_positions) {
            if (i <= VoiceRoomConfig.getMaxChairNumber()) {
                voiceChairs.get(i).lockSeat();
            }
        }
        Iterator iterator = enterRoomResult.users.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            EnterRoomResult.EnterRoomUserItem member = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            if (member == null) {
                continue;
            }
            if (member.position <= VoiceRoomConfig.getMaxChairNumber()) {
                RoomChair chair = voiceChairs.get(member.position);
                if (chair != null) {
                    chair.upSeat(member);
                }
            }
        }
        voiceMainViewUI.showMasterUI();
    }

    @Override
    public void showChangeTitleUI(String title, String type) {
        voiceMainViewUI.showChangeTitleUI(title, type);
    }

    @Override
    public void hideChangeTitleUI() {
        voiceMainViewUI.hideChangeTitleUI();
    }

    @Override
    public void updateMasterUI() {
        voiceMainViewUI.showMasterUI();
    }

    @Override
    public void saveTitle(String title, String type) {
        this.type = type;
        //如果当前是ktv房间，发送一个停止唱歌的消息
        if (VoiceRoomConfig.isIsKTV()) {
            MusicSocketHelper.INSTANCE.sendStopSing();
        }
        RoomSocketEngineHelper.sendUpdateTitle(title, type);
    }

    @Override
    public void updateSocketState(boolean isConnected) {
        if (isConnected) {
            adapter.refreshMessageList(VoiceConstants.CODE_CONNECT);
        } else {
            adapter.refreshMessageList(VoiceConstants.CODE_DISCONNECT);
        }
    }

    @Override
    public void setOnChangeTitleClickedListener(View.OnClickListener onChangeTitleClickedListener) {
        voiceMainViewUI.setOnChangeTitleClickedListener(onChangeTitleClickedListener);
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getOnLineMembers() {
        return VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
    }

    @Override
    public void destroy() {
        VoiceServerMessageHandler.getInstance().unRegisterPreHandler(preHandler);
        RxToolKit.UnRegister(this);
        voiceMainViewUI.destroy();
        messageList.clear();
        messageList = null;
        adapter.destroy();
        adapter = null;
        destroyChairs();
        destroyDialog();
        SmallGameManager.getInstance().destroy();
    }

    @Override
    public void hideChatMessage(boolean status) {
        mIsChatMessageHidden = status;
    }

    @Override
    public void hideJoinMessage(boolean status) {
        mIsJoinMessageHidden = status;
    }

    @Override
    public void hideGiftMessage(boolean status) {
        mIsGiftMessageHidden = status;
    }

    private void destroyChairs() {
        for (int i = 0; i <= VoiceRoomConfig.getMaxChairNumber(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.destroy();
            }
        }
        voiceChairs.clear();
        voiceChairs = null;
    }

    private void destroyDialog() {
        if (msgDialog != null) {
            if (msgDialog.isShowing()) {
                msgDialog.dismiss();
            }
        }

        if (freeStyleDialog != null) {
            if (freeStyleDialog.isShowing()) {
                freeStyleDialog.dismiss();
            }
        }

        if (redpacketDialog != null) {
            if (redpacketDialog.isShowing()) {
                redpacketDialog.dismiss();
            }
        }
    }

    @Override
    public void handleServerResponseEvent(ServerResponseEventOver event) {
        handleResponseEvent(event);
    }

    @Override
    public void handleServerEvent(ServerMessageEventOver event) {
        PLog.e(TAG, "event:" + event);
        handleMessageEvent(event);
    }

    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson
                (messageBean.payload.toString(), ServerMessageEnterResult.class);
        EnterRoomResult.EnterRoomUserItem member = TransferClassHelper.EnterResult2EnterRoomUser(item);
        PLog.e("test", "key:" + messageBean.msg_id);
        if (!mIsJoinMessageHidden) {
            adapter.refreshMessageList(messageBean.msg_id);
        }
        if (member.position <= VoiceRoomConfig.getMaxChairNumber()) {
            voiceChairs.get(member.position).upSeat(member);
        }
        voiceMainViewUI.showMasterUI();
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        adapter.refreshMessageList(messageBean.msg_id);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            voiceChairs.get(position).downSeat(VoiceConstants.TEMPORARY_POSITION);
            EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleLocked(VoiceMessageBean messageBean) {
        //unlock
        for (int i = 0; i <= VoiceRoomConfig.getMaxChairNumber(); i++) {
            voiceChairs.get(i).unLockSeat();
        }
        //lock
        LockResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), LockResult.class);
        for (Integer i : result.locked_positions) {
            if (i <= VoiceRoomConfig.getMaxChairNumber()) {
                voiceChairs.get(i).lockSeat();
            }
        }
    }

    @Override
    public void handleForceSeat(VoiceMessageBean messageBean) {
        handleUpSeat(messageBean);
    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user == null) {
            return;
        }
        //从老位置下来
        if (PositionHelper.isUpSeat(user.position)) {
            RoomChair oldChair = voiceChairs.get(user.position);
            if (oldChair != null) {
                oldChair.downSeat(VoiceConstants.TEMPORARY_POSITION);
            }
        }
        //坐到新位置
        int position = messageBean.payload.optInt(VoiceConstants.DEST_POSITION);
        if (PositionHelper.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.upSeat(user);
                adapter.refreshMessageList(messageBean.msg_id);
                EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
            }
        }
        voiceMainViewUI.showMasterUI();
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        adapter.refreshMessageList(messageBean.msg_id);
        voiceMainViewUI.showMasterUI();
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair chair = voiceChairs.get(position);
            chair.speak();
        }
    }

    @Override
    public void handleUnSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair chair = voiceChairs.get(position);
            chair.unSpeak();
        }
    }

    @Override
    public void handleKickOut(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        adapter.refreshMessageList(messageBean.msg_id);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            voiceChairs.get(position).downSeat(-1);
            voiceMainViewUI.showMasterUI();
            EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        if (PositionHelper.isMaster()) {
            return;
        }
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        //从老位置下来
        if (user.position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair oldChair = voiceChairs.get(user.position);
            oldChair.downSeat(-1);
        }
        adapter.refreshMessageList(messageBean.msg_id);
        //坐到房主位置
        voiceChairs.get(VoiceConstants.MASTER_POSITION).upSeat(user);
        voiceMainViewUI.showMasterUI();
    }

    @Override
    public void handleChat(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem item = findUserByPosition(position);
        if (item == null) {
            return;
        }
        //送礼
        JSONObject gift = messageBean.payload.optJSONObject(VoiceConstants.GIFT_CHAT);
        if (gift != null) {
            String type = gift.optString(Constant.DISPLAY_TYPE);
            sendGift(messageBean.msg_id, gift, type);
        } else {
            if (!mIsChatMessageHidden) {
                adapter.refreshMessageList(messageBean.msg_id);
            }
        }
    }

    private VoiceSystemDialog msgDialog;

    @Override
    public void handleSystemMsg(VoiceMessageBean messageBean) {
        if (messageBean.payload.has(VoiceConstants.TYPE)) {
            type = messageBean.payload.optString(VoiceConstants.TYPE);
            if (TextUtils.isEmpty(type)) {
                return;
            }
            switch (type) {
                case VoiceConstants.SYSTEM_MSG_TYPE_DIALOG:
                    String content = messageBean.payload.optString(VoiceConstants.CONTENT);
                    int duration = RoomSocketEngineHelper.getOptDurTime(messageBean.payload);
                    msgDialog = new VoiceSystemDialog(mContext, content);
                    msgDialog.showDialog(duration);
                    break;
                case VoiceConstants.SYSTEM_MSG_TYPE_TEXT:
                    adapter.refreshMessageList(messageBean.msg_id);
                    break;
                case VoiceConstants.SYSTEM_MSG_TYPE_TOAST:
                    String temp = messageBean.payload.optString(VoiceConstants.CONTENT);
                    RunnableFactory.createMsgToastRunnable(temp).run();
                    break;
            }
        }
    }

    @Override
    public void handleDisconnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (PositionHelper.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.updateStatus(VoiceConstants.USER_STATUS_OFFLINE);
            }
        }
    }

    @Override
    public void handleConnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (PositionHelper.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.reConnected();
            }
        }
    }


    private VoiceFreeStyleDialog freeStyleDialog;

    @Override
    public void handleRequestFreeMode(VoiceMessageBean messageBean) {
        //现在逻辑:删除旧的，展示新的
        if (freeStyleDialog != null) {
            if (freeStyleDialog.isShowing()) {
                freeStyleDialog.dismiss();
            }
            freeStyleDialog = null;
        }
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        int duration = messageBean.payload.optInt(VoiceConstants.DURATION) / 1000;
        if (PositionHelper.isSelfPosition(position)) {
            if (PositionHelper.isMaster(position)) {
                VoiceSocketHelper.acceptFreeMode();
            } else {
                freeStyleDialog = new VoiceFreeStyleDialog(mContext, duration, position);
                freeStyleDialog.show();
            }
        }
    }

    @Override
    public void handleAcceptFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        handleFreeStyle(position);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    @Override
    public void handleRejectFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    @Override
    public void handleUpdateConfig(VoiceMessageBean messageBean) {
        updateChairNumber();
    }

    @Override
    public void handlePurchasedResult(VoiceMessageBean messageBean) {
        //购买房契结果，只有自己能收到
        if (!TextUtils.isEmpty(messageBean.message)) {
            ToastToolKit.showShort(messageBean.message);
        }
    }

    @Override
    public void handleHandleOverCreatorResult(VoiceMessageBean messageBean) {
        if (messageBean.payload == null) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
            return;
        }
        String from = messageBean.payload.optString("from");
        String to = messageBean.payload.optString("to");
        if (TextUtils.isEmpty(from) || TextUtils.isEmpty(to)) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
            return;
        }
        if (!TextUtils.isEmpty(from) && PositionHelper.isSelf(from)) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
        } else if (!TextUtils.isEmpty(to) && PositionHelper.isSelf(to)) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_be_deed));
        }
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
        if (result.room_info != null) {
            PLog.e(TAG, "restore: " + result.room_info.toString());
            restoreConfig();
            restoreMembers(result.room_info);
            restoreSpeech(result.game_info);
            restoreDeath(result.game_info);
            voiceMainViewUI.initLikeCount(result.room_info.likeCount);
            voiceMainViewUI.showMasterUI();
        }
    }

    private void restoreConfig() {
        //更新一下房间座位数
        updateChairNumber();
    }

    @Override
    public void handleUpdateTitle(VoiceMessageBean messageBean) {
        type = messageBean.payload.optString(VoiceConstants.TYPE);
        adapter.refreshMessageList(messageBean.msg_id);
        if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_SPY, type)) {
            showChairReady();
            handleSpyStopConfig();
        } else {
            clearChairSpyInfo();
        }

        voiceMainViewUI.showMasterUI();

        //检测一下
        if (VoiceRoomConfig.needChangeUI) {
            for (int i = 0; i < voiceChairs.size(); i++) {
                RoomChair roomChair = voiceChairs.get(i);
                if (roomChair != null) {
                    roomChair.hideKTV();
                    roomChair.stopSing();
                }
            }
        }
    }

    @Override
    public void handleAddFriend(VoiceMessageBean messageBean) {
        //获取最后一条加好友消息，没有被处理才处理
        VoiceAddFriendBean bean = VoiceRoomDataSourceManager.getInstance().getLatestAddFriendBean();
        if (!bean.isHandle) {
            VoiceFriendDialog dialog = new VoiceFriendDialog(mContext, bean.id);
            dialog.show();
        }
    }

    @Override
    public void handleLikeRoom(VoiceMessageBean messageBean) {
        int count = messageBean.payload.optInt(VoiceConstants.VOICE_COUNT);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.likeRoom();
            }
        }
        voiceMainViewUI.likeRoom(count);

        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_CAMEL);
        EnterRoomResult.EnterRoomUserItem item = findUserById(userId);
        if (item == null) {
            return;
        }
        adapter.refreshMessageList(messageBean.msg_id);
    }

    @Override
    public void handleChangeState(VoiceMessageBean messageBean) {
        String model = messageBean.payload.optString(VoiceConstants.MODEL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        PLog.e(TAG, "uerState: " + userItem.state);
        if (TextUtils.equals(VoiceConstants.LIMIT, model)) {
            handleBanMic(position);
            adapter.refreshMessageList(messageBean.msg_id);
        } else if (TextUtils.equals(VoiceConstants.FREE, model)) {
            // 2018-02-05 不再接受老版本的自由模式消息
            handleFreeStyle(position);
            adapter.refreshMessageList(messageBean.msg_id);
        } else if (TextUtils.equals(VoiceConstants.SPACE_STR, model) || TextUtils.isEmpty(model)) {
            handleUnBanMic(position);
            handleUnFreeStyle(position);
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    private void handleBanMic(int position) {
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.banMic();
            }
        }
    }

    private void handleUnBanMic(int position) {
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.openMic();
            }
        }
    }

    private void handleFreeStyle(int position) {
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.openFreeStyle();
            }
        }
    }

    private void handleUnFreeStyle(int position) {
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.closeFreeStyle();
            }
        }
    }

    @Override
    public void handleEmotion(VoiceMessageBean messageBean) {
        String mark = messageBean.payload.optString(VoiceConstants.MARK);
        String url = messageBean.payload.optString(VoiceConstants.URL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                EffectsUtils.postEmotionEffects(
                        position,
                        roomChair.getEmotionHeadPositionOnScreen(),
                        roomChair.getEmotionHeadWidth(),
                        roomChair.getEmotionHeadHeight(),
                        mark,
                        url,
                        2000
                );
            }
        }
    }

    @Override
    public void handleSmallGame(VoiceMessageBean messageBean) {
        JSONObject data = messageBean.payload.optJSONObject(VoiceConstants.DATA);
        if (data != null) {
            int code = data.optInt(VoiceConstants.CODE);
            String msg = data.optString(VoiceConstants.MSG);
            if (code == VoiceConstants.BALANCE_NO_ENOUGH) {
                ToastToolKit.showShort(msg);
            } else if (code == VoiceConstants.NO_RECOGNIZED) {
                ToastToolKit.showShort(msg);
            } else {
                String mark = messageBean.payload.optString(VoiceConstants.MARK);
                int stayTime = data.optInt(VoiceConstants.STAY_TIME);
                int result = data.optInt(VoiceConstants.RES);
                int position = messageBean.payload.optInt(VoiceConstants.POSITION);
                EnterRoomResult.EnterRoomUserItem item = findUserByPosition(position);
                if (item == null) {
                    return;
                }
                if (position <= VoiceRoomConfig.getMaxChairNumber()) {
                    SmallGameManager.SmallGame game = new SmallGameManager.SmallGame(mark, result, stayTime);
                    RoomChair roomChair = voiceChairs.get(position);
                    try {
                        roomChair.showGame(game);
                        //不是ktv房间才有鼓掌的声音
                        if (!VoiceRoomConfig.isIsKTV()) {
                            String soundId = data.optString(VoiceConstants.SOUND);
                            VoiceRoomManager.playSound(mContext, soundId, stayTime);
                        }
                    } catch (Exception e) {
                        PLog.e("error", e.getMessage());
                    }
                }
                //打分,麦下的只显示提示信息
                if (TextUtils.equals(VoiceConstants.POINTS, mark)) {
                    adapter.refreshMessageList(messageBean.msg_id);
                }
            }
        }
    }

    @Override
    public void handleSpyStartConfig() {
        RoomChair chair = voiceChairs.get(VoiceConstants.MASTER_POSITION);
        if (chair != null) {
            chair.updateStatus(VoiceConstants.MASTER_STATUS_SETTING);
        }
    }

    @Override
    public void handleSpyUpdateConfig(VoiceMessageBean messageBean) {
        handleSpyStopConfig();
    }

    @Override
    public void handleSpyStopConfig() {
        RoomChair chair = voiceChairs.get(VoiceConstants.MASTER_POSITION);
        if (chair != null) {
            if (VoiceRoomConfig.SpyConfig.isIsConfiged()) {
                chair.updateStatus(VoiceConstants.MASTER_STATUS_SETTING_OVER);
            } else {
                chair.hideStatus();//隐藏设置中按钮
            }
        }
    }

    @Override
    public void handleSpyGamePrepare(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (PositionHelper.isPlayer(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.updateStatus(VoiceConstants.USER_STATUS_PREPARE);
            }
        }
    }

    @Override
    public void handleSpyGameUnPrepare(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (PositionHelper.isPlayer(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.unPrepare();
                chair.tipPrepare();
            }
        }
    }

    @Override
    public void handleSpyGameStart(VoiceMessageBean messageBean) {
        if (!VoiceRoomConfig.isIsPlaying()) {
            return;
        }
        //游戏开始不能设置话题
        voiceMainViewUI.hideChangeTitle();
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.hideReady();
                chair.hideStatus();
            }
        }
    }

    @Override
    public void handleSpyGameSpeech(VoiceMessageBean messageBean) {
        hideEndSpeech();
        VoiceSpySpeech spySpeech = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpySpeech.class);
        PLog.e(TAG, spySpeech.toString());
        if (!spySpeech.dead) {
            RoomChair chair = voiceChairs.get(spySpeech.position);
            if (chair != null) {
                chair.turns(spySpeech.duration / 1000);
            }
        }
    }

    @Override
    public void handleSpyGameEndSpeech(VoiceMessageBean messageBean) {
        VoiceSpySpeech spySpeech = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpySpeech.class);
        PLog.e(TAG, spySpeech.toString());
        if (!spySpeech.dead) {
            RoomChair chair = voiceChairs.get(spySpeech.position);
            if (chair != null) {
                chair.endSpeech();
            }
        }
    }

    @Override
    public void handleBeforeVote(VoiceMessageBean messageBean) {
        hideEndSpeech();
        VoiceSpyBeforeVote beforeVote = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyBeforeVote.class);
        if (beforeVote.alives != null && beforeVote.alives.size() > 0) {
            //只能给未出局的人投票
            for (int i : beforeVote.alives) {
                RoomChair chair = voiceChairs.get(i);
                if (chair != null) {
                    //更新存活的人状态：投票中
                    chair.updateStatus(VoiceConstants.USER_STATUS_VOTE);
                    //自己是玩家，没出局，对方是玩家，没出局，显示给对方投票的按钮
                    if (PositionHelper.isPlayer() && !PersonalDataHelper.getPersonalData().isOut()
                            && PositionHelper.isPlayer(i)) {
                        EnterRoomResult.EnterRoomUserItem item = findUserByPosition(i);
                        if (item != null && !item.isOut) {
                            chair.showVote(beforeVote.duration);
                        }
                    }
                }
            }
        } else {
            //房主视角，没有alives
        }
    }

    private void hideEndSpeech() {
        for (int i = 1; i <= VoiceRoomConfig.getMaxChairNumber(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.endSpeech();
            }
        }
    }

    @Override
    public void handleSpyGameOver(VoiceMessageBean messageBean) {
        VoiceSpyGameOverBean gameOverBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameOverBean.class);
        String spyWord = "";
        String goodWord = "";
        List<VoiceSpyGameOverBean.RoleInfo> undercover = gameOverBean.roles.undercover;
        List<VoiceSpyGameOverBean.RoleInfo> people = gameOverBean.roles.people;

        if (people != null && people.size() > 0) {
            goodWord = people.get(0).detail;
        }

        if (undercover != null && undercover.size() > 0) {
            VoiceSpyGameOverBean.RoleInfo roleInfo = undercover.get(0);
            if (roleInfo != null) {
                spyWord = roleInfo.detail;
            }
            if (TextUtils.isEmpty(spyWord)) {
                //说明卧底为幽灵
                if (gameOverBean.words != null) {
                    if (TextUtils.equals(goodWord, gameOverBean.words.first)) {
                        spyWord = gameOverBean.words.second;
                    } else {
                        spyWord = gameOverBean.words.first;
                    }
                }
            }
        }

        boolean isSpy = false;//房主和麦下人都视为好人
        boolean isVictory = false;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        //游戏结束的一瞬间被挤出就不再处理
        if (personalData == null) {
            return;
        }
        if (gameOverBean.winners != null) {
            isVictory = gameOverBean.winners.contains(personalData.getSelfPosition());
        }
        String selfWord = personalData.getSelfRole();
        if (TextUtils.isEmpty(selfWord) && PositionHelper.isPlayer()) {
            //说明自己被设置为幽灵,自己是卧底
            isSpy = true;
        } else {
            if (TextUtils.equals(spyWord, selfWord)) {
                //自己是卧底
                isSpy = true;
            } else if (TextUtils.equals(goodWord, selfWord)) {
                //自己是好人
                isSpy = false;
            }
        }

        String win_type = gameOverBean.win_type;
        if (PositionHelper.isMaster()) {
            if (TextUtils.equals(VoiceConstants.SPY_TAG, win_type)) {
                isVictory = false;
            } else if (TextUtils.equals(VoiceConstants.GOOD_TAG, win_type)) {
                isVictory = true;
            }
        }

        try {
            List<VoiceSimpleUserBean> spyGameOverBeans = new ArrayList<>();
            for (VoiceSpyGameOverBean.RoleInfo bean : gameOverBean.roles.undercover) {
                EnterRoomResult.EnterRoomUserItem item = findUserByPosition(bean.position);
                if (PositionHelper.isSelf(item.id)) {
                    spyGameOverBeans.add(0, new VoiceSimpleUserBean(item.id, item.name, item.avatar, item.position));
                } else {
                    spyGameOverBeans.add(new VoiceSimpleUserBean(item.id, item.name, item.avatar, item.position));
                }
            }
            List<VoiceSimpleUserBean> goodGameOverBeans = new ArrayList<>();
            for (VoiceSpyGameOverBean.RoleInfo bean : gameOverBean.roles.people) {
                EnterRoomResult.EnterRoomUserItem item = findUserByPosition(bean.position);
                if (PositionHelper.isSelf(item.id)) {
                    goodGameOverBeans.add(0, new VoiceSimpleUserBean(item.id, item.name, item.avatar, item.position));
                } else {
                    goodGameOverBeans.add(new VoiceSimpleUserBean(item.id, item.name, item.avatar, item.position));
                }
            }

            VoiceSAFBean bean = new VoiceSAFBean();
            bean.isSpy = isSpy;
            bean.victory = isVictory;
            bean.spyWord = spyWord;
            bean.goodWord = goodWord;
            if (isSpy) {
                bean.first = spyGameOverBeans;
                bean.second = goodGameOverBeans;
            } else {
                bean.first = goodGameOverBeans;
                bean.second = spyGameOverBeans;
            }

            //只有玩家才可以看到胜利/失败界面
            if (PositionHelper.isPlayer()) {
                VoiceVictoryOrFailureDialog dialog = new VoiceVictoryOrFailureDialog(mContext);
                dialog.setOnConfirmClickedListener(v -> {
                    dialog.dismiss();
                });
                dialog.setData(bean);

                dialog.show();
            }
        } catch (Exception e) {
            PLog.e(TAG, "no user message!");
        }

        //处理系统消息
        adapter.refreshMessageList(messageBean.msg_id);
        //重开游戏
        reOpenGame();
    }

    private void reOpenGame() {
        //游戏结束可以设置话题
        voiceMainViewUI.showMasterUI();
        for (int i = 1; i < voiceChairs.size(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.resetStatus();
                chair.tipPrepare();
            }
        }
        //game over房主发送获取系统词库
        if (PositionHelper.isMaster()) {
            VoiceSocketHelper.sendSpyGetSystemWords();
        }
    }

    @Override
    public void handleSpyGameVoteResult(VoiceMessageBean messageBean) {
        VoiceSpyVoteResult voteResult = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyVoteResult.class);
        PLog.e(TAG, voteResult.toString());
        if (TextUtils.equals(voteResult.type, "death")) {
            //已投票/投票结束就隐藏投票按钮
            hideChairVote();
            //玩家本次投票的结果
            if (!voteResult.finished) {
                //被投票的人的集合
                Set<Integer> beVoted = voteResult.vote_info.keySet();
                for (int v : beVoted) {
                    if (PositionHelper.isPlayer(v)) {
                        RoomChair roomChair = voiceChairs.get(v);
                        if (roomChair != null) {
                            //清除上一次的投票信息
                            roomChair.clearVoteInfo();
                            //投给此人的人的集合
                            List<Integer> votes = voteResult.vote_info.get(v);
                            //展示本次投票信息
                            for (int position : votes) {
                                roomChair.beVote(position);
                            }
                        }
                    }
                }
            } else {
                //投票的最终结果
                for (int i = 1; i < voiceChairs.size(); i++) {
                    RoomChair roomChair = voiceChairs.get(i);
                    if (roomChair != null) {
                        roomChair.clearVoteInfo();
                        roomChair.hideStatus();
                    }
                }

                //处理系统消息
                if (voteResult.positions != null && voteResult.positions.size() > 0) {
                    //处理投票结果
                    StringBuilder sbVoteResult = new StringBuilder();
                    Set<Integer> beVoted = voteResult.vote_info.keySet();
                    for (int v : beVoted) {
                        List<Integer> voted = voteResult.vote_info.get(v);
                        StringBuilder votedSb = new StringBuilder();
                        for (int i : voted) {
                            votedSb.append(i).append("、");
                        }
                        if (v > 0) {
                            //正常投票
                            String votedStr = votedSb.deleteCharAt(votedSb.length() - 1).toString();
                            String voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg, votedStr, v);
                            sbVoteResult.append(voteInfo).append("\n");
                        } else {
                            //弃票
                            String votedStr = votedSb.deleteCharAt(votedSb.length() - 1).toString();
                            String voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg_give_up_number, votedStr);
                            sbVoteResult.append(voteInfo).append("\n");
                        }

                    }

                    //有平票
                    if (voteResult.need_pk) {
                        //处理PK消息
                        if (voteResult.positions != null && voteResult.positions.size() >= 2) {
                            String message = "";
                            Collections.sort(voteResult.positions);
                            if (voteResult.positions.size() == 2) {
                                message = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold, voteResult.positions.get(0), voteResult.positions.get(1))).toString();
                            } else {
                                StringBuilder sb = new StringBuilder();
                                for (Integer position : voteResult.positions) {
                                    sb.append(position).append("、");
                                }
                                String votePositions = sb.deleteCharAt(sb.length() - 1).toString();
                                message = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold_more, votePositions)).toString();
                            }
                            //添加PK信息
                            sbVoteResult.append(message);
                        }
                    } else {
                        //有人出局会在handleDeathInfo()进行处理
                        //无PK，删除最后一个换行符
                        sbVoteResult.deleteCharAt(sbVoteResult.length() - 1);
                    }
                    adapter.refreshMessageList(messageBean.msg_id);
                } else {
                    //所有玩家弃票
                    adapter.refreshMessageList(messageBean.msg_id);
                }
            }
        }
    }

    @Override
    public void handleDeathInfo(VoiceMessageBean messageBean) {
        VoiceSpyGameDeathInfo deathInfo = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameDeathInfo.class);
        if (deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
            VoiceSpyGameDeathInfo.DeathInfo info = deathInfo.death_info.get(0);
            int outPosition = info.killed;
            if (PositionHelper.isPlayer(outPosition)) {
                RoomChair chair = voiceChairs.get(outPosition);
                if (chair != null) {
                    chair.out();
                }
            }

            //处理系统消息
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    //隐藏投票按钮
    private void hideChairVote() {
        for (int i = 1; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.hideVote();
            }
        }
    }

    private void showChairReady() {
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.tipPrepare();
            }
        }
    }

    //清除所有跟"谁是卧底"有关的数据
    private void clearChairSpyInfo() {
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.clearVoteInfo();
                roomChair.hideStatus();
                roomChair.hideVote();
                roomChair.hideReady();
            }
        }
    }

    @Override
    public void handleGuessWordResult(VoiceMessageBean messageBean) {
        adapter.refreshMessageList(messageBean.msg_id);
    }

    @Override
    public void handleAuthorizeSing(VoiceMessageBean messageBean) {
        try {
            //还原座位伴奏
            for (int i = 0; i < voiceChairs.size(); i++) {
                RoomChair roomChair = voiceChairs.get(i);
                if (roomChair != null) {
                    roomChair.hideKTV();
                }
            }

            //展示新的伴奏座位
            String id = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
            EnterRoomResult.EnterRoomUserItem user = findUserById(id);
            if (user != null) {
                if (PositionHelper.isUpSeat(user.position)) {
                    RoomChair roomChair = voiceChairs.get(user.position);
                    if (roomChair != null) {
                        roomChair.showKTV();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleRemoveSong(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user != null) {
            if (PositionHelper.isUpSeat(user.position)) {
                RoomChair chair = voiceChairs.get(user.position);
                if (chair != null) {
                    chair.hideKTV();
                    chair.stopSing();
                }
            }
        }
    }

    @Override
    public void handlePrepareSing(VoiceMessageBean messageBean) {

        //收到准备，就停止唱歌
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.stopSing();
            }
        }
    }

    @Override
    public void handleStartSing(VoiceMessageBean messageBean) {

        //隐藏所有伴奏标签
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.hideKTV();
            }
        }

        try {
            //设置新的演唱者头像旋转效果
            String singID = MusicCompanyConfig.singerId;
            EnterRoomResult.EnterRoomUserItem user = findUserById(singID);
            if (user != null) {
                if (PositionHelper.isUpSeat(user.position)) {
                    RoomChair roomChair = voiceChairs.get(user.position);
                    if (roomChair != null) {
                        roomChair.sing();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void handleStopSing(VoiceMessageBean messageBean) {
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair roomChair = voiceChairs.get(i);
            if (roomChair != null) {
                roomChair.hideKTV();
                roomChair.stopSing();
            }
        }
    }

    @Override
    public void handleSendPacket(VoiceMessageBean messageBean) {
        String packetId = messageBean.payload.optString(VoiceConstants.PACKET_ID);
        VoiceRoomConfig.setLastRedpacketId(packetId);
        RedpacketActivity.Launch(mContext);
    }

    private void sendGift(Long key, JSONObject gift, String displayType) {
        String type = gift.optString(VoiceConstants.GIFT_TYPE);
        if (!mIsGiftMessageHidden) {
            adapter.refreshMessageList(key);
        }
        /**
         * 如果送的是卡片，直接结束
         */
        if (EffectsManager.IsCardByGiftType(type)) {
            return;
        }

        int from = gift.optInt(VoiceConstants.GIFT_FROM);
        int to = gift.optInt(VoiceConstants.GIFT_TO);

        PointF start = null;
        PointF end = null;

        //麦上给麦上送
        if (from <= VoiceRoomConfig.getMaxChairNumber() && to <= VoiceRoomConfig.getMaxChairNumber()) {
            start = getGiftPosition(from);
            end = getGiftPosition(to);
        }

        //麦下给麦上送
        if (from > VoiceRoomConfig.getMaxChairNumber() && to <= VoiceRoomConfig.getMaxChairNumber()) {
            start = getGiftPosition(from);
            end = getGiftPosition(to);
        }

        if (start == null || end == null) {
            return;
        }
        /**
         * 这里是处理送礼物特效,处理默认数据
         */
        int fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        int toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;

        RoomChair chair = voiceChairs.get(to);//默认取终点
        int chairWidth = 0;
        int chairHeight = 0;
        if (chair != null) {
            chairWidth = chair.getHeadWidth();
            chairHeight = chair.getHeadHeight();
            //处理有房主的情况
            if (PositionHelper.isMaster(from) || PositionHelper.isMaster(to)) {
                if (from != to) {
                    RoomChair largeChair = voiceChairs.get(0);//房主位置
                    int largeWidth = largeChair.getHeadWidth();
                    int largeHeight = largeChair.getHeadHeight();

                    RoomChair smallChair = voiceChairs.get(1);//随便麦下位置
                    //chair宽高取最小值
                    chairWidth = smallChair.getHeadWidth();
                    chairHeight = smallChair.getHeadHeight();

                    int dx = (largeWidth - chairWidth) >> 1;
                    int dy = (largeHeight - chairHeight) >> 1;
                    if (PositionHelper.isMaster(from)) {
                        //起点是房主
                        start.x += dx;
                        start.y += dy;

                    } else if (PositionHelper.isMaster(to)) {
                        //终点是房主
                        end.x += dx;
                        end.y += dy;
                    }
                }
            }

            //处理一下坐标问题
            int dy = ScreenUtils.dip2px(13) >> 1;//11是用户名字的高度，加上默认的padding,取一半向上偏移
            //送礼的坐标处理，返现的不处理
            PointF start1 = new PointF(start.x, start.y - dy);
            PointF end1 = new PointF(end.x, end.y - dy);



            //KTV送礼的额外处理
            if (TextUtils.equals(displayType, Constant.DISPLAY_TYPE_SING)) {
                //表示是点击播放器送礼
                //终点就取播放器的头像
                if (VoiceRoomConfig.isIsKTV()) {
                    int[] location = {ScreenUtils.getScreenWidth() >> 1, ScreenUtils.getScreenHeight()};
                    start1 = new PointF(location[0], location[1]);
                    try {
                        end1 = MusicManager.Companion.getInstance().getAlbumPosition();
                        int delta = ((chairWidth - MusicManager.Companion.getInstance().getAlbumWidth()) >> 1);
                        end1 = new PointF(end1.x - delta, end1.y - delta);
                        //chairWidth = MusicManager.Companion.getInstance().getAlbumWidth();
                        //chairHeight = MusicManager.Companion.getInstance().getAlbumWidth();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

          // AnimationCommander.DispatchAnimEvent(mContext, GiftAnimConstant.GIFT_FROM_TYPE_VOICE, type, start1, end1, fromO, toO, chairWidth, chairHeight, from, to);
            EffectsManager.DispatchAnimEvent(mContext, GiftAnimConstant.GIFT_FROM_TYPE_VOICE, type, start1, end1, fromO, toO, chairWidth, chairHeight, from, to);
        }


        //返现处理
        try {
            if (EffectsManager.SupportThisGiftType(type)) {
                JSONObject giftJsonObj = gift;
                if (giftJsonObj.has("rebate")) {
                    /**
                     * 这里需要处理返现
                     */
                    WereWolfGiftRebate rebate = GsonHelper.getGson().fromJson(giftJsonObj.optJSONObject("rebate").toString(), WereWolfGiftRebate.class);
                    if (rebate != null) {
                        int dim = rebate.value;
                        String dimString = Integer.toString(dim);

                        if (TextUtils.isEmpty(dimString)) {
                            return;
                        }

                        if (PositionHelper.isUpSeat(to)) {//终点在麦上才展示
                            PLog.e("gift-rebate", "to:" + to);
                            EffectsManager.DispatchRebateAnimEvent(mContext, dimString, new PointF(start.x, start.y), new PointF(end.x, end.y), fromO, toO, chairWidth, chairHeight);
                        }

                        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(to);
                        if (!TextUtils.equals(userItem.id, GlobalUserState.getGlobalState().getUserId())
                                && !Utils.checkUserIdIsTourist(userItem.id)) {
                            /**
                             * 不能为游客，不能给自己
                             */
                            PLog.e("gift-rebate", "is not self and tourist");
                            LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(type, userItem.id, userItem.name, userItem.avatar, userItem.sex, dimString);
                        }
                    }
                } else {
                    /**
                     * ignore
                     */
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void restoreMembers(EnterRoomResult.EnterRoomMessage roomInfo) {
        resetChairs();
        restoreChairs(roomInfo);
    }

    //说话状态恢复
    private void restoreSpeech(RestoreResult.RestoreRoleMessage game_info) {
        if (game_info != null) {
            PLog.e(TAG, "restore: " + game_info.toString());
            if (game_info.speech_info != null && game_info.speech_info.current != null) {
                int position = game_info.speech_info.current.position;//此人正在说话
                int duration = game_info.speech_info.duration / 1000;
                if (PositionHelper.isPlayer(position)) {
                    RoomChair chair = voiceChairs.get(position);
                    if (chair != null) {
                        chair.turns(duration);
                    }
                }
            }
        }
    }

    //死亡信息恢复
    private void restoreDeath(RestoreResult.RestoreRoleMessage game_info) {
        if (game_info != null) {
            if (game_info.death_info != null && game_info.death_info.size() > 0) {
                for (RestoreResult.GameInfoDeathInfo deathInfo : game_info.death_info) {
                    if (PositionHelper.isPlayer(deathInfo.position)) {
                        RoomChair chair = voiceChairs.get(deathInfo.position);
                        if (chair != null) {
                            chair.out();
                        }
                    }
                }
            }
        }
    }

    private void resetChairs() {
        for (int position = 0; position < voiceChairs.size(); position++) {
            RoomChair chair = voiceChairs.get(position);
            chair.reset();
        }
    }

    private void restoreChairs(EnterRoomResult.EnterRoomMessage enterRoomResult) {
        for (Integer i : enterRoomResult.locked_positions) {
            if (i <= VoiceRoomConfig.getMaxChairNumber()) {
                voiceChairs.get(i).lockSeat();
            }
        }
        //KTV用另一种方式处理重连
        if (VoiceRoomConfig.isIsKTV()) {
            List<EnterRoomResult.EnterRoomUserItem> onLineMembers = getOnLineMembers();
            for (EnterRoomResult.EnterRoomUserItem member : onLineMembers) {
                if (member == null) {
                    continue;
                }
                if (member.position <= VoiceRoomConfig.getMaxChairNumber()) {
                    voiceChairs.get(member.position).upSeat(member);
                }
            }
        } else {
            Iterator iterator = enterRoomResult.users.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                EnterRoomResult.EnterRoomUserItem member = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
                if (member == null) {
                    continue;
                }
                if (member.position <= VoiceRoomConfig.getMaxChairNumber()) {
                    voiceChairs.get(member.position).upSeat(member);
                }
            }
        }
    }

    private PointF getGiftPosition(int position) {
        //在麦上的就取chair的位置
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair voiceChair = voiceChairs.get(position);
            if (voiceChair != null) {
                return voiceChair.getPositionOnScreen();
            }
        } else {
            //不在麦上的取屏幕右下角位置
            int[] location = {ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight()};
            PointF pointF = new PointF(location[0], location[1]);
            return pointF;
        }

        return null;
    }

    private void updateChairNumber() {
        voiceMainViewUI.updateChairNumber();
    }

    public EnterRoomResult.EnterRoomUserItem findUserByPosition(int position) {
        return VoiceRoomDataSourceManager.getInstance().findUserByPosition(position);
    }

    public EnterRoomResult.EnterRoomUserItem findUserById(String id) {
        return VoiceRoomDataSourceManager.getInstance().findUserById(id);
    }

    @Override
    public void onResume() {
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.onResume();
            }
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onReEnter() {
        if (adapter != null) {
            adapter.restoreMessage(VoiceRoomDataSourceManager.getInstance().getMessageList());
        }
    }
}
