package orangelab.project.voice.api;

import android.text.TextUtils;

import com.datasource.GlobalUserState;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.toolkit.action.ApiResult;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.exception.ApiFailedException;
import orangelab.project.common.exception.NetWorkException;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.model.VoiceRedPacketInfoBean;
import orangelab.project.voice.model.VoiceRedPacketListBean;
import orangelab.project.voice.model.VoiceRoomBackGroundApiResult;
import orangelab.project.voice.model.VoiceRoomConfigAPI;

/**
 * Created by lloydfinch on 28/11/2017.
 * <p>
 * 语音房API相关操作
 */

public class VoiceApiManager {

    /**
     * 获取语音房话题列表
     *
     * @param roomId 房间号
     * @param result 返回的json
     */
    public static void GetTopicList(String roomId, ApiResult<String> result) {
        JSONObject json = new JSONObject();
        try {
            json.put("room_id", roomId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create()
                .isGet(false)
                .request(VoiceConstants.GET_TOPIC_LIST)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .load(json)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        result.onResult(response, null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        result.onResult(failed, new Exception(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        result.onResult("", new NetWorkException(e));
                    }
                }).build().execute();
    }

    /**
     * 获取房间背景
     *
     * @param roomId    房间号
     * @param apiResult 返回的数据
     */
    public static void GetRoomBackGroundData(String roomId, ApiResult<VoiceRoomBackGroundApiResult> apiResult) {
        JSONObject json = new JSONObject();
        try {
            json.put("room_id", roomId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create()
                .isGet(false)
                .request(VoiceConstants.GET_BACKGROUND)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .load(json)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        VoiceRoomBackGroundApiResult result = GsonHelper.getGson().fromJson(response, VoiceRoomBackGroundApiResult.class);
                        if (apiResult != null) {
                            apiResult.onResult(result, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void ReplaceBackGroundImage(String roomId, String pre_image_id, String newUrl, String newImageId, ApiResult<String> result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room_id", roomId);
            jsonObject.put("pre_image_id", pre_image_id);
            jsonObject.put("url", newUrl);
            jsonObject.put("image_id", newImageId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request(VoiceConstants.REPLACE_BACKGROUND).withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (result != null) {
                            result.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (result != null) {
                            result.onResult("", new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (result != null) {
                            result.onResult("", new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    /**
     * 添加自定义背景
     *
     * @param roomId   房间号
     * @param image_id 上传的图片id
     * @param url      上传的图片路径
     * @param result   返回结果
     */
    public static void AddBackGroundImage(String roomId, String image_id, String url, ApiResult<String> result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room_id", roomId);
            jsonObject.put("image_id", image_id);
            jsonObject.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request(VoiceConstants.ADD_BACKGROUND).withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (result != null) {
                            result.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (result != null) {
                            result.onResult("", new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (result != null) {
                            result.onResult("", new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    /**
     * 删除上传的图片
     *
     * @param roomId  房间号
     * @param imageId 图片id
     * @param url     图片路径
     * @param result  返回结果
     */
    public static void RemoveBackGroundImage(String roomId, String imageId, String url, ApiResult<String> result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room_id", roomId);
            jsonObject.put("image_id", imageId);
            jsonObject.put("url", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request(VoiceConstants.REMOVE_BACKGROUND).withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (result != null) {
                            result.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (result != null) {
                            result.onResult("", new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (result != null) {
                            result.onResult("", new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void VoiceRoomConfigApi(String roomId, String roomType, ApiResult<VoiceRoomConfigAPI> result) {
        if (TextUtils.isEmpty(roomId) || TextUtils.isEmpty(roomType)) {
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room_id", roomId);
            jsonObject.put("type", roomType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject).request(VoiceConstants.VOICE_ROOM_CONFIG_API).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    VoiceRoomConfigAPI res = GsonHelper.getGson().fromJson(response, VoiceRoomConfigAPI.class);
                    if (result != null) {
                        result.onResult(res, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (result != null) {
                        result.onResult(null, new NetWorkException());
                    }
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.onResult(null, new ApiFailedException(code, failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

    public static void GetRedPacketHistoryList(String roomId, long startTime, int page, int limit, ApiResult<VoiceRedPacketListBean> result) {
        if (TextUtils.isEmpty(roomId)) {
            return;
        }
        Map<String,String> map=new HashMap<>();

        map.put("room_id", roomId);
        map.put("page", Integer.toString(page));
        map.put("limit", Integer.toString(limit));
        map.put("start_time", Long.toString(startTime));
        RequestTask.create().isGet(true).withToken(GlobalUserState.getGlobalState().getToken())
                .withGetPayload(map).request(VoiceConstants.GET_PACKET_LIST).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    VoiceRedPacketListBean res = GsonHelper.getGson().fromJson(response, VoiceRedPacketListBean.class);
                    if (result != null) {
                        result.onResult(res, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (result != null) {
                        result.onResult(null, new NetWorkException());
                    }
                }
            }
            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.onResult(null, new ApiFailedException(code, failed));
                }
            }
            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

    public static void GetRedPacketInfoList(String packetId, ApiResult<VoiceRedPacketInfoBean> result) {
        if (TextUtils.isEmpty(packetId)) {
            return;
        }
        Map<String,String> map=new HashMap<>();
        map.put("packet_id", packetId);
        RequestTask.create().isGet(true).withToken(GlobalUserState.getGlobalState().getToken())
                .withGetPayload(map).request(VoiceConstants.GET_PACKET_INFO).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    VoiceRedPacketInfoBean res = GsonHelper.getGson().fromJson(response, VoiceRedPacketInfoBean.class);
                    if (result != null) {
                        result.onResult(res, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (result != null) {
                        result.onResult(null, new NetWorkException());
                    }
                }
            }
            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.onResult(null, new ApiFailedException(code, failed));
                }
            }
            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

}
