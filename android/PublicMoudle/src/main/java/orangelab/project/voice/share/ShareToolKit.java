package orangelab.project.voice.share;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.ToastToolKit;
import com.networktoolkit.transport.Constant;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;

import cn.intviu.support.ShareTools;
import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.event.ShareWeChatEvent;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceInviteFriendDialog;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.model.VoiceDeedShareBean;
import orangelab.project.voice.model.VoiceSAFBean;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by lloydfinch on 28/11/2017.
 */

public class ShareToolKit {

    private static final String TAG = "ShareToolKit";

    private static ShareToolKit shareTool = new ShareToolKit();
    private static ShareBuilder mBuilder;
    private RxPermissions rxPermissions;


    private ShareToolKit() {
    }

    //不存在线程安全问题,不加同步锁提高效率
    private static ShareToolKit getInstance() {
        if (shareTool == null) {
            shareTool = new ShareToolKit();
        }
        return shareTool;
    }

    public static ShareToolKit build(ShareBuilder builder) {
        mBuilder = builder;
        return getInstance();
    }

    public static ShareToolKit create() {
        return getInstance();
    }

    //分享到微信
    public void shareToWeChat(Activity activity) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
        String title = mBuilder.getShareTitle();
        String message = mBuilder.getShareMsg();
        String url = mBuilder.getShareUrl();

        PLog.e(TAG, "shareToWeChat");
        if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
            if (isFromSetting()) {
                Utils.runSafely(() -> {
                    ThreadToolKit.Async(() -> shareSoftwareOnlyImage(SHARE_MEDIA.WEIXIN, activity));
                });
            } else {
                ThreadToolKit.Async(() ->
                        shareSoftware(SHARE_MEDIA.WEIXIN, activity, title, message, url));
            }
        }
        clearData();
    }

    //分享到朋友圈
    public void shareToMoments(Activity activity) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
        String title = mBuilder.getShareTitle();
        String message = mBuilder.getShareMsg();
        String url = mBuilder.getShareUrl();
        if (filterAppExit(ShareTools.SHARE_WEIXIN)) {
            if (isFromSetting()) {
                Utils.runSafely(() -> {
                    ThreadToolKit.Async(() -> shareSoftwareOnlyImage(SHARE_MEDIA.WEIXIN_CIRCLE, activity));
                });
            } else {
                ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.WEIXIN_CIRCLE, activity, title, message, url));
            }
        }
        clearData();
    }

    //分享到QQ
    public void shareToQQ(Activity activity) {
        if (rxPermissions == null) {
            rxPermissions = new RxPermissions(activity);
        }
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .onBackpressureBuffer(VoiceConstants.RX_BACK_PRESSURE)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);
                            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
                            String title = mBuilder.getShareTitle();
                            String mShareMessage = mBuilder.getShareMsg();
                            String url = mBuilder.getShareUrl();
                            if (filterAppExit(ShareTools.SHARE_QQ)) {
                                if (!TextUtils.isEmpty(mShareMessage)) {
                                    if (mShareMessage.length() > 60) {
                                        StringBuilder message = new StringBuilder();
                                        message.append(mShareMessage.substring(0, 57));
                                        message.append("...");
                                        mShareMessage = message.toString();
                                    }
                                }
                                if (isFromSetting()) {
                                    shareSoftwareOnlyImage(SHARE_MEDIA.QQ, activity);
                                } else {
                                    shareSoftware(SHARE_MEDIA.QQ, activity, title, mShareMessage, url);
                                }
                                String category = ShareMedia.getShareMedia().getType();
                                String type = VoiceConstants.SHARE_TO_QQ;
                                if (TextUtils.equals(category, VoiceConstants.INVITE)) {
                                    type = VoiceConstants.INVITE_TO_QQ;
                                }
                                shareSuccessReported(type);
                            }
                            clearData();
                        } else {
                            ToastToolKit.showShort(R.string.permission_read_store_error);
                        }
                    }
                });
    }

    //分享到Line
    public void shareToLine(Activity activity) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);

        String title = mBuilder.getShareTitle();
        String mShareMessage = mBuilder.getShareMsg();
        String url = mBuilder.getShareUrl();

        if (filterAppExit(ShareTools.SHARE_LINE)) {
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
            if (isFromSetting()) {
                shareSoftwareOnlyImage(SHARE_MEDIA.LINE, activity);
            } else {
                ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.LINE, activity, title, mShareMessage, url));
            }
        }
        clearData();
    }

    //分享到FaceBook
    public void shareToFaceBook(Activity activity) {
        ShareMedia.getShareMedia().setType(VoiceConstants.INVITE);
        String title = mBuilder.getShareTitle();
        String message = mBuilder.getShareMsg();
        String url = mBuilder.getShareUrl();
        if (filterAppExit(ShareTools.SHARE_FACEBOOK)) {
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
            if (isFromSetting()) {
                Utils.runSafely(() -> {
                    shareSoftwareOnlyImage(SHARE_MEDIA.FACEBOOK, activity);
                });
            } else {
                ThreadToolKit.Async(() -> shareSoftware(SHARE_MEDIA.FACEBOOK, activity, title, message, url));
            }
        }
        clearData();
    }

    //分享到游戏好友
    public void shareToGameFriend(Activity activity) {
        VoiceInviteFriendDialog friendDialog = new VoiceInviteFriendDialog(activity);
        friendDialog.show();
    }

    //分享到家族
    public void shareToFamily() {
        clearData();
    }

    public void shareToOther() {
        clearData();
    }

    private boolean isFromSetting() {
        //这里将从设置进入的分享页面功能还原
        return false;
    }

    private boolean filterAppExit(String app) {
        if (ShareTools.existsApp(MainApplication.getInstance(), app)) {
            return true;
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            PLog.e(TAG, "your phone hasn't install " + app);
            return false;
        }
    }

    private void shareSoftwareOnlyImage(SHARE_MEDIA share_media, Activity activity) {
        Utils.runSafely(() -> {
            UMImage image = new UMImage(activity.getApplicationContext(), R.mipmap.share_image_from_setting);
            image.compressStyle = UMImage.CompressStyle.QUALITY;
            new ShareAction(activity).withMedia(image).setPlatform(share_media).setCallback(mUmShareListener).share();
        });
    }

    private void shareSoftware(SHARE_MEDIA share_media, Activity activity, String shareTitle, String shareMessage, String shareUrl) {
        Bitmap shareICon = BitmapFactory.decodeResource(activity.getResources(), ProjectConfig.APP_SHARE_ICON);
        UMImage image = new UMImage(activity.getApplicationContext(), shareICon);
        UMWeb umWeb = new UMWeb((shareUrl));
        umWeb.setTitle(shareTitle);
        umWeb.setDescription(shareMessage);
        umWeb.setThumb(image);
        new ShareAction(activity)
                .withMedia(umWeb)
                .setPlatform(share_media)
                .setCallback(mUmShareListener)
                .share();
        PLog.e(TAG, "shareToWeChat1");

    }

    private UMShareListener mUmShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            PLog.i(TAG, "share: onStart" + share_media);
        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            PLog.i(TAG, "share to " + platform + " success");
            String category = ShareMedia.getShareMedia().getType();
            String type = VoiceConstants.SHARE_TO_QQ;
            if (TextUtils.equals(category, VoiceConstants.INVITE)) {
                type = VoiceConstants.INVITE_TO_QQ;
            }
            shareSuccessReported(type);
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            PLog.i(TAG, "error when share to " + platform.toString() + " info: " + t.getMessage());
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            PLog.i(TAG, "cancel when share to " + platform.toString());
        }
    };

    private void shareSuccessReported(String type) {
        ApiManager.DoShareApi(type, 1);
    }

    //分享图片
    public void shareVoiceDeedImage(Activity activity, VoiceDeedShareBean bean, SHARE_MEDIA share_media) {
        //标记为分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        final Bitmap bitmap = SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).getRoomDeedShareView(activity, bean);
        UMImage image = new UMImage(activity, bitmap);
        new ShareAction(activity)
                .setPlatform(share_media)
                .withMedia(image)
                .setCallback(new UMShareListener() {

                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onResult(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onResult: ");
                        //RxToolKit.Emit(new VoiceEvent.VoiceShareActivityFinishEvent());
                        releaseShareBitmap(bitmap);
                        shareSuccessReported(Constant.SHARE_TO_QQ);
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                        Log.i(TAG, "onError: ", throwable);
                        //RxToolKit.Emit(new VoiceEvent.VoiceShareActivityFinishEvent());
                        releaseShareBitmap(bitmap);
                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onCancel: ");
                        //RxToolKit.Emit(new VoiceEvent.VoiceShareActivityFinishEvent());
                        releaseShareBitmap(bitmap);
                    }
                }).share();
    }


    //谁是卧底分享图片
    public void shareSpyGameOverImage(Activity activity, VoiceSAFBean bean, SHARE_MEDIA share_media) {
        //标记为分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        final Bitmap bitmap = SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).getSpyShareView(activity, bean);
        UMImage image = new UMImage(activity, bitmap);
        new ShareAction(activity)
                .setPlatform(share_media)
                .withMedia(image)
                .setCallback(new UMShareListener() {

                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onResult(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onResult: ");
                        releaseShareBitmap(bitmap);
                        shareSuccessReported(Constant.SHARE_TO_QQ);
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                        Log.i(TAG, "onError: ", throwable);
                        releaseShareBitmap(bitmap);
                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onCancel: ");
                        releaseShareBitmap(bitmap);
                    }
                }).share();
    }


    //单纯分享图片
    public void shareOnlyImage(Activity activity, Bitmap bitmap, SHARE_MEDIA share_media) {
        //标记为分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        UMImage image = new UMImage(activity, bitmap);
        new ShareAction(activity)
                .setPlatform(share_media)
                .withMedia(image)
                .setCallback(new UMShareListener() {

                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onResult(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onResult: ");
                        releaseShareBitmap(bitmap);
                        shareSuccessReported(Constant.SHARE_TO_QQ);
                        EventBus.getDefault().post(new ShareWeChatEvent()); //回调成功的通知
                        EventBus.getDefault().post(new VoiceEvent.VoiceShareActivityFinishEvent());
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                        Log.i(TAG, "onError: ", throwable);
                        releaseShareBitmap(bitmap);
                        EventBus.getDefault().post(new VoiceEvent.VoiceShareActivityFinishEvent());
                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onCancel: ");
                        releaseShareBitmap(bitmap);
                        EventBus.getDefault().post(new VoiceEvent.VoiceShareActivityFinishEvent());
                    }
                }).share();
    }


    private static void releaseShareBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    //确认不需要再调用,确保释放内存
    public static void destroy() {
        shareTool = null;
    }

    //分享完毕直接调用,防止泄漏
    private void clearData() {
        mBuilder = null;
    }
}
