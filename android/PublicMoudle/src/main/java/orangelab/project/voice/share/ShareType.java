package orangelab.project.voice.share;

/**
 * Created by lloydfinch on 29/11/2017.
 * <p>
 * 分享的类型
 */

public enum ShareType {

    FROM_TYPE_GAME("FROM_GAME"),
    FROM_TYPE_SETTING("FROM_SETTING"),
    FROM_TYPE_VOICE("FROM_VOICE"),
    FROM_TYPE_SPY_ROOM("FROM_TYPE_SPY_ROOM");


    private String value;

    ShareType(String value) {
        this.value = value;
    }
}
