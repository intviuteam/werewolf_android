package orangelab.project.voice.handler;

import com.androidtoolkit.transport.Transportable;
import com.toolkit.action.Keepable;

import org.json.JSONObject;

import javax.annotation.Nonnull;

/**
 * Created by lloydfinch on 08/03/2018.
 * //语音房消息机制统一实体类
 */

public class VoiceMessageBean implements Keepable, Transportable {

    public int code;//状态码
    public String message;//提示信息
    public String type;//消息类型
    public long msg_id;//消息编号
    public JSONObject payload;//消息json数据实体

    public VoiceMessageBean() {
    }

    public VoiceMessageBean(int code, long msg_id, String type, @Nonnull JSONObject payload, String message) {
        this.code = code;
        this.msg_id = msg_id;
        this.type = type;
        this.payload = payload;
        this.message = message;
    }

    //......添加其他类型来处理
}
