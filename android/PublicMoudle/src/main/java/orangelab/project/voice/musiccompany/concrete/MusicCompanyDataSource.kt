package orangelab.project.voice.musiccompany.concrete

import android.text.TextUtils
import cn.intviu.support.GsonHelper
import com.androidtoolkit.PLog
import com.toolkit.action.Destroyable
import orangelab.project.MainApplication
import orangelab.project.common.db.MusicDataEntityDaoHelper
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.handler.AbstractSocketMessageHandler
import orangelab.project.voice.handler.VoiceMessageBean
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager
import orangelab.project.voice.musiccompany.api.MusicApiConstants
import orangelab.project.voice.musiccompany.component.MusicStateSwitcher
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import orangelab.project.voice.musiccompany.model.MusicOrderSong
import orangelab.project.voice.musiccompany.model.PrepareSingBean
import orangelab.project.voice.utils.TransferClassHelper
import java.util.*
import kotlin.collections.ArrayList

/**
 * 伴奏音乐数据
 */
object MusicCompanyDataSource : MusicStateSwitcher(), Destroyable {

    private val TAG = "MusicCompanyDataSource"

    private val musicDataEntityDaoHelper: MusicDataEntityDaoHelper = MusicDataEntityDaoHelper(MainApplication.getInstance())

    /**
     * 曲库音乐列表，所有的数据获取都从这个列表中获取//只存放最后拉取的
     */
    var musicList: ArrayList<MusicDataEntity> = ArrayList()

    /**
     * 已下载音乐列表
     */
    var downLoadedList: ArrayList<MusicDataEntity> = ArrayList()

    /**
     * 当前播放的音乐
     */
    private var currentMusic: MusicDataEntity = MusicDataEntity()

    /**
     * 刷新曲库//只存放最后拉到的
     */
    fun refreshStock(musicList: ArrayList<MusicDataEntity>) {
        //处理本地已经存在的
        val orderedList = musicDataEntityDaoHelper.queryForAll()
        musicList.forEach { music ->
            orderedList.forEach { ordered ->
                run {
                    if (TextUtils.equals(music.id, ordered.id)) {
                        music.refresh(ordered)
                    }
                }
            }
        }

        PLog.e(TAG, "size = ${musicList.size}")

        this.musicList.clear()
        this.musicList.addAll(musicList)

        this.musicList.forEach {
            PLog.e(TAG, "it = $it")
        }
    }

    /**
     * 刷新已点
     */
    fun refreshOrdered() {
        try {
            this.downLoadedList.clear()
            this.downLoadedList.addAll(musicDataEntityDaoHelper.queryForAll())
        } catch (e: Exception) {
            e.printStackTrace()
            PLog.e(TAG, "error occur when refreshOrdered, error is ${e.message}")
        }
    }

    override fun switchMusic(MusicDataEntity: MusicDataEntity): MusicCompanyDataSource {
        this.currentMusic = MusicDataEntity

        return this
    }

    override fun switchToOrder() {
        currentMusic.state = MusicState.STATE_DOWN_NON
    }

    override fun switchToDownLoading() {
        currentMusic.state = MusicState.STATE_DOWN_ING
    }

    override fun switchToDownPause() {
        currentMusic.state = MusicState.STATE_DOWN_PAUSE
    }

    override fun switchToDownLoaded() {
        currentMusic.state = MusicState.STATE_DOWN_FINISH
    }

    override fun switchToDownFailed() {
        currentMusic.state = MusicState.STATE_DOWN_FAILED
    }

    override fun switchProgress(progress: Int) {
        currentMusic.progress = progress
    }

    override fun switchToNon() {
        currentMusic.state = MusicState.STATE_PLAY_NON
    }

    override fun switchToPlaying() {
        currentMusic.state = MusicState.STATE_PLAY_ING
    }

    override fun switchToPlayPause() {
        currentMusic.state = MusicState.STATE_PLAY_PAUSE
    }

    override fun switchToPlayStop() {
        currentMusic.state = MusicState.STATE_PLAY_STOP
    }

    override fun switchToPlayLoop() {
        currentMusic.state = MusicState.STATE_PLAY_LOOP
    }

    override fun refresh(MusicDataEntity: MusicDataEntity, position: Int) {
        musicList.forEach {
            if (TextUtils.equals(it.id, MusicDataEntity.id)) {
                it.refresh(MusicDataEntity)
            }
        }
    }

    //ktv二期 start=====================================
    /**
     * 排麦歌曲列表
     */
    private var orderedSongMenu: LinkedList<MusicOrderSong> = LinkedList()

    /**
     * 清空已点的歌
     */
    fun clear() {
        orderedSongMenu.clear()
    }

    /**
     * 添加一首排麦歌曲
     */
    fun addToSongMenu(song: MusicOrderSong) {
        if (!TextUtils.isEmpty(song.id)) {
            if (!hasAddToMenu(song)) {
                song.position = orderedSongMenu.size + 1
                orderedSongMenu.add(song)
            }
        }
    }

    /**
     * 返回已排麦的歌曲数目
     */
    fun getSongMenuNumber(): Int {
        return orderedSongMenu.size
    }

    /**
     * 判断是否已经排麦
     */
    fun hasAddToMenu(song: MusicOrderSong): Boolean {
        orderedSongMenu.forEach {
            val added: Boolean = TextUtils.equals(it.id, song.id) &&
                    TextUtils.equals(it.orderId, song.orderId)

            if (added) {
                return true
            }
        }

        return false
    }

    /**
     * 获取所有排麦的歌曲,按照有序列表返回
     */
    fun getOrderedSongs(): ArrayList<MusicOrderSong> {

        //刷新已点歌曲
        refreshOrdered()

        //构造已点列表
        var datas = ArrayList<MusicOrderSong>()
        datas.addAll(orderedSongMenu)

        PLog.e(TAG, "datasize = ${datas.size}")
        datas.forEach {
            PLog.e(TAG, "song = $it")
        }

        return datas
    }


    /**
     * 返回下载的歌曲
     */
    fun getLocalMusicById(id: String): MusicDataEntity? {
        downLoadedList.forEach {
            if (TextUtils.equals(it.id, id)) {
                return it
            }
        }

        return null
    }

    /**
     * 返回已点歌曲
     */
    fun getOrderedSongById(musicId: String, userId: String): MusicOrderSong? {
        orderedSongMenu.forEach {
            val isOrdered = TextUtils.equals(musicId, it.id) &&
                    TextUtils.equals(userId, it.orderId)
            if (isOrdered) {
                return it
            }
        }
        return null
    }


    /**
     * 删除某人点的某首歌
     */
    fun removeMusicByUser(musicId: String, userId: String) {

        val removeSongs: ArrayList<MusicOrderSong> = ArrayList()

        orderedSongMenu.forEach {
            val canRemove = TextUtils.equals(musicId, it.id) &&
                    TextUtils.equals(userId, it.orderId)
            if (canRemove) {
                removeSongs.add(it)
            }
        }

        orderedSongMenu.removeAll(removeSongs)

        refreshPosition()
    }

    /**
     * 歌曲置顶
     */
    fun insertToPop(musicId: String, userId: String) {
        val song = getOrderedSongById(musicId, userId)
        if (song != null) {
            //删除旧的
            orderedSongMenu.remove(song)
            //添加到0号位置
            orderedSongMenu.add(0, song)

            refreshPosition()
        }
    }

    /**
     * 更新歌曲序号
     */
    fun refreshPosition() {
        orderedSongMenu.forEach {
            it.position = orderedSongMenu.indexOf(it) + 1
        }
    }

    /**
     * 更新下一首
     */
    fun updateNextSong() {
        if (orderedSongMenu.size > 0) {
            MusicCompanyConfig.nextSong = orderedSongMenu[0]
        } else {
            MusicCompanyConfig.nextSong = null
        }
    }
    //ktv二期 end=====================================

    //消息观察处理器
    var observer: AbstractSocketMessageHandler = object : AbstractSocketMessageHandler() {

        //点歌
        override fun handleBookSong(messageBean: VoiceMessageBean) {
            val bean: PrepareSingBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), PrepareSingBean::class.java)
            val song: MusicOrderSong = TransferClassHelper.MusicPrepareSong2MusicOrderSong(bean)

            //说明只是个提示消息
            if (TextUtils.isEmpty(song.id)) {
                return
            }

            //添加到歌单
            addToSongMenu(song)

            //更新下一首
            updateNextSong()
        }

        //置顶
        override fun handlePeakSong(messageBean: VoiceMessageBean) {
            val musicID = messageBean.payload.optString(MusicApiConstants.MUSIC_ID)
            val userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER)

            //说明是个无效的提示消息
            if (TextUtils.isEmpty(musicID) || TextUtils.isEmpty(userId)) {
                return
            }

            insertToPop(musicID, userId)

            updateNextSong()
        }

        //删除
        override fun handleRemoveSong(messageBean: VoiceMessageBean) {
            val musicID = messageBean.payload.optString(MusicApiConstants.MUSIC_ID)
            val userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER)
            removeMusicByUser(musicID, userId)

            //删除的是即将播放的歌曲
            if (TextUtils.equals(musicID, MusicCompanyConfig.musicId) &&
                    TextUtils.equals(userId, MusicCompanyConfig.orderId)) {
                MusicCompanyConfig.clearPrepareMusic()

                //删除的是即将播放的歌曲，就停止
                MusicManager.instance.stop()
            }

            updateNextSong()
        }

        //授权
        override fun handleAuthorizeSing(messageBean: VoiceMessageBean) {

        }

        //加载歌曲
        override fun handlePrepareSing(messageBean: VoiceMessageBean) {
            //更新本次的播放信息
            val bean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), PrepareSingBean::class.java)
            if (bean != null) {
                val song = bean.song
                if (song != null) {
                    MusicCompanyConfig.lyricUrl = song.lyric_url
                    MusicCompanyConfig.playUrl = song.play_url
                    MusicCompanyConfig.albumUrl = song.img
                    MusicCompanyConfig.musicId = song.music_id
                    MusicCompanyConfig.orderId = song.booker
                    MusicCompanyConfig.roomID = song.room_id
                    MusicCompanyConfig.songName = song.song_name
                    MusicCompanyConfig.orderName = song.booker_name
                    MusicCompanyConfig.orderImg = song.booker_avatar
                }
            }

            MusicCompanyConfig.isPrepare = true
            MusicCompanyConfig.isSing = false
        }

        //开唱
        override fun handleStartSing(messageBean: VoiceMessageBean) {
            MusicCompanyConfig.isPrepare = false

            //设置房间为唱歌状态
            MusicCompanyConfig.isSing = true

            //开唱就设置为当前歌曲，并从歌单中移除
            val bean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), PrepareSingBean::class.java)
            if (bean != null) {
                val song = bean.song
                if (song != null) {
                    //再次赋值，防止不prepare的情况
                    MusicCompanyConfig.lyricUrl = song.lyric_url
                    MusicCompanyConfig.playUrl = song.play_url
                    MusicCompanyConfig.albumUrl = song.img
                    MusicCompanyConfig.musicId = song.music_id
                    MusicCompanyConfig.orderId = song.booker
                    MusicCompanyConfig.roomID = song.room_id
                    MusicCompanyConfig.songName = song.song_name
                    MusicCompanyConfig.orderName = song.booker_name
                    MusicCompanyConfig.orderImg = song.booker_avatar
                    MusicCompanyConfig.singerId = song.booker
                    MusicCompanyConfig.startTime = bean.song!!.start_time
                    MusicCompanyConfig.timeLength = bean.song!!.timelength.toLong()
                    val currentSong = TransferClassHelper.MusicPrepareSong2MusicOrderSong(song)
                    MusicCompanyConfig.currentSong = currentSong
                    if (currentSong != null) {
                        removeMusicByUser(currentSong.id, currentSong.orderId)
                    }
                }
            }

            updateNextSong()
        }

        //结束
        override fun handleStopSing(messageBean: VoiceMessageBean?) {
            MusicCompanyConfig.isPrepare = false
            MusicCompanyConfig.isSing = false

            //删除上次播放信息
            MusicCompanyConfig.clearMusicLenInfo()
        }

        //处理切换房间
        override fun handleUpdateTitle(messageBean: VoiceMessageBean?) {

        }

        override fun destroy() {

        }
    }


    //根据消息处理数据

    //============================================


    override fun destroy() {

        musicList.clear()
        downLoadedList.clear()
        orderedSongMenu.clear()
    }

}