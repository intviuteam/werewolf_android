package orangelab.project.voice.privateroom.model

import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.model.UserVipInfo

/**
 *game_werewolf
 *2018/3/28 上午11:32
 *Mystery
 */
interface PrivateRoomMemberData {

    fun getMemberPosition(): Int

    fun getMemberId(): String?

    fun getMemberIcon(): String?

    fun getMemberName(): String?

    fun getMemberSex(): Int

    fun getMemberLevel(): Int

    fun getMemberSignature(): String?

    fun memberIsSpeaking(): Boolean

    fun memberIsMaster(): Boolean

    fun memberIsFreeStyle(): Boolean

    fun memberIsSelf(): Boolean

    fun memberIsDeputy(): Boolean

    fun memberIsBanMic(): Boolean

    fun memberIsUpSeat(): Boolean

    fun memberNeedRefresh(): Boolean

    fun memberRefreshDone()

    fun memberMsgDecor(): UserInfoResult.MessageBox?

    fun memberHeadDecor(): UserInfoResult.AvatarBox?

    fun memberVipInfo(): UserVipInfo?
}