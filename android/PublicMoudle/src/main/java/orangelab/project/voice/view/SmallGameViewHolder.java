package orangelab.project.voice.view;

import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.view.viewpagergroup.ViewPagerGroupHolder;

public class SmallGameViewHolder extends ViewPagerGroupHolder {
    public ImageView mImageView;
    public TextView mTextView;
}
