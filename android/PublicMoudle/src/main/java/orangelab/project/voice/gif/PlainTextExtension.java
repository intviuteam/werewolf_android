package orangelab.project.voice.gif;

/**
 * Created by lloydfinch on 31/10/2017.
 */

public class PlainTextExtension {
    public byte[] bytes;
    public int size;

    public PlainTextExtension(byte[] bytes, int offset) {
        int blockSize;
        // get size
        size = 0x0F;

        blockSize = bytes[offset + size] & 0xFF;
        size += 1;
        while (blockSize != 0x00) {
            size += blockSize;
            blockSize = bytes[offset + size] & 0xFF;
            size += 1;
        }

        this.bytes = new byte[size];
        System.arraycopy(bytes, offset, this.bytes, 0, size);
    }
}
