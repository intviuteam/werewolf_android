package orangelab.project.voice.privateroom.view

import orangelab.project.voice.privateroom.model.PrivateRoomPaperPlane
import com.toolkit.action.Destroyable

/**
 *game_werewolf
 *2018/3/28 下午6:50
 *Mystery
 */
interface PrivateRoomPaperPlaneView : Destroyable {

    fun addPaperPlane(paperPlaneList: List<PrivateRoomPaperPlane>?)

    fun addPaperPlane(paperPlaneView: PrivateRoomPaperPlane)

    fun clearPaperPlane()

}