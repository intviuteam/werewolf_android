package orangelab.project.voice.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.R;
import orangelab.project.voice.manager.VoiceTopicManager;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lloydfinch on 18/10/2017.
 */

public class VoiceTopicTypeAdapter extends RecyclerView.Adapter<VoiceTopicTypeAdapter.MyViewHolder> implements Destroyable {

    private List<VoiceTopicManager.Params.VoiceTopic> topicList = new ArrayList<>();

    private HashMap<Integer, RadioButton> radios = new HashMap<>();

    private int selectedPosition;

    public VoiceTopicTypeAdapter(int position) {
        this.selectedPosition = position;
    }

    public VoiceTopicTypeAdapter(List<VoiceTopicManager.Params.VoiceTopic> topicList) {
        this.topicList.addAll(topicList);
    }

    public void updateTopicList(List<VoiceTopicManager.Params.VoiceTopic> topicList) {
        if (topicList == null) {
            return;
        }
        this.topicList.clear();
        this.topicList.addAll(topicList);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_voice_topic_type, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        VoiceTopicManager.Params.VoiceTopic topic = topicList.get(position);
        if (topic != null) {
            holder.radioButtonType.setText(topic.getDescription());
        }
        if (!radios.containsKey(position)) {
            radios.put(position, holder.radioButtonType);
        }
        if (position == selectedPosition) {
            holder.radioButtonType.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return topicList.size();
    }

    @Override
    public void destroy() {
        topicList.clear();
        radios.clear();
    }

    public VoiceTopicManager.Params.VoiceTopic getSelectedTopic() {
        return topicList.size() > 0 ? topicList.get(selectedPosition) : null;
    }

    private void updateSelectedRadio() {
        for (int index = 0; index < radios.size(); index++) {
            RadioButton radioButton = radios.get(index);
            if (radioButton != null) {
                if (index == selectedPosition) {
                    radioButton.setChecked(true);
                } else {
                    radioButton.setChecked(false);
                }
            }
        }
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButtonType;

        public MyViewHolder(View itemView) {
            super(itemView);
            radioButtonType = (RadioButton) itemView.findViewById(R.id.rb_voice_type);
            radioButtonType.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    selectedPosition = getPosition();
                }
                updateSelectedRadio();
            });
        }
    }
}
