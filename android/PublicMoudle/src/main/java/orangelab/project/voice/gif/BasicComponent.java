package orangelab.project.voice.gif;

import android.os.Handler;

import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

/**
 * Created by lloydfinch on 24/10/2017.
 */

public class BasicComponent {

    private Handler mHandler = null;

    public BasicComponent() {
        mHandler = new SafeHandler();
    }

    protected void postDelay(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(runnable, delay);
        }
    }

    protected void post(Runnable runnable) {
        postDelay(runnable, 0);
    }

    protected void postDelaySafely(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(() -> {
                Utils.runSafely(runnable);
            }, delay);
        }
    }

    protected void postSafely(Runnable runnable) {
        postDelaySafely(runnable, 0);
    }

    public void destroy() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }
}
