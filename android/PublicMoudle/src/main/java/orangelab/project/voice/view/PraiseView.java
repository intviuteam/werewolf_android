package orangelab.project.voice.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.SafeHandler;

/**
 * Created by lloydfinch on 28/10/2017.
 * 点赞动画的父容器,扩展ing
 */

public class PraiseView extends RelativeLayout implements Destroyable {

    private static final int SEND_PRAISE_INTERVAL = 2 * 60 * 1000;

    private ImageView fabLikeRoom;
    private View fabLikeRoomAnim;
    private View likeRoomContainer;
    private TextView tvLikeRoomNumber;

    private Context mContext;
    private int likePraiseCount = 0;
    private boolean canSend = true;
    private SafeHandler safeHandler;

    public PraiseView(Context context) {
        super(context);
        init(context);
    }

    public PraiseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PraiseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        initBase(context);
        initView();
        initListener();
        //initEvent();
    }

    private void initBase(Context context) {
        mContext = context;
        safeHandler = new SafeHandler();
    }

    private void initView() {
        addView(inflate(mContext, R.layout.layout_praise_room, null));
        fabLikeRoom = (ImageView) findViewById(R.id.fab_voice_like_room);
        fabLikeRoomAnim = findViewById(R.id.fab_voice_like_room1);
        tvLikeRoomNumber = (TextView) findViewById(R.id.tv_voice_like_room_number);
        likeRoomContainer = findViewById(R.id.fab_voice_like_room1_container);
    }

    private void initListener() {
        fabLikeRoom.setOnClickListener(v -> {
            if (canSend) {
                sendPraise();
                fabLikeRoom.setImageResource(R.mipmap.ico_like_room_gray);
                canSend = false;
                safeHandler.postDelaySafely(() -> {
                    fabLikeRoom.setImageResource(R.mipmap.ico_voice_like_room);
                    canSend = true;
                }, SEND_PRAISE_INTERVAL);
            }
        });
    }

    //待扩展
    private void initEvent() {
    }

    public void initPraiseCount(int likeCount) {
        if (likeCount <= 0) {
            tvLikeRoomNumber.setVisibility(View.INVISIBLE);
            this.likePraiseCount = 0;
        } else {
            tvLikeRoomNumber.setVisibility(View.VISIBLE);
            tvLikeRoomNumber.setText(String.valueOf(likeCount));
            this.likePraiseCount = likeCount;
        }
    }

    public int getLikeCount() {
        return likePraiseCount;
    }

    //发送点赞
    private void sendPraise() {
        RoomSocketEngineHelper.sendLikeRoom();
    }

    //收到点赞
    public void receivePraise(int count) {
        if (count <= 0) {
            tvLikeRoomNumber.setVisibility(View.INVISIBLE);
        } else {
            tvLikeRoomNumber.setVisibility(View.VISIBLE);
            tvLikeRoomNumber.setText(String.valueOf(count));
        }
        fabLikeRoom.startAnimation(AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.anim_voice_like_scale));
        likeRoomContainer.setVisibility(View.VISIBLE);
        fabLikeRoomAnim.startAnimation(generateAnimation());
    }

    private Animation generateAnimation() {
        Animation animation = AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.anim_voice_like_translate);
        animation.setRepeatCount(5);
        animation.setRepeatMode(Animation.RESTART);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (likeRoomContainer != null) {
                    likeRoomContainer.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return animation;
    }

    @Override
    public void destroy() {
        safeHandler.release();
        canSend = true;
        fabLikeRoom.clearAnimation();
        fabLikeRoomAnim.clearAnimation();
    }
}
