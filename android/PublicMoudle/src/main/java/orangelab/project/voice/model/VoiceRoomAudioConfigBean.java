package orangelab.project.voice.model;

import orangelab.project.common.model.EnterRoomResult;
import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 04/01/2018.
 * //语音房音频配置信息
 */

public class VoiceRoomAudioConfigBean implements Keepable {
    public String roomId;
    public EnterRoomResult.RoomServerMessage mediaServer;
}
