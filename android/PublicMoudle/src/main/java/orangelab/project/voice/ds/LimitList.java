package orangelab.project.voice.ds;

import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 * Created by lloydfinch on 27/09/2017.
 * 线程安全容量受限的消息队列
 */

public final class LimitList<T> {
    public static final String TAG = "LimitList";

    private static final int DEFAULT_SIZE = 300;
    private int size = DEFAULT_SIZE;

    private Vector<T> messages = new Vector<>();

    public LimitList() {
    }

    public LimitList(int size) {
        this.size = size;
    }

    public void changeSize(int size) {
        this.size = size;
    }

    public int size() {
        return messages.size();
    }

    public boolean add(T t) {
        if (messages.size() >= size) {
            messages.remove(0);
        }
        return messages.add(t);
    }

    public void addAll(Collection<? extends T> collection) {
        messages.addAll(collection);
    }

    public boolean remove(T t) {
        return messages.remove(t);
    }

    public T get(int position) {
        return messages.get(position);
    }

    public void clear() {
        messages.clear();
    }

    public List<T> getRealMessages() {
        return messages;
    }
}
