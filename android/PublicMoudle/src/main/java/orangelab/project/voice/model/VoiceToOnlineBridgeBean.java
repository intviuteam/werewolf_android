package orangelab.project.voice.model;

import com.androidtoolkit.transport.Transportable;
import orangelab.project.common.model.EnterRoomResult;
import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 05/03/2018.
 */

public class VoiceToOnlineBridgeBean implements Keepable, Transportable {

    public boolean isForceUpseat = false;
    public int newPosition = 0;
    public String roomTitle = "";
    public List<EnterRoomResult.EnterRoomUserItem> onLineUsers = new ArrayList<>();

    @Override
    public String toString() {
        return "VoiceToOnlineBridgeBean{" +
                "isFroceUpseat=" + isForceUpseat +
                ", newPosition=" + newPosition +
                ", roomTitle='" + roomTitle + '\'' +
                ", onLineUsers=" + onLineUsers +
                '}';
    }
}
