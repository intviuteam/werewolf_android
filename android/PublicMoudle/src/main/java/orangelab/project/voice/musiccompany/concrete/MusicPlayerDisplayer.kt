package orangelab.project.voice.musiccompany.concrete

import android.graphics.PointF
import android.graphics.drawable.AnimationDrawable
import android.os.Build
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import com.datasource.GlobalUserState
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable
import de.hdodenhof.circleimageview.CircleImageView
import orangelab.project.MainApplication
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.common.exhibition.ExhibitionsActivity
import orangelab.project.common.exhibition.ExhibitionsConstant
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.SafeHandler
import orangelab.project.common.utils.Utils
import orangelab.project.voice.dialog.VoiceLeaveDialog
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager
import orangelab.project.voice.musiccompany.MusicStockActivity
import orangelab.project.voice.musiccompany.api.MusicSocketHelper
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import orangelab.project.voice.musiccompany.dialog.MusicRoomControlDialog
import orangelab.project.voice.musiccompany.event.MusicEvent
import orangelab.project.voice.musiccompany.lyric.impl.DefaultLrcBuilder
import orangelab.project.voice.musiccompany.lyric.impl.LrcView
import orangelab.project.voice.musiccompany.tecent.music.MusicCompanyManager
import orangelab.project.voice.utils.PositionHelper
import orangelab.project.voice.utils.SystemScreenUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*

/**
 * 播放器 TODO 逻辑优化,内存优化
 */

class MusicPlayerDisplayer(contentView: View) : Destroyable, Keepable {

    private val TAG = "MusicPlayerDisplayer"

    //标题
    private val tvTitle: TextView = contentView.findViewById(R.id.tv_music_player_title) as TextView

    //专辑底层背景
    private val albumBg: View = contentView.findViewById(R.id.rl_music_player_album_bg)

    //专辑封面
    private val ivAlbum: CircleImageView = contentView.findViewById(R.id.iv_music_player_album) as CircleImageView

    //音符跳动动画
    private val ivNoteAnim: ImageView = contentView.findViewById(R.id.iv_music_note_anim) as ImageView

    //进度条
    private val seekProgress: SeekBar = contentView.findViewById(R.id.seek_music_player_progress) as SeekBar

    //点歌按钮
    private val btnOrder: Button = contentView.findViewById(R.id.btn_music_player_order) as Button

    //送礼/均衡器按钮
    private val btnSendGift: Button = contentView.findViewById(R.id.btn_music_player_send_gift) as Button

    //开始按钮(播放下一曲)
    private val btnStart: Button = contentView.findViewById(R.id.btn_music_player_start) as Button

    //播放按钮(开始播放音乐)
    private val btnPlay: ImageView = contentView.findViewById(R.id.btn_music_player_play) as ImageView

    //提示语
    private val tvTips: TextView = contentView.findViewById(R.id.tv_music_player_tips) as TextView

    //歌词展示器
    private val lyricView: LrcView = contentView.findViewById(R.id.lv_music_player_song_lyric) as LrcView

    //关闭唱歌
    private val ivBoot: View = contentView.findViewById(R.id.iv_music_player_boot)

    //即将播放前的展示信息
    private val tvPrepareInfo: TextView = contentView.findViewById(R.id.tv_music_player_prepare_info) as TextView

    //当前进度
    private val tvCurrentProgress: TextView = contentView.findViewById(R.id.tv_current_progress) as TextView

    //最大进度
    private val tvMaxProgress: TextView = contentView.findViewById(R.id.tv_max_progress) as TextView

    //进度条外层
    private val llProgressContainer: View = contentView.findViewById(R.id.ll_progress_container)

    private var timerTask: Timer? = null

    private var safeHandler = SafeHandler()

    private val timeInterval: Long = 300L

    init {

        EventBus.getDefault().register(this)

        //进度条不能拖拽
        seekProgress.setOnTouchListener { _, _ -> true }

        //跳转到曲库
        btnOrder.setOnClickListener {
            //直接可以点歌
            if (PositionHelper.isUpSeat()) {
                MusicStockActivity.Launch(contentView.context)
            } else {
                ToastToolKit.showShort(R.string.str_music_order_song_tips)
            }
        }

        //点击停止播放
        ivBoot.setOnClickListener {
            val dialog = VoiceLeaveDialog(contentView.context,
                    MessageUtils.getString(R.string.str_music_tip),
                    MessageUtils.getString(R.string.str_music_if_over))
            dialog.setConfirmClickListener {
                dialog.dismiss()
                try {

                    //先停止伴奏
                    stop()

                    //唱歌阶段发送停止
                    if (MusicCompanyConfig.isSing) {
                        if (PositionHelper.isSinger() || PositionHelper.isMaster()) {
                            MusicSocketHelper.INSTANCE.sendStopSing()
                        }
                    }

                    //准备阶段发送删除
                    if (MusicCompanyConfig.isPrepare) {
                        if (PositionHelper.canSing() || PositionHelper.isMaster()) {
                            MusicSocketHelper.INSTANCE.removeSongFromMenu(MusicCompanyConfig.musicId, MusicCompanyConfig.orderId)
                        }
                    }

                } catch (e: Exception) {
                    PLog.e(TAG, "error when sendStop ; ${e.message}")
                }
            }
            dialog.negative(true)
            dialog.show()
        }

        //点击开始播放
        btnPlay.setOnClickListener {
            btnPlay.visibility = View.GONE
            if (PositionHelper.canSing()) {
                MusicSocketHelper.INSTANCE.sendStartSing()
            }
        }

        showTips()
        hideNextSong()
        hidePlayButton()
        hideBoot()

        //默认是不能送礼的按钮
        toSendGiftNo()
    }

    /**
     *  获取唱歌人的头像在屏幕上的位置
     */
    fun getPosition(): PointF {
        val location = IntArray(2)
        ivAlbum.getLocationOnScreen(location)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            location[1] -= SystemScreenUtils.getStatusBarHeight()
        }
        return PointF(location[0].toFloat(), location[1].toFloat())
    }

    /**
     * 获取宽度
     */
    fun getAlbumWidth(): Int {
        return ivAlbum.width
    }

    /**
     * 转换为可以点击的送礼按钮
     */
    private fun toSendGiftCan() {
        btnSendGift.setText(R.string.str_send_gift)
        btnSendGift.setBackgroundResource(R.drawable.bg_btn_send_gift)
        btnSendGift.setOnClickListener {
            when {
                GlobalUserState.getGlobalState().isTourist -> ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift))
                Utils.checkUserIdIsTourist(MusicCompanyConfig.singerId) -> ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift_2))
                else -> {
                    var singer = VoiceRoomDataSourceManager.getInstance().findUserById(MusicCompanyConfig.singerId)
                    if (singer == null) {
                        //唱歌人为空，就找准备的人
                        singer = VoiceRoomDataSourceManager.getInstance().findUserById(MusicCompanyConfig.orderId)
                    }
                    if (singer != null) {
                        ExhibitionsActivity.startActivityFromGame(
                                btnSendGift.context, singer.id, singer.name, singer.sex, singer.avatar, singer.position, ExhibitionsConstant.DISPLAY_TYPE_SING)
                    } else {
                        ToastToolKit.showShort(R.string.str_voice_already_leave)
                    }
                }
            }
        }
    }

    /**
     * 转化为不能点击的送礼按钮
     */
    private fun toSendGiftNo() {
        btnSendGift.setText(R.string.str_send_gift)
        btnSendGift.setBackgroundResource(R.mipmap.ic_music_player_btn_gray)
        btnSendGift.isClickable = false
    }

    /**
     * 转换为可以点击的均衡器按钮
     */
    private fun toControlCan() {
        btnSendGift.setText(R.string.str_music_tip_control)
        btnSendGift.setBackgroundResource(R.drawable.bg_btn_adjust)
        btnSendGift.setOnClickListener {
            MusicRoomControlDialog(btnSendGift.context).show()
        }
    }

    /**
     * 转换为不能点击的均衡器按钮
     */
    private fun toConrtrolNo() {
        btnSendGift.setText(R.string.str_music_tip_control)
        btnSendGift.setBackgroundResource(R.mipmap.ic_music_player_btn_gray)
        btnSendGift.isClickable = false
    }

    /**
     * 显示提示消息
     */
    private fun showTips() {
        tvTips.visibility = View.VISIBLE
    }

    /**
     * 隐藏提示信息
     */
    private fun hideTips() {
        tvTips.visibility = View.GONE
    }

    /**
     * 展示结束按钮
     */
    fun showBoot() {
        ivBoot.visibility = View.VISIBLE
    }

    /**
     * 隐藏结束按钮
     */
    private fun hideBoot() {
        ivBoot.visibility = View.INVISIBLE
    }

    /**
     * 显示"开始"按钮
     */
    fun showNextSong() {
        //有下一首
        if (MusicCompanyConfig.nextSong != null) {
            btnStart.visibility = View.VISIBLE
            btnStart.setOnClickListener {
                if (PositionHelper.inRoom(MusicCompanyConfig.orderId)) {
                    if (PositionHelper.isUpSeat(MusicCompanyConfig.orderId)) {
                        MusicSocketHelper.INSTANCE.sendAuthorizeSing(MusicCompanyConfig.orderId)
                    } else {
                        //不在麦上了
                        ToastToolKit.showShort(R.string.str_music_not_in_seat)
                    }
                } else {
                    //已经离开房间
                    ToastToolKit.showShort(R.string.str_voice_already_leave)
                }
            }
        }
    }

    /**
     * 隐藏"开始"按钮
     */
    private fun hideNextSong() {
        btnStart.visibility = View.GONE
    }

    /**
     * 展示播放按钮
     */
    private fun showPlayButton() {
        btnPlay.visibility = View.VISIBLE
    }

    /**
     * 隐藏播放按钮
     */
    private fun hidePlayButton() {
        btnPlay.visibility = View.GONE
    }

    /**
     * 展示进度条
     */
    private fun showProgress() {
        llProgressContainer.visibility = View.VISIBLE
    }

    /**
     * 隐藏进度条
     */
    private fun hideProgress() {
        llProgressContainer.visibility = View.INVISIBLE
    }

    /**
     * 展示准备信息
     */
    private fun showPrepareInfo() {

        var name = if (MusicCompanyConfig.nextSong != null) MusicCompanyConfig.nextSong.order else MusicCompanyConfig.orderName
        var songName = if (MusicCompanyConfig.nextSong != null) MusicCompanyConfig.nextSong.songName else MusicCompanyConfig.songName
        tvPrepareInfo.text = MessageUtils.getString(R.string.str_music_player_prepare_info, songName, name)
        tvPrepareInfo.visibility = View.VISIBLE
    }

    /**
     * 隐藏准备信息
     */
    private fun hidePrepareInfo() {
        tvPrepareInfo.visibility = View.GONE
    }

    /**
     * 刷新标题
     */
    fun refreshTitle(title: String) {
        tvTitle.text = title
    }

    /**
     * 准备播放，隐藏提示信息，展示歌词信息
     */
    fun prepare(musicData: MusicDataEntity) {

        //先重置状态
        stop()
        hideTips()
        hideNextSong()

        //即将唱歌的人可以唱歌
        if (MusicCompanyConfig.hasAuthorized()) {
            hidePrepareInfo()
            try {
                val lrcRows = DefaultLrcBuilder().getLrcRows(musicData.lyrics)
                lyricView.setLrc(lrcRows)
                lyricView.visibility = View.VISIBLE
            } catch (e: Exception) {
                PLog.e(TAG, "error occur when prepare = ${e.message}")
            }

            //唱歌人视角
            if (PositionHelper.canSing() && PositionHelper.isSelf(MusicCompanyConfig.orderId)) {
                showBoot()
                showPlayButton()
                toControlCan()
            } else {
                hideBoot()
                hidePlayButton()
                toSendGiftCan()
            }
        } else {
            //展示准备信息
            showPrepareInfo()
            hideBoot()
            hidePlayButton()
            toSendGiftNo()
            if (PositionHelper.isMaster()) {
                showNextSong()
            }
        }

        //只要是房主，就可以关闭或删除当前歌曲
        if (PositionHelper.isMaster()) {
            showBoot()
        }

        //加载专辑图片
        ivAlbum.visibility = View.VISIBLE
        PicassoUtils.loadImageByDefault(MainApplication.getInstance(), MusicCompanyConfig.orderImg, ivAlbum, R.mipmap.default_head)
    }

    /**
     * 开始播放
     */
    fun start() {
        MusicCompanyConfig.isPause = false
        hideNextSong()
        hidePlayButton()
        hideTips()
        hidePrepareInfo()

        //开唱
        if (PositionHelper.isSinger()) {
            toControlCan()
            showBoot()
        } else {
            toSendGiftCan()
            hideBoot()
        }

        if (PositionHelper.isMaster()) {
            showBoot()
        }

        try {
            MusicCompanyManager.getInstance(ivBoot.context).startFuckMusic()
            //设置进度条最大值
            seekProgress.max = MusicCompanyConfig.timeLength.toInt()
            //歌曲时间长度展示
            tvMaxProgress.text = getFormatTime(MusicCompanyConfig.timeLength)
            //展示进度条
            showProgress()

            //开始检测歌词
            loopLyric()
        } catch (e: Exception) {
            PLog.e(TAG, "error occur when start: ${e.message}")
        }

        startMusicAnim()
    }

    /**
     * 暂停
     */
    fun pause() {
        if (PositionHelper.isSinger()) {
            showPlayButton()
        }
        stopMusicAnim()
        MusicCompanyManager.getInstance(tvTitle.context).pause()
        MusicCompanyConfig.isPause = true
    }

    /**
     * 复原
     */
    fun resume() {
        hidePlayButton()
        startMusicAnim()
        MusicCompanyManager.getInstance(tvTitle.context).resume()
        MusicCompanyConfig.isPause = false
    }

    /**
     * 停止
     */
    fun stop() {
        PLog.e(TAG, "stop start...")

        MusicCompanyConfig.isPause = false
        hideNextSong()
        hidePlayButton()
        quitLyric()
        stopMusicAnim()
        seekProgress.progress = 0
        hideProgress()
        showTips()
        hideBoot()
        toSendGiftNo()
        hidePrepareInfo()

        //隐藏专题图片
        ivAlbum.setImageResource(R.mipmap.default_head)
        ivAlbum.visibility = View.INVISIBLE

        //stop就先暂停
        MusicCompanyManager.getInstance(tvTitle.context).pause()
        //暂停后立即请求语音权限
        EventBus.getDefault().post(MusicEvent.ExitKTVRoomEvent())

        //再退出房间
        MusicCompanyManager.getInstance(ivBoot.context).exitMusicCompany()

        PLog.e(TAG, "stop over!!")
    }

    /**
     * 展示动画
     */
    private fun startMusicAnim() {

        //专辑旋转
        val animation = AnimationUtils.loadAnimation(albumBg.context, R.anim.rotate_ktv_sing)
        animation.interpolator = LinearInterpolator()
        albumBg.startAnimation(animation)

        //音符跳动
        ivNoteAnim.setImageResource(R.drawable.anim_note_jump)
        ivNoteAnim.visibility = View.VISIBLE
        try {
            (ivNoteAnim.drawable as AnimationDrawable).start()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * 停止动画
     */
    private fun stopMusicAnim() {
        albumBg.clearAnimation()

        try {
            (ivNoteAnim.drawable as AnimationDrawable).stop()
            ivNoteAnim.setImageResource(R.mipmap.ic_music_player_anim1)
            ivNoteAnim.visibility = View.INVISIBLE
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 滚动歌词
     */
    private fun loopLyric() {
        try {
            var currentPosition: Long = MusicCompanyConfig.passTime()
            lyricView.reset()
            val task: TimerTask = object : TimerTask() {
                override fun run() {
                    safeHandler.postSafely {
                        //获取伴奏时间
                        var progress: Long = MusicCompanyManager.getInstance(ivBoot.context).currentPosition
                        if (progress <= 0) {
                            //获取不到伴奏时间，就自己计算
                            progress = currentPosition
                        }
                        PLog.e(TAG, "current progress = $progress")
                        lyricView.seekLrcToTime(progress)
                        seekProgress.progress = progress.toInt()
                        if (progress <= MusicCompanyConfig.timeLength) {
                            tvCurrentProgress.text = getFormatTime(progress)
                        }

                        currentPosition += timeInterval
                    }
                }
            }

            timerTask = Timer(true)
            //每隔300ms检测一下歌词
            timerTask?.schedule(task, 0, timeInterval)
        } catch (e: Exception) {
            PLog.e(TAG, "error loopLyric ${e.message}")
        }
    }

    /**
     * 停止歌词
     */
    private fun quitLyric() {
        try {
            timerTask?.cancel()
            timerTask = null
            lyricView.release()
            PLog.e(TAG, "quitLyric")
        } catch (e: Exception) {
            PLog.e(TAG, "error quitLyric ${e.message}")
        }
    }

    override fun destroy() {
        safeHandler.release()
        EventBus.getDefault().unregister(this)
    }

    /**
     * 获取展示的时间
     */
    private fun getFormatTime(duration: Long): String {
        val dataFormat = SimpleDateFormat("mm:ss")
        return dataFormat.format(duration)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MusicEvent.MusicFileSetEvent) {

//        //设置进度条最大值
//        seekProgress.max = event.length.toInt()
//        //歌曲时间长度展示
//        tvMaxProgress.text = getFormatTime(event.length)
//
//        //展示进度条
//        showProgress()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MusicEvent.MusicCompanyStart) {
        if (PositionHelper.canSing()) {
            //loopLyric()
        }
    }
}