package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.androidtoolkit.ToastToolKit;
import com.R;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.model.VoiceWords;
import orangelab.project.voice.view.LimitTextWatcher;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 24/11/2017.
 * <p>
 * 谁是卧底房主设置对话框
 */

public class VoiceSpySettingDialog extends BasicFormatDialog implements Destroyable {

    private EditText txtSpyWord;
    private EditText txtGoodWord;

    private Button btnSystemWords;
    private Button btnUserWords;

    private CheckBox switch1;
    private CheckBox switch2;
    private CheckBox switch3;
    private CheckBox switch4;

    private Button btnConfirm;
    private Button btnCancel;

    private Context mContext;

    public VoiceSpySettingDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceSpySettingDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;

        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void initView() {
        setContentView(R.layout.layout_dialog_voice_spy_setting);

        txtSpyWord = (EditText) findViewById(R.id.txt_topic1);
        txtGoodWord = (EditText) findViewById(R.id.txt_topic2);
        String regex = "[^a-zA-Z\u4E00-\u9FA5]";
        txtSpyWord.addTextChangedListener(new LimitTextWatcher(txtSpyWord, regex));
        txtGoodWord.addTextChangedListener(new LimitTextWatcher(txtGoodWord, regex));

        btnSystemWords = (Button) findViewById(R.id.btn_system_word);
        btnUserWords = (Button) findViewById(R.id.btn_player_word);

        switch1 = (CheckBox) findViewById(R.id.switch_add_spy);
        switch2 = (CheckBox) findViewById(R.id.switch_set_spy_to_ghost);
        switch3 = (CheckBox) findViewById(R.id.switch_whether_insert_mic);
        switch4 = (CheckBox) findViewById(R.id.switch_spy_guess_word);
        switch1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                if (VoiceRoomConfig.getOnSeatNumber() < VoiceConstants.CAN_ADD_SPY_NUM) {
                    switch1.setChecked(false);
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_add_spy_limit));
                }
            }
        });

        btnConfirm = (Button) findViewById(R.id.btn_sure);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
    }

    @Override
    public void initListener() {

        btnSystemWords.setOnClickListener(v -> getSystemWords());
        btnUserWords.setOnClickListener(v -> getPlayerWords());

        btnConfirm.setOnClickListener(v -> {
            sendConfirmSetting();
        });

        btnCancel.setOnClickListener(v -> {
            sendFinishedSetting();
            dismiss();
        });
    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initData() {
        String spyWord = VoiceRoomConfig.SpyConfig.getFirst();
        String goodWord = VoiceRoomConfig.SpyConfig.getSecond();
        boolean addSpy = VoiceRoomConfig.SpyConfig.isAddOneSpy();
        boolean setSpyToGhost = VoiceRoomConfig.SpyConfig.isSetSpyToGhost();
        boolean canInsertMic = VoiceRoomConfig.SpyConfig.isCanInsertMic();
        boolean canGuessWord = VoiceRoomConfig.SpyConfig.isCanGuessWord();

        txtSpyWord.setText(spyWord);
        txtGoodWord.setText(goodWord);
        switch1.setChecked(addSpy);
        switch2.setChecked(setSpyToGhost);
        switch3.setChecked(canInsertMic);
        switch4.setChecked(canGuessWord);
    }

    @Override
    protected void release() {

    }

    //点击系统词库
    private void getSystemWords() {
        VoiceSocketHelper.sendSpyGetSystemWords();
    }

    //点击玩家词库
    private void getPlayerWords() {
        VoiceSocketHelper.sendSpyGetCustomWords();
    }

    private void sendConfirmSetting() {
        String spyWord = "";
        String goodWord = "";
        Editable text1 = txtSpyWord.getText();
        Editable text2 = txtGoodWord.getText();
        if (text1 != null) {
            spyWord = text1.toString().trim();
        }
        if (text2 != null) {
            goodWord = text2.toString().trim();
        }
        boolean addOneSpy = switch1.isChecked();
        boolean setSpyToGhost = switch2.isChecked();
        boolean canInsertMic = switch3.isChecked();
        boolean canGuessWord = switch4.isChecked();

        if (TextUtils.isEmpty(spyWord)) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_tip_spy_word_empty));
            return;
        }
        if (TextUtils.isEmpty(goodWord)) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_tip_good_word_empty));
            return;
        }
        VoiceRoomConfig.SpyConfig.setIsDefaultSetting(false);//不是默认设置
        VoiceSocketHelper.sendSpySettingUpdate(addOneSpy, setSpyToGhost, canInsertMic, canGuessWord, spyWord, goodWord);
        sendFinishedSetting();
        dismiss();
    }

    private void sendFinishedSetting() {
        VoiceSocketHelper.sendSpySettingStop();
    }

    public void handleGetSystemWords(VoiceWords words) {
        txtSpyWord.setText(words.first);
        txtGoodWord.setText(words.second);
    }

    @Override
    public void destroy() {

    }
}
