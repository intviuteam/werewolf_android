package orangelab.project.voice.handler;

import android.text.TextUtils;

import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.lobby.api.LobbyRoomConstants;
import orangelab.project.voice.musiccompany.api.MusicApiConstants;
import orangelab.project.voice.utils.TransferClassHelper;

import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 02/01/2018.
 * //抽象模版模式的socket消息处理器
 */

public abstract class AbstractSocketMessageHandler implements Keepable, Destroyable {

    protected SafeHandler safeHandler = new SafeHandler();

    //通用消息 start====================================================================
    public void handleJoin(VoiceMessageBean messageBean) {
    }//进入房间

    public void handleLeave(VoiceMessageBean messageBean) {
    }//离开房间

    public void handleForceSeat(VoiceMessageBean messageBean) {
    }//抱用户上麦

    public void handleUpSeat(VoiceMessageBean messageBean) {
    }//上麦

    public void handleDownSeat(VoiceMessageBean messageBean) {
    }//下麦

    public void handleSpeak(VoiceMessageBean messageBean) {
    }//说话

    public void handleUnSpeak(VoiceMessageBean messageBean) {
    }//停止说话

    public void handleKickOut(VoiceMessageBean messageBean) {
    }//踢出房间

    public void handleChat(VoiceMessageBean messageBean) {
    }//聊天(包括送礼)

    public void handleSystemMsg(VoiceMessageBean messageBean) {
    }//系统消息

    public void handleLocked(VoiceMessageBean messageBean) {
    }//锁座位

    public void handleRestoreRoom(VoiceMessageBean messageBean) {
    }//异常状态复原(断网,杀死app)

    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
    }//更换房主

    public void handleUpdateTitle(VoiceMessageBean messageBean) {
    }//更新标题

    public void handleChangePassword(VoiceMessageBean messageBean) {
    }//更改密码

    public void handleAddFriend(VoiceMessageBean messageBean) {
    }//加好友

    public void handleLikeRoom(VoiceMessageBean messageBean) {
    }//点赞

    public void handleChangeState(VoiceMessageBean messageBean) {
    }//更改用户状态:自由模式、禁麦、恢复正常模式

    public void handleEmotion(VoiceMessageBean messageBean) {
    }//发表情

    public void handleSmallGame(VoiceMessageBean messageBean) {
    }//小游戏

    public void handleCardCheckResult(VoiceMessageBean messageBean) {
    }//送礼验证

    public void handleUpdateConfig(VoiceMessageBean messageBean) {
    }//更新房间设置

    public void handleRequestFreeMode(VoiceMessageBean messageBean) {
    }//请求开启自由模式

    public void handleAcceptFreeMode(VoiceMessageBean messageBean) {
    }//接受开启自由模式

    public void handleRejectFreeMode(VoiceMessageBean messageBean) {
    }//拒绝开启自由模式

    public void handlePurchasedResult(VoiceMessageBean messageBean) {
    }//购买房契结果

    public void handleHandleOverCreatorResult(VoiceMessageBean messageBean) {
    }//转让房契结果

    public void handleBlackList(VoiceMessageBean messageBean) {
    }//获取黑名单

    public void handleUnBlack(VoiceMessageBean messageBean) {
    }//解除黑名单

    public void handleAdminList(VoiceMessageBean messageBean) {
    }//获取管理员

    public void handleUnAdmin(VoiceMessageBean messageBean) {
    }//解除管理员

    //通用消息 end====================================================================//


    //谁是卧底相关消息 start====================================================================//
    public void handleSpyStartConfig() {
    }//开始"谁是卧底"相关设置

    public void handleGetWords(VoiceMessageBean messageBean) {
    }//获取词语

    public void handleSpyUpdateConfig(VoiceMessageBean messageBean) {
    }//更新设置

    public void handleSpyStopConfig() {
    }//设置完成

    public void handleSpyGamePrepare(VoiceMessageBean messageBean) {
    }//准备

    public void handleSpyGameUnPrepare(VoiceMessageBean messageBean) {
    }//取消准备

    public void handleSpyGameStart(VoiceMessageBean messageBean) {
    }//开始游戏

    public void handleAssignedRole(VoiceMessageBean messageBean) {
    }//分配词语

    public void handleSpyGameSpeech(VoiceMessageBean messageBean) {
    }//开始发言

    public void handleSpyGameEndSpeech(VoiceMessageBean messageBean) {
    }//结束发言,目前server没有返回此消息

    public void handleBeforeVote(VoiceMessageBean messageBean) {
    }//开始投票

    public void handleSpyGameVoteResult(VoiceMessageBean messageBean) {
    }//投票结果

    public void handleDeathInfo(VoiceMessageBean messageBean) {
    }//死亡信息

    public void handleSpyGameGuessWord(VoiceMessageBean messageBean) {
    }//卧底开始猜词

    public void handleGuessWordResult(VoiceMessageBean messageBean) {
    }//卧底猜词结果

    public void handleSpyGameOver(VoiceMessageBean messageBean) {
    }//游戏结束

    public void handleDisconnect(VoiceMessageBean messageBean) {
    }//玩家离线

    public void handleConnect(VoiceMessageBean messageBean) {
    }//玩家上
    // 谁是卧底相关消息 end====================================================================//

    // 游戏大厅消息处理 start====================================================================//
    public void handleLobbyInviteGame(VoiceMessageBean messageBean) {
    }//大厅游戏邀请

    public void handleLobbyAcceptGame(VoiceMessageBean messageBean) {
    }//大厅游戏接受

    public void handleMiniGameServer(VoiceMessageBean messageBean) {
    }//小游戏返回消息
    // 游戏大厅消息处理 end====================================================================//


    //预处理 start====================================================================//
    public void handlePreDownSeat(int position) {
    }//预先下麦操作
    //预处理 start====================================================================//


    //提供给数据处理 start===============================================
    public void handleMessageEvent(ServerMessageEvent event) {
        handleSocketEvent(TransferClassHelper.ServerMessage2VoiceMessage(event));
    }

    public void handleResponseEvent(ServerResponseEvent event) {
        handleSocketEvent(TransferClassHelper.ServerResponse2VoiceMessage(event));
    }
    //提供给数据处理 end===============================================


    //提供给UI处理 start===============================================
    public void handleMessageEvent(ServerMessageEventOver event) {
        handleSocketEvent(TransferClassHelper.ServerMessageOver2VoiceMessage(event));
    }

    public void handleResponseEvent(ServerResponseEventOver event) {
        handleSocketEvent(TransferClassHelper.ServerResponseOver2VoiceMessage(event));
    }
    //提供给UI处理 end===============================================

    //音乐伴奏相关 start======================================================
    public void handleStartSing(VoiceMessageBean messageBean) {
    }//开始伴奏

    public void handleStopSing(VoiceMessageBean messageBean) {
    }//停止伴奏

    public void handleBookSong(VoiceMessageBean messageBean) {
    }//排麦

    public void handlePeakSong(VoiceMessageBean messageBean) {
    }//顶歌

    public void handleRemoveSong(VoiceMessageBean messageBean) {
    }//删歌

    public void handleAuthorizeSing(VoiceMessageBean messageBean) {
    }//授权

    public void handlePrepareSing(VoiceMessageBean messageBean) {
    }//准备唱歌
    //音乐伴奏相关 end======================================================

    //红包相关 start======================================================
    public void handleSendPacket(VoiceMessageBean messageBean) {
    }//发送红包
    //红包相关 end======================================================

    //逻辑处理
    public void handleSocketEvent(VoiceMessageBean messageBean) {
        String type = messageBean.type;
        PLog.e("handleSocketEvent", "handleSocketEvent come in, type is " + type);
        if (TextUtils.equals(VoiceConstants.TYPE_JOIN, type)) {
            handleJoin(messageBean);
        } else if (TextUtils.equals(VoiceConstants.LEAVE, type)) {
            handleLeave(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPEAK, type)) {
            handleSpeak(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UNSPEAK, type)) {
            handleUnSpeak(messageBean);
        } else if (TextUtils.equals(VoiceConstants.KICK_OUT, type)) {
            handleKickOut(messageBean);
        } else if (TextUtils.equals(VoiceConstants.TYPE_CHAT, type)) {
            handleChat(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SYSTEM_MSG, type)) {
            handleSystemMsg(messageBean);
        } else if (TextUtils.equals(VoiceConstants.LOCK, type)) {
            handleLocked(messageBean);
        } else if (TextUtils.equals(VoiceConstants.FORCE_SEAT, type)) {
            handleForceSeat(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UP_SEAT, type)) {
            handleUpSeat(messageBean);
        } else if (TextUtils.equals(VoiceConstants.DOWN_SEAT, type)) {
            handleDownSeat(messageBean);
        } else if (TextUtils.equals(VoiceConstants.RESTORE_ROOM, type)) {
            handleRestoreRoom(messageBean);
        } else if (TextUtils.equals(VoiceConstants.HAND_OVER_MASTER, type)) {
            handleChangeRoomOwner(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UPDATE_TITLE, type)) {
            handleUpdateTitle(messageBean);
        } else if (TextUtils.equals(VoiceConstants.CHANGE_PASSWORD, type)) {
            handleChangePassword(messageBean);
        } else if (TextUtils.equals(VoiceConstants.TYPE_ADD_FRIEND, type)) {
            handleAddFriend(messageBean);
        } else if (TextUtils.equals(VoiceConstants.LIKE_ROOM, type)) {
            handleLikeRoom(messageBean);
        } else if (TextUtils.equals(VoiceConstants.CHANGE_USER_STATE, type)) {
            handleChangeState(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SHOW_EMOTION, type)) {
            handleEmotion(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SHOW_GAME, type)) {
            handleSmallGame(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_START_CONFIG, type)) {
            handleSpyStartConfig();
        } else if (TextUtils.equals(VoiceConstants.SPY_UPDATE_CONFIG, type)) {
            handleSpyUpdateConfig(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GET_WORDS_RESPONSE, type)) {
            handleGetWords(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_STOP_CONFIG, type)) {
            handleSpyStopConfig();
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_PREPARE, type)) {
            handleSpyGamePrepare(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UNPREPARE, type)) {
            handleSpyGameUnPrepare(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_VOTE_RESULT, type)) {
            handleSpyGameVoteResult(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_DEATH_INFO, type)) {
            handleDeathInfo(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_GUESS_WORD, type)) {
            handleSpyGameGuessWord(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_GUESS_WORD_RESULT, type)) {
            handleGuessWordResult(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_ASSIGNED_ROLE, type)) {
            handleAssignedRole(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_START, type)) {
            handleSpyGameStart(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_SPEECH, type)) {
            handleSpyGameSpeech(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_END_SPEECH, type)) {
            handleSpyGameEndSpeech(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_VOTE, type)) {
            handleBeforeVote(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_OVER, type)) {
            handleSpyGameOver(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_DISCONNECT, type)) {
            handleDisconnect(messageBean);
        } else if (TextUtils.equals(VoiceConstants.SPY_GAME_CONNECT, type)) {
            handleConnect(messageBean);
        } else if (TextUtils.equals(Constant.CARD_CHECK_RESULT, type)) {
            handleCardCheckResult(messageBean);
        } else if (TextUtils.equals(VoiceConstants.REQUEST_FREE_MODE, type)) {
            handleRequestFreeMode(messageBean);
        } else if (TextUtils.equals(VoiceConstants.ACCEPT_FREE_MODE, type)) {
            handleAcceptFreeMode(messageBean);
        } else if (TextUtils.equals(VoiceConstants.REJECT_FREE_MODE, type)) {
            handleRejectFreeMode(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UPDATE_CONFIG, type)) {
            handleUpdateConfig(messageBean);
        } else if (TextUtils.equals(VoiceConstants.PURCHASE_ROOM_RESULT, type)) {
            handlePurchasedResult(messageBean);
        } else if (TextUtils.equals(VoiceConstants.HAND_OVER_CREATOR_RESULT, type)) {
            handleHandleOverCreatorResult(messageBean);
        } else if (TextUtils.equals(VoiceConstants.VOICE_BLACK_LIST, type)) {
            handleBlackList(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UN_BLACK, type)) {
            handleUnBlack(messageBean);
        } else if (TextUtils.equals(VoiceConstants.PACKET_SEND, type)) {
            handleSendPacket(messageBean);
        } else if (TextUtils.equals(VoiceConstants.VOICE_ADMIN_LIST, type)) {
            handleAdminList(messageBean);
        } else if (TextUtils.equals(VoiceConstants.UN_ADMIN, type)) {
            handleUnAdmin(messageBean);
        }


        //处理ktv相关消息
        if (VoiceRoomConfig.isIsKTV()) {
            handleKtvEvent(messageBean);
        }

        //大厅消息处理
        if (VoiceRoomConfig.isLobby()) {
            handleLobbySocketEvent(messageBean);
        }
    }


    //处理ktv消息 start===================================================//
    public void handleKtvEvent(VoiceMessageBean messageBean) {
        String type = messageBean.type;
        if (TextUtils.equals(MusicApiConstants.BOOK_SONG, type)) {
            handleBookSong(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.START_SING, type)) {
            handleStartSing(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.STOP_SING, type)) {
            handleStopSing(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.MUSIC_POP_SONG, type)) {
            handlePeakSong(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.MUSIC_REMOVE_SONG, type)) {
            handleRemoveSong(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.MUSIC_AUTHORIZE_SING, type)) {
            handleAuthorizeSing(messageBean);
        } else if (TextUtils.equals(MusicApiConstants.MUSIC_PREPARE_SONG, type)) {
            handlePrepareSing(messageBean);
        }
    }


    /**
     * 大厅消息处理
     */
    public void handleLobbySocketEvent(VoiceMessageBean messageBean) {
        PLog.e("handleLobbySocketEvent", "messageBean: " + messageBean);
        if (TextUtils.equals(LobbyRoomConstants.LOBBY_GAME_INVITE, messageBean.type)) {
            handleLobbyInviteGame(messageBean);
        } else if (TextUtils.equals(LobbyRoomConstants.LOBBY_GAME_ACCEPT, messageBean.type)) {
            handleLobbyAcceptGame(messageBean);
        } else if (TextUtils.equals(LobbyRoomConstants.PLAY_MINI_GAME_SERVER, messageBean.type)) {
            handleMiniGameServer(messageBean);
        }
    }
    //处理ktv消息 end===================================================//

}
