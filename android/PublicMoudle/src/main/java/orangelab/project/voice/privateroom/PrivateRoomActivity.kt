package orangelab.project.voice.privateroom

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.WindowManager
import com.R
import com.androidtoolkit.RxToolKit
import orangelab.project.common.activity.SafeActivity
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.voice.manager.VoiceRoomCommander
import com.toolkit.action.Destroyable
import orangelab.project.common.activity.RoomSafeActivity
import orangelab.project.common.engine.SocketEngine
import orangelab.project.common.model.ServerMessageEvent
import org.json.JSONObject

class PrivateRoomActivity : RoomSafeActivity(), Destroyable {

    companion object {
        val TAG = "PrivateRoomActivity"

        fun Launch(context: Context) {
            val intent = Intent(context, PrivateRoomActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var mUIManager: PrivateRoomController

    override fun onActivityWindowInitFinish() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_privateroom)

        var mPrivateRoomLightContext = VoiceRoomCommander.ContextManager.GetPrivateRoomContext()

        if (mPrivateRoomLightContext == null) {
            /**
             * 此时发现Context 已经 不存在了 直接销毁
             */
            finish()
            return
        }

        mUIManager = PrivateRoomController(this, findViewById<ViewGroup>(R.id.id_root_view) as ViewGroup, mPrivateRoomLightContext!!)

        initWindow()
    }

    fun initWindow() {
        window.addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (mUIManager?.dispatchTouchEvent(ev)) {
            return true
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onStart() {
        super.onStart()
        mUIManager?.onActivityStart()
    }

    override fun onResume() {
        super.onResume()
        mUIManager?.onActivityResume()
    }

    override fun onPause() {
        super.onPause()
        mUIManager?.onActivityPause()
    }

    override fun onStop() {
        super.onStop()
        mUIManager.onActivityStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun destroy() {

    }

    override fun finish() {
        super.finish()
        destroy()
    }

    override fun onBackPressed() {
        mUIManager?.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mUIManager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun printInfo(info: String) {
        Log.i(TAG, info)
    }

}
