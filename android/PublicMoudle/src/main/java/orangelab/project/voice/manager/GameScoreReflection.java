package orangelab.project.voice.manager;

import com.R;

/**
 * Created by lloydfinch on 01/11/2017.
 */

public class GameScoreReflection {

    //打分
    public static int scoreToImageId(int score) {
        int imageId = R.mipmap.ico_voice_game_score_1;
        switch (score) {
            case 1:
                imageId = R.mipmap.ico_voice_game_score_1;
                break;
            case 2:
                imageId = R.mipmap.ico_voice_game_score_2;
                break;
            case 3:
                imageId = R.mipmap.ico_voice_game_score_3;
                break;
            case 4:
                imageId = R.mipmap.ico_voice_game_score_4;
                break;
            case 5:
                imageId = R.mipmap.ico_voice_game_score_5;
                break;
            case 6:
                imageId = R.mipmap.ico_voice_game_score_6;
                break;
            case 7:
                imageId = R.mipmap.ico_voice_game_score_7;
                break;
            case 8:
                imageId = R.mipmap.ico_voice_game_score_8;
                break;
            case 9:
                imageId = R.mipmap.ico_voice_game_score_9;
                break;
        }

        return imageId;
    }

    //骰子
    public static int diceToImageId(int point) {
        int imageId = R.mipmap.ico_game_dice_1;
        switch (point) {
            case 1:
                imageId = R.mipmap.ico_game_dice_1;
                break;
            case 2:
                imageId = R.mipmap.ico_game_dice_2;
                break;
            case 3:
                imageId = R.mipmap.ico_game_dice_3;
                break;
            case 4:
                imageId = R.mipmap.ico_game_dice_4;
                break;
            case 5:
                imageId = R.mipmap.ico_game_dice_5;
                break;
            case 6:
                imageId = R.mipmap.ico_game_dice_6;
                break;
        }

        return imageId;
    }

    //猜拳
    public static int guessToImageId(int result) {
        int imageId = R.mipmap.ico_caiquan_shitou;
        switch (result) {
            case 1:
                imageId = R.mipmap.ico_caiquan_shitou;
                break;
            case 2:
                imageId = R.mipmap.ico_caiquan_jiandao;
                break;
            case 3:
                imageId = R.mipmap.ico_caiquan_bu;
                break;
        }

        return imageId;
    }

    //抛硬币
    public static int coinToImageId(int result) {
        int imageId = R.mipmap.ico_coin_zhenxinhua;
        switch (result) {
            case 1:
                imageId = R.mipmap.ico_coin_zhenxinhua;
                break;
            case 2:
                imageId = R.mipmap.ico_coin_damaoxian;
                break;
        }

        return imageId;
    }
}
