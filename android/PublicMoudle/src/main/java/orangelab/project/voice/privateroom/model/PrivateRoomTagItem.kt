package orangelab.project.voice.privateroom.model

/**
 * game_werewolf
 * 2018/3/25 下午8:39
 * Mystery
 */

class PrivateRoomTagItem {
    var tagColor: String = "#222222"
    var tagText: String = ""
    var clicked = false

    constructor(tagColor: String, tagText: String) {
        this.tagColor = tagColor
        this.tagText = tagText
    }


}
