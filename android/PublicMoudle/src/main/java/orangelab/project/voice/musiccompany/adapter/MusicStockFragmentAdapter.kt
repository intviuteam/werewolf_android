package orangelab.project.voice.musiccompany.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import orangelab.project.voice.musiccompany.fragment.MusicOrderedFragment

/**
 * 曲库/已点 fragment适配器
 */
class MusicStockFragmentAdapter(fragmentManager: FragmentManager, fragments: ArrayList<Fragment>) : FragmentPagerAdapter(fragmentManager) {

    private var fragments: ArrayList<Fragment> = fragments
    var titles: ArrayList<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        if (titles.isEmpty()) {
            return ""
        }
        return titles[position]
    }

    override fun getItemPosition(`object`: Any): Int {
        (`object` as MusicOrderedFragment).refresh()
        return super.getItemPosition(`object`)
    }

//    override fun getItemPosition(`object`: Any?): Int {
//        (`object` as MusicOrderedFragment).refresh()
//        return super.getItemPosition(`object`)
//    }
}
