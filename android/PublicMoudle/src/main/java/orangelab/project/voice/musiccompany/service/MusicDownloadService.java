package orangelab.project.voice.musiccompany.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.androidtoolkit.FileToolKit;

import org.greenrobot.eventbus.EventBus;

import orangelab.project.MainApplication;
import orangelab.project.common.db.MusicDataEntityDaoHelper;
import orangelab.project.common.db.entity.MusicDataEntity;
import orangelab.project.voice.musiccompany.concrete.MusicDownLoader;
import orangelab.project.voice.musiccompany.concrete.MusicState;
import orangelab.project.voice.musiccompany.event.MusicEvent;

/**
 * 下载的后台服务类，下载任务都在这里进行
 */
public class MusicDownloadService extends Service {

    private static final String TAG = "MusicDownloadService";
    public static final String SERVICE_EXTRA_SIGNAL = "downloadParcel";       //外界启动服务传递数据的key值
    public static final String POSITION = "POSITION";                         //当前下载的音乐在列表中的位置key值
    public MusicDataEntityDaoHelper daoHelper = new MusicDataEntityDaoHelper(MainApplication.getInstance());

    public MusicDownloadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            MusicDataEntity data = intent.getParcelableExtra(SERVICE_EXTRA_SIGNAL);
            int position = intent.getIntExtra(POSITION, -1);
            if (data != null) {
                download(data, position);
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 进行下载
     */
    private void download(final MusicDataEntity download, final int position) {

        MusicDownLoader.download(download.getPlay_url(), new MusicDownLoader.MusicDownLoadCallback() {

            @Override
            public void onStart() {
                download.setState(MusicState.Companion.getSTATE_DOWN_ING());
                //daoHelper.insert(download);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }

            @Override
            public void onWait() {
                download.setState(MusicState.Companion.getSTATE_DOWN_WAIT());
                //daoHelper.update(download);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }

            @Override
            public void onPause() {
                download.setState(MusicState.Companion.getSTATE_DOWN_PAUSE());
                //daoHelper.update(download);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }

            @Override
            public void onSuccess(byte[] data) {
                download.setState(MusicState.Companion.getSTATE_DOWN_FINISH());
                daoHelper.insert(download);
                //daoHelper.update(download);
                FileToolKit.saveToMp3(download.getPlay_url(), data);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }

            @Override
            public void onFailure(String message) {
                download.setState(MusicState.Companion.getSTATE_DOWN_FAILED());
                daoHelper.delete(download);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }

            @Override
            public void onProgressUpdate(int progress, int position) {
                download.setProgress(progress);
                post(new MusicEvent.MusicDownLoadEvent(download, position));
            }
        }, position);
    }

    /**
     * 发布下载进度
     */
    private void post(MusicEvent.MusicDownLoadEvent event) {
        EventBus.getDefault().post(event);
    }
}
