package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;
import orangelab.project.common.dialog.SafeDialog;

import java.util.HashMap;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 31/10/2017.
 * //打分对话框
 */

public class VoiceGameScoreDialog extends SafeDialog implements Destroyable {

    private static final String TAG = "VoiceGameScoreDialog";
    private static final int DEFAULT_SCORE = 5;//默认5分

    private View btnSure;
    private View btnCancel;
    private HashMap<Integer, TextView> scoreTvs = new HashMap<>();
    private OnOkClickedListener onOkClickedListener;

    private Context mContext;
    private int currentScore = DEFAULT_SCORE;

    public VoiceGameScoreDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceGameScoreDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_game_score, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        initScoreViews(contentView);
        updateUI(DEFAULT_SCORE);
    }

    private void initScoreViews(View contentView) {
        for (int i = 1; i <= 9; i++) {
            String idField = "tv_num_" + i;
            try {
                int viewId = R.id.class.getField(idField).getInt(null);
                TextView textView = (TextView) contentView.findViewById(viewId);
                textView.setTag(i);
                scoreTvs.put(i, textView);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(380);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }

        params.gravity = Gravity.BOTTOM;
        params.y = ScreenUtils.dip2px(80);
        getWindow().setAttributes(params);
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            if (onOkClickedListener != null) {
                onOkClickedListener.onOkClicked(currentScore);
            }
            dismiss();
        });
        btnCancel.setOnClickListener(v -> dismiss());
        for (int index = 1; index <= scoreTvs.size(); index++) {
            TextView textView = scoreTvs.get(index);
            textView.setOnClickListener(v -> {
                Integer score = (Integer) textView.getTag();
                currentScore = score;
                updateUI(score);
            });
        }
    }

    private void updateUI(int exception) {
        for (int key = 1; key <= scoreTvs.size(); key++) {
            TextView textView = scoreTvs.get(key);
            if (textView == null) {
                continue;
            }
            if (key == exception) {
                textView.setBackgroundResource(R.color.color_voice_game_score_bg_checked);
            } else {
                textView.setBackgroundResource(R.color.color_voice_game_score_bg_normal);
            }
        }
    }

    public void setOnOkClickedListener(OnOkClickedListener onOkClickedListener) {
        this.onOkClickedListener = onOkClickedListener;
    }

    public interface OnOkClickedListener {
        void onOkClicked(int score);
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {
        scoreTvs.clear();
    }
}
