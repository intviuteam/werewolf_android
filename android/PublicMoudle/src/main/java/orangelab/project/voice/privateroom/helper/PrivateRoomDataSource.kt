package orangelab.project.voice.privateroom.helper

import android.text.TextUtils
import android.util.Log
import android.util.SparseArray
import com.datasource.GlobalUserState
import orangelab.project.common.effect.EffectsUtils
import orangelab.project.common.effect.event.RoomEffectEvent
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.common.model.action.ServerActionRestoreRoom
import orangelab.project.voice.privateroom.PrivateRoomTools
import orangelab.project.voice.privateroom.model.*
import org.greenrobot.eventbus.EventBus
import java.util.*

/**
 *game_werewolf
 *2018/3/22 下午4:47
 *Mystery
 */
class PrivateRoomDataSource {

    var mSelfPosition: Int = -1

    private var mSeatStartPosition = -1;
    private var mSeatEndPosition = -1

    private var mRoomSeats: MutableList<PrivateRoomSeatDataImpl> //用于存放座位信息 长度固定为麦上座位数量
    private var mRoomMember: SparseArray<PrivateRoomMemberDataImpl> //用于存放所有的用户数据 Key 为用户 position
    private var mRoomMemberUserIdMap: MutableMap<String, PrivateRoomMemberDataImpl> //用于存放所有的用户数据 Key 为用户ID

    private var mRoomChatMessage: LinkedList<Any>   //用于存放所有语音房的聊天数据
    private val mMaxChatMessageCount = 300 //最大300条数据
    private val mChatClearMessageCount = 30//当达到最大条数限制的数据时，清理的数据量

    private var mPaperPlaneCache: LinkedList<PrivateRoomPaperPlane> //纸飞机的Cache

    private var mSendTempImageEnable = false
    var mBlockUpSeatDuration = 0L

    var mMiniModeEnable = true

    var mRoomTag = ""
    var mRoomTagColor = "#ff8eff"

    var mRoomTitle = ""
    var mRoomIntroduce = ""
    var mRoomPassword = ""
    var mRoomId = ""
    var mRoomType = ""

    var mRoomLikeCount: Long = 0

    var mRoomBackGroundType: String = ""
    var mRoomBackGroundUrl: String = ""
    var mRoomBackGroundChanged = false

    constructor() {
        mRoomMember = SparseArray()
        mRoomMemberUserIdMap = HashMap()
        mRoomSeats = ArrayList()
        mRoomChatMessage = LinkedList()
        mPaperPlaneCache = LinkedList()
    }

    fun updateRoomPassword(password: String) {
        mRoomPassword = password
        GlobalUserState.getGlobalState().password = mRoomPassword
    }

    fun initSeats(start: Int, end: Int) {
        mSeatEndPosition = end
        mSeatStartPosition = start
        for (i: Int in start until end) {
            val seat = PrivateRoomSeatDataImpl(i)
            seat.isLock = false
            mRoomSeats.add(seat)
        }
    }

    fun initLockedSeats(locked: MutableList<Int>?) {
        if (locked != null && locked!!.size > 0) {
            mRoomSeats.forEach({
                if (locked.contains(it.position)) {
                    it.isLock = true
                } else {
                    it.isLock = false
                }
            })
            printMemberData()
        }
    }

    fun updateLock(locked: MutableList<Int>?) {
        if (locked != null && locked!!.size > 0) {
            mRoomSeats.forEach({
                val data = it
                if (locked.contains(data.position)) {
                    data.isLock = true
                } else {
                    data.isLock = false
                }
            })
        } else {
            mRoomSeats.forEach({
                val data = it
                data.isLock = false
            })
        }
        printMemberData()
    }

    fun updateMemberSpeaking(position: Int, isSpeaking: Boolean) {
        val temp = get(position)
        if (temp == null) {
            return
        }
        temp.isSpeaking = isSpeaking
    }

    fun getRoomMembersTotalCount(): Int {
        return mRoomMember.size()
    }

    fun remove(position: Int) {
        val temp = mRoomMember.get(position)
        if (temp == null) {
            return
        }
        mRoomMemberUserIdMap.remove(temp.id)
        mRoomMember.remove(position)
        /**
         * 根据需要计算seats数据是否有变动
         */
        if (position >= mSeatEndPosition) {

        } else {
            var seatFrom = mRoomSeats.get(position)
            seatFrom.memberData = null
            temp.isUpSeat = false
        }
        printMemberData()
    }

    fun remove(userId: String) {
        val temp = mRoomMemberUserIdMap.get(userId)
        if (temp == null) {
            return
        }
        mRoomMemberUserIdMap.remove(temp.id)
        mRoomMember.remove(temp.position)
        /**
         * 根据需要计算seats数据是否有变动
         */
        if (temp.position >= mSeatEndPosition) {

        } else {
            var seatFrom = mRoomSeats.get(temp.position)
            seatFrom.memberData = null
            temp.isUpSeat = false
        }
        printMemberData()
    }

    fun addMember(position: Int, data: PrivateRoomMemberDataImpl?) {
        if (data == null) {
            return
        }
        mRoomMember.put(position, data)
        mRoomMemberUserIdMap.put(data.id!!, data)
    }

    fun locateMember(position: Int, data: PrivateRoomMemberDataImpl?) {
        if (data == null) {
            return
        }

        addMember(position, data)
        /**
         * 根据需要计算seats数据是否有变动
         */
        if (position >= mSeatEndPosition) {

        } else {
            var seatFrom = mRoomSeats.get(position)
            seatFrom.memberData = data
            data.isUpSeat = true
        }
        printLog("roomMember${mRoomMember}")
        printLog("seatsData${mRoomSeats}")
    }

    fun get(position: Int): PrivateRoomMemberDataImpl? {
        return mRoomMember.get(position)
    }

    fun getSeat(position: Int): PrivateRoomSeatDataImpl? {
        return mRoomSeats.get(position)
    }

    fun get(userId: String): PrivateRoomMemberDataImpl? {
        return mRoomMemberUserIdMap.get(userId)
    }

    fun getSeatsData(): MutableList<PrivateRoomSeatDataImpl> {
        return mRoomSeats
    }

    fun changePosition(from: Int, to: Int) {
        var temp = get(from)
        if (temp == null) {
            return
        }

        temp.position = to
//        temp.isSpeaking = false    20180509 变换位置的时候 不再首先强制更改玩家isSpeaking状态，而是由下面进行判断
        /**
         * 如果from是房主，则标记为不是房主
         */
        if (from == PrivateRoomTools.MASTER_POSITION) {
            temp.isMaster = false
        }

        if (from == PrivateRoomTools.DEPUTY_POSITION) {
            temp.isDeputy = false
        }

        /**
         * 如果to的位置是房主，则标记为房主
         */
        if (to == PrivateRoomTools.MASTER_POSITION) {
            temp.isMaster = true
        }

        if (to == PrivateRoomTools.DEPUTY_POSITION) {
            temp.isDeputy = true
        }

        mRoomMember.remove(from)
        mRoomMember.put(temp.position, temp)

        /**
         * 根据需要计算seats数据是否有变动
         */
        if (from >= mSeatEndPosition) {
            /**
             * 之前就在麦下
             */
        } else {
            var seatFrom = mRoomSeats.get(from)
            seatFrom.memberData = null
            temp.isUpSeat = false
        }

        if (to >= mSeatEndPosition) {
            /**
             * 之后进入了麦下,  20180509 强制关闭isSpeaking状态
             */
            temp.isSpeaking = false
        } else {
            var seatTo = mRoomSeats.get(to)
            seatTo.memberData = temp
            temp.isUpSeat = true
        }

        /**
         * 更新自己的位置
         */
        if (temp.isSelf) {
            mSelfPosition = temp.position

            EffectsUtils.postUpdateContextData(mSelfPosition, PrivateRoomTools.MASTER_POSITION, PrivateRoomTools.PositionInUpSeat(mSelfPosition))
        }
        printMemberData()
    }

    fun getMembersData(): MutableList<PrivateRoomMemberData> {
        var result = ArrayList<PrivateRoomMemberData>()
        mRoomMemberUserIdMap.forEach({
            result.add(it.value)
        })
        return result
    }

    fun refreshData(enterRoomResult: EnterRoomResult) {
        mSelfPosition = enterRoomResult.position
        refreshData(enterRoomResult.room)

        EffectsUtils.postUpdateContextData(mSelfPosition, PrivateRoomTools.MASTER_POSITION, PrivateRoomTools.PositionInUpSeat(mSelfPosition))
    }

    fun refreshData(serverActionRestoreRoom: ServerActionRestoreRoom) {
        refreshData(serverActionRestoreRoom.room_info)
    }

    fun refreshData(roomInfo: EnterRoomResult.EnterRoomMessage) {

        /**
         * 恢复背景
         */
        try {
            val backgroundUrl = roomInfo.config.room_info.background.url
            val backgroundtype = roomInfo.config.room_info.background.type
            if (TextUtils.equals(mRoomBackGroundType, backgroundtype)
                    && TextUtils.equals(mRoomBackGroundUrl, backgroundUrl)) {
                /**
                 * 房间背景没有变化
                 */
                mRoomBackGroundChanged = false
            } else {
                /**
                 * 房间背景有变化
                 */
                mRoomBackGroundType = backgroundtype
                mRoomBackGroundUrl = backgroundUrl
                mRoomBackGroundChanged = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            /**
             * 处理背景时发生了错误，为防止崩溃，这里就直接认为房间背景没有变化
             */
            mRoomBackGroundChanged = false
        }

        /**
         * 恢复点赞数量
         */
        mRoomLikeCount = roomInfo.likeCount.toLong()

        /**
         * 用于restore 所有数据
         */
        val users = roomInfo.users
        var locked = roomInfo.locked_positions

        /**
         * 数据有问题
         */
        if (users == null || users.size == 0) {
            return
        }
        if (locked == null) {
            locked = ArrayList()
        }

        /**
         * 先清理所有的房间人员数据
         */
        mRoomMember.clear()
        mRoomMemberUserIdMap.clear()


        /**
         * 恢复所有人员的数据
         */
        users.forEach {
            addMember(it.key, PrivateRoomTools.TurnIntoPrivateRoomMemberFromEnterResult(it.value))
        }

        /**
         * 开始恢复座位的信息
         */
        mRoomSeats.forEach {
            /**
             * 注意这里面的座位信息如果有人的话存放的是老的数据
             */
            if (it.isLock) {
                /**
                 * 这个位置先前是锁着的
                 */
                if (locked.contains(it.position)) {
                    /**
                     * 这个位置现在还是锁的,说明状态没有更改
                     */

                } else {
                    /**
                     * 这个位置不再是锁的了
                     */
                    it.isNeedRefresh = true //标记为更新

                    it.isLock = false

                    /**
                     * 去查找这个位置的人
                     */
                    val member = get(it.position)
                    if (member == null) {
                        /**
                         * 这个位置不再被锁住但是没有人
                         */
                        it.memberData = null
                    } else {
                        /**
                         * 这个位置坐了人
                         */
                        it.memberData = member
                        member.isUpSeat = true
                    }
                }
            } else {
                /**
                 * 这个位置先前不是锁着的
                 */
                val oldMember = it.memberData  //老的人员

                if (locked.contains(it.position)) {
                    /**
                     * 这个位置现在被锁了
                     */
                    it.isNeedRefresh = true
                    it.isLock = true
                    it.memberData = null

                    if (oldMember == null) {
                        /**
                         * 之前这个位置没有人
                         */

                    } else {
                        /**
                         * 之前这个位置有人
                         */
                        val temp = get(oldMember.getMemberId()!!)
                        if (temp == null) {
                            /**
                             * 这个人已经不在房间了
                             */
                        } else {
                            /**
                             * 这个人现在还是在房间的
                             */
                            temp.isUpSeat = false
                        }
                    }

                } else {

                    val newMember = get(it.position) //新的人员
                    /**
                     * 新老成员比对
                     */
                    if (oldMember == null && newMember == null) {
                        /**
                         * 状态无需要改变
                         */
                    } else {
                        if (oldMember == null && newMember != null) {
                            /**
                             * 原来无人但是现在有人
                             */
                            it.isNeedRefresh = true
                            it.isLock = false
                            it.memberData = newMember
                            newMember.isUpSeat = true
                        }

                        if (oldMember != null && newMember == null) {
                            /**
                             * 原来有人但是现在无人了
                             */
                            it.isNeedRefresh = true
                            it.isLock = false
                            it.memberData = null

                            val temp = get(oldMember.getMemberId()!!)
                            if (temp == null) {
                                /**
                                 * 这个人已经不在房间了
                                 */
                            } else {
                                /**
                                 * 这个人现在还是在房间的
                                 */
                                temp.isUpSeat = false
                            }
                        }

                        if (oldMember != null && newMember != null) {
                            /**
                             * 之前之后都有人
                             */
                            if (TextUtils.equals(oldMember.getMemberId(), newMember.getMemberId())) {
                                /**
                                 * 人没有变动
                                 */
                                get(newMember.getMemberId()!!)?.isUpSeat = true
                            } else {
                                /**
                                 * 人变了
                                 */
                                it.isNeedRefresh = true
                                it.isLock = false
                                it.memberData = newMember
                                newMember.isUpSeat = true

                                val temp = get(oldMember.getMemberId()!!)
                                if (temp == null) {
                                    /**
                                     * 这个人已经不在房间了
                                     */
                                } else {
                                    /**
                                     * 这个人现在还是在房间的
                                     */
                                    temp.isUpSeat = false
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * 恢复房间基本信息
         */
        try {
            mRoomTitle = roomInfo.config.theme.title
            mRoomTag = roomInfo.config.theme.tag
            mRoomTagColor = roomInfo.config.theme.tag_color
            mRoomIntroduce = roomInfo.config.theme.description
            mRoomPassword = roomInfo.password
            mSendTempImageEnable = roomInfo.config.enable_share_image
        } catch (e: Exception) {
            e.printStackTrace()
        }

        printMemberData()
    }

    fun addGiftMsg(giftMessageItem: GiftMessageItem) {
        clearChatDataIfNeed()
        mRoomChatMessage.add(giftMessageItem)
    }

    fun addChatMsg(chatMessageItem: ChatMessageItem) {
        clearChatDataIfNeed()
        mRoomChatMessage.add(chatMessageItem)
    }

    fun addTempPicture(tempPictureMessageItem: TempPictureMessageItem) {
        clearChatDataIfNeed()
        mRoomChatMessage.add(tempPictureMessageItem)
    }

    fun addSysMsg(systemMessageItem: SystemMessageItem) {
        clearChatDataIfNeed()
        mRoomChatMessage.add(systemMessageItem)
    }

    fun getChatMessageList(): List<Any> {
        return mRoomChatMessage
    }

    fun clear() {
        mRoomMember.clear()
        mRoomMemberUserIdMap.clear()
        mRoomSeats.clear()
        mRoomChatMessage.clear()
        mPaperPlaneCache.clear()
    }

    fun addPaperPlane(paperPlane: PrivateRoomPaperPlane) {
        mPaperPlaneCache.add(paperPlane)
    }

    fun getPaperPlaneCache(): List<PrivateRoomPaperPlane> {
        return mPaperPlaneCache
    }

    fun cleartPaperPlaneCache() {
        mPaperPlaneCache.clear()
    }

    fun updateSendTempImageEnable(enable: Boolean) {
        mSendTempImageEnable = enable
        printLog("updateSendTempImageEnable : ${enable}")
    }

    fun getSendTempImageEnable(): Boolean {
        return mSendTempImageEnable
    }

    private fun clearChatDataIfNeed() {
        if (mRoomChatMessage.size >= mMaxChatMessageCount) {
            /**
             * 数据比较大 开始进行清理
             */
            for (i: Int in 0 until mChatClearMessageCount) {
                mRoomChatMessage.removeAt(0)
            }
            printLog("chat data too many so clear${mChatClearMessageCount}")
        }
    }

    private fun printLog(info: String) {
        Log.i("PrivateRoomDataSource", info)
    }

    private fun printMemberData() {
        printLog("roomMember${mRoomMember}")
        printLog("seatsData${mRoomSeats}")
    }

}