package orangelab.project.voice.handler;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.event.GlobalEvent;
import orangelab.project.common.model.BaseServerEvent;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomAudioSocketManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.musiccompany.api.MusicApiConstants;

/**
 * Created by lloydfinch on 02/01/2018.
 * <p>
 * Update by lloydfinch on 03/04/2018
 * message:由原来的有序广播改为顺序调用,避免异步引起的问题
 * <p>
 * Update by lloydfinch 0n 03/21/2018
 * message:添加转换器,可以在消息处理前或后进行处理器切换处理
 * <p>
 * //TODO list替换为map，可以进行角色替换
 * //socket消息处理器
 */

public class VoiceServerMessageHandler extends AbstractSocketMessageHandler {

    private static final String TAG = "VoiceServerMessageHandler";
    private String enginToken = "";
    private static VoiceServerMessageHandler instance;
    private boolean needFilter = false;//是否需要过滤
    private boolean isRegister = false;//是否已经注册，防止重复注册
    private List<AbstractSocketMessageHandler> PreMessageHandler = new ArrayList<>(0);//消息预处理器，需要在数据更改之前处理
    private List<AbstractSocketMessageHandler> UIMessageHandler = new ArrayList<>(0);//UI消息处理器，在数据更改之后处理
    private List<IHandlerSwitcher> switchers = new ArrayList<>(0);//需要切换角色的处理器集合

    public static VoiceServerMessageHandler getInstance() {
        if (instance == null) {
            synchronized (VoiceServerMessageHandler.class) {
                if (instance == null) {
                    instance = new VoiceServerMessageHandler();
                }
            }
        }
        return instance;
    }

    private VoiceServerMessageHandler() {
    }

    //注册消息监听
    public void register(String token) {
        if (!isRegister) {
            this.enginToken = token;
            initVoiceRoomDestroyEvent();
            initServerMessageListener();
            initServerResponseListener();
            isRegister = true;
        }
    }

    //反注册防止RxBus
    public void unRegister() {
        RxToolKit.UnRegister(this);
        isRegister = false;
    }


    //注册UI消息处理器
    public void registerUIHandler(AbstractSocketMessageHandler handler) {
        if (handler != null) {
            UIMessageHandler.add(handler);
        }
    }

    //反注册UI消息处理器
    public void unRegisterUIHandler(AbstractSocketMessageHandler handler) {
        UIMessageHandler.remove(handler);
    }

    //注册消息预处理器
    public void registerPreHandler(AbstractSocketMessageHandler handler) {
        if (handler != null) {
            PreMessageHandler.add(handler);
        }
    }

    //反注册消息预处理器
    public void unRegisterPreHandler(AbstractSocketMessageHandler handler) {
        PreMessageHandler.remove(handler);
    }

    //添加消息转换器
    public void addHandlerSwitcher(IHandlerSwitcher handlerSwitcher) {
        if (handlerSwitcher != null && !switchers.contains(handlerSwitcher)) {
            switchers.add(handlerSwitcher);
        }
    }

    //删除消息转换器
    public void removeHandlerSwitcher(IHandlerSwitcher handlerSwitcher) {
        if (switchers.contains(handlerSwitcher)) {
            switchers.remove(handlerSwitcher);
        }
    }

    @Override
    public void destroy() {
        unRegister();
        instance = null;
    }

    //消息拦截，在数据更改之前在这里做相关操作 start==========================================
    @Override
    public void handleForceSeat(VoiceMessageBean bean) {
        handleUpSeat(bean);
    }

    @Override
    public void handleUpSeat(VoiceMessageBean bean) {
        for (AbstractSocketMessageHandler handler : PreMessageHandler) {
            String userId = bean.payload.optString(VoiceConstants.USER_ID_LOWER);
            EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserById(userId);
            if (user != null) {
                handler.handlePreDownSeat(user.position);
            }
        }
    }

    @Override
    public void handleDownSeat(VoiceMessageBean bean) {
        for (AbstractSocketMessageHandler handler : PreMessageHandler) {
            String userId = bean.payload.optString(VoiceConstants.USER_ID_LOWER);
            EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserById(userId);
            if (user != null) {
                handler.handlePreDownSeat(user.position);
            }
        }
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean bean) {
        for (AbstractSocketMessageHandler handler : PreMessageHandler) {
            int position = RoomSocketEngineHelper.getPositionFromJSON(bean.payload);
            EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserByPosition(position);
            if (user != null) {
                handler.handlePreDownSeat(user.position);
            }
        }
    }
    //消息拦截，在数据更改之前在这里做相关操作 end==========================================

    //数据消息处理 start=====================================================
    private void dispatchToDataHandler(ServerMessageEvent event) {
        VoiceRoomDataSourceManager.getInstance().handleMessageEvent(event);
    }

    private void dispatchToDataHandler(ServerResponseEvent event) {
        VoiceRoomDataSourceManager.getInstance().handleResponseEvent(event);
    }
    //数据消息处理 end=====================================================

    //UI消息处理 start=====================================================
    private void dispatchToUIHandler(ServerMessageEventOver event) {
        for (AbstractSocketMessageHandler handler : UIMessageHandler) {
            handler.handleMessageEvent(event);
        }
    }

    private void dispatchToUIHandler(ServerResponseEventOver event) {
        for (AbstractSocketMessageHandler handler : UIMessageHandler) {
            handler.handleResponseEvent(event);
        }
    }
    //UI消息处理 end=====================================================


    //角色切换 start======================================================
    //在消息处理之前切换处理器
    private void switchHandlerBeforeHandleMessage() {
        for (IHandlerSwitcher handler : switchers) {
            handler.switchBeforeHandleMessage();
        }
    }

    //在消息处理之后切换处理器
    private void switchHandlerAfterHandleMessage() {
        for (IHandlerSwitcher handler : switchers) {
            handler.switchAfterHandleMessage();
        }
    }
    //角色切换 end======================================================


    //接收并处理server直接的message类型的消息
    private void initServerMessageListener() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(serverMessageEvent -> {
            safeHandler.postSafely(() -> {
                try {

                    //添加消息过滤，只处理语音房消息
                    if (!filterType(serverMessageEvent)) {
                        return;
                    }

                    //进行前置切换处理
                    switchHandlerBeforeHandleMessage();

                    //预处理
                    if (needFilter()) {
                        handleMessageEvent(serverMessageEvent);
                    }
                    //数据源处理
                    dispatchToDataHandler(serverMessageEvent);

                    //UI处理
                    dispatchToUIHandler(new ServerMessageEventOver(serverMessageEvent));

                    //进行后置切换处理
                    switchHandlerAfterHandleMessage();

                } catch (Exception e) {
                    ReportEventUtils.reportError(e);
                    PLog.e(TAG, "an error occur when handle server message, ths error is: " + e.getMessage());
                }
            });
        }).register();
    }

    //接收并处理server直接的处理response类型的消息
    private void initServerResponseListener() {
        RxToolKit.Build(this, ServerResponseEvent.class).action(serverResponseEvent -> {
            safeHandler.postSafely(() -> {
                try {

                    //添加消息过滤，只处理语音房消息
                    if (!filterType(serverResponseEvent)) {
                        return;
                    }

                    //添加过滤器,只处理满足类型的消息
                    if (!filterMessage(serverResponseEvent.type)) {
                        return;
                    }

                    //进行前置切换处理
                    switchHandlerBeforeHandleMessage();

                    //预处理
                    if (needFilter()) {
                        handleResponseEvent(serverResponseEvent);
                    }
                    //数据源处理
                    dispatchToDataHandler(serverResponseEvent);

                    //UI处理
                    dispatchToUIHandler(new ServerResponseEventOver(serverResponseEvent));

                    //进行后置切换处理
                    switchHandlerAfterHandleMessage();

                } catch (Exception e) {
                    ReportEventUtils.reportError(e);
                    PLog.e(TAG, "an error occur when handle server response, ths error is: " + e.getMessage());
                }
            });
        }).register();
    }

    /**
     * 对类型进行过滤，只处理某一房间的消息
     */
    private boolean filterType(BaseServerEvent event) {
        boolean filter = event.isAudio() || event.isPair();
        //return filter;
        //TODO 修改
        return true;
    }

    /**
     * 对消息进行过滤，只处理某一类型的消息
     */
    private boolean filterMessage(String type) {
        boolean filter = TextUtils.equals(type, VoiceConstants.SHOW_GAME) ||
                TextUtils.equals(type, VoiceConstants.VOICE_BLACK_LIST) ||
                TextUtils.equals(type, VoiceConstants.UN_BLACK) ||
                TextUtils.equals(type, MusicApiConstants.BOOK_SONG) ||
                TextUtils.equals(type, MusicApiConstants.MUSIC_POP_SONG);
        return filter;
    }

    //注册全局消息监听,收到就释放所有的消息监听
    private void initVoiceRoomDestroyEvent() {

//        //监听语音服务器销毁
//        RxToolKit.Build(this, ViewEvent.DestroyEvent.class).action(v -> {
//            //取消所有消息监听
//            unRegister();
//            //通知已销毁
//            RxToolKit.Emit(new GlobalEvent.VoiceDestroyCallBackEvent());
//        }).register();

        //监听全局消息，收到就销毁所有消息监听
        RxToolKit.Build(this, GlobalEvent.VoiceDestroyEvent.class)
                .action(event -> {
                    VoiceRoomGameSocketManager.getInstance().ShutDown(enginToken);
                    VoiceRoomAudioSocketManager.getInstance().ShutDown(null);
                }).register();
    }

    //是否需要在数据更改进行拦截操作
    private boolean needFilter() {
        return needFilter || PreMessageHandler.size() > 0;
    }

    public void releaseUIHandler() {
        PreMessageHandler.clear();
        UIMessageHandler.clear();
    }
}
