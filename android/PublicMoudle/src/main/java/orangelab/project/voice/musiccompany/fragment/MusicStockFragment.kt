package orangelab.project.voice.musiccompany.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.ToastToolKit
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.musiccompany.component.MusicStockDisplayer
import orangelab.project.voice.musiccompany.concrete.MusicManager
import orangelab.project.voice.musiccompany.concrete.MusicState
import orangelab.project.voice.musiccompany.event.MusicEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 曲库列表
 */
class MusicStockFragment : Fragment() {

    private lateinit var contentView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        contentView = inflater.inflate(R.layout.fragment_music_stock, container, false)
        init()
        return contentView
    }

    private fun init() {
        EventBus.getDefault().register(this)
        MusicManager.instance.refreshMusicStockDisplayer(MusicStockDisplayer(contentView))

        /**
         * 加载推荐歌曲
         */
        MusicManager.instance.loadCommendMusic()
    }

    /**
     * 下载状态更新的回调
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDownLoadUpdateEvent(event: MusicEvent.MusicDownLoadEvent) {
        MusicManager.instance.refreshMusicState(event.data, event.position)

        when (event.data.state) {
            MusicState.STATE_DOWN_PAUSE -> {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_music_down_load_pause, event.data.song_name));
            }
            MusicState.STATE_DOWN_FAILED -> {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_music_down_load_error, event.data.song_name));
            }
            MusicState.STATE_DOWN_FINISH -> {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_music_down_load_finish, event.data.song_name));
            }
        }
    }

    fun refresh() {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
    }
}
