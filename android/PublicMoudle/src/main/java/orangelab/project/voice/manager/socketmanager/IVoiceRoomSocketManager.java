package orangelab.project.voice.manager.socketmanager;

import orangelab.project.common.model.EnterRoomResult;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 30/12/2017.
 */

public interface IVoiceRoomSocketManager extends Destroyable, Keepable {

    void Start(EnterRoomResult result);

    void ShutDown();
}
