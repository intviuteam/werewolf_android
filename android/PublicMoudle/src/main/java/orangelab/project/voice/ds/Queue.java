package orangelab.project.voice.ds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 26/10/2017.
 */

public class Queue<AnyType> {

    private List<AnyType> contents = new ArrayList<AnyType>();

    public boolean enQueue(AnyType element) {
        return contents.add(element);
    }

    public AnyType poll() {
        if (contents.size() > 0) {
            AnyType anyType = contents.get(0);
            contents.remove(anyType);
            return anyType;
        }
        return null;
    }

    public int size() {
        return contents.size();
    }

    public void clear() {
        contents.clear();
    }
}
