package orangelab.project.voice.privateroom.view

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.text.InputFilter
import android.text.TextUtils
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.RxToolKit
import com.R
import orangelab.project.common.event.ViewEvent
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.voice.popwindow.EmotionPopWindow
import orangelab.project.voice.privateroom.PrivateRoomObserver
import com.toolkit.action.Destroyable
import orangelab.project.common.effect.event.RoomEffectConsoleViewEvent
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.manager.SmallGameManager
import orangelab.project.voice.manager.VoiceEmotionManager
import org.greenrobot.eventbus.EventBus

/**
 *game_werewolf
 *2018/3/21 上午10:58
 *Mystery
 */
class PrivateRoomSpeakView : View.OnClickListener, View.OnTouchListener, Destroyable, ViewTreeObserver.OnGlobalLayoutListener {

    private var mContainer: View;
    private var mSpeakByMouseContainer: View;
    private var mSpeakByMouse: TextView
    private var mSpeakByHand: EditText
    private var mSendMessage: Button
    private var mSendGift: Button
    private var mSwitch: ImageView
    private var mMicController: ImageView
    private var mEmoticon: View
    private var mGameEmoticon: View

    private var mPrivateRoomObserver: PrivateRoomObserver? = null
//    private var mEmoticonWindow: EmotionPopWindow? = null

    private var isFreeStyle: Boolean = false
    private var isSpeaking: Boolean = false

    constructor(rootView: View, observer: PrivateRoomObserver) {

        mPrivateRoomObserver = observer

        mContainer = rootView.findViewById(R.id.speak_container)
        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(this)
        mContainer.setBackgroundColor(Color.parseColor("#0a2124"))

        mSpeakByMouseContainer = rootView.findViewById(R.id.speak_by_mouth)
        mSpeakByMouse = rootView.findViewById<TextView>(R.id.speak_by_mouth_text) as TextView
        mSpeakByHand = rootView.findViewById<EditText>(R.id.speak_by_hand) as EditText
        mSpeakByHand.setImeOptions(EditorInfo.IME_ACTION_SEND)
        mSpeakByHand.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(200)))

        mSendMessage = rootView.findViewById<Button>(R.id.send_message) as Button
        mSendGift = rootView.findViewById<Button>(R.id.gift_button) as Button
        mSwitch = rootView.findViewById<ImageView>(R.id.switch_speak_way) as ImageView
        mMicController = rootView.findViewById<ImageView>(R.id.iv_voice_mic) as ImageView
        mSendGift = rootView.findViewById<Button>(R.id.gift_button) as Button

        mSendGift.setBackgroundResource(R.drawable.bg_voice_gift_button)

        mEmoticon = rootView.findViewById(R.id.btn_send_emotion)
        mEmoticon.setBackgroundResource(R.drawable.drawable_private_room_emotion)
        mGameEmoticon = rootView.findViewById(R.id.small_game_emotion)


        mSpeakByMouseContainer.setOnTouchListener(this)
        mSwitch.setOnClickListener(this)
        mSendMessage.setOnClickListener(this)
        mSendGift.setOnClickListener(this)
        mMicController.setOnClickListener(this)
        mEmoticon.setOnClickListener(this)
        mGameEmoticon.setOnClickListener(this)


    }

    private fun onTouchDown() {
        mSpeakByMouse.setText(R.string.str_up_end)
        mSpeakByMouseContainer.setBackgroundResource(R.drawable.bg_voice_speak_view_press)
    }

    private fun onTouchUp() {
        mSpeakByMouse.setText(R.string.str_press_speak)
        mSpeakByMouseContainer.setBackgroundResource(R.drawable.bg_voice_speak_view)
    }

    private fun startSpeak() {
        onTouchDown()
        sendSpeakEvent(MotionEvent.ACTION_DOWN)
    }

    public fun endSpeak() {
        onTouchUp()
        sendSpeakEvent(MotionEvent.ACTION_UP)
    }

    private fun sendSpeakEvent(event: Int) {
        if (event == MotionEvent.ACTION_UP) {
            RxToolKit.Emit(ViewEvent.SpeakViewEvent(event))
            RoomSocketEngineHelper.endSpeak()
            return
        }
        if (event == MotionEvent.ACTION_DOWN) {
            RxToolKit.Emit(ViewEvent.SpeakViewEvent(event))
            RoomSocketEngineHelper.startSpeak()
            return
        }
    }

    private fun sendMessage(msg: String) {
        if (!TextUtils.isEmpty(msg)) {
            RoomSocketEngineHelper.sendChatMessage(msg)
            mSpeakByHand.setText("")
            hideKeyBoard(mSpeakByHand)
        }
    }

//    private fun showEmotionWindow() {
//        dismissEmotionWindow()
//        mEmoticonWindow = EmotionPopWindow(mContainer.context)
//        mEmoticonWindow?.setFocusable(true)
//        mEmoticonWindow?.setTouchable(true)
//        mEmoticonWindow?.setOnItemClickedListener({ emotion ->
//            mPrivateRoomObserver?.onSendEmotion(emotion)
//            mEmoticonWindow?.setSendTime(System.currentTimeMillis())
//            dismissEmotionWindow()
//        })
//        mEmoticonWindow?.showAtLocation(mContainer, Gravity.BOTTOM, 0, 0)
//    }

//    private fun dismissEmotionWindow() {
//        if (mEmoticonWindow != null) {
//            if (mEmoticonWindow!!.isShowing) {
//                mEmoticonWindow?.dismiss()
//                mEmoticonWindow = null
//            }
//        }
//    }

    private fun switchMode() {
        if (mSpeakByHand.visibility != View.VISIBLE) {
            if (mPrivateRoomObserver?.onSwitchToHand() ?: false) {
                turnIntoSpeakByHand()
            }
        } else {
            if (mPrivateRoomObserver?.onSwitchToMouse() ?: false) {
                turnIntoSpeakByMouse()
            }
        }
    }

    fun turnIntoSpeakByHand() {
        endSpeak()
        mSwitch.setBackgroundResource(R.mipmap.ic_voice_radio)

        mSpeakByMouse.visibility = View.GONE
        mSpeakByMouseContainer.visibility = (View.GONE)
        mSpeakByHand.visibility = (View.VISIBLE)

        mEmoticon.visibility = (View.VISIBLE)
        mSendGift.visibility = (View.VISIBLE)
        mSendMessage.visibility = (View.GONE)
    }

    fun turnIntoSpeakByMouse() {

        mSwitch.setBackgroundResource(R.drawable.drawable_private_room_switch_message)

        mSpeakByMouse.visibility = (View.VISIBLE)
        mSpeakByMouseContainer.visibility = (View.VISIBLE)
        mSpeakByHand.visibility = (View.GONE)
        mEmoticon.visibility = (View.GONE)
        mSendGift.visibility = (View.GONE)
        mSendMessage.visibility = (View.GONE)

    }

    fun turnIntoFreeStyle(isFreeStyle: Boolean) {
        this.isFreeStyle = isFreeStyle
        if (isFreeStyle) {
            this.isSpeaking = true
            turnIntoSpeakByHand()
            startSpeak()
            mSwitch.setBackgroundResource(R.mipmap.ico_voice_speak_mic)
        } else {
            this.isSpeaking = false
            turnIntoSpeakByMouse()
            endSpeak()
        }
    }

    fun turnIntoBanMic(isBanMic: Boolean) {
        if (isBanMic) {
            turnIntoSpeakByHand()
            mSwitch.setBackgroundResource(R.mipmap.ico_voice_ban_speak_mic)
        } else {
            turnIntoSpeakByMouse()
        }
        endSpeak()
    }

    private fun hideKeyboard() {
        val imm = mSpeakByHand.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mSpeakByHand.getWindowToken(), 0)
    }

    private fun showKeyboard() {
        val imm = mSpeakByHand.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(mSpeakByHand, InputMethodManager.SHOW_FORCED)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.switch_speak_way -> {
                if (isFreeStyle) {
                    /**
                     * 自由模式下 用于切换说话与不说话模式
                     */
                    if (isSpeaking) {
                        mSwitch.setBackgroundResource(R.mipmap.ico_voice_ban_speak_mic)
                        endSpeak()
                    } else {
                        mSwitch.setBackgroundResource(R.mipmap.ico_voice_speak_mic)
                        startSpeak()
                    }
                    isSpeaking = !isSpeaking
                } else {
                    switchMode()
                }
            }
            R.id.send_message -> {
                val message = mSpeakByHand.getText().toString().trim()
                sendMessage(message)
            }
            R.id.gift_button -> {
                mPrivateRoomObserver?.onShowGiftDialog()
            }
            R.id.btn_send_emotion -> {
                if (mPrivateRoomObserver?.onBeforeSendEmotion() ?: false) {
//                    showEmotionWindow()
                    EventBus.getDefault().post(RoomEffectConsoleViewEvent(VoiceConstants.SHOW_EMOTION))
                }
            }
            R.id.small_game_emotion -> {
                /**
                 * 2018-05-28改动，不再弹出送礼物对话框，而是弹出小游戏弹出框
                 */
                if (mPrivateRoomObserver?.onBeforeSendGameEmotion() ?: false) {
//                    mPrivateRoomObserver?.onShowGameEmotion(mSendGift)
                    EventBus.getDefault().post(RoomEffectConsoleViewEvent(VoiceConstants.SHOW_GAME))
                }
            }
        }

    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                if (mPrivateRoomObserver?.onSpeakTouchDown() ?: false) {
                    onTouchDown()
                    sendSpeakEvent(event!!.action)
                }
            }
            MotionEvent.ACTION_UP -> {
                if (mPrivateRoomObserver?.onSpeakTouchUp() ?: true) {
                    onTouchUp()
                    sendSpeakEvent(event!!.action)
                }
            }
        }
        return true
    }

    override fun onGlobalLayout() {
        val r = Rect()
        mContainer.getWindowVisibleDisplayFrame(r)
        val screenHeight = mContainer.getRootView().getHeight()
        val heightDifference = screenHeight - r.bottom
        if (heightDifference > 200) {
            /**
             * 键盘弹起 需要隐去送礼物与表情按钮
             */
            mSendGift.setVisibility(View.GONE)
            mEmoticon.setVisibility(View.GONE)
            mSendMessage.setVisibility(View.VISIBLE)
            mSpeakByHand.requestFocus()
        } else {
            /**
             * 键盘落下需要展示送礼物与表情按钮
             */
            mSendGift.setVisibility(View.VISIBLE)
            mEmoticon.setVisibility(View.VISIBLE)
            mSendMessage.setVisibility(View.GONE)
            mSpeakByHand.clearFocus()
        }
    }

    protected fun hideKeyBoard(view: View) {
        try {
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun destroy() {
//        dismissEmotionWindow()
        mContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this)
        mPrivateRoomObserver = null
        VoiceEmotionManager.getInstance().destroy()
        SmallGameManager.getInstance().destroy()
    }
}