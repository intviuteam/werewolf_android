package orangelab.project.voice.musiccompany.view

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.R
import com.toolkit.action.Keepable
import kotlinx.android.synthetic.main.layout_voice_music_search_bar.view.*
import orangelab.project.common.db.entity.MusicDataEntity

/**
 * 搜索条
 */
class SearchBar(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs), Keepable {

    init {
        init()
    }

    private var stockList: ArrayList<MusicDataEntity> = ArrayList() //搜索的数据仓库
    private var callback: (ArrayList<MusicDataEntity>) -> Unit = {} //搜索结果回调
    var onSearchClick: ((String) -> Unit)? = null             //搜索的点击事件

    fun changeStock(stockList: ArrayList<MusicDataEntity>) {
        this.stockList = stockList
    }

    fun setCallBack(callback: (ArrayList<MusicDataEntity>) -> Unit) {
        this.callback = callback
    }

    private fun init() {

        addView(View.inflate(context, R.layout.layout_voice_music_search_bar, null))

        /**
         * 实现热搜索
         */
        txt_search_info.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (onSearchClick == null) {
                    search(s.toString())
                }
            }
        })

        /**
         * 实现懒搜索
         */
        btn_search.setOnClickListener {
            if (onSearchClick != null) {
                onSearchClick?.invoke(txt_search_info.text.toString())
            } else {
                search(txt_search_info.text.toString())
            }
        }

    }

    /**
     * 根据输入信息输出搜索结果
     */
    fun search(info: String) {

        //是否和条件匹配
        fun match(musicData: MusicDataEntity): Boolean {
            return musicData.song_name.contains(info) || musicData.author_name.contains(info) || TextUtils.isEmpty(info)
        }

        val results: ArrayList<MusicDataEntity> = ArrayList()
        //逐个检索
        stockList.forEach {
            if (match(it)) {
                results.add(it)
            }
        }

        //检索完毕，执行回调
        callback.invoke(results)

    }
}