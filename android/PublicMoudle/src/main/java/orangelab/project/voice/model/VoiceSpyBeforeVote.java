package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lloydfinch on 05/12/2017.
 */

public class VoiceSpyBeforeVote implements Serializable, Keepable {

    public List<Integer> alives;//存活的人
    public String type;
    public int duration;//投票持续时间

    @Override
    public String toString() {
        return "VoiceSpyBeforeVote{" +
                "alives=" + alives +
                ", type='" + type + '\'' +
                ", duration=" + duration +
                '}';
    }
}