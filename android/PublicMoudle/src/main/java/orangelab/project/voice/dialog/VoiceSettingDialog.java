package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.voice.activity.VoiceRoomBackGroundActivity;
import orangelab.project.voice.activity.VoiceRoomBlackListActivity;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.musiccompany.MusicStockActivity;
import orangelab.project.voice.utils.PositionHelper;

import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 12/10/2017.
 */

public class VoiceSettingDialog extends SafeDialog implements Destroyable {

    private View llChangeTopic;
    private View llChangePassword;
    private View llSetBlackList;
    private View llApplyDeed;//申请房契
    private View llChangeDeed;//转让房契
    private View llBeMaster;//成为房主
    private View llChangeSeatNumber;//修改麦位数量
    private View llChangeBackground;//修改房间背景

    private View btnCancel;

    private Context mContext;
    private String topic;
    private String password;
    private String topicType;

    private VoiceChangeTitleDialog voiceChangeTitleDialog;
    private VoicePasswordDialog passwordDialog;

    public VoiceSettingDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceSettingDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_setting, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llChangeTopic = contentView.findViewById(R.id.ll_voice_change_topic);
        llChangePassword = contentView.findViewById(R.id.ll_voice_change_password);
        llSetBlackList = contentView.findViewById(R.id.ll_voice_black_list);
        llApplyDeed = contentView.findViewById(R.id.ll_voice_apply_deed);
        llChangeDeed = contentView.findViewById(R.id.ll_voice_change_deed);
        llBeMaster = contentView.findViewById(R.id.ll_voice_be_master);
        llChangeSeatNumber = contentView.findViewById(R.id.ll_voice_change_seat_number);
        llChangeBackground = contentView.findViewById(R.id.ll_voice_change_background);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        if (PositionHelper.isMaster()) {
            llBeMaster.setVisibility(View.GONE);
            llChangeTopic.setVisibility(View.VISIBLE);
            llChangePassword.setVisibility(View.VISIBLE);
            llSetBlackList.setVisibility(View.VISIBLE);
            if (VoiceRoomConfig.isIsSupportChangeSeat() && !VoiceRoomConfig.isIsKTV()) {
                llChangeSeatNumber.setVisibility(View.VISIBLE);
            } else {
                llChangeSeatNumber.setVisibility(View.GONE);
            }
            if (VoiceRoomConfig.isIsSupportChangeBg()) {
                llChangeBackground.setVisibility(View.VISIBLE);
            } else {
                llChangeBackground.setVisibility(View.GONE);
            }
        } else {
            llChangeTopic.setVisibility(View.GONE);
            llChangePassword.setVisibility(View.GONE);
            llSetBlackList.setVisibility(View.GONE);
            llChangeSeatNumber.setVisibility(View.GONE);
            llChangeBackground.setVisibility(View.GONE);
            if (VoiceRoomConfig.isIsSupportDeed()) {
                if (VoiceRoomConfig.isIsOwnerRoom() && VoiceRoomConfig.isIsPurchased()) {
                    llBeMaster.setVisibility(View.VISIBLE);
                } else {
                    llBeMaster.setVisibility(View.GONE);
                }
            } else {
                llBeMaster.setVisibility(View.GONE);
            }
        }

        if (VoiceRoomConfig.isIsSupportDeed()) {
            if (VoiceRoomConfig.isIsOwnerRoom()) {
                if (VoiceRoomConfig.isIsPurchased()) {
                    llApplyDeed.setVisibility(View.GONE);
                    llChangeDeed.setVisibility(View.VISIBLE);
                } else {
                    llApplyDeed.setVisibility(View.VISIBLE);
                    llChangeDeed.setVisibility(View.GONE);
                }
            } else {
                //owner_id不是自己，不可购买也不可转让
                llApplyDeed.setVisibility(View.GONE);
                llChangeDeed.setVisibility(View.GONE);
            }
        } else {
            llApplyDeed.setVisibility(View.GONE);
            llChangeDeed.setVisibility(View.GONE);
        }


        /**
         * 如果是游戏大厅
         * 隐藏房契，隐藏改麦位，隐藏改密码
         */
        if (VoiceRoomConfig.isLobby()) {
            llApplyDeed.setVisibility(View.GONE);
            llChangeSeatNumber.setVisibility(View.GONE);
            llChangePassword.setVisibility(View.GONE);
        }
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(380);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        llChangeTopic.setOnClickListener((v) -> {
            voiceChangeTitleDialog = new VoiceChangeTitleDialog(mContext, topic, topicType);
            voiceChangeTitleDialog.show();
            dismiss();
        });
        llChangePassword.setOnClickListener((v) -> {
            passwordDialog = new VoicePasswordDialog(mContext, password);
            passwordDialog.show();
            dismiss();
        });

        llSetBlackList.setOnClickListener((v) -> {
            VoiceRoomBlackListActivity.launch(mContext);
            dismiss();
        });

        llApplyDeed.setOnClickListener(v -> {
            VoiceDeedDialog voiceDeedDialog = new VoiceDeedDialog(mContext, VoiceDeedDialog.APPLY_DEED);
            voiceDeedDialog.show();
            dismiss();
        });

        llChangeDeed.setOnClickListener(v -> {
            VoiceDeedDialog voiceDeedDialog = new VoiceDeedDialog(mContext, VoiceDeedDialog.CHANGE_DEED);
            voiceDeedDialog.show();
            dismiss();
        });

        llBeMaster.setOnClickListener(v -> {
            VoiceSocketHelper.requestMaster();
            dismiss();
        });

        llChangeSeatNumber.setOnClickListener(v -> {

            if (VoiceRoomConfig.isIsSupportChangeSeat() && !VoiceRoomConfig.isIsKTV()) {
                if (canChangeSeatNumber()) {
                    VoiceChangeSeatDialog dialog = new VoiceChangeSeatDialog(mContext);
                    dialog.show();
                    dismiss();
                } else {
                    ToastToolKit.showShort(R.string.str_voice_tips_cannot_change_seat);
                }
            } else {
                ToastToolKit.showShort(R.string.str_voice_bg_no_permission);
            }
        });

        llChangeBackground.setOnClickListener(v -> {
            if (VoiceRoomConfig.isIsSupportChangeBg()) {
                if (canChangeBackground()) {
                    VoiceRoomBackGroundActivity.Companion.Launch(mContext, VoiceRoomConfig.getRoomId(), VoiceRoomConfig.getRoomType());
                    dismiss();
                }
            } else {
                ToastToolKit.showShort(R.string.str_voice_bg_no_permission);
            }
        });

        btnCancel.setOnClickListener(v -> dismiss());
    }

    public VoiceSettingDialog setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public VoiceSettingDialog setTopicType(String topicType) {
        this.topicType = topicType;
        return this;
    }

    public VoiceSettingDialog setPassword(String password) {
        this.password = password;
        return this;
    }

    private boolean canChangeSeatNumber() {
        //是房主并且房间只有自己，才可以设置房间座位数
        //支持更换房间类型，并且是房主并且房间只有自己，才可以设置房间座位数
        if (VoiceRoomConfig.isIsSupportChangeSeat() && PositionHelper.isMaster() && VoiceRoomConfig.getOnLineNumber() == 1) {
            return true;
        }
        return false;
    }

    private boolean canChangeBackground() {
        if (PositionHelper.isMaster()) {
            return true;
        }
        return false;
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {
        if (voiceChangeTitleDialog != null) {
            if (voiceChangeTitleDialog.isShowing()) {
                voiceChangeTitleDialog.dismiss();
            }
            voiceChangeTitleDialog.destroy();
            voiceChangeTitleDialog = null;
        }
        if (passwordDialog != null) {
            if (passwordDialog.isShowing()) {
                passwordDialog.dismiss();
            }
            passwordDialog = null;
        }
    }
}
