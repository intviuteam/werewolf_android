package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.R;
import orangelab.project.common.dialog.SafeDialog;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 12/10/2017.
 */

public class VoiceQuitDialog extends SafeDialog implements Destroyable {

    private View llInSmallWindow;
    private View llQuitRoom;
    private View btnCancel;

    private Context mContext;
    private OnInSmallWindowClickedListener onInSamllWindowClickedListener;
    private OnQuitRoomClickedListener onQuitRoomClickedListener;

    public VoiceQuitDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceQuitDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_quit_choose, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llInSmallWindow = contentView.findViewById(R.id.ll_voice_small_window);
        llQuitRoom = contentView.findViewById(R.id.ll_voice_quit_room);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(380);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        llInSmallWindow.setOnClickListener((v) -> {
            if (onInSamllWindowClickedListener != null) {
                onInSamllWindowClickedListener.onInSmallWindowClicked();
            }
            dismiss();
        });
        llQuitRoom.setOnClickListener((v) -> {
            if (onQuitRoomClickedListener != null) {
                onQuitRoomClickedListener.onQuitRoomClicked();
            }
            dismiss();
        });

        btnCancel.setOnClickListener(v -> dismiss());
    }

    public void setOnInSamllWindowClickedListener(OnInSmallWindowClickedListener onInSamllWindowClickedListener) {
        this.onInSamllWindowClickedListener = onInSamllWindowClickedListener;
    }

    public void setOnQuitRoomClickedListener(OnQuitRoomClickedListener onQuitRoomClickedListener) {
        this.onQuitRoomClickedListener = onQuitRoomClickedListener;
    }

    //点击小窗模式
    public interface OnInSmallWindowClickedListener {
        void onInSmallWindowClicked();
    }

    //点击退出房间
    public interface OnQuitRoomClickedListener {
        void onQuitRoomClicked();
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {

    }
}
