package orangelab.project.voice.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.transport.TransportHelper;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.RecycleViewDivider;
import orangelab.project.voice.adapter.OnLineUsersAdapter;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceBlackDialog;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.model.VoiceToOnlineBridgeBean;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;
import orangelab.project.voice.utils.PositionHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.intviu.support.NetworkHelpers;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;


public class VoiceOnlineUsersActivity extends SafeActivity implements View.OnClickListener {

    public static final String TAG = "VoiceOnlineUsersActivity";

    private View ivBack;
    private TextView tvTitle;
    private RecyclerView listOnLineUsers;

    private List<EnterRoomResult.EnterRoomUserItem> gameMembers = new ArrayList<>();
    private OnLineUsersAdapter adapter;

    private boolean isForceUpSeat;
    private VoiceToOnlineBridgeBean bean;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VoiceToOnlineBridgeBean temp = TransportHelper.getTransport(TAG);
        if (temp != null) {
            bean = temp;
        }
        if (bean == null) {
            finish();
            return;
        }

        initWindow();
        setContentView(R.layout.activity_on_line_users);
        initView();
        initData();
        initListener();
    }

    private void initWindow() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        Utils.setWindowStatusBarColor(this, R.color.vocie_status_bar);
    }

    private void initView() {

        ivBack = this.findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        tvTitle = (TextView) this.findViewById(R.id.tv_title);
        listOnLineUsers = (RecyclerView) this.findViewById(R.id.list_online_users);
        listOnLineUsers.setLayoutManager(new WrapContentLinearLayoutManager(this));
        listOnLineUsers.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL));
    }

    private void initData() {

        String title = bean.roomTitle;
        isForceUpSeat = bean.isForceUpseat;
        tvTitle.setText(title);
        adapter = new OnLineUsersAdapter(this, gameMembers);
        initOnItemClickListener();
        listOnLineUsers.setAdapter(adapter);
        pickDownSeatMembers(null);
    }

    //检索不在麦上的用户，构造"抱用户上麦列表"
    private void pickDownSeatMembers(List<EnterRoomResult.EnterRoomUserItem> newList) {
        gameMembers.clear();
        adapter.notifyDataSetChanged();
        List<EnterRoomResult.EnterRoomUserItem> onlineUsers = (newList == null)
                ? bean.onLineUsers
                : newList;

        //抱用户上麦，只显示不在麦上的用户
        if (isForceUpSeat) {
            adapter.setNeedShowBlack(false);
            for (EnterRoomResult.EnterRoomUserItem member : onlineUsers) {
                if (member.position > VoiceRoomConfig.getMaxChairNumber()) {
                    gameMembers.add(member);
                }
            }
        } else {
            //查看在线人数
            gameMembers.addAll(onlineUsers);
            adapter.setNeedShowBlack(true);
        }

        int position = bean.newPosition;
        Collections.sort(gameMembers);
        adapter.notifyDataSetChanged();

        //转让房主就过滤掉房主
        if (VoiceConstants.OVER_POSITION == position) {
            adapter.setNeedShowBlack(false);
            gameMembers.remove(0);
            adapter.notifyDataSetChanged();
        }
        adapter.setGameMembers(gameMembers);
        adapter.notifyDataSetChanged();
    }

    //在线人数变动就更新列表
    private void initListener() {

        initKickOutListener();

        //用来接收查看在线用户时，用户列表更新的事件
        RxToolKit.Build(this, VoiceEvent.OnLineUsersChangeEvent.class)
                .action(onLineUsersChangeEvent -> {
                    if (adapter != null) {
                        pickDownSeatMembers(onLineUsersChangeEvent.onLineMemberList);
                    }
                }).register();

        adapter.setOnAddToBlackClickListener(position -> {
            if (position >= 0 && position < gameMembers.size()) {
                EnterRoomResult.EnterRoomUserItem userItem = gameMembers.get(position);
                if (userItem != null) {
                    if (judgeSocketConnect()) {
                        if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
                            //"谁是卧底"正在进行中
                            if (PositionHelper.isUpSeat(userItem.position)) {
                                //麦上玩家不可踢出
                                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_kickout));
                            } else {
                                //GameHelper.addVoiceBlack(userItem.id);
                                if (TextUtils.equals(userItem.id, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
                                } else {
                                    new VoiceBlackDialog(this, userItem.id).show();
                                }
                            }
                        } else {
                            //GameHelper.addVoiceBlack(userItem.id);
                            if (TextUtils.equals(userItem.id, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
                            } else {
                                new VoiceBlackDialog(this, userItem.id).show();
                            }
                        }
                    }
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
                }
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
            }
        });
    }

    //处理查看列表时被踢出的情况，由VoiceRoomActivity在destroy时发出
    private void initKickOutListener() {
        RxToolKit.Build(this, FinishConversationEvent.class)
                .action(event -> runOnUiThreadSafely(() -> finish()))
                .register();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            onBackPressed();
        }
    }

    private void initOnItemClickListener() {
        adapter.setOnItemClickListener(position -> {
            if (position >= 0 && position < gameMembers.size()) {
                EnterRoomResult.EnterRoomUserItem member = gameMembers.get(position);
                if (member != null) {
                    if (isForceUpSeat) {
                        //抱用户上麦
                        int newPosition = bean.newPosition;
                        RxToolKit.Emit(new VoiceEvent.ResponseToNewChairEvent(newPosition, member));
                        finish();
                    } else {
                        //转让房主
                        int desPosition = bean.newPosition;
                        if (desPosition == VoiceConstants.OVER_POSITION) {
                            if (judgeSocketConnect()) {

//                                //ktv房间，被转让成为房主的人正在唱歌，发送stop_sing
//                                if (VoiceRoomConfig.isIsKTV()) {
//                                    if (member.isSing) {
//                                        MusicSocketHelper.INSTANCE.sendStopSing();
//                                    }
//                                }

                                //发送转让房主请求
                                RoomSocketEngineHelper.sendHandOverRoomOwner(member.position);
                                finish();
                            }
                        } else {
                            //查看个人信息
                            PersonalInfoActivity.Launch(this, member.id);
                        }
                    }
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
                }
            }
        });
    }

    private boolean judgeSocketConnect() {
        //网络断开或者socket没连上
        boolean isConnected = VoiceRoomGameSocketManager.getInstance().isConnected();
        if (!NetworkHelpers.isNetworkAvailable(MainApplication.getInstance()) || !isConnected) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
            return false;
        }
        return true;
    }

    private void destroy() {
        RxToolKit.UnRegister(this);
        gameMembers.clear();
        if (adapter != null) {
            adapter.destroy();
        }
    }

    @Override
    public void finish() {
        destroy();
        super.finish();
    }

    public static void Launch(Context context, VoiceToOnlineBridgeBean bean) {
        Intent intent = new Intent(context, VoiceOnlineUsersActivity.class);
        TransportHelper.putTransportable(TAG, bean);
        context.startActivity(intent);
    }
}
