package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;

/**
 * Created by lloydfinch on 14/10/2017.
 */

public class VoiceFriendDialog extends SafeDialog {

    public static final String TAG = "VoiceFriendDialog";

    private View btnSure;
    private View btnCancel;
    private View btn_close;
    private UserHeadView mHeadView;
    private TextView userName;
    private TextView userLevel;
    private TextView userPopularity;
    private View llUserInfoContainer;

    private Context mContext;
    private String mUserId;

    public VoiceFriendDialog(@NonNull Context context, String userId) {
        this(context, R.style.DarkDialog, userId);
    }

    public VoiceFriendDialog(@NonNull Context context, @StyleRes int themeResId, String userId) {
        super(context, themeResId);
        this.mUserId = userId;
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
        initData();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_receive_friend, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llUserInfoContainer = contentView.findViewById(R.id.rl_user_detail_container);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        mHeadView = (UserHeadView) findViewById(R.id.user_head_view);
        userName = (TextView) findViewById(R.id.voice_user_name);
        userLevel = (TextView) findViewById(R.id.voice_user_level);
        userPopularity = (TextView) findViewById(R.id.voice_user_popularity);
        btn_close = findViewById(R.id.ibtn_close);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        params.gravity = Gravity.BOTTOM;
        params.y = ScreenUtils.dip2px(80);
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> accept());
        btnCancel.setOnClickListener(v -> refuse());
        llUserInfoContainer.setOnClickListener(v -> {
            toPersonalInfoActivity();
        });
        btn_close.setOnClickListener(v -> dismiss());
    }

    private void toPersonalInfoActivity() {
        if (Utils.checkUserIdIsTourist(mUserId)) {
            ToastToolKit.showShort(R.string.str_login_info);
            return;
        }
        PersonalInfoActivity.Launch(mContext, mUserId);
    }


    private void getUserInfo(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        try {
            ApiManager.GetUserInfoApi(userId, (userInfoResult, e)
                    -> runSafely(() -> updateData(userInfoResult)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        getUserInfo(mUserId);
    }

    private void updateData(UserInfoResult result) {
        if (result != null) {
            mHeadView.setUserSex(result.sex);
            mHeadView.setUserHeadImageUrl(result.image);
            userName.setText(result.name);
            userLevel.setText(
                    MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level))
            );
            userPopularity.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));
        }
    }

    private void accept() {
        ApiManager.AcceptFriend(mUserId, (s, e) -> {
            runSafely(() -> {
                if (e == null) {
                    UnifiedBridgeHelper.SendAddFriendAction(mUserId);
                }
                ToastToolKit.showShort(s);
                dismiss();
            });
        });
    }

    private void refuse() {
        ApiManager.RefuseFriend(mUserId, (s, e) -> {
            runSafely(() -> {
                ToastToolKit.showShort(s);
                dismiss();
            });
        });
    }


    @Override
    public void show() {
        super.show();
        VoiceRoomDataSourceManager.getInstance().markAddFriendHandled(mUserId);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    protected void release() {
        mContext = null;
    }
}
