package orangelab.project.voice.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;

import cn.intviu.support.GsonHelper;
import de.hdodenhof.circleimageview.CircleImageView;

import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.manager.VoiceRoomCommander;
import orangelab.project.voice.model.VoiceRedPacketGrabBean;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

/**
 * Created by hwx on 12/07/2018.
 */

public class RedpacketActivity extends SafeActivity {

    private static final String TAG = "RedpacketActivity";

    private ImageView mPacketImage;
    private ImageView mPacketImageBg;

    private View mPacketTitle;

    private ConstraintLayout mOpenSuccessBackground;
    private View mOpenFailedBackground;
    private View mShare;

    private TextView mDiamondNum;
    private TextView mSenderName;
    private CircleImageView mSenderHead;

    private AnimationDrawable mDrawablePacketNotOpen;
    private AnimationDrawable mDrawablePacketOpen;
    private AnimationDrawable mDrawablePacketOpenSuccess;
    private AnimationDrawable mDrawablePacketOpenFailed;
    private AnimationDrawable mDrawablePacketOpenSuccessBg;

    private Runnable mFinish = this::finish;
    private static final int SELF_FINISH_DURATION = 10000;
    private static Boolean isOpened = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initNotOpenListener();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (isOpened) {
            isOpened = false;
            mPacketTitle.setVisibility(View.GONE);
            mOpenSuccessBackground.setVisibility(View.GONE);
            mOpenFailedBackground.setVisibility(View.GONE);
            cancelOpenFailAnimation();
            cancelOpenSuccessAnimation();
            startNotOpenAnimation();
            initNotOpenListener();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(true);
    }

    private void initView() {
        isOpened = false;

        setContentView(R.layout.activity_red_packet);
        mPacketImage = (ImageView) findViewById(R.id.red_packet_image);
        mPacketImageBg = (ImageView) findViewById(R.id.red_packet_image_bg);
        mPacketTitle = findViewById(R.id.red_packet_open_title);
        mOpenSuccessBackground = (ConstraintLayout) findViewById(R.id.red_packet_open_success);
        mOpenFailedBackground = findViewById(R.id.red_packet_open_failed);

        mShare = findViewById(R.id.share);

        mDiamondNum = findViewById(R.id.diamond_value);
        mSenderName = findViewById(R.id.sender_name);
        mSenderHead = (CircleImageView) findViewById(R.id.head_image);

        startNotOpenAnimation();
        initVoiceRoomExitListener();
    }

    private void startNotOpenAnimation() {
        mOpenFailedBackground.setVisibility(View.GONE);
        mDrawablePacketNotOpen = (AnimationDrawable) getResources().getDrawable(R.drawable.drawable_redpakcet_not_open);
        mPacketImage.setImageDrawable(mDrawablePacketNotOpen);
        mDrawablePacketNotOpen.start();
        runOnUiThreadSafely(mFinish, SELF_FINISH_DURATION);
    }

    private void cancelNotOpenAnimation() {
        if (mDrawablePacketNotOpen != null && mDrawablePacketNotOpen.isRunning()) {
            mDrawablePacketNotOpen.stop();
        }
    }

    private void openPacketSuccess(String result) {
        mOpenFailedBackground.setVisibility(View.GONE);
        VoiceRedPacketGrabBean grabResult =
                GsonHelper.getGson().fromJson(result, VoiceRedPacketGrabBean.class);
        PLog.d(TAG, "diamondNum = " + grabResult.ammount);
        mDiamondNum.setText(String.format(getResources().getString(R.string.str_red_packet_diamond), grabResult.ammount));
        mSenderName.setText(grabResult.sender.name);
        PicassoUtils.loadImage(this, grabResult.sender.avatar, mSenderHead, R.mipmap.default_head);

        cancelNotOpenAnimation();
        mDrawablePacketNotOpen = null;
        startOpenSuccessAnimation();
    }

    private void openPacketFail() {
        mPacketTitle.setVisibility(View.VISIBLE);
        mOpenSuccessBackground.setVisibility(View.GONE);
        mOpenFailedBackground.setVisibility(View.VISIBLE);

        cancelNotOpenAnimation();
        mDrawablePacketNotOpen = null;
        startOpenFailAnimation();
    }

    private void startOpenSuccessAnimation() {
        cancelOpenFailAnimation();
        mDrawablePacketOpen = (AnimationDrawable) getResources().getDrawable(R.drawable.drawable_redpakcet_open);
        mDrawablePacketOpenSuccess = (AnimationDrawable) getResources().getDrawable(R.drawable.drawable_redpakcet_open_success);
        mDrawablePacketOpenSuccessBg = (AnimationDrawable) getResources().getDrawable(R.drawable.drawable_redpakcet_open_success_bg);

        mPacketImage.setImageDrawable(mDrawablePacketOpen);
        mDrawablePacketOpen.start();

        runOnUiThreadSafely(() -> {
            mPacketTitle.setVisibility(View.VISIBLE);
            mOpenSuccessBackground.setVisibility(View.VISIBLE);
            mDrawablePacketOpenSuccess.start();
            mPacketImageBg.setImageDrawable(mDrawablePacketOpenSuccessBg);
            mDrawablePacketOpenSuccessBg.start();
            mPacketImage.setImageDrawable(mDrawablePacketOpenSuccess);
            initOpenListener(true);
        }, getAnimationDuration(mDrawablePacketOpen));

        removeCallBackSafely(mFinish);
        //重新计时
        runOnUiThreadSafely(mFinish, SELF_FINISH_DURATION);
    }

    private void cancelOpenSuccessAnimation() {
        if (mDrawablePacketOpen != null && mDrawablePacketOpen.isRunning()) {
            mDrawablePacketOpen.stop();
        }
        if (mDrawablePacketOpenSuccess != null && mDrawablePacketOpenSuccess.isRunning()) {
            mDrawablePacketOpenSuccess.stop();
        }
        if (mDrawablePacketOpenSuccessBg != null && mDrawablePacketOpenSuccessBg.isRunning()) {
            mDrawablePacketOpenSuccessBg.stop();
        }
    }

    private void startOpenFailAnimation() {
        cancelOpenSuccessAnimation();
        mDrawablePacketOpenFailed = (AnimationDrawable) getResources().getDrawable(R.drawable.drawable_redpakcet_open_success);
        mPacketImage.setImageDrawable(mDrawablePacketOpenFailed);
        mDrawablePacketOpenFailed.start();
        runOnUiThreadSafely(() -> {
            initOpenListener(false);
        }, getAnimationDuration(mDrawablePacketOpenFailed));

        removeCallBackSafely(mFinish);
        //重新计时
        runOnUiThreadSafely(mFinish, SELF_FINISH_DURATION);
    }

    private void cancelOpenFailAnimation() {
        if (mDrawablePacketOpenFailed != null && mDrawablePacketOpenFailed.isRunning()) {
            mDrawablePacketOpenFailed.stop();
        }
    }

    private void initNotOpenListener() {
        mPacketImage.setOnClickListener(v -> {
            //防止连续点击
            mPacketImage.setOnClickListener(null);
            String lastPacketId = VoiceRoomConfig.getLastRedpacketId();
            if (Utils.judgeConnectStatusWithToast(MessageUtils.getString(R.string.network_is_unavailable))) {
                VoiceSocketHelper.grabPackage(lastPacketId, result -> {
                    runOnUiThreadSafely(() -> {
                        openPacketSuccess(result);
                    });
                }, result -> {
                    runOnUiThreadSafely(this::openPacketFail);
                });
            }
        });
    }

    private void initOpenListener(boolean success) {
        isOpened = true;
        mPacketImage.setOnClickListener(v -> finish());
        if (success) {
            mShare.setOnClickListener(v -> {
                ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_FROM_CUSTOM);
                data.fromType = Constant.GAME_TYPE_AUDIO;
                data.title = getResources().getString(R.string.str_red_packet_share_text_title);
                data.message = getResources().getString(R.string.str_red_packet_share_text);

                StringBuilder sb = new StringBuilder(ProjectConfig.APP_API)
                        .append("/invite_packet?id=")
                        .append(VoiceRoomConfig.getRoomId())
                        .append("&inviter=")
                        .append(GlobalUserState.INSTANCE.getUserUid());
                if (!VoiceRoomConfig.getPsw().equals("")) {
                    sb.append("&password=").append(VoiceRoomConfig.getPsw());
                }
                data.url = sb.toString();
                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onVoiceRoomShare(this, data);
            });
        }
    }

    private void releaseAllAnimation() {
        cancelNotOpenAnimation();
        cancelOpenFailAnimation();
        cancelOpenSuccessAnimation();
        mDrawablePacketNotOpen = null;
        mDrawablePacketOpen = null;
        mDrawablePacketOpenSuccess = null;
        mDrawablePacketOpenFailed = null;
        mDrawablePacketOpenSuccessBg = null;
    }

    private int getAnimationDuration(AnimationDrawable animationDrawable) {
        int duration = 0;
        if (animationDrawable != null) {
            for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
                duration += animationDrawable.getDuration(i);
            }
        }
        return duration;
    }

    //异常退出房间后finish
    private void initVoiceRoomExitListener() {
        RxToolKit.Build(this, FinishConversationEvent.class)
                .action(event -> runOnUiThreadSafely(this::finish))
                .register();
    }

    @Override
    protected void onActivityWindowInitFinish() {
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseAllAnimation();
        RxToolKit.UnRegister(this);
        isOpened = false;
        //如果红包history显示中，则刷新dialog列表
        EventBus.getDefault().post(new VoiceEvent.RedPacketHistoryListEvent("", false, true));
    }

    public static void Launch(Context context) {
        Intent intent = new Intent(context, RedpacketActivity.class);
        context.startActivity(intent);
    }
}
