package orangelab.project.voice.component;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.effect.event.RoomEffectConsoleViewEvent;
import orangelab.project.common.model.PersonalData;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.utils.PositionHelper;

import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;

import orangelab.project.voice.dialog.VoiceChangeTitleDialog;
import orangelab.project.voice.popwindow.SmallGamePopWindow;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.voice.gif.BasicComponent;
import orangelab.project.voice.basic.ShareIsFirstComeIn;
import orangelab.project.voice.dialog.VoiceGameApplauseDialog;
import orangelab.project.voice.dialog.VoiceGameScoreDialog;
import orangelab.project.voice.view.PraiseView;


/**
 * Created by lloydfinch on 25/09/2017.
 */

public class VoiceMainViewUI extends BasicComponent implements View.OnClickListener, Destroyable {

    private ViewGroup contentView;

    private View flChangeTitleContainer;
    private View llInputRoomTopic;
    private View llInputRoomTopicActive;
    private EditText txtTopicContent;
    private TextView tvTopicLength;
    private Button btnSaveTopic;
    private ListView listMsg;
    private View viewComeInTag;
    private PraiseView praiseView;
    private View passView;
    private View redPacket;
//    private SmallGamePopWindow gamePopWindow;
//    private VoiceGameScoreDialog scoreDialog;
//    private VoiceGameApplauseDialog pointsDialog;
    private SmallGamePopWindow gamePopWindow;
    private VoiceGameScoreDialog scoreDialog;
    private VoiceGameApplauseDialog pointsDialog;
    private View llFirstRow; //第一排麦位
    private View llSecondRow; //第二排麦位
    private View llThirdRow;//第三排座位
    private View llFourRow;//第四排座位

    private Context mContext;
    private View.OnClickListener onChangeTitleClickedListener;
    private View.OnClickListener onSaveTitleClickedListener;


    public VoiceMainViewUI(Context mContext, ViewGroup contentView) {
        this.mContext = mContext;
        this.contentView = contentView;
        init();
    }

    private void init() {
        initView();
        initListener();
    }

    private void initView() {
        flChangeTitleContainer = contentView.findViewById(R.id.fl_change_title_container);
        llInputRoomTopic = contentView.findViewById(R.id.ll_input_room_topic);
        llInputRoomTopicActive = contentView.findViewById(R.id.ll_input_room_topic_active);
        txtTopicContent = (EditText) contentView.findViewById(R.id.txt_topic_content);
        tvTopicLength = (TextView) contentView.findViewById(R.id.tv_topic_length);
        btnSaveTopic = (Button) contentView.findViewById(R.id.btn_save_topic);
        listMsg = (ListView) contentView.findViewById(R.id.list_message);
        praiseView = (PraiseView) contentView.findViewById(R.id.praise_view);
        passView = contentView.findViewById(R.id.pass_view);
        redPacket = contentView.findViewById(R.id.red_packet);
        listMsg.addHeaderView(LayoutInflater.from(mContext).inflate(R.layout.layout_voice_warn, null));
        llFirstRow = contentView.findViewById(R.id.ll_first_row);
        llSecondRow = contentView.findViewById(R.id.ll_second_row);
        llThirdRow = contentView.findViewById(R.id.ll_third_row);
        llFourRow = contentView.findViewById(R.id.ll_four_row);
        updateChairNumber();
        viewComeInTag = contentView.findViewById(R.id.view_tag_come_in);
        ShareIsFirstComeIn.build(mContext);
        onResume();
    }

    public void updateChairNumber() {

        //4排麦位
        if (VoiceRoomConfig.getMaxChairNumber() == VoiceConstants.MAX_CHAIR_16) {
            llFirstRow.setVisibility(View.VISIBLE);
            llSecondRow.setVisibility(View.VISIBLE);
            llThirdRow.setVisibility(View.VISIBLE);
            llFourRow.setVisibility(View.VISIBLE);
        }
        //3排麦位
        if (VoiceRoomConfig.getMaxChairNumber() == VoiceConstants.MAX_CHAIR_12) {
            llFirstRow.setVisibility(View.VISIBLE);
            llSecondRow.setVisibility(View.VISIBLE);
            llThirdRow.setVisibility(View.VISIBLE);
            llFourRow.setVisibility(View.GONE);
        }
        //2排麦位
        if (VoiceRoomConfig.getMaxChairNumber() == VoiceConstants.MAX_CHAIR_8) {
            llFirstRow.setVisibility(View.VISIBLE);
            llSecondRow.setVisibility(View.VISIBLE);
            llThirdRow.setVisibility(View.GONE);
            llFourRow.setVisibility(View.GONE);
        }
        //1排麦位
        if (VoiceRoomConfig.getMaxChairNumber() == VoiceConstants.MAX_CHAIR_4) {
            llFirstRow.setVisibility(View.VISIBLE);
            llSecondRow.setVisibility(View.GONE);
            llThirdRow.setVisibility(View.GONE);
            llFourRow.setVisibility(View.GONE);
        }
        //只有房主
        if (VoiceRoomConfig.getMaxChairNumber() == VoiceConstants.MAX_CHAIR_0) {
            llFirstRow.setVisibility(View.GONE);
            llSecondRow.setVisibility(View.GONE);
            llThirdRow.setVisibility(View.GONE);
            llFourRow.setVisibility(View.GONE);
        }
    }

    public void initLikeCount(int likeCount) {
        praiseView.initPraiseCount(likeCount);
    }

    private void initListener() {
        llInputRoomTopic.setOnClickListener(this);
        btnSaveTopic.setOnClickListener(this);
        txtTopicContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String content = s.toString().trim();
                if (TextUtils.isEmpty(content)) {
                    tvTopicLength.setText("0/15");
                }
                tvTopicLength.setText(content.length() + "/15");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passView.setOnClickListener(v -> EventBus.getDefault().post(new RoomEffectConsoleViewEvent(VoiceConstants.SHOW_GAME)));
    }

//    private void showGameList() {
//        destroyWindow();
//        gamePopWindow = new SmallGamePopWindow(mContext);
//        gamePopWindow.setFocusable(true);
//        gamePopWindow.setTouchable(true);
//        gamePopWindow.setOnItemClickListener((smallGame) -> {
//            if (smallGame == null) {
//                return;
//            }
//            String mark = smallGame.getMark();
//            PersonalData personalData = PersonalDataHelper.getPersonalData();
//            if (personalData == null) {
//                return;
//            }
//            int position = personalData.getSelfPosition();
//            if (TextUtils.equals(VoiceConstants.POINTS, smallGame.getMark())) {
//                destroyDialog();
//                scoreDialog = new VoiceGameScoreDialog(mContext);
//                scoreDialog.setOnOkClickedListener((score) -> {
//                    RoomSocketEngineHelper.sendGameEmotion(mark, position, score);
//                });
//                scoreDialog.show();
//            } else if (TextUtils.equals(VoiceConstants.APPLAUSE, smallGame.getMark())) {
//                destroyDialog();
//                pointsDialog = new VoiceGameApplauseDialog(mContext);
//                pointsDialog.setOnOkClickedListener(() -> {
//                    RoomSocketEngineHelper.sendGameEmotion(mark, position, VoiceConstants.NO_RES);
//                });
//                pointsDialog.show();
//            } else {
//                RoomSocketEngineHelper.sendGameEmotion(mark, position, VoiceConstants.NO_RES);
//            }
//            gamePopWindow.setStartTime(System.currentTimeMillis());
//            gamePopWindow.dismiss();
//        });
//        gamePopWindow.showAtLocation(contentView, Gravity.BOTTOM, 0, 0);
//    }

//    private void destroyWindow() {
//        if (gamePopWindow != null) {
//            if (gamePopWindow.isShowing()) {
//                gamePopWindow.dismiss();
//            }
//            gamePopWindow = null;
//        }
//    }

    private void destroyDialog() {
//        if (scoreDialog != null) {
//            if (scoreDialog.isShowing()) {
//                scoreDialog.dismiss();
//            }
//            scoreDialog.destroy();
//            scoreDialog = null;
//        }
//        if (pointsDialog != null) {
//            if (pointsDialog.isShowing()) {
//                pointsDialog.dismiss();
//            }
//            pointsDialog.destroy();
//            pointsDialog = null;
//        }

        if (changeTitleDialog != null) {
            if (changeTitleDialog.isShowing()) {
                changeTitleDialog.dismiss();
            }
            changeTitleDialog.destroy();
            changeTitleDialog = null;
        }
    }

    public void onResume() {
        if (ShareIsFirstComeIn.hadComeIn()) {
            viewComeInTag.setVisibility(View.GONE);
        } else {
            viewComeInTag.setVisibility(View.VISIBLE);
            ShareIsFirstComeIn.recordComeIn();
        }
    }

    private void showSmallGame() {
        if (PositionHelper.isUpSeat()) {
            passView.setVisibility(View.VISIBLE);
        } else {
            passView.setVisibility(View.GONE);
        }
    }

    public void showMasterUI() {
        if (PositionHelper.isMaster()) {
            showChangeTitle();
        } else {
            hideChangeTitle();
        }
        showSmallGame();

        //如果是ktv房间，直接隐藏
        if (VoiceRoomConfig.isIsKTV()) {
            hideChangeTitle();
        }
    }

    public void showChangeTitle() {
        flChangeTitleContainer.setVisibility(View.VISIBLE);
    }

    public void hideChangeTitle() {
        flChangeTitleContainer.setVisibility(View.GONE);
    }

    private VoiceChangeTitleDialog changeTitleDialog;

    public void showChangeTitleUI(String title, String type) {

        changeTitleDialog = new VoiceChangeTitleDialog(mContext, title, type);
        changeTitleDialog.show();
    }

    public void hideChangeTitleUI() {
        hideKeyboard();
        llInputRoomTopicActive.setVisibility(View.GONE);
        llInputRoomTopic.setVisibility(View.VISIBLE);
    }

    public void setOnChangeTitleClickedListener(View.OnClickListener onChangeTitleClickedListener) {
        this.onChangeTitleClickedListener = onChangeTitleClickedListener;
    }

    public void setOnSaveTitleClickedListener(View.OnClickListener onSaveTitleClickedListener) {
        this.onSaveTitleClickedListener = onSaveTitleClickedListener;
    }

    private void hideKeyboard() {
        if (txtTopicContent != null && txtTopicContent.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) txtTopicContent
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(txtTopicContent.getWindowToken(), 0);
        }
    }

    private void showKeyboard() {
        if (txtTopicContent != null && txtTopicContent.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) txtTopicContent
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtTopicContent, InputMethodManager.SHOW_FORCED);
        }
    }

    public ViewGroup findChairUIById(int id) {
        return (ViewGroup) contentView.findViewById(id);
    }

    public ListView getListMsg() {
        return listMsg;
    }

    public View getBtnRedPacket() {
        return redPacket;
    }

    public void likeRoom(int number) {
        praiseView.receivePraise(number);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ll_input_room_topic) {
            onChangeTitleClickedListener.onClick(llInputRoomTopic);
        } else if (id == R.id.btn_save_topic) {
            onSaveTitleClickedListener.onClick(txtTopicContent);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        praiseView.destroy();
//        destroyWindow();
        destroyDialog();
    }
}
