package orangelab.project.voice.utils;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.voice.config.VoiceRoomConfig;
import com.networktoolkit.transport.Constant;

import java.util.Comparator;

/**
 * Created by lloydfinch on 19/12/2017.
 * //语音房黑名单比较器
 * 1 不在麦上
 * 2 在麦上
 * 3 房主
 * 4 条件相同按照进入时间从上到下
 */

public class VoiceBlackListComparator implements Comparator<EnterRoomResult.EnterRoomUserItem> {

    @Override
    public int compare(EnterRoomResult.EnterRoomUserItem o1, EnterRoomResult.EnterRoomUserItem o2) {

        //比较顺序：不在麦上的最靠前，然后在麦上的，然后房主，然后按进入时间，进入早的在前

        if (o1.position == Constant.MASTER_POSITION) {
            return 1;
        }

        if (o2.position == Constant.MASTER_POSITION) {
            return -1;
        }

        if (o1.position <= VoiceRoomConfig.getMaxChairNumber() && o2.position > VoiceRoomConfig.getMaxChairNumber()) {
            return 1;
        }
        if (o1.position > VoiceRoomConfig.getMaxChairNumber() && o2.position <= VoiceRoomConfig.getMaxChairNumber()) {
            return -1;
        }

        return o1.enter_time.compareToIgnoreCase(o2.enter_time);
    }
}
