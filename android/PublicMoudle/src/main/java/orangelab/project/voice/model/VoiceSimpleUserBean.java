package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 06/12/2017.
 */

public class VoiceSimpleUserBean implements Serializable, Keepable {

    public String userId = "";
    public Integer uid = 0;
    public String userName = "";
    public String headUrl = "";
    public int position;

    public VoiceSimpleUserBean() {
    }

    public VoiceSimpleUserBean(String userId, String userName, String headUrl, int position) {
        this.userId = userId;
        this.userName = userName;
        this.headUrl = headUrl;
        this.position = position;
    }

    public VoiceSimpleUserBean(String userId, Integer uid, String userName, String headUrl, int position) {
        this.userId = userId;
        this.uid = uid;
        this.userName = userName;
        this.headUrl = headUrl;
        this.position = position;
    }
}
