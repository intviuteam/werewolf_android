package orangelab.project.voice.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.model.VoiceDeedShareBean;
import orangelab.project.voice.share.ShareToolKit;

import com.networktoolkit.transport.Constant;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Destroyable;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.greenrobot.eventbus.EventBus;

import cn.intviu.support.ShareTools;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by lloydfinch on 30/01/2018.
 * //此dialog跟房间无关，只支持分享跟房间无关的内容(目前房契的分享使用)
 */

public class VoiceDeedShareDialog extends SafeDialog implements Destroyable {

    private View llShareToWechat;
    private View llShareToMoments;
    private View llShareToQQ;
    private View llShareToFaceBook;
    private View llShareToLine;

    private View btnCancel;

    private RxPermissions permissions;
    private Context mContext;
    private VoiceDeedShareBean bean;

    public VoiceDeedShareDialog(@NonNull Context context, VoiceDeedShareBean bean) {
        this(context, R.style.radius_dialog);
        this.bean = bean;
    }

    public VoiceDeedShareDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
    }

    @Override
    protected void release() {

    }

    private void init() {
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        initView();
        initWindow();
        initData();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_deed_share, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        llShareToWechat = contentView.findViewById(R.id.ll_share_to_wechat);
        llShareToMoments = contentView.findViewById(R.id.ll_share_to_moments);
        llShareToQQ = contentView.findViewById(R.id.ll_share_to_qq);
        llShareToFaceBook = contentView.findViewById(R.id.ll_share_to_facebook);
        llShareToLine = contentView.findViewById(R.id.ll_share_to_line);

        btnCancel = contentView.findViewById(R.id.btn_cancel);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initData() {
        if (mContext instanceof Activity) {
            this.permissions = new RxPermissions((Activity) mContext);
        } else {
            ToastToolKit.showShort(R.string.permission_read_store_error);
        }
    }

    private void initListener() {

        llShareToWechat.setOnClickListener(v -> shareToWechat());
        llShareToMoments.setOnClickListener(v -> shareToMoments());
        llShareToQQ.setOnClickListener(v -> shareToQQ());
        llShareToFaceBook.setOnClickListener(v -> shareToFaceBook());
        llShareToLine.setOnClickListener(v -> shareToLine());
        if (ProjectConfig.isKuaiWan()) {
            llShareToFaceBook.setVisibility(View.GONE);
            llShareToLine.setVisibility(View.GONE);
        }

        btnCancel.setOnClickListener(v -> {
            dismiss();
            RxToolKit.Emit(new VoiceEvent.VoiceShareActivityFinishEvent());
            EventBus.getDefault().post(new VoiceEvent.VoiceShareActivityFinishEvent());
        });
    }

    private void shareToWechat() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.WEIXIN));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToMoments() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.WEIXIN_CIRCLE));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToQQ() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_QQ)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                doShare(SHARE_MEDIA.QQ);
                                orangelab.project.common.utils.ShareTools.shareSuccessReported(Constant.SHARE_TO_QQ);
                            } else {
                                ToastToolKit.showShort(R.string.permission_read_store_error);
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToFaceBook() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_FACEBOOK)) {
            ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.FACEBOOK));
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void shareToLine() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
        if (ShareTools.existsApp(mContext, ShareTools.SHARE_LINE)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                ThreadToolKit.Sync(() -> doShare(SHARE_MEDIA.LINE));
                            } else {
                                ToastToolKit.showShort(R.string.permission_read_store_error);
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(R.string.share_error);
        }
    }

    private void doShare(SHARE_MEDIA share_media) {
        if (mContext instanceof Activity) {
            ShareToolKit.create().shareVoiceDeedImage((Activity) mContext, bean, share_media);
        } else {
            PLog.e("VoiceDeedShareDialog", "share need a Activity not a Context!!!");
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void destroy() {

    }
}
