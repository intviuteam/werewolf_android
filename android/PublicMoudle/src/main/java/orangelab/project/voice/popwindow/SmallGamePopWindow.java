package orangelab.project.voice.popwindow;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.R;
import com.toolkit.action.Destroyable;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.effect.view.RoomEffectsContext;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceGameApplauseDialog;
import orangelab.project.voice.dialog.VoiceGameScoreDialog;
import orangelab.project.voice.view.SmallGameViewPagerGroup;

/**
 * Created by lloydfinch on 30/10/2017.
 * Modified by hwx on 1/8/2018
 */

public class SmallGamePopWindow extends PopupWindow implements Destroyable {


    private SmallGameViewPagerGroup mSmallGameViewPagerGroup;
    private VoiceGameScoreDialog scoreDialog;
    private VoiceGameApplauseDialog pointsDialog;

    private RoomEffectsContext mRoomEffectsContext;
    private View hideView;
    private int ITEMNUM_MAX = 12;
    private int ITEMNUM_COLUME = 6;

    public SmallGamePopWindow(RoomEffectsContext context) {
        super(context.getContext());
        this.mRoomEffectsContext = context;
        init();
    }

    private void init() {
        initView();
        initRequest();
        initListener();
    }

    private void initView() {
        View contentView = View.inflate(mRoomEffectsContext.getContext(), R.layout.layout_small_game, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setAnimationStyle(R.style.voice_emotion_pop);

        mSmallGameViewPagerGroup = (SmallGameViewPagerGroup) contentView.findViewById(R.id.rv_voice_small_game);
        mSmallGameViewPagerGroup.initResource(R.layout.emotion_viewpage_item, ITEMNUM_MAX, ITEMNUM_COLUME,
                R.mipmap.gift_index_selected, R.mipmap.gift_index_normal);

        hideView = contentView.findViewById(R.id.view_voice_hide);
    }

    private void initRequest() {
        mSmallGameViewPagerGroup.setData(mRoomEffectsContext.getSmallGameList());
    }


    private void initListener() {
        mSmallGameViewPagerGroup.setOnItemClickedListener((smallGame) -> {
            if (smallGame == null) {
                return;
            }

            if (mRoomEffectsContext.isSelfIsAvailablePosition() && mRoomEffectsContext.getSelfPosition() != -1) {

                final int position = mRoomEffectsContext.getSelfPosition();

                if (TextUtils.equals(VoiceConstants.POINTS, smallGame.type)) {

                    destroyDialog();
                    scoreDialog = new VoiceGameScoreDialog(mRoomEffectsContext.getContext());
                    scoreDialog.setOnOkClickedListener((score) -> {
                        //TODO EventBus
                        RoomSocketEngineHelper.sendGameEmotion(smallGame.type, position, score);
                        dismiss();

                    });
                    scoreDialog.show();

                } else if (TextUtils.equals(VoiceConstants.APPLAUSE, smallGame.type)) {

                    destroyDialog();
                    pointsDialog = new VoiceGameApplauseDialog(mRoomEffectsContext.getContext());
                    pointsDialog.setOnOkClickedListener(() -> {

                        //TODO EventBus
                        RoomSocketEngineHelper.sendGameEmotion(smallGame.type, position, VoiceConstants.NO_RES);
                        dismiss();

                    });
                    pointsDialog.show();

                } else {
                    //TODO EventBus
                    RoomSocketEngineHelper.sendGameEmotion(smallGame.type, position, VoiceConstants.NO_RES);
                    dismiss();
                }
            }
//            mRoomEffectsContext.recordLastSendEffectsTime(System.currentTimeMillis());
        });
    }

    private void destroyDialog() {
        if (scoreDialog != null) {
            if (scoreDialog.isShowing()) {
                scoreDialog.dismiss();
            }
            scoreDialog.destroy();
            scoreDialog = null;
        }
        if (pointsDialog != null) {
            if (pointsDialog.isShowing()) {
                pointsDialog.dismiss();
            }
            pointsDialog.destroy();
            pointsDialog = null;
        }
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        long currentTime = System.currentTimeMillis();
        long sendTime = mRoomEffectsContext.getLastSendEffectsTime();
        long terminal = mRoomEffectsContext.getSmallGameDefaultDuration();
        if (currentTime - sendTime > terminal) {
            hideView.setVisibility(View.GONE);
        } else {
            hideView.setVisibility(View.VISIBLE);
            long delayTime = terminal - (currentTime - sendTime);
            mRoomEffectsContext.post(() -> {
                hideView.setVisibility(View.GONE);
            }, delayTime);
        }
        super.showAtLocation(parent, gravity, x, y);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        destroy();
    }

    @Override
    public void destroy() {
        destroyDialog();
        if (mSmallGameViewPagerGroup != null) {
            mSmallGameViewPagerGroup.destroy();
            mSmallGameViewPagerGroup = null;
        }
    }
}
