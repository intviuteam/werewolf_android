package orangelab.project.voice.privateroom.view

import android.view.MotionEvent
import com.toolkit.action.Destroyable

/**
 *game_werewolf
 *2018/4/1 下午12:12
 *Mystery
 */
interface PrivateRoomToolBoxView : Destroyable {

    fun openToolBox()

    fun closeToolBox()

    fun goneToolBox()

    fun showToolBox()

    fun turnIntoUpSeatToolBox() //用于给麦上人展示的工具箱

    fun dispatchTouchEvent(ev: MotionEvent?): Boolean //事件拦截器

    fun turnIntoMasterToolBox() //用于给房主展示的工具箱

    fun turnIntoOutSeatToolBox() //用于给麦下人的工具箱

    fun showOpenSendTempImage()//展示临时图片为打开

    fun showCloseSendTempImage() //展示临时图片为关闭

    fun openToolBoxItemSendTempImage() //显示临时图片功能

    fun closeToolBoxItem()
}