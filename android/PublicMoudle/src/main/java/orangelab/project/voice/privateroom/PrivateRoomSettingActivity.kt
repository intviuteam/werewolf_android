package orangelab.project.voice.privateroom

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.androidtoolkit.view.ColorPickerView
import com.R
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.transport.TransportHelper
import com.androidtoolkit.transport.Transportable
import com.androidtoolkit.view.CornerTextView
import com.toolkit.TextToolKit
import com.toolkit.action.Action
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.tool.DialogManager
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.common.view.TitleView
import orangelab.project.voice.api.VoiceApiManager
import orangelab.project.voice.dialog.RoomPasswordDialog
import orangelab.project.voice.privateroom.model.PrivateRoomTagItem

/**
 * game_werewolf
 * 2018/3/25 下午6:19
 * Mystery
 */

class PrivateRoomSettingActivity : PrivateRoomBaseActivity(), View.OnClickListener {

    class PayLoad : Transportable {
        var roomId: String = ""
        var roomType: String = ""
        var title: String = ""
        var description = ""
        var color = ""
        var tag = ""
        var password = ""
    }

    companion object {

        val TAG = "PrivateRoomSettingActivity"

        fun Launch(activity: Activity, payload: PayLoad) {
            val intent = Intent(activity, PrivateRoomSettingActivity::class.java)
            TransportHelper.putTransportable(TAG, payload)
            activity.startActivity(intent)
        }

    }

    private val PASSWORD_DIALOG = "PrivateRoomSettingPasswordDialog"
    private var mPayload: PayLoad? = null

    private lateinit var mTitleView: TitleView
    private lateinit var mColorPicker: ColorPickerView

    private lateinit var mTagText: CornerTextView
    private lateinit var mTagEditText: EditText
    private lateinit var mTagContainer: View
    private var mCustomTagEnable = false

    private lateinit var mTagGridView: GridView
    private lateinit var mAdapter: Adapter

    private lateinit var mRoomNameEditText: EditText

    private lateinit var mRoomIntroduceEditText: EditText

    private lateinit var mRoomPasswordView: View
    private lateinit var mRoomPasswordText: TextView

    private var mCurRoomTagColor = ""
    private var mCurRoomTag = ""
    private var mCurRoomTitle = ""
    private var mCurRoomIntroduce = ""
    private var mCurRoomPassword = ""

    private var mDialogManager = DialogManager()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPayload = TransportHelper.getTransport(TAG)
        if (mPayload == null) {
            closeActivity()
            return
        }
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color)
        setContentView(R.layout.layout_private_room_setting)

        mTitleView = findViewById<TitleView>(R.id.title) as TitleView
        mTitleView.setActionBack {
            handleSave()
        }
        mTitleView.setTitle(R.string.str_private_room_setting_title)

        /**
         * 房间名称
         */
        mRoomNameEditText = findViewById<EditText>(R.id.edittext_room_name) as EditText
        mRoomNameEditText.setText(mPayload!!.title)
        mCurRoomTitle = mPayload!!.title

        /**
         * 房间介绍
         */
        mRoomIntroduceEditText = findViewById<EditText>(R.id.edittext_room_introduce) as EditText
        mRoomIntroduceEditText.setText(mPayload!!.description)
        mCurRoomIntroduce = mPayload!!.description

        mTagText = findViewById<CornerTextView>(R.id.tag_text) as CornerTextView
        mTagEditText = findViewById<EditText>(R.id.edit_room_tag) as EditText
        mTagContainer = findViewById(R.id.tag_text_container)

        /**
         * 房间密码
         */
        mRoomPasswordView = findViewById(R.id.container5)
        mRoomPasswordText = findViewById<TextView>(R.id.room_password) as TextView
        mRoomPasswordText.setText(mPayload!!.password)
        mRoomPasswordView.setOnClickListener(this)
        mCurRoomPassword = mPayload!!.password

        val testData = ArrayList<PrivateRoomTagItem>()
        mTagGridView = findViewById<GridView>(R.id.room_tag_gridview) as GridView
        mAdapter = Adapter(this, testData)
        mTagGridView.adapter = mAdapter
        mTagGridView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mAdapter.clearState()
                mAdapter.click(position)
                enableCustomTag(false, false)
                val curItem = mAdapter.getItem(position)
                curItem ?: return
                updateCurTag(curItem.tagText, curItem.tagColor)
            }
        })

        mColorPicker = findViewById<ColorPickerView>(R.id.color_picker) as ColorPickerView
        mColorPicker.setIndicatorColor(Color.GRAY)
        mColorPicker.setOrientation(ColorPickerView.Orientation.HORIZONTAL)
        mColorPicker.setOnColorPickerChangeListener(object : ColorPickerView.OnColorPickerChangeListener {
            override fun onColorChanged(picker: ColorPickerView?, color: Int) {
                runOnUiThreadSafely({
                    mCurRoomTagColor = Integer.toHexString(color)
                    printLog("select color ${mCurRoomTagColor}")
                    try {
                        mTagText.setTextBackGround(Color.parseColor("#${mCurRoomTagColor}"))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
            }

            override fun onStartTrackingTouch(picker: ColorPickerView?) {
            }

            override fun onStopTrackingTouch(picker: ColorPickerView?) {
            }
        })

        if (mCurRoomTagColor.isNullOrBlank()) {
            mCurRoomTagColor = "${Integer.toHexString(mColorPicker.color)}"
        }

        /**
         * 房间Tag
         */
        mCurRoomTag = mPayload!!.tag
        mCurRoomTagColor = mPayload!!.color

        mTagText.setText(MessageUtils.getString(R.string.str_private_room_tag_custom))
        mTagContainer.setOnClickListener {
            if (mCustomTagEnable) {
                return@setOnClickListener
            }
            enableCustomTag(true, true)
            showKeyBoard(mTagEditText)
        }
        mTagEditText.setOnClickListener {
            if (mCustomTagEnable) {
                return@setOnClickListener
            }
            enableCustomTag(true, true)
        }
        mTagEditText.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                enableCustomTag(true, true)
                showKeyBoard(mTagEditText)
            } else {

            }
        })
//        mTagEditText.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                updateTagText(s?.toString())
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//        })

        /**
         * 初始化房间的默认Tag
         */
        VoiceApiManager.VoiceRoomConfigApi(mPayload!!.roomId, mPayload!!.roomType, { result, e ->
            runOnUiThreadSafely({
                try {

                    result.default_config.tags.forEach {
                        val temp = PrivateRoomTagItem(it.color, it.text)
                        testData.add(temp)
                    }
                    mAdapter.notifyDataSetChanged()
                    /**
                     * 这里需要判断一下上次设定的是不是自定义还是系统的默认的
                     */
                    if (testData.size > 0) {
                        /**
                         * 先查找上次设定的是不是在这里
                         */
                        if (TextUtils.isEmpty(mCurRoomTag) || TextUtils.isEmpty(mCurRoomTagColor)) {
                            enableCustomTag(false, false)
                            mAdapter.click(0)
                            mAdapter.notifyDataSetChanged()
                            val curItem = mAdapter.getItem(0)
                            curItem ?: return@runOnUiThreadSafely
                            updateCurTag(curItem.tagText, curItem.tagColor)
                            return@runOnUiThreadSafely
                        }

                        var useCustom = true
                        testData.forEach {
                            if (TextUtils.equals(it.tagColor, mCurRoomTagColor) && TextUtils.equals(it.tagText, mCurRoomTag)) {
                                it.clicked = true
                                updateCurTag(it.tagText, it.tagColor)
                                useCustom = false
                                return@forEach
                            }
                        }
                        mAdapter.notifyDataSetChanged()

                        if (useCustom) {
                            enableCustomTag(true, false)
                            updateCustomTag(mCurRoomTag, mCurRoomTagColor)
                        }
                    } else {
                        /**
                         * 上次使用的是自定义的
                         */
                        enableCustomTag(true, false)
                        updateCustomTag(mCurRoomTag, mCurRoomTagColor)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
        })
    }

    private fun updateCustomTag(tag: String, tagColor: String) {
        mTagEditText.setText(tag)
        mTagText.setText(tag)
        var realColor = tagColor
        if (!tagColor.isNullOrBlank()) {
            realColor = tagColor.replace("#", "")
            try {
                mTagText.setTextBackGround(Color.parseColor("#${realColor}"))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun updateCurTag(tag: String, tagColor: String) {
        mCurRoomTagColor = tagColor
        mCurRoomTag = tag
        printLog("update Tag:${mCurRoomTag} , TagColor:${mCurRoomTagColor}")
    }

    private fun updateTagText(text: String?) {
        mTagText.setText(text)
    }

    private fun enableCustomTag(enable: Boolean, toDefault: Boolean) {
        if (mCustomTagEnable == enable) {
            return
        }
        mCustomTagEnable = enable
        if (mCustomTagEnable) {
            mAdapter.clearState()
            mColorPicker.setEnable(true)
//            mTagEditText.isEnabled = true
            mTagContainer.setBackgroundResource(R.drawable.drawable_private_room_tag_clicked)
            if (toDefault) {
                val color = Integer.toHexString(mColorPicker.color)
                updateCurTag("", color)
                updateCustomTag(mCurRoomTag, mCurRoomTagColor)
                updateTagText(getString(R.string.str_private_room_tag_custom))
                try {
                    mTagText.setTextBackGround(Color.parseColor("#${color}"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else {
            mColorPicker.setEnable(false)
//            mTagEditText.isEnabled = false
            mTagContainer.setBackgroundColor(Color.TRANSPARENT)
            updateTagText(getString(R.string.str_private_room_tag_custom))
            try {
                mTagText.setTextBackGround(Color.parseColor("#e3e3e3"))
            } catch (e: Exception) {
                e.printStackTrace()
            }
            mTagEditText.clearFocus()
            mTagEditText.setText("")
        }
    }

    override fun onBackPressed() {

    }

    private fun printLog(info: String) {
        Log.i("PrivateRoomSetting", info)
    }

    override fun closeActivity() {
        super.closeActivity()
        mDialogManager.destroy()
    }

    fun handleSave() {
        /**
         * 执行保存
         */
        mCurRoomTitle = mRoomNameEditText.text.toString()
        mCurRoomIntroduce = mRoomIntroduceEditText.text.toString()
        if (mCustomTagEnable) {
            mCurRoomTag = mTagEditText.text.toString()
        } else {
            //do nothing
        }

        if (mCurRoomTag.isNullOrBlank()) {

        } else {
            if (!TextToolKit.isChinese(mCurRoomTag)) {
                ToastToolKit.showShort(R.string.str_private_room_tag_input_error)
                return
            }
        }

        RoomSocketEngineHelper.sendUpdateTheme(mCurRoomTitle, mCurRoomIntroduce, mCurRoomTagColor, mCurRoomTag, mCurRoomPassword)
        closeActivity()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.container5 -> {
                /**
                 * 打开输入密码对话框
                 */
                val passwordDialog = RoomPasswordDialog(this, mPayload!!.password, Action {
                    mCurRoomPassword = it
                    mRoomPasswordText.setText(it)
                })
                mDialogManager.putAndShow(PASSWORD_DIALOG, passwordDialog)
            }
        }
    }

    class Adapter : BaseAdapter {

        var mData: MutableList<PrivateRoomTagItem>
        var mContext: Context

        constructor(mContext: Context, mData: MutableList<PrivateRoomTagItem>) : super() {
            this.mData = mData
            this.mContext = mContext
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var holder: ViewHolder
            var view: View? = convertView
            if (view == null) {
                holder = ViewHolder()
                view = View.inflate(mContext, R.layout.layout_private_room_tag, null)
                holder.text = view!!.findViewById<CornerTextView>(R.id.tag_text) as CornerTextView
                holder.textContainer = view!!.findViewById(R.id.tag_text_container)
                view.setTag(holder)
            } else {
                holder = view.getTag() as ViewHolder
            }
            val data = getItem(position)
            if (data != null) {
                try {
                    holder.text!!.setText(data.tagText)
                    try {
                        var realColor = data.tagColor
                        if (!data.tagColor.isNullOrBlank()) {
                            realColor = data.tagColor.replace("#", "")
                        }
                        holder.text!!.setTextBackGround(Color.parseColor("#${realColor}"))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (data.clicked) {
                        holder.textContainer!!.setBackgroundResource(R.drawable.drawable_private_room_tag_clicked)
                    } else {
                        holder.textContainer!!.setBackgroundColor(Color.TRANSPARENT)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return view
        }

        fun click(position: Int) {
            mData?.get(position)?.clicked = true
            notifyDataSetChanged()
        }

        fun clearState() {
            mData?.forEach {
                it.clicked = false
            }
            notifyDataSetChanged()
        }

        override fun getItem(position: Int): PrivateRoomTagItem? {
            return mData.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong();
        }

        override fun getCount(): Int {
            return mData.size
        }
    }

    class ViewHolder {
        var text: CornerTextView? = null
        var textContainer: View? = null
    }

}
