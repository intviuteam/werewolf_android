package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 01/12/2017.
 * <p>
 * 猜词结果
 */

public class VoiceSpyGuessWordResult implements Serializable, Keepable {

    public String word;
    public int duration;
    public boolean success;

    @Override
    public String toString() {
        return "VoiceSpyGuessWordResult{" +
                "word='" + word + '\'' +
                ", duration=" + duration +
                ", success=" + success +
                '}';
    }
}
