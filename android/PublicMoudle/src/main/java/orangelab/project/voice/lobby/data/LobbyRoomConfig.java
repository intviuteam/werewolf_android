package orangelab.project.voice.lobby.data;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.model.PersonalData;

public class LobbyRoomConfig {

    public static int maxChair = 1;                //最大座位数
    public static int maxSeatPosition = 0;         //最大座位号
    public static int minSeatPosition = 0;         //最小座位号
    public static int maxMessageLeaveTime = 1 * 10 * 1000; //消息最大存留时间,默认3分钟

    /**
     * 自己是否在座位上
     */
    public static boolean isUpSeat() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            return personalData.getSelfPosition() <= maxSeatPosition && personalData.getSelfPosition() >= minSeatPosition;
        }
        return false;
    }

    //是否是麦上位置
    public static boolean isUpSeat(int position) {
        return position <= maxSeatPosition && position >= minSeatPosition;
    }


}
