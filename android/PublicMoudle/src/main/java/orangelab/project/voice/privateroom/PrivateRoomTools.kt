package orangelab.project.voice.privateroom

import android.text.Html
import android.text.TextUtils
import android.widget.TextView
import com.datasource.GlobalUserState
import com.R
import orangelab.project.game.component.GameMember
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.common.model.ServerMessageEnterResult
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData
import orangelab.project.voice.privateroom.model.PrivateRoomMemberDataImpl

/**
 *game_werewolf
 *2018/3/19 下午5:40
 *Mystery
 */

object PrivateRoomTools {

    val PRIVATE_ROOM_INVITE_LIST = "PRIVATE_ROOM_INVITE_LIST";

    val DEFAULT_TAG_COLOR = "ffff8eff"

    val REQUEST_FRIEND = "private_room_request_friend"

    val MASTER_POSITION = 0
    val DEPUTY_POSITION = 1

    val MIN_POSITION = 0
    val MAX_POSITION = 5

    val MAX_POSITION_REAL = 6

    val DEFAULT_BACKGROUND_WIDTH = 750
    val DEFAULT_BACKGROUND_HEIGHT = 1334

    fun TurnIntoPrivateRoomMemberFromEnterResult(enterRoomUserItem: EnterRoomResult.EnterRoomUserItem): PrivateRoomMemberDataImpl {
        val privateRoomMember = PrivateRoomMemberDataImpl()
        privateRoomMember.position = enterRoomUserItem.position
        privateRoomMember.id = enterRoomUserItem.id
        privateRoomMember.level = enterRoomUserItem.level
        privateRoomMember.name = enterRoomUserItem.name
        privateRoomMember.sex = enterRoomUserItem.sex
        privateRoomMember.url = enterRoomUserItem.avatar
        privateRoomMember.isSpeaking = enterRoomUserItem.speaking
        privateRoomMember.signature = enterRoomUserItem.signature
        privateRoomMember.isFreeStyle = enterRoomUserItem.isFreeStyle
        privateRoomMember.isBanMic = enterRoomUserItem.isBanMic
        if (TextUtils.equals(enterRoomUserItem.id, GlobalUserState.getGlobalState().userId)) {
            privateRoomMember.isSelf = true
        }
        if (enterRoomUserItem.position == PrivateRoomTools.MASTER_POSITION) {
            privateRoomMember.isMaster = true
        }
        if (enterRoomUserItem.position == PrivateRoomTools.DEPUTY_POSITION) {
            privateRoomMember.isDeputy = true
        }
        privateRoomMember.memberVipInfo = enterRoomUserItem.vipInfo
        privateRoomMember.memberHeadDecor = enterRoomUserItem.avatar_box
        privateRoomMember.memberMsgDecor = enterRoomUserItem.message_box
        return privateRoomMember
    }

    fun FillTextViewByHtml(textView: TextView, source: String) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY))
        } else {
            textView.setText(Html.fromHtml(source))
        }
    }

    fun IsMaster(position: Int?): Boolean {
        if (position == null) {
            return false
        }
        return position == 0
    }

    fun PositionInUpSeat(position: Int): Boolean {
        return position in MIN_POSITION until MAX_POSITION_REAL
    }

    fun PositionInOutSeat(position: Int): Boolean {
        return (position > MAX_POSITION)
    }

    fun MessageItemPositionIv(position: Int): Int {
//        if (position == 0) {
//            return R.mipmap.voice_tag_master
//        }
        val temp = position + 1

        if (temp <= MAX_POSITION_REAL) {
            try {
                return R.mipmap::class.java.getDeclaredField("voice_tag_$temp").getInt(null)
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            }
        }

        return R.mipmap.voice_tag_other
    }

    fun PrivateRoomMembersTurnIntoGameMembers(privateMembers: MutableList<PrivateRoomMemberData>): MutableList<GameMember.GameMemberData> {
        val result = ArrayList<GameMember.GameMemberData>()
        privateMembers.forEach {
            val temp = GameMember.GameMemberData()
            temp.position = it.getMemberPosition()
            temp.userId = it.getMemberId()
            temp.userImage = it.getMemberIcon()
            temp.userName = it.getMemberName()
            temp.userSex = it.getMemberSex()
            temp.userLevel = it.getMemberLevel().toString()
            result.add(temp)
        }
        return result
    }


    fun ExecActionUpSeat(position: Int) {
        RoomSocketEngineHelper.sendUpSeat(position)
    }

}
