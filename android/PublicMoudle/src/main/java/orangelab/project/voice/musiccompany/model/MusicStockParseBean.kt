package orangelab.project.voice.musiccompany.model

import com.toolkit.action.Keepable
import orangelab.project.common.db.entity.MusicDataEntity
import java.io.Serializable

/**
 * 曲库解析类
 */
class MusicStockParseBean : Keepable, Serializable {
    var code: Int = 0
    var message: String = ""
    var data: Data = Data()


    class Data : Serializable, Keepable {
        var docs: ArrayList<MusicDataEntity> = ArrayList()
        var total: Int = 0
        var limit: Int = 0
        var offset: Int = 0
    }

}