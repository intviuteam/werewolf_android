package orangelab.project.voice.dialog;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.model.FriendsResult;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

/**
 * Created by lloydfinch on 14/10/2017.
 */

public class VoiceInviteFriendDialog extends SafeDialog {

    private static final String TAG = "VoiceInviteFriendDialog";

    private ListView listView;
    private TextView placeHolder;
    private View placeHolderView;
    private VoiceInviteFriendDialog.IntviuFriendAdapter adapter;
    private Activity activity;

    private LinearLayout llContentViewContainer;
    private View btnSure;
    private View btnCancel;

    public VoiceInviteFriendDialog(Activity activity) {
        super(activity, R.style.radius_dialog);
        this.activity = activity;

        init();
    }


    private void init() {
        initView();
        initWindow();
    }

    private void initView() {
        View contentView = LayoutInflater.from(activity).inflate(R.layout.layout_invite_online_friend, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llContentViewContainer = (LinearLayout) contentView.findViewById(R.id.ll_content_view_container);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        View view = LayoutInflater.from(activity).inflate(R.layout.layout_intviufriend, null);
        placeHolder = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolderView = view.findViewById(R.id.intviufriend_placeholder_iv);
        placeHolder.setVisibility(View.VISIBLE);
        listView = (ListView) view.findViewById(R.id.intviu_friend);
        adapter = new VoiceInviteFriendDialog.IntviuFriendAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(0);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            VoiceInviteFriendDialog.ViewHolder holder = (VoiceInviteFriendDialog.ViewHolder) view1.getTag();
            IntviuFriendDialog.FriendItem item = adapter.getItem(position);
            if (item == null) {
                return;
            }
            if (item.hasCheck) {
                item.hasCheck = false;
                if (holder != null) {
                    holder.radio.setChecked(false);
                }
                adapter.removeSelected(item);
            } else {
                item.hasCheck = true;
                if (holder != null) {
                    holder.radio.setChecked(true);
                }
                Log.i("Lean", "add item " + item);
                adapter.addSelected(item);
            }
        });
        changeContentView(view);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        btnSure.setOnClickListener(v -> {
            if (adapter == null) return;
            if (adapter.getCount() == 0) {
                ToastToolKit.showShort(R.string.intviu_friend_select);
                return;
            }
            if (adapter.getSelected().size() > 0) {
                Log.i("Lean", TAG + adapter.getSelected());
                LeanCloudChatHelper.sendIntviuToConversation(
                        GlobalUserState.getGlobalState().getCurRoomId(),
                        GlobalUserState.getGlobalState().getPassword(),
                        adapter.getSelected()
                );
                ToastToolKit.showShort(R.string.intviu_friend_send);
                dimissAndDestroy();
            } else {
                ToastToolKit.showShort(R.string.intviu_friend_select);
            }


        });
        btnCancel.setOnClickListener(v -> {
            dimissAndDestroy();
        });
        UnifiedBridge.RegisterHandler(this, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST, value -> {
            Utils.runSafely(() -> {
                FriendsResult result = GsonHelper.getGson().fromJson(value.getModel().params.toString(), FriendsResult.class);
                runSafely(() -> {
                    if (listView == null || adapter == null) {
                        showPlaceHolder();
                        return;
                    }
                    if (result != null && result.list != null && result.list.size() > 0) {
                        goneShowPlaceHolder();
                        if (adapter != null) {
                            adapter.updateDatas(result.list);
                        }
                    } else {
                        showPlaceHolder();
                    }
                });
            });
        });
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST, true);
        EventBus.getDefault().register(this);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void changeContentView(View contentView) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(contentView);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        dimissAndDestroy();
    }

    private void dimissAndDestroy() {
        onlyDismiss();
        destroy();
    }

    private void onlyDismiss() {
        dismiss();
    }

    private void destroy() {
        if (listView != null) {
            listView = null;
        }
        if (adapter != null) {
            adapter.destroy();
            adapter = null;
        }
        if (placeHolder != null) {
            placeHolder = null;
        }
        if (activity != null) {
            activity.finish();
            activity = null;
        }
        UnifiedBridge.UnRegisterHandler(this);
        EventBus.getDefault().unregister(this);
    }

    private void showPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.VISIBLE);
            placeHolder.setText(MessageUtils.getString(R.string.intviu_friend_empty));
            placeHolder.setVisibility(View.VISIBLE);
        }
        if (listView != null) {
            listView.setVisibility(View.GONE);
        }
    }

    private void goneShowPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.GONE);
            placeHolder.setVisibility(View.GONE);
        }
        if (listView != null) {
            listView.setVisibility(View.VISIBLE);
        }
    }

    private static class IntviuFriendAdapter extends BaseAdapter {

        private Context context;
        private List<IntviuFriendDialog.FriendItem> datas;
        private List<IntviuFriendDialog.FriendItem> selected;

        public IntviuFriendAdapter(Context context) {
            this.context = context;
            datas = new ArrayList<>();
            selected = new ArrayList<>();
        }

        public void destroy() {
            datas.clear();
            notifyDataSetChanged();
            context = null;
        }

        public List<IntviuFriendDialog.FriendItem> getSelected() {
            return selected;
        }

        public void addSelected(IntviuFriendDialog.FriendItem item) {
            if (selected.contains(item))
                return;
            selected.add(item);
        }

        public void removeSelected(IntviuFriendDialog.FriendItem item) {
            selected.remove(item);
        }

        public void updateDatas(List<IntviuFriendDialog.FriendItem> update) {
            if (update != null) {
                datas.clear();
                datas.addAll(update);
                notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public IntviuFriendDialog.FriendItem getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            VoiceInviteFriendDialog.ViewHolder holder = null;
            if (context == null) {
                return convertView;
            }
            if (convertView == null) {
                holder = new VoiceInviteFriendDialog.ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.layout_intviufriend_item, null);
                holder.userhead = (UserHeadView) convertView.findViewById(R.id.intviufriend_ic);
                holder.level = (TextView) convertView.findViewById(R.id.intviufriend_level);
                holder.name = (TextView) convertView.findViewById(R.id.intviufriend_name);
                holder.radio = (RadioButton) convertView.findViewById(R.id.intviufriend_check);

                holder.littleOnline = (ImageView) convertView.findViewById(R.id.intviufriend_online);
                holder.littleRoom = (ImageView) convertView.findViewById(R.id.intviufriend_littleroom);
                holder.roomId = (TextView) convertView.findViewById(R.id.intviufriend_roomId);
                holder.status = (TextView) convertView.findViewById(R.id.intviufriend_status);

                convertView.setTag(holder);
            } else {
                holder = (VoiceInviteFriendDialog.ViewHolder) convertView.getTag();
            }
            IntviuFriendDialog.FriendItem item = getItem(position);
            holder.radio.setFocusable(false);
            holder.radio.setClickable(false);
            if (item != null) {
                holder.userhead.setUserHeadImageUrl(item.image);
                holder.userhead.setUserSex(item.sex);
                holder.name.setText(item.name);
                holder.level.setText("Lv." + item.game.level);
                holder.radio.setVisibility(View.VISIBLE);
                item.game_status = item.turnToGameStatus();
                if (item.game_status == 1) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.INVISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_online_txt));
                } else if (item.game_status == 2) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_prepare));
                } else if (item.game_status == 3) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_gaming));
                } else {
                    holder.littleOnline.setVisibility(View.GONE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.GONE);
                    holder.status.setVisibility(View.GONE);
                }

                holder.radio.setChecked(item.hasCheck);
            }
            return convertView;
        }
    }

    public static class FriendItem {
        public String id;
        public String name;
        public String image;
        public int sex;
        public int game_status;
        public String status;
        public String room_id;
        public IntviuFriendDialog.FriendItemGame game;
        public boolean hasCheck = false;
        public long created_at;

        @Override
        public boolean equals(Object obj) {
            boolean result = false;
            if (obj == null) {
                result = false;
            }
            try {
                IntviuFriendDialog.FriendItem other = (IntviuFriendDialog.FriendItem) obj;
                if (other != null && TextUtils.equals(id, other.id)) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        public int turnToGameStatus() {
            if (TextUtils.isEmpty(status)) {
                return 1;
            }
            String[] strs = status.split(":");
            try {
                if (strs.length != 2) {
                    return 1;
                } else {
                    int result = Integer.parseInt(strs[0]);
                    return result;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        @Override
        public String toString() {
            return "FriendItem{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", image='" + image + '\'' +
                    ", sex=" + sex +
                    ", game_status=" + game_status +
                    ", room_id='" + room_id + '\'' +
                    ", game=" + game +
                    ", hasCheck=" + hasCheck +
                    ", created_at=" + created_at +
                    '}';
        }
    }

    public static class FriendItemGame {
        public int win;
        public int lose;
        public int level;
        public int experience;

        @Override
        public String toString() {
            return "FriendItemGame{" +
                    "win=" + win +
                    ", lose=" + lose +
                    ", level=" + level +
                    ", experience=" + experience +
                    '}';
        }
    }

    private static class ViewHolder {
        public UserHeadView userhead;
        public TextView name;
        public TextView level;
        public RadioButton radio;

        public ImageView littleOnline;
        public ImageView littleRoom;
        public TextView roomId;
        public TextView status;

    }

    @Override
    protected void release() {

    }
}
