package orangelab.project.voice.lobby.data;

import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.minigame.model.MiniGameItem;
import orangelab.project.voice.ds.LimitMap;

/**
 * 数据仓库
 */
public class LobbyDataStock implements Destroyable {

    private static LobbyDataStock instance;

    //小游戏列表
    public List<MiniGameItem> mMiniGameItems = new ArrayList<>();

    public static LobbyDataStock getInstance() {
        if (instance == null) {
            synchronized (LobbyDataStock.class) {
                if (instance == null) {
                    instance = new LobbyDataStock();
                }
            }
        }
        return instance;
    }

    private LobbyDataStock() {

    }

    //刷新小游戏列表
    public void refreshMiniGames(List<MiniGameItem> miniGameItems) {
        if (miniGameItems != null) {
            this.mMiniGameItems.clear();
            this.mMiniGameItems.addAll(miniGameItems);
        }
    }


    /**
     * 小游戏邀请消息列表
     * ID:用户id，后者覆盖前者
     */
    private LimitMap<String, SystemMessageItem> messageMap = new LimitMap<>();

    public void addMessage(String id, SystemMessageItem messageItem) {
        messageMap.put(id, messageItem);
    }

    public SystemMessageItem getLatestMessage(String key) {
        return messageMap.get(key);
    }

    /**
     * 获取所有的聊天消息
     */
    public Collection<SystemMessageItem> getAllInviteMessages() {
        return messageMap.values();
    }

    /**
     * 获取最大的能邀请游戏的数量
     */
    public int getMaxInviteCount() {
        if (mMiniGameItems == null) {
            return 0;
        }
        return mMiniGameItems.size();
    }

    @Override
    public void destroy() {
        mMiniGameItems.clear();
        messageMap.clear();
        instance = null;
    }
}
