package orangelab.project.voice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.R;
import com.toolkit.action.Destroyable;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 31/10/2017.
 * //提示需要钻石对话框
 */

public class VoiceGameApplauseDialog extends Dialog implements Destroyable {

    private View btnSure;
    private View btnCancel;

    private Context mContext;
    private OnOkClickedListener onOkClickedListener;

    public VoiceGameApplauseDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceGameApplauseDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_applause, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(380);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            if (onOkClickedListener != null) {
                onOkClickedListener.onOkClicked();
            }
            dismiss();
        });
        btnCancel.setOnClickListener(v -> dismiss());
    }

    public void setOnOkClickedListener(OnOkClickedListener onOkClickedListener) {
        this.onOkClickedListener = onOkClickedListener;
    }

    public interface OnOkClickedListener {
        void onOkClicked();
    }

    @Override
    public void destroy() {

    }
}
