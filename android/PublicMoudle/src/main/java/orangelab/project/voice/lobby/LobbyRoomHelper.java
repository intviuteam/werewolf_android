package orangelab.project.voice.lobby;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;

public class LobbyRoomHelper {

    /**
     * 判断此人是否在房间
     */
    public static boolean isInRoom(String id) {
        EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserById(id);

        return user != null;
    }
}
