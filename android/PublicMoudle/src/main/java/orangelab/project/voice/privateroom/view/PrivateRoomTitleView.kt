package orangelab.project.voice.privateroom.view

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.view.CornerTextView
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.privateroom.PrivateRoomObserver
import orangelab.project.voice.privateroom.PrivateRoomTools
import com.toolkit.action.Destroyable

/**
 *game_werewolf
 *2018/3/20 下午6:07
 *Mystery
 */
class PrivateRoomTitleView : View.OnClickListener, Destroyable {
    private var mTitleManager: View
    private var mTitleIntviu: View
    private var mTitleSmall: View
    private var mTitleBack: View
    private var mRoomId: TextView
    private var mOnLineNumberText: TextView
    private var mOnLineNumberContainer: View
    //    private var mRoomLike: TextView
    private var mAudioWifi: ImageView

    private var mTagContainer: View
    private var mTagText: CornerTextView
    private var mTagIntroduce: TextView

    private var mPrivateRoomObserver: PrivateRoomObserver? = null

    constructor(mRootView: ViewGroup, privateRoomObserver: PrivateRoomObserver) {

        mPrivateRoomObserver = privateRoomObserver

        mTitleManager = mRootView!!.findViewById(R.id.manage)
        mTitleManager.setOnClickListener(this)
        mTitleIntviu = mRootView!!.findViewById(R.id.intviu)
        mTitleIntviu.setOnClickListener(this)
        mTitleSmall = mRootView!!.findViewById(R.id.be_small)
        mTitleSmall.setOnClickListener(this)

        mTitleBack = mRootView!!.findViewById(R.id.id_back)
        mTitleBack.setOnClickListener(this)

        mAudioWifi = mRootView!!.findViewById<ImageView>(R.id.id_audio_wifi) as ImageView

        mRoomId = mRootView!!.findViewById<TextView>(R.id.room_id) as TextView
        mOnLineNumberContainer = mRootView!!.findViewById(R.id.id_online_number_container)
        mOnLineNumberContainer.setOnClickListener(this)

        mOnLineNumberText = mRootView!!.findViewById<TextView>(R.id.id_online_number) as TextView
//        mRoomLike = mRootView!!.findViewById(R.id.like_number) as TextView

        mTagContainer = mRootView!!.findViewById(R.id.tag_container)
        mTagText = mRootView!!.findViewById<CornerTextView>(R.id.tag_text) as CornerTextView
        try {
            mTagText.setTextBackGround(Color.parseColor("#ff8eff"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mTagIntroduce = mRootView!!.findViewById<TextView>(R.id.tag_introduce) as TextView
        mTagContainer.setOnClickListener(this)
    }

    fun audioWifiState(isDisConnect: Boolean) {
        if (isDisConnect) {
            mAudioWifi.setImageResource(R.mipmap.wifi_disconnect)
        } else {
            mAudioWifi.setImageResource(R.mipmap.wifi_connect)
        }
    }

    private fun setTag(text: String, introduce: String, color: String) {
        mTagContainer.visibility = View.VISIBLE
        mTagText.setText(text)
        mTagIntroduce.setText(introduce)
        try {
            var realcolor = color
            if (!color.isNullOrBlank()) {
                realcolor = color.replace("#", "")
            }
            mTagText.setTextBackGround(Color.parseColor("#${realcolor}"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun defaultTag() {
        setTag(MessageUtils.getString(R.string.str_private_room_default_tag), MessageUtils.getString(R.string.str_private_room_default_tag_introduce), PrivateRoomTools.DEFAULT_TAG_COLOR)
    }

    fun goneTag() {
        mTagContainer.visibility = View.GONE
    }

    fun goneMiniMode() {
        mTitleSmall.visibility = View.GONE
    }

    fun updateTitle(isMaster: Boolean) {
        if (isMaster) {
            mTitleSmall.visibility = View.VISIBLE
            mTitleIntviu.visibility = View.VISIBLE
            mTitleManager.visibility = View.VISIBLE

        } else {
            mTitleSmall.visibility = View.VISIBLE
            mTitleIntviu.visibility = View.VISIBLE
            mTitleManager.visibility = View.GONE
        }
    }

    fun updateTag(text: String, introduce: String, color: String) {
        setTag(text, introduce, color)
    }

    fun setRoomId(roomId: String) {
        PrivateRoomTools.FillTextViewByHtml(mRoomId, MessageUtils.getString(R.string.str_private_room_id, roomId))
    }

    fun setOnlineNumber(number: Int) {
        PrivateRoomTools.FillTextViewByHtml(mOnLineNumberText, MessageUtils.getString(R.string.str_private_room_online_number, number.toString()))
    }

    fun setLikeNumber(number: Long) {
//        mRoomLike.setText(number.toString())
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.manage -> {
                mPrivateRoomObserver?.onManageRoom()
            }
            R.id.intviu -> {
                mPrivateRoomObserver?.onInvite()
            }
            R.id.be_small -> {
                mPrivateRoomObserver?.onMinimize()
            }
            R.id.id_online_number_container -> {
                mPrivateRoomObserver?.onOnlineNumber()
            }
            R.id.id_back -> {
                mPrivateRoomObserver?.onExit()
            }
            R.id.tag_container -> {
                mPrivateRoomObserver?.onManageTag()
            }
            else -> {
                return
            }
        }
    }

    override fun destroy() {
        mPrivateRoomObserver = null
    }

}