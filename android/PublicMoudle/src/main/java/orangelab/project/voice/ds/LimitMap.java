package orangelab.project.voice.ds;

import com.androidtoolkit.PLog;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Created by lloydfinch on 28/03/2018.
 */

public final class LimitMap<AnyKey, AnyValue> {

    public static final String TAG = "LimitMap";

    private static final int DEFAULT_SIZE = 200;
    private int size = DEFAULT_SIZE;
    private LinkedHashMap<AnyKey, AnyValue> limitMap = new LinkedHashMap<>();

    public LimitMap() {
        this.size = DEFAULT_SIZE;
    }

    public LimitMap(int size) {
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * 增
     */
    public AnyValue put(AnyKey key, AnyValue value) {
        if (limitMap.size() > size) {
            try {
                limitMap.remove(limitMap.keySet().toArray()[0]);
            } catch (Exception e) {
                PLog.e(TAG, "put exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
        PLog.e(TAG, "put value, key is " + key + ", value is " + value);
        return limitMap.put(key, value);
    }

    /**
     * 查
     */
    public AnyValue get(AnyKey key) {
        return limitMap.get(key);
    }

    /**
     * 删
     */
    public AnyValue remove(AnyKey key) {
        return limitMap.remove(key);
    }

    /**
     * 删，根据value
     */
    public AnyValue removeByValue(AnyValue value) {
        AnyKey anyKey = null;
        Set<AnyKey> anyKeys = limitMap.keySet();
        for (AnyKey key : anyKeys) {
            if (limitMap.get(key).equals(value)) {
                anyKey = key;
                break;
            }
        }
        PLog.e(TAG, "remove by value, key is " + anyKey + ", value is " + value);

        return limitMap.remove(anyKey);
    }

    public Collection<AnyValue> values() {
        return limitMap.values();
    }

    public int size() {
        return limitMap.size();
    }

    public void clear() {
        limitMap.clear();
    }
}