package orangelab.project.voice.model;

import orangelab.project.common.model.EnterRoomResult;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by lloydfinch on 06/12/2017.
 */

public class VoiceRoomConfigBean implements Serializable, Keepable {

    public String type;
    public int min_user;
    public int max_user;
    public int lock_start;
    public int active_count;
    public boolean can_boom;
    public boolean has_sheriff;
    public boolean show_role_if_dead;
    public int can_save_self;
    public boolean kill_all;
    public boolean can_cut_speaker;//是否可插mai
    public boolean night_speak = false;
    public int min_level;
    public int max_level;
    public int level;
    public Map<String, Integer> experience;
    public List<Integer> level_limitations;
    public EnterRoomResult.ExportConfig export_request;
    public UnderCoverConfig undercover;//谁是卧底相关配置

    public static class ExportConfig implements Serializable, Keepable {
        public int level;
        public EnterRoomResult.ExportCostConfig cost;
    }

    public static class ExportCostConfig implements Serializable, Keepable {
        public String type;
        public int value;
    }

    public static class UnderCoverConfig implements Serializable, Keepable {
        public boolean can_guess;
        public boolean has_ghost;
        public boolean more_undercover;
        public boolean configed;
        public boolean configing;
        public int undercover_count;

    }
}
