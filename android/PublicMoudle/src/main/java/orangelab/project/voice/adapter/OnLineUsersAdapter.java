package orangelab.project.voice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 30/08/2017.
 */

public class OnLineUsersAdapter extends RecyclerView.Adapter<OnLineUsersAdapter.MyViewHolder> {

    private Context mContext;
    List<EnterRoomResult.EnterRoomUserItem> gameMembers = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnAddToBlackClickListener onAddToBlackClickListener;
    private boolean needShowBlack = false;

    public OnLineUsersAdapter(Context mContext, List<EnterRoomResult.EnterRoomUserItem> gameMembers) {
        this.mContext = mContext;
        this.gameMembers = gameMembers;
    }

    public void setGameMembers(List<EnterRoomResult.EnterRoomUserItem> gameMembers) {
        this.gameMembers = gameMembers;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OnAddToBlackClickListener getOnAddToBlackClickListener() {
        return onAddToBlackClickListener;
    }

    public void setOnAddToBlackClickListener(OnAddToBlackClickListener onAddToBlackClickListener) {
        this.onAddToBlackClickListener = onAddToBlackClickListener;
    }

    public void setNeedShowBlack(boolean needShowBlack) {
        this.needShowBlack = needShowBlack;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_online_user, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        EnterRoomResult.EnterRoomUserItem gameMember = gameMembers.get(position);
        if (gameMember != null) {
            holder.tvName.setText(gameMember.name);

            holder.tvMotto.setText(gameMember.signature);
            if (!TextUtils.isEmpty(gameMember.avatar)) {
                PicassoUtils.loadImageByDefault(mContext, gameMember.avatar, holder.ivHead, R.mipmap.default_head);
            }
            if (gameMember.position == VoiceConstants.MASTER_POSITION) {
                holder.tvMaster.setVisibility(View.VISIBLE);
                holder.tvUpSeat.setVisibility(View.GONE);
                holder.btnAddBlack.setVisibility(View.GONE);
            } else {
                holder.tvMaster.setVisibility(View.GONE);
                if (showUpSeatTag(gameMember.position)) {
                    holder.tvUpSeat.setVisibility(View.VISIBLE);
                } else {
                    holder.tvUpSeat.setVisibility(View.GONE);
                }
                boolean isMaster = false;
                PersonalData personalData = PersonalDataHelper.getPersonalData();
                if (personalData != null && personalData.getSelfPosition() == VoiceConstants.MASTER_POSITION) {
                    isMaster = true;
                }
                if (needShowBlack && isMaster) {
                    holder.btnAddBlack.setVisibility(View.VISIBLE);
                } else {
                    holder.btnAddBlack.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 是否展示"上麦"标签
     */
    private boolean showUpSeatTag(int position) {
        boolean show = (position <= VoiceRoomConfig.getMaxChairNumber()) &&
                !VoiceRoomConfig.isLobby();

        return show;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return gameMembers.size();
    }

    public void destroy() {
        gameMembers.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivHead;
        TextView tvName;
        TextView tvMaster;
        TextView tvUpSeat;
        TextView tvMotto;
        Button btnAddBlack;

        public MyViewHolder(View contentView) {
            super(contentView);
            ivHead = (ImageView) contentView.findViewById(R.id.iv_online_user);
            tvName = (TextView) contentView.findViewById(R.id.tv_online_name);
            tvMaster = (TextView) contentView.findViewById(R.id.tv_online_master);
            tvUpSeat = (TextView) contentView.findViewById(R.id.tv_online_up_seat);
            tvMotto = (TextView) contentView.findViewById(R.id.tv_online_motto);
            btnAddBlack = (Button) contentView.findViewById(R.id.btn_add_black);
            btnAddBlack.setOnClickListener(v -> {
                if (onAddToBlackClickListener != null) {
                    onAddToBlackClickListener.onAddToBlackClick(getPosition());
                }
            });
            contentView.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(getPosition());
                }
            });
        }
    }

    public interface OnAddToBlackClickListener {
        void onAddToBlackClick(int position);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
