package orangelab.project.voice.component;

import orangelab.project.common.model.ServerMessageEventOver;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public interface VoiceSpeakViewComponent extends Destroyable {

    void displayData();

    void handleSpeak(boolean canSpeak);

    void handleServerEvent(ServerMessageEventOver event);

    void restoreFormSmallWindow();
}
