package orangelab.project.voice.privateroom.holder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.R
import orangelab.project.voice.privateroom.model.SystemMessageItem
import me.drakeet.multitype.ItemViewBinder

/**
 * game_werewolf
 * 2018/3/21 下午6:03
 * Mystery
 */

class SystemMsgItemViewBinder : ItemViewBinder<SystemMessageItem, SystemMsgItemViewBinder.SystemMsgItemHolder>() {


    class SystemMsgItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var message: TextView

        init {
            message = itemView!!.findViewById(R.id.tv_item_system_msg) as TextView
        }
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): SystemMsgItemHolder {
        val root = inflater.inflate(R.layout.layout_private_room_sys_msg, parent, false)
        return SystemMsgItemHolder(root)
    }

    override fun onBindViewHolder(holder: SystemMsgItemHolder, item: SystemMessageItem) {
        if (item.textColorResource != -1) {
            holder.message.setTextColor(holder.message.context.resources.getColor(item.textColorResource))
        }
        holder.message.setText(item.message)
    }


}
