package orangelab.project.voice.model;

import com.androidtoolkit.transport.Transportable;
import com.toolkit.action.Keepable;

public class VoiceAddFriendBean implements Keepable, Transportable {

    public String id = "";
    public boolean isHandle = false;

    public VoiceAddFriendBean(String id, boolean isHandle) {
        this.id = id;
        this.isHandle = isHandle;
    }
}
