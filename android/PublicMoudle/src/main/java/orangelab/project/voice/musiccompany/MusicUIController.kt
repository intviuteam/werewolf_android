package orangelab.project.voice.musiccompany

import android.content.Context
import android.support.v4.widget.DrawerLayout
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import orangelab.project.voice.handler.AbstractSocketMessageHandler
import orangelab.project.voice.handler.VoiceMessageBean
import orangelab.project.voice.handler.VoiceServerMessageHandler
import orangelab.project.voice.musiccompany.api.MusicSocketHelper
import orangelab.project.voice.musiccompany.component.MusicBodyView
import orangelab.project.voice.musiccompany.component.MusicSongMenuView
import orangelab.project.voice.musiccompany.component.MusicTitleView
import orangelab.project.voice.utils.PositionHelper

class MusicUIController(context: Context, contentView: ViewGroup) : Destroyable {

    private val TAG = "MusicUIController"

    //伴奏顶部相关布局
    private var musicTitleView: MusicTitleView = MusicTitleView(context, contentView.findViewById(R.id.rl_title_container) as ViewGroup)

    //伴奏中部相关布局，包括播放器
    private var musicBodyView: MusicBodyView = MusicBodyView(context, contentView.findViewById(R.id.voice_container) as ViewGroup)

    //侧滑菜单，向左滑弹出歌单
    private var drawerLayout: DrawerLayout = contentView.findViewById(R.id.drawer_layout_music) as DrawerLayout

    //歌单
    private var musicSongMenuView: MusicSongMenuView = MusicSongMenuView(context, contentView.findViewById(R.id.ll_music_song_menu_container))

    //观察器
    private var observers: ArrayList<AbstractSocketMessageHandler> = ArrayList()

    init {
        musicTitleView.setOnMenuClickListener(
                View.OnClickListener {
                    popUpSongMenu()
                }
        )

        //侧滑的事件监听
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {

            override fun onDrawerStateChanged(newState: Int) {
                PLog.e(TAG, "onDrawerStateChanged: $newState")
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                PLog.e(TAG, "onDrawerSlide: $slideOffset")
            }

            override fun onDrawerClosed(drawerView: View) {
                PLog.e(TAG, "onDrawerClosed")
            }

            override fun onDrawerOpened(drawerView: View) {
                PLog.e(TAG, "onDrawerOpened")
                musicSongMenuView.refreshTitle()
                musicSongMenuView.refreshList()
            }

        })


        initEventListener()

    }

    /**
     * 初始化消息处理器
     */
    private fun initEventListener() {

    }

    /**
     * 注册事件监听
     */
    fun registerEvent() {
        observers.add(musicTitleView.observer)
        observers.add(musicBodyView.observer)
        observers.add(musicSongMenuView.observer)
        VoiceServerMessageHandler.getInstance().registerUIHandler(observer)

    }

    /**
     * 展示数据
     */
    fun displayData() {
        musicTitleView.refreshSongNumber()
    }

    /**
     * 展示ktv相关UI
     */
    fun showKtv() {
        musicTitleView.showSongMenu()
        musicBodyView.showPlayerView()
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    /**
     * 隐藏ktv相关UI
     */
    fun hideKtv() {
        musicTitleView.hideSongMenu()
        musicBodyView.hidePlayerView()
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        //如果当前自己在唱歌的时候，改了房间话题，需要切换发送一个stop唱歌的消息
        if (PositionHelper.isSinger()) {
            MusicSocketHelper.INSTANCE.sendStopSing()
        }
    }

    /**
     * 弹出侧滑
     */
    private fun popUpSongMenu() {
        drawerLayout.openDrawer(Gravity.RIGHT)
    }

    override fun destroy() {

        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(observer)

        observers.clear()

        musicTitleView.destroy()
        musicBodyView.destroy()
    }

    /**
     * 消息处理
     */
    private var observer: AbstractSocketMessageHandler = object : AbstractSocketMessageHandler() {

        override fun handleBookSong(messageBean: VoiceMessageBean?) {
            PLog.e(TAG, "handleBookSong")
            observers.forEach { it.handleBookSong(messageBean) }
        }

        override fun handleStartSing(messageBean: VoiceMessageBean?) {
            observers.forEach { it.handleStartSing(messageBean) }
        }

        override fun handleStopSing(messageBean: VoiceMessageBean?) {
            observers.forEach { it.handleStopSing(messageBean) }
        }

        override fun handlePeakSong(messageBean: VoiceMessageBean?) {
            if (messageBean != null) {
                if (!TextUtils.isEmpty(messageBean.message)) {
                    ToastToolKit.showShort(messageBean.message)
                }
            }
            observers.forEach { it.handlePeakSong(messageBean) }
        }

        override fun handleRemoveSong(messageBean: VoiceMessageBean?) {
            observers.forEach { it.handleRemoveSong(messageBean) }
        }

        override fun destroy() {

        }

    }
}

