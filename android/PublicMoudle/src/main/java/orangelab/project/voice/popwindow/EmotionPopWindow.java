package orangelab.project.voice.popwindow;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.R;

import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.view.RoomEffectsContext;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import com.toolkit.action.Destroyable;

import orangelab.project.common.utils.Utils;
import orangelab.project.voice.view.EmotionViewPagerGroup;

/**
 * Created by lloydfinch on 21/10/2017.
 * Modified by hwx on 1/8/2018
 */

public class EmotionPopWindow extends PopupWindow implements Destroyable {

    private EmotionViewPagerGroup mEmotionViewPagerGroup;
    private RoomEffectsContext mRoomEffectsContext;

    private View hideView;
    private static final int ITEMNUM_MAX = 10;
    private static final int ITEMNUM_COLUME = 5;

    public EmotionPopWindow(RoomEffectsContext context) {
        super(context.getContext());
        this.mRoomEffectsContext = context;
        init();
    }

    private void init() {
        initView();
        initRequest();
        initListener();
    }

    private void initView() {
        View contentView = View.inflate(mRoomEffectsContext.getContext(), R.layout.layout_pop_voice_emotion, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setAnimationStyle(R.style.voice_emotion_pop);

        mEmotionViewPagerGroup = (EmotionViewPagerGroup) contentView.findViewById(R.id.rv_voice_emotion);
        mEmotionViewPagerGroup.initResource(R.layout.emotion_viewpage_item, ITEMNUM_MAX, ITEMNUM_COLUME,
                R.mipmap.gift_index_selected, R.mipmap.gift_index_normal);
        hideView = contentView.findViewById(R.id.view_voice_hide);
    }


    private void initRequest() {
        EffectsManager.QueryEmotionList(value -> {
            mRoomEffectsContext.post(() -> {
                if (!Utils.targetListIsNull(value)) {
                    mEmotionViewPagerGroup.setData(value);
                }
            });
        });
    }


    private void initListener() {
        mEmotionViewPagerGroup.setOnItemClickedListener((emotion) -> {
            if (mRoomEffectsContext.isSelfIsAvailablePosition()) {
                RoomSocketEngineHelper.sendEmotion(emotion.type, emotion.url, mRoomEffectsContext.getSelfPosition());
            }
            dismiss();
        });
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        long currentTime = System.currentTimeMillis();
        long sendTime = mRoomEffectsContext.getLastSendEffectsTime();
        long terminal = mRoomEffectsContext.getEmotionDefaultDuration();
        if (currentTime - sendTime > terminal) {
            hideView.setVisibility(View.GONE);
        } else {
            hideView.setVisibility(View.VISIBLE);
            long delayTime = terminal - (currentTime - sendTime);
            mRoomEffectsContext.post(() -> {
                hideView.setVisibility(View.GONE);
            }, delayTime);
        }
        super.showAtLocation(parent, gravity, x, y);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        destroy();
    }

    @Override
    public void destroy() {
        if (mEmotionViewPagerGroup != null) {
            mEmotionViewPagerGroup.destroy();
            mEmotionViewPagerGroup = null;
        }
    }
}
