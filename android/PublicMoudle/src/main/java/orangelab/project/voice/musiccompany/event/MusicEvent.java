package orangelab.project.voice.musiccompany.event;

import orangelab.project.common.db.entity.MusicDataEntity;
import orangelab.project.voice.musiccompany.model.MusicOrderSong;

public class MusicEvent {

    public static class MusicDownLoadEvent {
        public MusicDataEntity data;
        public int position;

        public MusicDownLoadEvent(MusicDataEntity data, int position) {
            this();
            this.data = data;
            this.position = position;
        }

        public MusicDownLoadEvent() {
        }

        @Override
        public String toString() {
            return "MusicDownLoadEvent{" +
                    "data=" + data +
                    ", position=" + position +
                    '}';
        }
    }

    public static class MusicStartEvent {

        public MusicDataEntity musicData;

        public MusicStartEvent(MusicDataEntity musicData) {
            this.musicData = musicData;
        }
    }

    public static class MusicFileSetEvent {
        public long length;

        public MusicFileSetEvent(long length) {
            this.length = length;
        }
    }

    public static class MusicCompanyStart {

    }

    public static class MusicOrderEvent {

        public MusicOrderSong orderSong;

        public MusicOrderEvent(MusicOrderSong musicData) {
            this.orderSong = musicData;
        }
    }

    /**
     * 表示完全退出腾讯ktv房间
     */
    public static class ExitKTVRoomEvent {

    }
}
