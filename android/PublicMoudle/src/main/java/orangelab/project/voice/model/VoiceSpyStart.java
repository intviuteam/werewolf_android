package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * "谁是卧底"开始游戏server返回的结构
 */

public class VoiceSpyStart implements Serializable, Keepable {

    public int duration;
    public RoleMap role_map;

    public static class RoleMap implements Serializable {
        public int undercover;
        public int people;

        @Override
        public String toString() {
            return "RoleMap{" +
                    "undercover=" + undercover +
                    ", people=" + people +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "VoiceSpyStart{" +
                "duration=" + duration +
                ", role_map=" + role_map +
                '}';
    }
}
