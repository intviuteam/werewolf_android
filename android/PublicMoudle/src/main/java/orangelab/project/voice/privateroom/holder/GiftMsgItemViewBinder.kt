package orangelab.project.voice.privateroom.holder

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.R
import orangelab.project.common.effect.EffectsManager
import orangelab.project.voice.privateroom.PrivateRoomTools
import orangelab.project.voice.privateroom.model.GiftMessageItem
import de.hdodenhof.circleimageview.CircleImageView
import me.drakeet.multitype.ItemViewBinder
import orangelab.project.common.view.VipNameView

/**
 * game_werewolf
 * 2018/3/21 下午6:57
 * Mystery
 */

class GiftMsgItemViewBinder : ItemViewBinder<GiftMessageItem, GiftMsgItemViewBinder.GiftMsgItemViewHolder>() {

    class GiftMsgItemViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var message: TextView
        var position: CircleImageView
        var name: VipNameView
        var gift: ImageView
        var iv: ImageView
        var container: View
        var ivContainer: View

        init {
            message = itemView!!.findViewById(R.id.tv_item_msg) as TextView
            name = itemView!!.findViewById(R.id.tv_item_name) as VipNameView
            position = itemView!!.findViewById(R.id.iv_item_position) as CircleImageView
            gift = itemView!!.findViewById(R.id.iv_item_gift) as ImageView
            ivContainer = itemView!!.findViewById(R.id.ll_msg_container_2)
            container = itemView!!.findViewById(R.id.ll_msg_container);
            iv = itemView!!.findViewById(R.id.msg_iv) as ImageView
        }

    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): GiftMsgItemViewHolder {
        val view = inflater.inflate(R.layout.layout_private_room_gift_msg, parent, false)
        return GiftMsgItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: GiftMsgItemViewHolder, item: GiftMessageItem) {
        holder.name.setText(item.name)
        holder.message.setText(item.message)
        holder.position.setImageResource(PrivateRoomTools.MessageItemPositionIv(item.position))
        if (!TextUtils.isEmpty(item.type)) {
            EffectsManager.FillGiftImageView(item.type, holder.gift)
            holder.container.visibility = View.VISIBLE
            holder.ivContainer.visibility = View.GONE
        }
        if (item.res > 0) {
            holder.iv.setImageResource(item.res)
            holder.ivContainer.visibility = View.VISIBLE
            holder.container.visibility = View.GONE
        }
        if (item.textColorResource != -1) {
            holder.message.setTextColor(holder.message.context.resources.getColor(item.textColorResource))
        }
        holder.name.handleVipInfo(item.vipInfo)
    }
}
