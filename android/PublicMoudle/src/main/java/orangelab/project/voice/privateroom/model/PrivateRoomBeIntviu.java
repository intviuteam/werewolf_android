package orangelab.project.voice.privateroom.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/5/23 下午2:46
 * Mystery
 */
public class PrivateRoomBeIntviu implements Keepable {

    public String invite_user_id;
    public String target_user_id;
    public String room_id;
    public String room_type;
    public String room_pw;
    public PrivateRoomIntviuUser invite_user;

    public static class PrivateRoomIntviuUser implements Keepable {

        public String id;
        public String name;
        public String avatar;
        public int sex;
    }

}
