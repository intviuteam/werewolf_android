package orangelab.project.voice.musiccompany.download;

public interface EntityCallBack {
    void callBack(long count, long current, boolean mustNoticeUI);
}
