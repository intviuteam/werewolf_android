package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * 房间贡献榜实体
 */
public class VoicePopularBean implements Serializable, Keepable {

    public int sortNumber;      //排名
    public String headImg;      //头像
    public String name;         //名字
    public int popularity;      //贡献度
}
