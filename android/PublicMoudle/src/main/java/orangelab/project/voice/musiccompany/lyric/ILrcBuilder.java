package orangelab.project.voice.musiccompany.lyric;


import com.toolkit.action.Keepable;

import java.util.List;

import orangelab.project.voice.musiccompany.lyric.impl.LrcRow;

/**
 * 解析歌词，得到LrcRow的集合
 */
public interface ILrcBuilder extends Keepable {
    List<LrcRow> getLrcRows(String rawLrc);
}
