package orangelab.project.voice.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import com.androidtoolkit.PLog;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.gif.GifImageDecoder;
import orangelab.project.common.effect.gif.GifImageView;

import java.io.ByteArrayInputStream;

/**
 * Created by lloydfinch on 31/10/2017.
 * 播放后，自动停在最后一帧
 */

public class SmallGameView extends GifImageView {

    private byte[] bytes;
    private long playTime = 2000;//播放持续时长
    private long delayTime = 0;//播放完毕延时时长
    private int pauseFrame = -1;//需要停留的帧数
    private long frameDuration = 40;//每帧播放时长 ms
    private SafeHandler safeHandler;
    private OnPlayEndListener onPlayEndListener;

    private Bitmap delayBitmap;

    public SmallGameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SmallGameView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setVisibility(GONE);
        safeHandler = new SafeHandler();
    }

    @Override
    public void setBytes(byte[] bytes) {
        super.setBytes(bytes);
        this.bytes = bytes;
        if (delayBitmap == null) {
            createDelayBitmap();
        }
    }

    //手动设置播放时长需要放在setBytes[]后面
    public void setPlayTime(long playTime) {
        this.playTime = playTime;
    }

    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }

    public void setPauseFrame(int pauseFrame) {
        this.pauseFrame = pauseFrame;
    }

    public void setOnPlayEndListener(OnPlayEndListener onPlayEndListener) {
        this.onPlayEndListener = onPlayEndListener;
    }

    public void setDelayBitmap(Bitmap delayBitmap) {
        this.delayBitmap = delayBitmap;
    }

    public void start() {
        if (playTime > 0) {
            setImageBitmap(null);
            setVisibility(VISIBLE);
            try {
                super.startAnimation();
            } catch (Exception e) {
                PLog.e("SmallGameView", "play too quickly");
            }
        }
        safeHandler.postDelaySafely(() -> {
            stop();
            playTime = 2000;
        }, playTime);
    }

    public void stop() {
        clearAnimation();
        clear();
        onPause();
    }

    private void createDelayBitmap() {
        GifImageDecoder gifImageDecoder = new GifImageDecoder();
        try {
            gifImageDecoder.read(new ByteArrayInputStream(bytes));
            int frameCount = gifImageDecoder.getFrameCount();
            if (pauseFrame > 0 && pauseFrame < frameCount) {
                Bitmap bitmap = gifImageDecoder.getFrame(pauseFrame);
                if (bitmap != null) {
                    delayBitmap = bitmap.copy(Bitmap.Config.ARGB_4444, true);
                }
                pauseFrame = -1;
            }

            computePlayTime(frameCount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //根据帧数计算播放时长
    private void computePlayTime(int frameCount) {
        this.playTime = frameDuration * frameCount;
    }

    //停止时显示的画面
    private void onPause() {
        if (delayBitmap != null) {
            setImageBitmap(delayBitmap);
        }
        safeHandler.postDelaySafely(() -> {
            setVisibility(GONE);
            release();
            if (onPlayEndListener != null) {
                onPlayEndListener.onPlayEnd();
            }
            delayTime = 0;
        }, delayTime);
    }

    private void release() {
        if (delayBitmap != null) {
            delayBitmap.recycle();
            delayBitmap = null;
        }
    }

    public void destroy() {
        clear();
        safeHandler.release();
        bytes = null;
    }

    public interface OnPlayEndListener {
        void onPlayEnd();
    }
}
