package orangelab.project.voice.musiccompany.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.dialog.VoiceLeaveDialog
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager
import orangelab.project.voice.musiccompany.api.MusicSocketHelper
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import orangelab.project.voice.musiccompany.model.MusicOrderSong
import orangelab.project.voice.utils.PositionHelper

/**
 * "歌单"适配器
 */
class MusicSongOrderAdapter(songs: ArrayList<MusicOrderSong>) : RecyclerView.Adapter<MusicSongOrderAdapter.SongOrderViewHolder>() {

    private val TAG = "MusicSongOrderAdapter"

    //所有已排麦歌曲
    private var orderedSongs: ArrayList<MusicOrderSong> = songs

    init {
        PLog.e(TAG, "orderdSongsSize = ${orderedSongs.size}")
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SongOrderViewHolder {
        val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_music_order_song, parent, false)
        return SongOrderViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return orderedSongs.size
    }

    override fun onBindViewHolder(holder: SongOrderViewHolder?, position: Int) {

        if (holder != null) {
            val song: MusicOrderSong = orderedSongs[position]
            holder.tvSongPosition.text = song.position.toString()
            holder.tvSongName.text = "${song.songName}-${song.author}"
            holder.tvSongOrder.text = song.order

            val order = VoiceRoomDataSourceManager.getInstance().findUserById(song.orderId)
            if (order == null) {
                //不在房间
                holder.tvSongOrderState.text = MessageUtils.getString(R.string.str_music_song_no_room)
                holder.tvSongOrderState.setBackgroundResource(R.drawable.bg_song_menu_no_room_tag)
            } else {
                //在房间
                if (PositionHelper.isUpSeat(order.position)) {
                    //在麦上
                    if (PositionHelper.isMaster(order.position)) {
                        //是房主
                        holder.tvSongOrderState.text = MessageUtils.getString(R.string.str_music_song_master)
                        holder.tvSongOrderState.setBackgroundResource(R.drawable.bg_song_menu_master_tag)
                    } else {
                        //不是房主
                        holder.tvSongOrderState.text = MessageUtils.getString(R.string.str_music_song_seat_num, order.position)
                        holder.tvSongOrderState.setBackgroundResource(R.drawable.bg_song_menu_seat_num_tag)
                    }
                } else {
                    //不在麦上
                    holder.tvSongOrderState.text = MessageUtils.getString(R.string.str_music_song_no_seat)
                    holder.tvSongOrderState.setBackgroundResource(R.drawable.bg_song_menu_seat_down_tag)
                }
            }

            PicassoUtils.loadImageByDefault(
                    holder.itemView.context,
                    song.orderImg,
                    holder.ivSongHead,
                    R.mipmap.default_head)

            //删除歌曲
            holder.btnSongRemove.setOnClickListener {
                MusicSocketHelper.INSTANCE.removeSongFromMenu(song.id, song.orderId)
            }

            //根据位置设置"顶" or "唱"
            if (position <= 0) {
                holder.btnSongPeak.setText(R.string.str_music_song_sing)
                holder.btnSongPeak.setBackgroundResource(R.drawable.bg_music_song_menu_sing_tag)
                holder.btnSongPeak.setOnClickListener {

                    if (!PositionHelper.inRoom(song.orderId)) {
                        //不在房间
                        ToastToolKit.showShort(R.string.str_voice_already_leave)
                    } else {
                        if (!PositionHelper.isUpSeat(song.orderId)) {
                            //在房间未上麦
                            ToastToolKit.showShort(R.string.str_music_not_in_seat)
                        } else {

                            //在房间在麦上有人唱
                            if (MusicCompanyConfig.isSing) {
                                val dialog = VoiceLeaveDialog(holder.btnSongPeak.context,
                                        MessageUtils.getString(R.string.str_music_tip),
                                        MessageUtils.getString(R.string.str_music_if_over))
                                dialog.setConfirmClickListener {
                                    dialog.dismiss()
                                    MusicSocketHelper.INSTANCE.sendAuthorizeSing(song.orderId)
                                }
                                dialog.negative(true)
                                dialog.show()
                            } else {
                                //在房间在麦上无人唱
                                MusicSocketHelper.INSTANCE.sendAuthorizeSing(song.orderId)
                            }
                        }
                    }
                }
            } else {
                holder.btnSongPeak.setText(R.string.str_music_song_peak)
                holder.btnSongPeak.setBackgroundResource(R.drawable.bg_music_song_menu_peak_tag)
                holder.btnSongPeak.setOnClickListener {
                    MusicSocketHelper.INSTANCE.peakSong(song.id, song.orderId)
                }
            }

            //设置可见性
            //房主可见所有的歌曲的"顶"、"删"
            if (PositionHelper.isMaster()) {
                holder.btnSongPeak.visibility = View.VISIBLE
                holder.btnSongRemove.visibility = View.VISIBLE
            }
            //非房主，只能见自己歌曲的"删"
            if (!PositionHelper.isMaster()) {
                holder.btnSongPeak.visibility = View.INVISIBLE
                if (PositionHelper.isSelf(song.orderId)) {
                    holder.btnSongRemove.visibility = View.VISIBLE
                } else {
                    holder.btnSongRemove.visibility = View.INVISIBLE
                }
            }

        } else {
            PLog.e(TAG, "onBindViewHolder holder is null!")
        }
    }

    /**
     * 刷新数据
     */
    fun refreshData(datas: ArrayList<MusicOrderSong>) {
        this.orderedSongs.clear()
        this.orderedSongs.addAll(datas)

        PLog.e(TAG, "orderdSongsSize = ${orderedSongs.size}")
        notifyDataSetChanged()
    }

    inner class SongOrderViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        //标号
        var tvSongPosition: TextView = itemView?.findViewById(R.id.tv_order_song_number) as TextView
        //头像
        var ivSongHead: ImageView = itemView?.findViewById(R.id.iv_order_song_head) as ImageView
        //歌名-原唱
        var tvSongName: TextView = itemView?.findViewById(R.id.tv_order_song_song_name) as TextView
        //点歌者
        var tvSongOrder: TextView = itemView?.findViewById(R.id.tv_order_song_order_name) as TextView
        //点歌人状态
        var tvSongOrderState: TextView = itemView?.findViewById(R.id.tv_order_song_seat_status) as TextView
        //顶/唱
        var btnSongPeak: Button = itemView?.findViewById(R.id.btn_music_menu_peak) as Button
        //删
        var btnSongRemove: Button = itemView?.findViewById(R.id.btn_music_menu_remove) as Button
    }

}
