package orangelab.project.voice.api;

import android.content.Context;
import android.text.TextUtils;

import cn.intviu.support.SharePreStoreUtils;

/**
 * Created by lloydfinch on 28/11/2017.
 * <p>
 * 语音房API 常量
 */

public final class VoiceApiDefines {

    public static final String REQUEST_NAME = "REQUEST_NAME";
    public static final String REQUEST_URL_KEY = "REQUEST_URL";
    public static final String REQUEST_AUDIO_URL_KEY = "REQUEST_AUDIO_URL";

    public static String HTTP_URL = "http://staging.intviu.cn:8200";
    public static String WEBSOCKET_URL = "ws://staging.intviu.cn:8200";
    public static String AUDIO_HTTP_URL = HTTP_URL;

    public static final String URL_TYPE_AUDIO = "type_audio";
    public static final String URL_TYPE_DEFAULT = "type_default";
    public static final String URL_VOICE_EMOTIONS = "/config/audio_emoticon";

    //share_audio替换成share_xiaoyu(2017-12-18)
    public static final String SHARE_AUDIO_URL = "/share_xiaoyu/?roomid=%s&type=xiaoyu&password=%s";
    public static final String SHARE_AUDIO_URL_WEREWOLF = "/share_audio/?roomid=%s&type=audio&password=%s";
    public static final String SHARE_AUDIO_URL_KUAIWAN = "/share_audio_kuaiwan/?roomid=%s&type=audio&password=%s";
    public static final String SHARE_MINI_GAME = "/share/game?";

    public static void init(Context context) {
        String tempurl = SharePreStoreUtils.getString(REQUEST_NAME, context, REQUEST_URL_KEY);
        if (TextUtils.isEmpty(tempurl)) {
            return;
        }
        HTTP_URL = tempurl;
        String tempAudioUrl = SharePreStoreUtils.getString(REQUEST_NAME, context, REQUEST_AUDIO_URL_KEY);
        if (TextUtils.isEmpty(tempAudioUrl)) {
            return;
        }
        AUDIO_HTTP_URL = tempAudioUrl;
    }
}
