package orangelab.project.voice.privateroom.model

import orangelab.project.common.model.UserVipInfo

/**
 *game_werewolf
 *2018/4/1 下午6:01
 *Mystery
 */
class TempPictureMessageItem {

    var name: String
    var position: Int

    var countdown: Long
    var picture: String
    var time: Long
    var isSelfUpload = false //此消息是否是正在上传图片
    var progress = 0
    var vipInfo: UserVipInfo? = null

    constructor(name: String, position: Int, countdown: Long, picture: String, time: Long)
            : this(name, position, countdown, picture, time, false)

    constructor(name: String, position: Int, countdown: Long, picture: String, time: Long, isSelfUpload: Boolean) {
        this.name = name
        this.position = position
        this.countdown = countdown
        this.picture = picture
        this.time = time
        this.isSelfUpload = isSelfUpload
    }


}