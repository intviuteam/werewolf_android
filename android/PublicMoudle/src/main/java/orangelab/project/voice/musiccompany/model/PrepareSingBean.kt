package orangelab.project.voice.musiccompany.model

import com.toolkit.action.Keepable
import java.io.Serializable

class PrepareSingBean : Keepable, Serializable {

    var song: PrepareSong? = null


    class PrepareSong : Keepable, Serializable {
        var music_id = ""
        var author_name = ""
        var img = ""
        var timelength = ""
        var lyric_url = ""
        var song_name = ""
        var play_url = ""
        var booker = ""
        var room_id = ""//此歌曲所在的tencent的roomid
        var booker_name = ""
        var booker_avatar = ""
        var start_time: Long = 0  //开始时间

        override fun toString(): String {
            return "PrepareSong(music_id='$music_id', author_name='$author_name', img='$img', timelength='$timelength', lyric_url='$lyric_url', song_name='$song_name', play_url='$play_url', booker='$booker', room_id='$room_id', booker_name='$booker_name')"
        }


    }

    override fun toString(): String {
        return "PrepareSingBean(song=$song)"
    }


}