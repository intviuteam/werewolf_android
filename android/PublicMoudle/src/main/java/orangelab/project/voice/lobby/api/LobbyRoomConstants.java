package orangelab.project.voice.lobby.api;

public interface LobbyRoomConstants {

    String LOBBY_GAME_INVITE = "invite_game"; //发送游戏邀请信息
    String LOBBY_GAME_ACCEPT = "accept_game"; //发送接受游戏邀请信息
    String LOBBY_GAME_GAME = "game";

    String TYPE = "type";
    String LOBBY_GAME_NAME = "name";
    String LOBBY_GAME_ICON = "icon";
    String SEND_ID = "send_id";

    String INVITE_ID = "invite_id";
    String USER_ID = "";
    String LOBBY_GAME_TIME_OUT = "";

    String PLAY_MINI_GAME = "play_mini_game";
    String INVITE = "invite";
    String ACCEPT_INVITE = "accept_invite";
    String GAME_TYPE = "game_type";
    String WAIT_TIMER = "wait_timer";
    String INVITE_USER_ID = "invite_user_id";
    String TARGET_USER_ID = "target_user_id";
    String PLAY_MINI_GAME_SERVER = "play_mini_game_server";
    String INVITED_SUCCESS = "invited_success";
    String UUID = "uuid";
    String UPDATE_USER_STATUS = "update_user_status"; //no egg use in lobby currently


    String PLAY_GAME = "play_game";
    String JOIN_LOBBY = "JOIN_LOBBY";


}
