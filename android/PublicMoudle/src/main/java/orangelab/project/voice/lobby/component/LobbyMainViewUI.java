package orangelab.project.voice.lobby.component;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListView;

import com.R;
import com.toolkit.action.Destroyable;

import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.dialog.VoiceChangeTitleDialog;
import orangelab.project.voice.gif.BasicComponent;


/**
 * Created by lloydfinch on 25/09/2017.
 */

public class LobbyMainViewUI extends BasicComponent implements Destroyable {

    private ViewGroup contentView;

    private ListView listMsg;
    private CheckBox checkBoxHiddenMessage;

    private Context mContext;

    public LobbyMainViewUI(Context mContext, ViewGroup contentView) {
        this.mContext = mContext;
        this.contentView = contentView;
        init();
    }

    private void init() {
        initView();
        initListener();
        initShow();
    }

    private void initView() {
        listMsg = (ListView) contentView.findViewById(R.id.list_lobby_message);
        checkBoxHiddenMessage = (CheckBox) contentView.findViewById(R.id.check_lobby_hide_message);
    }

    private void initListener() {
    }

    private void initShow() {
        checkBoxHiddenMessage.setChecked(VoiceRoomConfig.isIsHiddenMsg());
    }

    private void destroyDialog() {
        if (changeTitleDialog != null) {
            if (changeTitleDialog.isShowing()) {
                changeTitleDialog.dismiss();
            }
            changeTitleDialog.destroy();
            changeTitleDialog = null;
        }
    }

    private VoiceChangeTitleDialog changeTitleDialog;

    public void showChangeTitleUI(String title, String type) {
        changeTitleDialog = new VoiceChangeTitleDialog(mContext, title, type);
        changeTitleDialog.show();
    }

    public ViewGroup findChairUIById(int id) {
        return (ViewGroup) contentView.findViewById(id);
    }

    public ListView getListMsg() {
        return listMsg;
    }

    public CheckBox getCheckBoxHiddenMessage() {
        return checkBoxHiddenMessage;
    }

    @Override
    public void destroy() {
        super.destroy();
        destroyDialog();
    }
}
