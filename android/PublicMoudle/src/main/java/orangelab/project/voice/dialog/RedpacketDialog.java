package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.ToastToolKit;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.voice.activity.RedpacketActivity;
import orangelab.project.voice.adapter.RedPacketHistoryAdapter;
import orangelab.project.voice.adapter.RedPacketInfoAdapter;
import orangelab.project.voice.api.VoiceApiManager;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.model.VoiceRedPacketInfoBean;
import orangelab.project.voice.model.VoiceRedPacketListBean;

/**
 * Created by hwx on 11/07/2018.
 */

public class RedpacketDialog extends SafeDialog implements Destroyable {

    private static final String TAG = "RedpacketDialog";
    private Context mContext;

    private EditText mInputDiamondEditText;
    private EditText mInputPacketEditText;
    private TextView mTipsDiamond;
    private TextView mTipsRedpacketFirst;
    private TextView mTipsRedpacketSecond;
    private TextView mTipsRedpacketRemainTime;

    private View mSendRedpacket;
    private View mRedpacketHistory;

    private View mDiamondNumPlus;
    private View mDiamondNumMinus;
    private View mPacketNumPlus;
    private View mPacketNumMinus;

    private View mCancel;
    private View mSend;

    private RedPacketHistoryAdapter mRedPacketHistoryAdapter;
    private RedPacketInfoAdapter mRedPacketInfoAdapter;

    private long mDiamondNum;
    private long mPacketNum;

    private int mInputMinDiamondNum;
    private int mInputMinPacketNum;
    private int mInputMaxPacketNum;
    private int mDiamondRemainTime;

    private static final int DEFAULT_PACKET = 5;

    //记录当前红包历史页数：红包历史需要分页请求，每页请求20条
    private static int mHistoryItemPage;

    public RedpacketDialog(@NonNull Context context) {
        this(context, R.style.DarkDialog);
        this.mContext = context;
        EventBus.getDefault().register(this);
    }

    private RedpacketDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        mInputMinDiamondNum = (VoiceRoomConfig.getMinDiamond() == 0) ? 50 : VoiceRoomConfig.getMinDiamond();
        mInputMinPacketNum = (VoiceRoomConfig.getMinPacket() == 0) ? 1 : VoiceRoomConfig.getMinPacket();
        mInputMaxPacketNum = (VoiceRoomConfig.getMaxPacket() == 0) ? 200 : VoiceRoomConfig.getMaxPacket();
        mDiamondRemainTime = (VoiceRoomConfig.getRemainTime() == 0) ? 1 : VoiceRoomConfig.getRemainTime();
        initSendRedpacket();
        WereWolfDialogDecorate.DecorateDialog(getWindow());
    }

    private void initSendRedpacket() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_redpacket, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);

        mRedpacketHistory = contentView.findViewById(R.id.red_packet_history);
        mRedPacketHistoryAdapter = null;
        mInputDiamondEditText = (EditText) contentView.findViewById(R.id.input_text_diamond_num);
        mInputPacketEditText = (EditText) contentView.findViewById(R.id.input_text_packet_num);
        mInputDiamondEditText.setText(String.valueOf(mInputMinDiamondNum));
        mInputPacketEditText.setText(String.valueOf(Math.max(DEFAULT_PACKET,mInputMinPacketNum)));
        mDiamondNum = mInputMinDiamondNum;
        mPacketNum = Math.max(DEFAULT_PACKET,mInputMinPacketNum);

        mDiamondNumPlus = contentView.findViewById(R.id.plus_diamond);
        mDiamondNumMinus = contentView.findViewById(R.id.minus_diamond);
        mPacketNumPlus = contentView.findViewById(R.id.plus_red_packet);
        mPacketNumMinus = contentView.findViewById(R.id.minus_red_packet);

        mTipsDiamond = contentView.findViewById(R.id.tips1);
        mTipsRedpacketFirst = contentView.findViewById(R.id.tips2);
        mTipsRedpacketSecond = contentView.findViewById(R.id.tips3);
        mTipsRedpacketRemainTime = contentView.findViewById(R.id.tips5);
        mTipsRedpacketRemainTime.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_tip5), mDiamondRemainTime));

        mCancel = contentView.findViewById(R.id.btn_cancel);
        mSend = contentView.findViewById(R.id.btn_send);

        mRedPacketHistoryAdapter = null;
        mRedPacketInfoAdapter = null;
        setContentView(contentView);

        initSendRedpacketListeners();
    }

    //初始化红包历史列表
    private void initRedpacketHistory(VoiceRedPacketListBean result) {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_redpacket_history, null);
        ListView history_list = (ListView) contentView.findViewById(R.id.history_list);
        mRedPacketHistoryAdapter = new RedPacketHistoryAdapter(mContext, result);
        history_list.setAdapter(mRedPacketHistoryAdapter);

        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        mSendRedpacket = contentView.findViewById(R.id.red_packet_send);

        mRedPacketInfoAdapter = null;
        initRedpacketHistoryListeners(history_list, result.pages);
        setContentView(contentView);
    }

    //初始化红包详情列表
    private void initRedpacketInfo(VoiceRedPacketInfoBean result) {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_redpacket_info, null);

        ListView history_list = (ListView) contentView.findViewById(R.id.info_list);
        mRedPacketInfoAdapter = new RedPacketInfoAdapter(mContext, result);
        history_list.setAdapter(mRedPacketInfoAdapter);

        TextView senderTitle = contentView.findViewById(R.id.red_packet_info_title);
        TextView packetStatus = contentView.findViewById(R.id.red_packet_info);
        senderTitle.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_someone), result.sender.name));
        packetStatus.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_number_receive), result.left, result.count));

        mRedPacketHistoryAdapter = null;
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);
    }

    private void initSendRedpacketListeners() {
        //输入钻石数目
        mInputDiamondEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                //钻石数最少为0
                if (text.equals("")) {
                    mDiamondNum = 0;
                    mInputDiamondEditText.setText(String.valueOf(mDiamondNum));
                } else {
                    //钻石最大值为Long.MAX_VALUE
                    try {
                        mDiamondNum = Long.parseLong(text);
                    } catch (NumberFormatException e) {
                        mInputDiamondEditText.setText(String.valueOf(Long.MAX_VALUE));
                        mInputDiamondEditText.setSelection(String.valueOf(Long.MAX_VALUE).length());
                        mDiamondNum = Long.MAX_VALUE;
                    }
                }
                PLog.d(TAG, "input diamondNum = " + mDiamondNum);
                //刷新提示显示状态
                showDiamondTips();
                showRedpacketTips();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        //输入红包个数
        mInputPacketEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                //不小于红包数最少
                if (text.equals("") || Long.parseLong(text) < mInputMinPacketNum) {
                    mPacketNum = mInputMinPacketNum;
                    mInputPacketEditText.setText(String.valueOf(mPacketNum));
                } else {
                    //红包数最大值为Long.MAX_VALUE
                    try {
                        mPacketNum = Long.parseLong(text);
                    } catch (NumberFormatException e) {
                        mInputPacketEditText.setText(String.valueOf(Long.MAX_VALUE));
                        mInputPacketEditText.setSelection(String.valueOf(Long.MAX_VALUE).length());
                        mPacketNum = Long.MAX_VALUE;
                    }
                }
                PLog.d(TAG, "input packetNum = " + mPacketNum);
                //刷新提示显示状态
                showRedpacketTips();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        //红包历史
        mRedpacketHistory.setOnClickListener(v -> {
            if (Utils.judgeConnectStatusWithToast(MessageUtils.getString(R.string.network_is_unavailable))) {
                //首次固定请求第1页，20条
                VoiceApiManager.GetRedPacketHistoryList(VoiceRoomConfig.getRoomId(), VoiceRoomConfig.getEnterTime(), 1, 20, (result, e) -> {
                            runSafely(() -> {
                                if (e == null) {
                                    initRedpacketHistory(result);
                                    mHistoryItemPage = 1;
                                } else {
                                    ToastToolKit.showShort(e.getMessage());
                                }
                            });
                        }
                );
            }
        });

        mDiamondNumPlus.setOnClickListener(v -> {
            String numString = mInputDiamondEditText.getText().toString();
            try {
                mDiamondNum = Long.valueOf(numString);
                if (mDiamondNum <= Long.MAX_VALUE - 10) {
                    mDiamondNum = mDiamondNum + 10;
                }
                mInputDiamondEditText.setText(String.valueOf(mDiamondNum));
                mInputDiamondEditText.setSelection(String.valueOf(mDiamondNum).length());
            } catch (NumberFormatException e) {
                mDiamondNum = Long.MAX_VALUE;
                mInputDiamondEditText.setText(String.valueOf(mDiamondNum));
                mInputDiamondEditText.setSelection(String.valueOf(mDiamondNum).length());
            }
        });
        mDiamondNumMinus.setOnClickListener(v -> {
            String numString = mInputDiamondEditText.getText().toString();
            mDiamondNum = Long.valueOf(numString);
            if (mDiamondNum > 10) {
                long temp = mDiamondNum - 10;
                mInputDiamondEditText.setText(String.valueOf(temp));
                mInputDiamondEditText.setSelection(String.valueOf(temp).length());
            } else {
                mInputDiamondEditText.setText("0");
                mInputDiamondEditText.setSelection(String.valueOf("0").length());
            }
        });
        mPacketNumPlus.setOnClickListener(v -> {
            String numString = mInputPacketEditText.getText().toString();
            try {
                mPacketNum = Long.valueOf(numString);
                if (mPacketNum <= Long.MAX_VALUE - 1) {
                    mPacketNum = mPacketNum + 1;
                }
                mInputPacketEditText.setText(String.valueOf(mPacketNum));
                mInputPacketEditText.setSelection(String.valueOf(mPacketNum).length());
            } catch (NumberFormatException e) {
                mPacketNum = Long.MAX_VALUE;
                mInputPacketEditText.setText(String.valueOf(mPacketNum));
                mInputPacketEditText.setSelection(String.valueOf(mPacketNum).length());
            }
        });
        mPacketNumMinus.setOnClickListener(v -> {
            String numString = mInputPacketEditText.getText().toString();
            mPacketNum = Long.valueOf(numString);
            if (mPacketNum > mInputMinPacketNum) {
                long temp = mPacketNum - 1;
                mInputPacketEditText.setText(String.valueOf(temp));
                mInputPacketEditText.setSelection(String.valueOf(temp).length());
            }
        });

        mSend.setOnClickListener(v -> {
            //是否当前有红色字的tips提示
            boolean tipNotShowed = !showDiamondTips() && !showRedpacketTips();
            if (tipNotShowed) {
                if (Utils.judgeConnectStatusWithToast(MessageUtils.getString(R.string.network_is_unavailable))) {
                    VoiceSocketHelper.sendRedPackage(mDiamondNum, mPacketNum, result -> {
                        runSafely(this::dismiss);
                    }, result -> {
                        runSafely(() -> {
                            Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
                        });
                    });
                }
            } else {
                //有tips提示显示，则无法发送并使用动画再次提示
                animationForTips();
            }
        });
        mCancel.setOnClickListener(v -> {
            dismiss();
        });
    }

    private void initRedpacketHistoryListeners(ListView lv, int pageNum) {
        //发红包(title)
        mSendRedpacket.setOnClickListener(v -> initSendRedpacket());
        //红包历史list滑动到底，如果还有数据则继续请求
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && mRedPacketHistoryAdapter != null && lv != null && mHistoryItemPage < pageNum) {
                    int lastVisiblePosition = lv.getLastVisiblePosition();
                    if (lastVisiblePosition == mRedPacketHistoryAdapter.getCount() - 1) {
                        VoiceApiManager.GetRedPacketHistoryList(VoiceRoomConfig.getRoomId(), VoiceRoomConfig.getEnterTime(), mHistoryItemPage + 1, 20, (result, e) -> {
                                    runSafely(() -> {
                                        if (e == null) {
                                            mRedPacketHistoryAdapter.add(result);
                                            mHistoryItemPage = result.page;
                                            PLog.d(TAG, "packet history list page after scroll to bottom is " + mHistoryItemPage);
                                        } else {
                                            ToastToolKit.showShort(e.getMessage());
                                        }
                                    });
                                }
                        );
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
    }

    private boolean showDiamondTips() {
        boolean isDiamondTipsShowed = true;
        if (mDiamondNum < mInputMinDiamondNum) {
            mTipsDiamond.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_tip1), mInputMinDiamondNum));
        } else {
            //不设置visible gone来占位
            mTipsDiamond.setText("");
            isDiamondTipsShowed = false;
        }
        return isDiamondTipsShowed;
    }

    private boolean showRedpacketTips() {
        boolean isPacketTipShowed;
        if (mPacketNum > mInputMaxPacketNum) {
            mTipsRedpacketFirst.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_tip2), mInputMaxPacketNum));
            isPacketTipShowed = true;
        } else {
            //不设置visible gone来占位
            mTipsRedpacketFirst.setText("");
            isPacketTipShowed = false;
        }

        //红包个数大于钻石数目
        if (mPacketNum > mDiamondNum) {
            if (isPacketTipShowed) {
                mTipsRedpacketSecond.setText(R.string.str_red_packet_tip3);
            } else {
                mTipsRedpacketFirst.setText(R.string.str_red_packet_tip3);
                mTipsRedpacketSecond.setText("");
                isPacketTipShowed = true;
            }
        } else {
            if (isPacketTipShowed) {
                mTipsRedpacketSecond.setText("");
            } else {
                mTipsRedpacketFirst.setText("");
                mTipsRedpacketSecond.setText("");
                isPacketTipShowed = false;
            }
        }
        return isPacketTipShowed;
    }

    private void animationForTips() {
        if (mTipsDiamond != null && mTipsRedpacketFirst != null && mTipsRedpacketSecond != null) {
            mTipsDiamond.setAlpha(0f);
            mTipsDiamond.animate().alpha(1f).setDuration(500);
            mTipsRedpacketFirst.setAlpha(0f);
            mTipsRedpacketFirst.animate().alpha(1f).setDuration(500);
            mTipsRedpacketSecond.setAlpha(0f);
            mTipsRedpacketSecond.animate().alpha(1f).setDuration(500);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.RedPacketHistoryListEvent event) {
        //红包历史点击“领红包”后，返回时刷新红包历史
        if (event.refresh) {
            if (mRedPacketHistoryAdapter != null) {
                //为了防止1次请求太多数据(例如房间里有1000个红包历史，我抢的是列表最后的红包，这里不刷新1000条的数据，还是请求前20条)，回到列表首页
                VoiceApiManager.GetRedPacketHistoryList(VoiceRoomConfig.getRoomId(), VoiceRoomConfig.getEnterTime(), 1, 20, (result, e) -> {
                            runSafely(() -> {
                                if (e == null) {
                                    mRedPacketHistoryAdapter.update(result);
                                } else {
                                    ToastToolKit.showShort(e.getMessage());
                                }
                            });
                        }
                );
            }
            //如果刷新操作，则不进行下边的处理，返回
            return;
        }
        //红包历史页面点击“领红包”
        if (event.canGrab) {
            VoiceRoomConfig.setLastRedpacketId(event.packetId);
            RedpacketActivity.Launch(mContext);
            //红包历史页面点击“查看”
        } else {
            if (Utils.judgeConnectStatusWithToast(MessageUtils.getString(R.string.network_is_unavailable))) {
                VoiceApiManager.GetRedPacketInfoList(event.packetId, (result, e) -> {
                            runSafely(() -> {
                                if (e == null) {
                                    initRedpacketInfo(result);
                                } else {
                                    ToastToolKit.showShort(e.getMessage());
                                }
                            });
                        }
                );
            }
        }
    }

    @Override
    protected void release() {
        mRedPacketHistoryAdapter = null;
        mRedPacketInfoAdapter = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void destroy() {
    }
}
