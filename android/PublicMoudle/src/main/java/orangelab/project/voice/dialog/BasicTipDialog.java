package orangelab.project.voice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.R;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 12/10/2017.
 */

public class BasicTipDialog extends Dialog {

    private LinearLayout llContentViewContainer;
    private TextView tvTitle;
    private ImageView ivImage;
    private TextView tvMsg;
    private View btnSure;
    private View btnCancel;

    private Context mContext;

    public BasicTipDialog(@NonNull Context context) {
        this(context, R.style.DarkDialog);
    }

    public BasicTipDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_basic_tip, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llContentViewContainer = (LinearLayout) contentView.findViewById(R.id.ll_content_view_container);
        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        tvMsg = (TextView) contentView.findViewById(R.id.tv_msg);
        ivImage = (ImageView) contentView.findViewById(R.id.iv_image);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(300);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void changeContentView(int layoutResourceId) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(LayoutInflater.from(mContext).inflate(layoutResourceId, null));
    }

    public void changeContentView(View contentView) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(contentView);
    }

    public BasicTipDialog positive() {
        btnSure.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.GONE);
        return this;
    }

    public BasicTipDialog negative() {
        btnSure.setVisibility(View.GONE);
        btnCancel.setVisibility(View.VISIBLE);
        return this;
    }

    public BasicTipDialog twoButton() {
        btnSure.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);
        return this;
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> dismiss());
        btnCancel.setOnClickListener(v -> dismiss());
    }

    public BasicTipDialog setTitle(String title) {
        tvTitle.setText(title);
        return this;
    }

    public BasicTipDialog setMsg(String msg) {
        tvMsg.setText(msg);
        return this;
    }

    public BasicTipDialog setImage(int imageId) {
        ivImage.setImageResource(imageId);
        return this;
    }

    public BasicTipDialog setImage(Bitmap bitmap) {
        ivImage.setImageBitmap(bitmap);
        return this;
    }
}
