package orangelab.project.voice.utils;

import android.text.TextUtils;

import com.datasource.GlobalUserState;

import javax.annotation.Nonnull;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.db.entity.MusicDataEntity;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.game.component.GameMember;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.musiccompany.model.MusicOrderSong;
import orangelab.project.voice.musiccompany.model.PrepareSingBean;

/**
 * Created by lloydfinch on 26/09/2017.
 * class转换器
 */

public final class TransferClassHelper {

    public static EnterRoomResult.EnterRoomUserItem EnterResult2EnterRoomUser(@Nonnull ServerMessageEnterResult item) {
        return item.user;
    }

    public static GameMember.GameMemberData EnterRoomUser2GameMemberData(@Nonnull EnterRoomResult.EnterRoomUserItem member) {
        GameMember.GameMemberData gameMember = new GameMember.GameMemberData();
        gameMember.userId = member.id;
        gameMember.userName = member.name;
        gameMember.userSex = member.sex;
        gameMember.position = member.position;
        gameMember.userLevel = String.valueOf(member.level);
        gameMember.userImage = member.avatar;
        return gameMember;
    }

    public static PersonalData EnterRoomItem2PersonalData(@Nonnull EnterRoomResult.EnterRoomUserItem member) {

        //只处理需要的
        PersonalData personalData = new PersonalData();
        personalData.setUserId(member.id);
        personalData.setUserName(member.name);
        personalData.setOut(member.isOut);
        personalData.setUserSex(member.sex);
        personalData.setSelfPosition(member.position);
        personalData.setOffline(member.is_disconnected);
        personalData.setPrepared(member.prepared);
        personalData.setSpeaking(member.speaking);
        personalData.setImageUrl(member.avatar);
        personalData.setCanSpeak(member.canSpeak);
        personalData.setBanMic(TextUtils.equals(VoiceConstants.LIMIT, member.state));
        personalData.setFreeStyle(TextUtils.equals(VoiceConstants.FREE, member.state));
        personalData.setUid(member.uid);

        return personalData;
    }

    //server消息转换为语音房处理的消息 start=====================
    public static VoiceMessageBean ServerMessage2VoiceMessage(@Nonnull ServerMessageEvent event) {
        return new VoiceMessageBean(event.code, event.msg_id, event.type, event.payload, event.message);
    }

    public static VoiceMessageBean ServerResponse2VoiceMessage(@Nonnull ServerResponseEvent event) {
        return new VoiceMessageBean(event.code, event.msg_id, event.type, event.payload, event.message);
    }

    public static VoiceMessageBean ServerMessageOver2VoiceMessage(@Nonnull ServerMessageEventOver event) {
        return new VoiceMessageBean(event.code, event.msg_id, event.type, event.payload, event.message);
    }

    public static VoiceMessageBean ServerResponseOver2VoiceMessage(@Nonnull ServerResponseEventOver event) {
        return new VoiceMessageBean(event.code, event.msg_id, event.type, event.payload, event.message);
    }
    //server消息转换为语音房处理的消息 end=====================


    //ktv相关实体转换 start==================================
    public static MusicOrderSong MusicDataEntity2MusicOrderSong(MusicDataEntity entity) {

        //仅用于自己点歌时转换
        MusicOrderSong song = new MusicOrderSong();
        song.id = entity.getId();
        song.headImg = entity.getImg();
        song.author = entity.getAuthor_name();
        song.songName = entity.getSong_name();
        song.order = GlobalUserState.getGlobalState().getUserName();
        song.orderId = GlobalUserState.getGlobalState().getUserId();
        song.orderPosition = PersonalDataHelper.getPersonalData().getSelfPosition();
        song.lyricUrl = entity.getLyric_url();
        song.playUrl = entity.getPlay_url();
        song.orderImg = GlobalUserState.getGlobalState().getUserIcon();

        return song;
    }

    public static MusicOrderSong MusicPrepareSong2MusicOrderSong(PrepareSingBean bean) {
        MusicOrderSong song = new MusicOrderSong();
        PrepareSingBean.PrepareSong temp = bean.getSong();
        if (temp != null) {
            song = MusicPrepareSong2MusicOrderSong(temp);
        }

        return song;
    }

    public static MusicOrderSong MusicPrepareSong2MusicOrderSong(PrepareSingBean.PrepareSong temp) {
        MusicOrderSong song = new MusicOrderSong();
        if (temp != null) {
            song.id = temp.getMusic_id();
            song.orderId = temp.getBooker();
            song.headImg = temp.getImg();
            song.author = temp.getAuthor_name();
            song.songName = temp.getSong_name();
            song.lyricUrl = temp.getLyric_url();
            song.playUrl = temp.getPlay_url();
            song.order = temp.getBooker_name();
            song.orderImg = temp.getBooker_avatar();
        }

        return song;
    }
    //ktv相关实体转换 end==================================
}
