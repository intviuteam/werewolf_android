package orangelab.project.voice.lobby.api

import com.androidtoolkit.PLog
import orangelab.project.MainApplication
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.engine.task.SocketCallBack
import orangelab.project.spyroom.data.SpyRoomConstants
import orangelab.project.voice.constants.VoiceConstants
import org.json.JSONException
import org.json.JSONObject

/**
 * 谁是卧底房消息发送器
 */
object LobbyRoomSocketHelper {

    /**
     * 发送socket消息
     */
    fun nativeRequestMessage(type: String, payload: JSONObject) {
        nativeRequestMessage(type, payload, null, null)
    }

    /**
     * 发送socket消息，带有callback
     */
    fun nativeRequestMessage(type: String, payload: JSONObject?, success: SocketCallBack?, failed: SocketCallBack?) {
        MainApplication.getInstance().socketEngineHelper.nativeRequestMessage(type, payload, success, failed)
    }

    /**
     * 发送游戏邀请信息/大厅消息展示
     */
    fun sendInviteGame(gameType: String, gameName: String, gameIcon: String, uuid: String) {
        val json = JSONObject()
        try {
            val json2 = JSONObject()
            json2.put(LobbyRoomConstants.TYPE, gameType)
            json2.put(LobbyRoomConstants.LOBBY_GAME_NAME, gameName)
            json2.put(LobbyRoomConstants.LOBBY_GAME_ICON, gameIcon)
            json2.put(LobbyRoomConstants.UUID, uuid)
            json.put(LobbyRoomConstants.LOBBY_GAME_GAME, json2)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        nativeRequestMessage(LobbyRoomConstants.LOBBY_GAME_INVITE, json)
    }

    /**
     * 发送游戏接收消息/大厅消息展示
     */
    fun sendAcceptGame(id: String, gameType: String, sendId: String, uuid: String) {
        val json = JSONObject()
        try {
            json.put(LobbyRoomConstants.INVITE_ID, id)
            json.put(LobbyRoomConstants.TYPE, gameType)
            json.put(LobbyRoomConstants.SEND_ID, sendId)
            json.put(LobbyRoomConstants.UUID, uuid)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        nativeRequestMessage(LobbyRoomConstants.LOBBY_GAME_ACCEPT, json)
    }

    /**
     * 发送游戏邀请信息/小游戏逻辑//global去做
     */
    fun sendInvite(gameType: String, duration: Long) {
        val json = JSONObject()
        try {
            json.put(LobbyRoomConstants.TYPE, LobbyRoomConstants.INVITE)
            json.put(LobbyRoomConstants.GAME_TYPE, gameType)
            json.put(LobbyRoomConstants.WAIT_TIMER, duration)
        } catch (e: JSONException) {
            PLog.e("sendInvite", "error occur, error is ${e.message}")
            e.printStackTrace()
        }
        nativeRequestMessage(LobbyRoomConstants.PLAY_MINI_GAME, json)
    }

    /**
     * 发送游戏接受信息/小游戏逻辑//global去做
     */
    fun sendAccept(gameType: String, id: String, uuid: String) {
        val json = JSONObject()
        try {
            json.put(LobbyRoomConstants.TYPE, LobbyRoomConstants.ACCEPT_INVITE)
            json.put(LobbyRoomConstants.GAME_TYPE, gameType)
            json.put(LobbyRoomConstants.INVITE_USER_ID, id)
            json.put(LobbyRoomConstants.UUID, uuid)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        nativeRequestMessage(LobbyRoomConstants.PLAY_MINI_GAME, json)
    }

    /**
     * 发送离开消息
     */
    fun sendLeave(type: String) {
        var json = JSONObject()
        try {
            json.put(VoiceConstants.TYPE, type)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        nativeRequestMessage(VoiceConstants.LEAVE, json)
    }
}