package orangelab.project.voice.api;

import org.json.JSONException;
import org.json.JSONObject;

import orangelab.project.MainApplication;
import orangelab.project.common.engine.task.SocketCallBack;
import orangelab.project.voice.constants.VoiceConstants;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * 处理语音房socket相关
 */

public enum VoiceSocketHelper {

    INSTANCE;

    VoiceSocketHelper() {
    }

    public void nativeRequestMessage(final String type, JSONObject payload) {
        nativeRequestMessage(type, payload, null, null);
    }

    public void nativeRequestMessage(final String type, JSONObject payload, final SocketCallBack
            success, final SocketCallBack failed) {
        MainApplication.getInstance().getSocketEngineHelper().nativeRequestMessage(type, payload, success, failed);
    }

    //"谁是卧底"开始设置
    public static void sendSpySettingStart() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_START_CONFIG, jsonObject);
    }

    //"谁是卧底"结束设置
    public static void sendSpySettingStop() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_STOP_CONFIG, jsonObject);
    }

    //"谁是卧底"更新设置
    public static void sendSpySettingUpdate(boolean addSpy, boolean setSpyToGhost, boolean canInsertMic, boolean canGuessWord, String firstWord, String secondWord) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.SPY_MORE_UNDERCOVER, addSpy);
            jsonObject.put(VoiceConstants.SPY_HAS_GHOST, setSpyToGhost);
            jsonObject.put(VoiceConstants.SPY_CAN_CUT_SPEAKER, canInsertMic);
            jsonObject.put(VoiceConstants.SPY_CAN_GUESS, canGuessWord);
            jsonObject.put(VoiceConstants.SPY_FIRST_WORD, firstWord);
            jsonObject.put(VoiceConstants.SPY_SECOND_WORD, secondWord);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_UPDATE_CONFIG, jsonObject);
    }

    //"谁是卧底"获取系统词库
    public static void sendSpyGetSystemWords() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.TYPE, VoiceConstants.SPY_SYSTEM_WORDS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GET_WORDS, jsonObject);
    }

    //"谁是卧底"获取系统词库
    public static void sendSpyGetCustomWords() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.TYPE, VoiceConstants.SPY_CUSTOM_WORDS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GET_WORDS, jsonObject);
    }

    //"谁是卧底"游戏开始
    public static void sendSpyGameStart(SocketCallBack successCallback, SocketCallBack
            failedCallback) {
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_START, null, successCallback, failedCallback);
    }

    //"谁是卧底"准备
    public static void sendSpyGamePrepare() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_PREPARE, jsonObject);
    }

    //"谁是卧底"取消准备
    public static void sendSpyGameUnPrepare() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_UNPREPARE, jsonObject);
    }

    //"谁是卧底"投票
    public static void sendSpyGameVote(int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_VOTE, jsonObject);
    }

    //"谁是卧底"猜词
    public static void sendSpyGuessWord(String word) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.SPY_GAME_WORD, word);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_GUESS_WORD, jsonObject);
    }

    //评论题目
    public static void sendTopicComment(boolean isGoodTopic) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.SPY_GAME_TOPIC_ATTITUDE, isGoodTopic);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_TOPIC_COMMENT, jsonObject);
    }

    //结束发言
    public static void sendEndSpeech() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.SPY_GAME_END_SPEECH, jsonObject);
    }

    //请求自由模式
    public static void requestFreeMode(int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.REQUEST_FREE_MODE, jsonObject);
    }

    //接受自由模式
    public static void acceptFreeMode() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.ACCEPT_FREE_MODE, jsonObject);
    }

    //拒绝自由模式
    public static void rejectFreeMode() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.REJECT_FREE_MODE, jsonObject);
    }

    //请求成为房主
    public static void requestMaster() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.REQUEST_MASTER, jsonObject);
    }

    //购买房契
    public static void purchaseRoom() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.PURCHASE_ROOM, jsonObject);
    }

    //转让房契
    public static void handleOverCreator(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.USER_ID_LOWER, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.HAND_OVER_CREATOR, jsonObject);
    }

    //更改麦位数
    public static void changeSeatNumber(int count) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.COUNT, count);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.UPDATE_SEAT_COUNT, jsonObject);
    }

    //更换背景
    public static void updateBackground(String path) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.BG_URL, path);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.UPDATE_BG, jsonObject);
    }

    //发送红包
    public static void sendRedPackage(long amount, long count, SocketCallBack success, SocketCallBack failed)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.PACKET_COUNT,count);
            jsonObject.put(VoiceConstants.PACKET_AMMOUNT,amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.PACKET_SEND, jsonObject, success, failed);
    }

    //抢红包
    public static void grabPackage(String packetId, SocketCallBack success, SocketCallBack failed)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("packet_id",packetId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage("grab_packet", jsonObject, success, failed);
    }
}
