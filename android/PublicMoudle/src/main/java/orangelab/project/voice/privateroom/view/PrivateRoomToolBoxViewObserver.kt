package orangelab.project.voice.privateroom.view

/**
 *game_werewolf
 *2018/4/1 下午12:13
 *Mystery
 */
interface PrivateRoomToolBoxViewObserver {

    fun onChangeBackGround()

    fun onSendPaperPlane()

    fun onSendImage()
}