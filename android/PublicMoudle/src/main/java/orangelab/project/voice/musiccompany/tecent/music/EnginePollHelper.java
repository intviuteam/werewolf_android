package orangelab.project.voice.musiccompany.tecent.music;

import android.os.Handler;

import com.tencent.TMG.ITMGContext;
import com.toolkit.action.Keepable;

/**
 * Created by baixs on 2017/11/20.
 */

public class EnginePollHelper implements Keepable {

    private static EnginePollHelper s_enginePollHelper = null;

    public static void createEnginePollHelper() {
        if (s_enginePollHelper == null) {
            s_enginePollHelper = new EnginePollHelper();
            s_enginePollHelper.startTimer();
        }
    }

    public static void destroyEnginePollHelper() {
        if (s_enginePollHelper != null) {
            s_enginePollHelper.stopTimer();
            s_enginePollHelper = null;
        }
    }

    private Handler mhandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (ITMGContext.GetInstance(null) != null)
                ITMGContext.GetInstance(null).Poll();
            mhandler.postDelayed(mRunnable, 33);
        }
    };

    EnginePollHelper() {

    }

    private void startTimer() {
        mhandler.postDelayed(mRunnable, 33);
    }

    private void stopTimer() {
        mhandler.removeCallbacks(mRunnable);
    }
}
