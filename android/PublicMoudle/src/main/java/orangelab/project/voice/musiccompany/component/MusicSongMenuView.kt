package orangelab.project.voice.musiccompany.component

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.R
import com.toolkit.action.Destroyable
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.handler.AbstractSocketMessageHandler
import orangelab.project.voice.handler.VoiceMessageBean
import orangelab.project.voice.musiccompany.adapter.MusicSongOrderAdapter
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import org.w3c.dom.Text

class MusicSongMenuView(context: Context, contentView: View) : Destroyable {

    private var tvTitle: TextView = contentView.findViewById(R.id.tv_title) as TextView
    private var orderedSongList: RecyclerView = contentView.findViewById(R.id.rv_music_order_song) as RecyclerView
    private var adapter: MusicSongOrderAdapter = MusicSongOrderAdapter(MusicCompanyDataSource.getOrderedSongs())

    init {
        orderedSongList.layoutManager = WrapContentLinearLayoutManager(context)
        orderedSongList.adapter = adapter
        refreshList()

        refreshTitle()
    }

    /**
     * 刷新标题
     */
    fun refreshTitle() {
        tvTitle.text = MessageUtils.getString(R.string.str_music_song_menu_title,
                MusicCompanyDataSource.getSongMenuNumber(),
                MusicCompanyConfig.maxSongMenuCapacity()
        )
    }

    /**
     * 刷新列表
     */
    fun refreshList() {
        adapter.refreshData(MusicCompanyDataSource.getOrderedSongs())
    }

    override fun destroy() {
    }

    val observer = object : AbstractSocketMessageHandler() {

        override fun handleBookSong(messageBean: VoiceMessageBean?) {
            refreshTitle()
            refreshList()
        }

        override fun handlePeakSong(messageBean: VoiceMessageBean?) {
            refreshTitle()
            refreshList()
        }

        override fun handleRemoveSong(messageBean: VoiceMessageBean?) {
            refreshTitle()
            refreshList()
        }

        override fun destroy() {

        }
    }
}