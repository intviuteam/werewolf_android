package orangelab.project.voice.musiccompany.model

import com.toolkit.action.Keepable
import orangelab.project.common.db.entity.MusicDataEntity
import java.io.Serializable

class MusicDetailParseBean : Keepable, Serializable {
    var code: Int = 0
    var message: String = ""
    var data: MusicDataEntity = MusicDataEntity()
}