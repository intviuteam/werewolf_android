package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.floatwindow.FloatWindowCommander;
import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.manager.SWStyleManager;
import orangelab.project.voice.privateroom.PrivateRoomContext;
import orangelab.project.voice.manager.VoiceRoomCommander;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.utils.PositionHelper;

/**
 * game_werewolf
 * 2017/12/27 上午10:21
 * Mystery
 */

public class VoiceTurnRoomDialog extends SafeDialog implements View.OnClickListener {

    private View turn;
    private View keep;
    private Button cancel;

    private Runnable whenTurn;
    private Runnable parentDestroy;

    public VoiceTurnRoomDialog(@NonNull Context context) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_dialog_float_turn);

        turn = findViewById(R.id.ll_voice_turn_cur_room);
        keep = findViewById(R.id.ll_voice_keep_cur_room);
        cancel = (Button) findViewById(R.id.btn_cancel);

        turn.setOnClickListener(this);
        keep.setOnClickListener(this);
        cancel.setOnClickListener(this);
        initWindow();
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(340);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void setWhenTurn(Runnable whenTurn) {
        this.whenTurn = whenTurn;
    }

    public void setParentDestroy(Runnable parentDestroy) {
        this.parentDestroy = parentDestroy;
    }

    @Override
    protected void release() {
        whenTurn = null;
        parentDestroy = null;
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ll_voice_turn_cur_room) {
            //添加意外保险，理论上，断网是无法切换房间的
            //添加对网络的判断，如果断网，游戏中也能离开，否则收不到游戏结束的推送，无法离开
            boolean isConnect = false;
            SocketEngine engine = MainApplication.getInstance().getSocketEngineHelper().getEngine();
            if (engine == null) {
                isConnect = false;
            } else {
                isConnect = !engine.isDisConnected();
            }
            if (VoiceRoomConfig.isIsPlaying() && PositionHelper.isUpSeat() && isConnect) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_leave));
                if (parentDestroy != null) {
                    parentDestroy.run();
                }
                return;
            }
            /**
             * 目前分两种情况，先处理是否是私密房的小窗模式
             */
            PrivateRoomContext privateRoomContext = VoiceRoomCommander.ContextManager.INSTANCE.GetPrivateRoomContext();
            if (privateRoomContext != null) {
                /**
                 * 当前是私密房小窗模式
                 */
                FloatWindowCommander.DestroyFloatWindowImmediately(getContext(), value -> {
                    VoiceRoomCommander.ContextManager.INSTANCE.DestroyPrivateRoomContext(() -> {
                        if (whenTurn != null) {
                            whenTurn.run();
                        }
                        dismiss();
                    });
                });
                return;
            }

            /**
             * 下面是语音房小窗模式
             */
            FloatWindowCommander.DestroyFloatWindow(getContext(), value -> {
                if (value != null) {
                    ToastToolKit.showShort(value.getMessage());
                    if (parentDestroy != null) {
                        parentDestroy.run();
                    }
                    dismiss();
                } else {
                    if (whenTurn != null) {
                        whenTurn.run();
                    }
                    dismiss();
                }
            });

//            if (VoiceRoomDataSourceManager.getInstance().needHandOverMaster()) {
//                FloatWindowCommander.ReEnterRoom(true);
//                if (parentDestroy != null) {
//                    parentDestroy.run();
//                }
//            } else {
//                FloatWindowCommander.DestroyFloatWindow(getContext(), value -> {
//                    if (value != null) {
//                        ToastToolKit.showShort(value.getMessage());
//                        if (parentDestroy != null) {
//                            parentDestroy.run();
//                        }
//                        dismiss();
//                    } else {
//                        if (whenTurn != null) {
//                            whenTurn.run();
//                        }
//                        dismiss();
//                    }
//                });
//            }
        } else if (id == R.id.ll_voice_keep_cur_room) {
            FloatWindowCommander.ReEnterRoom();
            if (parentDestroy != null) {
                parentDestroy.run();
            }
            dismiss();
        } else if (id == R.id.btn_cancel) {
            if (parentDestroy != null) {
                parentDestroy.run();
            }
            dismiss();
        }
//        switch (v.getId()) {
//            case R.id.ll_voice_turn_cur_room:
//                //添加意外保险，理论上，断网是无法切换房间的
//                //添加对网络的判断，如果断网，游戏中也能离开，否则收不到游戏结束的推送，无法离开
//                boolean isConnect = false;
//                SocketEngine engine = MainApplication.getInstance().getSocketEngineHelper().getEngine();
//                if (engine == null) {
//                    isConnect = false;
//                } else {
//                    isConnect = !engine.isDisConnected();
//                }
//                if (VoiceRoomConfig.isIsPlaying() && PositionHelper.isUpSeat() && isConnect) {
//                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_leave));
//                    if (parentDestroy != null) {
//                        parentDestroy.run();
//                    }
//                    return;
//                }
//                /**
//                 * 目前分两种情况，先处理是否是私密房的小窗模式
//                 */
//                PrivateRoomContext privateRoomContext = VoiceRoomCommander.ContextManager.INSTANCE.GetPrivateRoomContext();
//                if (privateRoomContext != null) {
//                    /**
//                     * 当前是私密房小窗模式
//                     */
//                    if (privateRoomContext.isMaster()) {
//                        /**
//                         * 自己是房主
//                         */
//                        FloatWindowCommander.ReEnterRoom(true);
//                        if (parentDestroy != null) {
//                            parentDestroy.run();
//                        }
//                        return;
//                    } else {
//                        FloatWindowCommander.DestroyFloatWindowImmediately(getContext(), value -> {
//                            VoiceRoomCommander.ContextManager.INSTANCE.DestroyPrivateRoomContext(() -> {
//                                if (whenTurn != null) {
//                                    whenTurn.run();
//                                }
//                                dismiss();
//                            });
//                        });
//                        return;
//                    }
//                }
//
//                /**
//                 * 下面是语音房小窗模式
//                 */
//                if (VoiceRoomDataSourceManager.getInstance().needHandOverMaster()) {
//                    FloatWindowCommander.ReEnterRoom(true);
//                    if (parentDestroy != null) {
//                        parentDestroy.run();
//                    }
//                } else {
//                    FloatWindowCommander.DestroyFloatWindow(getContext(), value -> {
//                        if (value != null) {
//                            ToastToolKit.showShort(value.getMessage());
//                            if (parentDestroy != null) {
//                                parentDestroy.run();
//                            }
//                            dismiss();
//                        } else {
//                            if (whenTurn != null) {
//                                whenTurn.run();
//                            }
//                            dismiss();
//                        }
//                    });
//                }
//                break;
//            case R.id.ll_voice_keep_cur_room:
//                FloatWindowCommander.ReEnterRoom();
//                if (parentDestroy != null) {
//                    parentDestroy.run();
//                }
//                dismiss();
//                break;
//            case R.id.btn_cancel:
//                if (parentDestroy != null) {
//                    parentDestroy.run();
//                }
//                dismiss();
//                break;
//        }
    }
}
