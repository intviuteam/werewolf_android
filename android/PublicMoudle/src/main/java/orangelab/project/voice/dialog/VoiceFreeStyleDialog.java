package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.androidtoolkit.view.ScreenUtils;
import com.R;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.CountDownView;
import orangelab.project.voice.api.VoiceSocketHelper;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 08/01/2018.
 * //接收自由模式对话框
 */

public class VoiceFreeStyleDialog extends SafeDialog implements Destroyable {
    public static final String TAG = "VoiceFreeStyleDialog";


    private View btnSure;
    private View btnCancel;
    private TextView tvSeconds;

    private Context mContext;
    private int duration;
    private int position;
    private CountDownView countDownView;

    public VoiceFreeStyleDialog(@NonNull Context context, int duration, int position) {
        this(context, R.style.DarkDialog);
        this.duration = duration;
        this.position = position;
        mContext = context;
        init();
    }

    public VoiceFreeStyleDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_receive_freestyle, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        tvSeconds = (TextView) findViewById(R.id.tv_seconds);

        countDownView = new CountDownView(tvSeconds, duration, () -> dismiss());
        countDownView.setCountDownRule(R.string.str_voice_second_with_wrap_ss);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        params.gravity = Gravity.BOTTOM;
        params.y = ScreenUtils.dip2px(80);
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> accept());
        btnCancel.setOnClickListener(v -> refuse());
    }

    private void accept() {
        VoiceSocketHelper.acceptFreeMode();
        dismiss();
    }

    private void refuse() {
        VoiceSocketHelper.rejectFreeMode();
        dismiss();
    }

    @Override
    public void show() {
        super.show();
        if (countDownView != null) {
            countDownView.start();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (countDownView != null) {
            countDownView.cancel();
        }
    }

    @Override
    protected void release() {
        mContext = null;
    }

    @Override
    public void destroy() {

    }
}
