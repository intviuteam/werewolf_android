package orangelab.project.voice.musiccompany.api

import com.androidtoolkit.PLog
import orangelab.project.MainApplication
import orangelab.project.common.engine.task.SocketCallBack
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.element.VoiceChair
import orangelab.project.voice.musiccompany.event.MusicEvent
import orangelab.project.voice.musiccompany.model.MusicOrderSong
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

enum class MusicSocketHelper {

    INSTANCE;

    fun nativeRequestMessage(type: String, payload: JSONObject) {
        nativeRequestMessage(type, payload, null, null)
    }

    fun nativeRequestMessage(type: String, payload: JSONObject?, success: SocketCallBack?, failed: SocketCallBack?) {
        MainApplication.getInstance().socketEngineHelper.nativeRequestMessage(type, payload, success, failed)
    }

    /**
     * 开始唱歌
     */
    fun sendStartSing() {
        var jsonObject = JSONObject()
        nativeRequestMessage(MusicApiConstants.START_SING, jsonObject)
    }

    /**
     * 结束唱歌
     */
    fun sendStopSing() {
        PLog.e("MusicSocketHelper", "sendStopSing...")
        var jsonObject = JSONObject()
        nativeRequestMessage(MusicApiConstants.STOP_SING, jsonObject)
    }

    /**
     * 点歌到歌单
     */
    fun sendOrderSong(song: MusicOrderSong) {
        val jsonObject = JSONObject()

        jsonObject.put(MusicApiConstants.MUSIC_ID, song.id)
        nativeRequestMessage(MusicApiConstants.BOOK_SONG, jsonObject)
    }

    /**
     * 授权
     */
    fun sendAuthorizeSing(id: String) {
        val jsonObject = JSONObject()
        jsonObject.put(VoiceConstants.USER_ID_LOWER, id)

        nativeRequestMessage(MusicApiConstants.MUSIC_AUTHORIZE_SING, jsonObject)
    }

    /**
     * 置顶歌曲
     */
    fun peakSong(musicID: String, userId: String) {
        val jsonObject = JSONObject()
        jsonObject.put(MusicApiConstants.MUSIC_ID, musicID)
        jsonObject.put(VoiceConstants.USER_ID_LOWER, userId)

        nativeRequestMessage(MusicApiConstants.MUSIC_POP_SONG, jsonObject)
    }

    /**
     * 删除歌曲
     */
    fun removeSongFromMenu(musicID: String, userID: String) {
        val jsonObject = JSONObject()
        jsonObject.put(MusicApiConstants.MUSIC_ID, musicID)
        jsonObject.put(VoiceConstants.USER_ID_LOWER, userID)

        nativeRequestMessage(MusicApiConstants.MUSIC_REMOVE_SONG, jsonObject)
    }
}