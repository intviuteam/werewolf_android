package orangelab.project.voice.handler;

import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 21/03/2018.
 * 用于切换消息处理器
 */

public interface IHandlerSwitcher extends Keepable {

    //在消息处理之前切换
    boolean switchBeforeHandleMessage();

    //在消息处理之后切换
    boolean switchAfterHandleMessage();

}
