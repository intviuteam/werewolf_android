package orangelab.project.voice.privateroom

import orangelab.project.voice.privateroom.model.PrivateRoomPaperPlane

/**
 *game_werewolf
 *2018/3/28 下午6:54
 *Mystery
 */
interface PrivateRoomPaperPlaneViewObserver {


    fun onPaperPlaneReplay(privateRoomPaperPlane: PrivateRoomPaperPlane?)

}