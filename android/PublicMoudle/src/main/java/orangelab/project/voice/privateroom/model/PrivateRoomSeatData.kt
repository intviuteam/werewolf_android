package orangelab.project.voice.privateroom.model

/**
 *game_werewolf
 *2018/3/28 上午11:42
 *Mystery
 */
interface PrivateRoomSeatData {

    fun getSeatPosition(): Int

    fun seatIsLock(): Boolean

    fun getSeatMemberData(): PrivateRoomMemberData?

    fun seatNeedRefresh(): Boolean

    fun seatRefreshDone()
}