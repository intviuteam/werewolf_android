package orangelab.project.voice.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.datasource.GlobalUserState;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.voice.model.VoiceRedPacketInfoBean;

/**
 * Created by hwx on 13/07/2018.
 */

public class RedPacketInfoAdapter extends BaseAdapter {
    private Context mContext;
    private List<VoiceRedPacketInfoBean.Getter> mRedPacketInfo;

    public RedPacketInfoAdapter(Context context, VoiceRedPacketInfoBean packetInfo) {
        this.mContext = context;
        initData(packetInfo);
    }

    private void initData(VoiceRedPacketInfoBean packetInfo) {
        if (packetInfo != null) {
            this.mRedPacketInfo = packetInfo.getters;
            //按照抢的钻石数目排序大小
            Collections.sort(mRedPacketInfo, (o1, o2) -> {
                int i = o2.count-  o1.count;
                return i;
            });
            //设置手气最佳
            if (mRedPacketInfo.get(0) != null) {
                mRedPacketInfo.get(0).best = true;
            }
            //再把自己排到首位
            int size = mRedPacketInfo.size();
            for (int i = 0; i < size; i++) {
                if (mRedPacketInfo.get(i).name.equals(GlobalUserState.INSTANCE.getUserName())) {
                    VoiceRedPacketInfoBean.Getter temp = mRedPacketInfo.get(i);
                    mRedPacketInfo.remove(i);
                    mRedPacketInfo.add(0,temp);
                }
            }
        }
    }
    @Override
    public int getCount() {
        return mRedPacketInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mRedPacketInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.layout_dialog_redpacket_info_item, null);
            viewHolder = new ViewHolder();
            viewHolder.avatar= (CircleImageView) convertView.findViewById(R.id.head_image);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.packetNum = (TextView) convertView.findViewById(R.id.packet_number);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        VoiceRedPacketInfoBean.Getter item = mRedPacketInfo.get(position);
        if (item != null) {
            PicassoUtils.loadImage(mContext, item.image, viewHolder.avatar, R.mipmap.default_head);
            viewHolder.userName.setText(item.name);
            viewHolder.packetNum.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_diamond), item.count));
            if (item.best) {
                viewHolder.crown = (ImageView) convertView.findViewById(R.id.crown);
                viewHolder.best = (TextView) convertView.findViewById(R.id.user_best);
                viewHolder.crown.setVisibility(View.VISIBLE);
                viewHolder.best.setVisibility(View.VISIBLE);
            }
        }
        return convertView;
    }

    class ViewHolder {
        CircleImageView avatar;
        TextView userName;
        TextView packetNum;
        ImageView crown;
        TextView best;
    }
}
