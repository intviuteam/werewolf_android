package orangelab.project.voice.privateroom.view

/**
 *game_werewolf
 *2018/3/21 上午11:54
 *Mystery
 */
interface PrivateRoomTitleObserver {

    fun onMinimize()

    fun onInvite()

    fun onExit()

    fun onManageRoom()

    fun onManageTag()

    fun onOnlineNumber()
}