package orangelab.project.voice.musiccompany.component

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.PLog
import com.toolkit.action.Destroyable
import orangelab.project.voice.handler.AbstractSocketMessageHandler
import orangelab.project.voice.handler.VoiceMessageBean
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource
import orangelab.project.voice.musiccompany.view.SongMenuViewTag

/**
 * 伴奏界面顶部相关UI
 */
class MusicTitleView(context: Context, contentView: ViewGroup) : Destroyable {

    private val TAG = "MusicTitleView"

    //点击到"歌单"
    private val songMenu: SongMenuViewTag = contentView.findViewById(R.id.so_music_song_menu) as SongMenuViewTag

    init {

        PLog.e(TAG, "MusicTitleView init")
        refreshSongNumber()
    }

    /**
     * 点击事件
     */
    fun setOnMenuClickListener(onClickListener: View.OnClickListener) {
        songMenu.setOnClickListener(onClickListener)
    }

    /**
     * 展示歌单
     */
    fun showSongMenu() {
        songMenu.visibility = View.VISIBLE
        refreshSongNumber()
    }

    /**
     * 隐藏歌单
     */
    fun hideSongMenu() {
        songMenu.visibility = View.GONE
    }

    /**
     * 刷新歌单数目
     */
    fun refreshSongNumber() {
        songMenu.refreshSongNumber(MusicCompanyDataSource.getSongMenuNumber())
    }

    override fun destroy() {
        //release something
    }

    /**
     * 消息处理
     */
    val observer = object : AbstractSocketMessageHandler() {

        override fun handleBookSong(messageBean: VoiceMessageBean?) {
            refreshSongNumber()
        }

        override fun handleRemoveSong(messageBean: VoiceMessageBean?) {
            refreshSongNumber()
        }

        override fun handleStartSing(messageBean: VoiceMessageBean?) {
            refreshSongNumber()
        }

        override fun handleStopSing(messageBean: VoiceMessageBean?) {
            refreshSongNumber()
        }

        override fun destroy() {

        }

    }
}