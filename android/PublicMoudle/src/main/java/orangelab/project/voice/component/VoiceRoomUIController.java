package orangelab.project.voice.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import org.json.JSONObject;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.effect.AnimationViewGroup;
import orangelab.project.common.effect.IAnimationSurface;
import orangelab.project.common.effect.IBaseAnimationSurface;
import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.exhibition.card.CardUtils;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.VoiceRoomCommander;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * Created by lloydfinch on 28/12/2017.
 * //语音房UI控制器
 */

public class VoiceRoomUIController extends AbstractSocketMessageHandler implements Keepable, Destroyable {

    //主要成员控件
    public VoiceTitleViewComponent titleViewComponent;
    public VoiceMainViewComponent mainViewComponent;
    public VoiceSpeakViewComponent speakViewComponent;

    //背景
    private ImageView ivBg;

    //断网后覆盖物
    private View viewDisconnectCover;
    private View viewDisconnectPartCover;

    //送礼动画相关
    private IAnimationSurface animationViewGroup;
    private IBaseAnimationSurface secondGiftAnimationView;

    //Context(Activity)
    private Context mContext;

    public VoiceRoomUIController(Context context, ViewGroup contentView) {
        mContext = context;
        ViewGroup titleViewContainer = (ViewGroup) contentView.findViewById(R.id.rl_title_container);
        titleViewComponent = new VoiceTitleView(context, titleViewContainer);

        ViewGroup mainViewContainer = (ViewGroup) contentView.findViewById(R.id.voice_container);
        mainViewComponent = new VoiceMainView(context, mainViewContainer);

        ViewGroup speakViewContainer = (ViewGroup) contentView.findViewById(R.id.voice_speak_container);
        speakViewComponent = new VoiceSpeakView(speakViewContainer);
        speakViewComponent.handleSpeak(false);

        animationViewGroup = (AnimationViewGroup) contentView.findViewById(R.id.giftAnimationView);
        secondGiftAnimationView = (IBaseAnimationSurface) contentView.findViewById(R.id.secondGiftAnimation);

        ivBg = (ImageView) contentView.findViewById(R.id.iv_activity_bg);
        viewDisconnectCover = contentView.findViewById(R.id.view_voice_disconnect);
        viewDisconnectPartCover = contentView.findViewById(R.id.view_voice_disconnect1);
    }

    public void init() {
        initView();
        initListener();

        //将自己添加到消息处理器中去
        VoiceServerMessageHandler.getInstance().registerUIHandler(this);
    }

    private void initView() {
        titleViewComponent.displayData();
        mainViewComponent.displayData();
        speakViewComponent.displayData();
    }

    private void initListener() {
        mainViewComponent.setOnChangeTitleClickedListener(v ->
                mainViewComponent.showChangeTitleUI(VoiceRoomConfig.getRoomTitle(), VoiceRoomConfig.getRoomType()));
        titleViewComponent.onSettingClicked(null);
        titleViewComponent.onShareClicked(null);
    }

    public void onBackPressed(View.OnClickListener onBackPressedListener) {
        titleViewComponent.onBackPressed(onBackPressedListener);
    }

    public void onOnLineNumberClicked(View.OnClickListener onOnLineNumberClickedListener) {
        titleViewComponent.onOnLineNumberClicked(onOnLineNumberClickedListener);
    }

    //分发socket的message事件
    public void dispatchSocketEvent(ServerMessageEventOver event) {

    }

    //分发socket的response事件
    public void dispatchSocketEvent(ServerResponseEventOver event) {

    }

    //使UI不可触摸(断网时使用)
    public void disableTouch() {
        viewDisconnectCover.setVisibility(View.VISIBLE);
        viewDisconnectPartCover.setVisibility(View.VISIBLE);
    }

    //使UI可触摸(重连成功后使用)
    public void enableTouch() {
        viewDisconnectCover.setVisibility(View.GONE);
        viewDisconnectPartCover.setVisibility(View.GONE);
    }

    //更新音频socket连接状态
    public void changeAudioSocketConnectedState(boolean isConnected) {
        titleViewComponent.updateWifiState(isConnected);
    }

    //更新游戏socket连接状态
    public void changeGameSocketConnectedState(boolean isConnected) {
        mainViewComponent.updateSocketState(isConnected);
    }

    //更新在线人数
    public void updateOnLineNumber(int number) {
        titleViewComponent.updateOnLineNumber(number);
    }

    //更新房主才可见控件
    public void updateMasterUI() {
        titleViewComponent.updateMasterUI();
        mainViewComponent.updateMasterUI();
    }

    //更新房间背景
    public void updateBackground() {
        String background = VoiceRoomConfig.getBackground();
        if (!TextUtils.isEmpty(background)) {
            PicassoUtils.loadImageByDefaultNoPlaceHolder(mContext, background, ivBg, R.mipmap.ic_bg_voice);
        } else {
            //如果background为null,就设置默认背景
            ivBg.setImageResource(R.mipmap.ic_bg_voice);
        }
    }

    //自定义背景
    public void customBG() {
        if (VoiceRoomConfig.isBlurBg()) {
            //模糊效果
            VoiceRoomCommander.BackGroundManager.INSTANCE.BlurBackGround(VoiceRoomConfig.getBackground(), ivBg, safeHandler);
        } else {
            //非模糊效果
            updateBackground();
        }
    }

    public void setChatMessagehidden(boolean hidden) {
        mainViewComponent.hideChatMessage(hidden);
    }

    public void setGiftMessagehidden(boolean hidden) {
        mainViewComponent.hideGiftMessage(hidden);
    }

    public void setJoinMessagehidden(boolean hidden) {
        mainViewComponent.hideJoinMessage(hidden);
    }

    public void onResume() {
        mainViewComponent.onResume();
    }

    public void restoreMessage() {
        mainViewComponent.onReEnter();
    }

    @Override
    public void destroy() {

        safeHandler.release();
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(this);

        if (titleViewComponent != null) {
            titleViewComponent.destroy();
            titleViewComponent = null;
        }
        if (mainViewComponent != null) {
            mainViewComponent.destroy();
            mainViewComponent = null;
        }
        if (speakViewComponent != null) {
            speakViewComponent.destroy();
            speakViewComponent = null;
        }
        if (animationViewGroup != null) {
            animationViewGroup.destroy();
            animationViewGroup = null;
        }
        if (secondGiftAnimationView != null) {
            secondGiftAnimationView.destroy();
            secondGiftAnimationView = null;
        }
    }

    //处理socket消息=======================================================
    @Override
    public void handleMessageEvent(ServerMessageEventOver event) {
        mainViewComponent.handleServerEvent(event);
        titleViewComponent.handleServerEvent(event);
        speakViewComponent.handleServerEvent(event);

        //自己处理
        handleServerEvent(event);
    }

    private void handleServerEvent(ServerMessageEventOver event) {
        String type = event.type;
        if (TextUtils.equals(type, VoiceConstants.TYPE_CHAT)) {
            handleChat(TransferClassHelper.ServerMessageOver2VoiceMessage(event));
        } else if (TextUtils.equals(type, Constant.CARD_CHECK_RESULT)) {
            handleCardCheckResult(TransferClassHelper.ServerMessageOver2VoiceMessage(event));
        }
    }

    @Override
    public void handleResponseEvent(ServerResponseEventOver event) {
        mainViewComponent.handleServerResponseEvent(event);
    }

    @Override
    public void handleChat(VoiceMessageBean messageBean) {
        JSONObject gift = messageBean.payload.optJSONObject(VoiceConstants.GIFT_CHAT);
        if (gift != null) {
            int to = gift.optInt(VoiceConstants.GIFT_TO) + 1;
            String type = gift.optString(VoiceConstants.GIFT_TYPE);
            if (TextUtils.isEmpty(type)) {
                return;
            }
            if (!TextUtils.isEmpty(CardUtils.getCardName(type))) {
                if (to == PersonalDataHelper.getPersonalData().getSelfPosition()) {
                    if (animationViewGroup != null) {
                        animationViewGroup.refreshCard();
                    }
                }
            }
        }
    }

    @Override
    public void handleCardCheckResult(VoiceMessageBean messageBean) {
        animationViewGroup.userCard(ExhibitionsConstant.CARD_CHECK, false);
    }
    //==========================================================================

}
