package orangelab.project.voice.lobby.component;

import android.content.Context;
import android.graphics.PointF;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.AnimationEvent;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.model.ServerResponseEventOver;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.model.WereWolfGiftRebate;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceClickMsgDialog;
import orangelab.project.voice.dialog.VoiceFreeStyleDialog;
import orangelab.project.voice.dialog.VoiceFriendDialog;
import orangelab.project.voice.dialog.VoiceSystemDialog;
import orangelab.project.voice.element.RoomChair;
import orangelab.project.voice.element.VoiceChair;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.lobby.LobbyMessageAdapter;
import orangelab.project.voice.lobby.data.LobbyRoomConfig;
import orangelab.project.voice.lobby.event.LobbyEvent;
import orangelab.project.voice.manager.SmallGameManager;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * 语音房copy过来
 * TODO clean code
 */

public class LobbyMainView extends AbstractSocketMessageHandler {

    private static final String TAG = "LobbyMainView";
    private LobbyMainViewUI voiceMainViewUI;
    private SparseArray<RoomChair> voiceChairs = new SparseArray<>();
    private List<SystemMessageItem> messageList;
    private LobbyMessageAdapter adapter;

    private Context mContext;
    private String type;

    //预处理器
    private AbstractSocketMessageHandler preHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handlePreDownSeat(int position) {
            if (LobbyRoomConfig.isUpSeat(position)) {
                RoomChair oldChair = voiceChairs.get(position);
                if (oldChair != null) {
                    oldChair.downSeat(VoiceConstants.TEMPORARY_POSITION);
                    EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
                }
            }
        }

        @Override
        public void destroy() {

        }
    };

    public LobbyMainView(Context context, ViewGroup contentView) {
        this.mContext = context;
        voiceMainViewUI = new LobbyMainViewUI(context, contentView);
        EventBus.getDefault().register(this);
        init();
    }

    private void init() {
        initChairs();
        initUIEvent();
        initData();

        //添加预处理器，可以在更改数据前进行特殊操作
        VoiceServerMessageHandler.getInstance().registerPreHandler(preHandler);
    }

    private void initChairs() {

        //这里只初始化一个位置
        for (int position = 0; position <= LobbyRoomConfig.maxSeatPosition; position++) {
            try {
                int chairId = R.id.class.getField("voice_member" + position).getInt(null);
                PLog.e(TAG, "" + chairId);
                ViewGroup chairContentView = voiceMainViewUI.findChairUIById(chairId);
                voiceChairs.put(position, new VoiceChair(chairContentView, position));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private void initUIEvent() {
        RxToolKit.Build(this, ViewEvent.RequestAddFriendEvent.class)
                .action(requestAddFriendEvent -> {
                    int position = findUserById(requestAddFriendEvent.getToUserId()).position;
                    RoomSocketEngineHelper.sendRequestFriendInGame(requestAddFriendEvent.getToUserId(), position);
                })
                .register();
    }

    private void initData() {
        messageList = new ArrayList<>();
        adapter = new LobbyMessageAdapter(mContext, voiceMainViewUI.getListMsg(), messageList);
        adapter.setOnNameClickListener(id -> {
            //只有房主点击不是自己的人才有效
            if (PositionHelper.isMaster() && !PositionHelper.isSelf(id)) {
                EnterRoomResult.EnterRoomUserItem user = findUserById(id);
                //点击的人没走才有效
                if (user != null) {
                    new VoiceClickMsgDialog(mContext, user).show();
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
                }
            } else {
                PLog.e(TAG, "useless click!");
            }
        });
        voiceMainViewUI.getListMsg().setAdapter(adapter);

        voiceMainViewUI.getCheckBoxHiddenMessage().setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                adapter.hideChatMessage();
            } else {
                adapter.showAllMessage();
            }
            VoiceRoomConfig.setIsHiddenMsg(isChecked);
        });
    }

    public void displayData() {

        //处理加锁位置
        List<Integer> lockedSeatList = VoiceRoomDataSourceManager.getInstance().getLockedSeatList();
        if (lockedSeatList != null) {
            for (Integer position : lockedSeatList) {
                if (LobbyRoomConfig.isUpSeat(position)) {
                    voiceChairs.get(position).lockSeat();
                }
            }
        }

        //处理麦上用户
        List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        if (onLineUsers != null) {
            for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
                if (LobbyRoomConfig.isUpSeat(user.position)) {
                    voiceChairs.get(user.position).upSeat(user);
                }
            }
        }
    }

    public void showChangeTitleUI(String title, String type) {
        voiceMainViewUI.showChangeTitleUI(title, type);
    }

    public void updateSocketState(boolean isConnected) {
        if (isConnected) {
            adapter.refreshMessageList(VoiceConstants.CODE_CONNECT);
        } else {
            adapter.refreshMessageList(VoiceConstants.CODE_DISCONNECT);
        }
    }

    @Override
    public void destroy() {
        VoiceServerMessageHandler.getInstance().unRegisterPreHandler(preHandler);
        RxToolKit.UnRegister(this);
        EventBus.getDefault().unregister(this);
        voiceMainViewUI.destroy();
        messageList.clear();
        messageList = null;
        adapter.destroy();
        adapter = null;
        destroyChairs();
        destroyDialog();
        SmallGameManager.getInstance().destroy();
    }

    private void destroyChairs() {
        for (int i = 0; i <= VoiceRoomConfig.getMaxChairNumber(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.destroy();
            }
        }
        voiceChairs.clear();
        voiceChairs = null;
    }

    private void destroyDialog() {
        if (msgDialog != null) {
            if (msgDialog.isShowing()) {
                msgDialog.dismiss();
            }
        }

        if (freeStyleDialog != null) {
            if (freeStyleDialog.isShowing()) {
                freeStyleDialog.dismiss();
            }
        }
    }

    public void handleServerResponseEvent(ServerResponseEventOver event) {
        handleResponseEvent(event);
    }

    public void handleServerEvent(ServerMessageEventOver event) {
        PLog.e(TAG, "event:" + event);
        handleMessageEvent(event);
    }

    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson
                (messageBean.payload.toString(), ServerMessageEnterResult.class);
        EnterRoomResult.EnterRoomUserItem member = TransferClassHelper.EnterResult2EnterRoomUser(item);
        PLog.e("test", "key:" + messageBean.msg_id);
        //刷新消息UI
        adapter.refreshMessageList(messageBean.msg_id);
        if (LobbyRoomConfig.isUpSeat(member.position)) {
            voiceChairs.get(member.position).upSeat(member);
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        //隐藏进入/离开消息
        adapter.refreshMessageList(messageBean.msg_id);
        if (LobbyRoomConfig.isUpSeat(position)) {
            voiceChairs.get(position).downSeat(VoiceConstants.TEMPORARY_POSITION);
            EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());

        if (VoiceRoomConfig.isIsHiddenMsg()) {
            adapter.hideChatMessage();
        } else {
            adapter.showAllMessage();
        }

    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user == null) {
            return;
        }
        //从老位置下来
        if (LobbyRoomConfig.isUpSeat(user.position)) {
            RoomChair oldChair = voiceChairs.get(user.position);
            if (oldChair != null) {
                oldChair.downSeat(VoiceConstants.TEMPORARY_POSITION);
            }
        }
        //坐到新位置
        int position = messageBean.payload.optInt(VoiceConstants.DEST_POSITION);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.upSeat(user);
                adapter.refreshMessageList(messageBean.msg_id);
                EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
            }
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        adapter.refreshMessageList(messageBean.msg_id);
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            chair.speak();
        }
    }

    @Override
    public void handleUnSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            chair.unSpeak();
        }
    }

    @Override
    public void handleKickOut(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        //隐藏进入/离开消息
        adapter.refreshMessageList(messageBean.msg_id);
        if (LobbyRoomConfig.isUpSeat(position)) {
            voiceChairs.get(position).downSeat(-1);
            EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(position));
        }
        RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent());
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        if (PositionHelper.isMaster()) {
            return;
        }
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        //从老位置下来
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair oldChair = voiceChairs.get(user.position);
            oldChair.downSeat(-1);
        }
        adapter.refreshMessageList(messageBean.msg_id);
        //坐到房主位置
        voiceChairs.get(VoiceConstants.MASTER_POSITION).upSeat(user);
    }

    @Override
    public void handleChat(VoiceMessageBean messageBean) {
        //送礼
        JSONObject gift = messageBean.payload.optJSONObject(VoiceConstants.GIFT_CHAT);
        if (gift != null) {
            //新需求不显示动效，只显示消息
            //sendGift(messageBean.msg_id, gift);
            adapter.refreshMessageList(messageBean.msg_id);
        } else {
            adapter.refreshMessageList(messageBean.msg_id);
        }

        //如果是自己发送的消息，就取消屏蔽消息，展示所有消息
        if (!VoiceRoomConfig.isIsHiddenMsg()) {
            voiceMainViewUI.getCheckBoxHiddenMessage().setChecked(false);
            adapter.showAllMessage();
        }
    }

    private VoiceSystemDialog msgDialog;

    @Override
    public void handleSystemMsg(VoiceMessageBean messageBean) {
        if (messageBean.payload.has(VoiceConstants.TYPE)) {
            type = messageBean.payload.optString(VoiceConstants.TYPE);
            if (TextUtils.isEmpty(type)) {
                return;
            }
            switch (type) {
                case VoiceConstants.SYSTEM_MSG_TYPE_DIALOG:
                    String content = messageBean.payload.optString(VoiceConstants.CONTENT);
                    int duration = RoomSocketEngineHelper.getOptDurTime(messageBean.payload);
                    msgDialog = new VoiceSystemDialog(mContext, content);
                    msgDialog.showDialog(duration);
                    break;
                case VoiceConstants.SYSTEM_MSG_TYPE_TEXT:
                    adapter.refreshMessageList(messageBean.msg_id);
                    break;
                case VoiceConstants.SYSTEM_MSG_TYPE_TOAST:
                    String temp = messageBean.payload.optString(VoiceConstants.CONTENT);
                    RunnableFactory.createMsgToastRunnable(temp).run();
                    break;
            }
        }
    }

    @Override
    public void handleDisconnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.updateStatus(VoiceConstants.USER_STATUS_OFFLINE);
            }
        }
    }

    @Override
    public void handleConnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair chair = voiceChairs.get(position);
            if (chair != null) {
                chair.reConnected();
            }
        }
    }

    private VoiceFreeStyleDialog freeStyleDialog;

    @Override
    public void handleRequestFreeMode(VoiceMessageBean messageBean) {
        //现在逻辑:删除旧的，展示新的
        if (freeStyleDialog != null) {
            if (freeStyleDialog.isShowing()) {
                freeStyleDialog.dismiss();
            }
            freeStyleDialog = null;
        }
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        int duration = messageBean.payload.optInt(VoiceConstants.DURATION) / 1000;
        if (PositionHelper.isSelfPosition(position)) {
            if (PositionHelper.isMaster(position)) {
                VoiceSocketHelper.acceptFreeMode();
            } else {
                freeStyleDialog = new VoiceFreeStyleDialog(mContext, duration, position);
                freeStyleDialog.show();
            }
        }
    }

    @Override
    public void handleAcceptFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        handleFreeStyle(position);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    @Override
    public void handleRejectFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    @Override
    public void handlePurchasedResult(VoiceMessageBean messageBean) {
        //购买房契结果，只有自己能收到
        if (!TextUtils.isEmpty(messageBean.message)) {
            ToastToolKit.showShort(messageBean.message);
        }
    }

    @Override
    public void handleHandleOverCreatorResult(VoiceMessageBean messageBean) {
        if (messageBean.payload == null) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
            return;
        }
        String from = messageBean.payload.optString("from");
        String to = messageBean.payload.optString("to");
        if (TextUtils.isEmpty(from) || TextUtils.isEmpty(to)) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
            return;
        }
        if (!TextUtils.isEmpty(from) && PositionHelper.isSelf(from)) {
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message);
            }
        } else if (!TextUtils.isEmpty(to) && PositionHelper.isSelf(to)) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_be_deed));
        }
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
        if (result.room_info != null) {
            PLog.e(TAG, "restore: " + result.room_info.toString());
            restoreMembers(result.room_info);
        }
    }

    @Override
    public void handleUpdateTitle(VoiceMessageBean messageBean) {
        type = messageBean.payload.optString(VoiceConstants.TYPE);
        adapter.refreshMessageList(messageBean.msg_id);
    }

    @Override
    public void handleAddFriend(VoiceMessageBean messageBean) {
        //获取最后一条加好友消息，没有被处理才处理
        VoiceAddFriendBean bean = VoiceRoomDataSourceManager.getInstance().getLatestAddFriendBean();
        if (!bean.isHandle) {
            VoiceFriendDialog dialog = new VoiceFriendDialog(mContext, bean.id);
            dialog.show();
        }
    }

    @Override
    public void handleLikeRoom(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.likeRoom();
            }
        }

        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_CAMEL);
        EnterRoomResult.EnterRoomUserItem item = findUserById(userId);
        if (item == null) {
            return;
        }
        adapter.refreshMessageList(messageBean.msg_id);
    }

    @Override
    public void handleChangeState(VoiceMessageBean messageBean) {
        String model = messageBean.payload.optString(VoiceConstants.MODEL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        PLog.e(TAG, "uerState: " + userItem.state);
        if (TextUtils.equals(VoiceConstants.LIMIT, model)) {
            handleBanMic(position);
            adapter.refreshMessageList(messageBean.msg_id);
        } else if (TextUtils.equals(VoiceConstants.FREE, model)) {
            // 2018-02-05 不再接受老版本的自由模式消息
            handleFreeStyle(position);
            adapter.refreshMessageList(messageBean.msg_id);
        } else if (TextUtils.equals(VoiceConstants.SPACE_STR, model) || TextUtils.isEmpty(model)) {
            handleUnBanMic(position);
            handleUnFreeStyle(position);
            adapter.refreshMessageList(messageBean.msg_id);
        }
    }

    private void handleBanMic(int position) {
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.banMic();
            }
        }
    }

    private void handleUnBanMic(int position) {
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.openMic();
            }
        }
    }

    private void handleFreeStyle(int position) {
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.openFreeStyle();
            }
        }
    }

    private void handleUnFreeStyle(int position) {
        if (LobbyRoomConfig.isUpSeat(position)) {
            RoomChair roomChair = voiceChairs.get(position);
            if (roomChair != null) {
                roomChair.closeFreeStyle();
            }
        }
    }

    private void sendGift(Long key, JSONObject gift) {
        String type = gift.optString(VoiceConstants.GIFT_TYPE);

        adapter.refreshMessageList(key);
        /**
         * 如果送的是卡片，直接结束
         */
        if (EffectsManager.IsCardByGiftType(type)) {
            return;
        }

        int from = gift.optInt(VoiceConstants.GIFT_FROM);
        int to = gift.optInt(VoiceConstants.GIFT_TO);

        PointF start = null;
        PointF end = null;

        //麦上给麦上送
        if (from <= VoiceRoomConfig.getMaxChairNumber() && to <= VoiceRoomConfig.getMaxChairNumber()) {
            start = getGiftPosition(from);
            end = getGiftPosition(to);
        }

        //麦下给麦上送
        if (from > VoiceRoomConfig.getMaxChairNumber() && to <= VoiceRoomConfig.getMaxChairNumber()) {
            start = getGiftPosition(from);
            end = getGiftPosition(to);
        }

        if (start == null || end == null) {
            return;
        }
        /**
         * 这里是处理送礼物特效,处理默认数据
         */
        int fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        int toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;

        RoomChair chair = voiceChairs.get(to);//默认取终点
        int chairWidth = 0;
        int chairHeight = 0;
        if (chair != null) {
            chairWidth = chair.getHeadWidth();
            chairHeight = chair.getHeadHeight();
            //处理有房主的情况
            if (PositionHelper.isMaster(from) || PositionHelper.isMaster(to)) {
                if (from != to) {
                    RoomChair largeChair = voiceChairs.get(0);//房主位置
                    int largeWidth = largeChair.getHeadWidth();
                    int largeHeight = largeChair.getHeadHeight();

                    RoomChair smallChair = voiceChairs.get(1);//随便麦下位置
                    //chair宽高取最小值
                    chairWidth = smallChair.getHeadWidth();
                    chairHeight = smallChair.getHeadHeight();

                    int dx = (largeWidth - chairWidth) >> 1;
                    int dy = (largeHeight - chairHeight) >> 1;
                    if (PositionHelper.isMaster(from)) {
                        //起点是房主
                        start.x += dx;
                        start.y += dy;

                    } else if (PositionHelper.isMaster(to)) {
                        //终点是房主
                        end.x += dx;
                        end.y += dy;
                    }
                }
            }
            //处理一下坐标问题
            int dy = ScreenUtils.dip2px(13) >> 1;//11是用户名字的高度，加上默认的padding,取一半向上偏移
            //送礼的坐标处理，返现的不处理
            PointF start1 = new PointF(start.x, start.y - dy);
            PointF end1 = new PointF(end.x, end.y - dy);

            EffectsManager.DispatchAnimEvent(mContext, GiftAnimConstant.GIFT_FROM_TYPE_VOICE, type, start1, end1, fromO, toO, chairWidth, chairHeight, from, to);
        }


        //额外处理
        try {
            if (EffectsManager.SupportThisGiftType(type)) {
                JSONObject giftJsonObj = gift;
                if (giftJsonObj.has("rebate")) {
                    /**
                     * 这里需要处理返现
                     */
                    WereWolfGiftRebate rebate = GsonHelper.getGson().fromJson(giftJsonObj.optJSONObject("rebate").toString(), WereWolfGiftRebate.class);
                    if (rebate != null) {
                        int dim = rebate.value;
                        String dimString = Integer.toString(dim);

                        if (TextUtils.isEmpty(dimString)) {
                            return;
                        }

                        if (PositionHelper.isUpSeat(to)) {//终点在麦上才展示
                            PLog.e("gift-rebate", "to:" + to);
                            EffectsManager.DispatchRebateAnimEvent(mContext, dimString, new PointF(start.x, start.y), new PointF(end.x, end.y), fromO, toO, chairWidth, chairHeight);
                        }

                        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(to);
                        if (!TextUtils.equals(userItem.id, GlobalUserState.getGlobalState().getUserId())
                                && !Utils.checkUserIdIsTourist(userItem.id)) {
                            /**
                             * 不能为游客，不能给自己
                             */
                            PLog.e("gift-rebate", "is not self and tourist");
                            LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(type, userItem.id, userItem.name, userItem.avatar, userItem.sex, dimString);
                        }
                    }
                } else {
                    /**
                     * ignore
                     */
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PointF getGiftPosition(int position) {
        //在麦上的就取chair的位置
        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            RoomChair voiceChair = voiceChairs.get(position);
            if (voiceChair != null) {
                return voiceChair.getPositionOnScreen();
            }
        } else {
            //不在麦上的取屏幕右下角位置
            int[] location = {ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight()};
            PointF pointF = new PointF(location[0], location[1]);
            return pointF;
        }

        return null;
    }

    private void restoreMembers(EnterRoomResult.EnterRoomMessage roomInfo) {
        resetChairs();
        restoreChairs(roomInfo);
    }

    private void resetChairs() {
        for (int position = 0; position < voiceChairs.size(); position++) {
            RoomChair chair = voiceChairs.get(position);
            chair.reset();
        }
    }

    private void restoreChairs(EnterRoomResult.EnterRoomMessage enterRoomResult) {
        for (Integer i : enterRoomResult.locked_positions) {
            if (LobbyRoomConfig.isUpSeat(i)) {
                voiceChairs.get(i).lockSeat();
            }
        }
        Iterator iterator = enterRoomResult.users.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            EnterRoomResult.EnterRoomUserItem member = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            if (member == null) {
                continue;
            }
            if (LobbyRoomConfig.isUpSeat(member.position)) {
                voiceChairs.get(member.position).upSeat(member);
            }
        }
    }

    public EnterRoomResult.EnterRoomUserItem findUserByPosition(int position) {
        return VoiceRoomDataSourceManager.getInstance().findUserByPosition(position);
    }

    public EnterRoomResult.EnterRoomUserItem findUserById(String id) {
        return VoiceRoomDataSourceManager.getInstance().findUserById(id);
    }

    public void onResume() {
        for (int i = 0; i < voiceChairs.size(); i++) {
            RoomChair chair = voiceChairs.get(i);
            if (chair != null) {
                chair.onResume();
            }
        }
    }

    public void onPause() {

    }

    public void onReEnter() {
        if (adapter != null) {
            adapter.restoreMessage(VoiceRoomDataSourceManager.getInstance().getMessageList());
        }
    }

    //大厅消息处理 start================================================================//
    @Override
    public void handleLobbyInviteGame(VoiceMessageBean messageBean) {
        adapter.refreshMessageList(messageBean.msg_id);
    }

    @Override
    public void handleLobbyAcceptGame(VoiceMessageBean messageBean) {
        adapter.showAllMessage();
    }
    //大厅消息处理 end================================================================//

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemoveMessageEvent(LobbyEvent.LobbyRemoveMessageEvent event) {
        adapter.showAllMessage();
    }
}
