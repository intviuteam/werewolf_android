package orangelab.project.voice.cache;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.LruCache;

import com.toolkit.action.Destroyable;
import com.androidtoolkit.PLog;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by lloydfinch on 29/10/2017.
 * 功能:数据缓存，目前两级缓存
 */

public class MemoryCache implements Destroyable {

    private static final String TAG = "MemoryCache";
    private static MemoryCache ourInstance;

    private LruCache<String, Bitmap> mMemoryCache;
    private HashMap<String, SoftReference<Bitmap>> softCache;

    private int maxMemory;

    public static MemoryCache getInstance() {
        if (ourInstance == null) {
            ourInstance = new MemoryCache();
        }
        return ourInstance;
    }

    private MemoryCache() {
        maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //设置内存尺寸为内存的1/8
        mMemoryCache = new LruCache<String, Bitmap>(maxMemory / 8) {
            protected int sizeOf(String key, Bitmap bitmap) {

                int ret = 0;
                if (Build.VERSION.SDK_INT >= 19) {
                    ret = bitmap.getAllocationByteCount();
                } else if (Build.VERSION.SDK_INT >= 12) {
                    ret = bitmap.getByteCount();
                } else {
                    int rowBytes = bitmap.getRowBytes();
                    int height = bitmap.getHeight();
                    ret = rowBytes * height;
                }
                return ret;
            }
        };

        softCache = new LinkedHashMap<>();
    }

    /**
     * 保存bitmap到本地
     */
    public void putBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (key != null && bitmap != null) {
            mMemoryCache.put(key, bitmap);
            softCache.put(key, new SoftReference<>(bitmap));
        }
    }

    /**
     * 从本地获取bitmap
     */
    public Bitmap getBitmapFromMemory(String key) {
        Bitmap bitmap = mMemoryCache.get(key);
        if (bitmap == null) {
            SoftReference<Bitmap> bitmapSoftReference = softCache.get(key);
            if (bitmapSoftReference != null) {
                bitmap = bitmapSoftReference.get();
                if (bitmap != null) {
                    mMemoryCache.put(key, bitmap);
                }
            }
        }
        return bitmap;
    }

    public boolean recycle(String key) {
        try {
            mMemoryCache.remove(key);
            if (softCache.containsKey(key)) {
                softCache.remove(key);
            }
            return true;
        } catch (Exception e) {
            PLog.e(TAG, "error when recycle from MemoryCache, info: " + e.getMessage());
        }

        return false;
    }

    @Override
    public void destroy() {
        mMemoryCache.evictAll();
        softCache.clear();
    }
}
