package orangelab.project.voice.privateroom.model

/**
 *game_werewolf
 *2018/3/21 下午5:55
 *Mystery
 */
open class SystemMessageItem {
    var textColorResource: Int = -1
    var message = ""

    constructor(textColorResource: Int, message: String) {
        this.textColorResource = textColorResource
        this.message = message
    }

    constructor(message: String) {
        this.message = message
    }


}