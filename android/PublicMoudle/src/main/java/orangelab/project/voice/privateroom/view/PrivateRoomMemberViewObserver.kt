package orangelab.project.voice.privateroom.view

/**
 *game_werewolf
 *2018/3/21 下午5:21
 *Mystery
 */
interface PrivateRoomMemberViewObserver {

    fun onMemberClickAtPerson(position: Int)

    fun onMemberClickAtNoPerson(position: Int)

    fun onMemberClickAtLock(position: Int)

}