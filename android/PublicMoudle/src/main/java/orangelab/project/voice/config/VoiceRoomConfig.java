package orangelab.project.voice.config;

import android.text.TextUtils;

import com.R;
import com.toolkit.action.Keepable;

import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.model.VoiceSpyConfigBean;
import orangelab.project.voice.model.VoiceSpyStart;

/**
 * Created by lloydfinch on 02/11/2017.
 * 房间类型，statusbar，background等的配置文件
 */

public class VoiceRoomConfig implements Keepable {

    private static String roomTitle = "";
    private static String roomType = "";
    private static String roomId = "";
    private static boolean isSupportSW = false;//是否支持小窗模式
    private static boolean isSupportDeed = false;//是否支持房契功能
    private static boolean isSupportChangeSeat = true;//是否可以更换麦位数量
    private static boolean isSupportChangeBg = true;///是否可以更换背景
    private static String psw = "";
    private static boolean isPlaying;
    private static boolean isSmallWindowStyle = false;//当前是否是小窗模式
    private static boolean isComeInBySW = false;//是否是从小窗进入
    private static volatile int onSeatNumber;//在麦上的人数,包含房主
    private static volatile int onLineNumber;//在线人数
    private static boolean isOwnerRoom;
    private static String roomOwnerId = "";//房契拥有者id
    private static boolean isPurchased;
    private static int roomPrice = 0;//房间价格
    private static String background = "";//房间背景
    private static String bgType = "";//房间背景类型
    private static int maxChairNumber = VoiceConstants.MAX_CHAIR_4;//默认最大座位8个
    private static boolean isLobby = false; //是否是游戏大厅房间
    private static boolean isHiddenMsg = false; //是否屏蔽了消息
    private static int userCount = 0;         //房间人数
    public static boolean isKtv = false;  //这里加个保险处理
    //红包相关
    private static String mLastRedpacketId;  //房间里最新的红包
    private static int mRemainTime;  //未抢完钻石返回发送者的时间
    private static long mEnterTime;  //进入房间的时间
    private static int mMinDiamond;  //红包最小钻石数
    private static int mMaxPacket;  //红包最大红包个数
    private static int mMinPacket;  //红包最小红包个数


    public static boolean needChangeUI = false; //是否需要更改UI(ktv<->其他房间)需要更改

    public static String transferTypeToShareTitle() {
        String title = "";
        try {
            title = VoiceTopicManager.getInstance().findTopicDesByType(getRoomType());
        } catch (Exception e) {
        }
        return title;
    }

    /**
     * 是否是游戏大厅房间
     */
    public static boolean isLobby() {
        return TextUtils.equals(roomType, com.networktoolkit.transport.Constant.GAME_TYPE_PAIR) || isLobby;
    }

    public static void setIsLobby(boolean isLobby) {
        VoiceRoomConfig.isLobby = isLobby;
    }

    public static void setRoomType(String roomType) {
        VoiceRoomConfig.roomType = roomType;
    }

    public static void setRoomId(String roomId) {
        VoiceRoomConfig.roomId = roomId;
    }

    public static void setPsw(String psw) {
        VoiceRoomConfig.psw = psw;
    }

    public static String getRoomType() {
        return roomType;
    }

    public static String getRoomId() {
        return roomId;
    }

    public static String getPsw() {
        return psw;
    }

    public static String getRoomTitle() {
        return roomTitle;
    }

    public static void setRoomTitle(String roomTitle) {
        VoiceRoomConfig.roomTitle = roomTitle;
    }

    public static boolean isIsPlaying() {
        return isPlaying;
    }

    public static void setIsPlaying(boolean isPlaying) {
        VoiceRoomConfig.isPlaying = isPlaying;
    }

    public static String getBackground() {
        return background;
    }

    public static void setBackground(String background) {
        if (!TextUtils.isEmpty(background)) {
            VoiceRoomConfig.background = background;
        }
    }

    public static String getBgType() {
        return bgType;
    }

    public static void setBgType(String bgType) {
        VoiceRoomConfig.bgType = bgType;
    }

    //当前是否是头像模糊效果
    public static boolean isBlurBg() {
        return TextUtils.equals(VoiceConstants.BG_BLUR, bgType);
    }

    //是否是"谁是卧底"房
    public static boolean isSpyRoom() {
        return TextUtils.equals(VoiceConstants.SPY_TAG, getRoomType());
    }

    public static int getOnSeatNumber() {
        return onSeatNumber;
    }

    public static boolean isIsSmallWindowStyle() {
        return isSmallWindowStyle;
    }

    public static void setIsSmallWindowStyle(boolean isSmallWindowStyle) {
        VoiceRoomConfig.isSmallWindowStyle = isSmallWindowStyle;
    }

    public static synchronized void setOnSeatNumber(int onSeatNumber) {
        VoiceRoomConfig.onSeatNumber = onSeatNumber;
    }

    public static int getOnLineNumber() {
        return onLineNumber;
    }

    public static void setOnLineNumber(int onLineNumber) {
        VoiceRoomConfig.onLineNumber = onLineNumber;
    }

    public static boolean isIsComeInBySW() {
        return isComeInBySW;
    }

    public static void setIsComeInBySW(boolean isComeInBySW) {
        VoiceRoomConfig.isComeInBySW = isComeInBySW;
    }

    public static synchronized void addOnSeatNumber() {
        VoiceRoomConfig.onSeatNumber++;
    }

    public static synchronized void subOnSeatNumber() {
        VoiceRoomConfig.onSeatNumber--;
    }

    public static boolean isIsSupportSW() {
        return isSupportSW;
    }

    public static void setIsSupportSW(boolean isSupportSW) {
        VoiceRoomConfig.isSupportSW = isSupportSW;
    }

    public static boolean isIsSupportDeed() {
        return isSupportDeed;
    }

    public static void setIsSupportDeed(boolean isSupportDeed) {
        VoiceRoomConfig.isSupportDeed = isSupportDeed;
    }

    public static boolean isIsSupportChangeSeat() {
        return isSupportChangeSeat;
    }

    public static void setIsSupportChangeSeat(boolean isSupportChangeSeat) {
        VoiceRoomConfig.isSupportChangeSeat = isSupportChangeSeat;
    }

    public static boolean isIsSupportChangeBg() {
        return isSupportChangeBg;
    }

    public static void setIsSupportChangeBg(boolean isSupportChangeBg) {
        VoiceRoomConfig.isSupportChangeBg = isSupportChangeBg;
    }

    public static boolean isIsOwnerRoom() {
        return isOwnerRoom;
    }

    public static void setIsOwnerRoom(boolean isOwnerRoom) {
        VoiceRoomConfig.isOwnerRoom = isOwnerRoom;
    }

    public static boolean isIsPurchased() {
        return isPurchased;
    }

    public static void setIsPurchased(boolean isPurchased) {
        VoiceRoomConfig.isPurchased = isPurchased;
    }

    public static String getRoomOwnerId() {
        return roomOwnerId;
    }

    public static void setRoomOwnerId(String roomOwnerId) {
        VoiceRoomConfig.roomOwnerId = roomOwnerId;
    }

    public static int getRoomPrice() {
        return roomPrice;
    }

    public static void setRoomPrice(int roomPrice) {
        VoiceRoomConfig.roomPrice = roomPrice;
    }

    public static boolean isIsHiddenMsg() {
        return isHiddenMsg;
    }

    public static void setIsHiddenMsg(boolean isHiddenMsg) {
        VoiceRoomConfig.isHiddenMsg = isHiddenMsg;
    }

    public static int getUserCount() {
        return userCount;
    }

    public static void setUserCount(int userCount) {
        VoiceRoomConfig.userCount = userCount;
    }

    public static boolean isIsKTV() {
        return TextUtils.equals(roomType, com.networktoolkit.transport.Constant.GAME_TYPE_KTV) || isKtv;
    }

    //这里将后台传递的数量-1
    public static void setMaxChairNumber(int maxChairNumber) {
        if (maxChairNumber <= 0) {
            VoiceRoomConfig.maxChairNumber = 0;
        }
        if (maxChairNumber > 0 && maxChairNumber <= 4) {
            VoiceRoomConfig.maxChairNumber = 4;
        }
        if (maxChairNumber > 4 && maxChairNumber <= 8) {
            VoiceRoomConfig.maxChairNumber = 8;
        }
        if (maxChairNumber > 8 && maxChairNumber <= 12) {
            VoiceRoomConfig.maxChairNumber = 12;
        }
        if (maxChairNumber > 12) {
            VoiceRoomConfig.maxChairNumber = 16;
        }
    }

    public static int getMaxChairNumber() {
        return maxChairNumber;
    }

    public static int getStatusBarColorByType(String roomType) {
        return R.color.color_voice_status_bar;
    }

    //根据房间类型返回不同的背景图片
//    public static int getRoomBgByType(String roomType) {
//        if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_CHAT, roomType)) {
//            return R.mipmap.ico_voice_bg_chat;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_SING, roomType)) {
//            return R.mipmap.ico_voice_bg_ksong;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_LOVE, roomType)) {
//            return R.mipmap.ico_voice_bg_relation;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_FATE, roomType)) {
//            return R.mipmap.ico_voice_bg_fortune;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_MERRY, roomType)) {
//            return R.mipmap.ico_voice_bg_wedding;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_SEEK, roomType)) {
//            return R.mipmap.ico_voice_bg_feeling;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_GAME, roomType)) {
//            return R.mipmap.ico_voice_bg_game;
//        } else if (TextUtils.equals(VoiceConstants.VOICE_ROOM_TYPE_SPY, roomType)) {
//            return R.mipmap.ico_voice_bg_spy;
//        }
//        return VoiceConstants.VOICE_ROOM_TYPE_NO_BG;
//    }

    //根据房间类型返回不同的speakView颜色
    public static int getSpeakBarColorByType(String roomType) {
        return 0;
    }

    //房间里最新的红包
    public static String getLastRedpacketId() {
        return mLastRedpacketId;
    }
    public static void setLastRedpacketId(String packetId) {
        VoiceRoomConfig.mLastRedpacketId = packetId;
    }

    public static int getMinDiamond() {
        return mMinDiamond;
    }
    public static void setMinDiamond(int minDiamond) {
        VoiceRoomConfig.mMinDiamond = minDiamond;
    }
    public static int getMaxPacket() {
        return mMaxPacket;
    }
    public static void setMaxPacket(int maxPacket) {
        VoiceRoomConfig.mMaxPacket = maxPacket;
    }
    public static int getMinPacket() {
        return mMinPacket;
    }
    public static void setMinPacket(int minPacket) {
        VoiceRoomConfig.mMinPacket = minPacket;
    }

    public static long getEnterTime() {
        return mEnterTime;
    }
    public static void setEnterTime(long enterTime) {
        VoiceRoomConfig.mEnterTime = enterTime;
    }
    public static int getRemainTime() {
        return mRemainTime;
    }
    public static void setRemainTime(int remainTime) {
        VoiceRoomConfig.mRemainTime = remainTime;
    }
    //谁是卧底相关配置
    public static class SpyConfig implements Keepable {
        private static boolean isDefaultSetting;//是否是默认设置
        private static int spyNumber;//卧底人数
        private static int goodNumber;//好人人数
        private static int speakTime;//可说话时间
        private static boolean addOneSpy;//是否添加了卧底
        private static boolean setSpyToGhost;//是否设置卧底为幽灵
        private static boolean canInsertMic;//可否插麦
        private static boolean canGuessWord;//可否猜词
        private static String first;//卧底词
        private static String second;//好人词
        private static boolean isConfiging = false;
        private static boolean isConfiged = false;

        private static VoiceSpyStart spyStart;
        private static VoiceSpyConfigBean spyConfig;

        public static void setSpyStart(VoiceSpyStart spyStart) {
            SpyConfig.spyStart = spyStart;
            spyNumber = spyStart.role_map.undercover;
            goodNumber = spyStart.role_map.people;
        }

        public static void setSpyConfig(VoiceSpyConfigBean spyConfig) {
            SpyConfig.spyConfig = spyConfig;

            addOneSpy = spyConfig.more_undercover;
            setSpyToGhost = spyConfig.has_ghost;
            canInsertMic = spyConfig.can_cut_speaker;
            canGuessWord = spyConfig.can_guess;
            first = spyConfig.first;
            second = spyConfig.second;
        }

        public static int getSpeakTime() {
            return speakTime;
        }

        public static void setSpeakTime(int speakTime) {
            SpyConfig.speakTime = speakTime;
        }

        public static boolean isAddOneSpy() {
            return addOneSpy;
        }

        public static boolean isSetSpyToGhost() {
            return setSpyToGhost;
        }

        public static boolean isCanInsertMic() {
            return canInsertMic;
        }

        public static boolean isCanGuessWord() {
            return canGuessWord;
        }

        public static int getSpyNumber() {
            return spyNumber;
        }

        public static int getGoodNumber() {
            return goodNumber;
        }

        public static String getFirst() {
            return first;
        }

        public static String getSecond() {
            return second;
        }

        public static boolean isIsDefaultSetting() {
            return isDefaultSetting;
        }

        public static void setIsDefaultSetting(boolean isDefaultSetting) {
            SpyConfig.isDefaultSetting = isDefaultSetting;
        }

        public static boolean isIsConfiging() {
            return isConfiging;
        }

        public static void setIsConfiging(boolean isConfiging) {
            SpyConfig.isConfiging = isConfiging;
        }

        public static boolean isIsConfiged() {
            return isConfiged;
        }

        public static void setIsConfiged(boolean isConfiged) {
            SpyConfig.isConfiged = isConfiged;
        }

        public static void setAddOneSpy(boolean addOneSpy) {
            SpyConfig.addOneSpy = addOneSpy;
        }

        public static void setSetSpyToGhost(boolean setSpyToGhost) {
            SpyConfig.setSpyToGhost = setSpyToGhost;
        }

        public static void setCanInsertMic(boolean canInsertMic) {
            SpyConfig.canInsertMic = canInsertMic;
        }

        public static void setCanGuessWord(boolean canGuessWord) {
            SpyConfig.canGuessWord = canGuessWord;
        }

        public static void setFirst(String first) {
            SpyConfig.first = first;
        }

        public static void setSecond(String second) {
            SpyConfig.second = second;
        }

        //退出谁是卧底模式需要清除
        public static void clearSpyConfig() {

            addOneSpy = false;
            setSpyToGhost = false;
            canInsertMic = false;
            canGuessWord = false;

            clearLenGameConfig();
        }

        //一轮游戏结束需要清除
        public static void clearLenGameConfig() {
            isDefaultSetting = false;
            first = "";
            second = "";
            spyNumber = 0;
            goodNumber = 0;
            speakTime = 0;
            spyStart = null;
            spyConfig = null;
            isConfiging = false;
            isConfiged = false;
        }
    }

    public static void clearConfig() {
        roomTitle = "";
        roomType = "";
        roomId = "";
        psw = "";
        background = "";
        isPlaying = false;
        isSupportSW = false;
        isSmallWindowStyle = false;
        onSeatNumber = 0;
        isOwnerRoom = false;
        isPurchased = false;
        roomPrice = 0;
        maxChairNumber = VoiceConstants.MAX_CHAIR_8;
        roomOwnerId = "";
        isSupportSW = false;
        isSupportDeed = false;
        isHiddenMsg = false;
        userCount = 0;
        isLobby = false;
        mLastRedpacketId = "";
        mEnterTime = 0L;
        mRemainTime = 1;
        SpyConfig.clearSpyConfig();

    }
}
