package orangelab.project.voice.musiccompany.tecent.music;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.androidtoolkit.FileToolKit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.ToastToolKit;
import com.tencent.TMG.ITMGContext;
import com.tencent.av.sdk.AVError;
import com.tencent.av.sig.AuthBuffer;
import com.toolkit.action.Keepable;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.musiccompany.event.MusicEvent;
import orangelab.project.voice.utils.PositionHelper;

/**
 * 伴奏管理类，使用tencent sdk进行
 */
public class MusicCompanyManager implements TMGDispatcherBase, Keepable {

    private static String TAG = "MusicCompanyManager";

    private Map<String, Integer> mUserInfo = new HashMap<>();
    private static final int STATE_NONE = 0x00000000;
    private static final int STATE_AUDIO = 0x00000001;
    private static final int STATE_VIDEO = 0x00000020;

    private static MusicCompanyManager instance;
    private Context context;

    public static MusicCompanyManager getInstance(Context context) {
        if (instance == null) {
            synchronized (MusicCompanyManager.class) {
                if (instance == null) {
                    instance = new MusicCompanyManager(context);
                }
            }
        }
        return instance;
    }

    private MusicCompanyManager(Context context) {
        this.context = context;
        //context = MainApplication.getInstance().getApplicationContext();
    }

    long mCurrentTime = 0;

    /**
     * 外部公共进入方法
     * 所有人都调用此方法进入
     */
    public void startFuckMusic() {

        Log.i(TAG, "start context...");

        //接入步骤0：准备好EnginePoll
        EnginePollHelper.createEnginePollHelper();

        //接入步骤1：获取相关信息，由腾讯云申请， RoomID为大于等于6位的整数
        //AppVersion 为客户端的版本信息，查Log， 查BUG有用
        String sdkAppId = MusicCompanyConfig.AppID;
        String key = MusicCompanyConfig.key;
        String identifier = MusicCompanyConfig.getIdentifier();
        String strRoomID = MusicCompanyConfig.roomID;
        String strRoomType = MusicCompanyConfig.roomType;

        String strMaxMixCount = MusicCompanyConfig.strMaxMixCount;  //最大混音数
        String strSpeakerType = MusicCompanyConfig.strSpeakerType;  //1 通话 2 媒体
        String appVersion = MusicCompanyConfig.appVersion;          //appVersion

        //接入步骤2：用户自己实现回调分发函数
        TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENTER_ROOM, this);

        //接入步骤3：将自己实现的回调函数注册给SDK
        ITMGContext.GetInstance(context).SetTMGDelegate(TMGCallbackDispatcher.getInstance().getItmgDelegate());

        //设置混音路数
        ITMGContext.GetInstance(context).SetRecvMixStreamCount(Integer.valueOf(strMaxMixCount));

        //设置声音类型, 1--电话音量,  2--媒体音量
        ITMGContext.GetInstance(context).SetAdvanceParams("SetSpeakerStreamType", strSpeakerType);

        // 设置是否测试环境
        String strTestEnv = MusicCompanyConfig.strTestEnv; // 0 正常环境 1 测试环境
        if (strTestEnv.compareTo("1") == 0) {
            PLog.e(TAG, "进入测试环境");
        } else {
            PLog.e(TAG, "进入正式环境");
        }

        //这个接口只能在进房前设置一次
        ITMGContext.GetInstance(context).SetAdvanceParams("TestEnv", strTestEnv);

        //接入步骤4：将步骤1中的信息设置到SDK中
        ITMGContext.GetInstance(context).Init(sdkAppId, identifier);
        ITMGContext.GetInstance(context).SetAppVersion(appVersion);

        //接入步骤5：生成AuthBuffer，鉴权秘钥
        mCurrentTime = System.currentTimeMillis();
        long nExpUTCTime = 1800 + mCurrentTime / 1000L;
        AuthBuffer instance = AuthBuffer.getInstance();
        if (TextUtils.isEmpty(strRoomID)) {
            ToastToolKit.showShort(R.string.str_music_player_roomid_empty_tips);
            return;
        }
        if (instance != null) {
            try {
                byte[] authBuffer = instance.genAuthBuffer(Integer.parseInt(sdkAppId), Integer.parseInt(strRoomID),
                        identifier, key, (int) nExpUTCTime, (int) ITMGContext.ITMG_AUTH_BITS_DEFAULT);
                //接入步骤6：用生成的秘钥进房， 会收到ITMG_MAIN_EVENT_TYPE_ENTER_ROOM的回调， 标识进房成功
                ITMGContext.GetInstance(context).EnterRoom(Integer.parseInt(strRoomID), Integer.valueOf(strRoomType), authBuffer);
            } catch (Exception e) {
                ReportEventUtils.reportError(e);
                ToastToolKit.showShort(R.string.str_music_player_gme_error);
            }
        } else {
            ToastToolKit.showShort(R.string.str_music_tip_not_support);
        }
    }

    private void enterMusicCompany() {
        PLog.e(TAG, "enterMusicCompany");
        MusicEventHandler handler = new MusicEventHandler();
    }

    @Override
    public void OnEvent(ITMGContext.ITMG_MAIN_EVENT_TYPE type, Intent data) {
        int nErrCode = TMGCallbackHelper.ParseIntentParams2(data).nErrCode;
        String strMsg = TMGCallbackHelper.ParseIntentParams2(data).strErrMsg;

        //接入步骤7：收到进房信令， 进房成功， 可以操作设备， 参看RoomActivity
        if (nErrCode == AVError.AV_OK) {
            enterMusicCompany();
            finish();
        } else {
            PLog.e(TAG, String.format("result=%d, errorInfo=%s", nErrCode, strMsg));
            PLog.e(TAG, String.format("result=%d, errorInfo=%s", nErrCode, strMsg));
        }
    }

    /**
     * 暂停
     */
    public void pause() {
        ITMGContext.GetInstance(context).Pause();
    }

    /**
     * 恢复
     */
    public void resume() {
        ITMGContext.GetInstance(context).Resume();
    }

    /**
     * 生命周期模仿
     */
    public void finish() {
        //接入步骤8：降低内存， 将无用的监听事件清理出去（看需求是否清除，业务逻辑判定）
        TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENTER_ROOM, this);
    }

    /**
     * 打开麦克风
     */
    public void openMic() {
        ITMGContext.GetInstance(context).GetAudioCtrl().EnableMic(true);
        changeMicVolume(MusicCompanyConfig.mic);
    }

    /**
     * 关闭麦克风
     */
    public void closeMic() {
        ITMGContext.GetInstance(context).GetAudioCtrl().EnableMic(false);
    }

    /**
     * 打开扬声器
     */
    public void openSpeaker() {
        ITMGContext.GetInstance(context).GetAudioCtrl().EnableSpeaker(true);
        changeSpeakerVolume(MusicCompanyConfig.speakerMax);
    }

    /**
     * 关闭扬声器
     */
    public void clsoeSpeaker() {
        ITMGContext.GetInstance(context).GetAudioCtrl().EnableSpeaker(false);
    }

    /**
     * 设置麦克风音量
     * 0-150
     */
    public void changeMicVolume(int volume) {
        ITMGContext.GetInstance(context).GetAudioCtrl().SetMicVolume(volume);
        PLog.e(TAG, "changeMicVolume: " + volume);
    }

    /**
     * 设置扬声器音量
     */
    public void changeSpeakerVolume(int volume) {
        ITMGContext.GetInstance(context).GetAudioCtrl().SetSpeakerVolume(volume);
        PLog.e(TAG, "changeSpeakerVolume: " + volume);
    }

    public void changeCompanyVolume(int volume) {
        ITMGContext.GetInstance(context).GetAudioEffectCtrl().SetAccompanyVolume(volume);
        PLog.e(TAG, "changeCompanyVolume: " + volume);
    }

    /**
     * 修改房间音频类型
     */
    public void changeRoomType(int type) {
        try {
            ITMGContext.GetInstance(context).GetRoom().ChangeRoomType(type);
        } catch (Exception e) {
            PLog.e(TAG, "error,changeRoomType: " + e.getMessage());
        }
    }

    /**
     * 开始伴奏
     * path: 伴奏文件路径
     */
    public void startMusicCompany(String path) {
        // path:播放文件路径 true:开启混音 1:循环一次(-1表示无限循环)
        ITMGContext.GetInstance(context).GetAudioEffectCtrl().StartAccompany(path, true, 1);
        EventBus.getDefault().post(new MusicEvent.MusicFileSetEvent(getMusicMaxLength()));
        EventBus.getDefault().post(new MusicEvent.MusicCompanyStart());
    }

    /**
     * 停止伴奏
     */
    public void stopMusicCompany() {
        ITMGContext.GetInstance(context).GetAudioEffectCtrl().StopAccompany(1);
    }

    /**
     * 退出伴奏房间
     */
    public void exitMusicCompany() {
        try {
            ITMGContext.GetInstance(context).ExitRoom();
        } catch (Exception e) {
            e.printStackTrace();
            //EventBus.getDefault().post(new MusicEvent.ExitKTVRoomEvent());
        }
    }

    /**
     * 获取伴奏播放进度
     */
    public long getCurrentPosition() {
        return ITMGContext.GetInstance(context).GetAudioEffectCtrl().GetAccompanyFileCurrentPlayedTimeByMs();
    }

    /**
     * 获取伴奏最大进度
     */
    public long getMusicMaxLength() {
        return ITMGContext.GetInstance(context).GetAudioEffectCtrl().GetAccompanyFileTotalTimeByMs();
    }

    /**
     * 伴奏音乐事件处理类
     */
    class MusicEventHandler implements TMGDispatcherBase, Keepable {

        public MusicEventHandler() {
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVNET_TYPE_USER_UPDATE, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_EXIT_ROOM, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ROOM_DISCONNECT, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_MIC, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_MIC, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_SPEAKER, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_SPEAKER, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVNET_TYPE_USER_VOLUMES, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ACCOMPANY_FINISH, this);
            TMGCallbackDispatcher.getInstance().AddDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_CHANGE_ROOM_TYPE, this);

            openSpeaker();
            if (PositionHelper.isSinger()) {
                openMic();

                //这里也关掉耳返
                ITMGContext.GetInstance(context).GetAudioCtrl().EnableLoopBack(false);
                //打开耳返
                File file = FileToolKit.loadFile2Mp3(MusicCompanyConfig.playUrl);
                if (file != null) {
                    String path = file.getAbsolutePath();
                    Uri uri = Uri.parse(path);
                    if (TextUtils.equals("file", uri.getScheme())) {
                        path = uri.getPath();
                    }
                    PLog.e(TAG, "play path = " + path);
                    try {
                        startMusicCompany(path);
                    } catch (Error e) {
                        e.printStackTrace();
                        ToastToolKit.showShort(R.string.str_music_tip_not_support);
                        PLog.e(TAG, "error occur when start company:" + e.getMessage());
                    }
                }
            } else {
                closeMic();
                ITMGContext.GetInstance(context).GetAudioCtrl().EnableLoopBack(false);
            }
        }

        @Override
        public void OnEvent(ITMGContext.ITMG_MAIN_EVENT_TYPE type, Intent data) {
            PLog.e(TAG, "OnEvent:" + type);
            if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVNET_TYPE_USER_UPDATE == type) {
                //接入步骤11： 步骤9开了自己的音频， 会收到成员状态变化状态信息
                TMGCallbackHelper.ParamsUerInfo info = TMGCallbackHelper.ParseUserInfoUpdateInfoIntent(data);
                for (String id : info.identifierList) {
                    PLog.e(TAG, String.format("onEndpointsUpdateInfo|eventid=%d, id=%s", info.nEventID, id));
                    int state = STATE_NONE;
                    if (mUserInfo.containsKey(id)) {
                        state = mUserInfo.get(id);
                    }
                    switch (info.nEventID) {
                        case 3:
                            state |= STATE_VIDEO;
                            break;
                        case 4:
                            state &= (~STATE_VIDEO);
                            break;
                        case 5:
                            state |= STATE_AUDIO;
                            break;
                        case 6:
                            state &= (~STATE_AUDIO);
                            break;
                    }

                    if (state != STATE_NONE) {
                        mUserInfo.put(id, state);
                    } else if (mUserInfo.containsKey(id)) {
                        mUserInfo.remove(id);
                    }
                }

                //更新成员状态
                int nEventID = data.getIntExtra("event_id", 0);
                String[] identifierList = data.getStringArrayExtra("user_list");
                PLog.e(TAG, "event- " + nEventID);
                if (nEventID == ITMGContext.ITMG_EVENT_ID_USER_ENTER) {
                    //有成员进入房间
                } else if (nEventID == ITMGContext.ITMG_EVENT_ID_USER_EXIT) {
                    //有成员退出房间
                } else if (nEventID == ITMGContext.ITMG_EVENT_ID_USER_HAS_AUDIO) {
                    //有成员开启麦克风
                } else if (nEventID == ITMGContext.ITMG_EVENT_ID_USER_NO_AUDIO) {
                    //有成员关闭麦克风
                }

            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_MIC == type) {
                //接入步骤10： 收到mic事件
                TMGCallbackHelper.ParamsAudioDeviceInfo info = TMGCallbackHelper.ParseAudioDeviceInfoIntent(data);
                if (info.nErrCode == AVError.AV_OK) {
                    PLog.e(TAG, "打开麦克风成功");
                } else {
                    PLog.e(TAG, "打开麦克风失败");
                }
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_MIC == type) {
                //接入步骤10： 收到mic事件
                TMGCallbackHelper.ParamsAudioDeviceInfo info = TMGCallbackHelper.ParseAudioDeviceInfoIntent(data);
                if (info.nErrCode == AVError.AV_OK) {
                    PLog.e(TAG, "关闭麦克风成功");
                } else {
                    changeMicVolume(0);//关闭麦克风失败加的保险
                    PLog.e(TAG, "关闭麦克风失败");
                }
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_SPEAKER == type) {
                //接入步骤13： 收到扬声器事件
                TMGCallbackHelper.ParamsAudioDeviceInfo info = TMGCallbackHelper.ParseAudioDeviceInfoIntent(data);
                if (info.nErrCode == AVError.AV_OK) {
                    PLog.e(TAG, "开启扬声器成功");
                } else {
                    PLog.e(TAG, "开启扬声器失败");
                }
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_SPEAKER == type) {
                //接入步骤13： 收到扬声器事件
                TMGCallbackHelper.ParamsAudioDeviceInfo info = TMGCallbackHelper.ParseAudioDeviceInfoIntent(data);
                if (info.nErrCode == AVError.AV_OK) {
                    PLog.e(TAG, "关闭扬声器成功");
                } else {
                    PLog.e(TAG, "关闭扬声器失败");
                }
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_EXIT_ROOM == type) {
                //上报收到gme退出房间事件
                ReportEventUtils.reportReceivedExitKtvEvent();
                //通知申请语音权限
                //EventBus.getDefault().post(new MusicEvent.ExitKTVRoomEvent());
                //接入步骤15： 收到退房成功事件
                onDestory();
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ROOM_DISCONNECT == type) {
                //接入步骤16： 房间被迫退出
                onDestory();
                PLog.e(TAG, "房间网络原因断链");
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVNET_TYPE_USER_VOLUMES == type) {
                // 更改声音大小
                String info = "Volume:\n";
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    for (String key : bundle.keySet()) {
                        info += " " + key + " => " + bundle.get(key) + "\n";
                    }
                }
                PLog.e(TAG, "info  = " + info);
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ACCOMPANY_FINISH == type) {
                int code = data.getIntExtra("resutl", 0);
                boolean is_finished = data.getBooleanExtra("is_finished", false);
                PLog.e(TAG, "ITMG_MAIN_EVENT_TYPE_ACCOMPANY_FINISH, code = " + code + ", is_finished = " + is_finished);

                if (is_finished) {
                    if (PositionHelper.isSinger()) {
                        MusicSocketHelper.INSTANCE.sendStopSing();
                    }
                }
                Log.i(TAG, String.format("is_finished:%b,code:%d", is_finished, code));
            } else if (ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_CHANGE_ROOM_TYPE == type) {
                //房间类型变化
                int code = data.getIntExtra("resutl", 0);
                String strErrInfo = data.getStringExtra("error_info");
                int nNewRoomType = data.getIntExtra("new_room_type", 0);
                int nSubEventType = data.getIntExtra("sub_event_type", 0);
                switch (nSubEventType) {
                    case ITMGContext.ITMG_ROOM_CHANGE_EVENT_ENTERROOM:
                        PLog.e(TAG, String.format("已经切换为原始房间类型:%d!", nNewRoomType));
                        break;
                    case ITMGContext.ITMG_ROOM_CHANGE_EVENT_START:
                        break;
                    case ITMGContext.ITMG_ROOM_CHANGE_EVENT_COMPLETE:
                        PLog.e(TAG, String.format("房间已经被切换为:%d!", nNewRoomType));
                        break;
                    case ITMGContext.ITMG_ROOM_CHANGE_EVENT_REQUEST:
                        PLog.e(TAG, String.format("切换房间类型返回值:%d!", code));
                        break;
                }

                PLog.e(TAG, "room type changed, info is " + strErrInfo);
            }
        }

        /**
         * 生命周期方法模仿
         */
        public void onDestory() {
            //清空回调函数
            //接入步骤16： 清空多余事件，保证内存， 完成一次通话以及结束事件。回到原始状态
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVNET_TYPE_USER_UPDATE, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_EXIT_ROOM, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ROOM_DISCONNECT, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_MIC, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_MIC, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_DISABLE_SPEAKER, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ENABLE_SPEAKER, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_ACCOMPANY_FINISH, this);
            TMGCallbackDispatcher.getInstance().RemoveDelegate(ITMGContext.ITMG_MAIN_EVENT_TYPE.ITMG_MAIN_EVENT_TYPE_CHANGE_ROOM_TYPE, this);

            mUserInfo.clear();
        }
    }
}
