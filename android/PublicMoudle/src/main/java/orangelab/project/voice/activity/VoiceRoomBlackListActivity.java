package orangelab.project.voice.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.SpaceItemDecoration;
import orangelab.project.voice.adapter.VoiceRoomBlackListAdapter;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.model.BlackListParserMode;
import orangelab.project.voice.model.VoiceBlackListMode;

import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.NetworkHelpers;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

public class VoiceRoomBlackListActivity extends SafeActivity implements Destroyable {

    private View ivBack;
    private RecyclerView recyclerView;
    private VoiceRoomBlackListAdapter adapter;
    private List<VoiceBlackListMode> blackList;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_room_black_list);
        init();
        getBlackList();
    }

    private void init() {
        initWindow();
        initView();
        initData();
        initListener();
    }

    private void initWindow() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        Utils.setWindowStatusBarColor(this, R.color.vocie_status_bar);
    }

    private void initView() {
        ivBack = this.findViewById(R.id.iv_back);
        recyclerView = (RecyclerView) this.findViewById(R.id.list_black_list);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpaceItemDecoration(ScreenUtils.dip2px(4)));
    }

    private void initData() {
        blackList = new ArrayList<>();
        adapter = new VoiceRoomBlackListAdapter(this, blackList);
        recyclerView.setAdapter(adapter);
    }

    private void initListener() {
        ivBack.setOnClickListener(v -> onBackPressed());
        initServerMessageEvent();
        initKickOutListener();

        adapter.setOnItemClickListener(userItem -> {
            if (judgeSocketConnect()) {
                RoomSocketEngineHelper.removeVoiceBlack(userItem.id);
            }
        });
    }

    private void getBlackList() {
        RoomSocketEngineHelper.getVoiceBlackList();
    }

    //接受socket推送的消息
    private void initServerMessageEvent() {
        VoiceServerMessageHandler.getInstance().registerUIHandler(messageHandler);
    }

    private void initKickOutListener() {
        RxToolKit.Build(this, FinishConversationEvent.class)
                .action(event -> runOnUiThreadSafely(() -> finish()))
                .register();
    }

    private boolean judgeSocketConnect() {
        //网络断开或者socket没连上
        boolean isConnect = NetworkHelpers.isNetworkAvailable(MainApplication.getInstance()) &&
                VoiceRoomGameSocketManager.getInstance().isConnected();
        if (!isConnect) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
            return false;
        }
        return true;
    }

    @Override
    public void destroy() {

        RxToolKit.UnRegister(this);
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(messageHandler);

        if (blackList != null) {
            blackList.clear();
            adapter.notifyDataSetChanged();
        }
        if (adapter != null) {
            adapter.destroy();
        }
    }

    @Override
    public void finish() {
        destroy();
        super.finish();
    }

    public static void launch(Context context) {
        if (context != null) {
            context.startActivity(new Intent(context, VoiceRoomBlackListActivity.class));
        }
    }

    private AbstractSocketMessageHandler messageHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handleKickOut(VoiceMessageBean messageBean) {
            RoomSocketEngineHelper.getVoiceBlackList();
        }

        @Override
        public void handleBlackList(VoiceMessageBean messageBean) {
            BlackListParserMode mode = GsonHelper.getGson().fromJson(messageBean.payload.toString(), BlackListParserMode.class);
            blackList = mode.users;
            adapter.updateBlackList(blackList);
        }

        @Override
        public void handleUnBlack(VoiceMessageBean messageBean) {
            RoomSocketEngineHelper.getVoiceBlackList();
        }

        @Override
        public void destroy() {

        }
    };
}
