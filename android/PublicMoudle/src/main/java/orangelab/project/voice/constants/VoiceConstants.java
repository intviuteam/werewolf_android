package orangelab.project.voice.constants;

/**
 * Created by lloydfinch on 28/11/2017.
 */

public interface VoiceConstants {

    long CODE_CONNECT = 100011;
    long CODE_DISCONNECT = 100012;

    //全局//
    int CODE_1000 = 1000;//正常重连
    int CODE_1032 = 1032;//断网期间被踢出重连返回此code
    String CODE = "code";
    String DATA = "data";
    String RECONNECT = "reconnect";
    //全局//

    //"房间类型相关"//
    String GET_TOPIC_LIST = "/config/audio";//获取房间话题列表
    String GET_BACKGROUND = "/room/background";
    String ADD_BACKGROUND = "/room/addImage";
    String REPLACE_BACKGROUND = "/room/replaceImage";
    String REMOVE_BACKGROUND = "/room/deleteImage";
    String VOICE_ROOM_CONFIG_API = "/config/room";

    int VOICE_ROOM_TYPE_NO_BG = -1;
    String VOICE_ROOM_TYPE_CHAT = "chat";
    String VOICE_ROOM_TYPE_SING = "sing";
    String VOICE_ROOM_TYPE_LOVE = "love";
    String VOICE_ROOM_TYPE_FATE = "fate";
    String VOICE_ROOM_TYPE_MERRY = "merry";
    String VOICE_ROOM_TYPE_SEEK = "seek";
    String VOICE_ROOM_TYPE_GAME = "game";
    String VOICE_ROOM_TYPE_SPY = "undercover";
    //"房间类型相关"//


    //"谁是卧底相关"//
    int USER_STATUS_PREPARE = 10001;//准备中
    int USER_STATUS_VOTE = 10002;//投票中
    int USER_STATUS_OFFLINE = 10003;//离线中
    int MASTER_STATUS_SETTING = 20001;//设置中
    int MASTER_STATUS_SETTING_OVER = 20002;//设置完成
    String SPY_TAG = "undercover";
    String GOOD_TAG = "people";
    String SPY_START_CONFIG = "uc_start_config";
    String SPY_STOP_CONFIG = "uc_stop_config";
    String SPY_UPDATE_CONFIG = "uc_update_config";
    String SPY_HAS_GHOST = "has_ghost";
    String SPY_CAN_CUT_SPEAKER = "can_cut_speaker";
    String SPY_CAN_GUESS = "can_guess";
    String SPY_FIRST_WORD = "first";
    String SPY_SECOND_WORD = "second";
    String SPY_MORE_UNDERCOVER = "more_undercover";
    String SPY_GET_WORDS = "uc_get_words";
    String SPY_GET_WORDS_RESPONSE = "uc_get_words_result";
    String SPY_SYSTEM_WORDS = "system";
    String SPY_CUSTOM_WORDS = "custom";
    String SPY_ASSIGNED_ROLE = "assigned_role";
    String SPY_GAME_START = "start";
    String SPY_GAME_GUESS_WORD = "uc_guess_word";
    String SPY_GAME_GUESS_WORD_RESULT = "uc_guess_word_result";
    String SPY_GAME_WORD = "word";
    String SPY_GAME_VOTE = "vote";
    String SPY_GAME_VOTE_RESULT = "vote_result";
    String SPY_GAME_PREPARE = "prepare";
    String SPY_GAME_UNPREPARE = "unprepare";
    String SPY_GAME_SPEECH = "speech";
    String SPY_GAME_END_SPEECH = "end_speech";
    String SPY_GAME_OVER = "game_over";
    String SPY_GAME_TOPIC_COMMENT = "comment";
    String SPY_GAME_TOPIC_ATTITUDE = "approve";
    String SPY_GAME_DEATH_INFO = "death_info";
    String SPY_GAME_DISCONNECT = "disconnected";
    String SPY_GAME_CONNECT = "connected";
    //"谁是卧底相关"//


    //20180117添加 自由模式需要手动接受//
    String REQUEST_FREE_MODE = "request_free_mode";
    String ACCEPT_FREE_MODE = "accept_free_mode";
    String REJECT_FREE_MODE = "reject_free_mode";
    //20180117添加//

    //20180118添加 请求成为房主//
    String REQUEST_MASTER = "request_master";
    //20180118添加 请求成为房主//

    //20180119添加 已购买房间//
    String PURCHASED = "purchased";
    String PURCHASE_ROOM = "purchase_room";
    String PURCHASE_ROOM_RESULT = "purchase_room_result";
    String HAND_OVER_CREATOR = "handover_creator";
    String HAND_OVER_CREATOR_RESULT = "handover_creator_result";
    //20180119添加 已购买房间//

    //20180130 房契分享相关//
    String IS_FROM_DEED = "is_from_deed";
    String VOICE_DEED_BEAN = "voice_deed_bean";
    String SHARE_HOUSE_DEEDS = "SHARE_HOUSE_DEEDS";//房契分享与RN交互的action
    //20180130 房契分享相关//

    String EMPTY = "";
    String ADD_FRIEND_REQUEST = "/friend/add";
    String USER_DETAIL_REQUEST = "/user/info";
    String URI_APP_UPDATE = "/upgrade";
    String URI_TASK_DO = "/task/do";
    String GET_CARD_LIST = "/wealth/unusedCards";
    String REPORT_URL = "/account/report";
    String GOOGLE_PAY = "/pay/googlePay";
    String IS_NOPLAY = "/isnoplay";
    String FRIEND_DELETE = "/friend/delete";
    String ACCEPT_FRIEND = "/friend/accept?friend_id=";
    String REJECT_FRIEND = "/friend/reject?friend_id=";
    String FACEBOO_ADV_CONFIG = "/config/gg?type=";

    String FRIEND_ID = "friend_id";
    String ALIVES = "alives";
    String TYPE_ENTER = "enter";
    String TYPE_JOIN = "join";
    String TYPE_UPDATE_MASTER = "update_master";
    String TYPE_PREPARE = "prepare";
    String TYPE_UNPREPARE = "unprepare";
    String ID = "id";
    String DURATION = "duration";
    String START = "start";
    String PREPARE_TIME = "prepare_time";
    String DEATH = "death";
    String PREPARE = "prepare";
    String UNPREPARE = "unprepare";
    String NAME_LOWER = "name";
    String AVATAR_LOWER = "avatar";
    String SPEECH_DIRECTION = "speech_direction";
    String DIRECTION = "direction";
    String LEFT = "left";
    String RIGHT = "right";
    String SIGNATURE = "signature";
    String SPEAK = "speak";
    String UNSPEAK = "unspeak";
    String POSITION = "position";
    String DEST_POSITION = "dest_position";
    String ROLE = "role";
    String CARD_TYPE = "card_type";
    String SPEECH = "speech";
    String SELF_SPEECH = "SELF_SPEECH";
    String SELF_SPEECH_DANGER = "SELF_SPEECH_DANGER";
    String VOTE = "vote";
    String SPEECH_DANGER_TIME = "SPEECH_DANGER_TIME";
    String STOP_MEDIA = "STOP_MEDIA";
    String ENDSPEECH = "end_speech";
    String GAMEOVER = "game_over";
    String TYPE_VOTE_RESULT_DEATH = "death";
    String TYPE_VOTE_RESULT_NO_DEATH = "no_death";
    String TYPE_VOTE_RESULT_EQUAL = "equal";
    String WIN_PEOPELE = "people";
    String WIN_THIRD = "third_party";
    String STATE_VOTED = "voted";
    String STATE_ALIVE = "alive";
    String STATE_LEAVED = "leaved";
    String SYSTEM_MSG = "system_msg";
    String CONTENT = "content";
    String RESET_MASTER = "reset_master";
    String LEAVE = "leave";
    String REMOVE = "remove";
    String DEAD = "dead";
    String TYPE_CHAT = "chat";
    String MESSAGE = "message";
    String TAKE_AWAY_RESULT = "take_away_result";
    String MUTE_ALL = "mute_all";
    String UNMUTE_ALL = "unmute_all";
    String APPLY_ROLE = "apply_role";
    String APPLY_ROLE_RESULT = "apply_role_result";
    String NOT_ENOUGH_CARD = "not_enough_card";
    String NOT_ENOUGH_ROLES = "not_enough_roles";
    String ILLEGAL_STATE = "illegal_state";
    String FAILED = "failed";
    String NONE = "none";
    String SUCCESS = "success";
    String APPEND_TIME = "append_time";
    String APPEND_TIME_RESULT = "append_time_result";
    String LEFT_TIME = "left_time";
    String APPEND_DURATION = "append_duration";
    String TYPE_ADD_FRIEND = "add_friend";
    String SKIPED = "skiped";
    String HAND_OVER = "hand_over";
    String VOTE_TYPE = "type";
    String LOCK = "lock";
    String UNLOCK = "unlock";
    String KICK_OUT = "kick_out";
    String TO = "to";
    String FROM = "from";
    String PING = "ping";
    String MSG_ID = "msg_id";
    String CHANGE_PASSWORD = "change_password";
    String PASSWORD = "password";
    String RESTORE_ROOM = "restore_room";
    String VOICE_TITLE = "title";
    String VOICE_TITLE_TYPE = "type";
    String UPDATE_TITLE = "update_title";
    String TITLE = "title";
    String FORCE_SEAT = "force_seat";
    String UP_SEAT = "up_seat";
    String DOWN_SEAT = "down_seat";
    String HAND_OVER_MASTER = "handover_master";//语音房转让房主
    int MASTER_POSITION = 0;//房主座位号
    int CAN_ADD_SPY_NUM = 8;
    int MAX_CHAIR_0 = 0;
    int MAX_CHAIR_4 = 4;
    int MAX_CHAIR_8 = 8;//最后一个麦号
    int MAX_CHAIR_12 = 12;//最后一个麦号
    int MAX_CHAIR_16 = 16;//最后一个麦号
    int MAX_UN_SEAT = 9;//麦上最多人数
    int OVER_POSITION = 10086;//退出房间就视为去到这个位置
    String LIKE_ROOM = "like_room";
    int TEMPORARY_POSITION = -101;//上麦前不下麦的临时麦位
    String CHANGE_USER_STATE = "change_user_state";//语音房更新用户状态
    String SHOW_EMOTION = "show_emoticon";//语音房发表情
    String MODEL = "state";
    String MARK = "mark";
    String URL = "url";
    String THUMBNAIL = "thumbnail";
    String SHOW_GAME = "show_game_emoticon";
    String STAY_TIME = "stay_time";
    String SOUND = "sound";
    String RES = "res";
    int NO_RES = -1;
    String MSG = "msg";
    int BALANCE_NO_ENOUGH = 1039;//余额不足
    int NO_RECOGNIZED = 1020;//无法识别


    String POKER = "poker";
    String DICE = "dice";
    String LIGHT = "light";
    String HAND_UP = "hand_up";
    String FINGER_GUESSING = "finger_guessing";
    String POINTS = "points";
    String OCTOPUS = "octopus";
    String COINS = "coins";
    String MIC = "mic";
    String GIFTS = "gifts";
    String APPLAUSE = "applause";
    String CHILD_TYPE = "child_type";
    String SOUND_APPLAUSE = "play_applause";//鼓掌音乐id
    String VOICE_BLACK_LIST = "block_user_list";
    String ADD_BLACK = "block";
    String UN_BLACK = "unblock_user";
    String USERS = "users";
    String LIMIT = "limit";
    String FREE = "free";
    String SPACE_STR = "";
    String BACKGROUND = "background";
    String THEME = "theme";
    String VOICE_COUNT = "count";
    String USER_ID = "USER_ID";
    String USER_ID_LOWER = "user_id";
    String USER_ID_CAMEL = "userId";
    String UID = "uid";
    String EVENT_USER_MAIN_PAGE = "EVENT_USER_MAIN_PAGE";
    String EVENT_USER_LEAVE = "EVENT_USER_LEAVE";
    String EVENT_USER_START = "EVENT_USER_START";
    String EVENT_USER_LEAVE_FROM_WEB = "EVENT_USER_LEAVE_FROM_WEB";
    String PROTECT = "protect";
    String PROTECT_RESULT = "protect_result";

    //更新相关//
    String CHECK_UPGRADE = "check_upgrade";
    String DOWNLOAD_APK = "download_apk";
    String INITIATIVE_CHECK = "initiative_check";
    //更新相关//

    String UNACTIVE_WARNING = "unactive_warning";
    String ACTION_CONTINUE = "continue";

    //分享页面页面跳转进应用
    String ROOM_CODE = "room_code";
    String ROOM_PASSWORD = "password";
    String EVENT_START_BROWSABLE = "EVENT_START_BROWSABLE";
    String EVENT_CHAT = "EVENT_CHAT"; //聊天事件
    String EVENT_LEAVE_CHAT = "LEAVE_EVENT_CHAT"; //离开聊天事件
    String USER_NAME = "USER_NAME";  //用户名
    String USER_ICON = "USER_ICON";  //用户头像
    String IS_FRIEND = "IS_FRIEND";//是否是好友
    String USER_SEX = "USER_SEX";    //用户性别
    String USER_CHAT_TYPE = "USER_CHAT_TYPE"; //消息类型
    String CHAT_TIME = "CHAT_TIME";  //消息时间
    String CHAT_MESSAGE = "CHAT_MESSAGE";//聊天的消息
    String PARAMETER_USER_ID = "user_id";
    String PARAMETER_ROOM_ID = "room_id";
    String PARAMETER_TITLE = "title";
    String PARAMETER_FRIEND_ID = "friend_id";
    String MESSAGE_TYPE = "MESSAGE_TYPE";
    String MESSAGE_TYPE_CHAT = "MESSAGE_TYPE_CHAT";
    int RX_BACK_PRESSURE = 500;//rx 背压
    //分享页面页面跳转进应用


    //兼容IOS添加的消息字段
    String CONVERSATION_TYPE = "conversationType";//
    String USERNAME = "username";//
    String ACTION_ROBOT = "action_Robot";
    int ACTION_CHECK_TIME = 35;
    int DANGEROUS_TIME = 5;
    //兼容IOS添加的消息字段


    //日常任务分享/邀请
    String TYPE = "type";
    String COUNT = "count";
    String MINI_GAME_KEY = "share_type";
    String SHARE_TO_WEIXIN = "share_to_weixin_qq";
    String SHARE_TO_WEIXIN_CIRCLE = "share_to_wxwall";
    String SHARE_TO_QQ = "share_to_weixin_qq";
    String INVITE_TO_QQ = "invite";
    String INVITE_TO_WEIXIN = "invite";
    String INVITE_TO_WEIXIN_CIRCLE = "invite";
    String INVITE_TO_FRIEND = "invite";
    String INVITE = "invite";
    String SHARE = "share";
    //日常任务分享/邀请


    //送礼相关
    String GIFT_FROM = "from";
    String GIFT_TO = "to";
    String GIFT_TYPE = "type";
    String GIFT_CHAT = "gift";
    String GIFT_TICKET = "ticket";
    String SOCKET_SEND_GIFT = "send_gift";
    String SOCKET_SEND_CARD = "send_card";
    String SOCKET_SEND_GIFT_PEER_ID = "peer_id";
    String SOCKET_SEND_GIFT_GIFT_TYPE = "gift_type";
    String SOCKET_SEND_GIFT_CARD_TYPE = "card_type";
    //送礼相关


    //LeanCloud节点问题
    String LEANCLOUD_NODE = "LEANCLOUD_NODE";
    String LEANCLOUD_NODE_CONFIG = "LEANCLOUD_NODE_CONFIG";
    String LEANCLOUD_CN = "cn";
    String LEANCLOUD_US = "us";
    String HOST = "host";
    String LEANCLOUD_NODE_RN = "LEANCLOUD_NODE_RN";
    String LEANCLOUD_NODE_RN_CONFIG = "LEANCLOUD_NODE_RN_CONFIG";
    String LEANCLOUD_SERVER_API = "LEANCLOUD_SERVER_API";
    String LEANCLOUD_SERVER_RTM = "LEANCLOUD_SERVER_RTM";
    String LEANCLOUD_SERVER_NONE = "None";
    String LEANCLOUD_SERVER_API_DEFAULT = "https://intviu-api.leancloud.cn";
    String LEANCLOUD_SERVER_RTM_DEFAULT = "wss://intviu-rtm.leancloud.cn";
    //LeanCloud节点问题


    String UNMUTE_SEND = "unmute_send";
    String OLIVE_CHANGE = "olive_changed";
    String ENGINE_ORBIT = "orbit";
    String ENGINE_GVOICE = "gvoice";

    String CALL_TYPE_GAME = "game";
    String CALL_TYPE_LANGUAGE = "language";
    String CALL_TYPE_JOY = "joy";
    String CALL_TYPE_HIGH_LEVEL = "high_level";
    String EXPORT = "export";
    String EXPORT_PAY = "export_pay";

    /**
     * 权限
     */
    String CONFIG_DATA = "config_data";
    String PREMISSION_KEY = "premission_key";
    /**
     * 广告flag
     */
    String ADV_STATUS = "adv_status";
    String ADV_FAIL_COUNT = "adv_fail_count";
    String ADV_FAIL = "adv_fail";
    String ADV_SUCCESS = "adv_success";

    String REPORT_ESCAPE = "escape";
    String REPORT_AD = "ad";
    String REPORT_SEX = "sex";
    String REPORT_CHEAT = "cheat";
    String REPORT_ABUSE = "abuse";
    String REPORT_GANG_UP = "gang_up";
    String REPORT_TROUBLE = "trouble";

    String SERVER_GET_LOCATION = "lc";
    String CAN_SAVE = "can_save";

    /**
     * 连续登录
     */
    String CONTINUE_LOGIN = "CONTINUE_LOGIN";

    /**
     * 领取奖品
     */
    String MEDIA_RECEIVE_GOLD = "media_receive_gold";
    String OPEN_BOX = "open_box";
    int TYPE_ENTER_GAME_ADV = 1;//进入游戏广告展
    int TYPE_DIE_ADV = 2;//游戏内死亡后广告展示
    int TYPE_AWARD_ADV = 3;//领取奖励后弹出的广告
    int TYPE_GAME_OVER_ADV = 4;//游戏结束时，胜负板上的广告
    int TYPE_BANNER_ADV = 5;//转盘抽奖，悬浮的广告位
    int TYPE_DISK_ADV = 6;//转盘抽奖，领奖的广告

    int ERROR_CODE_NOT_ENOUGH_MONEY = 1008;
    int ERROR_CODE_BUY_ERROR = 1001;
    int MAX_RETRY = 6;

    String LEVEL_LIMIT_NONE = "0";
    String LEVEL_LIMIT_15 = "15";
    String LEVEL_LIMIT_30 = "30";
    String LEVEL_LIMIT_50 = "50";

    String MEDIA_SEER_WOLF = "MEDIA_SEER_WOLF";
    String MEDIA_SEER_WITCH_WOLF = "MEDIA_SEER_WITCH_WOLF";
    String MEDIA_SEER_GUARD_WOLF = "MEDIA_SEER_GUARD_WOLF";
    String MEDIA_SEER_GUARD_WITCH_WOLF = "MEDIA_SEER_GUARD_WITCH_WOLF";

    String SPEECH_DIRECTION_RESULT = "speech_direction_result";
    String SYSTEM_MSG_TYPE_DIALOG = "dialog";
    String SYSTEM_MSG_TYPE_TOAST = "toast";
    String SYSTEM_MSG_TYPE_TEXT = "text";

    String UPDATE_CONFIG = "update_config";

    String RESET_MASTER_RESULT = "reset_master_result";
    String REQUEST_RESET_MASTER = "request_reset_master";
    String REJECT_RESET_MASTER = "reject_reset_master";
    String ALLOW = "allow";


    String CAN_SPEAK = "can_speak";
    String NEED_HAND_OVER = "need_hand_over";

    String ENTER_MODE = "enter_mode";//进入语音房的方式
    String ENTER_FROM_LAUNCH = "enter_from_launch";//从launch进入
    String ENTER_MODE_SMALLWINDOW = "enter_from_small_window";//从小窗进入

    //更换房间背景
    String UPDATE_BG = "update_background";
    String BG_URL = "url";
    String BG_BLUR = "blur_avatar";

    //更换麦位数量
    String UPDATE_SEAT_COUNT = "update_active_count";

    //红包相关
    String GET_PACKET_LIST = "/packet/all";
    String GET_PACKET_INFO = "/packet/info";

    String PACKET_SEND = "send_packet";
    String PACKET_COUNT = "count";
    String PACKET_AMMOUNT = "ammount";
    String PACKET_ID = "packet_id";

    String SMALL_GAME_LOCAL_JSON = "small_game_local.json";

    String VOICE_ADMIN_LIST = "";
    String ADD_ADMIN = "";
    String UN_ADMIN = "";
}
