package orangelab.project.voice.gif;

/**
 * Created by lloydfinch on 31/10/2017.
 */

public class ApplicationExtension {
    public byte[] bytes;
    public int size;

    public ApplicationExtension(byte[] bytes, int offset) {
        int blockSize;
        // get size
        size = 0x0E;

        blockSize = bytes[offset + size] & 0xFF;
        size += 1;
        while (blockSize != 0x00) {
            size += blockSize;
            blockSize = bytes[offset + size] & 0xFF;
            size += 1;
        }

        this.bytes = new byte[size];
        System.arraycopy(bytes, offset, this.bytes, 0, size);
    }

    public int getExtensionIntroducer() {
        return bytes[0] & 0xFF;
    }

    public int getExtensionLabel() {
        return bytes[1] & 0xFF;
    }

    public int getBlockSize1() {
        return bytes[2] & 0xFF;
    }

    public String getApplicationIdentifier() {
        return new String(bytes, 3, 8);
    }

    public String getApplicationAuthenticationCode() {
        return new String(bytes, 11, 3);
    }
}
