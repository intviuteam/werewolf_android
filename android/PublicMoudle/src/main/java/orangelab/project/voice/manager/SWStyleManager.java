package orangelab.project.voice.manager;

import android.text.TextUtils;
import android.view.MotionEvent;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;

import orangelab.project.MainApplication;
import orangelab.project.common.event.SmallWindowEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomAudioSocketManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

import java.util.List;

/**
 * Created by lloydfinch on 26/12/2017.
 * 小窗模式管理
 */

public class SWStyleManager extends AbstractSocketMessageHandler {

    private static final String TAG = "SWStyleManager";

    private static SWStyleManager instance;

    private String engineToken = "";

    public static SWStyleManager getInstance() {
        if (instance == null) {
            instance = new SWStyleManager();
        }
        return instance;
    }

    private SWStyleManager() {

    }

    //从小窗销毁房间
    public void destroyRoom() {
        leaveRoom();
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(this);//解除server事件接收
        GlobalUserState.getGlobalState().setGaming(false);//销毁小窗设置当前不在游戏中
        VoiceRoomConfig.setIsSmallWindowStyle(false);//设置当前不是小窗模式
        MainApplication.getInstance().getSocketEngineHelper().releaseEngine(engineToken);
        VoiceRoomAudioSocketManager.getInstance().ShutDown(this::audioShutDownCallback);
    }

    //离开房间
    public void leaveRoom() {
        UnifiedBridgeHelper.SendLeaveAudioRoomAction();
        RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(VoiceConstants.LEAVE, null, null, null);
        RoomSocketEngineHelper.userLeaveReportRN();
        ReportEventUtils.reportGameLeaveIsGaming();
    }

    public void init(String engineToken) {

        this.engineToken = engineToken;

        //注册事件接收器
        VoiceServerMessageHandler.getInstance().registerUIHandler(this);

        //销毁处理
        RxToolKit.Build(this, SmallWindowEvent.DestroyRoomFromSwEvent.class)
                .action(event -> destroyRoom())
                .register();

//        RxToolKit.Build(this, ServerMessageEvent.class)
//                .action(serverMessageEvent -> {
//                    try {
//                        if (!serverMessageEvent.isAudio()) {
//                            return;
//                        }
//                        String type = serverMessageEvent.type;
//                        if (TextUtils.equals(type, VoiceConstants.RESTORE_ROOM)) {
//                            handleRestoreRoom(TransferClassHelper.ServerMessage2VoiceMessage(serverMessageEvent));
//                        }
//                    } catch (Exception e) {
//                        ReportEventUtils.reportError(e);
//                        e.printStackTrace();
//                    }
//                })
//                .register();

        RxToolKit.Build(this, ServerResponseEvent.class)
                .action(serverResponseEvent -> {
                    try {
                        if (!serverResponseEvent.isAudio()) {
                            return;
                        }
                        int code = serverResponseEvent.code;
                        String type = serverResponseEvent.type;
                        if (TextUtils.equals(type, VoiceConstants.RECONNECT)) {
                            if (code == VoiceConstants.CODE_1032) {
                                //失去连接
                                RxToolKit.Emit(new SmallWindowEvent.NotifyDesTorySW());
                            }
                        }
                    } catch (Exception e) {
                        ReportEventUtils.reportError(e);
                        e.printStackTrace();
                    }
                })
                .register();

//        //释放掉音频收到此回调，通知外面可以执行后续操作
//        RxToolKit.Build(this, ViewEvent.DestroyEvent.class).action(v -> {
//            RxToolKit.Emit(new SmallWindowEvent.VoiceEngineReleasedEvent());
//            RxToolKit.UnRegister(this);
//            instance = null;
//        }).register();
    }

    private void audioShutDownCallback() {
        RxToolKit.Emit(new SmallWindowEvent.VoiceEngineReleasedEvent());
        RxToolKit.UnRegister(this);
        VoiceServerMessageHandler.getInstance().destroy();
        VoiceRoomGameSocketManager.getInstance().ShutDown(engineToken);
        VoiceRoomAudioSocketManager.getInstance().destroy();
        instance = null;
    }

    @Override
    public void handleJoin(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleForceSeat(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleUpSeat(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleUnSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        if (PositionHelper.isSelfPosition(position)) {
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
        }
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        if (PositionHelper.isSelf(userId)) {
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
        }
    }

    @Override
    public void handleChangeState(VoiceMessageBean messageBean) {
        String model = messageBean.payload.optString(VoiceConstants.MODEL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        if (PositionHelper.isSelfPosition(position)) {
            if (TextUtils.equals(VoiceConstants.LIMIT, model)) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
            } else if (TextUtils.equals(VoiceConstants.FREE, model)) {
                //小窗不处理自由模式
            } else if (TextUtils.equals(VoiceConstants.SPACE_STR, model) || TextUtils.isEmpty(model)) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
            }
        }
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleKickOut(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean bean) {
        notifyToUpdateHead();
    }

    //断线重连，通知小窗更新房主头像
    private void notifyToUpdateHead() {
        EnterRoomResult.EnterRoomUserItem userItem =
                VoiceRoomDataSourceManager.getInstance().findUserById(GlobalUserState.getGlobalState().getUserId());
        if (userItem == null) {
            //自己被踢了
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
            RxToolKit.Emit(new SmallWindowEvent.NotifyDesTorySW());
        } else {
            if (!PositionHelper.isUpSeat()) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
            }
            List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
            for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
                if (user != null) {
                    if (PositionHelper.isMaster(user.position)) {
                        SmallWindowEvent.NotifySWChangeHead changeHead = new SmallWindowEvent.NotifySWChangeHead();
                        changeHead.headImgUrl = user.avatar;
                        RxToolKit.Emit(changeHead);
                        return;
                    }
                }
            }
        }
    }


    @Override
    public void destroy() {

    }
}
