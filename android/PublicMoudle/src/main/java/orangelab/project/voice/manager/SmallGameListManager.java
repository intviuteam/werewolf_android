package orangelab.project.voice.manager;

import android.content.Context;
import android.text.TextUtils;

import com.toolkit.JSONFileToolKit;
import com.toolkit.action.Destroyable;

import java.util.List;

import orangelab.project.common.effect.smallgame.SmallGameManiFest;
import orangelab.project.common.tool.GsonTurnToolKit;
import orangelab.project.voice.constants.VoiceConstants;

public class SmallGameListManager implements Destroyable {

    private static final String TAG = "SmallGameListManager";
    private SmallGameManiFest mSmallGame;

    public List<SmallGameManiFest.SmallGame> getSmallGameList() {
        if (mSmallGame != null) {
            return mSmallGame.data;
        } else {
            return null;
        }
    }

    public SmallGameListManager(Context context) {
        String json = "";
        try {
            json = JSONFileToolKit.getDatafromInputStream(context.getAssets().open(VoiceConstants.SMALL_GAME_LOCAL_JSON));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSmallGame = GsonTurnToolKit.turn(json, SmallGameManiFest.class);
    }

    public SmallGameManiFest.SmallGame find(String type) {
        List<SmallGameManiFest.SmallGame> temp = getSmallGameList();
        if (temp == null) return null;

        for (SmallGameManiFest.SmallGame smallGame : temp) {
            if (TextUtils.equals(smallGame.type, type)) {
                return smallGame;
            }
        }
        return null;
    }

    @Override
    public void destroy() {
        mSmallGame = null;
    }
}
