package orangelab.project.voice.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;

import orangelab.project.common.utils.CountDownView;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.dialog.VoiceNoticeDialog;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.view.StatusCardView;

import com.toolkit.action.Destroyable;

import java.util.Collections;
import java.util.List;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public class VoiceTitleViewUI implements Destroyable {

    private ViewGroup contentView;

    private View llOnLineContainer;
    private TextView tvTitle;
    private TextView tvComeIn;
    private View ivBack;
    private ImageView ivWifi;
    private TextView tvRoomId;
    private TextView tvOnLineNumber;
    private View ivSetting;
    private View ivShare;
    private ImageView ivTypeIcon;

    private View llSpyBtnContainer;
    private Button btnSpySetting;//设置卧底
    private Button btnSpyStart;//开始"谁是卧底"游戏

    private StatusCardView statusCardView;//系统消息板

    private Button btnVoiceNotice;//房间"公告&贡献榜"

    private Context mContext;

    VoiceTitleViewUI(Context context, ViewGroup contentView) {
        this.mContext = context;
        this.contentView = contentView;
        init();
    }

    private void init() {
        llOnLineContainer = contentView.findViewById(R.id.ll_online_container);
        tvComeIn = (TextView) contentView.findViewById(R.id.tv_come_in);
        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        ivBack = contentView.findViewById(R.id.iv_back);
        ivWifi = (ImageView) contentView.findViewById(R.id.iv_wifi_icon);
        tvRoomId = (TextView) contentView.findViewById(R.id.tv_room_id);
        tvOnLineNumber = (TextView) contentView.findViewById(R.id.tv_online_number);
        ivSetting = contentView.findViewById(R.id.iv_setting);
        ivShare = contentView.findViewById(R.id.iv_share);
        ivTypeIcon = (ImageView) contentView.findViewById(R.id.iv_voice_type_icon);

        llSpyBtnContainer = contentView.findViewById(R.id.ll_who_is_spy_btn_container);
        btnSpySetting = (Button) contentView.findViewById(R.id.btn_spy_setting);
        btnSpyStart = (Button) contentView.findViewById(R.id.btn_spy_start);

        statusCardView = (StatusCardView) contentView.findViewById(R.id.sc_voice_board);

        btnVoiceNotice = (Button) contentView.findViewById(R.id.btn_voice_notice_and_popular);
        btnVoiceNotice.setOnClickListener((view) -> {
            VoiceNoticeDialog noticeDialog = new VoiceNoticeDialog(mContext);
            noticeDialog.show();
        });
    }

    public void updateTitle(String title, String type) {
        tvTitle.setText(title);
        updateTypeIcon(VoiceTopicManager.getInstance().findTopicBgUrlByType(type));
    }

    public void setRoomId(String roomId) {
        tvRoomId.setText(roomId);
    }

    public void updateOnLineNumber(int number) {
        tvOnLineNumber.setText(String.valueOf(number));
    }

    public void updateWifiState(boolean isConnected) {
        if (isConnected) {
            ivWifi.setImageResource(R.mipmap.wifi_connect);
        } else {
            ivWifi.setImageResource(R.mipmap.wifi_disconnect);
        }
    }

    public void showComeIn(String name) {
        if (TextUtils.isEmpty(name)) {
            //防止名字为空
            tvComeIn.setText(MessageUtils.getString(R.string.str_comein, "..."));
        } else {
            tvComeIn.setText(MessageUtils.getString(R.string.str_comein, name.length() <= 5 ? name : name.substring(0, 5) + "..."));
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.voice_member_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                tvComeIn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (tvComeIn != null) {
                    tvComeIn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tvComeIn.startAnimation(animation);
    }

    public View getLlOnLineContainer() {
        return llOnLineContainer;
    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public View getIvBack() {
        return ivBack;
    }

    public ImageView getIvWifi() {
        return ivWifi;
    }

    public TextView getTvRoomId() {
        return tvRoomId;
    }

    public TextView getTvOnLineNumber() {
        return tvOnLineNumber;
    }

    public View getIvSetting() {
        return ivSetting;
    }

    public View getIvShare() {
        return ivShare;
    }

    public Button getBtnSpySetting() {
        return btnSpySetting;
    }

    public Button getBtnSpyStart() {
        return btnSpyStart;
    }

    public StatusCardView getStatusCardView() {
        return statusCardView;
    }

    //展示你的词
    public void showPlayerCard(CharSequence title, CharSequence word, boolean showAlarm, int duration) {

        statusCardView.showVote(false);
        statusCardView.showSystemMsg(false);

        statusCardView.changeTitle(title);
        statusCardView.showWordDescription(true);
        statusCardView.changeYourWord(word);
        if (showAlarm && duration > 0) {
            statusCardView.showAlarm(true);
            statusCardView.initCountView(duration, () -> {
                statusCardView.showAlarm(false);
                statusCardView.cancel();
            });
        } else {
            statusCardView.showAlarm(false);
            statusCardView.cancel();
        }
        statusCardView.setVisibility(View.VISIBLE);
    }

    //投票阶段：展示卧底词和好人词
    public void showMasterCard(String title, CharSequence spyWord, CharSequence goodWord, int duration) {

        statusCardView.showWordDescription(false);
        statusCardView.showSystemMsg(false);

        statusCardView.changeTitle(title);
        statusCardView.showVote(true);
        statusCardView.changeVoteSpy(spyWord);
        statusCardView.changeVoteGoodMan(goodWord);
        statusCardView.showAlarm(false);
        statusCardView.cancel();
        statusCardView.setVisibility(View.VISIBLE);
        //statusCardView.initCountView(duration, null);
    }

    //投票结束：展示投票结果,-1表示所有人弃票
    public void showVoteResult(int position, String role, int duration, CountDownView.CountDownFinish countDownFinish) {

        statusCardView.showWordDescription(false);
        statusCardView.showVote(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_vote_result));
        if (position == -1) {
            statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_vote_msg_give_up));
        } else {
            statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_vote_msg_vote_out, position, role));
        }
        if (duration > 0) {
            statusCardView.showAlarm(true);
            statusCardView.initCountView(duration, countDownFinish);
        } else {
            statusCardView.cancel();
            statusCardView.showAlarm(false);
        }
        statusCardView.setVisibility(View.VISIBLE);

    }

    public void showBombWord(int duration, CountDownView.CountDownFinish countDownFinish) {
        statusCardView.showWordDescription(false);
        statusCardView.showVote(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_spy_bomb_word));
        statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_moment_bomb_word));
        statusCardView.cancel();
        statusCardView.showAlarm(false);
        if (duration > 0) {
            statusCardView.initCountView(duration, countDownFinish);
        }
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void showBombWordResult(int duration, boolean success, CountDownView.CountDownFinish countDownFinish) {
        statusCardView.showWordDescription(false);
        statusCardView.showVote(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_bomb_word_result));
        if (success) {
            statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_bomb_word_success));
        }
//        statusCardView.showAlarm(false);
//        if (duration > 0) {
//            statusCardView.initCountView(duration, countDownFinish);
//        }
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void showTipMessage(String title, String message, CountDownView.CountDownFinish countDownFinish) {
        statusCardView.showWordDescription(false);
        statusCardView.showVote(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(title);
        statusCardView.changeSystemMsg(message);
        statusCardView.cancel();
        statusCardView.showAlarm(false);
//        if (duration > 0) {
//            statusCardView.initCountView(duration, countDownFinish);
//        }
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void showPK(List<Integer> positions, int duration, CountDownView.CountDownFinish countDownFinish) {
        if (positions == null || positions.size() < 2) {
            return;
        }

        statusCardView.showWordDescription(false);
        statusCardView.showVote(false);
        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_vote_result));
        Collections.sort(positions);
        if (positions.size() == 2) {
            statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_vote_msg_deuce, positions.get(0), positions.get(1)));
        } else {
            StringBuilder sb = new StringBuilder();
            for (Integer position : positions) {
                sb.append(position).append("、");
            }
            String votePositions = sb.deleteCharAt(sb.length() - 1).toString();
            statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_vote_msg_deuce_more, votePositions));
        }
        if (duration > 0) {
            statusCardView.showAlarm(true);
            statusCardView.initCountView(duration, countDownFinish);
        } else {
            statusCardView.cancel();
            statusCardView.showAlarm(false);
        }
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void gameOverSpyWin() {
        statusCardView.showVote(false);
        statusCardView.showWordDescription(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_game_over));
        statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_spy_victory));
        statusCardView.cancel();
        statusCardView.showAlarm(false);
        statusCardView.initCountView(3, null);
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void gameOverGoodWin() {
        statusCardView.showVote(false);
        statusCardView.showWordDescription(false);

        statusCardView.showSystemMsg(true);
        statusCardView.changeTitle(MessageUtils.getString(R.string.str_voice_game_over));
        statusCardView.changeSystemMsg(MessageUtils.getString(R.string.str_voice_good_victory));
        statusCardView.cancel();
        statusCardView.showAlarm(false);
        statusCardView.initCountView(3, null);
        statusCardView.setVisibility(View.VISIBLE);
    }

    public void resetStatusCard() {
        statusCardView.cancel();
        statusCardView.showAlarm(false);
        statusCardView.setVisibility(View.GONE);
    }

    public void showMasterUI() {
        if (showMasterUIFilter()) {
            if (!VoiceRoomConfig.isIsPlaying()) {
                //正在游戏中，就不显示设置按钮
                showSetting();
            }
        } else {
            hideSetting();
        }
        showSpyButton();
    }

    private boolean showMasterUIFilter() {

        //是房主或者owner_id是自己，就展示设置按钮，或者是活动房管理员
        if (PositionHelper.isMaster() || PositionHelper.isAdmin() || VoiceRoomConfig.isIsOwnerRoom()) {
            return true;
        }
        return false;
    }

    public void showSetting() {
        ivSetting.setVisibility(View.VISIBLE);
    }

    public void hideSetting() {
        ivSetting.setVisibility(View.GONE);
    }

    public void showSpyButton() {
        if (PositionHelper.isMaster()) {
            if (VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
                llSpyBtnContainer.setVisibility(View.VISIBLE);
            } else {
                llSpyBtnContainer.setVisibility(View.GONE);
            }
        } else {
            llSpyBtnContainer.setVisibility(View.GONE);
        }
    }

    public void hideSpyButton() {
        llSpyBtnContainer.setVisibility(View.GONE);
    }

    public void updateTypeIcon(String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            PicassoUtils.loadImage(mContext, imageUrl, ivTypeIcon);
        }
    }

    public void cancelAlarm() {
        statusCardView.showAlarm(false);
        statusCardView.cancel();
    }

    @Override
    public void destroy() {
        if (tvComeIn != null) {
            tvComeIn.clearAnimation();
            tvComeIn = null;
        }

        if (statusCardView != null) {
            statusCardView.destroy();
        }
    }
}
