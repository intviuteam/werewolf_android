package orangelab.project.voice.privateroom.view

import android.graphics.PointF
import android.graphics.drawable.AnimationDrawable
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.PicassoUtils
import com.R
import com.androidtoolkit.ThreadToolKit
import orangelab.project.common.effect.AnimationEvent
import orangelab.project.common.utils.SafeHandler
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.common.effect.gif.GifImageView
import orangelab.project.voice.manager.GameScoreReflection
import orangelab.project.voice.manager.SmallGameManager
import orangelab.project.voice.manager.VoiceEmotionManager
import orangelab.project.voice.privateroom.model.PrivateRoomSeatData
import orangelab.project.voice.view.OctopusView
import orangelab.project.voice.view.SmallGameView
import com.androidtoolkit.HttpToolkit
import com.androidtoolkit.StreamToolKit
import orangelab.project.common.view.HeadDecorView
import orangelab.project.common.view.VipNameView
import org.greenrobot.eventbus.EventBus
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 *game_werewolf
 *2018/3/21 下午3:03
 *Mystery
 */
class PrivateRoomMemberViewImpl : PrivateRoomMemberView {

    private var mMemberPosition = -1
    private var mMemberContainer: View

    private var mMemberHead: HeadDecorView
    private var mMemberPlace: ImageView
    private var mMemberSpeak: ImageView
    private var mMemberName: VipNameView
    private var mMemberNumber: TextView
    private var mMemberFreeStyle: View
    private var mMemberBanMic: View

    private var mMemberObserver: PrivateRoomMemberViewObserver? = null

    private var mSeatData: PrivateRoomSeatData? = null
    private var mSafeHandler: SafeHandler = SafeHandler(Looper.getMainLooper())


    constructor(position: Int, viewRoot: View, seatData: PrivateRoomSeatData) {
        mMemberPosition = position
        mMemberContainer = viewRoot
        mSeatData = seatData
        mMemberHead = viewRoot.findViewById<ImageView>(R.id.member_head) as HeadDecorView
        mMemberSpeak = viewRoot.findViewById<ImageView>(R.id.member_speak) as ImageView
        mMemberName = viewRoot.findViewById<VipNameView>(R.id.member_name) as VipNameView
        mMemberFreeStyle = viewRoot.findViewById(R.id.id_freestyle)
        mMemberBanMic = viewRoot.findViewById(R.id.id_banmic)
        mMemberNumber = viewRoot.findViewById<TextView>(R.id.member_number) as TextView
        mMemberPlace = viewRoot.findViewById<ImageView>(R.id.member_place) as ImageView
        mMemberNumber.setText((position + 1).toString())

        mMemberNumber.setText((position + 1).toString())
        mMemberContainer.setOnClickListener(View.OnClickListener {
            if (mSeatData?.seatIsLock() ?: false) {
                mMemberObserver?.onMemberClickAtLock(mMemberPosition)
            } else {
                if (mSeatData?.getSeatMemberData() == null) {
                    mMemberObserver?.onMemberClickAtNoPerson(mMemberPosition)
                } else {
                    mMemberObserver?.onMemberClickAtPerson(mMemberPosition)
                }
            }
        })
    }

    private fun dismissMemberView() {
        mMemberName.visibility = View.INVISIBLE
        mMemberHead.visibility = View.INVISIBLE
        mMemberHead.clearHeadDecorate()
        mMemberPlace.visibility = View.VISIBLE
    }

    private fun lock() {
        mMemberPlace.setImageResource(R.mipmap.ico_private_room_lock)
    }

    private fun unlock() {
        mMemberPlace.setImageResource(R.mipmap.ico_private_room_no_person)
    }

    private fun showMemberView() {
        mMemberName.visibility = View.VISIBLE
        mMemberHead.visibility = View.VISIBLE
        mMemberPlace.visibility = View.GONE
    }

    override fun turnIntoNoMember() {
        endSpeakingAnim()
        dismissMemberView()
        unlock()
        mMemberBanMic.visibility = View.GONE
        mMemberFreeStyle.visibility = View.GONE
    }

    override fun turnIntoLock() {
        turnIntoNoMember()
        lock()
    }

    override fun turnIntoUnLock() {
        if (mSeatData?.getSeatMemberData() != null) {
            return
        }
        unlock()
    }

    override fun turnIntoMember() {
        showMemberView()
        PicassoUtils.loadImage(mMemberHead.context, mSeatData?.getSeatMemberData()?.getMemberIcon(), mMemberHead.realHeadImage, R.mipmap.default_head)
        mMemberName.setText(mSeatData?.getSeatMemberData()?.getMemberName())
        mMemberName.handleVipInfo(mSeatData?.getSeatMemberData()?.memberVipInfo())
        turnIntoSpeaking(mSeatData?.getSeatMemberData()?.memberIsSpeaking() ?: false)
        turnIntoFreeStyle(mSeatData?.getSeatMemberData()?.memberIsFreeStyle() ?: false)
        turnIntoBanMic(mSeatData?.getSeatMemberData()?.memberIsBanMic() ?: false)
        mMemberHead.setHeadDecorate(mSeatData?.getSeatMemberData()?.memberHeadDecor())
    }

    override fun turnIntoSpeaking(isSpeaking: Boolean) {
        if (mSeatData?.getSeatMemberData() == null) {
            return
        }
        if (isSpeaking) {
            startSpeakingAnim()
        } else {
            endSpeakingAnim()
        }
    }

    override fun turnIntoFreeStyle(isFreeStyle: Boolean) {
        if (mSeatData?.getSeatMemberData() == null) {
            return
        }
        if (isFreeStyle) {
            mMemberFreeStyle.visibility = View.VISIBLE
        } else {
            mMemberFreeStyle.visibility = View.GONE
        }
    }

    override fun turnIntoBanMic(isBanMic: Boolean) {
        if (mSeatData?.getSeatMemberData() == null) {
            return
        }
        if (isBanMic) {
            mMemberBanMic.visibility = View.VISIBLE
        } else {
            mMemberBanMic.visibility = View.GONE
        }
    }

    private fun startSpeakingAnim() {
        try {
            mMemberSpeak.visibility = View.VISIBLE
            val drawable: AnimationDrawable? = mMemberSpeak.drawable as AnimationDrawable
            drawable?.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun endSpeakingAnim() {
        try {
            mMemberSpeak.visibility = View.GONE
            val drawable: AnimationDrawable? = mMemberSpeak.drawable as AnimationDrawable
            drawable?.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 刷新人员变动
     */
    override fun refresh() {
        if (mSeatData == null) {
            return
        }
        if (mSeatData!!.seatIsLock()) {
            turnIntoLock()
        } else {
            if (mSeatData!!.getSeatMemberData() != null) {
                turnIntoMember()
            } else {
                turnIntoNoMember()
            }
        }
        EventBus.getDefault().post(AnimationEvent.RemainAnimationTaskRemoveEvent(mSeatData!!.getSeatPosition()))
    }

    override fun refreshMemberState() {
        if (mSeatData == null || mSeatData?.getSeatMemberData() == null) {
            return
        }

        val member = mSeatData!!.getSeatMemberData()

        turnIntoFreeStyle(member!!.memberIsFreeStyle())
        turnIntoBanMic(member!!.memberIsBanMic())
    }

    override fun setObserver(privateRoomMemberViewObserver: PrivateRoomMemberViewObserver) {
        mMemberObserver = privateRoomMemberViewObserver
    }

    override fun getMemberPositionInScreen(): PointF {
        var result: PointF? = null
        val location = IntArray(2)
        mMemberHead.getLocationOnScreen(location)
        result = PointF(location[0].toFloat(), location[1].toFloat())
        return result
    }

    override fun getMemberWidth(): Int {
        return mMemberHead.width
    }

    override fun getMemberHeight(): Int {
        return mMemberHead.height
    }

    override fun refreshForce() {
        if (mSeatData == null) {
            return
        }
        if (mSeatData!!.seatNeedRefresh()) {
            refresh()
            mSeatData!!.seatRefreshDone()
        }
    }

    override fun destroy() {
        mSeatData = null
        mSafeHandler.release()
        endSpeakingAnim()
    }
}