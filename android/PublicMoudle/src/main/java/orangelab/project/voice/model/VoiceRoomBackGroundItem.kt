package orangelab.project.voice.model

import com.toolkit.action.Keepable

/**
 * game_werewolf
 * 2018/3/26 上午10:37
 * Mystery
 */

class VoiceRoomBackGroundItem : Keepable {
    var type: String = ""
    var id: String = ""

    var url: String = ""
    var localUrl: String = ""

    var resourceId: Int = -1
    var bgNeedBlur = false

    var selected = false
    var isAddPlaceHolder = false
    var isLoading = false

    override fun toString(): String {
        return "VoiceRoomBackGroundItem(id='$id', url=$url, bgNeedBlur=$bgNeedBlur)"
    }


}
