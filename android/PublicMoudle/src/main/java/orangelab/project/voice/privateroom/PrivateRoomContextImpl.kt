package orangelab.project.voice.privateroom

import android.os.Build
import android.text.TextUtils
import android.util.Log
import cn.intviu.sdk.model.User
import com.androidtoolkit.Environment
import com.androidtoolkit.RxToolKit
import com.datasource.GlobalUserState
import com.BuildConfig
import com.R
import com.androidtoolkit.ToastToolKit
import orangelab.project.MainApplication
import orangelab.project.common.context.BaseServerContext
import orangelab.project.common.engine.*
import orangelab.project.common.event.ServerEvent
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.common.model.action.*
import orangelab.project.voice.api.VoiceSocketHelper
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.privateroom.helper.PrivateRoomDataSource
import com.networktoolkit.transport.Constant
import com.toolkit.action.Action
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest
import orangelab.project.common.event.SmallWindowEvent
import orangelab.project.common.event.ViewEvent
import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.voice.manager.GameScoreReflection
import orangelab.project.voice.manager.VoiceRoomCommander
import orangelab.project.voice.privateroom.model.*
import java.util.*
import kotlin.collections.HashSet

/**
 *game_werewolf
 *2018/3/19 上午10:56
 *Mystery
 */

/**
 * Context 的 生命周期要比 Activity生命周期要长，切记
 */
class PrivateRoomContextImpl : BaseServerContext, PrivateRoomContext {

    private var mMinimize = false

    private var mRoomAudioEngine: IOrbitEngine? = null
    private var mEnterRoomResult: EnterRoomResult

    private var mRoomDataHelper: PrivateRoomDataSource
    private var mRoomRequestFriendFilter: MutableSet<String>

    private var mAudioConnect = false
    private var mAudioObserver: IOrbitEngineObserver? = null
    private var mContextObserver: PrivateRoomContextObserver? = null
    private var mSocketEngineToken: String?
    private var mSockeConnect = true

    constructor(socketEngine: String?, enterRoomResult: EnterRoomResult) : super(SocketEngine.PRIVACY) {
        printlnLog("Create")
        mEnterRoomResult = enterRoomResult
        mRoomDataHelper = PrivateRoomDataSource()
        mRoomRequestFriendFilter = HashSet()
        initBaseData()
        initListener()
        mSocketEngineToken = socketEngine
    }

    override fun refresh(enterRoomResult: EnterRoomResult) {
        printlnLog("Refresh")
        /**
         * 执行刷新操作
         */
        mRoomDataHelper.refreshData(enterRoomResult)
    }

    private fun initBaseData() {
        /**
         * 初始化block Time
         */
        mRoomDataHelper.mBlockUpSeatDuration = mEnterRoomResult.block_upseat_duration

        /**
         * 小窗是否可用
         */
        mRoomDataHelper.mMiniModeEnable = mEnterRoomResult.room.config.mini_mode

        /**
         * 初始化房间号
         */
        mRoomDataHelper.mRoomId = mEnterRoomResult.room.room_id
        mRoomDataHelper.mRoomType = mEnterRoomResult.room.config.type

        GlobalUserState.getGlobalState().curGameType = mRoomDataHelper.mRoomType
        GlobalUserState.getGlobalState().curRoomId = mRoomDataHelper.mRoomId
        GlobalUserState.getGlobalState().isGaming = true


        /**
         * 初始化房间密码
         */
        mRoomDataHelper.updateRoomPassword(mEnterRoomResult.room.password)

        /**
         * 初始化座位基本信息
         */
        mRoomDataHelper.initSeats(PrivateRoomTools.MIN_POSITION, PrivateRoomTools.MAX_POSITION_REAL)

        /**
         * 初始化座位状态信息
         */
        mRoomDataHelper.initLockedSeats(mEnterRoomResult.room.locked_positions)

        /**
         * 初始化房间人员信息
         */
        val users = mEnterRoomResult.room.users
        users.forEach {
            val temp = PrivateRoomTools.TurnIntoPrivateRoomMemberFromEnterResult(it.value)
            addMember(it.key, temp)
        }

        /**
         * 初始化自己的位置
         */
        mRoomDataHelper.mSelfPosition = mEnterRoomResult.position

        /**
         * 初始化like数量
         */
        mRoomDataHelper.mRoomLikeCount = mEnterRoomResult.room.likeCount.toLong()

        /**
         * 初始化房间背景
         */
        try {
            mRoomDataHelper.mRoomBackGroundUrl = mEnterRoomResult.room.config.room_info.background.url
            mRoomDataHelper.mRoomBackGroundType = mEnterRoomResult.room.config.room_info.background.type
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /**
         * 初始化房间基本信息数据
         */
        try {
            mRoomDataHelper.mRoomIntroduce = mEnterRoomResult.room.config.theme.description
            mRoomDataHelper.mRoomTitle = mEnterRoomResult.room.config.theme.title
            mRoomDataHelper.mRoomTag = mEnterRoomResult.room.config.theme.tag
            mRoomDataHelper.mRoomTagColor = mEnterRoomResult.room.config.theme.tag_color
            mRoomDataHelper.updateRoomPassword(mEnterRoomResult.room.password)
            mRoomDataHelper.mMiniModeEnable = mEnterRoomResult.room.config.mini_mode
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /**
         * 初始化工具箱
         */
        try {
            mRoomDataHelper.updateSendTempImageEnable(mEnterRoomResult.room.config.enable_share_image)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun initListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent::class.java).action(Action {
            exec(Runnable {
                when (it.action) {
                    ServerEvent.DISCONNECT_FROM_AUDIO_SERVER -> {
                        printlnLog("AudioDisConnect")
                        mAudioConnect = false
                        mAudioObserver?.onEngineDisConnect()
                    }
                    ServerEvent.RECONNECT_FROM_AUDIO_SERVER -> {
                        printlnLog("AudioConnect")
                        mAudioConnect = true
                        mAudioObserver?.onEngineConnect()
                    }
                }
            })
        }).register()

        RxToolKit.Build(this, ViewEvent.RequestAddFriendEvent::class.java).action { v ->
            exec(Runnable {
                val member = getMember(v.toUserId)
                member ?: return@Runnable
                RoomSocketEngineHelper.sendRequestFriendInGame(member.getMemberId(), member.getMemberPosition())
            })
        }.register()
    }

    override fun restoreContext() {
        /**
         * 尝试初始化纸飞机
         */
        mContextObserver?.onShowPaperPlane(mRoomDataHelper.getPaperPlaneCache())
        mRoomDataHelper.cleartPaperPlaneCache()
    }

    override fun restoreData(key: String, data: Any) {
        addReference(key, data)
    }

    override fun getRestoreData(key: String): Any? {
        return getReference(key)
    }

    override fun bindObserver(observer: PrivateRoomContextObserver) {
        mContextObserver = observer
    }

    override fun unBindObserver(observer: PrivateRoomContextObserver) {
        mContextObserver = null
    }

    override fun registerAudioObserver(orbitEngineObserver: IOrbitEngineObserver) {
        mAudioObserver = orbitEngineObserver
    }

    override fun unregisterAudioObserver(orbitEngineObserver: IOrbitEngineObserver) {
        mAudioObserver = null
    }

    private fun removeMember(position: Int) {
        mRoomDataHelper.remove(position)
    }

    override fun getRoomConfig(): EnterRoomResult.RoomConfig? {
        return mEnterRoomResult.room.config
    }

    override fun getAllMembers(): MutableList<PrivateRoomMemberData> {
        return mRoomDataHelper.getMembersData()
    }

    override fun getMember(position: Int): PrivateRoomMemberData? {
        return mRoomDataHelper.get(position)
    }

    override fun getMasterMember(): PrivateRoomMemberData? {
        return mRoomDataHelper.getSeat(0)?.memberData
    }

    override fun getAllMembersWithOutMySelf(): MutableList<PrivateRoomMemberData> {
        val result = ArrayList<PrivateRoomMemberData>()
        mRoomDataHelper.getMembersData().forEach {
            if (it.memberIsSelf()) {

            } else {
                result.add(it)
            }
        }
        return result
    }

    override fun getAllMembersOutSeats(): MutableList<PrivateRoomMemberData> {
        val result = ArrayList<PrivateRoomMemberData>()
        mRoomDataHelper.getMembersData().forEach {
            if (it.memberIsUpSeat()) {

            } else {
                result.add(it)
            }
        }
        return result
    }

    override fun getAllMembersInSeats(): MutableList<PrivateRoomMemberData> {
        val result = ArrayList<PrivateRoomMemberData>()
        mRoomDataHelper.getSeatsData().forEach {
            if (it.memberData != null) {
                result.add(it.memberData!!)
            }
        }
        return result
    }

    override fun getAllMembersForShow(): MutableList<PrivateRoomMemberData> {
        val result = ArrayList<PrivateRoomMemberData>()
        result.addAll(getAllMembersInSeats())
        result.addAll(getAllMembersOutSeats())
        return result
    }

    override fun getChatMessageList(): List<Any> {
        return mRoomDataHelper.getChatMessageList()
    }

    override fun getMember(userId: String): PrivateRoomMemberData? {
        return mRoomDataHelper.get(userId)
    }

    override fun getSeatsData(): MutableList<PrivateRoomSeatData> {
        var list: MutableList<PrivateRoomSeatData> = ArrayList()
        mRoomDataHelper.getSeatsData().forEach(
                {
                    list.add(it)
                }
        )
        return list
    }

    override fun getRequestFriendUserFilter(): Set<String> {
        return mRoomRequestFriendFilter
    }

    private fun addMember(position: Int, privateRoomMember: PrivateRoomMemberDataImpl) {
        mRoomDataHelper.locateMember(position, privateRoomMember)
    }

    override fun addTempPictureItem(tempPictureMessageItem: TempPictureMessageItem) {
        mRoomDataHelper.addTempPicture(tempPictureMessageItem)
        mContextObserver?.onMessageListChange()
    }

    override fun handleServerActionAddFriend(serverActionAddFriend: ServerActionAddFriend) {
        /**
         * 处理好友请求，先拿到好友数据
         */
        val member = getMember(serverActionAddFriend.user_id)
        member ?: return
        RxToolKit.Emit(ViewEvent.ReceiveAddFriendEvent(member))
    }

    override fun handleServerActionJoin(serverActionJoin: ServerActionJoin) {
        /**
         * 更新人员数据
         */
        addMember(serverActionJoin.position, PrivateRoomTools.TurnIntoPrivateRoomMemberFromEnterResult(serverActionJoin.user))

        /**
         * 更新聊天信息数据，并通知更新
         */
        // mRoomDataHelper.addSysMsg(SystemMessageItem(MessageUtils.getString(R.string.str_comein, serverActionJoin.user.name)))
        //  mContextObserver?.onMessageListChange()

        /**
         * 通知在线人数变化
         */
        mContextObserver?.onOnlineNumber(getOnlineNumber())

        /**
         * 通知座位UI刷新
         */
        if (PrivateRoomTools.PositionInUpSeat(serverActionJoin.position)) {
            mContextObserver?.onSeatUIRefresh(serverActionJoin.position)
        }
    }

    override fun handleServerActionSystemMsg(serverActionSystemMsg: ServerActionSystemMsg) {
        if (TextUtils.equals(serverActionSystemMsg.type, ServerActionSystemMsg.TEXT)) {
            /**
             * 创建一个文字
             */
            mRoomDataHelper.addSysMsg(SystemMessageItem(serverActionSystemMsg.content))
            mContextObserver?.onMessageListChange()
        }
        mContextObserver?.onSystemMsg(serverActionSystemMsg)
    }

    override fun handleServerActionKickOut(serverActionKickOut: ServerActionKickOut) {
        val member = getMember(serverActionKickOut.position)
        if (member == null) return
        /**
         * 先更新数据
         */
        removeMember(serverActionKickOut.position)

        /**
         * 通知在线人数变化
         */
        mContextObserver?.onOnlineNumber(getOnlineNumber())

        if (mMinimize) {
            if (member.memberIsSelf()) {
                /**
                 * 被踢的人是自己
                 */
                RxToolKit.Emit(SmallWindowEvent.NotifyDesTorySW())
                /**
                 * 销毁自己Context
                 */
                ToastToolKit.showShort(R.string.you_have_been_kick_out)
                VoiceRoomCommander.ContextManager.DestroyPrivateRoomContext(null)
                return
            }
        }

        /**
         * 更新UI
         */
        if (isUpSeat(member.getMemberPosition())) {
            mContextObserver?.onSeatUIRefresh(member.getMemberPosition())
        }

//        /**
//         * 更新消息
//         */
//        if (member.memberIsMaster()) {
//            /**
//             * 这个人是房主提示离开
//             */
//            mRoomDataHelper.addSysMsg(SystemMessageItem(MessageUtils.getString(R.string.str_leave, member.getMemberName())))
//        } else {
//            /**
//             * 显示被踢出房间
//             */
//            mRoomDataHelper.addSysMsg(SystemMessageItem(MessageUtils.getString(R.string.str_leave, member.getMemberName())))
//        }

        mContextObserver?.onKickOut(member)
    }

    override fun handleServerActionLeave(serverActionLeave: ServerActionLeave) {

        val member = getMember(serverActionLeave.position)
        if (member == null) return

        val position = serverActionLeave.position
        /**
         * 更新数据
         */
        removeMember(serverActionLeave.position)

        /**
         * 更新聊天信息
         */
//        mRoomDataHelper.addSysMsg(SystemMessageItem(MessageUtils.getString(R.string.str_leave, member.getMemberName())))
//        mContextObserver?.onMessageListChange()

        /**
         * 通知在线人数变化
         */
        mContextObserver?.onOnlineNumber(getOnlineNumber())

        /**
         * 通知UI刷新
         */
        if (PrivateRoomTools.PositionInUpSeat(position)) {
            mContextObserver?.onSeatUIRefresh(position)
        }

    }

    override fun handleServerActionChat(serverActionChat: ServerActionChat) {

        val member = getMember(serverActionChat.position)

        if (member == null) return

        if (serverActionChat.gift != null) {
            /**
             * 礼物消息
             */
            val item = GiftMessageItem(serverActionChat.message, serverActionChat.position, member.getMemberName()!!, serverActionChat.gift.type)
            item.vipInfo = member.memberVipInfo()
            mRoomDataHelper.addGiftMsg(item)
            mContextObserver?.onMessageListChange()
            /**
             * 展示礼物特效
             */
            mContextObserver?.onShowGiftAnim(serverActionChat.gift.from, serverActionChat.gift.to, serverActionChat.gift.type)

            return
        }
        if (serverActionChat.isChatImage && serverActionChat.image != null) {
            /**
             * 添加一个临时图片消息，这里做一个过滤，由于新版本需要先展示上传进度，此处过滤掉自己的消息
             */
            if (member.memberIsSelf()) {
                return
            }
            val tempImage = TempPictureMessageItem(
                    member!!.getMemberName()!!, member!!.getMemberPosition(), serverActionChat.image.expire_time, serverActionChat.image.url, RoomSocketEngineHelper.serverTime())
            tempImage.vipInfo = member.memberVipInfo()
            mRoomDataHelper.addTempPicture(tempImage)
            mContextObserver?.onMessageListChange()
            mContextObserver?.onTempPictureMessage(tempImage)
            return
        }
        if (serverActionChat.isPaperPlane) {
            /**
             * 这个是纸飞机消息
             */
            if (serverActionChat.from != null) {
                val member = getMember(serverActionChat.from.position)
                if (member == null) return
                val paperPlane = PrivateRoomPaperPlane()
                paperPlane.fromPosition = member.getMemberPosition()
                paperPlane.fromUserIcon = member.getMemberIcon() ?: ""
                paperPlane.fromUserId = member.getMemberId() ?: ""
                paperPlane.text = serverActionChat.message
                paperPlane.mSelfPosition = getSelfPosition()
                if (mMinimize) {
                    /**
                     * 小窗模式进行消息存储
                     */
                    mRoomDataHelper.addPaperPlane(paperPlane)
                } else {
                    mContextObserver?.onShowPaperPlane(paperPlane)
                }
            }
            return
        }
        /**
         * 其他消息,根据用户取出msgDecor
         */
        val chatItem = ChatMessageItem(serverActionChat.message, serverActionChat.position, member.getMemberName()!!)
        chatItem.vipInfo = member.memberVipInfo()
        chatItem.msgDecor = messageBoxTurnIntoMsgDecor(member.memberMsgDecor())
        mRoomDataHelper.addChatMsg(chatItem)
        mContextObserver?.onMessageListChange()
    }

    private fun messageBoxTurnIntoMsgDecor(messageBox: UserInfoResult.MessageBox?): MsgDecorManiFest.MsgDecorManiFestItemResource? {
        return EffectsManager.GetMsgDecorItem(messageBox?.message_box_type)?.animate
    }

    override fun handleServerActionLock(serverActionLock: ServerActionLock) {
        /**
         * 更新锁位置数据
         */
        mRoomDataHelper?.updateLock(serverActionLock.locked_positions)

        /**
         * 更新锁位置的UI
         */
        val locked_positions = serverActionLock.locked_positions
        if (serverActionLock.locked_positions != null && serverActionLock.locked_positions.size > 0) {
            getSeatsData().forEach {
                if (locked_positions.contains(it.getSeatPosition())) {
                    mContextObserver?.onSeatLocked(it.getSeatPosition(), true)
                } else {
                    mContextObserver?.onSeatLocked(it.getSeatPosition(), false)
                }
            }
        } else {
            getSeatsData().forEach {
                mContextObserver?.onSeatLocked(it.getSeatPosition(), false)
            }
        }
    }

    private fun handleMemberPositionChange(member: PrivateRoomMemberData, from: Int, to: Int) {

        /**
         * 刷新Title
         */
        mContextObserver?.onUpdateRoomTitleBar()

        /**
         * 刷新Tag
         */
        mContextObserver?.onUpdateRoomTag()


        /**
         * 刷新位置UI
         */
        if (PrivateRoomTools.PositionInUpSeat(from)) {
            mContextObserver?.onSeatUIRefresh(from)
        }
        if (PrivateRoomTools.PositionInUpSeat(to)) {
            mContextObserver?.onSeatUIRefresh(to)
        }

        if (mMinimize) {
            /**
             * 如果to是房主，那么则发送小窗更新换头像指令
             */
            val member = getMember(to)
            member ?: return
            if (member.memberIsMaster()) {
                RxToolKit.Emit(SmallWindowEvent.NotifySWChangeHead(member.getMemberIcon()))
            }
        }

        /**
         * 更新底部操作栏
         */
        if (member.memberIsSelf()) {
            if (PrivateRoomTools.PositionInUpSeat(from) && PrivateRoomTools.PositionInUpSeat(to)) {
                //do nothing
                mContextObserver?.onToolBoxUpdate()
                mContextObserver?.onToolBoxItemUpdate()
                return
            }
            if (PrivateRoomTools.PositionInUpSeat(from) && PrivateRoomTools.PositionInOutSeat(to)) {
                /**
                 * 自己下麦了
                 */
                mContextObserver?.onSelfDownSeat()
                mContextObserver?.onToolBoxUpdate()
                mContextObserver?.onToolBoxItemUpdate()
                return
            }
            if (PrivateRoomTools.PositionInOutSeat(from) && PrivateRoomTools.PositionInUpSeat(to)) {
                /**
                 * 自己上麦了
                 */
                mContextObserver?.onSelfUpSeat()
                mContextObserver?.onToolBoxUpdate()
                mContextObserver?.onToolBoxItemUpdate()
                return
            }
            if (PrivateRoomTools.PositionInOutSeat(from) && PrivateRoomTools.PositionInOutSeat(to)) {
                //do nothing
                return
            }
        }
    }

    override fun handleServerActionUpSeat(serverActionUpSeat: ServerActionUpSeat) {
        /**
         * 处理上麦,上麦是属于房间内的人员位置变动
         */
        val member: PrivateRoomMemberData? = getMember(serverActionUpSeat.user_id)

        if (member != null) {

            val from = member.getMemberPosition()
            val to = serverActionUpSeat.dest_position
            /**
             * 先更新数据
             */
            mRoomDataHelper?.changePosition(member!!.getMemberPosition(), serverActionUpSeat.dest_position)

            /**
             * 更新消息
             */
            if (to == PrivateRoomTools.MASTER_POSITION) {
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_new_master, getMember(to)?.getMemberName())
                        )
                )
            } else {
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_up_seat, getMember(to)?.getMemberName(), (to + 1).toString())
                        )
                )
            }
            mContextObserver?.onMessageListChange()

            /**
             * 位置信息变动
             */
            handleMemberPositionChange(member, from, to)
        }
    }

    override fun handleServerActionDownSeat(serverActionDownSeat: ServerActionDownSeat) {
        val member: PrivateRoomMemberData? = getMember(serverActionDownSeat.user_id)
        if (member != null) {
            val from = member.getMemberPosition()
            val to = serverActionDownSeat.dest_position
            /**
             * 先更新数据
             */
            mRoomDataHelper?.changePosition(member!!.getMemberPosition(), serverActionDownSeat.dest_position)

            /**
             * 更新消息
             */
            mRoomDataHelper.addSysMsg(
                    SystemMessageItem(
                            MessageUtils.getString(R.string.str_down_seat, getMember(to)?.getMemberName(), (from + 1).toString())
                    )
            )
            mContextObserver?.onMessageListChange()
            /**
             * 位置信息变动
             */
            handleMemberPositionChange(member, from, to)
        }
    }

    override fun handleServerActionForceSeat(serverActionForceSeat: ServerActionForceSeat) {
        val member: PrivateRoomMemberData? = getMember(serverActionForceSeat.user_id)
        if (member != null) {
            val from = member.getMemberPosition()
            val to = serverActionForceSeat.dest_position
            /**
             * 先更新数据
             */
            mRoomDataHelper?.changePosition(member!!.getMemberPosition(), serverActionForceSeat.dest_position)

            /**
             * 更新消息
             */
            if (to == PrivateRoomTools.MASTER_POSITION) {
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_new_master, getMember(to)?.getMemberName())
                        )
                )
            } else {
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_up_seat, getMember(to)?.getMemberName(), (to + 1).toString())
                        )
                )
            }
            mContextObserver?.onMessageListChange()

            /**
             * 位置信息变动
             */
            handleMemberPositionChange(member, from, to)
        }
    }

    override fun handleServerActionSpeak(serverActionSpeak: ServerActionSpeak) {
        mRoomDataHelper?.updateMemberSpeaking(serverActionSpeak.position, true)

        mContextObserver?.onSpeaking(serverActionSpeak.position)
    }

    override fun handleServerActionUnSpeak(serverActionUnSpeak: ServerActionUnSpeak) {
        mRoomDataHelper?.updateMemberSpeaking(serverActionUnSpeak.position, false)

        mContextObserver?.onUnSpeaking(serverActionUnSpeak.position)
    }

    override fun handleServerActionShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon) {
        mContextObserver?.onShowEmoticon(serverActionShowEmoticon)
    }

    override fun handleServerActionAcceptFreeMode(serverActionAcceptFreeMode: ServerActionAcceptFreeMode) {
        val member = mRoomDataHelper.get(serverActionAcceptFreeMode.position)
        if (member != null) {
            member.isFreeStyle = true

            mRoomDataHelper.addSysMsg(
                    SystemMessageItem(MessageUtils.getString(R.string.str_voice_accept_freemode, getMember(serverActionAcceptFreeMode.position)?.getMemberName()))
            )
            mContextObserver?.onMessageListChange()

            mContextObserver?.onSeatStateRefresh(serverActionAcceptFreeMode.position)

            if (isSelfPosition(serverActionAcceptFreeMode.position)) {
                mContextObserver?.onSelfTurnIntoFreeStyle(true)
            }
        }
    }

    override fun handleServerActionRequestFreeMode(serverActionRequestFreeMode: ServerActionRequestFreeMode) {
        if (serverActionRequestFreeMode.position == mRoomDataHelper.mSelfPosition) {
            if (mMinimize) {
                /**
                 * 当前是小窗模式 直接发送拒绝 并直接返回
                 */
                VoiceSocketHelper.rejectFreeMode()
                return
            }
        }
        mContextObserver?.onFreeStyleRequest(serverActionRequestFreeMode)
    }

    override fun handleServerActionRejectFreeMode(serverActionRejectFreeMode: ServerActionRejectFreeMode) {
        mRoomDataHelper.addSysMsg(
                SystemMessageItem(MessageUtils.getString(R.string.str_voice_reject_freemode, getMember(serverActionRejectFreeMode.position)?.getMemberName()))
        )
        mContextObserver?.onMessageListChange()
    }

    override fun handleServerActionChangeUserState(serverActionChangeUserState: ServerActionChangeUserState) {
        /**
         * 处理房间人员状态
         */
        val member = mRoomDataHelper.get(serverActionChangeUserState.position)

        val state = serverActionChangeUserState.state

        if (member == null) {
            return
        }

        if (member.isFreeStyle) {
            /**
             * 当前是freeStyle
             */
            if (TextUtils.equals(state, VoiceConstants.LIMIT)) {
                /**
                 * freeStyle 变成了 禁麦
                 */
                member.isFreeStyle = false
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_un_free_style,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoFreeStyle(false)
                }

                member.isBanMic = true
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_ban_mic1,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoBanMic(true)
                }
            }

            if (TextUtils.equals(VoiceConstants.SPACE_STR, state) || TextUtils.isEmpty(state)) {
                member.isFreeStyle = false
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_un_free_style,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoFreeStyle(false)
                }
                member.isBanMic = false
            }
            mContextObserver?.onSeatStateRefresh(serverActionChangeUserState.position)
            return
        }

        if (member.isBanMic) {
            /**
             * 当前是禁麦
             */
            if (TextUtils.equals(state, VoiceConstants.FREE)) {
                member.isBanMic = false
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_unban_mic1,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoBanMic(false)
                }

                member.isFreeStyle = true
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_free_style,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoFreeStyle(true)
                }

            }

            if (TextUtils.equals(VoiceConstants.SPACE_STR, state) || TextUtils.isEmpty(state)) {
                member.isBanMic = false
                mRoomDataHelper.addSysMsg(
                        SystemMessageItem(
                                MessageUtils.getString(R.string.str_voice_unban_mic1,
                                        serverActionChangeUserState.position + 1,
                                        getMember(serverActionChangeUserState.position)?.getMemberName()))
                )
                mContextObserver?.onMessageListChange()
                if (isSelfPosition(serverActionChangeUserState.position)) {
                    mContextObserver?.onSelfTurnIntoBanMic(false)
                }
                member.isFreeStyle = false
            }
            mContextObserver?.onSeatStateRefresh(serverActionChangeUserState.position)
            return
        }

        if (TextUtils.equals(state, VoiceConstants.FREE)) {
            member.isFreeStyle = true
            mRoomDataHelper.addSysMsg(
                    SystemMessageItem(
                            MessageUtils.getString(R.string.str_voice_free_style,
                                    serverActionChangeUserState.position + 1,
                                    getMember(serverActionChangeUserState.position)?.getMemberName()))
            )
            mContextObserver?.onMessageListChange()

            if (isSelfPosition(serverActionChangeUserState.position)) {
                mContextObserver?.onSelfTurnIntoFreeStyle(true)
            }
            mContextObserver?.onSeatStateRefresh(serverActionChangeUserState.position)
            return
        }

        if (TextUtils.equals(state, VoiceConstants.LIMIT)) {
            member.isBanMic = true
            mRoomDataHelper.addSysMsg(
                    SystemMessageItem(
                            MessageUtils.getString(R.string.str_voice_ban_mic1,
                                    serverActionChangeUserState.position + 1,
                                    getMember(serverActionChangeUserState.position)?.getMemberName()))
            )
            mContextObserver?.onMessageListChange()

            if (isSelfPosition(serverActionChangeUserState.position)) {
                mContextObserver?.onSelfTurnIntoBanMic(true)
            }
            mContextObserver?.onSeatStateRefresh(serverActionChangeUserState.position)
            return
        }
    }

    override fun handleServerActionLikeRoom(serverActionLikeRoom: ServerActionLikeRoom) {
        mRoomDataHelper.mRoomLikeCount = serverActionLikeRoom.count

        mContextObserver?.onLikeNumber(getLikeCount())

        mRoomDataHelper.addSysMsg(
                SystemMessageItem(MessageUtils.getString(R.string.str_private_room_like, getMember(serverActionLikeRoom.userId)?.getMemberName()))
        )
        mContextObserver?.onMessageListChange()
    }

    override fun handleServerActionRestoreRoom(serverActionRestoreRoom: ServerActionRestoreRoom) {
        /**
         * 恢复所有数据
         */
        mRoomDataHelper.refreshData(serverActionRestoreRoom)

        /**
         * 刷新所有UI
         */
        mContextObserver?.onSeatUIAllRefresh()

        /**
         * 刷新Title
         */
        mContextObserver?.onUpdateRoomTitle()
        mContextObserver?.onUpdateRoomTitleBar()
        mContextObserver?.onUpdateRoomTag()

        mContextObserver?.onLikeNumber(getLikeCount())
        mContextObserver?.onOnlineNumber(getOnlineNumber())

        mContextObserver?.onUpdateBackGround()
        mRoomDataHelper.mRoomBackGroundChanged = false

        mContextObserver?.onToolBoxUpdate()
        mContextObserver?.onToolBoxItemUpdate()

    }

    override fun handleServerActionChangePassword(serverActionChangePassword: ServerActionChangePassword) {

    }

    override fun handleServerActionUpdateConfig(serverActionUpdateConfig: ServerActionUpdateConfig) {

        /**
         * 处理背景的变化
         */
        try {
            val backgroundUrl = serverActionUpdateConfig.config.room_info.background.url
            val backgroundtype = serverActionUpdateConfig.config.room_info.background.type
            if (TextUtils.isEmpty(backgroundUrl) || TextUtils.isEmpty(backgroundtype)) {
                mRoomDataHelper.mRoomBackGroundChanged = false
            } else {
                if (TextUtils.equals(getBackGroundType(), backgroundtype)
                        && TextUtils.equals(getBackGroundUrl(), backgroundUrl)) {
                    /**
                     * 房间背景没有变化
                     */
                    mRoomDataHelper.mRoomBackGroundChanged = false
                } else {
                    /**
                     * 房间背景有变化
                     */
                    mRoomDataHelper.mRoomBackGroundType = backgroundtype
                    mRoomDataHelper.mRoomBackGroundUrl = backgroundUrl
                    mRoomDataHelper.mRoomBackGroundChanged = true
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            /**
             * 处理背景时发生了错误，为防止崩溃，这里就直接认为房间背景没有变化
             */
            mRoomDataHelper.mRoomBackGroundChanged = false
        }

        mContextObserver?.onUpdateBackGround()
        /**
         * 消息遍历循环结束后默认为监听者已经处理了消息
         */
        mRoomDataHelper.mRoomBackGroundChanged = false

        /**
         * 房间配置有变化
         */
        try {
            mRoomDataHelper.mMiniModeEnable = serverActionUpdateConfig.config.mini_mode

            if (serverActionUpdateConfig.change != null) {
                val change = serverActionUpdateConfig.change

                /**
                 * 房间密码变化
                 */
                if (change.password != null) {
                    mRoomDataHelper.updateRoomPassword(change.password)
                }
                /**
                 * 房间主题变化
                 */
                if (change.theme != null) {
                    /**
                     * 主题发生了变化
                     */
                    val theme = change.theme

                    val orgTitle = mRoomDataHelper.mRoomTitle
                    val newTitle = theme.title
                    if (!TextUtils.equals(orgTitle, newTitle)) {
                        mContextObserver?.onUpdateRoomTitle()
                    }

                    mRoomDataHelper.mRoomTitle = newTitle

                    /**
                     * 房间标签发生了变化
                     */
                    val orgTag = mRoomDataHelper.mRoomTag
                    val newTag = theme.tag
                    val orgTagColor = mRoomDataHelper.mRoomTagColor
                    val newTagColor = theme.tag_color
                    val orgIntroduce = mRoomDataHelper.mRoomIntroduce
                    val newIntroduce = theme.description

                    mRoomDataHelper.mRoomTag = newTag
                    mRoomDataHelper.mRoomTagColor = newTagColor
                    mRoomDataHelper.mRoomIntroduce = newIntroduce

                    if (!TextUtils.equals(orgTag, newTag) || !TextUtils.equals(orgTagColor, newTagColor) || !TextUtils.equals(orgIntroduce, newIntroduce)) {
                        mContextObserver?.onUpdateRoomTag()
                    }
                }

                /**
                 * 房间工具箱配置变化
                 */
                val newTempImageState = change.enable_share_image
                newTempImageState ?: return

                val oldTempImageState = mRoomDataHelper.getSendTempImageEnable()

                if (newTempImageState == oldTempImageState) {
                    /**
                     * 状态没有变化
                     */
                } else {
                    /**
                     * 状态发生了变化
                     */
                    mRoomDataHelper.updateSendTempImageEnable(newTempImageState)
                    mContextObserver?.onToolBoxUpdate()
                    mContextObserver?.onToolBoxItemUpdate()
                }
            } else {

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun handleServerActionShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion) {
        if (TextUtils.equals(VoiceConstants.POINTS, serverActionShowGameEmotion.mark)) {
            val position = serverActionShowGameEmotion.position
            val member = getMember(position)
            member ?: return
            mRoomDataHelper.addGiftMsg(
                    GiftMessageItem(
                            "", member.getMemberPosition(), member.getMemberName() ?: "", GameScoreReflection.scoreToImageId(Utils.stringToInt(serverActionShowGameEmotion.data.res)))
            )
            mContextObserver?.onMessageListChange()
        }
        mContextObserver?.onShowGameEmoticon(serverActionShowGameEmotion)
    }

    override fun handleServerSocketConnect() {
        mSockeConnect = true
        mRoomDataHelper.addSysMsg(
                SystemMessageItem(MessageUtils.getString(R.string.socket_reconnect))
        )
        mContextObserver?.onMessageListChange()
        mContextObserver?.onSocketConnect()
    }

    override fun handleServerSocketDisconnect() {
        mSockeConnect = false
        mRoomDataHelper.addSysMsg(
                SystemMessageItem(MessageUtils.getString(R.string.socket_disconnect))
        )
        mContextObserver?.onMessageListChange()
        mContextObserver?.onSocketDisconnect()
    }

    override fun handleServerSocketReconnectFailed(reason: String) {
        mSockeConnect = false
        mContextObserver?.onSocketClose()
    }

    override fun openAudioEngine() {

        mRoomAudioEngine = OrbitEngine(Environment.GetAppContext())
        mRoomAudioEngine!!.setDefaultOpenSpeaker(false)

        val user = User()
        var versionName = BuildConfig.VERSION_NAME
        if (Build.CPU_ABI.contains("86")) {
            versionName = versionName + "-x86"
        }
        user.setName(versionName + "_" + GlobalUserState.getGlobalState().getUserName())
        user.setID(GlobalUserState.getGlobalState().getUserId())

        mRoomAudioEngine!!.start(mEnterRoomResult.room.room_id, mEnterRoomResult.media_server, user)
    }

    override fun closeAudioEngine(complete: Runnable?) {
        if (mRoomAudioEngine != null) {
            mRoomAudioEngine?.shutdown(false, complete)
            mRoomAudioEngine = null
        } else {
            complete?.run()
        }
    }

    override fun setMinimize(flag: Boolean) {
        mMinimize = flag
    }

    override fun getMinimize(): Boolean {
        return mMinimize
    }

    override fun getTag(): String {
        return mRoomDataHelper.mRoomTag
    }

    override fun getTagColor(): String {
        return mRoomDataHelper.mRoomTagColor
    }

    override fun getRoomTitle(): String {
        return mRoomDataHelper.mRoomTitle
    }

    override fun getRoomType(): String {
        return mRoomDataHelper.mRoomType
    }

    override fun getRoomIntroduce(): String {
        return mRoomDataHelper.mRoomIntroduce
    }

    override fun getSelfPosition(): Int {
        return mRoomDataHelper.mSelfPosition
    }

    override fun getSelfMember(): PrivateRoomMemberData? {
        return mRoomDataHelper.get(mRoomDataHelper.mSelfPosition)
    }

    override fun getRoomId(): String {
        return mRoomDataHelper.mRoomId
    }

    override fun getRoomPassword(): String {
        return mRoomDataHelper.mRoomPassword
    }

    override fun getOnlineNumber(): Int {
        return mRoomDataHelper.getRoomMembersTotalCount()
    }

    override fun getSeatData(position: Int): PrivateRoomSeatData? {
        return mRoomDataHelper.getSeat(position)
    }

    override fun getBackGroundType(): String {
        return mRoomDataHelper.mRoomBackGroundType
    }

    override fun getBackGroundUrl(): String {
        return mRoomDataHelper.mRoomBackGroundUrl
    }

    override fun getBlockUpSeatTime(): Long {
        return mRoomDataHelper.mBlockUpSeatDuration
    }

    override fun isBackGroundChanged(): Boolean {
        return mRoomDataHelper.mRoomBackGroundChanged
    }

    override fun isMaster(): Boolean {
        return getMember(mRoomDataHelper.mSelfPosition)?.memberIsMaster() ?: false
    }

    override fun isSendTempImageEnable(): Boolean {
        return mRoomDataHelper.getSendTempImageEnable()
    }

    override fun isSelfBanMic(): Boolean {
        return getMember(mRoomDataHelper.mSelfPosition)?.memberIsBanMic() ?: false
    }

    override fun isSelfFreeStyle(): Boolean {
        return getMember(mRoomDataHelper.mSelfPosition)?.memberIsFreeStyle() ?: false
    }

    override fun isLocked(position: Int): Boolean {
        return getSeatData(position)?.seatIsLock() ?: false
    }

    override fun isSelf(userId: String): Boolean {
        return TextUtils.equals(userId, GlobalUserState.getGlobalState().userId)
    }

    override fun isSelfPosition(position: Int): Boolean {
        return position == mRoomDataHelper.mSelfPosition
    }

    override fun isDeputy(): Boolean {
        return getMember(mRoomDataHelper.mSelfPosition)?.memberIsDeputy() ?: false
    }

    override fun isDeputyPosition(position: Int): Boolean {
        return position == PrivateRoomTools.DEPUTY_POSITION
    }

    override fun isUpSeat(position: Int): Boolean {
        return PrivateRoomTools.PositionInUpSeat(position)
    }

    override fun isOutSeat(position: Int): Boolean {
        return PrivateRoomTools.PositionInOutSeat(position)
    }

    override fun isSelfOutSeat(): Boolean {
        return isOutSeat(mRoomDataHelper.mSelfPosition)
    }

    override fun isSelfUpSeat(): Boolean {
        return isUpSeat(mRoomDataHelper.mSelfPosition)
    }

    override fun isMiniEnable(): Boolean {
        return mRoomDataHelper.mMiniModeEnable
    }

    override fun isAudioConnected(): Boolean {
        return mAudioConnect
    }

    override fun isSocketConnected(): Boolean {
        return mSockeConnect
    }

    override fun getLikeCount(): Long {
        return mRoomDataHelper.mRoomLikeCount
    }

    override fun printlnLog(info: String) {
        Log.i("PrivateRoomContext", info)
    }

    override fun destroy(complete: Runnable?) {
        printlnLog("Begin Destroy")
        mContextObserver = null
        mRoomRequestFriendFilter.clear()
        RxToolKit.UnRegister(this)
        mMinimize = false
        mRoomDataHelper.clear()
        MainApplication.getInstance().socketEngineHelper.nativeRequestMessage(Constant.LEAVE, null, null, null)
        MainApplication.getInstance().socketEngineHelper.releaseEngine(mSocketEngineToken)
        mAudioConnect = false
        GlobalUserState.getGlobalState().isGaming = false
        GlobalUserState.getGlobalState().curGameType = ""
        GlobalUserState.getGlobalState().curRoomId = ""
        closeAudioEngine(complete)
        printlnLog("Destroy")
    }

    override fun destroy() {
        super.destroy()
        destroy(null)
    }
}