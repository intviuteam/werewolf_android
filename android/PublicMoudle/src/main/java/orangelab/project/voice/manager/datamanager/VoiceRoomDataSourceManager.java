package orangelab.project.voice.manager.datamanager;

import android.content.Intent;
import android.speech.tts.Voice;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.MainApplication;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.game.component.GameMember;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.data.VoiceRoomDataSource;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.lobby.LobbyRoomHelper;
import orangelab.project.voice.lobby.api.LobbyRoomConstants;
import orangelab.project.voice.lobby.data.LobbyDataStock;
import orangelab.project.voice.lobby.model.LobbyAcceptGameBean;
import orangelab.project.voice.lobby.model.LobbyInviteGameBean;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.model.VoiceAssignedRole;
import orangelab.project.voice.model.VoiceRoomAudioConfigBean;
import orangelab.project.voice.model.VoiceRoomConfigBean;
import orangelab.project.voice.model.VoiceSpyConfigBean;
import orangelab.project.voice.model.VoiceSpyGameDeathInfo;
import orangelab.project.voice.model.VoiceSpyGameOverBean;
import orangelab.project.voice.model.VoiceSpyGuessWordResult;
import orangelab.project.voice.model.VoiceSpyStart;
import orangelab.project.voice.model.VoiceSpyVoteResult;
import orangelab.project.voice.model.VoiceToRoomBridgeBean;
import orangelab.project.voice.model.VoiceTopicBean;
import orangelab.project.voice.model.VoiceWords;
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource;
import orangelab.project.voice.musiccompany.concrete.MusicManager;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.musiccompany.model.MusicOrderSong;
import orangelab.project.voice.musiccompany.model.PrepareSingBean;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * Created by lloydfinch on 02/01/2018.
 * //语音房数据管理器
 */

public class VoiceRoomDataSourceManager extends AbstractSocketMessageHandler implements IDataSourceOption {

    private static final String TAG = "VoiceRoomDataSourceManager";

    private static VoiceRoomDataSourceManager instance;

    private IDataSourceOption dataSource;//真正操作的数据源

    public static VoiceRoomDataSourceManager getInstance() {
        if (instance == null) {
            synchronized (VoiceRoomDataSourceManager.class) {
                if (instance == null) {
                    instance = new VoiceRoomDataSourceManager();
                }
            }
        }
        return instance;
    }

    private VoiceRoomDataSourceManager() {
        dataSource = new VoiceRoomDataSource();
        initEventListener();
    }

    //本地数据处理====================================================//
    @Override
    public void init(EnterRoomResult result) {
        dataSource.init(result);
        initRoomInfo(result);
    }

    public void init(Intent intent, VoiceToRoomBridgeBean bean) {

        //设置是否从小窗进入
        VoiceRoomConfig.setIsComeInBySW(bean.isFromSW);

        //初始化房间信息
        init(bean.result);

        //初始化个人信息,小窗模式进入就不在更新个人信息,因为自己没退出
        if (!VoiceRoomConfig.isIsComeInBySW()) {
            initSelfData(intent, bean);
        }

        //初始化ktv数据
        initMusicData(bean.result.ktv);
        MusicCompanyConfig.enterTime = bean.result.enter_time;
    }

    //初始化房间信息
    private void initRoomInfo(EnterRoomResult result) {
        if (TextUtils.equals(result.room.type, Constant.GAME_TYPE_AUDIO)) {
            VoiceRoomConfig.setRoomType(result.room.child_type);
        } else {
            VoiceRoomConfig.setRoomType(result.room.type);
        }
        VoiceRoomConfig.setRoomId(result.room.room_id);
        VoiceRoomConfig.setRoomTitle(result.room.title);
        VoiceRoomConfig.setRoomType(result.room.child_type);
        VoiceRoomConfig.setPsw(result.room.password);
        VoiceRoomConfig.setUserCount(result.room.user_count);
        VoiceRoomConfig.setIsPlaying(result.room.isPlaying);
        VoiceRoomConfig.setEnterTime(result.enter_time);
        VoiceRoomConfig.SpyConfig.setCanInsertMic(result.room.config.can_cut_speaker);
        if (result.room.config.undercover != null) {
            VoiceRoomConfig.SpyConfig.setIsConfiging(result.room.config.undercover.configing);
            VoiceRoomConfig.SpyConfig.setIsConfiged(result.room.config.undercover.configed);
        }
        if (result.room.config.room_info.background != null) {
            VoiceRoomConfig.setBackground(result.room.config.room_info.background.url);
        }
        setLikeCount(result.room.likeCount);

        //2018-02-13 出初始化房契
        initDeedConfig(result.room);
    }
//=======
//    private void initRoomInfo(EnterRoomResult.EnterRoomMessage room) {
//        VoiceRoomConfig.setRoomId(room.room_id);
//        VoiceRoomConfig.setRoomTitle(room.title);
//        //如果type不是audio,就不取child_type，直接用type
//        if (TextUtils.equals(room.type, Constant.GAME_TYPE_AUDIO)) {
//            VoiceRoomConfig.setRoomType(room.child_type);
//        } else {
//            VoiceRoomConfig.setRoomType(room.type);
//        }
//        VoiceRoomConfig.setPsw(room.password);
//        VoiceRoomConfig.setUserCount(room.user_count);
//        VoiceRoomConfig.setIsPlaying(room.isPlaying);
//        VoiceRoomConfig.SpyConfig.setCanInsertMic(room.config.can_cut_speaker);
//        if (room.config.undercover != null) {
//            VoiceRoomConfig.SpyConfig.setIsConfiging(room.config.undercover.configing);
//            VoiceRoomConfig.SpyConfig.setIsConfiged(room.config.undercover.configed);
//        }
//        if (room.config.room_info.background != null) {
//            VoiceRoomConfig.setBackground(room.config.room_info.background.url);
//        }
//        setLikeCount(room.likeCount);
//
//        //2018-02-13 初始化房契
//        initDeedConfig(room);
//>>>>>>> origin/game_werewolf_release_ktv_new_ui
//    }

    //初始化房契相关信息
    private void initDeedConfig(EnterRoomResult.EnterRoomMessage room) {
        VoiceRoomConfig.setIsSupportDeed(room.config.purchase_room);
        if (VoiceRoomConfig.isIsSupportDeed()) {
            VoiceRoomConfig.setRoomOwnerId(room.config.room_info.owner_id);
            if (PositionHelper.isSelf(room.config.room_info.owner_id)) {
                //是自己
                VoiceRoomConfig.setIsOwnerRoom(true);
            } else {
                VoiceRoomConfig.setIsOwnerRoom(false);
            }
            if (TextUtils.equals(room.config.room_info.owner_type, VoiceConstants.PURCHASED)) {
                VoiceRoomConfig.setIsPurchased(true);
            } else {
                VoiceRoomConfig.setIsPurchased(false);
            }
        } else {
            //不支持房契功能了,设置为false
            VoiceRoomConfig.setIsOwnerRoom(false);
            VoiceRoomConfig.setIsPurchased(false);
        }
    }

    //初始化个人信息
    private void initSelfData(Intent intent, VoiceToRoomBridgeBean bean) {
        EnterRoomResult result = bean.result;
        GlobalUserState.getGlobalState().setCurRoomId(result.room.room_id);
        GlobalUserState.getGlobalState().setCurGameType(IntentDataHelper.getGameType(intent));
        PersonalData myself = new PersonalData();
        myself.setUserExp(IntentDataHelper.getUserExp(intent));
        myself.setUserName(IntentDataHelper.getUserName(intent));
        myself.setUserId(IntentDataHelper.getUserID(intent));
        myself.setImageUrl(IntentDataHelper.getUserAvater(intent));
        myself.setGameType(IntentDataHelper.getGameType(intent));
        myself.setToken(IntentDataHelper.getUserToken(intent));
        myself.setUserSex(IntentDataHelper.getUserSex(intent));
        myself.setRoomId(result.room.room_id);
        myself.setSelfPosition(result.position);
        EnterRoomResult.EnterRoomUserItem item = findUserById(myself.getUserId());
        if (item != null) {
            myself.setUid(item.uid);
        }
        PersonalDataHelper.setPersonalData(myself);
    }

    private void initMusicData(EnterRoomResult.KTV ktv) {
        if (ktv != null) {
            //清空已点的歌
            MusicCompanyDataSource.INSTANCE.clear();
            //添加歌单
            if (ktv.songs != null) {
                for (PrepareSingBean.PrepareSong song : ktv.songs) {
                    MusicOrderSong temp = TransferClassHelper.MusicPrepareSong2MusicOrderSong(song);
                    if (temp != null) {
                        MusicCompanyDataSource.INSTANCE.addToSongMenu(temp);
                    }
                }
                MusicCompanyDataSource.INSTANCE.updateNextSong();
            }

            if (ktv.prepared_song != null) {
                MusicCompanyConfig.isPrepare = true;
                MusicCompanyConfig.isSing = false;
                MusicCompanyConfig.startTime = ktv.prepared_song.getStart_time();
                MusicCompanyConfig.timeLength = Long.parseLong(ktv.prepared_song.getTimelength());
                MusicOrderSong song = TransferClassHelper.MusicPrepareSong2MusicOrderSong(ktv.prepared_song);
                MusicCompanyConfig.orderId = song.orderId;
                MusicCompanyConfig.musicId = song.id;
                MusicCompanyConfig.albumUrl = song.headImg;
                MusicCompanyConfig.lyricUrl = song.lyricUrl;
                MusicCompanyConfig.playUrl = song.playUrl;
                MusicCompanyConfig.roomID = ktv.prepared_song.getRoom_id();
                MusicCompanyConfig.songName = song.songName;
                MusicCompanyConfig.orderName = song.order;
                MusicCompanyConfig.orderImg = song.orderImg;
            }

            //设置当前播放歌曲
            if (ktv.current_song != null) {
                MusicCompanyConfig.isPrepare = false;
                MusicCompanyConfig.isSing = true;
                MusicCompanyConfig.startTime = ktv.current_song.getStart_time();
                MusicCompanyConfig.timeLength = Long.parseLong(ktv.current_song.getTimelength());
                MusicOrderSong song = TransferClassHelper.MusicPrepareSong2MusicOrderSong(ktv.current_song);
                MusicCompanyConfig.currentSong = song;
                MusicCompanyConfig.orderId = song.orderId;
                MusicCompanyConfig.musicId = song.id;
                MusicCompanyConfig.albumUrl = song.headImg;
                MusicCompanyConfig.lyricUrl = song.lyricUrl;
                MusicCompanyConfig.playUrl = song.playUrl;
                MusicCompanyConfig.roomID = ktv.current_song.getRoom_id();
                MusicCompanyConfig.songName = song.songName;
                MusicCompanyConfig.orderName = song.order;
                MusicCompanyConfig.orderImg = song.orderImg;
            }

            //设置可以唱歌的人
            if (ktv.authorized_user != null) {
                if (!TextUtils.isEmpty(ktv.authorized_user.user_id)) {
                    MusicCompanyConfig.authorizedID = ktv.authorized_user.user_id;
                    if (ktv.current_song != null) {
                        MusicCompanyConfig.singerId = ktv.authorized_user.user_id;
                    } else {
                        MusicCompanyConfig.singerId = "";
                    }
                    EnterRoomResult.EnterRoomUserItem singer = findUserById(ktv.authorized_user.user_id);

                    //伴奏数据初始化
                    List<EnterRoomResult.EnterRoomUserItem> onLineUsers = getOnLineUsers();
                    for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
                        if (TextUtils.equals(ktv.authorized_user.user_id, user.id)) {
                            user.canSing = true;
                            if (ktv.current_song != null) {
                                user.isSing = true;
                            } else {
                                user.isSing = false;
                            }
                        } else {
                            user.canSing = false;
                            user.isSing = false;
                        }
                    }
                    PersonalData personalData = PersonalDataHelper.getPersonalData();
                    if (PositionHelper.isSelf(ktv.authorized_user.user_id)) {
                        personalData.setCanSing(true);
                        if (ktv.current_song != null) {
                            personalData.setSing(true);
                        } else {
                            personalData.setSing(false);
                        }
                    } else {
                        personalData.setCanSing(false);
                        personalData.setSing(false);
                    }
                }
            }
        }
    }

    //注册事件监听器，只接收有可能改变数据的事件
    private void initEventListener() {

        //用来接收"加好友"对话框处理后发送的event
        RxToolKit.Build(this, VoiceEvent.AddFriendEvent.class).action(event -> {
            //removeAddFriend(event.id);
        }).register();
    }

    @Override
    public void setSync(boolean isSync) {
        dataSource.setSync(isSync);
    }

    @Override
    public void addUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.addUser(user);
    }

    @Override
    public void addOnSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.addOnSeatUser(user);
    }

    @Override
    public void addDownSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.addDownSeatUser(user);
    }

    @Override
    public void removeUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.removeUser(user);
    }

    @Override
    public void removeOnSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.removeOnSeatUser(user);
    }

    @Override
    public void removeDownSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        dataSource.removeDownSeatUser(user);
    }

    @Override
    public void clearOnLineUsers() {
        dataSource.clearOnLineUsers();
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getOnLineUsers() {
        return dataSource.getOnLineUsers();
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getOnSeatUsers() {
        return dataSource.getOnSeatUsers();
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getDownSeatUsers() {
        return dataSource.getDownSeatUsers();
    }

    @Override
    public void setLockedSeatList(List<Integer> lockedSeatList) {
        dataSource.setLockedSeatList(lockedSeatList);
    }

    @Override
    public List<Integer> getLockedSeatList() {
        return dataSource.getLockedSeatList();
    }

    @Override
    public void clearLockedSeatList() {
        dataSource.clearLockedSeatList();
    }

    @Override
    public void addLockedSeat(Integer position) {
        dataSource.addLockedSeat(position);
    }

    @Override
    public void removeLockedSeat(Integer position) {
        dataSource.removeLockedSeat(position);
    }

    @Override
    public EnterRoomResult.EnterRoomUserItem findUserByPosition(int position) {
        return dataSource.findUserByPosition(position);
    }

    @Override
    public EnterRoomResult.EnterRoomUserItem findUserById(String id) {
        return dataSource.findUserById(id);
    }

    @Override
    public void addMessage(Long key, SystemMessageItem msg) {
        dataSource.addMessage(key, msg);
    }

    @Override
    public Collection<SystemMessageItem> getMessageList() {
        return dataSource.getMessageList();
    }

    @Override
    public SystemMessageItem getLatestMessage(Long key) {
        return dataSource.getLatestMessage(key);
    }

    @Override
    public void addFriendDialog(VoiceAddFriendBean bean) {
        dataSource.addFriendDialog(bean);
    }

    @Override
    public void removeAddFriend(String id) {
        dataSource.removeAddFriend(id);
    }

    @Override
    public void markAddFriendHandled(String id) {
        dataSource.markAddFriendHandled(id);
    }

    @Override
    public VoiceAddFriendBean getLatestAddFriendBean() {
        return dataSource.getLatestAddFriendBean();
    }

    @Override
    public List<VoiceAddFriendBean> getAddFriendIdList() {
        return dataSource.getAddFriendIdList();
    }

    @Override
    public void clearAddFriendDialogList() {
        dataSource.clearAddFriendDialogList();
    }

    @Override
    public void setLikeCount(int likeCount) {
        dataSource.setLikeCount(likeCount);
    }

    @Override
    public int getLikeCount() {
        return dataSource.getLikeCount();
    }

    @Override
    public VoiceRoomAudioConfigBean getAudioConfig() {
        return dataSource.getAudioConfig();
    }

    @Override
    public void clearAudioConfig() {
        dataSource.clearAudioConfig();
    }

    @Override
    public void filterSameLobbyGame(String userId, String gameType) {
        dataSource.filterSameLobbyGame(userId, gameType);
    }

    @Override
    public void filterLeaveType(String userId, String leaveType) {
        dataSource.filterLeaveType(userId, leaveType);
    }

    @Override
    public void filterSpecialMessage(String userId, String inviteId) {
        dataSource.filterSpecialMessage(userId, inviteId);
    }

    @Override
    public void removeMessageById(String userId) {
        dataSource.removeMessageById(userId);
    }

    @Override
    public void removeMessageByInviteId(String inviteId) {
        dataSource.removeMessageByInviteId(inviteId);
    }

    //本地数据处理====================================================//


    /**
     * 游戏大厅不展示进入/退出消息
     */
    private boolean needMsg() {
        return !VoiceRoomConfig.isLobby();
    }

    //Socket消息处理====================================================//
    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        //更新用户列表
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson(messageBean.payload.toString(), ServerMessageEnterResult.class);
        EnterRoomResult.EnterRoomUserItem member = TransferClassHelper.EnterResult2EnterRoomUser(item);
        //enter的时候列表里已有自己,这里不再添加自己
        if (!PositionHelper.isSelf(member.id)) {
            PLog.e(TAG, "add user");
            addUser(member);
        }

        //更新系统消息列表
        if (needMsg()) {
            addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_leave),
                    MessageUtils.getString(R.string.str_comein, item.user.name)));
        }

        //这里如果当前是大厅，就把所有进入的人的消息还原
        if (VoiceRoomConfig.isLobby()) {
            //还原邀请消息为正常形式
            filterLeaveType(item.user.id, LobbyRoomConstants.JOIN_LOBBY);

            //删除此人的邀请消息
            //removeMessageById(item.user.id);
        }
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        //更新用户列表
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        removeUser(user);

        //更新系统消息列表
        if (needMsg()) {
            addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_leave),
                    MessageUtils.getString(R.string.str_leave, user.name)));
        }

        //leave添加type
        if (user != null) {
            String type = messageBean.payload.optString(VoiceConstants.TYPE);
            //刷新UI为对战中
            filterLeaveType(user.id, type);
        }
    }

    @Override
    public void handleForceSeat(VoiceMessageBean messageBean) {
        handleUpSeat(messageBean);
    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user != null) {
            int newPosition = messageBean.payload.optInt(VoiceConstants.DEST_POSITION);
            user.position = newPosition;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setSelfPosition(user.position);
                }
            }

            //禁麦期间被转让成房主,就解除禁麦
            if (PositionHelper.isMaster(user.position)) {
                if (user.isBanMic()) {
                    user.state = VoiceConstants.SPACE_STR;
                    if (user.position == PersonalDataHelper.getPersonalData().getSelfPosition()) {
                        PersonalDataHelper.getPersonalData().setBanMic(false);
                    }
                }
            }

            //更新消息列表
            if (needMsg()) {
                if (PositionHelper.isMaster(newPosition)) {
                    addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                            MainApplication.getInstance().getResources().getColor(R.color.color_voice_up_down_seat),
                            MessageUtils.getString(R.string.str_new_master, user.name)));
                } else {
                    addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                            MainApplication.getInstance().getResources().getColor(R.color.color_voice_up_down_seat),
                            MessageUtils.getString(R.string.str_up_seat, user.name, newPosition)));
                }
            }
        } else {
            PLog.e(TAG, "handleUpSeat: user is null");
        }
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        int desPosition = messageBean.payload.optInt(VoiceConstants.DEST_POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user != null) {

            //更新系统消息列表
            if (needMsg()) {
                addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().getResources().getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_down_seat, user.name, user.position)));
            }

            //更改用户属性
            user.position = desPosition;
            user.speaking = false;
            user.state = VoiceConstants.SPACE_STR;
            user.prepared = false;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setSelfPosition(desPosition);
                    personalData.setSpeaking(false);//下麦停止说话
                    personalData.setFreeStyle(false);//下麦解除自由模式
                    personalData.setBanMic(false);//下麦解除禁言
                    personalData.setPrepared(false);//下麦取消准备
                }
            }
        } else {
            PLog.e(TAG, "handleDownSeat: user is null");
        }
    }

    @Override
    public void handleSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.speaking = true;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setSpeaking(true);
                }
            }
        } else {
            PLog.e(TAG, "handleSpeak: user is null");
        }
    }

    @Override
    public void handleUnSpeak(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.speaking = false;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setSpeaking(false);
                }
            }
        } else {
            PLog.e(TAG, "handleSpeak: user is null");
        }
    }

    @Override
    public void handleKickOut(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            removeUser(user);

            //更新消息列表
            if (needMsg()) {
                addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().getResources().getColor(R.color.color_voice_leave),
                        MessageUtils.getString(R.string.str_leave, user.name)));
            }
        } else {
            PLog.e(TAG, "handleKickOut: user is null");
        }
    }

    @Override
    public void handleChat(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        String message = messageBean.payload.optString(VoiceConstants.MESSAGE);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            String userName = user.name != null ? user.name : "[" + user.position + "]" + MessageUtils.getString(R.string.str_num_uer);
            JSONObject gift = messageBean.payload.optJSONObject(VoiceConstants.GIFT_CHAT);
            int msgPosition = position + 1;
            if (gift != null) {
                String type = gift.optString(VoiceConstants.GIFT_TYPE);
                SystemMessageItem messageItem = SystemMessageItem.createGiftMessage(
                        MainApplication.getInstance().getResources().getColor(R.color.color_voice_gift),
                        msgPosition,
                        message,
                        type,
                        userName);
                messageItem.setUser(user);
                addMessage(messageBean.msg_id, messageItem);
            } else {
                SystemMessageItem messageItem = SystemMessageItem.createMemberMessage(
                        MainApplication.getInstance().getResources().getColor(android.R.color.white),
                        msgPosition,
                        userName,
                        message);
                messageItem.setUser(user);
                addMessage(messageBean.msg_id, messageItem);
            }

            //如果是自己发言，就取消屏蔽，在view中会刷新消息
            if (PositionHelper.isSelf(user.id)) {
                VoiceRoomConfig.setIsHiddenMsg(false);
            }
        } else {
            PLog.e(TAG, "handleChat: user is null");

            //大厅消息的处理
            String name = messageBean.payload.optString("name");
            int position1 = messageBean.payload.optInt("position");
            SystemMessageItem messageItem = SystemMessageItem.createMemberMessage(
                    MainApplication.getInstance().getResources().getColor(android.R.color.white),
                    position + 1,
                    name,
                    message);
            user = new EnterRoomResult.EnterRoomUserItem();
            user.name = name;
            user.position = position1;
            messageItem.setUser(user);
            addMessage(messageBean.msg_id, messageItem);

            //如果是自己发言，就取消屏蔽，在view中会刷新消息
            if (PositionHelper.isSelfPosition(position1)) {
                VoiceRoomConfig.setIsHiddenMsg(false);
            }
        }
    }

    @Override
    public void handleSystemMsg(VoiceMessageBean messageBean) {
        String content = messageBean.payload.optString(VoiceConstants.CONTENT);
        addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                MainApplication.getInstance().getResources().getColor(R.color.color_voice_leave),
                content));
    }

    @Override
    public void handleLocked(VoiceMessageBean messageBean) {
        //this function in the class is no egg use
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
        if (result != null) {
            restoreConfig(result);
            restoreRoomInfo(result.room_info);
            restoreGameInfo(result.game_info);

            initMusicData(result.ktv);
            MusicCompanyConfig.enterTime = result.enter_time;
        }
    }

    //恢复配置信息
    private void restoreConfig(RestoreResult restoreResult) {
        EnterRoomResult.EnterRoomMessage roomMessage = restoreResult.room_info;
        String titleType = restoreResult.room_info.type;
        //如果type不是audio,就不取child_type，直接用type
        if (TextUtils.equals(titleType, Constant.GAME_TYPE_AUDIO)) {
            titleType = restoreResult.room_info.child_type;
        }

        //断网重连房间类型改变判断
        if (VoiceRoomConfig.isIsKTV() && !TextUtils.equals(titleType, Constant.GAME_TYPE_KTV) ||
                !VoiceRoomConfig.isIsKTV() && TextUtils.equals(titleType, Constant.GAME_TYPE_KTV)) {
            VoiceRoomConfig.needChangeUI = true;
        } else {
            VoiceRoomConfig.needChangeUI = false;
        }

        VoiceRoomConfig.setRoomType(titleType);
        VoiceRoomConfig.setRoomTitle(roomMessage.title);
        VoiceRoomConfig.setPsw(roomMessage.password);
        VoiceRoomConfig.setUserCount(roomMessage.user_count);
        try {
            VoiceRoomConfig.setBackground(roomMessage.config.room_info.background.url);
        } catch (Exception e) {
            PLog.e(TAG, "restoreConfig: " + e.getMessage());
        }
        VoiceRoomConfig.setIsPlaying(roomMessage.isPlaying);
        VoiceRoomConfig.setMaxChairNumber(restoreResult.room_info.config.active_count - 1);
        if (roomMessage.config.undercover != null) {
            VoiceRoomConfig.SpyConfig.setIsConfiging(roomMessage.config.undercover.configing);
            VoiceRoomConfig.SpyConfig.setIsConfiged(roomMessage.config.undercover.configed);

            //四个布尔值还原
            VoiceRoomConfig.SpyConfig.setAddOneSpy(roomMessage.config.undercover.more_undercover);
            VoiceRoomConfig.SpyConfig.setSetSpyToGhost(roomMessage.config.undercover.has_ghost);
            VoiceRoomConfig.SpyConfig.setCanInsertMic(roomMessage.config.can_cut_speaker);
            VoiceRoomConfig.SpyConfig.setCanGuessWord(roomMessage.config.undercover.can_guess);
        }

        //两个词语还原
        if (restoreResult.game_info != null) {
            if (restoreResult.game_info.words != null) {
                VoiceRoomConfig.SpyConfig.setFirst(restoreResult.game_info.words.first);
                VoiceRoomConfig.SpyConfig.setSecond(restoreResult.game_info.words.second);
            }
        }

        //房契相关还原
        VoiceRoomConfig.setRoomOwnerId(restoreResult.room_info.config.room_info.owner_id);
        VoiceRoomConfig.setIsSupportDeed(restoreResult.room_info.config.purchase_room);
        if (PositionHelper.isSelf(restoreResult.room_info.config.room_info.owner_id)) {
            //是自己
            VoiceRoomConfig.setIsOwnerRoom(true);
        } else {
            VoiceRoomConfig.setIsOwnerRoom(false);
        }
        if (TextUtils.equals(restoreResult.room_info.config.room_info.owner_type, VoiceConstants.PURCHASED)) {
            VoiceRoomConfig.setIsPurchased(true);
        } else {
            VoiceRoomConfig.setIsPurchased(false);
        }
    }

    //恢复房间信息
    private void restoreRoomInfo(EnterRoomResult.EnterRoomMessage roomInfo) {

        //恢复在线用户
        clearOnLineUsers();
        Iterator iterator = roomInfo.users.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            EnterRoomResult.EnterRoomUserItem member = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            PLog.e(TAG, "user" + member);
            if (member == null) {
                continue;
            }
            addUser(member);
        }

        setLockedSeatList(roomInfo.locked_positions);

        //恢复个人信息
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            EnterRoomResult.EnterRoomUserItem mySelf = findUserById(GlobalUserState.getGlobalState().getUserId());
            if (mySelf != null) {

                //更新个人的所有信息
                PersonalData newPersonalData = TransferClassHelper.EnterRoomItem2PersonalData(mySelf);
                PersonalDataHelper.setPersonalData(newPersonalData);
            } else {
                //被踢了, 将自己位置设为极大
                personalData.setSelfPosition(VoiceConstants.OVER_POSITION);
            }
        } else {
            //已经离开房间
        }

        //恢复点赞数
        setLikeCount(roomInfo.likeCount);
    }

    //恢复游戏信息
    private void restoreGameInfo(RestoreResult.RestoreRoleMessage gameInfo) {

        //恢复个人词语
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            personalData.setSelfRole(gameInfo.role);
        }

        //恢复出局信息
        if (gameInfo != null) {
            if (gameInfo.death_info != null && gameInfo.death_info.size() > 0) {
                for (RestoreResult.GameInfoDeathInfo deathInfo : gameInfo.death_info) {
                    EnterRoomResult.EnterRoomUserItem user = findUserByPosition(deathInfo.position);
                    if (user != null) {
                        user.isOut = true;
                        if (personalData != null) {
                            if (PositionHelper.isSelf(user.id)) {
                                personalData.setOut(true);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.position = VoiceConstants.MASTER_POSITION;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setSelfPosition(VoiceConstants.MASTER_POSITION);
                }
            }

            //处理系统消息
            if (needMsg()) {
                addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().getResources().getColor(R.color.color_voice_up_down_seat),
                        MessageUtils.getString(R.string.str_new_master, user.name)));
            }
        } else {
            PLog.e(TAG, "handleChangeRoomOwner: user is null");
        }

    }

    @Override
    public void handleUpdateTitle(VoiceMessageBean messageBean) {
        VoiceTopicBean topicBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceTopicBean.class);
        if (topicBean != null) {
            if (!TextUtils.equals(VoiceRoomConfig.getRoomTitle(), topicBean.title) ||
                    !TextUtils.equals(VoiceRoomConfig.getRoomType(), topicBean.type)) {
                addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                        MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                        MessageUtils.getString(R.string.str_voice_change_title)));
            }
            VoiceRoomConfig.setRoomTitle(topicBean.title);
            if (VoiceRoomConfig.isIsKTV() &&
                    !TextUtils.equals(topicBean.type, com.networktoolkit.transport.Constant.GAME_TYPE_KTV)) {
                //ktv切换到其他房间
                try {
                    MusicManager.Companion.getInstance().stop();
                    MusicCompanyConfig.destroy();
                    MusicManager.Companion.getInstance().destroy();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (VoiceRoomConfig.isIsKTV() && !TextUtils.equals(topicBean.type, Constant.GAME_TYPE_KTV) ||
                    !VoiceRoomConfig.isIsKTV() && TextUtils.equals(topicBean.type, Constant.GAME_TYPE_KTV)) {
                VoiceRoomConfig.needChangeUI = true;
            } else {
                VoiceRoomConfig.needChangeUI = false;
            }
            VoiceRoomConfig.setRoomType(topicBean.type);
            String bgUrl = topicBean.theme == null ? "" : topicBean.theme.background;
            VoiceRoomConfig.setBackground(bgUrl);
        }

        if (needMsg()) {
            addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                    MessageUtils.getString(R.string.str_voice_change_title)));
        }

        MusicCompanyDataSource.INSTANCE.getObserver().handleUpdateTitle(messageBean);
    }

    @Override
    public void handleChangePassword(VoiceMessageBean messageBean) {
        String password = messageBean.payload.optString(VoiceConstants.PASSWORD);
        VoiceRoomConfig.setPsw(password);
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            personalData.setCurPassword(password);
            GlobalUserState.getGlobalState().setPassword(password);
        }
    }

    @Override
    public void handleAddFriend(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        List<VoiceAddFriendBean> addFriendIdList = getAddFriendIdList();
        if (!hasAdded(userId)) {
            addFriendIdList.add(new VoiceAddFriendBean(userId, false));
        }
    }

    @Override
    public void handleLikeRoom(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_CAMEL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload) + 1;
        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
        if (user != null) {
            if (needMsg()) {
                String userName = user.name != null ? user.name : "[" + user.position + "]" + MessageUtils.getString(R.string.str_num_uer);
                SystemMessageItem messageItem = SystemMessageItem.createMemberMessage(
                        MainApplication.getInstance().getResources().getColor(android.R.color.white),
                        position, userName, MessageUtils.getString(R.string.str_voice_like));
                messageItem.setUser(user);
                addMessage(messageBean.msg_id, messageItem);
            }
        } else {
            PLog.e(TAG, "handleLikeRoom: user is null");
        }
    }

    @Override
    public void handleChangeState(VoiceMessageBean messageBean) {
        String model = messageBean.payload.optString(VoiceConstants.MODEL);
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            if (TextUtils.equals(VoiceConstants.LIMIT, model)) {
                handleBanMic(messageBean.msg_id, user);
            } else if (TextUtils.equals(VoiceConstants.FREE, model)) {
                handleFreeStyle(messageBean.msg_id, user);
            } else if (TextUtils.equals(VoiceConstants.SPACE_STR, model) || TextUtils.isEmpty(model)) {
                if (!TextUtils.isEmpty(user.state)) {
                    if (TextUtils.equals(VoiceConstants.LIMIT, user.state)) {
                        handleUnBanMic(messageBean.msg_id, user);
                    } else if (TextUtils.equals(VoiceConstants.FREE, user.state)) {
                        handleUnFreeStyle(messageBean.msg_id, user);
                    }
                }
            }
        } else {
            PLog.e(TAG, "handleChangeState: user is null");
        }
    }

    //禁言
    private void handleBanMic(Long key, EnterRoomResult.EnterRoomUserItem user) {
        user.state = VoiceConstants.LIMIT;
        user.speaking = false;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (PositionHelper.isSelf(user.id)) {
                personalData.setBanMic(true);
                personalData.setSpeaking(false);
            }
        }

        if (needMsg()) {
            addMessage(key, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                    MessageUtils.getString(R.string.str_voice_ban_mic1, user.position, user.name)));
        }
    }

    //开自由
    private void handleFreeStyle(Long key, EnterRoomResult.EnterRoomUserItem user) {
        user.state = VoiceConstants.FREE;
        user.speaking = true;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (PositionHelper.isSelf(user.id)) {
                personalData.setFreeStyle(true);
                personalData.setSpeaking(true);
            }
        }
        if (needMsg()) {
            addMessage(key, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                    MessageUtils.getString(R.string.str_voice_free_style, user.position, user.name)));
        }
    }

    //解除禁言
    private void handleUnBanMic(Long key, EnterRoomResult.EnterRoomUserItem user) {
        user.state = VoiceConstants.SPACE_STR;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (PositionHelper.isSelf(user.id)) {
                personalData.setBanMic(false);
            }
        }
        addMessage(key, SystemMessageItem.createSystemMessage(
                MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                MessageUtils.getString(R.string.str_voice_unban_mic1, user.position, user.name)));
    }

    //解除自由
    private void handleUnFreeStyle(Long key, EnterRoomResult.EnterRoomUserItem user) {
        user.state = VoiceConstants.SPACE_STR;
        user.speaking = false;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (PositionHelper.isSelf(user.id)) {
                personalData.setFreeStyle(false);
                personalData.setSpeaking(false);
            }
        }
        addMessage(key, SystemMessageItem.createSystemMessage(
                MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                MessageUtils.getString(R.string.str_voice_un_free_style, user.position, user.name)));
    }

    @Override
    public void handleEmotion(VoiceMessageBean messageBean) {
        //no egg use
    }

    @Override
    public void handleSmallGame(VoiceMessageBean messageBean) {
        JSONObject data = messageBean.payload.optJSONObject(VoiceConstants.DATA);
        if (data != null) {
            int code = data.optInt(VoiceConstants.CODE);
            if (code == VoiceConstants.BALANCE_NO_ENOUGH) {
                //donn't handle
            } else if (code == VoiceConstants.NO_RECOGNIZED) {
                //donn't handle
            } else {
                String mark = messageBean.payload.optString(VoiceConstants.MARK);
                int result = data.optInt(VoiceConstants.RES);
                int position = messageBean.payload.optInt(VoiceConstants.POSITION);
                EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
                if (user != null) {
                    String userName = user.name != null ? user.name : "[" + user.position + "]" + MessageUtils.getString(R.string.str_num_uer);
                    //打分,麦下的只显示提示信息
                    if (TextUtils.equals(VoiceConstants.POINTS, mark)) {
                        SystemMessageItem messageItem = SystemMessageItem.createScoreMessage(
                                position + 1,
                                result,
                                userName);
                        messageItem.setUser(user);
                        addMessage(messageBean.msg_id, messageItem);
                    }
                } else {
                    PLog.e(TAG, "handleSmallGame: user is null");
                }
            }
        }
    }

    @Override
    public void handleCardCheckResult(VoiceMessageBean messageBean) {
        //no egg use in there
    }

    @Override
    public void handleSpyStartConfig() {
        VoiceRoomConfig.SpyConfig.setIsConfiging(true);
        VoiceRoomConfig.SpyConfig.setIsConfiged(false);
    }

    @Override
    public void handleGetWords(VoiceMessageBean messageBean) {
        if (!VoiceRoomConfig.SpyConfig.isIsConfiging()) {
            VoiceWords words = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceWords.class);
            VoiceSocketHelper.sendSpySettingUpdate(
                    VoiceRoomConfig.SpyConfig.isAddOneSpy(),
                    VoiceRoomConfig.SpyConfig.isSetSpyToGhost(),
                    VoiceRoomConfig.SpyConfig.isCanInsertMic(),
                    VoiceRoomConfig.SpyConfig.isCanGuessWord(),
                    words.first,
                    words.second);
        }
    }

    @Override
    public void handleSpyUpdateConfig(VoiceMessageBean messageBean) {
        VoiceSpyConfigBean spyConfigBean = new VoiceSpyConfigBean();
        spyConfigBean.first = messageBean.payload.optString("first");
        spyConfigBean.second = messageBean.payload.optString("second");
        if (messageBean.payload.has("config")) {
            JSONObject configObject = messageBean.payload.optJSONObject("config");
            if (configObject != null) {
                VoiceRoomConfigBean roomConfigBean
                        = GsonHelper.getGson().fromJson(configObject.toString(), VoiceRoomConfigBean.class);
                if (roomConfigBean != null) {
                    spyConfigBean.can_cut_speaker = roomConfigBean.can_cut_speaker;
                    spyConfigBean.can_guess = roomConfigBean.undercover.can_guess;
                    spyConfigBean.has_ghost = roomConfigBean.undercover.has_ghost;
                    spyConfigBean.more_undercover = roomConfigBean.undercover.more_undercover;
                    VoiceRoomConfig.SpyConfig.setIsConfiging(roomConfigBean.undercover.configing);
                    VoiceRoomConfig.SpyConfig.setIsConfiged(roomConfigBean.undercover.configed);
                }
            }
        }
        VoiceRoomConfig.SpyConfig.setSpyConfig(spyConfigBean);
    }

    @Override
    public void handleSpyStopConfig() {
        VoiceRoomConfig.SpyConfig.setIsConfiging(false);
        VoiceRoomConfig.SpyConfig.setIsConfiged(true);
    }

    @Override
    public void handleSpyGamePrepare(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.prepared = true;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setPrepared(true);
                }
            }
        } else {
            PLog.e(TAG, "handleSpyGamePrepare: user is null");
        }
    }

    @Override
    public void handleSpyGameUnPrepare(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.prepared = false;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setPrepared(false);
                }
            }
        } else {
            PLog.e(TAG, "handleSpyGameUnPrepare: user is null");
        }
    }

    @Override
    public void handleSpyGameStart(VoiceMessageBean messageBean) {
        try {
            VoiceSpyStart spyStart = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyStart.class);
            VoiceRoomConfig.SpyConfig.setSpyStart(spyStart);
            VoiceRoomConfig.setIsPlaying(true);
        } catch (Exception e) {
            PLog.e(TAG, "handleSpyGameStart error:" + e.getMessage());
        }
    }

    @Override
    public void handleAssignedRole(VoiceMessageBean messageBean) {
        VoiceAssignedRole role = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceAssignedRole.class);
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            personalData.setSelfRole(role.role);
        }
    }

    @Override
    public void handleSpyGameSpeech(VoiceMessageBean messageBean) {
        //no egg use in there
    }

    @Override
    public void handleSpyGameEndSpeech(VoiceMessageBean messageBean) {
        //no egg use in there
    }

    @Override
    public void handleBeforeVote(VoiceMessageBean messageBean) {
        //no egg use in there
    }

    @Override
    public void handleSpyGameVoteResult(VoiceMessageBean messageBean) {
        VoiceSpyVoteResult voteResult = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyVoteResult.class);

        if (TextUtils.equals(voteResult.type, "death")) {
            if (voteResult.finished) {
                if (voteResult.positions != null && voteResult.positions.size() > 0) {
                    //处理投票结果
                    StringBuilder sbVoteResult = new StringBuilder();
                    Set<Integer> beVoted = voteResult.vote_info.keySet();
                    for (int v : beVoted) {
                        List<Integer> voted = voteResult.vote_info.get(v);
                        StringBuilder votedSb = new StringBuilder();
                        for (int i : voted) {
                            votedSb.append(i).append("、");
                        }
                        if (v > 0) {
                            //正常投票
                            String votedStr = votedSb.deleteCharAt(votedSb.length() - 1).toString();
                            String voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg, votedStr, v);
                            sbVoteResult.append(voteInfo).append("\n");
                        } else {
                            //弃票
                            String votedStr = votedSb.deleteCharAt(votedSb.length() - 1).toString();
                            String voteInfo = MessageUtils.getString(R.string.str_voice_vote_msg_give_up_number, votedStr);
                            sbVoteResult.append(voteInfo).append("\n");
                        }
                    }
                    //有平票
                    if (voteResult.need_pk) {
                        //处理PK消息
                        if (voteResult.positions != null && voteResult.positions.size() >= 2) {
                            String message = "";
                            Collections.sort(voteResult.positions);
                            if (voteResult.positions.size() == 2) {
                                message = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold, voteResult.positions.get(0), voteResult.positions.get(1))).toString();
                            } else {
                                StringBuilder sb = new StringBuilder();
                                for (Integer position : voteResult.positions) {
                                    sb.append(position).append("、");
                                }
                                String votePositions = sb.deleteCharAt(sb.length() - 1).toString();
                                message = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk_bold_more, votePositions)).toString();
                            }
                            //添加PK信息
                            sbVoteResult.append(message);
                        }
                    } else {
                        //无PK，删除最后一个换行符
                        sbVoteResult.deleteCharAt(sbVoteResult.length() - 1);
                    }
                    addMessage(messageBean.msg_id, SystemMessageItem.createSpyGameMessage(MessageUtils.getString(R.string.str_voice_vote_result), sbVoteResult.toString(), Gravity.LEFT));
                } else {
                    //所有玩家弃票
                    addMessage(messageBean.msg_id, SystemMessageItem.createSpyGameMessage(MessageUtils.getString(R.string.str_voice_vote_result), MessageUtils.getString(R.string.str_voice_vote_msg_give_up), Gravity.CENTER));
                }
            }
        }
    }

    @Override
    public void handleDeathInfo(VoiceMessageBean messageBean) {
        VoiceSpyGameDeathInfo deathInfo = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameDeathInfo.class);
        if (deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
            VoiceSpyGameDeathInfo.DeathInfo info = deathInfo.death_info.get(0);
            if (info != null) {
                EnterRoomResult.EnterRoomUserItem user = findUserByPosition(info.killed);
                if (user != null) {
                    user.isOut = true;
                    PersonalData personalData = PersonalDataHelper.getPersonalData();
                    if (personalData != null) {
                        if (PositionHelper.isSelf(user.id)) {
                            personalData.setOut(true);
                        }
                    }
                }

                //处理系统消息
                String role = TextUtils.equals(VoiceConstants.SPY_TAG, info.role) ?
                        MessageUtils.getString(R.string.str_voice_role_spy) :
                        MessageUtils.getString(R.string.str_voice_role_people);
                String title = MessageUtils.getString(R.string.str_voice_death_info);
                String msg = Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_identity_bold, info.killed, role)).toString();
                addMessage(messageBean.msg_id, SystemMessageItem.createSpyGameMessage(title, msg, Gravity.CENTER));
            }
        }
    }

    @Override
    public void handleSpyGameGuessWord(VoiceMessageBean messageBean) {

    }

    @Override
    public void handleGuessWordResult(VoiceMessageBean messageBean) {
        VoiceSpyGuessWordResult wordResult = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGuessWordResult.class);
        if (wordResult != null) {
            String title = MessageUtils.getString(R.string.str_voice_bomb_word_result);
            String msg = wordResult.success ? MessageUtils.getString(R.string.str_voice_bomb_word_success) :
                    MessageUtils.getString(R.string.str_voice_bomb_word_failure);
            addMessage(messageBean.msg_id, SystemMessageItem.createSpyGameMessage(title, msg, Gravity.CENTER));
        }
    }

    @Override
    public void handleSpyGameOver(VoiceMessageBean messageBean) {

        //1 重置游戏,放在2之前，防止2意外出错不能正常结束游戏
        resetGame();

        //2 处理游戏结束消息
        VoiceSpyGameOverBean gameOverBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameOverBean.class);
        String spyWord = "";
        String goodWord = "";
        List<VoiceSpyGameOverBean.RoleInfo> undercover = gameOverBean.roles.undercover;
        List<VoiceSpyGameOverBean.RoleInfo> people = gameOverBean.roles.people;
        if (people != null && people.size() > 0) {
            goodWord = people.get(0).detail;
        }
        if (undercover != null && undercover.size() > 0) {
            VoiceSpyGameOverBean.RoleInfo roleInfo = undercover.get(0);
            if (roleInfo != null) {
                spyWord = roleInfo.detail;
            }

            //卧底为幽灵的情况
            if (TextUtils.isEmpty(spyWord)) {
                if (gameOverBean.words != null) {
                    if (TextUtils.equals(goodWord, gameOverBean.words.first)) {
                        spyWord = gameOverBean.words.second;
                    } else {
                        spyWord = gameOverBean.words.first;
                    }
                }
            }
        }
        String win_type = gameOverBean.win_type;
        int strId = TextUtils.equals(VoiceConstants.SPY_TAG, win_type) ? R.string.str_voice_spy_victory : R.string.str_voice_good_victory;
        addMessage(messageBean.msg_id, SystemMessageItem.createSpyGameMessage(
                MessageUtils.getString(R.string.str_voice_game_over),
                MessageUtils.getString(strId) + "\n"
                        + MessageUtils.getString(R.string.str_voice_good_word) + ":" + goodWord + "\n"
                        + MessageUtils.getString(R.string.str_voice_spy_word) + ":" +
                        (TextUtils.isEmpty(spyWord) ? MessageUtils.getString(R.string.str_voice_word_is_ghost) : spyWord), Gravity.CENTER));
    }

    private void resetGame() {
        VoiceRoomConfig.setIsPlaying(false);//游戏结束
        VoiceRoomConfig.SpyConfig.setIsDefaultSetting(true);//设为默认设置
        VoiceRoomConfig.SpyConfig.clearLenGameConfig();//清除本轮游戏的配置信息

        //重置玩家信息
        List<EnterRoomResult.EnterRoomUserItem> users = getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem user : users) {
            user.prepared = false;//取消准备
            user.isOut = false;//设为"未出局"
        }

        //重置自身信息
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            personalData.setPrepared(false);
            personalData.setOut(false);
        }

        // 小窗模式发一个获取词语,非小窗模式由UI来发
        if (VoiceRoomConfig.isIsSmallWindowStyle()) {
            VoiceSocketHelper.sendSpyGetSystemWords();
        }
    }

    @Override
    public void handleDisconnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.is_disconnected = true;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setOffline(true);
                }
            }
        }
    }

    @Override
    public void handleConnect(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem user = findUserByPosition(position);
        if (user != null) {
            user.is_disconnected = false;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    personalData.setOffline(false);
                }
            }
        }
    }

    @Override
    public void handleAcceptFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            handleFreeStyle(messageBean.msg_id, userItem);
            addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                    MessageUtils.getString(R.string.str_voice_accept_freemode, userItem.name))
            );
        }
    }

    @Override
    public void handleRejectFreeMode(VoiceMessageBean messageBean) {
        int position = messageBean.payload.optInt(VoiceConstants.POSITION);
        EnterRoomResult.EnterRoomUserItem userItem = findUserByPosition(position);
        if (userItem != null) {
            addMessage(messageBean.msg_id, SystemMessageItem.createSystemMessage(
                    MainApplication.getInstance().getResources().getColor(R.color.color_voice_change_title),
                    MessageUtils.getString(R.string.str_voice_reject_freemode, userItem.name))
            );
        }
    }

    @Override
    public void handlePurchasedResult(VoiceMessageBean messageBean) {

    }

    @Override
    public void handleHandleOverCreatorResult(VoiceMessageBean messageBean) {

    }

    @Override
    public void handleUpdateConfig(VoiceMessageBean messageBean) {
        JSONObject jsonObject = messageBean.payload.optJSONObject("config");
        EnterRoomResult.RoomConfig roomConfig = GsonHelper.parseJsonToBean(jsonObject.toString(), EnterRoomResult.RoomConfig.class);
        VoiceRoomConfig.setRoomOwnerId(roomConfig.room_info.owner_id);
        VoiceRoomConfig.setIsSupportDeed(roomConfig.purchase_room);
        if (PositionHelper.isSelf(roomConfig.room_info.owner_id)) {
            //是自己
            VoiceRoomConfig.setIsOwnerRoom(true);
        } else {
            VoiceRoomConfig.setIsOwnerRoom(false);
        }
        if (TextUtils.equals(VoiceConstants.PURCHASED, roomConfig.room_info.owner_type)) {
            VoiceRoomConfig.setIsPurchased(true);
        } else {
            VoiceRoomConfig.setIsPurchased(false);
        }

        //设置房间座位数量
        VoiceRoomConfig.setMaxChairNumber(roomConfig.active_count - 1);

        //设置房间背景相关
        VoiceRoomConfig.setBackground(roomConfig.room_info.background.url);
        VoiceRoomConfig.setBgType(roomConfig.room_info.background.type);
    }

    @Override
    public void handleLobbyInviteGame(VoiceMessageBean messageBean) {
        //大厅: 发起游戏邀请
        LobbyInviteGameBean bean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), LobbyInviteGameBean.class);

        SystemMessageItem message = SystemMessageItem.createLobbyRoomGameMessage
                (bean.game.icon, bean.inviter.name, MessageUtils.getString(R.string.str_lobby_message, bean.game.name));
        EnterRoomResult.EnterRoomUserItem user = findUserById(bean.inviter.id);
        if (user == null) {
            user = new EnterRoomResult.EnterRoomUserItem();
            user.id = bean.inviter.id;
            user.name = bean.inviter.name;
            user.avatar = bean.inviter.avatar;
            user.position = bean.position;
        }
        message.setUser(user);
        message.setInvite_id(bean.game.invite_id);
        message.setGameType(bean.game.type);
        message.setUuid(bean.game.uuid);
        message.setNumber(user.position);

        //这里需要进行过滤处理
        filterInviteMessages(user.id, bean.game.type);
        addMessage(messageBean.msg_id, message);

        LobbyDataStock.getInstance().addMessage(user.id, message);
    }

    @Override
    public void handleLobbyAcceptGame(VoiceMessageBean messageBean) {
        //大厅: 接受游戏邀请
        LobbyAcceptGameBean bean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), LobbyAcceptGameBean.class);
        //找到点击的消息，并设置接受者
        Collection<SystemMessageItem> messageList = getMessageList();
        for (SystemMessageItem item : messageList) {
            if (TextUtils.equals(item.getInvite_id(), bean.invite_id)) {
                item.setAcceptor(bean.acceptor);
            } else {
                //未点击的不处理
            }
        }

        //sender:除此消息外其余删除
        filterSpecialMessage(bean.send_id, bean.invite_id);
//        if (LobbyRoomHelper.isInRoom(bean.send_id)) {
//            filterSpecialMessage(bean.send_id, bean.invite_id);
//        }
        //acceptor:全删
        removeMessageById(bean.acceptor.id);
//        if (LobbyRoomHelper.isInRoom(bean.acceptor.id)) {
//            removeMessageById(bean.acceptor.id);
//        }
    }

    //Socket消息处理 end====================================================//


    //伴奏数据处理 start=====================================================//
    @Override
    public void handleBookSong(VoiceMessageBean messageBean) {
        MusicCompanyDataSource.INSTANCE.getObserver().handleBookSong(messageBean);
    }

    @Override
    public void handleAuthorizeSing(VoiceMessageBean messageBean) {
        //重置所有人为不能唱歌
        List<EnterRoomResult.EnterRoomUserItem> onLineMembers = getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem user : onLineMembers) {
            if (user != null) {
                user.canSing = false;
                user.isSing = false;
            }
        }

        //重置自己为不能唱歌
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            personalData.setCanSing(false);
            personalData.setSing(false);
        }

        //获取新的可以唱歌人的id和位置
        String id = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);

        //设置新的可以唱歌的人
        EnterRoomResult.EnterRoomUserItem singer = VoiceRoomDataSourceManager.getInstance().findUserById(id);
        if (singer != null) {
            singer.canSing = true;
        }

        //如果是自己，设置自己可以唱歌
        if (PositionHelper.isSelf(id)) {
            PersonalDataHelper.getPersonalData().setCanSing(true);
        }

        //ktv数据处理
        MusicCompanyConfig.authorizedID = id;
        MusicCompanyDataSource.INSTANCE.getObserver().handleAuthorizeSing(messageBean);
    }

    @Override
    public void handlePrepareSing(VoiceMessageBean messageBean) {
        MusicCompanyDataSource.INSTANCE.getObserver().handlePrepareSing(messageBean);
    }

    @Override
    public void handleStartSing(VoiceMessageBean messageBean) {

        //重置新的正在唱歌的人
        String singId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        List<EnterRoomResult.EnterRoomUserItem> onLineMembers = getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem user : onLineMembers) {
            if (user != null) {

                //可以唱歌的人，收到唱歌的消息，说明是此人在唱歌
                if (user.canSing) {
                    user.isSing = true;
                } else {
                    user.isSing = false;
                }

                if (PositionHelper.isSelf(user.id)) {
                    PersonalData personalData = PersonalDataHelper.getPersonalData();
                    if (personalData != null) {
                        personalData.setSing(user.isSing);
                    }
                }
            }
        }


        MusicCompanyDataSource.INSTANCE.getObserver().handleStartSing(messageBean);
    }

    @Override
    public void handleStopSing(VoiceMessageBean messageBean) {
        try {
            //设置所有不能唱歌
            List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
            for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
                user.canSing = false;
                user.isSing = false;
            }
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                personalData.setCanSing(false);
                personalData.setSing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MusicCompanyConfig.singerId = "";
        MusicCompanyDataSource.INSTANCE.getObserver().handleStopSing(messageBean);
    }

    @Override
    public void handlePeakSong(VoiceMessageBean messageBean) {
        MusicCompanyDataSource.INSTANCE.getObserver().handlePeakSong(messageBean);
    }

    @Override
    public void handleRemoveSong(VoiceMessageBean messageBean) {

        //被移除的歌曲的点歌的人
//        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
//        EnterRoomResult.EnterRoomUserItem user = findUserById(userId);
//        if (user != null) {
//            user.canSing = false;
//        }
//        if (PositionHelper.isSelf(userId)) {
//            PersonalData personalData = PersonalDataHelper.getPersonalData();
//            if (personalData != null) {
//                personalData.setCanSing(false);
//            }
//        }

        MusicCompanyDataSource.INSTANCE.getObserver().handleRemoveSong(messageBean);
    }

    //伴奏数据处理 end======================================================//

    private boolean hasAdded(String id) {
        List<VoiceAddFriendBean> addFriendIdList = getAddFriendIdList();
        if (addFriendIdList != null) {
            //过滤掉相同的请求
            for (VoiceAddFriendBean bean : addFriendIdList) {
                if (TextUtils.equals(bean.id, id)) {
                    return true;
                }
            }
        }

        return false;
    }

    //是否需要转让房主 自己是房主并且房间人数大于1才需要转让
    public boolean needHandOverMaster() {
        return PositionHelper.isMaster() && getOnLineUsers().size() > 1;
    }

    //获取送礼的对象列表
    public List<GameMember.GameMemberData> getGiftUserList() {
        List<GameMember.GameMemberData> memberDataList = new ArrayList<>();
        List<EnterRoomResult.EnterRoomUserItem> onLineMembers = getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem member : onLineMembers) {
            if (member == null) {
                continue;
            }
            memberDataList.add(TransferClassHelper.EnterRoomUser2GameMemberData(member));
        }
        return memberDataList;
    }

    //进行删除处理
    //大厅的邀请消息，同一个人发送的就删除之前的
    private void filterInviteMessages(String userId, String gameType) {
        filterSameLobbyGame(userId, gameType);
    }

    //销毁处理====================================================//
    @Override
    public void destroy() {
        dataSource.destroy();
        instance = null;
    }
    //销毁处理====================================================//
}
