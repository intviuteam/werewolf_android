package orangelab.project.voice.manager

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.support.v8.renderscript.Allocation
import android.support.v8.renderscript.Element
import android.support.v8.renderscript.RenderScript
import android.support.v8.renderscript.ScriptIntrinsicBlur
import android.text.TextUtils
import android.widget.ImageView
import com.androidtoolkit.AndroidToolKit
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.UIActuator
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.voice.privateroom.PrivateRoomContext
import orangelab.project.voice.privateroom.PrivateRoomContextImpl
import com.toolkit.DestroyableManager
import com.toolkit.action.Action
import java.lang.IllegalStateException
import kotlin.collections.ArrayList


/**
 *game_werewolf
 *2018/3/19 上午11:15
 *Mystery
 */
object VoiceRoomCommander {

    object ActivityManager {
        private var mPrivateRoomActivities: MutableList<Activity> = ArrayList()

        fun RegisterActivity(activity: Activity) {
            if (mPrivateRoomActivities.contains(activity)) {
                return
            } else {
                mPrivateRoomActivities.add(activity)
            }
        }

        fun UnRegisterActivity(activity: Activity) {
            mPrivateRoomActivities.remove(activity)
        }

        fun ReleaseActivities() {
            mPrivateRoomActivities.forEach {
                it.finish()
            }
            mPrivateRoomActivities.clear()
        }
    }

    object ContextManager {

        private var mPrivateRoomContext: PrivateRoomContext? = null

        private fun CreatePrivateRoomContext(socketToken: String?, enterRoomResult: EnterRoomResult): PrivateRoomContext {
            return PrivateRoomContextImpl(socketToken, enterRoomResult)
        }

        private fun SavePrivateRoomContext(privateRoomContext: PrivateRoomContext) {
            mPrivateRoomContext = privateRoomContext
            DestroyableManager.getInstance().addDestroyAble(PrivateRoomContext::class.java.name, privateRoomContext)
        }

        private fun HasInitPrivateRoomContext(): Boolean {
            return DestroyableManager.getInstance().contains(PrivateRoomContext::class.java.name)
        }

        fun GetPrivateRoomContext(): PrivateRoomContext? {
            if (mPrivateRoomContext == null) {
                return null
            }
            return mPrivateRoomContext
        }

        fun InitPrivateRoomContext(socketToken: String?, enterRoomResult: EnterRoomResult) {
            if (HasInitPrivateRoomContext()) {
                throw IllegalStateException("You can not init private context twice")
            } else {
                SavePrivateRoomContext(CreatePrivateRoomContext(socketToken, enterRoomResult))
            }
        }

        private fun DestroyPrivateRoomContext() {
            DestroyableManager.getInstance().removeAndDestroyDestroyAble(PrivateRoomContext::class.java.name)
        }

        fun DestroyPrivateRoomContext(complete: Runnable?) {

            /**
             * 清理完毕后需要将Context置空确保 Context已经消失
             */
            var context = GetPrivateRoomContext()
            mPrivateRoomContext = null
            if (context == null) {
                DestroyPrivateRoomContext()
            } else {
                context.destroy(complete)
                DestroyableManager.getInstance().removeDestroyAble(PrivateRoomContext::class.java.name)
            }
        }

    }

    object BackGroundManager {


        val mBlurRadio = 2f

        fun BlurBitmap(context: Context, bitmap: Bitmap, radius: Float): Bitmap? {
            var resultBitmap: Bitmap? = bitmap
            try {
                val renderScript = RenderScript.create(context)

                val input = Allocation.createFromBitmap(renderScript, resultBitmap)
                val output = Allocation.createTyped(renderScript, input.type)

                val scriptIntrinsicBlur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))

                scriptIntrinsicBlur.setInput(input)

                scriptIntrinsicBlur.setRadius(radius)

                scriptIntrinsicBlur.forEach(output)

                output.copyTo(resultBitmap)

                renderScript.destroy()
            } catch (e: Exception) {
                e.printStackTrace()
                /**
                 * 如果使用官方的blur方式行不通则采用Java计算方式
                 */
                try {
                    resultBitmap = AndroidToolKit.BlurBitmapStackWay(bitmap, radius.toInt(), false)
                } catch (e: Exception) {
                    e.printStackTrace()
                    resultBitmap = null
                }
            }
            return resultBitmap
        }

        fun BlurBackGround(resouceId: Int, view: ImageView?, handler: Handler?, width: Int, height: Int) {
            if (view == null) {
                return
            }
            if (resouceId == -1) {
                return
            }
            var curHandler: Handler? = null;
            if (handler == null) {
                curHandler = UIActuator.getHandler()
            }
            PicassoUtils.getImage(view.context, resouceId, width, height, Action {
                var result: Bitmap? = null
                if (it != null) {
                    result = BlurBitmap(view.context, it, mBlurRadio)
                }
                if (result != null) {
                    curHandler!!.post({
                        view.setImageBitmap(result)
                    })
                }
            })
        }

        fun BlurBackGround(url: String, view: ImageView?, handler: Handler?, width: Int, height: Int) {
            if (view == null) {
                return
            }
            if (TextUtils.isEmpty(url)) {
                return
            }
            var curHandler: Handler? = null;
            if (handler == null) {
                curHandler = UIActuator.getHandler()
            }
            PicassoUtils.getImage(view.context, url, width, height, Action {
                var result: Bitmap? = null
                if (it != null) {
                    result = BlurBitmap(view.context, it, mBlurRadio)
                }
                if (result != null) {
                    curHandler!!.post({
                        view.setImageBitmap(result)
                    })
                }
            })
        }

        fun BlurBackGround(resouceId: Int, view: ImageView?, handler: Handler?) {
            if (view == null) {
                return
            }
            if (resouceId == -1) {
                return
            }
            var curHandler: Handler? = null;
            if (handler == null) {
                curHandler = UIActuator.getHandler()
            }
            PicassoUtils.getImage(view.context, resouceId, Action {
                var result: Bitmap? = null
                if (it != null) {
                    result = BlurBitmap(view.context, it, mBlurRadio)
                }
                if (result != null) {
                    curHandler!!.post({
                        view.setImageBitmap(result)
                    })
                }
            })
        }

        fun BlurBackGround(url: String, view: ImageView?, handler: Handler?) {
            if (view == null) {
                return
            }
            if (TextUtils.isEmpty(url)) {
                return
            }
            var curHandler: Handler? = null
            if (handler == null) {
                curHandler = UIActuator.getHandler()
            } else {
                curHandler = handler
            }
            PicassoUtils.getImage(view.context, url, Action {
                var result: Bitmap? = null

                if (it != null) {
                    result = BlurBitmap(view.context, it, mBlurRadio)
                }
                if (result != null) {
                    curHandler!!.post({
                        view.setImageBitmap(result)
                    })
                }
            })
        }
    }
}