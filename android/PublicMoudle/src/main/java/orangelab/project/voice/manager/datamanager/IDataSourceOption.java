package orangelab.project.voice.manager.datamanager;

import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import java.util.Collection;
import java.util.List;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.model.VoiceRoomAudioConfigBean;

/**
 * Created by lloydfinch on 02/01/2018.
 */

public interface IDataSourceOption extends Keepable, Destroyable {

    //enter时的初始化
    void init(EnterRoomResult result);

    //设置是否同步
    void setSync(boolean isSync);

    //添加一个用户
    void addUser(EnterRoomResult.EnterRoomUserItem user);

    //添加麦上用户
    void addOnSeatUser(EnterRoomResult.EnterRoomUserItem user);

    //添加麦下用户
    void addDownSeatUser(EnterRoomResult.EnterRoomUserItem user);

    //移除一个用户
    void removeUser(EnterRoomResult.EnterRoomUserItem user);

    //移除麦上用户
    void removeOnSeatUser(EnterRoomResult.EnterRoomUserItem user);

    //移除麦下用户
    void removeDownSeatUser(EnterRoomResult.EnterRoomUserItem user);

    //清除所有用户
    void clearOnLineUsers();

    //获取在线人列表
    List<EnterRoomResult.EnterRoomUserItem> getOnLineUsers();

    //获取麦上人列表
    List<EnterRoomResult.EnterRoomUserItem> getOnSeatUsers();

    //获取麦下人列表
    List<EnterRoomResult.EnterRoomUserItem> getDownSeatUsers();

    //设置加锁位置列表
    void setLockedSeatList(List<Integer> lockedSeatList);

    //获取加锁位置列表
    List<Integer> getLockedSeatList();

    //清空加锁位置列表
    void clearLockedSeatList();

    //添加一个加锁座位
    void addLockedSeat(Integer position);

    //移除一个加锁座位
    void removeLockedSeat(Integer position);

    //根据位置找人
    EnterRoomResult.EnterRoomUserItem findUserByPosition(int position);

    //根据id找人
    EnterRoomResult.EnterRoomUserItem findUserById(String id);

    //添加消息
    void addMessage(Long key, SystemMessageItem msg);

    //获取系统消息列表
    Collection<SystemMessageItem> getMessageList();

    //获取最后一条消息
    SystemMessageItem getLatestMessage(Long key);

    //添加一条"加好友"的消息
    void addFriendDialog(VoiceAddFriendBean bean);

    //删除一条"加好友"的消息
    void removeAddFriend(String id);

    //将回一条"加好友"消息标记为已解决
    void markAddFriendHandled(String id);

    //获取最近的加好友消息
    VoiceAddFriendBean getLatestAddFriendBean();

    //获取加好友的人的id列表
    List<VoiceAddFriendBean> getAddFriendIdList();

    //清除加好友的对话框列表
    void clearAddFriendDialogList();

    //设置点赞数(仅断线重连设置)
    void setLikeCount(int likeCount);

    //获取点赞数(仅断线重连获取)
    int getLikeCount();

    //获取音频socket配置信息
    VoiceRoomAudioConfigBean getAudioConfig();

    //清除音频配置信息(仅仅为了节省内存)
    void clearAudioConfig();

    //清除同一个人的游戏邀请消息
    void filterSameLobbyGame(String userId, String gameType);

    //处理离开房间的消息
    void filterLeaveType(String userId, String leaveType);

    //留下特定玩家特定类型的消息，其余删除
    void filterSpecialMessage(String userId, String inviteId);

    //删除某个玩家的所有消息
    void removeMessageById(String userId);

    //根据invite_id删除消息
    void removeMessageByInviteId(String inviteId);

}
