package orangelab.project.voice.manager.socketmanager;

import android.text.TextUtils;
import android.view.MotionEvent;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;

import orangelab.project.MainApplication;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.voice.event.VoiceEvent;

import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 30/12/2017.
 * //语音房游戏Socket管理器
 */

public class VoiceRoomGameSocketManager implements Keepable, Destroyable {

    private static final String TAG = "VoiceRoomGameSocketManager";
    private static VoiceRoomGameSocketManager instance;

    private boolean isConnected = true;//默认应该是true，因为进入房间前提是已经连接

    public static VoiceRoomGameSocketManager getInstance() {
        if (instance == null) {
            synchronized (VoiceRoomGameSocketManager.class) {
                if (instance == null) {
                    instance = new VoiceRoomGameSocketManager();
                }
            }
        }
        return instance;
    }

    public void Start() {
        initGameSocketConnectedStateListener();
    }

    private VoiceRoomGameSocketManager() {
    }

    //关闭游戏socket,彻底退出房间才使用
    public void ShutDown(String token) {
        MainApplication.getInstance().getSocketEngineHelper().releaseEngine(token);
        isConnected = false;
        destroy();
    }

    //是否连接
    public boolean isConnected() {
        return isConnected;
    }

    //游戏socket连接状态监听器
    private void initGameSocketConnectedStateListener() {
        RxToolKit.Build(this, SocketEvent.SocketConnectStateEvent.class)
                .actionFilter(value -> TextUtils.equals(value.getIntent(), SocketEngine.AUDIO))
                .action(socketConnectStateEvent -> {
                    switch (socketConnectStateEvent.getAction()) {
                        case SocketEvent.SOCKET_CONNECT:
                            if (!isConnected) {
                                //重新连接
                                RxToolKit.Emit(new VoiceEvent.GameSocketReconnectEvent(true));
                            }
                            PLog.e(TAG, "game socket connect");
                            isConnected = true;
                            break;
                        case SocketEvent.SOCKET_DISCONNECT:
                            if (isConnected) {
                                //断开连接
                                RxToolKit.Emit(new VoiceEvent.GameSocketReconnectEvent(false));
                            }
                            //发送一个停止说话的消息，保险处理，放在外面
                            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                            PLog.e(TAG, "game socket disconnect");
                            //发送一个停止说话的消息，保险处理，放在外面
                            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                            isConnected = false;
                            break;
                    }
                }).register();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        instance = null;
    }
}
