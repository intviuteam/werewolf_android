package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * Created by hwx on 07/08/2018.
 */

public class AdminListParserMode implements Keepable {
    public List<VoiceAdminListMode> users;

    @Override
    public String toString() {
        return "VoiceAdminListMode{" +
                "users=" + users +
                '}';
    }
}
