package orangelab.project.voice.privateroom

import android.text.TextUtils
import com.R
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.UIActuator
import com.datasource.GlobalUserState
import orangelab.project.common.*
import orangelab.project.common.engine.GlobalSocketConstant
import orangelab.project.common.engine.SocketEngine
import orangelab.project.common.tool.GsonTurnToolKit
import orangelab.project.voice.privateroom.model.PrivateRoomBeIntviu

/**
 *game_werewolf
 *2018/5/23 下午2:32
 *Mystery
 */
class PrivateRoomMultiIntviuGlobalServiceHandler : BaseGlobalServiceHandler {

    private var mWindowManager: GlobalFloatWindowManager? = null

    constructor(globalService: IGlobalService?, windowManager: GlobalFloatWindowManager) : super(globalService) {
        mWindowManager = windowManager
    }

    override fun isCareType(type: String?): Boolean {
        return TextUtils.equals(type, GlobalSocketConstant.MULTI_PRI_ROOM) || TextUtils.equals(type, GlobalSocketConstant.MULTI_PRI_ROOM_SERVER)
    }

    override fun handleMessage(type: String?, payload: String?) {
        if (TextUtils.equals(type, GlobalSocketConstant.INVITED_SUCCESS)) {
            /**
             * 私密房多人邀请成功
             */
            UIActuator.post { ToastToolKit.showShort(R.string.mini_game_intviu_success) }
            return
        }

        if (TextUtils.equals(type, GlobalSocketConstant.BE_INVITED)) {
            /**
             * 收到了邀请
             *
             * 展示私密房邀请条件
             *
             * 1。出了语音房之外的所有房间
             * 2。房间之外的所有页面
             */

            if (GlobalServiceUtils.canShowPrivateRoomInvite()) {
                /**
                 * 需要展示邀请
                 */
                UIActuator.post {
                    val beInvite = GsonTurnToolKit.turn(payload, PrivateRoomBeIntviu::class.java)
                    val invite = PrivateRoomInviteFloatWindowHelper.PrivateRoomInvite()
                    invite.roomId = beInvite.room_id
                    invite.roomPassword = beInvite.room_pw
                    invite.roomType = beInvite.room_type
                    invite.userIcon = beInvite.invite_user.avatar
                    invite.userId = beInvite.invite_user.id
                    invite.userName = beInvite.invite_user.name
                    mWindowManager?.show(invite)
                }

            } else {
                /**
                 * 不可展示邀请
                 */


            }

            return
        }
    }

    override fun destroy() {
        super.destroy()
        mWindowManager = null
    }

}