package orangelab.project.voice.adapter;

import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.effect.msgdecor.MsgDecorToolKit;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.view.VipNameView;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.ds.LimitList;
import orangelab.project.voice.manager.GameScoreReflection;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;

import java.util.Collection;
import java.util.List;

/**
 * Created by lloydfinch on 29/08/2017.
 */

public class MessageAdapter extends BaseAdapter {
    private static final int PADDING = ScreenUtils.dip2px(4);

    private Context mContext;
    private ListView listMsg;
    private LimitList<SystemMessageItem> messageList = new LimitList<>();
    private boolean isScrollToBottom = true;
    private OnNameClickListener onNameClickListener;

    public MessageAdapter(Context mContext, ListView listMsg, List<SystemMessageItem> messageList) {
        this.mContext = mContext;
        this.listMsg = listMsg;
        this.messageList.clear();
        this.messageList.addAll(messageList);

        initListScrollListener();
    }

    private void initListScrollListener() {
        listMsg.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        isScrollToBottom = true;
                    } else {
                        isScrollToBottom = false;
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    public void setOnNameClickListener(OnNameClickListener onNameClickListener) {
        this.onNameClickListener = onNameClickListener;
    }

    //刷新消息列表
    public void refreshMessageList(Long key) {
        SystemMessageItem latestMessage = VoiceRoomDataSourceManager.getInstance().getLatestMessage(key);
        if (latestMessage != null) {
            messageList.add(latestMessage);
            notifyDataSetChanged();

            //已经在底部就刷新底部
            if (isScrollToBottom) {
                listMsg.setSelection(getCount() - 1);
            }

            //自己发的消息就滚动到底部
            EnterRoomResult.EnterRoomUserItem user = latestMessage.getUser();
            String id = user == null ? "" : user.id;
            String myId = GlobalUserState.getGlobalState().getUserId();
            if (!TextUtils.isEmpty(myId) && TextUtils.equals(myId, id)) {
                listMsg.setSelection(getCount() - 1);
            }
        }
    }

    //小窗回来时复现之前的消息
    public void restoreMessage(Collection<SystemMessageItem> cMessageList) {
        messageList.clear();
        messageList.addAll(cMessageList);
        notifyDataSetChanged();
        listMsg.setSelection(getCount() - 1);
    }

    @Override
    public int getCount() {
        return messageList == null ? 0 : messageList.size();
    }

    @Override
    public Object getItem(int i) {
        return messageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View container, ViewGroup viewGroup) {

        View contentView = container == null ? LayoutInflater.from(mContext).inflate(R.layout.item_msg, null) : container;

        ViewHolder holder = (ViewHolder) contentView.getTag();
        if (holder == null) {
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        }

        SystemMessageItem message = messageList.get(i);
        switch (message.getType()) {
            case SystemMessageItem.TYPE_SYSTEM:
                holder.llMemberContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llSystemContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.VISIBLE);
                holder.tvSystemMsg.setText(message.getMsg());
                holder.tvSystemMsg.setTextColor(message.getColor());
                break;
            case SystemMessageItem.TYPE_MEMBER:
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.VISIBLE);
                holder.llMsgContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.VISIBLE);
                holder.tvName.setText(message.getName());
                holder.tvMsg.setText(message.getMsg());
                holder.tvMsg.setPadding(PADDING, PADDING, PADDING, PADDING);
                holder.tvMsg.setBackgroundResource(R.drawable.bg_msg);
                holder.tvMsg.setTextColor(message.getColor());
                holder.ivPosition.setImageResource(findIdByPosition(message.getNumber() - 1));
                boolean isGift = false;
                if (!TextUtils.isEmpty(message.getExhibitionType())) {
                    if (EffectsManager.SupportThisGiftType(message.getExhibitionType())) {
                        holder.ivGift.setVisibility(View.VISIBLE);
                        EffectsManager.FillGiftImageView(message.getExhibitionType(), holder.ivGift);
                        isGift = true;
                    } else {
                        holder.ivGift.setVisibility(View.INVISIBLE);
                        isGift = true;
                    }
                } else {
                    holder.ivGift.setVisibility(View.GONE);
                    isGift = false;
                }
                if (!isGift) {
                    EnterRoomResult.EnterRoomUserItem userItem = message.getUser();
                    if (userItem != null) {
                        holder.tvName.handleVipInfo(userItem.vipInfo);
                        if (userItem != null && userItem.message_box != null && !TextUtils.isEmpty(userItem.message_box.message_box_type)) {
                            MsgDecorManiFest.MsgDecorManiFestItem item = EffectsManager.GetMsgDecorItem(userItem.message_box.message_box_type);
                            if (item != null) {
                                MsgDecorToolKit.setNinePatchForViewBackGround(holder.tvMsg, holder.tvMsg, item.animate);
                            }
                        }
                    }
                }
                break;
            case SystemMessageItem.TYPE_SMALL_GAME:
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llMsgContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.VISIBLE);
                holder.llScoreContainer.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.tvName.setText(message.getName());
                holder.ivPosition.setImageResource(findIdByPosition(message.getNumber() - 1));
                holder.ivScore.setImageResource(GameScoreReflection.scoreToImageId(message.getScore()));
                break;
            case SystemMessageItem.TYPE_SPY_MSG:
                holder.llMsgContainer.setVisibility(View.GONE);
                holder.llMemberContainer.setVisibility(View.GONE);
                holder.llScoreContainer.setVisibility(View.GONE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.llSystemContainer.setVisibility(View.GONE);
                holder.llSpyMsgContainer.setVisibility(View.VISIBLE);

                holder.tvSpyMsgTitle.setText(message.getName());
                holder.tvSpyMsg.setText(message.getMsg());
                holder.tvSpyMsg.setGravity(message.getGravity());
                break;
            default:
                break;
        }

        if (onNameClickListener != null) {
            EnterRoomResult.EnterRoomUserItem user = message.getUser();
            final String id = user == null ? "" : user.id;
            holder.llNameContainer.setOnClickListener(v -> onNameClickListener.onNameClick(id));
        }

        return contentView;
    }

    private int findIdByPosition(int position) {
        if (position == VoiceConstants.MASTER_POSITION) {
            return R.mipmap.voice_tag_master;
        }

        if (position <= VoiceRoomConfig.getMaxChairNumber()) {
            try {
                return R.mipmap.class.getDeclaredField("voice_tag_" + position).getInt(null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        return R.mipmap.voice_tag_other;
    }

    public void destroy() {
        messageList.clear();
        messageList = null;
    }

    static class ViewHolder {
        View llMemberContainer;
        View llNameContainer;
        ImageView ivPosition;
        VipNameView tvName;

        View llMsgContainer;
        TextView tvMsg;
        ImageView ivGift;

        View llSystemContainer;
        TextView tvSystemMsg;

        View llScoreContainer;
        ImageView ivScore;

        View llSpyMsgContainer;
        TextView tvSpyMsgTitle;
        TextView tvSpyMsg;

        public ViewHolder(View contentView) {
            llMemberContainer = contentView.findViewById(R.id.ll_member_tag);
            llNameContainer = contentView.findViewById(R.id.ll_name_container);
            ivPosition = (ImageView) contentView.findViewById(R.id.iv_item_position);
            tvName = (VipNameView) contentView.findViewById(R.id.tv_item_name);

            llMsgContainer = contentView.findViewById(R.id.ll_msg_container);
            tvMsg = (TextView) contentView.findViewById(R.id.tv_item_msg);
            ivGift = (ImageView) contentView.findViewById(R.id.iv_item_gift);

            llSystemContainer = contentView.findViewById(R.id.ll_system_tag);
            tvSystemMsg = (TextView) contentView.findViewById(R.id.tv_item_system_msg);

            llScoreContainer = contentView.findViewById(R.id.ll_score_tag);
            ivScore = (ImageView) contentView.findViewById(R.id.iv_item_score);

            llSpyMsgContainer = contentView.findViewById(R.id.ll_spy_member_tag);
            tvSpyMsgTitle = (TextView) contentView.findViewById(R.id.tv_spy_item_name);
            tvSpyMsg = (TextView) contentView.findViewById(R.id.tv_spy_item_msg);

            tvMsg.setOnLongClickListener(v -> {
                ClipboardManager cmb = (ClipboardManager) MainApplication.getInstance().getSystemService(Context.CLIPBOARD_SERVICE);
                cmb.setText(tvMsg.getText().toString().trim());
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_copy));

                return true;
            });
        }
    }

    public interface OnNameClickListener {
        void onNameClick(String id);
    }


}
