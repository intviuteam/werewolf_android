package orangelab.project.voice.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.voice.manager.VoiceEmotionManager;
import com.androidtoolkit.PicassoUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by lloydfinch on 21/10/2017.
 */

public class VoiceEmotionAdapter extends RecyclerView.Adapter<VoiceEmotionAdapter.MyViewHolder> implements Destroyable {

    private List<EmotionManiFest.Emotion> emotionList = new ArrayList<>();
    private Context mContext;

    private OnItemClickedListener onItemClickedListener;

    public VoiceEmotionAdapter(Context context) {
        this.mContext = context;
    }

    public VoiceEmotionAdapter(List<EmotionManiFest.Emotion> emotionList) {
        this.emotionList.addAll(emotionList);
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public void updateEmotionList(List<EmotionManiFest.Emotion> emotionList) {
        this.emotionList.clear();
        this.emotionList.addAll(emotionList);
        notifyDataSetChanged();
    }

    @Override
    public VoiceEmotionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_voice_emotion, null);
        return new VoiceEmotionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VoiceEmotionAdapter.MyViewHolder holder, int position) {
        EmotionManiFest.Emotion emotion = emotionList.get(position);
        if (emotion != null) {
            if(Locale.getDefault().toString().equals("zh_CN")) {
                holder.tvEmotionName.setText(emotion.mark.j);
            } else {
                holder.tvEmotionName.setText(emotion.mark.f);
            }
            int resId = ApkResourceManager.GetApkImageResource(emotion.type);
            if (resId > 0) {
                holder.ivEmotion.setImageResource(resId);
            } else {
                InputStream inputStream = EffectsManager.GetEmotionInputStream(emotion.thumbnail);
                if (inputStream != null) {
                    holder.ivEmotion.setImageBitmap(BitmapFactory.decodeStream(inputStream));
                } else {
                    PicassoUtils.loadImage(mContext, emotion.thumbnail, holder.ivEmotion);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return emotionList.size();
    }

    @Override
    public void destroy() {
        emotionList.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivEmotion;
        TextView tvEmotionName;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ivEmotion = (ImageView) itemView.findViewById(R.id.iv_voice_emotion);
            tvEmotionName = (TextView) itemView.findViewById(R.id.tv_voice_emotion_name);
            this.itemView.setOnClickListener((v) -> {
                if (onItemClickedListener != null) {
                    EmotionManiFest.Emotion emotion = emotionList.get(getPosition());
                    //防止表情还没拉过来就点击的问题
                    if (emotion != null) {
                        onItemClickedListener.onItemClicked(emotion);
                    }
                }
            });
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(EmotionManiFest.Emotion emotion);
    }
}
