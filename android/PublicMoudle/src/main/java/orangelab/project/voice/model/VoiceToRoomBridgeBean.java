package orangelab.project.voice.model;

import com.androidtoolkit.transport.Transportable;

import orangelab.project.common.model.EnterRoomResult;

import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 07/03/2018.
 * //用于启动VoiceRoomActivity传入的数据实体
 */

public class VoiceToRoomBridgeBean implements Keepable, Transportable {
    public EnterRoomResult result;
    public boolean isFromSW;//是否从小窗进入
    public boolean isNeedHandleOverMaster;//是否需要转让房主
    public String enginToken;

    public VoiceToRoomBridgeBean(EnterRoomResult result, boolean isFromSW, String enginToken) {
        this.result = result;
        this.isFromSW = isFromSW;
        this.enginToken = enginToken;
        isNeedHandleOverMaster = false;
    }

    public VoiceToRoomBridgeBean(EnterRoomResult result, boolean isFromSW, boolean isNeedHandleOverMaster, String enginToken) {
        this.result = result;
        this.isFromSW = isFromSW;
        this.isNeedHandleOverMaster = isNeedHandleOverMaster;
        this.enginToken = enginToken;
    }
}
