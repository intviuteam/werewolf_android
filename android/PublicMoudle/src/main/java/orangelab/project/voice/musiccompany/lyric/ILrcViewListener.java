package orangelab.project.voice.musiccompany.lyric;


import com.toolkit.action.Keepable;

import orangelab.project.voice.musiccompany.lyric.impl.LrcRow;

/**
 * 歌词拖动时候的监听类
 */
public interface ILrcViewListener extends Keepable {
    /**
     * 当歌词被用户上下拖动的时候回调该方法
     */
    void onLrcSeeked(int newPosition, LrcRow row);
}
