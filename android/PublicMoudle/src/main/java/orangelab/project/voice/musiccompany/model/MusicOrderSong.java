package orangelab.project.voice.musiccompany.model;

import android.app.ActivityManager;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * 排麦歌曲实体
 */
public class MusicOrderSong implements Keepable, Serializable {

    public String id;                       //歌曲id
    public int position;                    //歌曲位置
    public String headImg = "";             //歌曲专辑封面
    public String songName = "";            //歌曲名字
    public String author = "";              //歌曲原唱
    public String order = "";               //点歌人名字
    public int orderPosition;               //点歌人的位置
    public String orderId = "";             //点歌人的id
    public String lyricUrl = "";
    public String playUrl = "";
    public String orderImg = "";            //点歌人的头像

    public MusicOrderSong() {
    }

    public MusicOrderSong(String headImg, String songName, String author) {
        this.headImg = headImg;
        this.songName = songName;
        this.author = author;

        ActivityManager.isUserAMonkey();
    }

    @Override
    public String toString() {
        return "MusicOrderSong{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", headImg='" + headImg + '\'' +
                ", songName='" + songName + '\'' +
                ", author='" + author + '\'' +
                ", order='" + order + '\'' +
                ", orderPosition=" + orderPosition +
                ", orderId='" + orderId + '\'' +
                ", lyricUrl='" + lyricUrl + '\'' +
                ", playUrl='" + playUrl + '\'' +
                ", orderImg='" + orderImg + '\'' +
                '}';
    }
}
