package orangelab.project.voice.privateroom.view

import android.os.Looper
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.text.TextUtils
import android.view.View
import android.widget.CompoundButton
import com.androidtoolkit.PLog
import com.androidtoolkit.view.SwitchButton
import com.R
import com.androidtoolkit.ToastToolKit
import com.toolkit.ToolKit
import orangelab.project.common.tool.DialogManager
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.common.utils.SafeHandler
import orangelab.project.voice.privateroom.PrivateRoomController
import orangelab.project.voice.privateroom.PrivateRoomObserver
import orangelab.project.voice.privateroom.holder.ChatMsgItemViewBinder
import orangelab.project.voice.privateroom.holder.GiftMsgItemViewBinder
import orangelab.project.voice.privateroom.holder.SystemMsgItemViewBinder
import orangelab.project.voice.privateroom.holder.TempPictureMsgItemViewBinder
import orangelab.project.voice.privateroom.model.ChatMessageItem
import orangelab.project.voice.privateroom.model.GiftMessageItem
import orangelab.project.voice.privateroom.model.SystemMessageItem
import orangelab.project.voice.privateroom.model.TempPictureMessageItem
import orangelab.project.voice.view.FlexibleRecycleView
import com.toolkit.action.Destroyable
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter
import orangelab.project.common.engine.RoomSocketEngine
import orangelab.project.common.engine.RoomSocketEngineHelper
import java.util.*

/**
 *game_werewolf
 *2018/3/21 下午5:45
 *Mystery
 */
class PrivateRoomMessageView : Destroyable, CompoundButton.OnCheckedChangeListener, FlexibleRecycleView.FlexibleRecycleViewObserver {

    class RestoreData {
        var isCheck = false
    }

    private val mHideDelay = 5000L

    private lateinit var mPrivateRoomRecycleView: FlexibleRecycleView
    private lateinit var mPrivateRoomRecycleViewContainer: View
    private var mPrivateRoomMultiTypeAdapter: MultiTypeAdapter

    private var mMessageHideSwitch: SwitchButton

    private var mPrivateRoomObserver: PrivateRoomObserver? = null
    private var mIsCheck = false
    private var mIsTouched = false

    private var mTempPictureBinder: TempPictureMsgItemViewBinder? = null

    private var mHideRunnable: Runnable = Runnable {
        if (mPrivateRoomRecycleView.visibility == View.VISIBLE) {
            mPrivateRoomRecycleView.visibility = View.INVISIBLE
        }
    }

    private var mMoveToRunnable: Runnable = Runnable {
        moveToBottom()
    }
    private var mHandler = SafeHandler(Looper.getMainLooper())

    constructor(viewRoot: View, dialogManager: DialogManager, chatList: List<Any>, privateRoomObserver: PrivateRoomObserver) {

        mPrivateRoomObserver = privateRoomObserver
        mPrivateRoomRecycleView = viewRoot.findViewById<FlexibleRecycleView>(R.id.list_message) as FlexibleRecycleView
        mPrivateRoomRecycleViewContainer = viewRoot.findViewById(R.id.list_message_container)
        mPrivateRoomRecycleViewContainer.setOnClickListener({ v ->
            if (mPrivateRoomRecycleView.visibility != View.VISIBLE) {
                mPrivateRoomRecycleView.visibility = View.VISIBLE
                startHideCheck()
                startMoveCheck()
            }
        })

        mMessageHideSwitch = viewRoot.findViewById<SwitchButton>(R.id.message_hide_switch) as SwitchButton

        mPrivateRoomMultiTypeAdapter = MultiTypeAdapter()
        mPrivateRoomMultiTypeAdapter.register(ChatMessageItem::class.java, ChatMsgItemViewBinder())
        mPrivateRoomMultiTypeAdapter.register(GiftMessageItem::class.java, GiftMsgItemViewBinder())
        mPrivateRoomMultiTypeAdapter.register(SystemMessageItem::class.java, SystemMsgItemViewBinder())

        mTempPictureBinder = TempPictureMsgItemViewBinder(viewRoot.context, PrivateRoomController.TEMP_PICTURE_DIALOG, dialogManager)
        mPrivateRoomMultiTypeAdapter.register(TempPictureMessageItem::class.java, mTempPictureBinder!!)

        mPrivateRoomMultiTypeAdapter.items = chatList

        mPrivateRoomRecycleView.adapter = mPrivateRoomMultiTypeAdapter
        mPrivateRoomRecycleView.layoutManager = WrapContentLinearLayoutManager(mPrivateRoomRecycleView.context)
        mPrivateRoomRecycleView.setObserver(this)
        mMessageHideSwitch.setOnCheckedChangeListener(this)
    }

    private fun startHideCheck() {
        if (mIsCheck) {
            mHandler.postDelaySafely(mHideRunnable, mHideDelay)
        }
    }

    private fun showMessage() {
        mPrivateRoomRecycleView.visibility = View.VISIBLE
    }

    private fun removeHideCheck() {
        mHandler.removeCallbacks(mHideRunnable)
    }

    private fun removeMoveCheck() {
        mHandler.removeCallbacks(mMoveToRunnable)
    }

    private fun startMoveCheck() {
        mHandler.postDelaySafely(mMoveToRunnable, mHideDelay)
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

        mIsCheck = isChecked

        if (isChecked) {
            /**
             * 打开了隐藏
             */
            startHideCheck()

        } else {
            /**
             * 关闭了隐藏
             */
            showMessage()
            removeHideCheck()
        }
    }

    fun refresh() {
        showMessage()
        removeHideCheck()
        mPrivateRoomMultiTypeAdapter.notifyDataSetChanged()
        if (mPrivateRoomRecycleView.isSlideToBottom) {
            moveToBottom()
        }
        startHideCheck()
    }

    fun moveToBottom() {
        mPrivateRoomRecycleView.scrollToPosition(mPrivateRoomMultiTypeAdapter.itemCount - 1)
    }

    override fun onEndTouch() {
        printlnLog("onEndTouch")
        mIsTouched = false
        if (mPrivateRoomRecycleView.visibility != View.VISIBLE) {
            /**
             * 已经消失了不做处理
             */
        } else {
            startMoveCheck()
            startHideCheck()
        }
    }

    override fun onKeyBoardDismiss() {
        moveToBottom()
    }

    override fun onKeyBoardShow() {

    }

    override fun onTouched() {
        printlnLog("onTouched")
        mIsTouched = true
        if (mPrivateRoomRecycleView.visibility != View.VISIBLE) {
            /**
             * 已经消失了不做处理
             */
            mPrivateRoomRecycleView.visibility = View.VISIBLE
            moveToBottom()
            startHideCheck()
        } else {
            removeMoveCheck()
            removeHideCheck()
        }
    }

    override fun onScrollStopped() {

    }

    override fun onScrolled() {

    }

    override fun onSlideToBottom() {
        removeMoveCheck()
    }

    fun restoreData(restoreData: RestoreData) {
        mIsCheck = restoreData.isCheck
        mMessageHideSwitch.isChecked = mIsCheck
        onCheckedChanged(null, mIsCheck)
    }

    fun getRestoreData(): RestoreData {
        val restoreData = RestoreData()
        restoreData.isCheck = mIsCheck
        return restoreData
    }

    fun updateTempPictureUploadError(key: String?) {

        if (TextUtils.isEmpty(key)) return

        val start = System.currentTimeMillis()
        val list = mPrivateRoomMultiTypeAdapter.items

        var targetItem: TempPictureMessageItem? = null

        list.forEach {
            if (it is TempPictureMessageItem) {
                if (TextUtils.equals(it.picture, key)) {
                    targetItem = it
                }
            }
        }

        targetItem?.let {
            list.remove(targetItem)
            mPrivateRoomMultiTypeAdapter.notifyDataSetChanged()
            /**
             * 提示上传失败
             */
            ToastToolKit.showShort(R.string.str_upload_failed)
        }

        val end = System.currentTimeMillis()
        printlnLog("updateTempPictureUploadError cost ${(end - start)}")
    }

    fun updateTempPictureUploadProgress(key: String?, progress: Int) {

        if (TextUtils.isEmpty(key)) return

        val start = System.currentTimeMillis()
        val list = mPrivateRoomMultiTypeAdapter.items

        list.forEach {
            if (it is TempPictureMessageItem) {
                if (TextUtils.equals(it.picture, key)) {
                    /**
                     * 更新这个消息的百分比
                     */
                    it.progress = progress
                    mPrivateRoomMultiTypeAdapter.notifyDataSetChanged()
                    return@forEach
                }
            }
        }

        val end = System.currentTimeMillis()
        printlnLog("updateTempPictureUploadProgress cost ${(end - start)}")
    }


    override fun destroy() {
        mTempPictureBinder?.destroy()
        mTempPictureBinder = null
        mPrivateRoomRecycleView.release()
        mHandler.release()
        mPrivateRoomObserver = null
    }

    private fun printlnLog(info: String) {
        PLog.i("PrivateRoomMessageView", info)
    }
}