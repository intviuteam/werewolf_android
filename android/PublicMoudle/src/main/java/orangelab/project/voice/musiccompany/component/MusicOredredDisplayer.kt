package orangelab.project.voice.musiccompany.component

import android.view.View
import com.toolkit.action.Destroyable
import kotlinx.android.synthetic.main.fragment_music_ordered.view.*
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.voice.musiccompany.adapter.MusicOrderedAdapter
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource

/**
 * 已点歌曲展示器，处理正在下载的音乐的UI展示
 */
class MusicOredredDisplayer(viewGroup: View) : Destroyable {

    private var contentView = viewGroup

    private var musics = ArrayList<MusicDataEntity>()
    private var musicStockAdapter = MusicOrderedAdapter(musics)

    /**
     * 当前处理的音乐
     */
    private lateinit var musicData: MusicDataEntity

    init {
        contentView.list_music.layoutManager = WrapContentLinearLayoutManager(contentView.context)
        contentView.list_music.adapter = musicStockAdapter

        refresh()

        contentView.search_bar.setCallBack { musicStockAdapter.updateStock(it) }
        contentView.search_bar.changeStock(MusicCompanyDataSource.downLoadedList)
        contentView.search_bar.setCallBack { refreshFilterMusic(it) }
    }

    /**
     * 刷线一下数据
     */
    fun refresh() {
        musics.clear()
        musics.addAll(MusicCompanyDataSource.downLoadedList)
        musicStockAdapter.updateStock(musics)
        contentView.search_bar.changeStock(musics)
    }

    /**
     * 刷新过滤过的列表
     */
    private fun refreshFilterMusic(list: List<MusicDataEntity>) {
        musics.clear()
        musics.addAll(list)
        musicStockAdapter.notifyDataSetChanged()
    }

    override fun destroy() {

    }
}
