package orangelab.project.voice.element;

import android.graphics.PointF;

import com.toolkit.action.Destroyable;

import orangelab.project.voice.manager.VoiceEmotionManager;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.voice.manager.SmallGameManager;

/**
 * Created by lloydfinch on 26/09/2017.
 */

public interface RoomChair extends Destroyable {

    void upSeat(EnterRoomResult.EnterRoomUserItem user);

    void downSeat(int desPosition);

    void lockSeat();

    void unLockSeat();

    void speak();

    void unSpeak();

    void reset();

    void banMic();

    void openMic();

    void openFreeStyle();

    void closeFreeStyle();

    void likeRoom();

    void showEmotion(VoiceEmotionManager.Emotions.Emotion emotion);

    void showGame(SmallGameManager.SmallGame smallGame);

    void tipPrepare();

    void unPrepare();

    void updateStatus(int code);

    void hideReady();

    void beVote(int position);

    void showVote(int duration);

    void endSpeech();

    void hideVote();

    void clearVoteInfo();

    void hideStatus();

    void resetStatus();

    void showKTV();

    void hideKTV();

    void sing();

    void stopSing();

    void turns(int duration);

    void out();

    void reConnected();

    PointF getPositionOnScreen();

    void onResume();

    void onPause();

    int getHeadWidth();

    int getHeadHeight();

    int getEmotionHeadWidth();

    int getEmotionHeadHeight();

    PointF getEmotionHeadPositionOnScreen();

}
