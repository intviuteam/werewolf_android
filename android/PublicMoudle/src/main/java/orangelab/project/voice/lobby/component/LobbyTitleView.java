package orangelab.project.voice.lobby.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;

import cn.intviu.support.GsonHelper;
import orangelab.project.ProjectConfig;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.component.VoiceTitleViewComponent;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.BasicTipDialog;
import orangelab.project.voice.dialog.VoiceSettingDialog;
import orangelab.project.voice.dialog.VoiceSpySettingDialog;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public class LobbyTitleView extends AbstractSocketMessageHandler implements VoiceTitleViewComponent {

    private static final String TAG = "VoiceTitleView";
    private LobbyTitleViewUI voiceTitleViewUI;

    private Context mContext;
    private String roomId;
    private String curTitle;
    private String titleType = "";
    private String curPassword;

    private BasicTipDialog dialog;
    private VoiceSpySettingDialog voiceSpySettingDialog;

    public LobbyTitleView(Context context, ViewGroup contentView) {
        this.mContext = context;
        voiceTitleViewUI = new LobbyTitleViewUI(context, contentView);
    }

    @Override
    public void displayData() {
        String roomId = VoiceRoomConfig.getRoomId();
        String title = VoiceRoomConfig.getRoomTitle();
        String type = VoiceRoomConfig.getRoomType();
        String password = VoiceRoomConfig.getPsw();

        setRoomId(roomId);
        updateTitle(title, type);
        setPassword(password);
    }

    @Override
    public void init(String roomId, String title, String password, String type) {
        setRoomId(roomId);
        updateTitle(title, type);
        setPassword(password);
    }

    @Override
    public void setRoomId(String roomId) {
        this.roomId = roomId;
        voiceTitleViewUI.setRoomId(roomId);
        VoiceRoomConfig.setRoomId(roomId);
    }

    @Override
    public void updateTitle(String title, String type) {
        this.curTitle = title;
        this.titleType = TextUtils.isEmpty(type) ? "" : type;
        voiceTitleViewUI.updateTitle(title, type);
        VoiceRoomConfig.setRoomType(type);
        VoiceRoomConfig.setRoomTitle(title);
    }

    @Override
    public void updateOnLineNumber(int number) {
        VoiceRoomConfig.setOnLineNumber(number);
        voiceTitleViewUI.updateOnLineNumber(number);
    }

    @Override
    public void updateWifiState(boolean isConnected) {
        voiceTitleViewUI.updateWifiState(isConnected);
    }

    @Override
    public void onBackPressed(View.OnClickListener onBackPressedListener) {
        if (onBackPressedListener != null) {
            voiceTitleViewUI.getIvBack().setOnClickListener(onBackPressedListener);
        }
    }

    @Override
    public void onOnLineNumberClicked(View.OnClickListener onOnLineNumberClickedListener) {
        if (onOnLineNumberClickedListener != null) {
            voiceTitleViewUI.getLlOnLineContainer().setOnClickListener(onOnLineNumberClickedListener);
        }
    }

    @Override
    public void onSettingClicked(View.OnClickListener onSettingClickedListener) {
        if (onSettingClickedListener != null) {
            voiceTitleViewUI.getIvSetting().setOnClickListener(onSettingClickedListener);
        } else {
            voiceTitleViewUI.getIvSetting().setOnClickListener((v) -> {
                VoiceSettingDialog voiceSettingDialog = new VoiceSettingDialog(mContext);
                voiceSettingDialog.setTopic(curTitle)
                        .setTopicType(getTitleType())
                        .setPassword(curPassword)
                        .show();
            });
        }
    }

    @Override
    public void onShareClicked(View.OnClickListener onShareClickedListener) {
        if (onShareClickedListener != null) {
            voiceTitleViewUI.getIvShare().setOnClickListener(onShareClickedListener);
        } else {
            voiceTitleViewUI.getIvShare().setOnClickListener((v) -> {

                String psw = TextUtils.isEmpty(curPassword) ? "" : curPassword;

                ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_FROM_LOBBY);
                data.userName = GlobalUserState.getGlobalState().getUserName();
                data.roomId = roomId;
                data.password = psw;

                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onLobbyRoomShare(mContext, data);
            });
        }
    }

    @Override
    public void setPassword(String password) {
        this.curPassword = password;
        PersonalDataHelper.getPersonalData().setCurPassword(curPassword);
        GlobalUserState.getGlobalState().setPassword(password);
        VoiceRoomConfig.setPsw(password);
    }

    @Override
    public String getTitle() {
        return curTitle.trim();
    }

    @Override
    public String getTitleType() {
        return titleType.trim();
    }

    @Override
    public void updateMasterUI() {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public int getOnLineNumber() {
        return VoiceRoomDataSourceManager.getInstance().getOnLineUsers().size();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }

        if (voiceSpySettingDialog != null) {
            if (voiceSpySettingDialog.isShowing()) {
                voiceSpySettingDialog.dismiss();
            }
            voiceSpySettingDialog = null;
        }

        voiceTitleViewUI.destroy();
    }

    @Override
    public void handleServerEvent(ServerMessageEventOver event) {
        PLog.e(TAG, "event:" + event);
        handleMessageEvent(event);
    }

    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson
                (messageBean.payload.toString(), ServerMessageEnterResult.class);
        EnterRoomResult.EnterRoomUserItem member = TransferClassHelper.EnterResult2EnterRoomUser(item);
        voiceTitleViewUI.showComeIn(member.name);
        updateOnLineNumber(getOnLineNumber());
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

    @Override
    public void handleChangePassword(VoiceMessageBean messageBean) {
        String password = messageBean.payload.optString(VoiceConstants.PASSWORD);
        setPassword(password);
    }

    @Override
    public void handleUpdateTitle(VoiceMessageBean messageBean) {
        String title = VoiceRoomConfig.getRoomTitle();
        String type = VoiceRoomConfig.getRoomType();
        updateTitle(title, type);
    }

    @Override
    public void handleForceSeat(VoiceMessageBean messageBean) {
        handleUpSeat(messageBean);
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        if (PositionHelper.isSelf(userId)) {
            //防止server返回非法数据
            if (PositionHelper.isUpSeat()) {
                showBeUpSeat();
            }
        }
    }

    //提示被抱上麦
    private void showBeUpSeat() {
        if (dialog == null) {
            dialog = new BasicTipDialog(mContext);
            dialog.setTitle(MessageUtils.getString(R.string.str_voice_tips))
                    .setMsg(MessageUtils.getString(R.string.str_be_up_seat))
                    .setImage(R.mipmap.vocie_be_seat_bg)
                    .positive();
        }
        if (dialog.isShowing()) {
            return;
        }
        dialog.show();
    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleKickOut(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

    @Override
    public void handleUpdateConfig(VoiceMessageBean messageBean) {
        updateMasterUI();
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        voiceTitleViewUI.showMasterUI();//更新房主特权控件
        RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
        if (result.room_info != null) {
            curTitle = VoiceRoomConfig.getRoomTitle();
            titleType = VoiceRoomConfig.getRoomType();
            curPassword = VoiceRoomConfig.getPsw();

            updateTitle(curTitle, titleType);
            updateOnLineNumber(getOnLineNumber());
        }
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

}
