package orangelab.project.voice.musiccompany.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import kotlinx.android.synthetic.main.layout_voice_music_stock_item.view.*
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.voice.musiccompany.item.ItemDisplayer

/**
 * 已点音乐适配器
 */
class MusicOrderedAdapter(musicList: ArrayList<MusicDataEntity>) : RecyclerView.Adapter<MusicOrderedAdapter.MusicStockViewHolder>() {

    private var musicList = musicList

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MusicStockViewHolder {

        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.layout_voice_music_stock_item, parent, false)

        return MusicStockViewHolder(view)
    }

    override fun getItemCount(): Int {
        return musicList.size
    }

    override fun onBindViewHolder(holder: MusicStockViewHolder, position: Int) {
        ItemDisplayer.displayOrderedItem(holder.itemView, musicList[position], position)
    }

    /**
     * 更新曲库
     */
    fun updateStock(musicList: ArrayList<MusicDataEntity>) {
        this.musicList = musicList
        notifyDataSetChanged()
    }

    inner class MusicStockViewHolder(contentView: View) : RecyclerView.ViewHolder(contentView) {

        init {
            itemView.btn_order_or_play.setOnClickListener {
                var musicData = musicList[adapterPosition]
            }
        }
    }
}