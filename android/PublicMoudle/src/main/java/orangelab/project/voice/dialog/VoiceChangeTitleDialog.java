package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.R;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;

import java.util.List;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.tool.WrapContentGridLayoutManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.adapter.VoiceTopicTypeAdapter;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;

/**
 * Created by lloydfinch on 13/10/2017.
 */

public class VoiceChangeTitleDialog extends SafeDialog implements RadioGroup.OnCheckedChangeListener, Destroyable {

    private static final int columns = 3;

    private EditText txtTitle;
    private View btnSure;
    private View btnCancel;
    private View llChangeTopicContainer;

    private RecyclerView recyclerViewType;
    private VoiceTopicTypeAdapter adapter;

    private Context mContext;
    private String title;
    private String titleType;

    public VoiceChangeTitleDialog(@NonNull Context context, String title, String titleType) {
        super(context, R.style.radius_dialog);
        this.mContext = context;
        this.title = title;
        this.titleType = titleType;
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        init();
    }

    public VoiceChangeTitleDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    private void init() {
        initView();
        initWindow();
        initData();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_change_title, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llChangeTopicContainer = contentView.findViewById(R.id.ll_voice_topic_container);
        txtTitle = (EditText) contentView.findViewById(R.id.txt_topic_content);
        txtTitle.setText(title);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        recyclerViewType = (RecyclerView) contentView.findViewById(R.id.rv_voice_topic_type);
        RecyclerView.LayoutManager manager = new WrapContentGridLayoutManager(mContext, columns);
        recyclerViewType.setLayoutManager(manager);

        if (hideTopicList()) {
            llChangeTopicContainer.setVisibility(View.GONE);
        } else {
            llChangeTopicContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 是否隐藏话题列表
     */
    private boolean hideTopicList() {
        //大厅房间就隐藏话题列表
        return VoiceRoomConfig.isLobby();
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initData() {
        adapter = new VoiceTopicTypeAdapter(VoiceTopicManager.getInstance().findTopicPositionByType(titleType));
        recyclerViewType.setAdapter(adapter);

        VoiceTopicManager.Params topic = VoiceTopicManager.getInstance().getTopic();
        if (topic == null) {
            llChangeTopicContainer.setVisibility(View.GONE);
        } else {
            initTopicType(topic.getTopics());
        }
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> saveTitle());
        btnCancel.setOnClickListener(v -> dismiss());
    }

    private void initTopicType(List<VoiceTopicManager.Params.VoiceTopic> topics) {
        adapter.updateTopicList(topics);
    }

    private void saveTitle() {
        title = txtTitle.getText().toString().trim();
        VoiceTopicManager.Params.VoiceTopic topic = adapter.getSelectedTopic();
        if (!TextUtils.isEmpty(title) && title.length() >= 4) {
            if (VoiceRoomConfig.isLobby()) {
                //大厅的写死成game
                RoomSocketEngineHelper.sendUpdateTitle(title, "game");
            } else {
                //如果当前是ktv房间，并且要改到一个不是ktv的房间，就发送一个停止唱歌的消息
                String type = topic.getType();
                if (VoiceRoomConfig.isIsKTV() && !TextUtils.equals(type, com.networktoolkit.transport.Constant.GAME_TYPE_KTV)) {
                    MusicSocketHelper.INSTANCE.sendStopSing();
                }
                RoomSocketEngineHelper.sendUpdateTitle(title, topic == null ? "" : type);
            }
            dismiss();
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_tip_title_length));
        }

        //不调用,测试类型
        //updateTopicType(topic);
    }

    @Override
    protected void release() {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

    }

    @Override
    public void destroy() {
        if (adapter != null) {
            adapter.destroy();
        }
    }
}
