package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.androidtoolkit.ToastToolKit;
import com.R;

import orangelab.project.common.utils.CountDownView;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.api.VoiceSocketHelper;

import com.toolkit.action.Destroyable;

import java.util.Collections;
import java.util.List;

/**
 * Created by lloydfinch on 25/11/2017.
 * <p>
 * 谁是卧底：投票结果/卧底爆词 对话框
 */

public class VoiceSpySystemMsgDialog extends BasicFormatDialog implements Destroyable {

    private TextView tvTitle;
    private TextView tvCount;
    private TextView tvMsg;
    private TextView tvNotifyMsg;
    private CountDownView countDownView;
    private EditText txtGuessContent;
    private View btnSure;

    public VoiceSpySystemMsgDialog(@NonNull Context context) {
        super(context);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    public VoiceSpySystemMsgDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    @Override
    public void initView() {
        setContentView(R.layout.layout_dialog_spy_system);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvCount = (TextView) findViewById(R.id.tv_count);
        tvMsg = (TextView) findViewById(R.id.tv_msg);
        tvNotifyMsg = (TextView) findViewById(R.id.tv_notify_msg);
        txtGuessContent = (EditText) findViewById(R.id.txt_guess_content);
        btnSure = findViewById(R.id.btn_sure);

        tvMsg.setText(Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_identity, "卧底")));
        tvNotifyMsg.setText(Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk, "PK")));
    }

    @Override
    public void initListener() {
        btnSure.setOnClickListener(v -> {
            guessWord();
        });
    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initData() {

    }

    public void showButton(boolean show) {
        if (show) {
            btnSure.setVisibility(View.VISIBLE);
        } else {
            btnSure.setVisibility(View.GONE);
        }
    }

    public void showEditTxt(boolean show) {
        if (show) {
            txtGuessContent.setVisibility(View.VISIBLE);
        } else {
            txtGuessContent.setVisibility(View.GONE);
        }
    }

    public void showNotifyMsg(boolean show) {
        if (show) {
            tvNotifyMsg.setVisibility(View.VISIBLE);
        } else {
            tvNotifyMsg.setVisibility(View.GONE);
        }
    }

    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }

    public void setTvMsg(CharSequence msg) {
        tvMsg.setText(msg);
    }

    public void setNotifyMsg(CharSequence nofity) {
        tvNotifyMsg.setText(nofity);
    }

    //启动计时
    public void startCount(int second, CountDownView.CountDownFinish countDownFinish) {
        if (countDownFinish == null) {
            countDownView = new CountDownView(tvCount, second, () -> dismiss());
        } else {
            countDownView = new CountDownView(tvCount, second, countDownFinish);
        }
        countDownView.setCountDownRule(R.string.str_voice_second_with_wrap_s);
        countDownView.start();
    }

    //发起猜词请求
    private void guessWord() {
        CharSequence word = txtGuessContent.getText();
        if (!TextUtils.isEmpty(word)) {
            String guess = word.toString().trim();
            VoiceSocketHelper.sendSpyGuessWord(guess);
            dismiss();
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_tip_guess_word_empty));
        }
    }

    public void showGuessWord() {
        showButton(true);
        showEditTxt(true);
        showNotifyMsg(false);
        setTitle(MessageUtils.getString(R.string.str_voice_spy_bomb_word));
        setTvMsg(MessageUtils.getString(R.string.str_voice_guess_good_word));
        show();
    }

    public void showPK(List<Integer> positions) {
        if (positions == null || positions.size() < 2) {
            return;
        }
        showButton(false);
        showEditTxt(false);
        showNotifyMsg(true);
        setTitle(MessageUtils.getString(R.string.str_voice_vote_result));
        setNotifyMsg(Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_pk)));
        Collections.sort(positions);
        if (positions.size() == 2) {
            setTvMsg(MessageUtils.getString(R.string.str_voice_vote_msg_deuce, positions.get(0), positions.get(1)));
        } else {
            StringBuilder sb = new StringBuilder();
            for (Integer position : positions) {
                sb.append(position).append("、");
            }
            String votePositions = sb.deleteCharAt(sb.length() - 1).toString();
            setTvMsg(MessageUtils.getString(R.string.str_voice_vote_msg_deuce_more, votePositions));
        }
        show();
    }

    public void showOut(int position, String role) {
        showButton(false);
        showEditTxt(false);
        showNotifyMsg(true);
        setTitle(MessageUtils.getString(R.string.str_voice_vote_result));
        setTvMsg(MessageUtils.getString(R.string.str_voice_vote_msg_out, position));
        setNotifyMsg(Html.fromHtml(MessageUtils.getString(R.string.str_voice_vote_msg_identity, role)));
        show();
    }

    public void showGiveUp() {
        showButton(false);
        showEditTxt(false);
        showNotifyMsg(true);
        setTitle(MessageUtils.getString(R.string.str_voice_vote_result));
        setTvMsg(MessageUtils.getString(R.string.str_voice_vote_msg_give_up));
        show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        hideKeyBoard();
    }

    private void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(txtGuessContent.getWindowToken(), 0);
            }
        } catch (Exception e) {
            PLog.e("hidekeyboard:", "error:" + e.getMessage());
        }
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {
        if (countDownView != null) {
            countDownView.cancel();
        }
    }
}
