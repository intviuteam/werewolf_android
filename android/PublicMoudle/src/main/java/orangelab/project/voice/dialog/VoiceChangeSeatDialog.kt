package orangelab.project.voice.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.view.ScreenUtils
import com.R
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.api.VoiceSocketHelper
import orangelab.project.voice.config.VoiceRoomConfig
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager
import orangelab.project.voice.utils.PositionHelper
import kotlinx.android.synthetic.main.layout_dialog_change_seat.*
import orangelab.project.voice.manager.VoiceTopicManager

/**
 * Created by lloydfinch on 17/03/2018.
 */
class VoiceChangeSeatDialog : SafeDialog {

    private var seatNumber: Int = 0//当前座位数

    init {
        seatNumber = VoiceRoomConfig.getMaxChairNumber()
    }

    constructor(context: Context) : this(context, R.style.radius_dialog)

    constructor(context: Context, res: Int) : super(context, res) {
        init()
    }

    private fun init() {
        initView()
        initWindow()
        initListener()
    }

    private fun initView() {
        setContentView(LayoutInflater.from(context).inflate(R.layout.layout_dialog_change_seat, null))

        if (VoiceTopicManager.getInstance().topic.active != null) {
            if (VoiceTopicManager.getInstance().topic.active.active_options != null) {
                if (VoiceTopicManager.getInstance().topic.active.active_options.contains(8 + 1)) {
                    this.findViewById<RadioButton>(R.id.rb_voice_8_seat).visibility = View.VISIBLE
                } else {
                    this.findViewById<RadioButton>(R.id.rb_voice_8_seat).visibility = View.GONE
                }
                if (VoiceTopicManager.getInstance().topic.active.active_options.contains(12 + 1)) {
                    this.findViewById<RadioButton>(R.id.rb_voice_12_seat).visibility = View.VISIBLE
                } else {
                    this.findViewById<RadioButton>(R.id.rb_voice_12_seat).visibility = View.GONE
                }
                if (VoiceTopicManager.getInstance().topic.active.active_options.contains(16 + 1)) {
                    this.findViewById<RadioButton>(R.id.rb_voice_16_seat).visibility = View.VISIBLE
                } else {
                    this.findViewById<RadioButton>(R.id.rb_voice_16_seat).visibility = View.GONE
                }
            }
        }

        when (seatNumber) {
            8 -> set8SeatNumber()
            12 -> set12SeatNumber()
            16 -> set16SeatNumber()
        }
    }

    private fun initWindow() {
        val dialogMaxWidth = ScreenUtils.dip2px(360f)
        val params = window!!
                .attributes
        val dur = (ScreenUtils.getScreenWidth() * 0.25f).toInt()
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth
        } else {
            params.width = (ScreenUtils.getScreenWidth() * 0.95f).toInt()
        }
        window!!.attributes = params
    }

    private fun initListener() {
        btn_sure.setOnClickListener {
            if (judgeSocketConnect()) {
                if (VoiceRoomConfig.isIsSupportChangeSeat()) {
                    if (canChangeSeat()) {
                        VoiceSocketHelper.changeSeatNumber(seatNumber + 1)
                        dismiss()
                    } else {
                        ToastToolKit.showShort(R.string.str_voice_tips_cannot_change_seat)
                    }
                }
            }
        }
        btn_cancel.setOnClickListener { dismiss() }

        rb_voice_8_seat.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                set8SeatNumber()
            }
        }

        rb_voice_12_seat.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                set12SeatNumber()
            }
        }

        rb_voice_16_seat.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                set16SeatNumber()
            }
        }
    }

    private fun canChangeSeat(): Boolean {
        if (PositionHelper.isMaster() && VoiceRoomConfig.getOnLineNumber() == 1) {
            return true
        }
        return false
    }

    //判断是否连接
    private fun judgeSocketConnect(): Boolean {
        val isConnected = VoiceRoomGameSocketManager.getInstance().isConnected
        if (!isConnected) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.socket_disconnect))
        }
        return isConnected
    }

    private fun set8SeatNumber() {
        rb_voice_8_seat.isChecked = true
        rb_voice_12_seat.isChecked = false
        rb_voice_16_seat.isChecked = false

        seatNumber = 8
    }

    private fun set12SeatNumber() {
        rb_voice_8_seat.isChecked = false
        rb_voice_12_seat.isChecked = true
        rb_voice_16_seat.isChecked = false

        seatNumber = 12
    }

    private fun set16SeatNumber() {
        rb_voice_8_seat.isChecked = false
        rb_voice_12_seat.isChecked = false
        rb_voice_16_seat.isChecked = true

        seatNumber = 16
    }

    override fun release() {
    }
}