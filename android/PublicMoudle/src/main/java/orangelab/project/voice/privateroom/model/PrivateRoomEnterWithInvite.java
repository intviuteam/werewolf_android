package orangelab.project.voice.privateroom.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2018/5/24 上午11:27
 * Mystery
 */
public class PrivateRoomEnterWithInvite implements Keepable {

    public String room_id;
    public String password;
    public String gameType;
    public List<String> ids;
}
