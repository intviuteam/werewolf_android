package orangelab.project.voice.element;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.component.BasicViewComponent;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.view.HeadDecorView;
import orangelab.project.common.view.VipNameView;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.common.effect.gif.GifImageView;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.view.OctopusView;
import orangelab.project.voice.view.ProgressTrackBar;
import orangelab.project.voice.view.SmallGameView;
import orangelab.project.voice.view.VoteNumberLayout;

/**
 * Created by lloydfinch on 04/09/2017.
 */

public class VoiceChairUI extends BasicViewComponent {

    public static final String TAG = "VoiceChairUI";

    private ViewGroup chairContainer;
    private int position;

    private ImageView ivChairNumber;
    public HeadDecorView ivHead;
    public VipNameView tvName;
    private View ivBanMic;
    private View ivFreeStyle;
    public View llSpeakContainer;
    public ImageView ivSpeak;
    public GifImageView ivEmotion;
    public SmallGameView ivGame;
    public View viewChouMic;
    public TextView tvChouMic;
    public OctopusView octopusView;
    public ImageView ivGameResult;
    public View userContainer;

    private TextView tvUserStatus;//房主和其他人要区分开,所有人可见
    private View btnReady;
    private View btnVote;
    private View llOutCover;
    private View ivOutIcon;
    private VoteNumberLayout voteContainer;//投票数字的容器
    private ProgressTrackBar progressTrackBar;//说话时外环进度条
    private View llStatus;//所有人可见,仅房主可设置
    private View ivStatusChecked;
    private View btnEndSpeech;

    private View ivShowKTV;
    private View ivShowSing;
    private View llMusicWaveContainer;
    private ImageView ivMusicWave;//唱歌时的波浪扩散效果

    private Context mContext;

    public ViewGroup getChairContainer() {
        return chairContainer;
    }

    public VoiceChairUI(ViewGroup chairContainer, int position) {
        this.chairContainer = chairContainer;
        this.position = position;
        mContext = chairContainer.getContext();
        init();
    }

    public void setOnChairClickListener(OnChairClickListener onChairClickListener) {
        this.ivHead.setOnClickListener((view) -> {
            onChairClickListener.onChairClick(view);
        });
    }

    private void init() {
        initView();
        initListener();
    }

    private void initView() {
        this.ivChairNumber = (ImageView) chairContainer.findViewById(R.id.voice_chair_tag);
        this.ivHead = (HeadDecorView) chairContainer.findViewById(R.id.iv_head_member);
        this.tvName = (VipNameView) chairContainer.findViewById(R.id.tv_name_member);
        this.ivBanMic = chairContainer.findViewById(R.id.voice_tag_ban_mic);
        this.llSpeakContainer = chairContainer.findViewById(R.id.ll_voice_speak_container);
        this.ivSpeak = (ImageView) chairContainer.findViewById(R.id.iv_voice_speak);
        this.ivFreeStyle = chairContainer.findViewById(R.id.voice_tag_free_mic);
        this.ivEmotion = (GifImageView) chairContainer.findViewById(R.id.iv_voice_chair_emotion);
        this.ivGame = (SmallGameView) chairContainer.findViewById(R.id.iv_voice_chair_game);
        this.viewChouMic = chairContainer.findViewById(R.id.rl_small_game);
        this.tvChouMic = (TextView) chairContainer.findViewById(R.id.tv_mic_result);
        this.octopusView = (OctopusView) chairContainer.findViewById(R.id.ov_game);
        this.ivGameResult = (ImageView) chairContainer.findViewById(R.id.iv_voice_chair_game_result);
        this.userContainer = chairContainer.findViewById(R.id.ll_user_container);
        this.tvUserStatus = (TextView) chairContainer.findViewById(R.id.tv_status);
        this.btnReady = chairContainer.findViewById(R.id.btn_ready);
        this.btnVote = chairContainer.findViewById(R.id.btn_vote);
        this.btnEndSpeech = chairContainer.findViewById(R.id.btn_end_speech);
        this.llOutCover = chairContainer.findViewById(R.id.ll_voice_user_out);
        this.ivOutIcon = chairContainer.findViewById(R.id.iv_user_out);
        this.voteContainer = (VoteNumberLayout) chairContainer.findViewById(R.id.vote_num_layout);
        this.progressTrackBar = (ProgressTrackBar) chairContainer.findViewById(R.id.progress_bar);
        this.llStatus = chairContainer.findViewById(R.id.ll_status);
        this.ivStatusChecked = chairContainer.findViewById(R.id.iv_status_checked);
        this.ivShowKTV = chairContainer.findViewById(R.id.voice_tag_open_ktv);
        this.ivShowSing = chairContainer.findViewById(R.id.voice_tag_sing);

        llMusicWaveContainer = chairContainer.findViewById(R.id.ll_wave_container);
        this.ivMusicWave = (ImageView) chairContainer.findViewById(R.id.iv_music_wave);

        setChairNumber();
        llSpeakContainer.setVisibility(View.GONE);
        viewChouMic.setVisibility(View.GONE);
        octopusView.setVisibility(View.GONE);
        ivGameResult.setVisibility(View.GONE);
        llStatus.setVisibility(View.GONE);
        tvUserStatus.setVisibility(View.GONE);
        btnReady.setVisibility(View.GONE);
        btnVote.setVisibility(View.GONE);
        btnEndSpeech.setVisibility(View.GONE);
        llOutCover.setVisibility(View.GONE);
        ivOutIcon.setVisibility(View.GONE);
        voteContainer.setVisibility(View.GONE);
        progressTrackBar.setVisibility(View.GONE);
    }

    private void setChairNumber() {
        if (PositionHelper.isMaster(position)) {
            ivChairNumber.setImageResource(R.mipmap.voice_tag_master);
        } else if (position <= VoiceConstants.MAX_CHAIR_16) {
            try {
                int imageId = R.mipmap.class.getDeclaredField("voice_tag_" + position).getInt(null);
                ivChairNumber.setImageResource(imageId);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private void initListener() {

    }

    public void banMic() {
        ivBanMic.setVisibility(View.VISIBLE);
    }

    public void openMic() {
        ivBanMic.setVisibility(View.GONE);
    }

    public void openFreeStyle() {
        ivFreeStyle.setVisibility(View.VISIBLE);
    }

    public void closeFreeStyle() {
        ivFreeStyle.setVisibility(View.GONE);
    }

    public void likeRoom() {
    }

    public void showReady() {
        btnReady.setVisibility(View.VISIBLE);
        ivHead.setBorderColor(Color.YELLOW);
    }

    public void hideReady() {
        btnReady.setVisibility(View.GONE);
        ivHead.setBorderColor(Color.parseColor("#19ffffff"));
    }


    public void showVote() {
        btnVote.setVisibility(View.VISIBLE);
    }

    public void hideVote() {
        btnVote.setVisibility(View.GONE);
    }

    public void showEndSpeech() {
        btnEndSpeech.setVisibility(View.VISIBLE);
    }

    public void hideEndSpeech() {
        btnEndSpeech.setVisibility(View.GONE);
    }

    public void showOut() {
        llOutCover.setVisibility(View.VISIBLE);
        ivOutIcon.setVisibility(View.VISIBLE);
    }

    public void hideOut() {
        llOutCover.setVisibility(View.GONE);
        ivOutIcon.setVisibility(View.GONE);
    }

    public void showKTV() {
        ivShowKTV.setVisibility(View.VISIBLE);
    }

    public void hideKTV() {
        ivShowKTV.setVisibility(View.GONE);
    }

    public void sing() {

        ivShowSing.setVisibility(View.VISIBLE);
        llMusicWaveContainer.setVisibility(View.VISIBLE);

        //启动波纹效果
        AnimationDrawable animationDrawable = (AnimationDrawable) ivMusicWave.getDrawable();
        if (animationDrawable != null) {
            animationDrawable.start();
        }
    }

    public void stopSing() {
        ivShowSing.setVisibility(View.GONE);
        ivHead.clearAnimation();

        //停止波纹效果
        AnimationDrawable animationDrawable = (AnimationDrawable) ivMusicWave.getDrawable();
        if (animationDrawable != null) {
            animationDrawable.stop();
        }
        llMusicWaveContainer.setVisibility(View.GONE);
    }

    public void showProgressBar(int progress) {
        progressTrackBar.setVisibility(View.VISIBLE);
        progressTrackBar.startTask(progress, () -> {
            hideEndSpeech();
        });
    }

    public void hideProgressBar() {
        progressTrackBar.setVisibility(View.GONE);
    }

    public void showUserStatus() {
        tvUserStatus.setVisibility(View.VISIBLE);
    }

    public void hideUserStatus() {
        llStatus.setVisibility(View.GONE);
    }

    public void changeUserStatus(String status) {
        llStatus.setVisibility(View.VISIBLE);
        tvUserStatus.setVisibility(View.VISIBLE);
        tvUserStatus.setText(status);
    }

    //提示准备
    public void tipPrepare() {
        //是玩家，是"谁是卧底"房，游戏不是进行中，才显示"准备"按钮
        if (PositionHelper.isPlayer() && VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
            showReady();
        }
    }

    public void clearTipPrepare() {
        ivHead.setBorderColor(Color.parseColor("#19ffffff"));
    }

    //准备中 only newPlayer
    public void momentPrepare() {
        //是玩家，是"谁是卧底"房，并且不在游戏中，才显示"准备中"
        if (PositionHelper.isPlayer(position) && VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
            hideReady();
            tvUserStatus.setTextColor(Color.BLACK);
            changeUserStatus(MessageUtils.getString(R.string.str_voice_user_status_ready));
            tvUserStatus.setBackgroundResource(R.drawable.bg_voice_user_status_ready);
            ivStatusChecked.setVisibility(View.GONE);
        }
    }

    //投票中 only newPlayer
    public void momentVote() {
        if (PositionHelper.isPlayer(position) && VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
            hideVote();
            tvUserStatus.setTextColor(Color.WHITE);
            changeUserStatus(MessageUtils.getString(R.string.str_voice_user_status_vote));
            tvUserStatus.setBackgroundResource(R.drawable.bg_voice_user_status_vote);
            ivStatusChecked.setVisibility(View.GONE);
        }
    }

    //离线中
    public void momentOffLine() {
        changeUserStatus(MessageUtils.getString(R.string.str_voice_user_status_offline));
        tvUserStatus.setTextColor(Color.BLACK);
        tvUserStatus.setBackgroundResource(R.drawable.bg_voice_user_status_offline);
        ivStatusChecked.setVisibility(View.GONE);
    }

    //设置中 only master
    public void momentSetting() {
        if (PositionHelper.isMaster(position) && VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
            changeUserStatus(MessageUtils.getString(R.string.str_voice_master_status_setting));
            tvUserStatus.setTextColor(Color.WHITE);
            tvUserStatus.setBackgroundResource(R.drawable.bg_voice_master_status_setting);
            ivStatusChecked.setVisibility(View.GONE);
        }
    }

    //设置完成 only master
    public void momentSetFinished() {
        if (PositionHelper.isMaster(position) && VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
            //默认设置也直接提示"设置完成"
//            if (VoiceRoomConfig.SpyConfig.isIsDefaultSetting()) {
//                //默认设置
//                changeUserStatus(MessageUtils.getString(R.string.str_voice_master_status_set_default));
//                tvUserStatus.setBackgroundResource(R.drawable.bg_voice_master_status_set_default);
//                ivStatusChecked.setVisibility(View.GONE);
//            } else {
//                changeUserStatus(MessageUtils.getString(R.string.str_voice_master_status_set_over));
//                tvUserStatus.setBackgroundResource(R.drawable.bg_voice_master_status_finished_set);
//                ivStatusChecked.setVisibility(View.VISIBLE);
//            }

            changeUserStatus(MessageUtils.getString(R.string.str_voice_master_status_set_over));
            tvUserStatus.setTextColor(Color.WHITE);
            tvUserStatus.setBackgroundResource(R.drawable.bg_voice_master_status_finished_set);
            ivStatusChecked.setVisibility(View.VISIBLE);
        }
    }

    //不显示
    public void momentNothing() {
        changeUserStatus("");
    }

    //出局
    public void out() {
        showOut();
    }

    //开局UI
    public void open() {
        hideOut();
        clearStatus();
        showReady();
    }

    //被投票
    public void beVote(int vPosition) {
        if (PositionHelper.isPlayer(vPosition) && VoiceRoomConfig.isSpyRoom()) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_voice_vote_number, null);
            TextView tvVoteNumber = (TextView) view.findViewById(R.id.tv_vote_number);
            tvVoteNumber.setText(String.valueOf(vPosition));
            voteContainer.addVoteView(view);
            voteContainer.setVisibility(View.VISIBLE);
        }
    }

    //清除投票信息
    public void clearVoteInfo() {
        voteContainer.removeAllViews();
        voteContainer.setVisibility(View.GONE);
    }

    //轮到说话,展示进度条
    public void turns(int duration) {
        if (PositionHelper.isPlayer(position) && VoiceRoomConfig.isSpyRoom()) {
            progressTrackBar.setVisibility(View.VISIBLE);
            progressTrackBar.setMaxProgress(duration);
            progressTrackBar.startTask(0, () -> {
                try {
                    PersonalData personalData = PersonalDataHelper.getPersonalData();
                    if (personalData != null) {
                        if (PositionHelper.isSelfPosition(position)) {
                            hideEndSpeech();
                        }
                    } else {
                        hideEndSpeech();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
            try {
                PersonalData personalData = PersonalDataHelper.getPersonalData();
                if (personalData != null) {
                    if (PositionHelper.isSelfPosition(position)) {
                        showEndSpeech();
                    } else {
                        hideEndSpeech();
                    }
                } else {
                    hideEndSpeech();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void endSpeech() {
        if (progressTrackBar != null) {
            progressTrackBar.stopTask();
            progressTrackBar.setVisibility(View.GONE);
        }
        hideEndSpeech();
    }

    public void setOnPrepareClickListener(OnPrepareClickListener onPrepareClickListener) {
        this.btnReady.setOnClickListener(v -> onPrepareClickListener.onPrepareClick(v));
    }

    public void setOnVoteClickListener(OnVoteClickListener onVoteClickListener) {
        this.btnVote.setOnClickListener(v -> {
            hideVote();
            onVoteClickListener.onVoteClick(v);
        });
    }

    public void setEndSpeechListener(OnEndSpeechClickListener onEndSpeechClickListener) {
        this.btnEndSpeech.setOnClickListener(v -> {
            hideEndSpeech();
            onEndSpeechClickListener.onEndSpeechClick(v);
        });
    }

    @Override
    public void destroy() {
        super.destroy();
        ivHead = null;
        tvName = null;
        chairContainer = null;
        if (ivEmotion != null) {
            if (ivEmotion.isAnimating()) {
                ivEmotion.clearAnimation();
            }
            ivEmotion.clear();
            ivEmotion = null;
        }

        if (ivGame != null) {
            if (ivGame.isAnimating()) {
                ivGame.clearAnimation();
            }
            ivGame.destroy();
            ivGame = null;
        }

        if (tvChouMic != null) {
            tvChouMic.clearAnimation();
            tvChouMic = null;
        }

        if (ivGameResult != null) {
            ivGameResult.clearAnimation();
        }

        if (octopusView != null) {
            octopusView.destroy();
        }

        if (progressTrackBar != null) {
            progressTrackBar.stopTask();
        }
        if (voteContainer != null) {
            voteContainer.destroy();
        }
    }

    public void clearStatus() {
        hideReady();
        hideUserStatus();
        hideVote();
        hideOut();
        hideEndSpeech();
        hideProgressBar();
        clearVoteInfo();
    }

    public interface OnChairClickListener {
        void onChairClick(View view);
    }

    public interface OnPrepareClickListener {
        void onPrepareClick(View view);
    }

    public interface OnVoteClickListener {
        void onVoteClick(View view);
    }

    public interface OnEndSpeechClickListener {
        void onEndSpeechClick(View view);
    }
}
