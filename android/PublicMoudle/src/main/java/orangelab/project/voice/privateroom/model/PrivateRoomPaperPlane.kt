package orangelab.project.voice.privateroom.model

import com.toolkit.action.Keepable

/**
 * game_werewolf
 * 2018/3/28 下午6:51
 * Mystery
 */

class PrivateRoomPaperPlane : Keepable {

    var fromPosition: Int = -1
    var fromUserId: String = ""
    var fromUserIcon: String = ""
    var text: String = ""

    var mSelfPosition = -1

}
