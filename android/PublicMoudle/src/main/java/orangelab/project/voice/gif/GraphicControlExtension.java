package orangelab.project.voice.gif;

import com.androidtoolkit.StreamToolKit;

/**
 * Created by lloydfinch on 31/10/2017.
 */

public class GraphicControlExtension {


    public byte[] bytes;
    public int size;

    public GraphicControlExtension(byte[] bytes, int offset) {
        size = 8;
        this.bytes = new byte[size];
        System.arraycopy(bytes, offset, this.bytes, 0, size);
    }

    public int getExtensionIntroducer() {
        return bytes[0] & 0xFF;
    }

    public int getGraphicControlLabel() {
        return bytes[1] & 0xFF;
    }

    public int getBlockSize() {
        return bytes[2] & 0xFF;
    }

    public int getReserved() {
        return (bytes[3] & 0xE0) >> 5;
    }

    public int getDisposalMothod() {
        return (bytes[3] & 0x1C) >> 2;
    }

    public int getUserInputFlag() {
        return (bytes[3] & 0x02) >> 1;
    }

    public int getTransparentColorFlag() {
        return (bytes[3] & 0x01);
    }

    public int getDelayTime() {
        return (bytes[4] & 0xFF) + ((bytes[5] & 0xFF) << 8);
    }

    public int getTransparentColorIndex() {
        return bytes[6];
    }

    public void setTransparentColorFlagTrue() {
        int value = getReserved() | getDisposalMothod() | getUserInputFlag() | 0x01;
        bytes[3] = (byte) Integer.parseInt(StreamToolKit.toHex(value, 2), 16);
    }
}
