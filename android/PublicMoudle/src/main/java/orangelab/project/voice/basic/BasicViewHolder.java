package orangelab.project.voice.basic;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lloydfinch on 08/11/2017.
 * recycler view通用ViewHolder
 * 当itemView的子view很多时牺牲空间提高时间效率
 */

public class BasicViewHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> childViewSet = new SparseArray<>();

    public BasicViewHolder(View itemView) {
        super(itemView);
    }

    public BasicViewHolder setOnItemClickListener(OnItemClickListener onItemClickListener) {
        if (onItemClickListener != null) {
            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(itemView, getPosition()));
        }
        return this;
    }

    public BasicViewHolder setOnChildClickListener(int viewId, OnItemClickListener onChildClickListener) {
        View view = findChildViewById(viewId);
        if (view != null && onChildClickListener != null) {
            view.setOnClickListener(v -> onChildClickListener.onItemClick(view, getPosition()));
        }
        return this;
    }

    public BasicViewHolder setText(int viewId, int resId) {
        TextView textView = findChildViewById(viewId);
        textView.setText(resId);

        return this;
    }

    public BasicViewHolder setText(int viewId, CharSequence text) {
        TextView textView = findChildViewById(viewId);
        textView.setText(text);

        return this;
    }

    public BasicViewHolder setImage(int viewId, int resId) {
        ImageView imageView = findChildViewById(viewId);
        imageView.setImageResource(resId);

        return this;
    }

    public BasicViewHolder setImage(int viewId, Bitmap bitmap) {
        ImageView imageView = findChildViewById(viewId);
        imageView.setImageBitmap(bitmap);

        return this;
    }

    public BasicViewHolder setVisibility(int viewId, int visibility) {
        View view = findChildViewById(viewId);
        view.setVisibility(visibility);
        return this;
    }

    public BasicViewHolder setVisibility(int visibility) {
        itemView.setVisibility(visibility);
        return this;
    }

    public <T extends View> T findChildViewById(int viewId) {
        View childView = childViewSet.get(viewId);
        if (childView == null) {
            childView = itemView.findViewById(viewId);
            childViewSet.put(viewId, childView);
        }
        if (childView == null) {
            throw new IllegalArgumentException("invalid view id,please make sure the id you passed in the current layout!");
        }
        return (T) childView;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
