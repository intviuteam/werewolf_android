package orangelab.project.voice.privateroom.model

import com.toolkit.action.Keepable

/**
 * game_werewolf
 * 2018/3/22 下午4:31
 * Mystery
 */

class PrivateRoomSeatDataImpl : Keepable, PrivateRoomSeatData {

    var position = -1
    var isLock = false
    var memberData: PrivateRoomMemberData? = null
    var isNeedRefresh = false

    constructor(position: Int) {
        this.position = position
    }

    override fun toString(): String {
        return "PrivateRoomSeatData(position=$position, isLock=$isLock, memberData=$memberData)\n"
    }

    override fun getSeatPosition(): Int {
        return position
    }

    override fun seatIsLock(): Boolean {
        return isLock
    }

    override fun seatNeedRefresh(): Boolean {
        return isNeedRefresh
    }

    override fun seatRefreshDone() {
        isNeedRefresh = false
    }

    override fun getSeatMemberData(): PrivateRoomMemberData? {
        return memberData
    }

}
