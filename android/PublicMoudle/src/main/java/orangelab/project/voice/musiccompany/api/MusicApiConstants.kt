package orangelab.project.voice.musiccompany.api

/**
 * 音乐伴奏api地址
 */
class MusicApiConstants {


    companion object {

        private const val COMMON_URL: String = "http://staging.intviu.cn:8206"

        const val START_SING: String = "start_sing"
        const val STOP_SING: String = "stop_sing"
        const val MUSIC_ID: String = "music_id"
        const val BOOK_SONG: String = "book_song"

        const val MUSIC_POP_SONG = "pop_song"
        const val MUSIC_AUTHORIZE_SING = "authorize_sing"
        const val MUSIC_REMOVE_SONG = "remove_song"
        const val MUSIC_PREPARE_SONG = "prepare_song"
        const val GME_ROOM_ID = "room_id"

        /**
         * 获取推荐歌曲的url
         */
        fun getRecommendList(page: Int, pagesize: Int): String {
            return "/music/song/paging?show=commend&page=$page&pagesize=$pagesize"
        }

        /**
         * 获取曲库列表的url
         */
        fun getSongStockUrl(keyword: String, page: Int, pagesize: Int): String {
            return "/music/song/paging?keyword=$keyword&page=$page&pagesize=$pagesize"
        }

        /**
         * 获取歌曲详情url
         */
        fun getSongInfoUrl(id: String): String {
            return "/music/song/get?id=$id"
        }
    }
}