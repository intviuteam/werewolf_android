package orangelab.project.voice.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.transport.TransportHelper;
import com.datasource.GlobalUserState;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import cn.intviu.orbit.OrbitError;
import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import orangelab.project.MainApplication;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.activity.RoomSafeActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.dialog.LoadingDialog;
import orangelab.project.common.dialog.MsgDialog;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.GlobalEvent;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.ExhibitionsEvent;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.exhibition.gift.GiftEvent;
import orangelab.project.common.floatwindow.FloatWindowCommander;
import orangelab.project.common.floatwindow.model.FloatUser;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.component.VoiceRoomUIController;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceDialogSendGift;
import orangelab.project.voice.dialog.VoiceFriendDialog;
import orangelab.project.voice.dialog.VoiceLeaveDialog;
import orangelab.project.voice.dialog.VoiceQuitDialog;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.SWStyleManager;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomAudioSocketManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.model.VoiceRoomAudioConfigBean;
import orangelab.project.voice.model.VoiceToOnlineBridgeBean;
import orangelab.project.voice.model.VoiceToRoomBridgeBean;
import orangelab.project.voice.musiccompany.MusicUIController;
import orangelab.project.voice.musiccompany.concrete.MusicManager;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.musiccompany.event.MusicEvent;
import orangelab.project.voice.musiccompany.model.PrepareSingBean;
import orangelab.project.voice.musiccompany.service.MusicDownloadService;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;
import rx.android.schedulers.AndroidSchedulers;

/**
 * 目前Activity扮演的角色: 控制器
 * 根据不同的数据控制UI的展示，以及消息的处理
 */
public class VoiceRoomActivity extends RoomSafeActivity {

    public static final String TAG = "VoiceRoomActivity";

    //静态UI===================================================
    private VoiceRoomUIController voiceRoomUIController;
    private MusicUIController musicUIController;

    //弹出式UI==============================================
    private VoiceQuitDialog quitDialogSW;//退出对黄框，有小窗选项
    private VoiceLeaveDialog quitDialogNormal;//退出对话框，无小窗选项
    private LoadingDialog mExitLoadingDialog;//退出时的延迟对话框
    private MsgDialog permissionDialog;//提示权限对话框
    //==============================================

    private boolean isSocketReconnectedFailed = false;
    private VoiceToRoomBridgeBean bean;
    private String enginToken = ""; //连接的engin的token

    @Override
    protected void onActivityWindowInitFinish() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PLog.i(TAG, "onCreate: ");

        EventBus.getDefault().register(this);

        checkStateAndClearResidualData();

        //数据初始化
        VoiceToRoomBridgeBean temp = TransportHelper.getTransport(TAG);
        if (temp != null) {
            bean = temp;
            enginToken = bean.enginToken;
        }
        if (bean == null) {
            exitBySendMessage();
            return;
        }

        //设置游戏类型
        GlobalUserState.getGlobalState().setCurGameType(IntentDataHelper.getGameType(getIntent()));
        init();
        ReportEventUtils.reportGameEnter();
        GlobalUserState.getGlobalState().setGaming(true);

        //根据是正常进入还是小窗进入做不同处理
        disposeByEnterMode();
    }

    //检察socket连接状态，释放残留的app数据(通知)
    private void checkStateAndClearResidualData() {
        if (!MainApplication.getInstance().socketIsExit()) {
            finish();
            return;
        }
        NotificationUtils.IMPL().clearAllNotification(this);
    }

    private void init() {
        initWindow();
        initView();
        initData();
        initListener();

        //根据类型切换UI
        switchUI();
        if (VoiceRoomConfig.isIsKTV()) {
            //是ktv房间s展示直播数据
            showLive();
        } else {
            //不是ktv房间，申请语音权限
            requestPermission();
        }
    }

    private void initWindow() {
        //状态栏一体化
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View decorView = getWindow().getDecorView();
                int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                decorView.setSystemUiVisibility(option);
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //屏幕常亮
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void initView() {
        setContentView(R.layout.activity_music_room);
        ViewGroup contentView = (ViewGroup) this.findViewById(android.R.id.content);
        //初始化房间座位数量
        VoiceRoomConfig.setMaxChairNumber(bean.result.room.config.active_count - 1);
        voiceRoomUIController = new VoiceRoomUIController(this, contentView);

        //初始化音乐播放控制器
        musicUIController = new MusicUIController(this, contentView);
    }

    /**
     * 根据房间类型切换UI
     */
    private void switchUI() {
        if (VoiceRoomConfig.isIsKTV()) {
            musicUIController.showKtv();
        } else {
            musicUIController.hideKtv();
        }
    }

    private void initData() {
        Intent intent = getIntent();
        VoiceRoomDataSourceManager.getInstance().init(intent, bean);
        voiceRoomUIController.init();
        musicUIController.displayData();
        initBg();
        updateOnLineNumber();
        restoreUI();
        VoiceRoomConfig.setIsSmallWindowStyle(false);
    }

    private void initListener() {
        initServerMessageEvent();
        initGameSocketConnectedStateListener();
        initAudioSocketConnectedStateListener();
        initOrbitErrorListener();
        initViewListener();
        initDestroyListener();
        initSocketSendGiftListener();
        initGiftEvent();
        initOnLineUsersEvent();
        VoiceServerMessageHandler.getInstance().register(enginToken);
        VoiceRoomGameSocketManager.getInstance().Start();
        musicUIController.registerEvent();
    }

    //处理socket消息 start==========================================================//
    private void initServerMessageEvent() {
        VoiceServerMessageHandler.getInstance().registerUIHandler(messageHandler);
        VoiceServerMessageHandler.getInstance().registerUIHandler(musicHandler);
    }
    //处理socket消息 end==========================================================//

    //游戏服务器连接状态监听
    private void initGameSocketConnectedStateListener() {
        //Socket断开重连的回调，失败就强制退出
        RxToolKit.Build(this, SocketEvent.ReConnectedFailedEvent.class)
                .actionFilter(value -> TextUtils.equals(value.getIntent(), SocketEngine.AUDIO))
                .action(event -> {
                    isSocketReconnectedFailed = true;
                    exitRoomCompletely();
                }).register();

        //监听是否可进行触摸操作
        RxToolKit.Build(this, VoiceEvent.GameSocketReconnectEvent.class)
                .action(event -> {
                    if (event.isReconnected) {
                        reconnect();
                    } else {
                        disconnect();
                    }

                    //唱歌时语音服务器的逻辑
                    if (VoiceRoomConfig.isIsKTV()) {
                        changeWifiNetWhenSing(event.isReconnected);
                    }

                }).register();
    }

    //语音服务器连接状态监听
    private void initAudioSocketConnectedStateListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent.class)
                .action(event -> {
                    Utils.runSafely(() -> {
                        switch (event.getAction()) {
                            case ServerEvent.RECONNECT_FROM_AUDIO_SERVER:
                                changeNetWorkState(true);
                                break;
                            case ServerEvent.DISCONNECT_FROM_AUDIO_SERVER:
                                changeNetWorkState(false);
                                break;
                        }
                    });
                }).register();
    }

    //view相关处理的回调
    private void initViewListener() {
        //点击送礼按钮展示送礼dialog
        RxToolKit.Build(this, ExhibitionsEvent.ExhibitionsByGameEvent.class)
                .action(event -> {
                    runOnUiThreadSafely(() -> {
                        VoiceDialogSendGift dialog = new
                                VoiceDialogSendGift(VoiceRoomActivity.this, VoiceRoomDataSourceManager.getInstance().getGiftUserList());
                        dialog.show();
                    });
                }).register();

        voiceRoomUIController.onOnLineNumberClicked(v -> toOnLineUsersList(false, -1));
        voiceRoomUIController.onBackPressed(v -> onBack());
    }

    //orbit语音服务器出错的回调
    private void initOrbitErrorListener() {
        RxToolKit.Build(this, OrbitError.class)
                .action(error -> runOnUiThreadSafely(() -> {
                    switch (error.getErrorCode()) {
                        case OrbitError.AUDIO_RECORD_BEEN_RACE:
                        case OrbitError.AUDIO_RECORD_NO_PERMISSION: {

                            //再加一次拦截，如果当前正在唱歌，就不弹出
                            if (!isVoiceAudioOccupy()) {
                                if (permissionDialog == null) {
                                    buildPermissionDialogCaseNoPermission();
                                    permissionDialog.show();
                                } else {
                                    if (permissionDialog.isShowing()) {
                                        break;
                                    } else {
                                        buildPermissionDialogCaseNoPermission();
                                        permissionDialog.show();
                                    }
                                }
                            }
                        }
                        ReportRobot.robot.report("AUDIO_PERMISSION_FAIL_ALL");
                        break;
                    }
                })).register();
    }

    //音频socket断开会发送此event，收到就销毁房间
    private void initDestroyListener() {
//        RxToolKit.Build(this, ViewEvent.DestroyEvent.class)
//                .action(destroyEvent -> {
//                    if (destroyEvent.isNormal()) {
//                        exitGameRoom();
//                        return;
//                    } else {
//                        exitGameRoom();
//                    }
//                }).register();
    }

    //送礼相关 start======================================================
    private void initSocketSendGiftListener() {
        RxToolKit.Build(this, SocketEvent.SocketSendGiftEvent.class)
                .action(event -> runOnUiThreadSafely(() -> RoomSocketEngineHelper.SendGiftMessageByType(event.getPeer_id(), event.getGift_type(), event.getDisplayType(),
                        result1 -> runOnUiThreadSafely(() -> {
                            if (event.getSuccessCallBack() != null) {
                                event.getSuccessCallBack().func(result1);
                            }
                        }),
                        result12 -> runOnUiThreadSafely(() -> {
                            if (event.getFailedCallBack() != null) {
                                event.getFailedCallBack().func(result12);
                            }
                        })
                ))).register();

        RxToolKit.Build(this, SocketEvent.SocketSendCardEvent.class)
                .action(event -> runOnUiThreadSafely(() -> RoomSocketEngineHelper.SendCardMessageByType(event.getPeer_id(), event.getGift_type(), event.getDisplayType(),
                        result1 -> runOnUiThreadSafely(() -> {
                            if (event.getSuccessCallBack() != null) {
                                event.getSuccessCallBack().func(result1);
                            }
                        }),
                        result2 -> runOnUiThreadSafely(() -> {
                            if (event.getFailedCallBack() != null) {
                                event.getFailedCallBack().func(result2);
                            }
                        })
                ))).register();
    }

    private void initGiftEvent() {
        //送礼
        RxToolKit.Build(this, GiftEvent.GiftForGameWrapperEvent.class)
                .action(event -> {
                    int position = event.getExhibitionPackage().getToPosition();
                    EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserByPosition(position);
                    RoomSocketEngineHelper.sendGiftChatMessageByType(
                            MessageUtils.getString(
                                    R.string.str_send_gift_msg,
                                    user.name,
                                    EffectsManager.GetGiftNameByType(event.getGift().getGiftType())
                            ),
                            event.getExhibitionPackage().getFromPosition(),
                            event.getExhibitionPackage().getToPosition(),
                            event.getGift().getGiftType(),
                            event.getGift().getGiftTicket(),
                            event.getExhibitionPackage().getDisplayType()
                    );
                }).register();

        //送卡片
        RxToolKit.Build(this, CardEvent.CardForGameWrapperEvent.class)
                .action(event -> {
                    int position = event.getExhibitionPackage().getToPosition();
                    EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserByPosition(position);
                    RoomSocketEngineHelper.sendGiftChatMessageByType(
                            MessageUtils.getString(
                                    R.string.str_send_gift_msg,
                                    user.name,
                                    EffectsManager.GetCardNameByType(event.getCard().getRealType())
                            ),
                            event.getExhibitionPackage().getFromPosition(),
                            event.getExhibitionPackage().getToPosition(),
                            event.getCard().getRealType(),
                            event.getCard().getCardTicket(),
                            event.getExhibitionPackage().getDisplayType()
                    );
                }).register();
    }
    //送礼相关 end======================================================

    //跳转到在线用户列表
    private void initOnLineUsersEvent() {
        RxToolKit.Build(this, VoiceEvent.RequestToNewChairEvent.class)
                .action(event -> toOnLineUsersList(true, event.requestChairPosition))
                .register();
    }

    //请求麦克风权限
    private void requestPermission() {

        //加个保险处理
        if (isVoiceAudioOccupy()) {
            return;
        }
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.RECORD_AUDIO)
                .onBackpressureBuffer(VoiceConstants.RX_BACK_PRESSURE)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        initVoiceEngine();
                    } else {
                        //再次加个保险
                        if (isVoiceAudioOccupy()) {
                            return;
                        }
                        RxToolKit.Emit(new OrbitError(OrbitError.AUDIO_RECORD_NO_PERMISSION));
                        ReportEventUtils.reportGetAudioFailureInKtv();
                    }
                });
    }

    //小窗模式相关 start================================================
    private void disposeByEnterMode() {
        if (VoiceRoomConfig.isIsComeInBySW()) {
            judgeShouldExit();
            showFriendRequest();
            //复现消息
            voiceRoomUIController.restoreMessage();
        }
    }

    //判断是否可以应该退出
    private void judgeShouldExit() {
        if (bean.isNeedHandleOverMaster && VoiceRoomDataSourceManager.getInstance().needHandOverMaster()) {
            onQuitRoomClicked();
        } else {
            //do nothing
        }
    }

    //展示好友请求dialog
    private void showFriendRequest() {
        List<VoiceAddFriendBean> dialogs = VoiceRoomDataSourceManager.getInstance().getAddFriendIdList();
        for (VoiceAddFriendBean bean : dialogs) {
            if (!bean.isHandle) {
                VoiceFriendDialog dialog = new VoiceFriendDialog(this, bean.id);
                dialog.show();
            }
        }
    }
    //小窗模式相关 end================================================

    //更新网络状态
    private void changeNetWorkState(boolean isConnect) {
        if (voiceRoomUIController != null) {
            voiceRoomUIController.changeAudioSocketConnectedState(isConnect);
        }
    }

    //更新房主视角UI
    private void restoreUI() {
        voiceRoomUIController.updateMasterUI();
    }

    //更新房间背景
    private void initBg() {
        voiceRoomUIController.updateBackground();
    }

    //更新在线列表
    private void updateOnLineNumber() {
        List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        if (onLineUsers != null) {
            RxToolKit.Emit(new VoiceEvent.OnLineUsersChangeEvent(onLineUsers));
            voiceRoomUIController.updateOnLineNumber(onLineUsers.size());
        }
    }

    //启动音频服务器
    private void initVoiceEngine() {
        if (VoiceRoomConfig.isIsComeInBySW()) {
            //小窗模式直接通知一下即可
            MainApplication.getInstance().getSocketEngineHelper().startPollMessage();
            //更新一下语音连接状态
            changeNetWorkState(VoiceRoomAudioSocketManager.getInstance().isConnected());
        } else {
            VoiceRoomAudioConfigBean configBean = VoiceRoomDataSourceManager.getInstance().getAudioConfig();
            VoiceRoomAudioSocketManager.getInstance().Start(configBean);
        }
    }

    //断网重连退出房间,直接关闭声音socket
    private void exitRoomCompletely() {
        VoiceRoomAudioSocketManager.getInstance().ShutDown(() -> runOnUiThreadSafely(() -> {
            if (isSocketReconnectedFailed) {
                //socket断开重连失败的提示dialog
                if (!isFinishing()) {
                    quitDialogNormal = new VoiceLeaveDialog(
                            VoiceRoomActivity.this,
                            MessageUtils.getString(R.string.dialog_hint),
                            MessageUtils.getString(R.string.str_voice_leave_room),
                            v -> exitBySendMessage(),
                            v -> exitBySendMessage(),
                            false
                    );
                    quitDialogNormal.negative(false);
                    quitDialogNormal.setCanceledOnTouchOutside(false);
                    quitDialogNormal.show();
                }
            } else {
                exitBySendMessage();
            }
        }));
    }

    //没有获取到声音权限的处理
    private void buildPermissionDialogCaseNoPermission() {
        permissionDialog = new MsgDialog(
                this,
                MessageUtils.getString(R.string.dialog_hint),
                MessageUtils.getString(R.string.audio_permission_error_in_gaming),
                v -> {
                    permissionDialog.dismiss();
                    exitBySendMessage();
                }
        );
    }

    private void exitBySendMessage() {
        VoiceRoomAudioSocketManager.getInstance().ShutDown(this::exitGameRoom);
    }

    //正常退出房间，收到语音服务器断开的消息才调用
    private void exitGameRoom() {
        runOnUiThreadSafely(() -> {
            if (quitDialogNormal != null && quitDialogNormal.isShowing()) {
                quitDialogNormal.dismiss();
            }
            quitToMain();
            finish();
        });
    }

    //进入小窗模式
    private void comeInSmallWindow() {
        EnterRoomResult.EnterRoomUserItem user = VoiceRoomDataSourceManager.getInstance().findUserByPosition(0);
        FloatUser floatUser = new FloatUser();
        if (user != null) {
            floatUser.userId = user.id;
            floatUser.userHead = user.avatar;
            floatUser.userName = user.name;
        }
        FloatWindowCommander.StartService(this, floatUser, value -> {
            if (value == 0) {//0 success    -1 failure
                VoiceServerMessageHandler.getInstance().releaseUIHandler();//清除UI处理器
                VoiceRoomConfig.setIsSmallWindowStyle(true);
                SWStyleManager.getInstance().init(enginToken);//开启小窗管理器的监听
                quitToSmallWindow();
            } else {
                //ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_not_support_the_style));
            }
        });
    }

    //跳转到用户列表界面，根据不同的情景作出不同的处理
    private void toOnLineUsersList(boolean isForceUpSeat, int newPosition) {
        VoiceToOnlineBridgeBean bean = new VoiceToOnlineBridgeBean();
        if (isForceUpSeat) {
            bean.roomTitle = MessageUtils.getString(R.string.str_choose_user);
        } else {
            if (newPosition == VoiceConstants.OVER_POSITION) {
                bean.roomTitle = MessageUtils.getString(R.string.str_choose_new_ro);
            } else {
                bean.roomTitle = VoiceRoomConfig.getRoomTitle();
            }
        }
        if (newPosition != -1) {
            bean.newPosition = newPosition;
        }
        bean.isForceUpseat = isForceUpSeat;
        bean.onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        VoiceOnlineUsersActivity.Launch(this, bean);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PLog.i(TAG, "onResume: ");
        MobclickAgent.onResume(this);
        RxToolKit.Emit(new ActivityEvent.GameActivityEvent(ActivityEvent.onResume));

        //用于重现由于onPause丢失的动画
        if (voiceRoomUIController != null) {
            voiceRoomUIController.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        PLog.i(TAG, "onPause: ");
        MobclickAgent.onPause(this);
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (!personalData.isFreeStyle()) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                RoomSocketEngineHelper.endSpeak();
            }
        } else {
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
            RoomSocketEngineHelper.endSpeak();
        }

        RxToolKit.Emit(new ActivityEvent.GameActivityEvent(ActivityEvent.onPause));
    }

    @Override
    protected void onStop() {
        super.onStop();
        PLog.i(TAG, "onStop: ");
        RxToolKit.Emit(new ActivityEvent.GameActivityEvent(ActivityEvent.onStop));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PLog.i(TAG, "onDestroy: ");
        try {
            //停止下载服务
            if (VoiceRoomConfig.isIsKTV()) {
                boolean b = MainApplication.getInstance().stopService(new Intent(MainApplication.getInstance(), MusicDownloadService.class));
                PLog.e(TAG, "stop start service: " + b);
            }
            dismissExitLoadingDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finish() {
        unRegisterRxBus();
        unRegisterUIMessageHandler();
        destroyView();
        hideKeyBoard();
        EventBus.getDefault().unregister(this);
        super.finish();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            onBack();
        }
    }

    //反注册RxBus，防止内存泄露
    private void unRegisterRxBus() {
        RxToolKit.UnRegister(this);
    }

    private void unRegisterUIMessageHandler() {
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(messageHandler);
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(musicHandler);
    }

    //销毁所有视图
    private void destroyView() {
        if (quitDialogNormal != null && quitDialogNormal.isShowing()) {
            quitDialogNormal.dismiss();
            quitDialogNormal = null;
        }

        if (quitDialogSW != null) {
            if (quitDialogSW.isShowing()) {
                quitDialogSW.dismiss();
            }
            quitDialogSW = null;
        }

        if (permissionDialog != null && permissionDialog.isShowing()) {
            permissionDialog.dismiss();
            permissionDialog = null;
        }

        if (voiceRoomUIController != null) {
            voiceRoomUIController.destroy();
            voiceRoomUIController = null;
        }

        if (musicUIController != null) {
            musicUIController.destroy();
            musicUIController = null;
        }
    }

    //尝试隐藏键盘,退出房间时强制调用
    private void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
            }
        } catch (Exception e) {
            PLog.e("hideKeyBoard:", "error:" + e.getMessage());
        }
    }

    //点击回退键
    private void onBack() {
        if (VoiceRoomConfig.isIsSupportSW() && !VoiceRoomConfig.isIsKTV()) {
            popQuitDialogSw();
        } else {
            //游戏中且自己在麦上就不能离开房间
            if (!canQuitRoomWhilePlayingFilter()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_leave));
                return;
            }
            popQuitDialogNormal();
        }
    }

    //点击退出房间
    private void onQuitRoomClicked() {
        PLog.e(TAG, "can quick:" + needHandOverMasterFilter());
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData == null) {
            tipLeaveRoom();
            PLog.e(TAG, "Malicious click!");
        } else {
            tipLeaveRoom();
        }
    }

    //提示离开房间
    private void tipLeaveRoom() {
        if (needHandOverMasterFilter()) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_choose_new_master));
            toOnLineUsersList(false, VoiceConstants.OVER_POSITION);
            if (quitDialogNormal != null) {
                quitDialogNormal.dismiss();
            }
        } else {
            exitBySendMessage();
        }
    }

    //支持小窗模式下按回退键
    private void popQuitDialogSw() {
        quitDialogSW = null;
        quitDialogSW = new VoiceQuitDialog(this);
        quitDialogSW.setOnInSamllWindowClickedListener(this::comeInSmallWindow);
        quitDialogSW.setOnQuitRoomClickedListener(() -> {
            //游戏中，不能离开
            if (!canQuitRoomWhilePlayingFilter()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_leave));
                return;
            }
            onQuitRoomClicked();
        });
        quitDialogSW.show();
    }

    //不支持小窗模式下按回退键
    private void popQuitDialogNormal() {
        quitDialogNormal = null;
        quitDialogNormal = new VoiceLeaveDialog(this,
                MessageUtils.getString(R.string.dialog_hint),
                MessageUtils.getString(R.string.leave_the_room),
                v -> {
                    if (needHandOverMasterFilter()) {
                        ToastToolKit.showShort(MessageUtils.getString(R.string.str_choose_new_master));
                        toOnLineUsersList(false, VoiceConstants.OVER_POSITION);
                        quitDialogNormal.dismiss();
                    } else {
                        exitBySendMessage();
                    }
                },
                v -> quitDialogNormal.dismiss(),
                false
        );
        quitDialogNormal.negative(true);
        quitDialogNormal.show();
    }

    //正常退出 释放资源
    private void quitToMain() {
        destroyResource();
        shutDownSocket();
    }

    //正常退出 销毁资源
    private void destroyResource() {
        PLog.i(TAG, "Exec Destroy: ");
        UnifiedBridgeHelper.SendLeaveAudioRoomAction();
        RoomSocketEngineHelper.userLeaveReportRN();
        RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(VoiceConstants.LEAVE, null, null, null);
        ReportEventUtils.reportGameLeaveIsGaming();
        releaseMessage();
        showExitLoadingDialog();
        GlobalUserState.getGlobalState().setGaming(false);
        VoiceRoomConfig.clearConfig();//退出房间，清除配置信息
        GlobalUserState.getGlobalState().setCurGameType("");
        EventBus.getDefault().post(new FinishConversationEvent());
        RxToolKit.Emit(new FinishConversationEvent());
        ReportEventUtils.reportGameLeave();
        PersonalDataHelper.setPersonalData(null);
        VoiceRoomDataSourceManager.getInstance().destroy();//销毁所有数据

        try {
            MusicCompanyConfig.destroy();
            MusicManager.Companion.getInstance().stop();
            MusicManager.Companion.getInstance().destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //正常退出 关闭两个socket
    private void shutDownSocket() {
        //停止消息监听
        VoiceServerMessageHandler.getInstance().destroy();
        //关闭游戏socket
        VoiceRoomGameSocketManager.getInstance().ShutDown(enginToken);
        VoiceRoomAudioSocketManager.getInstance().destroy();
    }

    //退出到小窗 只释放应该释放的UI资源
    private void quitToSmallWindow() {
        releaseMessage();
        showExitLoadingDialog();
        GlobalUserState.getGlobalState().setGaming(true);
        destroySWResource();
        finish();
    }

    //退出小窗需要释放的资源
    private void destroySWResource() {
        VoiceRoomDataSourceManager.getInstance().clearAddFriendDialogList();
    }

    //展示退出dialog
    private void showExitLoadingDialog() {
        if (mExitLoadingDialog != null) {
            if (mExitLoadingDialog.isShowing()) {
                mExitLoadingDialog.dismiss();
            }
            mExitLoadingDialog = null;
        }
        mExitLoadingDialog = new LoadingDialog(this);
        mExitLoadingDialog.setCanceledOnTouchOutside(false);
        mExitLoadingDialog.setMessage(MessageUtils.getString(R.string.gvoice_quit_room));
        mExitLoadingDialog.show();
    }

    //关闭退出dialog
    private void dismissExitLoadingDialog() {
        if (mExitLoadingDialog != null) {
            if (mExitLoadingDialog.isShowing()) {
                mExitLoadingDialog.dismiss();
            }
            mExitLoadingDialog = null;
        }
    }

    //重新连接
    private void reconnect() {
        VoiceRoomDataSourceManager.getInstance().addMessage(
                VoiceConstants.CODE_CONNECT,
                SystemMessageItem.createSystemMessage(
                        getResources().getColor(R.color.color_voice_net_status),
                        MessageUtils.getString(R.string.socket_reconnect)));
        if (voiceRoomUIController != null) {
            voiceRoomUIController.changeGameSocketConnectedState(true);
            voiceRoomUIController.enableTouch();
        }
    }

    //断开连接
    private void disconnect() {
        VoiceRoomDataSourceManager.getInstance().addMessage(
                VoiceConstants.CODE_DISCONNECT,
                SystemMessageItem.createSystemMessage(
                        getResources().getColor(R.color.color_voice_net_status),
                        MessageUtils.getString(R.string.socket_disconnect)));
        if (voiceRoomUIController != null) {
            voiceRoomUIController.changeGameSocketConnectedState(false);
            voiceRoomUIController.disableTouch();
        }

        //断网如果正在播放，就pause掉
        try {
            if (MusicCompanyConfig.isSing) {
                MusicManager.Companion.getInstance().pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //是否需要转让房主的过滤器
    private boolean needHandOverMasterFilter() {
        //房间人数大于1，自己是房主，网络连接，才提示转让房主
        if (VoiceRoomDataSourceManager.getInstance().getOnLineUsers().size() > 1 &&
                PositionHelper.isMaster() &&
                VoiceRoomGameSocketManager.getInstance().isConnected()) {
            return true;
        }
        return false;
    }

    //游戏中是否能离开房间
    private boolean canQuitRoomWhilePlayingFilter() {
        //正在游戏中，自己是玩家，网络连接，就不能退出房间
        if (VoiceRoomConfig.isIsPlaying() &&
                PositionHelper.isUpSeat() &&
                VoiceRoomGameSocketManager.getInstance().isConnected()) {
            return false;
        }
        return true;
    }

    /**
     * 处理在语音房间内部的 私密房邀请
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPrivateRoomInviteEvent(GlobalEvent.PrivateRoomInviteForVoiceRoom event) {
        /**
         * 语音房内收到的私密房邀请处理并点击了同意按钮
         *
         * 1。退出当前的语音房
         * 2。进入语音房房间
         */
        quitDialogNormal = new VoiceLeaveDialog(this,
                MessageUtils.getString(R.string.dialog_hint),
                MessageUtils.getString(R.string.leave_the_room),
                v -> {
                    VoiceRoomAudioSocketManager.getInstance().ShutDown(() -> {
                        runOnUiThreadSafely(() -> {
                            /**
                             * 执行页面退出
                             */
                            if (quitDialogNormal != null && quitDialogNormal.isShowing()) {
                                quitDialogNormal.dismiss();
                            }
                            quitToMain();
                            finish();

                            /**
                             * 退出语音房完毕准备进入私密房
                             */
                            EventBus.getDefault().post(new GlobalEvent.PrivateRoomInviteForNormal(event.getRoomId(), event.getRoomPassword(), event.getRoomType()));
                        });
                    });
                },
                v -> quitDialogNormal.dismiss(),
                false
        );
        quitDialogNormal.negative(true);
        quitDialogNormal.show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onKtvExit(MusicEvent.ExitKTVRoomEvent event) {
        requestPermission();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.ChatMessageHiddenEvent event) {
        if (voiceRoomUIController != null) {
            voiceRoomUIController.setChatMessagehidden(event.isHidden);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.GiftMessageHiddenEvent event) {
        if (voiceRoomUIController != null) {
            voiceRoomUIController.setGiftMessagehidden(event.isHidden);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VoiceEvent.JoinMessageHiddenEvent event) {
        if (voiceRoomUIController != null) {
            voiceRoomUIController.setJoinMessagehidden(event.isHidden);
        }
    }

    public static void Launch(Context context, String gameType, String password, VoiceToRoomBridgeBean bean) {

        Intent intent = new Intent(context, VoiceRoomActivity.class);
        TransportHelper.putTransportable(TAG, bean);

        IntentDataHelper.setUserName(intent, GlobalUserState.getGlobalState().getUserName());
        IntentDataHelper.setUserId(intent, GlobalUserState.getGlobalState().getUserId());
        IntentDataHelper.setUserSex(intent, GlobalUserState.getGlobalState().getUserSex());
        IntentDataHelper.setUserToken(intent, GlobalUserState.getGlobalState().getToken());
        IntentDataHelper.setUserAvater(intent, GlobalUserState.getGlobalState().getUserIcon());
        IntentDataHelper.setGameType(intent, gameType);
        IntentDataHelper.setRoomPassword(intent, password);
        IntentDataHelper.setUserExp(intent, GlobalUserState.getGlobalState().getUserExp());
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        context.startActivity(intent);
    }

    //消息处理类
    private AbstractSocketMessageHandler messageHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handleKickOut(VoiceMessageBean messageBean) {
            int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
            if (PositionHelper.isSelfPosition(position) || PositionHelper.isIllegalPosition(position)) {
                //房主被踢出是转让房主主动退出,不提示被踢出
                if (!PositionHelper.isMaster(position)) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.you_have_been_kick_out));
                }
                exitBySendMessage();
            }
        }

        @Override
        public void handleRestoreRoom(VoiceMessageBean messageBean) {
            String id = GlobalUserState.getGlobalState().getUserId();
            EnterRoomResult.EnterRoomUserItem mySelf = VoiceRoomDataSourceManager.getInstance().findUserById(id);
            if (mySelf == null) {
                exitBySendMessage();
            } else {
                updateOnLineNumber();
                initBg();
            }
        }

        @Override
        public void handleChangeRoomOwner(VoiceMessageBean messageBean) {

            //收到转让房主的消息并且自己是房主，表示自己要退出房间
            if (PositionHelper.isIllegalPosition()) {
                exitBySendMessage();
            }
        }

        @Override
        public void handleUpdateTitle(VoiceMessageBean messageBean) {
            initBg();
            if (VoiceRoomConfig.needChangeUI) {
                switchUI();
            }
        }

        @Override
        public void handleUpdateConfig(VoiceMessageBean messageBean) {
            voiceRoomUIController.customBG();
        }

        @Override
        public void destroy() {

        }
    };


    //=========处理音乐播放相关======================================================================//
    /**
     * 处理音乐播放
     */
    private AbstractSocketMessageHandler musicHandler = new AbstractSocketMessageHandler() {

        @Override
        public void handleJoin(VoiceMessageBean messageBean) {
            try {
                if (VoiceRoomConfig.isIsKTV()) {
                    MusicManager.Companion.getInstance().refreshTitle();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void handleBookSong(VoiceMessageBean messageBean) {
            MusicManager.Companion.getInstance().refreshTitle();
        }

        @Override
        public void handlePeakSong(VoiceMessageBean messageBean) {
            MusicManager.Companion.getInstance().refreshTitle();
        }

        @Override
        public void handleRemoveSong(VoiceMessageBean messageBean) {
            MusicManager.Companion.getInstance().refreshTitle();
        }

        @Override
        public void handlePrepareSing(VoiceMessageBean messageBean) {
            PLog.e(TAG, "handlePrepareSing.." + messageBean);
            PrepareSingBean bean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), PrepareSingBean.class);
            MusicManager.Companion.getInstance().refreshTitle();
            //如果正在唱歌，收到授权，就停止唱歌
            PrepareSingBean.PrepareSong song = bean.getSong();
            if (song != null) {
                MusicManager.Companion.getInstance().prepareSong(MusicCompanyConfig.lyricUrl);
            }
        }

        @Override
        public void handleAuthorizeSing(VoiceMessageBean messageBean) {
            MusicManager.Companion.getInstance().resetPrepare();
        }

        @Override
        public void handleStartSing(VoiceMessageBean messageBean) {
            try {
                //关闭语音服务器后才进行伴奏
                VoiceRoomAudioSocketManager.getInstance().ShutDown(() -> {
                    //changeNetWorkState(false); //语音服务器连接状态改为:未连接
                    runOnUiThreadSafely(() -> {
                        MusicManager.Companion.getInstance().refreshTitle();
                        //播放
                        MusicManager.Companion.getInstance().playSong();
                    });
                });
            } catch (Exception e) {
                PLog.e(TAG, "error when push stream: " + e.getMessage());
            }
        }

        @Override
        public void handleStopSing(VoiceMessageBean messageBean) {
            PLog.e(TAG, "handleStopSing.." + messageBean);
            try {
                MusicManager.Companion.getInstance().refreshTitle();
                MusicManager.Companion.getInstance().stop();
            } catch (Exception e) {
                PLog.e(TAG, "handleStopSing error: " + e.getMessage());
            }
        }

        @Override
        public void handleRestoreRoom(VoiceMessageBean messageBean) {
            if (VoiceRoomConfig.needChangeUI) {
                switchUI();
            }
            if (VoiceRoomConfig.isIsKTV()) {
                if (musicUIController != null) {
                    musicUIController.displayData();
                }
                MusicManager.Companion.getInstance().refreshTitle();
                if (MusicCompanyConfig.isSing) {
                    //暂停的，并且自己还能唱歌
                    if (MusicCompanyConfig.isPause && PositionHelper.canSing()) {
                        MusicManager.Companion.getInstance().resume();
                    } else {
                        VoiceRoomAudioSocketManager.getInstance().ShutDown(() -> {
                            runOnUiThreadSafely(() -> {
                                //进来时就在唱歌，即使语音服务器没连接，也设置wifi为连接
                                changeNetWorkState(true);
                                //播放
                                MusicManager.Companion.getInstance().prepareSong(MusicCompanyConfig.lyricUrl,
                                        () -> runOnUiThreadSafely(() -> MusicManager.Companion.getInstance().playSong()));
                            });
                        });
                    }
                } else {
                    MusicManager.Companion.getInstance().stop();
                    //如果准备状态就展示歌曲
                    if (MusicCompanyConfig.isPrepare) {
                        MusicManager.Companion.getInstance().prepareSong(MusicCompanyConfig.lyricUrl);
                    }
                }
            } else {
                //no egg things to do
                try {
                    MusicManager.Companion.getInstance().destroy();
                } catch (Exception e) {
                    requestPermission();
                }
            }
        }

        @Override
        public void destroy() {

        }

        @Override
        public void handleUpSeat(VoiceMessageBean messageBean) {
            //如果是房主，应该出现"开始"或"播放按钮"
            if (PositionHelper.isMaster()) {
                if (VoiceRoomConfig.isIsKTV()) {
                    if (MusicCompanyConfig.isPrepare && !MusicCompanyConfig.hasAuthorized()) {
                        MusicManager.Companion.getInstance().showNextButton();
                    } else if (MusicCompanyConfig.isSing) {
                        MusicManager.Companion.getInstance().showBoot();
                    }
                }
            }
        }
    };

    /**
     * 展示直播数据
     */
    private void showLive() {
        if (MusicCompanyConfig.isSing) {
            RxPermissions rxPermissions = new RxPermissions(this);
            rxPermissions.request(Manifest.permission.RECORD_AUDIO)
                    .onBackpressureBuffer(VoiceConstants.RX_BACK_PRESSURE)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aBoolean -> {
                        if (aBoolean) {
                            //拿到权限才去播放
                            MainApplication.getInstance().getSocketEngineHelper().startPollMessage();
                            VoiceRoomAudioSocketManager.getInstance().ShutDown(() -> {
                                runOnUiThreadSafely(() -> {
                                    //进来时就在唱歌，即使语音服务器没连接，也设置wifi为连接
                                    changeNetWorkState(true);
                                    //播放
                                    MusicManager.Companion.getInstance().prepareSong(MusicCompanyConfig.lyricUrl,
                                            () -> runOnUiThreadSafely(() -> MusicManager.Companion.getInstance().playSong()));
                                });
                            });
                        } else {
                            RxToolKit.Emit(new OrbitError(OrbitError.AUDIO_RECORD_NO_PERMISSION));
                        }
                    });
        } else {
            //如果没有直播，就申请音频权限
            requestPermission();

            //如果准备状态就展示歌曲
            if (MusicCompanyConfig.isPrepare) {
                MusicManager.Companion.getInstance().prepareSong(MusicCompanyConfig.lyricUrl);
            }
        }
    }

    /**
     * 开启伴奏主动关闭语音服务器，后续接收不到语音服务器的监听，就监听游戏服务器
     */
    private void changeWifiNetWhenSing(boolean isConnected) {
        //只有房间在唱歌的时候，才使用这个方法，其余情况走原有逻辑
        if (MusicCompanyConfig.isSing) {
            changeNetWorkState(isConnected);
        }
    }


    /**
     * 判断当前语音通道是否被占用
     */
    private boolean isVoiceAudioOccupy() {
        //如果是ktv房间，且正在唱歌，语音通道就处于占用状态，就直接进行拦截
        return VoiceRoomConfig.isIsKTV() && MusicCompanyConfig.isSing;
    }

}
