package orangelab.project.voice.privateroom.view

import android.graphics.PointF
import android.os.Looper
import android.view.View
import android.widget.Button
import com.R
import orangelab.project.game.view.RoundProgressBar
import orangelab.project.common.utils.CountDownTaskManager
import orangelab.project.common.utils.SafeHandler
import orangelab.project.common.effect.EffectsManager

/**
 *game_werewolf
 *2018/3/26 下午4:59
 *Mystery
 */
class PrivateRoomLikeViewImpl : PrivateRoomLikeView, View.OnClickListener {

    class RestoreData {
        var remainCountTime = 0L
        var restoreEnable = true
        var remainCountDownSecond = 0

        constructor(remainCountTime: Long, restoreEnable: Boolean, remainCountDownSecond: Int) {
            this.remainCountTime = remainCountTime
            this.restoreEnable = restoreEnable
            this.remainCountDownSecond = remainCountDownSecond
        }
    }

    private var mLike: Button
    private var mLikeProgress: RoundProgressBar
    private var mLikeDelaySeconds = 120
    private var mCurCountDown = 0

    private var mLikeViewObserver: PrivateRoomLikeViewObserver? = null
    private var mCountDownTask: CountDownTaskManager.CountDownTask? = null
    private var mHandler: SafeHandler = SafeHandler(Looper.getMainLooper())

    private var mEnable = true;

    constructor(rootView: View, observer: PrivateRoomLikeViewObserver) {
        mLike = rootView.findViewById<Button>(R.id.id_room_like_button) as Button
        mLikeProgress = rootView.findViewById<Button>(R.id.id_room_like_progress) as RoundProgressBar
        mLikeViewObserver = observer
        mLike.setOnClickListener(this)
    }

    override fun startLoad() {
        like(mLikeDelaySeconds, false)
    }

    private fun like(second: Int, needNotify: Boolean) {
        /**
         * 点击了like
         */
        mHandler.postSafely {
            if (mEnable) {
                mEnable = false
                mLike.isEnabled = mEnable

                val likeDelay = (1000 * second).toLong()

                if (needNotify) {
                    showAnim()
                    mLikeViewObserver?.onLike()
                }
                /**
                 * 开始执行倒计时
                 */
                mCountDownTask?.cancelTask()
                mCountDownTask?.destroyTask()

                mLikeProgress.max = likeDelay.toInt()

                mCountDownTask = CountDownTaskManager.CountDownTask(likeDelay, 16, second, object : CountDownTaskManager.CountDownTaskListener {
                    override fun onTick(seconds: Int) {
                        mHandler.postSafely {
                            mCurCountDown = (seconds / 1000)
                            val dur = mLikeProgress.getMax() - seconds
                            if (dur >= 0) {
                                mLikeProgress.setProgress(dur)
                            }
                        }
                    }

                    override fun onStart(startSeconds: Int) {
                        mHandler.postSafely {
                            mLikeProgress.visibility = View.VISIBLE
                        }
                    }

                    override fun onFinish() {
                        mHandler.postSafely {
                            mLikeProgress.visibility = View.GONE
                            mCountDownTask?.cancelTask()
                            mCountDownTask?.destroyTask()
                            mEnable = true
                            mLike.isEnabled = mEnable
                        }
                    }
                })
                mCountDownTask?.startTask()
            } else {
                return@postSafely
            }
        }
    }

    private fun like() {
        like(mLikeDelaySeconds, true)
    }

    override fun restore(restoreData: RestoreData?) {
        restoreData ?: return
        val restoreEnable = restoreData.restoreEnable
        val restoreTime = restoreData.remainCountTime
        val restoreCountDown = restoreData.remainCountDownSecond
        val curTime = System.currentTimeMillis()
        val delta = (restoreCountDown - ((curTime - restoreTime) / 1000)).toInt()
        if (delta <= 0) {
            if (restoreEnable) {

            } else {
                mEnable = true
            }
        } else {
            mEnable = true
            like(delta, false)
            return
        }
    }

    override fun getRestore(): RestoreData {
        return RestoreData(System.currentTimeMillis(), mEnable, mCurCountDown)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.id_room_like_button -> {
                like()
            }
        }
    }

    private fun showAnim() {
        /**
         * 播放一个like的特效
         */
        val location = IntArray(2)

        mLike.getLocationOnScreen(location)

        val startPoint = PointF(location[0].toFloat(), location[1].toFloat())

        val endPoint = PointF(startPoint.x, 0f)

        EffectsManager.DispatchPraiseAnimEvent(mLike.context, startPoint, endPoint, mLike.width, mLike.height)
    }

    override fun destroy() {
        mCountDownTask?.cancelTask()
        mCountDownTask?.destroyTask()
        mLikeViewObserver = null
        mHandler.release()
    }


}