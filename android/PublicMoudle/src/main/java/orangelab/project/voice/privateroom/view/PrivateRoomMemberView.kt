package orangelab.project.voice.privateroom.view

import android.graphics.PointF
import orangelab.project.voice.manager.VoiceEmotionManager
import com.toolkit.action.Destroyable
import orangelab.project.voice.manager.SmallGameManager

/**
 *game_werewolf
 *2018/3/21 下午3:15
 *Mystery
 */
interface PrivateRoomMemberView : Destroyable {

    fun setObserver(privateRoomMemberViewObserver: PrivateRoomMemberViewObserver)

    fun turnIntoNoMember()

    fun turnIntoLock()

    fun turnIntoUnLock()

    fun refresh()

    fun refreshForce()

    fun refreshMemberState()

    fun turnIntoMember()

    fun turnIntoSpeaking(isSpeaking: Boolean)

    fun turnIntoFreeStyle(isFreeStyle: Boolean)

    fun turnIntoBanMic(isBanMic: Boolean)

    fun getMemberPositionInScreen(): PointF

    fun getMemberWidth(): Int

    fun getMemberHeight(): Int

}