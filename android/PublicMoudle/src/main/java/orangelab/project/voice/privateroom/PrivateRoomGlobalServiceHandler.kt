package orangelab.project.voice.privateroom

import android.text.TextUtils
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.UIActuator
import com.datasource.GlobalUserState
import com.toolkit.action.Printable
import orangelab.project.common.BaseGlobalServiceHandler
import orangelab.project.common.GlobalFloatWindowManager
import orangelab.project.common.IGlobalService
import orangelab.project.common.engine.GlobalSocketConstant
import orangelab.project.common.utils.MessageUtils
import orangelab.project.minigame.model.GlobalSocketJoinRoom
import orangelab.project.voice.privateroom.model.PrivateRoomIntviuRefused
import org.greenrobot.eventbus.EventBus

/**
 *game_werewolf
 *2018/4/16 上午11:11
 *Mystery
 */
class PrivateRoomGlobalServiceHandler : BaseGlobalServiceHandler, Printable<String> {


    constructor(globalService: IGlobalService?) : super(globalService)

    override fun isCareType(type: String?): Boolean {
        return TextUtils.equals(type, GlobalSocketConstant.PRIVATE_ROOM_SERVER_TYPE)
                || TextUtils.equals(type, GlobalSocketConstant.PRIVATE_ROOM_TYPE)

    }

    override fun handleMessage(type: String?, payload: String?) {
        printInfo("handleMessage type= ${type},payload ${payload}")
        try {
            if (TextUtils.equals(type, GlobalSocketConstant.INVITED_SUCCESS)) {
                /**
                 * 私密房邀请成功
                 */
                UIActuator.post { ToastToolKit.showShort(R.string.mini_game_intviu_success) }
                return
            }
            if (TextUtils.equals(type, GlobalSocketConstant.INVITE_VAILD)) {
                UIActuator.post { ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_intviu_vaild)) }
                return
            }
            if (TextUtils.equals(type, GlobalSocketConstant.INVITE_REFUSE_SERVER)) {
                val data = GsonHelper.getGson().fromJson(payload, PrivateRoomIntviuRefused::class.java)
                if (TextUtils.equals(data.target_user?.id, GlobalUserState.getGlobalState().userId)) {
                    UIActuator.post {
                        ToastToolKit.showLong(R.string.str_private_room_refused)
                    }
                    return
                }
                UIActuator.post {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_private_room_chat_intviu_be_refused, data.target_user?.name))
                    return@post
                }
                return
            }
            if (TextUtils.equals(type, GlobalSocketConstant.JOIN_ROOM)) {
                if (GlobalUserState.getGlobalState().isGaming()) {
                    /**
                     * 正在游戏中无法处理游戏邀请
                     */
                } else {
                    val room = GsonHelper.getGson().fromJson(payload, GlobalSocketJoinRoom::class.java)
                    EventBus.getDefault().post(room)
                }
                return
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun printInfo(info: String?) {
        PLog.i("PrivateRoomGlobalServiceHandler", info)
    }
}