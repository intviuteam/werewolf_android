package orangelab.project.voice.utils;

import android.content.res.Resources;

import orangelab.project.MainApplication;
import com.androidtoolkit.PLog;

import com.androidtoolkit.view.ScreenUtils;


/**
 * Created by lloydfinch on 21/11/2017.
 */

public class SystemScreenUtils {

    private static final String TAG = "SystemScreenUtils";

    public static int getStatusBarHeight() {
        try {
            Resources resources = MainApplication.getInstance().getResources();
            int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
            int height = resources.getDimensionPixelSize(resourceId);
            PLog.e(TAG, "Status height:" + height);
            return height;
        } catch (Exception e) {
            PLog.e(TAG, "ERROR: " + e.getMessage());
        }
        return ScreenUtils.dip2px(30);
    }
}
