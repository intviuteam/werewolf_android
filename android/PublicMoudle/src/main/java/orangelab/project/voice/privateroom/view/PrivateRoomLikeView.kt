package orangelab.project.voice.privateroom.view

import com.toolkit.action.Destroyable

/**
 * game_werewolf
 * 2018/3/26 下午4:58
 * Mystery
 */

interface PrivateRoomLikeView : Destroyable {

    fun startLoad();

    fun restore(restoreData: PrivateRoomLikeViewImpl.RestoreData?)

    fun getRestore(): PrivateRoomLikeViewImpl.RestoreData
}
