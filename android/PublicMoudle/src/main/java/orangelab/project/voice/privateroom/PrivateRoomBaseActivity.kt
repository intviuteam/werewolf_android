package orangelab.project.voice.privateroom

import android.os.Bundle
import orangelab.project.common.activity.SafeActivity
import orangelab.project.voice.manager.VoiceRoomCommander

/**
 * game_werewolf
 * 2018/3/26 上午10:35
 * Mystery
 */

open class PrivateRoomBaseActivity : SafeActivity() {

    override fun onActivityWindowInitFinish() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        VoiceRoomCommander.ActivityManager.RegisterActivity(this)
    }

    override fun closeActivity() {
        VoiceRoomCommander.ActivityManager.UnRegisterActivity(this)
        finish()
    }


}
