package orangelab.project.voice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.R;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 14/10/2017.
 */

public class VoiceShareDialog extends Dialog {

    private LinearLayout llContentViewContainer;
    private View btnCancel;

    private Context mContext;

    public VoiceShareDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceShareDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_share, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        llContentViewContainer = (LinearLayout) contentView.findViewById(R.id.ll_content_view_container);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void setOnlyButtonListener(View.OnClickListener onlyButtonListener) {
        btnCancel.setOnClickListener(onlyButtonListener);
    }

    private void initListener() {
        btnCancel.setOnClickListener(v -> dismiss());
    }

    public void changeContentView(int layoutResourceId) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(LayoutInflater.from(mContext).inflate(layoutResourceId, null));
    }

    public void changeContentView(View contentView) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(contentView);
    }
}
