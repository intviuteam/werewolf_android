package orangelab.project.voice.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import com.R
import orangelab.thirdparty.qiniu.QiNiuManager
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.view.ScreenUtils
import com.datasource.GlobalUserState
import orangelab.project.common.activity.SafeActivity
import orangelab.project.common.manager.ApiManager
import orangelab.project.common.tool.WrapImagePicker
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.common.view.ActionSheetDialog
import orangelab.project.common.view.TitleView
import orangelab.project.voice.api.VoiceApiManager
import orangelab.project.voice.manager.VoiceRoomCommander
import orangelab.project.voice.model.VoiceRoomBackGroundApiResult
import orangelab.project.voice.model.VoiceRoomBackGroundItem
import orangelab.project.voice.privateroom.PrivateRoomTools
import com.networktoolkit.transport.Constant
import com.qiniu.android.http.ResponseInfo
import com.qiniu.android.storage.UpCompletionHandler
import com.qiniu.android.storage.UploadOptions
import com.tbruyelle.rxpermissions.RxPermissions
import com.toolkit.action.ApiResult
import org.json.JSONObject
import java.net.URL
import kotlin.collections.ArrayList

/**
 * game_werewolf
 * 2018/3/26 上午10:15
 * Mystery
 */

class VoiceRoomBackGroundActivity : SafeActivity() {

    companion object {

        val UPLOAD_WIDTH: Int = PrivateRoomTools.DEFAULT_BACKGROUND_WIDTH
        val UPLOAD_HEIGHT: Int = PrivateRoomTools.DEFAULT_BACKGROUND_HEIGHT

        fun Launch(context: Context, roomId: String, roomType: String) {
            if (TextUtils.isEmpty(roomId) || TextUtils.isEmpty(roomType)) {
                return
            }
            val intent = Intent(context, VoiceRoomBackGroundActivity::class.java)
            intent.putExtra(Constant.PARAMETER_ROOM_ID, roomId)
            intent.putExtra(Constant.TYPE, roomType)
            context.startActivity(intent)
        }
    }

    private lateinit var mTitle: TitleView
    private lateinit var mRoomId: String
    private lateinit var mRoomType: String

    private lateinit var mDefaultGridView: GridView
    private lateinit var mDefaultAdapter: Adapter
    private lateinit var mSystemGridView: GridView
    private lateinit var mSystemAdapter: Adapter
    private lateinit var mCustomGridView: GridView
    private lateinit var mCustomAdapter: Adapter

    private lateinit var mUpLoadTimeController: UpLoadTimeController

    private var mSelectItem: VoiceRoomBackGroundItem? = null
    private var mCurUpLoadingItem: VoiceRoomBackGroundItem? = null
    private var mFirstCustomUpLoadItem: VoiceRoomBackGroundItem? = null

    private lateinit var mPermission: RxPermissions
    private var mActionSheetDialog: ActionSheetDialog? = null
    private var mImagePicker = WrapImagePicker()
    private var mUpLoadCancel = false
    private var mCanExit = true

    private var mOldImageImage: VoiceRoomBackGroundApiResult.CurImageItem? = null

    override fun onActivityWindowInitFinish() {

    }

    override fun closeActivity() {
        mUpLoadCancel = true
        VoiceRoomCommander.ActivityManager.UnRegisterActivity(this)
        finish()
    }

    private fun goBack() {
        if (mCanExit) {
            closeActivity()
        } else {
            saveChange()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPermission = RxPermissions(this)
        VoiceRoomCommander.ActivityManager.RegisterActivity(this)

        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color)

        mImagePicker.initImageConfig(UPLOAD_WIDTH, UPLOAD_HEIGHT)

        mRoomId = intent.getStringExtra(Constant.PARAMETER_ROOM_ID)
        mRoomType = intent.getStringExtra(Constant.TYPE)

        setContentView(R.layout.layout_private_room_background)
        mTitle = findViewById(R.id.title) as TitleView
        mTitle.setTitle(R.string.str_private_room_background_title)
        mTitle.setActionBack {
            goBack()
        }

        mDefaultGridView = findViewById(R.id.default_gridview) as GridView
        mCustomGridView = findViewById(R.id.custom_bg_gridview) as GridView
        mSystemGridView = findViewById(R.id.backgroud_gridview) as GridView

        /**
         * 处理监听事件,处理选中以及更换当前图片
         */
        mDefaultGridView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mUpLoadTimeController.isPermission()) {
                    noActionPermission()
                    return@onItemClick
                }
                mDefaultAdapter.select(position)
                mSelectItem = mDefaultAdapter.getItem(position)
                mCustomAdapter.clearSelected()
                mSystemAdapter.clearSelected()
            }
        })

        mCustomGridView.setOnItemLongClickListener { parent, view, position, id ->
            if (!mUpLoadTimeController.isPermission()) {
                noActionPermission()
                return@setOnItemLongClickListener true
            }

            val curSelectItem = mCustomAdapter.getItem(position)

            if (curSelectItem.selected) {
                ToastToolKit.showShort(R.string.str_private_room_background_selected_delete_error)
                return@setOnItemLongClickListener true
            }

            if (!curSelectItem.isLoading && !curSelectItem.isAddPlaceHolder) {
                beginDeletePhoto(curSelectItem)
            }
            return@setOnItemLongClickListener true
        }

        mCustomGridView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mUpLoadTimeController.isPermission()) {
                    noActionPermission()
                    return@onItemClick
                }
                val temp = mCustomAdapter.getItem(position)
                if (temp.isLoading) {
                    /**
                     * 点击的图片正在上传
                     */
                    return
                }
                if (temp.isAddPlaceHolder) {
                    /**
                     * 点击了添加图片
                     */
                    if (mCurUpLoadingItem != null) {
                        ToastToolKit.showShort(R.string.str_upload_cur_is_uploading)
                        return
                    }
                    if (mUpLoadTimeController.canUpLoad()) {
                        beginSelectUpLoadPhoto()
                    } else {
                        /**
                         * 次数到了上限无法传输
                         */
                        ToastToolKit.showShort(R.string.str_upload_time_limit)
                    }
                    return
                }
                mCustomAdapter.clearSelected()
                mCustomAdapter.select(position)
                mSelectItem = mCustomAdapter.getItem(position)
                mDefaultAdapter.clearSelected()
                mSystemAdapter.clearSelected()
            }
        })
        mSystemGridView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mUpLoadTimeController.isPermission()) {
                    noActionPermission()
                    return@onItemClick
                }
                mSystemAdapter.clearSelected()
                mSystemAdapter.select(position)
                mSelectItem = mSystemAdapter.getItem(position)
                mDefaultAdapter.clearSelected()
                mCustomAdapter.clearSelected()
            }
        })

        VoiceApiManager.GetRoomBackGroundData(mRoomId, ApiResult { result, e ->
            runOnUiThreadSafely({
                mCanExit = false
                if (e == null) {
                    mOldImageImage = result.current
                    /**
                     * 得到房间背景的数据
                     */
                    var selectUrl = ""
                    try {
                        if (result.current.isBlur) {
                            selectUrl = GlobalUserState.getGlobalState().userIcon
                        } else {
                            selectUrl = result.current.url
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    mUpLoadTimeController = UpLoadTimeController(result.upload_limit, result.upload_times)

                    /**
                     * 1.填充自己的默认背景
                     */
                    val defaultList = ArrayList<VoiceRoomBackGroundItem>()
                    val defaultItem = VoiceRoomBackGroundItem()
                    defaultItem.bgNeedBlur = true
                    defaultItem.url = GlobalUserState.getGlobalState().userIcon
                    if (TextUtils.equals(defaultItem.url, selectUrl)) {
                        defaultItem.selected = true
                        mSelectItem = defaultItem
                    }
                    defaultItem.type = VoiceRoomBackGroundApiResult.BLUR_AVATAR
                    defaultList.add(defaultItem)
                    mDefaultAdapter = Adapter(this, defaultList)
                    mDefaultGridView.adapter = mDefaultAdapter

                    /**
                     * 2.填充自定义的背景
                     */
                    val customList = ArrayList<VoiceRoomBackGroundItem>()
                    if (result.images != null && result.images.size > 0) {
                        result.images.forEach {
                            val tempCustom = VoiceRoomBackGroundItem()
                            tempCustom.bgNeedBlur = false
                            tempCustom.url = it.url
                            tempCustom.id = it.image_id
                            tempCustom.type = VoiceRoomBackGroundApiResult.CUSTOM
                            if (TextUtils.equals(tempCustom.url, selectUrl)) {
                                tempCustom.selected = true
                                mSelectItem = tempCustom
                            }
                            customList.add(tempCustom)
                        }
                        mFirstCustomUpLoadItem = customList.get(0)
                    }
                    val tempAdd = VoiceRoomBackGroundItem()
                    tempAdd.isAddPlaceHolder = true
                    tempAdd.bgNeedBlur = false
                    tempAdd.type = VoiceRoomBackGroundApiResult.CUSTOM
                    tempAdd.selected = false
                    tempAdd.url = ""
                    customList.add(tempAdd)
                    mCustomAdapter = Adapter(this, customList)
                    mCustomGridView.adapter = mCustomAdapter

                    /**
                     *  3.填充系统背景
                     */
                    val systemList = ArrayList<VoiceRoomBackGroundItem>()
                    if (result.default_bg != null && result.default_bg.size > 0) {
                        result.default_bg.forEach {
                            val tempItem = VoiceRoomBackGroundItem()
                            tempItem.url = it
                            if (TextUtils.equals(tempItem.url, selectUrl)) {
                                tempItem.selected = true
                                mSelectItem = tempItem
                            }
                            tempItem.bgNeedBlur = false
                            tempItem.id = ""
                            tempItem.type = VoiceRoomBackGroundApiResult.DEFAULT
                            systemList.add(tempItem)
                        }
                    }
                    mSystemAdapter = Adapter(this, systemList)
                    mSystemGridView.adapter = mSystemAdapter

                } else {
                    ToastToolKit.showShort(e.message)
                    closeActivity()
                }
            })
        })

        mImagePicker.bindCallBack(object : WrapImagePicker.EmptyImagePickerObserver() {
            override fun onPicked(path: String?) {
                runOnUiThreadSafely({

                    /**
                     * 选中了图片,执行上传操作
                     */
                    if (TextUtils.isEmpty(path)) {
                        return@runOnUiThreadSafely
                    }

                    val item = VoiceRoomBackGroundItem()
                    item.selected = false
                    item.localUrl = path!!
                    item.url = ""
                    item.type = VoiceRoomBackGroundApiResult.CUSTOM
                    item.isLoading = true

                    if (TextUtils.equals(mRoomType, Constant.GAME_TYPE_PRIVACY) && mFirstCustomUpLoadItem != null) {
                        mCustomAdapter.replaceFirst(item)
                    } else {
                        mCustomAdapter.addFirst(item)
                    }

                    mCurUpLoadingItem = item

                    var uploadPath = ""
                    /**
                     * 进行一个替换字符串的操作
                     */
                    if (path!!.contains("file://")) {
                        uploadPath = path!!.replace("file://", "")
                    }
                    ApiManager.RefreshUpLoadToken(mRoomId, ApiResult { result, e ->
                        runOnUiThreadSafely({
                            if (e == null) {
                                /**
                                 * 准备开始上传图片
                                 */
                                var key: String? = null
                                if (!TextUtils.isEmpty(result.saveKey)) {
                                    key = result.saveKey
                                }
                                QiNiuManager.UpLoadImage(uploadPath, key, result.token, object : UpCompletionHandler {
                                    override fun complete(key: String?, info: ResponseInfo?, response: JSONObject?) {
                                        runOnUiThreadSafely({
                                            if (info?.isOK ?: false) {
                                                /**
                                                 * 上传成功
                                                 */
                                                val urlString = response?.optString("url")
                                                val keyString = response?.optString("key")

                                                printLog("upload finish url=${urlString} key=${keyString}")
                                                if (TextUtils.isEmpty(urlString)) {
                                                    ToastToolKit.showShort(R.string.str_upload_failed)
                                                    uploadItemFailed()
                                                    mCurUpLoadingItem = null
                                                    return@runOnUiThreadSafely
                                                }

                                                if (TextUtils.isEmpty(keyString)) {
                                                    ToastToolKit.showShort(R.string.str_upload_failed)
                                                    uploadItemFailed()
                                                    mCurUpLoadingItem = null
                                                    return@runOnUiThreadSafely
                                                }

                                                mCurUpLoadingItem!!.url = urlString!!
                                                mCurUpLoadingItem!!.id = keyString!!

                                                /**
                                                 * 上传成功 开始向服务器汇报
                                                 */
                                                if (TextUtils.equals(mRoomType, Constant.GAME_TYPE_PRIVACY) && mFirstCustomUpLoadItem != null) {
                                                    VoiceApiManager.ReplaceBackGroundImage(mRoomId, mFirstCustomUpLoadItem!!.id, urlString, keyString, { result, e ->
                                                        runOnUiThreadSafely({
                                                            if (e == null) {
                                                                ToastToolKit.showShort(R.string.str_upload_success)
                                                                uploadItemFinish()
                                                            } else {
                                                                ToastToolKit.showShort(e.message)
                                                                uploadItemFailed()
                                                            }
                                                        })
                                                    })
                                                } else {
                                                    VoiceApiManager.AddBackGroundImage(mRoomId, keyString!!, urlString!!, { result, e ->
                                                        runOnUiThreadSafely({
                                                            if (e == null) {
                                                                ToastToolKit.showShort(R.string.str_upload_success)
                                                                uploadItemFinish()
                                                            } else {
                                                                ToastToolKit.showShort(e.message)
                                                                uploadItemFailed()
                                                            }
                                                        })
                                                    })
                                                }
                                            } else {
                                                /**
                                                 * 上传失败
                                                 */
                                                ToastToolKit.showShort(R.string.str_upload_failed)
                                                uploadItemFailed()
                                                return@runOnUiThreadSafely
                                            }
                                        })
                                    }
                                }, UploadOptions(null, null, false, { key, percent ->
                                    //.....
                                }, { mUpLoadCancel }))
                            } else {
                                ToastToolKit.showShort(e.message)
                            }
                        })
                    })
                })
            }
        })
    }

    private fun uploadItemFinish() {
        mUpLoadTimeController.upLoadSuccess()
        if (mCurUpLoadingItem != null) {
            mCustomAdapter.uploadDone(mCurUpLoadingItem)
            mSystemAdapter.clearSelected()
            mCustomAdapter.clearSelected()
            mDefaultAdapter.clearSelected()
            mCustomAdapter.select(mCurUpLoadingItem!!)
            mSelectItem = mCurUpLoadingItem!!
            mFirstCustomUpLoadItem = mCurUpLoadingItem!!
            mCurUpLoadingItem = null
        }
    }

    private fun uploadItemFailed() {
        if (mCurUpLoadingItem != null) {
            if (TextUtils.equals(mRoomType, Constant.GAME_TYPE_PRIVACY) && mFirstCustomUpLoadItem != null) {
                mCustomAdapter.replaceFirst(mFirstCustomUpLoadItem!!)
                mSystemAdapter.clearSelected()
                mCustomAdapter.clearSelected()
                mDefaultAdapter.clearSelected()
                mCustomAdapter.select(mFirstCustomUpLoadItem!!)
            } else {
                mCustomAdapter.remove(mCurUpLoadingItem!!)
            }
            mCurUpLoadingItem = null
        }
    }

    private fun isChangeBackGround(): Boolean {
        try {
            val oldImagePath = Uri.parse(mOldImageImage?.url ?: "").path
            val newImagePath = Uri.parse(mSelectItem?.url ?: "").path
            if (TextUtils.equals(oldImagePath, newImagePath) && TextUtils.equals(mOldImageImage?.type, mSelectItem?.type)) {
                /**
                 * 认为没有变化
                 */
                return false
            } else {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    private fun saveChange() {
        if (mSelectItem != null) {
            /**
             * 判断当前的选择是否和之前的选择有变化
             */
            if (isChangeBackGround()) {
                /**
                 * 发送当前选中的图片
                 */
                RoomSocketEngineHelper.sendUpdateBackGround(mSelectItem!!.url, mSelectItem!!.type, mSelectItem!!.id)
            }
            closeActivity()
        } else {
            ToastToolKit.showShort(R.string.str_private_room_background_need_selected)
            return
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goBack()
    }

    override fun finish() {
        super.finish()
        if (mActionSheetDialog?.isShowing ?: false) {
            mActionSheetDialog?.dismiss()
        }
        mImagePicker.destroy()
    }

    fun beginDeletePhoto(data: VoiceRoomBackGroundItem) {
        if (mActionSheetDialog?.isShowing ?: false) {
            mActionSheetDialog?.dismiss()
        }
        if (mActionSheetDialog != null && mActionSheetDialog!!.isShowing) {
            mActionSheetDialog!!.dismiss()
        }
        mActionSheetDialog = ActionSheetDialog(this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                .addSheetItem(MessageUtils.getString(R.string.delete), ActionSheetDialog.SheetItemColor.Blue) { which ->
                    if (data != null) {
                        VoiceApiManager.RemoveBackGroundImage(mRoomId, data!!.id, data!!.url, { result, e ->
                            runOnUiThreadSafely({
                                if (e == null) {
                                    mCustomAdapter.remove(data!!)
                                } else {
                                    ToastToolKit.showShort(e.message)
                                }
                            })
                        })
                    }
                }
        mActionSheetDialog?.show()
    }

    fun beginSelectUpLoadPhoto() {
        mImagePicker.startPick(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mImagePicker.onActivityResult(this, requestCode, resultCode, data)
    }

    private fun printLog(info: String) {
        PLog.i("VoiceRoomBackGroundActivity", info)
    }

    private fun noActionPermission() {
        ToastToolKit.showShort(R.string.str_private_room_custom_background_no_permission)
    }

    private class Adapter : BaseAdapter {
        companion object {
            val mWidth = ScreenUtils.dip2px(80f)
            val mHeight = ScreenUtils.dip2px(80f)
        }

        private var mContext: Context
        private var mData: MutableList<VoiceRoomBackGroundItem>

        constructor(mContext: Context, mData: MutableList<VoiceRoomBackGroundItem>) : super() {
            this.mContext = mContext
            this.mData = mData
        }

        fun select(position: Int) {
            if (mData.get(position)?.selected ?: false) {
                return;
            }
            mData.get(position)?.selected = !mData.get(position)?.selected
            notifyDataSetChanged()
        }

        fun clearSelected() {
            mData.forEach {
                it.selected = false
            }
            notifyDataSetChanged()
        }

        fun select(item: VoiceRoomBackGroundItem) {
            item.selected = true
            notifyDataSetChanged()
        }

        fun replaceFirst(item: VoiceRoomBackGroundItem) {
            mData.set(0, item)
            notifyDataSetChanged()
        }

        fun addFirst(item: VoiceRoomBackGroundItem) {
            mData.add(0, item)
            notifyDataSetChanged()
        }

        fun add(item: VoiceRoomBackGroundItem) {
            mData.add(item)
            notifyDataSetChanged()
        }

        fun remove(item: VoiceRoomBackGroundItem) {
            item.isLoading = false
            mData.remove(item)
            notifyDataSetChanged()
        }

        fun uploadDone(item: VoiceRoomBackGroundItem?) {
            if (item == null) {
                return
            }
            mData.forEach {
                if (TextUtils.equals(item?.id, it.id)) {
                    item.isLoading = false
                    notifyDataSetChanged()
                    return@forEach
                }
            }
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var holder: ViewHolder? = null
            var view = convertView
            if (view == null) {
                view = View.inflate(mContext, R.layout.layout_private_room_background_item, null)
                holder = ViewHolder()
                holder!!.background = view!!.findViewById(R.id.item_background_iv) as ImageView
                holder!!.check = view!!.findViewById(R.id.item_check) as ImageView
                holder!!.add = view!!.findViewById(R.id.item_add)
                holder!!.progress = view!!.findViewById(R.id.progress)
                holder!!.progressContainer = view!!.findViewById(R.id.progress_container)
                view!!.setTag(holder)
            } else {
                holder = view!!.getTag() as ViewHolder
            }

            val data = getItem(position)

            if (data.isAddPlaceHolder) {
                holder.check?.visibility = View.GONE
                holder.add?.visibility = View.VISIBLE
                holder.background?.visibility = View.GONE
                holder.progress?.visibility = View.GONE
                holder.progressContainer?.visibility = View.GONE
            } else {
                holder.check?.visibility = View.VISIBLE
                holder.add?.visibility = View.GONE
                holder.background?.visibility = View.VISIBLE
                if (data.resourceId == -1) {
                    if (TextUtils.isEmpty(data.localUrl)) {
                        if (data.bgNeedBlur) {
                            VoiceRoomCommander.BackGroundManager.BlurBackGround(data.url ?: "", holder.background!!, null, mWidth, mHeight)
                        } else {
                            PicassoUtils.loadImage(holder.background!!.context, data.url, holder.background!!, mWidth, mHeight)
                        }
                    } else {
                        PicassoUtils.loadImage(holder.background!!.context, data.localUrl, holder.background!!, mWidth, mHeight)
                    }
                } else {
                    if (data.bgNeedBlur) {
                        VoiceRoomCommander.BackGroundManager.BlurBackGround(data.resourceId, holder.background!!, null, mWidth, mHeight)
                    } else {
                        PicassoUtils.loadImage(holder.background!!.context, data.resourceId, holder.background!!, mWidth, mHeight)
                    }
                }
                if (data.isLoading) {
                    holder.progress!!.visibility = View.VISIBLE
                    holder.progressContainer!!.visibility = View.VISIBLE
                    holder.check!!.setImageResource(R.drawable.ico_private_room_check_false)
                } else {
                    holder.progress!!.visibility = View.GONE
                    holder.progressContainer!!.visibility = View.GONE
                    if (data.selected) {
                        holder.check!!.setImageResource(R.drawable.ico_private_room_check_true)
                    } else {
                        holder.check!!.setImageResource(R.drawable.ico_private_room_check_false)
                    }
                }
            }
            return view
        }

        override fun getItem(position: Int): VoiceRoomBackGroundItem {
            return mData.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mData.size
        }

    }

    private class ViewHolder {

        var check: ImageView? = null
        var background: ImageView? = null
        var add: View? = null

        var progress: View? = null
        var progressContainer: View? = null
    }

    class UpLoadTimeController {
        var upload_limit = 0
        var upload_times = 0

        constructor(upload_limit: Int, upload_times: Int) {
            this.upload_limit = upload_limit
            this.upload_times = upload_times
        }

        fun upLoadSuccess() {
            upload_times++
        }

        fun isPermission(): Boolean {
            if (upload_limit == 0) {
                return false
            } else {
                return true
            }
        }

        fun canUpLoad(): Boolean {
            if (upload_limit == -1) {
                return true
            }
            if (upload_times >= upload_limit) {
                return false
            }
            return true
        }
    }
}
