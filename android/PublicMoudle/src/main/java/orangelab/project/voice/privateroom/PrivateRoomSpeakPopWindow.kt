package orangelab.project.voice.privateroom

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.PopupWindow
import com.androidtoolkit.view.ScreenUtils
import com.R

/**
 *game_werewolf
 *2018/3/23 上午10:35
 *Mystery
 */
class PrivateRoomSpeakPopWindow : PopupWindow {

    private var speak_iv: ImageView
    private var drawable: AnimationDrawable?

    constructor(context: Context) : super(context) {

        val rootView = LayoutInflater.from(context).inflate(R.layout.layout_private_room_speak_pop, null)
        speak_iv = rootView.findViewById<ImageView>(R.id.speak_iv) as ImageView
        drawable = speak_iv.drawable as AnimationDrawable
        contentView = rootView
        isFocusable = true
        isTouchable = true
        isOutsideTouchable = false
        width = ScreenUtils.dip2px(120f)
        height = ScreenUtils.dip2px(120f)
        setBackgroundDrawable(BitmapDrawable(context.resources))
    }

    fun showPopWindow(parent: View) {
        if (!isShowing) {
            showAtLocation(parent, Gravity.CENTER, 0, 0)
            if (drawable != null) {
                drawable!!.start()
            }
        }
    }

    fun dismissPopWindow() {
        if (isShowing) {
            if (drawable != null) {
                drawable!!.stop()
            }
            dismiss()

        }
    }

}