package orangelab.project.voice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;
import orangelab.project.voice.manager.SmallGameManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 30/10/2017.
 */

public class SmallGameAdapter extends RecyclerView.Adapter<SmallGameAdapter.SmallGameViewHolder> implements Destroyable {


    private Context mContext;
    private OnItemClickListener onItemClickListener;
    private List<SmallGameManager.SmallGame> gameList = new ArrayList<>();

    public SmallGameAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void updateGameList(List<SmallGameManager.SmallGame> gameList) {
        if (gameList != null) {
            this.gameList = gameList;
            notifyDataSetChanged();
        }
    }

    @Override
    public SmallGameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_small_game, null);
        return new SmallGameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SmallGameViewHolder holder, int position) {
        SmallGameManager.SmallGame game = gameList.get(position);
        if (game != null) {
            holder.tvGameName.setText(game.getName());
            holder.ivSmallGame.setImageResource(game.getImageId());
        }
    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }

    @Override
    public void destroy() {
        gameList.clear();
    }

    class SmallGameViewHolder extends RecyclerView.ViewHolder {

        ImageView ivSmallGame;
        TextView tvGameName;

        public SmallGameViewHolder(View itemView) {
            super(itemView);

            ivSmallGame = (ImageView) itemView.findViewById(R.id.iv_voice_small_game);
            tvGameName = (TextView) itemView.findViewById(R.id.tv_voice_game_name);
            itemView.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    SmallGameManager.SmallGame smallGame = gameList.get(getPosition());
                    if (smallGame != null) {
                        onItemClickListener.onItemClick(smallGame);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SmallGameManager.SmallGame smallGame);
    }
}
