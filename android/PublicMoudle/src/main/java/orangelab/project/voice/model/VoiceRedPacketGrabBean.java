package orangelab.project.voice.model;

import com.toolkit.action.Keepable;


/**
 * Created by hwx on 11/07/2018.
 */

public class VoiceRedPacketGrabBean implements Keepable {
    public int ammount;
    public Sender sender;

    public static class Sender implements Keepable{
        public String name;
        public String avatar;
    }
}
