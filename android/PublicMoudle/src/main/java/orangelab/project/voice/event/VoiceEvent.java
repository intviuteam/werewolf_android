package orangelab.project.voice.event;

import orangelab.project.voice.manager.VoiceEmotionManager;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.common.model.EnterRoomResult;

import java.util.List;

/**
 * Created by lloydfinch on 05/09/2017.
 */

public class VoiceEvent {

    //发起抱用户上麦请求
    public static class RequestToNewChairEvent {
        public int requestChairPosition;//发起请求的位置

        public RequestToNewChairEvent(int requestChairPosition) {
            this.requestChairPosition = requestChairPosition;
        }
    }

    //响应抱用户上麦请求
    public static class ResponseToNewChairEvent {
        public int responseChairPosition;//响应的位置，和发起请求的位置对应才处理
        public EnterRoomResult.EnterRoomUserItem selectedUser;

        public ResponseToNewChairEvent(int responseChairPosition, EnterRoomResult.EnterRoomUserItem selectedUser) {
            this.responseChairPosition = responseChairPosition;
            this.selectedUser = selectedUser;
        }
    }

    //在线用户列表更新的事件
    public static class OnLineUsersChangeEvent {
        public List<EnterRoomResult.EnterRoomUserItem> onLineMemberList;

        public OnLineUsersChangeEvent() {
        }

        public OnLineUsersChangeEvent(List<EnterRoomResult.EnterRoomUserItem> list) {
            this.onLineMemberList = list;
        }
    }

    public static class AddFriendEvent {
        public String id;
        public boolean isShow;
    }

    public static class ChangeTopicEvent {
        public VoiceTopicManager.Params.VoiceTopic topic;
    }

    public static class SendEmotionEvent {
        public VoiceEmotionManager.Emotions.Emotion emotion;
    }

    public static class SendPraise {
    }

    public static class ReceivePraise {
        public int likeCount;
    }

    public static class FirstComeIn {

    }

    //通知语音房chair下麦
    public static class DownSeatEvent {
        public int position;//需要进行下麦操作的位置
        public int desPosition;//去到的位置

        public DownSeatEvent() {
        }

        public DownSeatEvent(int position, int desPosition) {
            this.position = position;
            this.desPosition = desPosition;
        }
    }

    //游戏socket断开/连接事件
    public static class GameSocketReconnectEvent {
        public boolean isReconnected;

        public GameSocketReconnectEvent() {
        }

        public GameSocketReconnectEvent(boolean isReconnected) {
            this.isReconnected = isReconnected;
        }
    }

    //语音socket断开/连接事件
    public static class AudioSocketReconnectEvent {
        public boolean isReconnected;

        public AudioSocketReconnectEvent() {
        }

        public AudioSocketReconnectEvent(boolean isReconnected) {
            this.isReconnected = isReconnected;
        }
    }

    //用来使VoiceShareActivity finish的action
    public static class VoiceShareActivityFinishEvent {

    }

    //红包列表点击
    public static class RedPacketHistoryListEvent {
        public String packetId;
        public boolean canGrab;
        public boolean refresh;
        public RedPacketHistoryListEvent(String id, Boolean canGrab, Boolean refresh) {
            this.packetId = id;
            this.canGrab = canGrab;
            this.refresh = refresh;
        }
    }

    //活动房屏蔽聊天消息
    public static class ChatMessageHiddenEvent {
        public boolean isHidden;
        public ChatMessageHiddenEvent(boolean hidden) {
            this.isHidden = hidden;
        }
    }

    //活动房屏蔽礼物消息
    public static class GiftMessageHiddenEvent {
        public boolean isHidden;
        public GiftMessageHiddenEvent(boolean hidden) {
            this.isHidden = hidden;
        }
    }

    //活动房屏蔽进出消息
    public static class JoinMessageHiddenEvent {
        public boolean isHidden;
        public JoinMessageHiddenEvent(boolean hidden) {
            this.isHidden = hidden;
        }
    }
}
