package orangelab.project.voice.privateroom

import orangelab.project.common.model.action.ServerActionRequestFreeMode
import orangelab.project.common.model.action.ServerActionShowEmoticon
import orangelab.project.common.model.action.ServerActionShowGameEmotion
import orangelab.project.common.model.action.ServerActionSystemMsg
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData
import orangelab.project.voice.privateroom.model.PrivateRoomPaperPlane
import orangelab.project.voice.privateroom.model.TempPictureMessageItem

/**
 * game_werewolf
 * 2018/4/10 下午4:32
 * Mystery
 */

interface PrivateRoomContextObserver {

    fun onOnlineNumber(number: Int)

    fun onLikeNumber(number: Long)

    fun onMessageListChange()

    fun onTempPictureMessage(tempPictureMessageItem: TempPictureMessageItem?)

    fun onSeatUIRefresh(position: Int)

    fun onSeatUIAllRefresh()

    fun onSeatStateRefresh(position: Int)

    fun onSpeaking(position: Int)

    fun onUnSpeaking(position: Int)

    fun onSeatLocked(position: Int, isLocked: Boolean)

    fun onShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon)

    fun onShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion)

    fun onShowGiftAnim(fromPosition: Int, toPosition: Int, type: String)

    fun onShowPaperPlane(paperPlane: PrivateRoomPaperPlane)

    fun onShowPaperPlane(paperPlaneList: List<PrivateRoomPaperPlane>)

    fun onFreeStyleRequest(serverActionRequestFreeMode: ServerActionRequestFreeMode)

    fun onKickOut(member: PrivateRoomMemberData)

    fun onUpdateRoomTitle()

    fun onUpdateRoomTitleBar()

    fun onUpdateBackGround()

    fun onUpdateRoomTag()

    fun onSocketConnect()

    fun onSocketDisconnect()

    fun onSocketClose()

    fun onSelfUpSeat()

    fun onSelfDownSeat()

    fun onSelfTurnIntoBanMic(flag: Boolean)

    fun onSelfTurnIntoFreeStyle(flag: Boolean)

    fun onToolBoxItemUpdate()

    fun onToolBoxUpdate()

    fun onSystemMsg(serverActionSystemMsg: ServerActionSystemMsg)
}
