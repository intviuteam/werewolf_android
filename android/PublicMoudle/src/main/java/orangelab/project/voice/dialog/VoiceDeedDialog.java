package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.adapter.VoiceSpinnerAdapter;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.interfaces.GetOnLineUsers;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.model.VoiceSimpleUserBean;
import orangelab.project.voice.utils.PositionHelper;

import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 10/01/2018.
 * //申请、转让房契都用这个
 */

public class VoiceDeedDialog extends SafeDialog implements Destroyable {

    public static final int APPLY_DEED = 10001;
    public static final int CHANGE_DEED = 10002;

    private TextView tvTitle;
    private TextView tvContent;
    private View llSpendContainer;
    private View llSpinnerContainer;
    private Spinner spinner;
    private TextView tvSpend;
    private View btnSure;
    private View btnCancel;

    private Context mContext;
    private int type;
    private VoiceSpinnerAdapter adapter;
    private List<VoiceSimpleUserBean> beans;

    public VoiceDeedDialog(@NonNull Context context, int type) {
        this(context, R.style.radius_dialog, type);
    }

    public VoiceDeedDialog(@NonNull Context context, @StyleRes int themeResId, int type) {
        super(context, themeResId);
        this.mContext = context;
        this.type = type;
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        init();
    }

    private void init() {
        initView();
        initWindow();
        initData();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_deed, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        tvTitle = (TextView) contentView.findViewById(R.id.tv_title);
        tvContent = (TextView) contentView.findViewById(R.id.tv_content);
        llSpendContainer = contentView.findViewById(R.id.ll_spend_container);
        tvSpend = (TextView) contentView.findViewById(R.id.tv_spend);
        llSpinnerContainer = contentView.findViewById(R.id.ll_spinner_container);
        spinner = (Spinner) contentView.findViewById(R.id.spinner_deed);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);

        spinner.setDropDownVerticalOffset(ScreenUtils.dip2px(50));
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(380);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        params.gravity = Gravity.TOP;
        params.y = ScreenUtils.dip2px(30);
        getWindow().setAttributes(params);
    }

    private void initData() {

        if (type == APPLY_DEED) {
            tvTitle.setText(MessageUtils.getString(R.string.str_voice_apply_deed));
            llSpinnerContainer.setVisibility(View.GONE);
        } else {
            tvTitle.setText(MessageUtils.getString(R.string.str_voice_change_deed));
            llSpinnerContainer.setVisibility(View.VISIBLE);
        }
        String duration = "7天";
        VoiceTopicManager.Params topic = VoiceTopicManager.getInstance().getTopic();
        if (topic != null && !TextUtils.isEmpty(topic.room_reset_duration)) {
            duration = topic.room_reset_duration;
        }
        tvContent.setText(Html.fromHtml(MessageUtils.getString(R.string.str_voice_apply_deed_content, VoiceRoomConfig.getRoomId(), duration)));

        //这里初始化一下spinner
        tvSpend.setText(MessageUtils.getString(R.string.str_voice_number_deed_spend, VoiceRoomConfig.getRoomPrice()));
        initSpinner();
    }

    private void initSpinner() {
        beans = new ArrayList<>();
        updateBeans();
        adapter = new VoiceSpinnerAdapter(beans, mContext);
        spinner.setAdapter(adapter);
    }

    private void syncData() {
        updateBeans();
        adapter = new VoiceSpinnerAdapter(beans, mContext);
        spinner.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void updateBeans() {
        beans.clear();
        List<EnterRoomResult.EnterRoomUserItem> onLineMembers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem user : onLineMembers) {
            if (!PositionHelper.isSelf(user.id)) {
                beans.add(new VoiceSimpleUserBean(user.id, user.uid, user.name, user.avatar, user.position));
            }
        }
    }

    private void initListener() {
        //根据类型决定调用哪个方法
        btnSure.setOnClickListener(v -> {
            if (type == APPLY_DEED) {
                buyDeed();
                dismiss();
            } else {
                changeDeed();
            }
        });
        btnCancel.setOnClickListener(v -> dismiss());

        //不再动态刷新列表，防止点击错误
        //用来接收查看在线用户时，用户列表更新的事件
//        RxToolKit.Build(this, VoiceEvent.OnLineUsersChangeEvent.class).action(
//                (onLineUsersChangeEvent) -> syncData(onLineUsersChangeEvent.onLineMemberList)
//        ).register();
    }

    //购买房契
    private void buyDeed() {
        VoiceSocketHelper.purchaseRoom();
    }

    //转让房契
    private void changeDeed() {
        int position = spinner.getSelectedItemPosition();
        if (position >= 0 && position < beans.size()) {
            VoiceSimpleUserBean bean = beans.get(position);
            //点击的瞬间该用户还在房间才转让
            if (bean != null && beansContainsUser(bean.userId)) {
                VoiceSocketHelper.handleOverCreator(bean.userId);
                dismiss();
            } else {
                syncData();
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
                //dismiss();
            }
        } else {
            //syncData();
            //ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
            dismiss();
        }
    }

    private boolean beansContainsUser(String id) {
        List<EnterRoomResult.EnterRoomUserItem> onLineUsers = VoiceRoomDataSourceManager.getInstance().getOnLineUsers();
        for (EnterRoomResult.EnterRoomUserItem bean : onLineUsers) {
            if (bean != null && TextUtils.equals(id, bean.id)) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        beans.clear();
        adapter.destroy();
    }


}
