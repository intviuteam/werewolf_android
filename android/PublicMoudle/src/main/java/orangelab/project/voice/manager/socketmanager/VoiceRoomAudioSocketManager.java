package orangelab.project.voice.manager.socketmanager;

import android.os.Build;
import android.view.MotionEvent;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import cn.intviu.sdk.model.User;
import cn.intviu.support.AppUtil;
import orangelab.project.MainApplication;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.engine.OrbitEngineManager;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.model.VoiceRoomAudioConfigBean;

/**
 * Created by lloydfinch on 30/12/2017.
 * //语音房音频Socket管理器
 */

public class VoiceRoomAudioSocketManager implements Keepable, Destroyable {

    private static final String TAG = "VoiceRoomAudioSocketManager";

    private static VoiceRoomAudioSocketManager instance;

    private boolean isAudioSocketConnected = false;

    public static VoiceRoomAudioSocketManager getInstance() {
        if (instance == null) {
            synchronized (VoiceRoomAudioSocketManager.class) {
                if (instance == null) {
                    instance = new VoiceRoomAudioSocketManager();
                }
            }
        }
        return instance;
    }

    //启动音频连接
    public void Start(VoiceRoomAudioConfigBean configBean) {
        initAndStartAudioEngine(configBean);
        //初始化完毕，可以说话
        MainApplication.getInstance().getSocketEngineHelper().startPollMessage();
    }

    //关闭音频连接
    public void ShutDown(Runnable callback) {
        releaseAndCloseAudioEngine(callback);
    }

    //获取连接状态
    public boolean isConnected() {
        return isAudioSocketConnected;
    }

    private VoiceRoomAudioSocketManager() {
        initAudioSocketConnectedStateListener();
        initAudioSocketShutDownListener();
    }

    //初始化并启动
    private void initAndStartAudioEngine(VoiceRoomAudioConfigBean configBean) {
        if (configBean != null && configBean.mediaServer != null) {
            User user = new User();
            String versionName = AppUtil.getVersionName(MainApplication.getInstance());
            if (Build.CPU_ABI.contains("86")) {
                versionName = versionName + "-x86";
            }
            user.setName(versionName + "_" + GlobalUserState.getGlobalState().getUserName());
            user.setID(GlobalUserState.getGlobalState().getUserId());

            //启动orbit
            OrbitEngineManager.INSTANCE.start(MainApplication.getInstance(), configBean.roomId, configBean.mediaServer, user);
        }
    }

    //关闭orbit
    private void releaseAndCloseAudioEngine(Runnable callback) {
        //保险起见，退出房间，先关闭语音通道，再断开语音服务器
        OrbitEngineManager.INSTANCE.closeRecord();
        OrbitEngineManager.INSTANCE.shutDown(callback);
    }

    //音频socket连接状态监听器
    private void initAudioSocketConnectedStateListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent.class).action(audioServerEvent -> {
            Utils.runSafely(() -> {
                switch (audioServerEvent.getAction()) {
                    case ServerEvent.RECONNECT_FROM_AUDIO_SERVER:
                        isAudioSocketConnected = true;
                        //restoreFree();
                        PLog.e(TAG, "RECONNECT_FROM_AUDIO_SERVER:" + isAudioSocketConnected);
                        break;
                    case ServerEvent.DISCONNECT_FROM_AUDIO_SERVER:
                        isAudioSocketConnected = false;
                        PLog.e(TAG, "DISCONNECT_FROM_AUDIO_SERVER:" + isAudioSocketConnected);
                        break;
                }
            });
        }).register();
    }

    //音频socket断开监听器
    private void initAudioSocketShutDownListener() {
//        RxToolKit.Build(this, ViewEvent.DestroyEvent.class).action(v -> {
//            //已经断开连接，就销毁自己
//            destroy();
//        }).register();
    }

    //断网重连后，如果是自由模式并且在说话，就发送说话请求
    //这里不再处理，VoiceChair已经处理，如果消息延迟，有可能卡麦
    private void restoreFree() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            if (personalData.isSpeaking()) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_DOWN));
            }
        }
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        instance = null;
    }
}
