package orangelab.project.voice.manager;

import android.content.Context;
import android.os.Message;
import android.view.View;

import orangelab.project.common.effect.gif.GifImageView;
import com.androidtoolkit.HttpToolkit;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.gif.BasicComponent;
import orangelab.project.voice.ds.Queue;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lloydfinch on 27/10/2017.
 */

public class GifManager extends BasicComponent {

    private static final String TAG = "GifManager";

    private Queue<VoiceEmotionManager.Emotions.Emotion> emotionQueue;
    private GifImageView gifImageView;
    private Context mContext;

    private volatile boolean isSafeLy = true;
    private SafeHandler emotionHandler = new SafeHandler() {
        @Override
        public void handleMessage(Message msg) {
            VoiceEmotionManager.Emotions.Emotion emotion = emotionQueue.poll();
            if (emotion != null) {
                startEmotion(emotion);
            }
        }
    };

    public GifManager(Context context, GifImageView gifImageView) {
        this.mContext = context.getApplicationContext();
        this.gifImageView = gifImageView;
        emotionQueue = new Queue<>();
    }

    public void addEmotion(VoiceEmotionManager.Emotions.Emotion emotion) {
        emotionQueue.enQueue(emotion);
        if (isSafeLy) {
            emotionHandler.sendEmptyMessage(0);
        }
    }

    private void startEmotion(VoiceEmotionManager.Emotions.Emotion emotion) {
        isSafeLy = false;
        gifImageView.setVisibility(View.VISIBLE);
        Observable.just(emotion).map(s -> {
            byte[] data = VoiceEmotionManager.getInstance().findEmotionGif(emotion);
            if (data != null) {
                return data;
            } else {
                return HttpToolkit.doGet(emotion.getUrl());
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> postSafely(() -> {
                    if (bytes != null) {
                        gifImageView.setBytes(bytes);
                        gifImageView.startAnimation();
                        postDelaySafely(() -> endEmotion(), 2000);
                    }
                }));
    }

    private void endEmotion() {
        postSafely(() -> {
            gifImageView.setVisibility(View.GONE);
            gifImageView.clearAnimation();
            gifImageView.clear();

            //播完一个后通知播放下一个
            isSafeLy = true;
            emotionHandler.sendEmptyMessage(0);
        });
    }


    @Override
    public void destroy() {
        super.destroy();
        if (emotionQueue != null) {
            emotionQueue.clear();
        }

        if (emotionHandler != null) {
            emotionHandler.release();
        }
    }
}
