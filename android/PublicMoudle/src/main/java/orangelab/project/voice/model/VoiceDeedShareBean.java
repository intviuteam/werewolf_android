package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 29/01/2018.
 * <p>
 * //房契分享使用
 */

public class VoiceDeedShareBean implements Serializable, Keepable {

    public String id;//id
    public String nickname;//昵称
    public String roomId;//房间号
    public int uid;//uid
    public String headImageUrl;//头像url
    public long time;//时间

    public VoiceDeedShareBean() {
    }

    public VoiceDeedShareBean(String id, String nickname, int uid, String headImageUrl) {
        this.id = id;
        this.uid = uid;
        this.nickname = nickname;
        this.headImageUrl = headImageUrl;
    }

    public VoiceDeedShareBean(String id, String nickname, String roomId, int uid, String headImageUrl) {
        this.id = id;
        this.nickname = nickname;
        this.roomId = roomId;
        this.uid = uid;
        this.headImageUrl = headImageUrl;
    }

    @Override
    public String toString() {
        return "VoiceDeedShareBean{" +
                "id='" + id + '\'' +
                ", nickname='" + nickname + '\'' +
                ", roomId='" + roomId + '\'' +
                ", uid=" + uid +
                ", headImageUrl='" + headImageUrl + '\'' +
                ", time=" + time +
                '}';
    }
}
