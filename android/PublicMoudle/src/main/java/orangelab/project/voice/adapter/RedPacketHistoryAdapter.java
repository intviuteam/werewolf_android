package orangelab.project.voice.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.model.VoiceRedPacketListBean;

/**
 * Created by hwx on 13/07/2018.
 */

public class RedPacketHistoryAdapter extends BaseAdapter {
    private Context mContext;
    private List<VoiceRedPacketListBean.Package> mRedPacketList;

    public RedPacketHistoryAdapter(Context context, VoiceRedPacketListBean packetList) {
        this.mContext = context;
        if (packetList != null) {
            mRedPacketList = packetList.packages;
        }
    }

    @Override
    public int getCount() {
        return mRedPacketList.size();
    }

    @Override
    public Object getItem(int position) {
        return mRedPacketList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.layout_dialog_redpacket_history_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.userName = (TextView) convertView.findViewById(R.id.user);
            viewHolder.packetNum = (TextView) convertView.findViewById(R.id.packet_number);
            viewHolder.packetStatus = (TextView) convertView.findViewById(R.id.packet_status);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        VoiceRedPacketListBean.Package item = mRedPacketList.get(position);
        if (item != null) {
            viewHolder.userName.setText(item.sender.name);
            //自己已经抢过该红包并且红包已经抢完，则只能查看
            boolean canGrabThisPacket = !item.grabbed && item.left > 0;
            if (canGrabThisPacket) {
                viewHolder.packetStatus.setText(R.string.str_red_packet_receive);
                viewHolder.packetStatus.setTextColor(mContext.getResources().getColor(R.color.color_voice_font_white));
                viewHolder.packetStatus.setBackgroundResource(R.drawable.drawable_redpacket_check_button);
            } else {
                viewHolder.packetStatus.setText(R.string.str_red_packet_find);
                viewHolder.packetStatus.setTextColor(mContext.getResources().getColor(R.color.color_red_packet_purple));
                viewHolder.packetStatus.setBackgroundResource(R.drawable.drawable_redpacket_info_button);
            }
            //红包已经抢完
            if (item.left == 0) {
                viewHolder.packetNum.setText(R.string.str_red_packet_received);
                viewHolder.packetNum.setTextColor(mContext.getResources().getColor(R.color.color_red_packet_gray));
            } else {
                viewHolder.packetNum.setText(String.format(mContext.getResources().getString(R.string.str_red_packet_number),item.left,item.count));
                viewHolder.packetNum.setTextColor(mContext.getResources().getColor(R.color.color_red_packet_purple));
            }

            viewHolder.packetStatus.setOnClickListener(v -> {
                EventBus.getDefault().post(new VoiceEvent.RedPacketHistoryListEvent(item._id, canGrabThisPacket,false));
            });
        }
        return convertView;
    }

    class ViewHolder {
        TextView userName;
        TextView packetNum;
        TextView packetStatus;
    }

    public void update(VoiceRedPacketListBean bean) {
        if (bean != null) {
            this.mRedPacketList = bean.packages;
            notifyDataSetChanged();
        }
    }
    public void add(VoiceRedPacketListBean bean) {
        if (bean != null) {
            this.mRedPacketList.addAll(bean.packages);
            notifyDataSetChanged();
        }
    }

}
