package orangelab.project.voice.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.R;
import orangelab.project.common.utils.CountDownView;
import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 25/11/2017.
 * <p>
 * 谁是卧底右上角状态板
 */

public class StatusCardView extends RelativeLayout implements Destroyable {

    private TextView tvCardTitle;//标题
    private View llCardContent;//内容

    private TextView tvSystemMsg;//系统消息栏

    private View llWordShow;//展示"你的词"
    private TextView tvYourWord;//你的词

    private View llVote;//投票栏
    private View llVoteGoodMan;//投票栏:好人
    private View llVoteSpy;//投票栏:卧底
    private TextView tvVoteGoodMan;//好人词
    private TextView tvVoteSpy;//卧底词

    private View llAlarm;//倒计时器
    private CountDownView countDownView;
    private TextView tvLeftSeconds;//剩余倒计时
    private boolean isShow = false;

    public StatusCardView(Context context) {
        super(context);
        init(context);
    }

    public StatusCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public StatusCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        addView(inflate(context, R.layout.layout_state_card, null));
        tvCardTitle = (TextView) findViewById(R.id.tv_card_title);
        llCardContent = findViewById(R.id.ll_card_content);
        tvSystemMsg = (TextView) findViewById(R.id.tv_card_system_msg);
        llWordShow = findViewById(R.id.ll_card_content_description);
        tvYourWord = (TextView) findViewById(R.id.tv_card_your_word_content);

        llVote = findViewById(R.id.ll_card_vote);
        llVoteGoodMan = findViewById(R.id.ll_card_content_vote_good_man);
        llVoteSpy = findViewById(R.id.ll_card_content_vote_spy);
        tvVoteGoodMan = (TextView) findViewById(R.id.tv_card_good_man);
        tvVoteSpy = (TextView) findViewById(R.id.tv_card_spy);

        llAlarm = findViewById(R.id.ll_alarm_container);
        tvLeftSeconds = (TextView) findViewById(R.id.tv_left_second);
    }

    //外部调用,初始化倒计时器
    public void initCountView(int second, CountDownView.CountDownFinish countDownFinish) {
        if (countDownFinish == null) {
            countDownView = new CountDownView(tvLeftSeconds, second, this.countDownFinish);
        } else {
            countDownView = new CountDownView(tvLeftSeconds, second, countDownFinish);
        }
        countDownView.setCountDownRule(R.string.str_voice_second_with_s);
        countDownView.cancel();
        countDownView.start();
    }

    public void showSystemMsg(boolean show) {
        if (show) {
            tvSystemMsg.setVisibility(View.VISIBLE);
        } else {
            tvSystemMsg.setVisibility(View.GONE);
        }
    }

    public void showWordDescription(boolean show) {
        if (show) {
            llWordShow.setVisibility(View.VISIBLE);
        } else {
            llWordShow.setVisibility(View.GONE);
        }
    }

    public void showVote(boolean show) {
        if (show) {
            llVote.setVisibility(View.VISIBLE);
        } else {
            llVote.setVisibility(View.GONE);
        }
    }

    public void showAlarm(boolean show) {
        if (show) {
            llAlarm.setVisibility(VISIBLE);
        } else {
            llAlarm.setVisibility(GONE);
        }
    }

    public void cancel() {
        if (countDownView != null) {
            countDownView.cancel();
        }
    }

    public void changeTitle(CharSequence title) {
        tvCardTitle.setText(title);
    }

    public void changeSystemMsg(CharSequence msg) {
        tvSystemMsg.setText(msg);
    }

    public void changeYourWord(CharSequence word) {
        tvYourWord.setText(word);
    }

    public void changeVoteGoodMan(CharSequence goodMan) {
        tvVoteGoodMan.setText(goodMan);
    }

    public void changeVoteSpy(CharSequence spy) {
        tvVoteSpy.setText(spy);
    }

    public CountDownView.CountDownFinish countDownFinish = () -> {
        setVisibility(GONE);
    };

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility != VISIBLE) {
            if (countDownView != null) {
                countDownView.cancel();
            }
        }
    }

    @Override
    public void destroy() {
        if (countDownView != null) {
            countDownView.cancel();
        }
    }
}
