package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.config.VoiceRoomConfig;

/**
 * Created by lloydfinch on 03/11/2017.
 */

public class VoiceBlackDialog extends SafeDialog {
    public static final String TAG = "VoiceFriendDialog";

    private View btnSure;
    private View btnCancel;
    private ImageView mHeadView;
    private ImageView ivUserSex;
    private TextView userName;
    private TextView userLevel;
    private TextView userPopularity;
    private View llUserInfoContainer;

    private Context mContext;
    private String mUserId;

    public VoiceBlackDialog(@NonNull Context context, String userId) {
        this(context, R.style.radius_dialog, userId);
    }

    public VoiceBlackDialog(@NonNull Context context, @StyleRes int themeResId, String userId) {
        super(context, themeResId);
        this.mUserId = userId;
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
        initData();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_black_list, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llUserInfoContainer = contentView.findViewById(R.id.rl_user_detail_container);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        mHeadView = (ImageView) findViewById(R.id.user_head_view);
        ivUserSex = (ImageView) findViewById(R.id.iv_user_sex);
        ivUserSex.setVisibility(View.GONE);
        userName = (TextView) findViewById(R.id.voice_user_name);
        userLevel = (TextView) findViewById(R.id.voice_user_level);
        userPopularity = (TextView) findViewById(R.id.voice_user_popularity);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            if (TextUtils.equals(mUserId, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
            } else {
                addBlack();
                dismiss();
            }
        });
        btnCancel.setOnClickListener(v -> dismiss());
        llUserInfoContainer.setOnClickListener(v -> {
            toPersonalInfoActivity();
        });
    }

    private void toPersonalInfoActivity() {
        if (Utils.checkUserIdIsTourist(mUserId)) {
            ToastToolKit.showShort(R.string.str_login_info);
            return;
        }
        PersonalInfoActivity.Launch(mContext, mUserId);
    }

    private void getUserInfo(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        try {
            ApiManager.GetUserInfoApi(userId, (userInfoResult, e) -> runSafely(() -> updateData(userInfoResult)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        getUserInfo(mUserId);
    }

    private void updateData(UserInfoResult result) {
        if (!TextUtils.isEmpty(result.image)) {
            PicassoUtils.loadImage(mContext, result.image, mHeadView);
        }
        if (result.sex == 1) {
            ivUserSex.setVisibility(View.VISIBLE);
            ivUserSex.setImageResource(R.mipmap.ico_voice_sex_man);
        } else if (result.sex == -1) {
            ivUserSex.setVisibility(View.GONE);
        } else {
            ivUserSex.setVisibility(View.VISIBLE);
            ivUserSex.setImageResource(R.mipmap.ico_voice_sex_wuman);
        }
        userName.setText(result.name);
        userLevel.setText(
                MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level))
        );
        userPopularity.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));
    }

    private void addBlack() {
        RoomSocketEngineHelper.addVoiceBlack(mUserId);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    protected void release() {
        mContext = null;
    }
}
