package orangelab.project.voice.privateroom.model

/**
 * game_werewolf
 * 2018/3/21 下午5:55
 * Mystery
 */

class GiftMessageItem : ChatMessageItem {

    var type: String = ""
    var res: Int = 0

    constructor(textColorResource: Int, message: String, position: Int, name: String, type: String) : super(textColorResource, message, position, name) {
        this.type = type
    }

    constructor(message: String, position: Int, name: String, type: String) : super(message, position, name) {
        this.type = type
    }

    constructor(message: String, position: Int, name: String, res: Int) : super(message, position, name) {
        this.res = res
    }

}
