package orangelab.project.voice.manager;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.text.TextUtils;

import orangelab.project.MainApplication;

import com.R;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import orangelab.project.common.utils.MessageUtils;

import com.androidtoolkit.FileToolKit;

import orangelab.project.voice.gif.GifDownLoader;

import com.androidtoolkit.HttpToolkit;
import com.androidtoolkit.StreamToolKit;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.intviu.support.GsonHelper;

/**
 * Created by lloydfinch on 21/10/2017.
 */

public class VoiceEmotionManager implements Destroyable {
    private static final String TAG = "VoiceEmotionManager";
    private static final String EMOTION_LIST = "voice_room_emotion_list";//保存在本地的filename
    private static volatile VoiceEmotionManager mInstance;
    private static final String THUMBNAIL = "https://game-cdn.xiaobanhui.com/img/audio_room_bq/t/";
    private static final String URL = "https://game-cdn.xiaobanhui.com/img/audio_room_bq/u/";


    private Emotions emotions = new Emotions();
    private HashMap<String, String> markToEmotions = new HashMap<>();
    private OnEmotionSyncFinished onEmotionSyncFinished;

    private VoiceEmotionManager() {
        init();
    }

    public static VoiceEmotionManager getInstance() {
        if (mInstance == null) {
            synchronized (VoiceEmotionManager.class) {
                if (mInstance == null) {
                    mInstance = new VoiceEmotionManager();
                }
            }
        }
        return mInstance;
    }

    public void setOnEmotionSyncFinished(OnEmotionSyncFinished onEmotionSyncFinished) {
        this.onEmotionSyncFinished = onEmotionSyncFinished;
    }

    public Emotions getEmotions() {
        return emotions;
    }

    public void init() {
        String localData = getLocalEmotionList();
        if (TextUtils.isEmpty(localData)) {
            //file没有就取app
            initByInner();
        } else {
            //file有说明app里的是过时的,就取file里面的
            emotions = GsonHelper.getGson().fromJson(localData, Emotions.class);
        }
    }

    private void initByInner() {
        emotions = new Emotions();
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_lh), R.mipmap.ico_voice_emotion_sweat, "lh.png", "lh.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_dx), R.mipmap.ico_voice_emotion_smoke, "dx.png", "dx.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_smm), R.mipmap.ico_voice_emotion_amorous, "smm.png", "smm.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_wy), R.mipmap.ico_voice_emotion_no_word, "wy.png", "wy.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_an), R.mipmap.ico_voice_emotion_love, "an.png", "an.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_xx), R.mipmap.ico_voice_emotion_shame, "xx.png", "hx.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_ll), R.mipmap.ico_voice_emotion_tear, "ll.png", "dk.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_sq), R.mipmap.ico_voice_emotion_hungry, "sq.png", "sq.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_gl), R.mipmap.ico_voice_emotion_gramace, "gl.png", "gl.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_t), R.mipmap.ico_voice_emotion_spit, "t.png", "t.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_tx), R.mipmap.ico_voice_emotion_titter, "tx.png", "tx.gif"));
//        emotions.data.add(new Emotions.Emotion(MessageUtils.getString(R.string.str_voice_emotion_by), R.mipmap.ico_voice_emotion_white_eye, "by.png", "by.gif"));

        initEmotionAnimByLocal();
    }

    private void initEmotionAnimByLocal() {
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_lh), "gif/emotion/sweat.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_dx), "gif/emotion/smoke.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_smm), "gif/emotion/se.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_wy), "gif/emotion/no_word.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_an), "gif/emotion/love.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_xx), "gif/emotion/shame.gif");
//
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_ll), "gif/emotion/tear.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_sq), "gif/emotion/angry.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_gl), "gif/emotion/guilian.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_t), "gif/emotion/spit.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_tx), "gif/emotion/smoke_s.gif");
//        markToEmotions.put(MessageUtils.getString(R.string.str_voice_emotion_by), "gif/emotion/white_eye.gif");
    }

    public byte[] findEmotionGif(Emotions.Emotion emotion) {
        if (emotion == null) {
            return null;
        }
        String mark = emotion.getMark();

        //app中有
        if (markToEmotions.containsKey(mark)) {
            try {
                return StreamToolKit.readStream(MainApplication.getInstance().getAssets().open(findEmotionAnimByEmotion(emotion)));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            //app中没有就取file中的
            if (emotions == null) {
                return null;
            } else {
                for (Emotions.Emotion e : emotions.data) {
                    if (TextUtils.equals(e.getMark(), mark)) {
                        return FileToolKit.loadFile(e.getUrl());
                    }
                }
                //file中没有，更新
                //运行到这里说明没找到,就去更新表情包
                syncEmotions();
            }
        }

        return null;
    }

    public String findEmotionAnimByEmotion(Emotions.Emotion emotion) {
        if (emotion == null) {
            return "";
        }
        String mark = emotion.getMark();
        if (markToEmotions.containsKey(mark)) {
            return markToEmotions.get(mark);
        }

        return "";
    }

    @Override
    public void destroy() {

        if (updateEmotionListTask != null) {
            if (!updateEmotionListTask.isCancelled()) {
                updateEmotionListTask.cancel(true);
            }
            updateEmotionListTask = null;
        }

        if (emotions != null) {
            emotions.data.clear();
        }
        if (markToEmotions != null) {
            markToEmotions.clear();
        }

        mInstance = null;
    }

    private AsyncTask updateEmotionListTask;

    //同步表情
    @SuppressLint("StaticFieldLeak")
    public void syncEmotions() {
        try {
            if (updateEmotionListTask != null) {
                if (!updateEmotionListTask.isCancelled()) {
                    updateEmotionListTask.cancel(true);
                }
                updateEmotionListTask = null;
            }
            updateEmotionListTask = new AsyncTask<Object, Integer, String>() {
                @Override
                protected String doInBackground(Object... params) {
                    String url = Constant.AUDIO_HTTP_URL + Constant.URL_VOICE_EMOTIONS;
                    byte[] data = HttpToolkit.doGet(url);
                    if (data != null && data.length > 0) {
                        return new String(data);
                    }
                    return "";
                }

                @Override
                protected void onPostExecute(String s) {
                    updateEmotionList(s);
                }
            };
            updateEmotionListTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateEmotionList(String json) {
        PLog.e(TAG, "updateEmotionList: " + json);
        saveEmotionListToLocal(json);
        emotions = GsonHelper.getGson().fromJson(json, Emotions.class);
        if (emotions != null && emotions.data != null && emotions.data.size() > 0) {

            //更新完回调更新UI
            if (onEmotionSyncFinished != null) {
                onEmotionSyncFinished.onSyncFinished();
            }

            //更新gif
            for (Emotions.Emotion emotion : emotions.data) {
                //只需要更新gif，不需要更新mipmap(picasso会处理)
                String gifUrl = emotion.getUrl();
                if (!TextUtils.isEmpty(gifUrl)) {
                    byte[] bytes = FileToolKit.loadFile(gifUrl);
                    if (bytes == null) {
                        //本地没有就下载
                        PLog.e(TAG, "download emotion start : " + gifUrl);
                        GifDownLoader.downLoad(gifUrl, new GifDownLoader.CallBack() {
                            @Override
                            public void onSuccess(byte[] data) {
                                PLog.e(TAG, "download success: " + gifUrl);
                                FileToolKit.saveFile(gifUrl, data);
                            }

                            @Override
                            public void onFailure() {
                                PLog.e(TAG, "download emotion fail: " + gifUrl);
                            }
                        });
                    }
                }
            }
        }
    }

    private void saveEmotionListToLocal(String data) {
        try {
            FileToolKit.saveFile(EMOTION_LIST, data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            PLog.e(TAG, "saveEmotionListToLocal error: " + e.getMessage());
        }
    }

    private String getLocalEmotionList() {
        byte[] bytes = FileToolKit.loadFile(EMOTION_LIST);
        if (bytes == null) {
            return "";
        }
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static class Emotions implements Serializable, Keepable {

        int code;
        List<Emotion> data;

        public Emotions() {
            data = new ArrayList<>();
        }

        public List<Emotion> getData() {
            return data;
        }

        public static class Emotion implements Keepable {
            String mark = "";
            String thumbnail = "";
            int thumbId = 0;
            String url = "";

            public Emotion(String mark, String thumbnail, String url) {
                this(mark, 0, thumbnail, url);
            }

            public Emotion(String mark, int thumbId, String thumbnail, String url) {
                this.mark = mark;
                this.thumbnail = THUMBNAIL + thumbnail;
                this.thumbId = thumbId;
                this.url = URL + url;
            }

            public String getMark() {
                return mark;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public String getUrl() {
                return url;
            }

            public int getThumbId() {
                return thumbId;
            }

            @Override
            public String toString() {
                return "Emotion{" +
                        "mark='" + mark + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", thumbId='" + thumbId + '\'' +
                        ", url='" + url + '\'' +
                        '}';
            }
        }
    }

    public interface OnEmotionSyncFinished {
        void onSyncFinished();
    }
}


