package orangelab.project.voice.privateroom.view

import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.view.ScreenUtils
import com.R
import orangelab.project.common.utils.SafeHandler
import orangelab.project.voice.privateroom.PrivateRoomPaperPlaneViewObserver
import orangelab.project.voice.privateroom.PrivateRoomTools
import orangelab.project.voice.privateroom.model.PrivateRoomPaperPlane
import java.util.*

/**
 *game_werewolf
 *2018/3/28 下午6:50
 *Mystery
 */
class PrivateRoomPaperPlaneViewImpl : PrivateRoomPaperPlaneView {


    private var mPlaneWidth = ScreenUtils.dip2px(300f)
    private var mPlaneHeight = ScreenUtils.dip2px(70f)

    private var mRootView: ViewGroup
    private var mObserver: PrivateRoomPaperPlaneViewObserver? = null

    private var mPaperPlanesCache: MutableList<PrivateRoomPaperPlane>
    private var mCurHolder: ViewHolder? = null
    private var mSafeHandler = SafeHandler()

    constructor(mRootView: ViewGroup, mObserver: PrivateRoomPaperPlaneViewObserver) {
        this.mRootView = mRootView
        this.mObserver = mObserver
        mPaperPlanesCache = LinkedList()

        mRootView.setOnTouchListener { v, event ->
            if (mCurHolder == null) {
                false
            } else {
                if (event.action == MotionEvent.ACTION_DOWN) {
                    removeTop()
                }
                true
            }
        }
    }

    override fun destroy() {
        clearPaperPlane()
        mSafeHandler.release()
        mObserver = null
    }

    override fun addPaperPlane(paperPlaneList: List<PrivateRoomPaperPlane>?) {
        paperPlaneList?.forEach {
            addPaperPlane(it)
        }
    }

    override fun addPaperPlane(paperPlane: PrivateRoomPaperPlane) {
        mSafeHandler.postSafely {
            if (mCurHolder == null) {
                val holder = createPaperPlaneViewHolder(paperPlane)
                mCurHolder = holder
                mRootView.addView(holder.root, ViewGroup.LayoutParams(mPlaneWidth, mPlaneHeight))
            } else {
                /**
                 * 将老的数据放入队列
                 */
                val old = mCurHolder!!.replaceAndGet(paperPlane)
                if (old != null) {
                    mPaperPlanesCache.add(0, old)
                }
            }
        }
    }

    private fun removeTop() {
        if (mCurHolder != null) {
            mCurHolder!!.data = null
            if (mPaperPlanesCache.size > 0) {
                val cache = mPaperPlanesCache.removeAt(0)
                mCurHolder!!.replaceAndGet(cache)
            } else {
                mRootView.removeView(mCurHolder!!.root)
                mCurHolder = null
            }
        } else {
            return
        }
    }

    private fun createPaperPlaneViewHolder(paperPlane: PrivateRoomPaperPlane): ViewHolder {
        val holder = ViewHolder()
        holder.root = View.inflate(mRootView.context, R.layout.layout_private_room_paper_plane_view, null)
        holder.head = holder.root.findViewById<ImageView>(R.id.id_userhead) as ImageView
        holder.text = holder.root.findViewById<TextView>(R.id.id_paper_plane_text) as TextView
        holder.close = holder.root.findViewById(R.id.id_close)
        holder.replay_text = holder.root.findViewById<TextView>(R.id.text2) as TextView
        holder.close.setOnClickListener({ v ->
            removeTop()
        })
        holder.root.setOnClickListener({ v ->
            mObserver?.onPaperPlaneReplay(holder.data)
            removeTop()
        })
        holder.replaceAndGet(paperPlane)
        return holder
    }

    override fun clearPaperPlane() {
        mPaperPlanesCache.clear()
        mRootView.removeAllViews()
    }

    class ViewHolder {
        lateinit var text: TextView
        lateinit var replay_text: TextView
        lateinit var head: ImageView
        lateinit var close: View
        lateinit var root: View
        var data: PrivateRoomPaperPlane? = null

        fun replaceAndGet(paperPlane: PrivateRoomPaperPlane): PrivateRoomPaperPlane? {
            val old = data
            PicassoUtils.loadImage(head!!.context, paperPlane.fromUserIcon, head, R.mipmap.default_head)
            text!!.setText(paperPlane.text)
            data = paperPlane
            if (PrivateRoomTools.PositionInOutSeat(paperPlane.mSelfPosition)) {
                root.isClickable = false
                replay_text.visibility = View.GONE
            } else {
                root.isClickable = true
                replay_text.visibility = View.VISIBLE
            }
            return old
        }
    }
}