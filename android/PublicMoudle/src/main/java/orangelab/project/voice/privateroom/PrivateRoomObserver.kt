package orangelab.project.voice.privateroom

import orangelab.project.voice.privateroom.view.*

/**
 *game_werewolf
 *2018/3/21 上午11:33
 *Mystery
 */
interface PrivateRoomObserver : PrivateRoomSpeakObserver, PrivateRoomTitleObserver, PrivateRoomMemberViewObserver
        , PrivateRoomLikeViewObserver,PrivateRoomPaperPlaneViewObserver ,PrivateRoomToolBoxViewObserver{

}