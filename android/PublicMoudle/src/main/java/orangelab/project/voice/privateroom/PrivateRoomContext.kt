package orangelab.project.voice.privateroom

import orangelab.project.common.context.IBaseContext
import orangelab.project.common.engine.IOrbitEngineObserver
import orangelab.project.common.model.EnterRoomResult
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData
import orangelab.project.voice.privateroom.model.PrivateRoomSeatData
import com.toolkit.action.Destroyable
import orangelab.project.voice.privateroom.model.TempPictureMessageItem


/**
 *game_werewolf
 *2018/3/19 下午12:13
 *Mystery
 */
interface PrivateRoomContext : IBaseContext, Destroyable {

    fun restoreContext()

    fun restoreData(key: String, data: Any)

    fun getRestoreData(key: String): Any?

    fun refresh(enterRoomResult: EnterRoomResult)

    fun bindObserver(observer: PrivateRoomContextObserver)

    fun unBindObserver(observer: PrivateRoomContextObserver)

    fun registerAudioObserver(orbitEngineObserver: IOrbitEngineObserver)

    fun unregisterAudioObserver(orbitEngineObserver: IOrbitEngineObserver)

    fun openAudioEngine()

    fun closeAudioEngine(complete: Runnable?)

    fun addTempPictureItem(tempPictureMessageItem: TempPictureMessageItem)

    fun setMinimize(flag: Boolean)

    fun getMinimize(): Boolean

    fun getSelfPosition(): Int

    fun getMember(position: Int): PrivateRoomMemberData?

    fun getMember(userId: String): PrivateRoomMemberData?

    fun getSeatsData(): MutableList<PrivateRoomSeatData>

    fun getSeatData(position: Int): PrivateRoomSeatData?

    fun getAllMembers(): MutableList<PrivateRoomMemberData>

    fun getAllMembersOutSeats(): MutableList<PrivateRoomMemberData>

    fun getAllMembersInSeats(): MutableList<PrivateRoomMemberData>

    fun getAllMembersWithOutMySelf(): MutableList<PrivateRoomMemberData>

    fun getAllMembersForShow(): MutableList<PrivateRoomMemberData>

    fun getChatMessageList(): List<Any>

    fun getRoomId(): String

    fun getRoomPassword(): String

    fun getOnlineNumber(): Int

    fun getLikeCount(): Long

    fun getTag(): String

    fun getRoomTitle(): String

    fun getRoomIntroduce(): String

    fun getTagColor(): String

    fun getRoomType(): String

    fun getMasterMember(): PrivateRoomMemberData?

    fun getSelfMember(): PrivateRoomMemberData?

    fun getBackGroundType(): String

    fun getBackGroundUrl(): String

    fun getBlockUpSeatTime(): Long

    fun getRequestFriendUserFilter(): Set<String>

    fun isBackGroundChanged(): Boolean

    fun isMaster(): Boolean

    fun isDeputy(): Boolean

    fun isSelfFreeStyle(): Boolean

    fun isSelfBanMic(): Boolean

    fun isLocked(position: Int): Boolean

    fun isSelfPosition(position: Int): Boolean

    fun isSelf(userId: String): Boolean

    fun isSelfUpSeat(): Boolean

    fun isSelfOutSeat(): Boolean

    fun isDeputyPosition(position: Int): Boolean

    fun isUpSeat(position: Int): Boolean

    fun isOutSeat(position: Int): Boolean

    fun isAudioConnected(): Boolean

    fun isSocketConnected(): Boolean

    fun isSendTempImageEnable(): Boolean

    fun isMiniEnable(): Boolean

    fun destroy(complete: Runnable?)

    fun getRoomConfig(): EnterRoomResult.RoomConfig?

}