package orangelab.project.voice.data;

import android.text.TextUtils;

import com.androidtoolkit.PLog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.SystemMessageItem;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.ds.LimitMap;
import orangelab.project.voice.lobby.api.LobbyRoomConstants;
import orangelab.project.voice.lobby.data.LobbyRoomConfig;
import orangelab.project.voice.lobby.event.LobbyEvent;
import orangelab.project.voice.manager.datamanager.IDataSourceOption;
import orangelab.project.voice.model.VoiceAddFriendBean;
import orangelab.project.voice.model.VoiceRoomAudioConfigBean;
import orangelab.project.voice.utils.PositionHelper;

/**
 * Created by lloydfinch on 28/12/2017.
 * //语音房数据源,不包括房间配置信息,房间配置相关数据在VoiceRoomConfig
 */

public class VoiceRoomDataSource implements IDataSourceOption {

    private boolean isSync = true;//是否开启同步,同步:添加在线人数时，会自动判断是否在麦上然后添加到对应的列表
    private List<EnterRoomResult.EnterRoomUserItem> onLineUsers;//在线人列表
    private List<EnterRoomResult.EnterRoomUserItem> onSeatUsers;//麦上人列表
    private List<EnterRoomResult.EnterRoomUserItem> downSeatUsers;//麦下人列表
    private List<Integer> lockedSeatList;//加锁座位列表
    private LimitMap<Long, SystemMessageItem> messageMap;//消息列表
    private List<VoiceAddFriendBean> addFriendDialogIds;//加好友的id列表
    private int likeCount = 0;//点赞数量

    //TODO 放在config中
    private VoiceRoomAudioConfigBean configBean;//音频初始化需要

    public VoiceRoomDataSource() {
        super();
        init();
    }

    private void init() {
        onLineUsers = new ArrayList<>();
        onSeatUsers = new ArrayList<>();
        downSeatUsers = new ArrayList<>();
        lockedSeatList = new ArrayList<>();
        messageMap = new LimitMap<>();
        addFriendDialogIds = new ArrayList<>();
        configBean = new VoiceRoomAudioConfigBean();
    }

    @Override
    public void init(EnterRoomResult result) {
        clearOnLineUsers();
        Iterator iterator = result.room.users.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            EnterRoomResult.EnterRoomUserItem member = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            addUser(member);
        }

        configBean.roomId = result.room.room_id;
        configBean.mediaServer = result.media_server;

        setLockedSeatList(result.room.locked_positions);
    }

    @Override
    public void setSync(boolean isSync) {
        this.isSync = isSync;
    }

    @Override
    public void addUser(EnterRoomResult.EnterRoomUserItem user) {
        PLog.e("addUser", "add user: " + user);
        onLineUsers.add(user);
        if (isSync) {
            if (user != null) {
                if (PositionHelper.isUpSeat(user.position)) {
                    addOnSeatUser(user);
                } else {
                    addDownSeatUser(user);
                }
            }
        }
    }

    @Override
    public void addOnSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        onSeatUsers.add(user);
    }

    @Override
    public void addDownSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        downSeatUsers.add(user);
    }

    @Override
    public void removeUser(EnterRoomResult.EnterRoomUserItem user) {
        PLog.e("addUser()", "remove user: " + user);
        onLineUsers.remove(user);
        if (isSync) {
            removeOnSeatUser(user);
            removeDownSeatUser(user);
        }
    }

    @Override
    public void removeOnSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        onSeatUsers.remove(user);
    }

    @Override
    public void removeDownSeatUser(EnterRoomResult.EnterRoomUserItem user) {
        downSeatUsers.remove(user);
    }

    @Override
    public void clearOnLineUsers() {
        onLineUsers.clear();
        onSeatUsers.clear();
        downSeatUsers.clear();
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getOnLineUsers() {
        return onLineUsers;
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getOnSeatUsers() {
        return onSeatUsers;
    }

    @Override
    public List<EnterRoomResult.EnterRoomUserItem> getDownSeatUsers() {
        return downSeatUsers;
    }

    @Override
    public void setLockedSeatList(List<Integer> lockedSeatList) {
        this.lockedSeatList.clear();
        this.lockedSeatList.addAll(lockedSeatList);
    }

    @Override
    public List<Integer> getLockedSeatList() {
        return lockedSeatList;
    }

    @Override
    public void clearLockedSeatList() {
        lockedSeatList.clear();
    }

    @Override
    public void addLockedSeat(Integer position) {
        lockedSeatList.add(position);
    }

    @Override
    public void removeLockedSeat(Integer position) {
        lockedSeatList.remove(position);
    }

    @Override
    public EnterRoomResult.EnterRoomUserItem findUserByPosition(int position) {
        for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
            if (user.position == position) {
                return user;
            }
        }
        return null;
    }

    @Override
    public EnterRoomResult.EnterRoomUserItem findUserById(String id) {
        for (EnterRoomResult.EnterRoomUserItem user : onLineUsers) {
            if (TextUtils.equals(user.id, id)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void addMessage(Long key, SystemMessageItem msg) {
        messageMap.put(key, msg);
    }

    @Override
    public Collection<SystemMessageItem> getMessageList() {
        return messageMap.values();
    }

    @Override
    public SystemMessageItem getLatestMessage(Long key) {
        return messageMap.get(key);
    }

    @Override
    public void addFriendDialog(VoiceAddFriendBean bean) {
        addFriendDialogIds.add(bean);
    }

    @Override
    public void removeAddFriend(String id) {
        for (VoiceAddFriendBean bean : addFriendDialogIds) {
            if (TextUtils.equals(bean.id, id)) {
                addFriendDialogIds.remove(bean);
            }
        }
    }

    @Override
    public void markAddFriendHandled(String id) {
        for (VoiceAddFriendBean bean : addFriendDialogIds) {
            if (TextUtils.equals(bean.id, id)) {
                bean.isHandle = true;
            }
        }
    }

    @Override
    public VoiceAddFriendBean getLatestAddFriendBean() {
        return addFriendDialogIds.get(addFriendDialogIds.size() - 1);
    }

    @Override
    public List<VoiceAddFriendBean> getAddFriendIdList() {
        return addFriendDialogIds;
    }

    @Override
    public void clearAddFriendDialogList() {
        addFriendDialogIds.clear();
    }

    @Override
    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    @Override
    public int getLikeCount() {
        return likeCount;
    }

    @Override
    public VoiceRoomAudioConfigBean getAudioConfig() {
        return configBean;
    }

    @Override
    public void clearAudioConfig() {
        configBean = null;
    }

    @Override
    public void filterSameLobbyGame(String userId, String gameType) {
        Collection<SystemMessageItem> messageList = new ArrayList<>(getMessageList());

        //删除此人发送的所有游戏邀请消息
        for (SystemMessageItem item : messageList) {
            EnterRoomResult.EnterRoomUserItem user = item.getUser();
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                if (user != null && TextUtils.equals(userId, user.id)) {
                    //删除相同类型的游戏
                    if (TextUtils.equals(gameType, item.getGameType())) {
                        messageMap.removeByValue(item);
                    }
                }
            }
        }

    }

    @Override
    public void filterLeaveType(String userId, String leaveType) {
        Collection<SystemMessageItem> messageList = new ArrayList<>(getMessageList());

        //删除此人发送的所有游戏邀请消息
        for (SystemMessageItem item : messageList) {
            EnterRoomResult.EnterRoomUserItem user = item.getUser();
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                if (user != null && TextUtils.equals(userId, user.id)) {
                    item.setLeaveType(leaveType);
                    if (TextUtils.equals(leaveType, LobbyRoomConstants.PLAY_GAME)) {
                        //离开房间参加小游戏,开启计时器倒计时删除消息
                        //这里不再需要倒计时
                        //String inviteId = item.getInvite_id();
                        //timerRemoveMessage(inviteId);
                    } else {
                        //正常离开房间，就删除此条消息
                        messageMap.removeByValue(item);
                    }
                }
            }
        }
    }

    @Override
    public void filterSpecialMessage(String userId, String inviteId) {
        Collection<SystemMessageItem> messageList = new ArrayList<>(getMessageList());

        //删除此人发送的所有游戏邀请消息
        for (SystemMessageItem item : messageList) {
            EnterRoomResult.EnterRoomUserItem user = item.getUser();
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                if (user != null && TextUtils.equals(userId, user.id)) {
                    //删除不是特定邀请的消息
                    if (!TextUtils.equals(inviteId, item.getInvite_id())) {
                        messageMap.removeByValue(item);
                    }
                }
            }
        }
    }

    @Override
    public void removeMessageById(String userId) {
        Collection<SystemMessageItem> messageList = new ArrayList<>(getMessageList());

        //删除此人发送的所有游戏邀请消息
        for (SystemMessageItem item : messageList) {
            EnterRoomResult.EnterRoomUserItem user = item.getUser();
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                if (user != null && TextUtils.equals(userId, user.id)) {
                    messageMap.removeByValue(item);
                }
            }
        }
    }

    @Override
    public void removeMessageByInviteId(String inviteId) {
        Collection<SystemMessageItem> messageList = new ArrayList<>(getMessageList());

        //删除此人发送的所有游戏邀请消息
        for (SystemMessageItem item : messageList) {
            if (SystemMessageItem.TYPE_LOBBY == item.getType()) {
                if (TextUtils.equals(inviteId, item.getInvite_id())) {
                    messageMap.removeByValue(item);
                }
            }
        }
    }

    @Override
    public void destroy() {
        timerHandler.release();
        clearOnLineUsers();
        lockedSeatList.clear();
        messageMap.clear();
        addFriendDialogIds.clear();
        likeCount = 0;
    }

    private void timerRemoveMessage(String inviteId) {
        timerHandler.postDelaySafely(() -> {
                    removeMessageByInviteId(inviteId);
                    EventBus.getDefault().post(new LobbyEvent.LobbyRemoveMessageEvent());
                },
                LobbyRoomConfig.maxMessageLeaveTime);
    }

    //用于所有消息的倒计时删除
    private SafeHandler timerHandler = new SafeHandler();

}
