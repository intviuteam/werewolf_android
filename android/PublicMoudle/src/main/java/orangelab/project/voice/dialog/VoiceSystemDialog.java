package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.CountDownView;

/**
 * Created by lloydfinch on 17/11/2017.
 */

public class VoiceSystemDialog extends SafeDialog implements CountDownView.CountDownFinish {

    private Button btnOK;
    private TextView mTextView;
    private TextView mCountDown;
    private CountDownView countDownView;

    public VoiceSystemDialog(@NonNull Context context, String msg) {
        super(context, R.style.radius_dialog);
        setContentView(R.layout.layout_dialog_voice_system_msg);
        btnOK = (Button) findViewById(R.id.btn_sure);
        mCountDown = (TextView) findViewById(R.id.tv_countdown);
        mTextView = (TextView) findViewById(R.id.tv_msg);
        mTextView.setText(msg);
        btnOK.setOnClickListener(v -> dismiss());
    }

    public void showDialog(int seconds) {
        if (!isShowing()) {
            show();
        }
        if (seconds != 0) {
            mCountDown.setVisibility(View.VISIBLE);
            countDownView = new CountDownView(mCountDown, seconds, this);
            countDownView.start();
        } else {
            mCountDown.setVisibility(View.GONE);
        }
    }

    @Override
    protected void release() {
        if (countDownView != null) {
            countDownView.cancel();
        }
    }

    @Override
    public void onFinish() {
        if (isShowing()) {
            dismiss();
        }
    }
}
