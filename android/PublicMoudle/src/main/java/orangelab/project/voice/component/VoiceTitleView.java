package orangelab.project.voice.component;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.datasource.GlobalUserState;

import cn.intviu.support.GsonHelper;
import orangelab.project.ProjectConfig;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.BasicTipDialog;
import orangelab.project.voice.dialog.VoiceNewSettingDialog;
import orangelab.project.voice.dialog.VoiceSettingDialog;
import orangelab.project.voice.dialog.VoiceSpySettingDialog;
import orangelab.project.voice.dialog.VoiceSpySystemMsgDialog;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.manager.datamanager.VoiceRoomDataSourceManager;
import orangelab.project.voice.model.VoiceAssignedRole;
import orangelab.project.voice.model.VoiceSpyBeforeVote;
import orangelab.project.voice.model.VoiceSpyGameDeathInfo;
import orangelab.project.voice.model.VoiceSpyGameOverBean;
import orangelab.project.voice.model.VoiceSpyGuessWord;
import orangelab.project.voice.model.VoiceSpyGuessWordResult;
import orangelab.project.voice.model.VoiceSpySpeech;
import orangelab.project.voice.model.VoiceSpyVoteResult;
import orangelab.project.voice.model.VoiceWords;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.TransferClassHelper;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public class VoiceTitleView extends AbstractSocketMessageHandler implements VoiceTitleViewComponent {

    private static final String TAG = "VoiceTitleView";
    private VoiceTitleViewUI voiceTitleViewUI;

    private Context mContext;
    private String roomId;
    private String curTitle;
    private String titleType = "";
    private String curPassword;

    private BasicTipDialog dialog;
    private VoiceSpySettingDialog voiceSpySettingDialog;

    public VoiceTitleView(Context context, ViewGroup contentView) {
        this.mContext = context;
        voiceTitleViewUI = new VoiceTitleViewUI(context, contentView);
        initEvent();
    }

    @Override
    public void displayData() {
        String roomId = VoiceRoomConfig.getRoomId();
        String title = VoiceRoomConfig.getRoomTitle();
        String type = VoiceRoomConfig.getRoomType();
        String password = VoiceRoomConfig.getPsw();

        setRoomId(roomId);
        updateTitle(title, type);
        setPassword(password);
    }

    @Override
    public void init(String roomId, String title, String password, String type) {
        setRoomId(roomId);
        updateTitle(title, type);
        setPassword(password);
    }

    private void initEvent() {
        initSpyGameListener();
    }

    @Override
    public void setRoomId(String roomId) {
        this.roomId = roomId;
        voiceTitleViewUI.setRoomId(roomId);
        VoiceRoomConfig.setRoomId(roomId);
    }

    @Override
    public void updateTitle(String title, String type) {
        this.curTitle = title;
        this.titleType = TextUtils.isEmpty(type) ? "" : type;
        voiceTitleViewUI.updateTitle(title, type);
        VoiceRoomConfig.setRoomType(type);
        VoiceRoomConfig.setRoomTitle(title);
        if (TextUtils.equals(VoiceConstants.SPY_TAG, type)) {
            voiceTitleViewUI.showSpyButton();
        } else {
            voiceTitleViewUI.hideSpyButton();
        }
    }

    @Override
    public void updateOnLineNumber(int number) {
        voiceTitleViewUI.updateOnLineNumber(number);
        VoiceRoomConfig.setOnLineNumber(number);
    }

    @Override
    public void updateWifiState(boolean isConnected) {
        voiceTitleViewUI.updateWifiState(isConnected);
    }

    @Override
    public void onBackPressed(View.OnClickListener onBackPressedListener) {
        if (onBackPressedListener != null) {
            voiceTitleViewUI.getIvBack().setOnClickListener(onBackPressedListener);
        }
    }

    @Override
    public void onOnLineNumberClicked(View.OnClickListener onOnLineNumberClickedListener) {
        if (onOnLineNumberClickedListener != null) {
            voiceTitleViewUI.getLlOnLineContainer().setOnClickListener(onOnLineNumberClickedListener);
        }
    }

    @Override
    public void onSettingClicked(View.OnClickListener onSettingClickedListener) {
        if (onSettingClickedListener != null) {
            voiceTitleViewUI.getIvSetting().setOnClickListener(onSettingClickedListener);
        } else {
            voiceTitleViewUI.getIvSetting().setOnClickListener((v) -> {
                VoiceSettingDialog voiceSettingDialog = new VoiceSettingDialog(mContext);
                voiceSettingDialog.setTopic(curTitle)
//                VoiceNewSettingDialog voicenewSettingDialog = new VoiceNewSettingDialog(mContext);
//                voicenewSettingDialog.setTopic(curTitle)
                        .setTopicType(getTitleType())
                        .setPassword(curPassword)
                        .show();
            });
        }
    }

    @Override
    public void onShareClicked(View.OnClickListener onShareClickedListener) {
        if (onShareClickedListener != null) {
            voiceTitleViewUI.getIvShare().setOnClickListener(onShareClickedListener);
        } else {
            voiceTitleViewUI.getIvShare().setOnClickListener((v) -> {

                String psw = TextUtils.isEmpty(curPassword) ? "" : curPassword;

                ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_FROM_VOICE);
                data.userName = GlobalUserState.getGlobalState().getUserName();
                data.roomId = roomId;
                data.password = psw;

                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onVoiceRoomShare(mContext, data);
            });
        }
    }

    @Override
    public void setPassword(String password) {
        this.curPassword = password;
        PersonalDataHelper.getPersonalData().setCurPassword(curPassword);
        GlobalUserState.getGlobalState().setPassword(password);
        VoiceRoomConfig.setPsw(password);
    }

    @Override
    public String getTitle() {
        return curTitle.trim();
    }

    @Override
    public String getTitleType() {
        return titleType.trim();
    }

    @Override
    public void updateMasterUI() {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public int getOnLineNumber() {
        return VoiceRoomDataSourceManager.getInstance().getOnLineUsers().size();
    }

    //谁是卧底的设置和开始游戏的点击事件
    private void initSpyGameListener() {
        //设置
        voiceTitleViewUI.getBtnSpySetting().setOnClickListener(v -> {
            int style = R.style.radius_dialog;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                style = R.style.radius_dialog_material;
            }

            voiceSpySettingDialog = new VoiceSpySettingDialog(mContext, style);
            voiceSpySettingDialog.show();
            VoiceSocketHelper.sendSpySettingStart();
        });
        //开始
        voiceTitleViewUI.getBtnSpyStart().setOnClickListener(v -> {
            //游戏开始，解除禁麦和自由模式
            for (int position = 0; position <= VoiceRoomConfig.getMaxChairNumber(); position++) {
                RoomSocketEngineHelper.sendUnBanMic(position);
                RoomSocketEngineHelper.sendUnFreeStyle(position);
            }

            VoiceSocketHelper.sendSpyGameStart(null, result -> {
                UIActuator.post(() -> ToastToolKit.showShort(result));
            });
        });
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }

        if (voiceSpySettingDialog != null) {
            if (voiceSpySettingDialog.isShowing()) {
                voiceSpySettingDialog.dismiss();
            }
            voiceSpySettingDialog = null;
        }

        voiceTitleViewUI.destroy();
    }

    @Override
    public void handleServerEvent(ServerMessageEventOver event) {
        PLog.e(TAG, "event:" + event);
        handleMessageEvent(event);
    }

    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson
                (messageBean.payload.toString(), ServerMessageEnterResult.class);
        EnterRoomResult.EnterRoomUserItem member = TransferClassHelper.EnterResult2EnterRoomUser(item);
        voiceTitleViewUI.showComeIn(member.name);
        updateOnLineNumber(getOnLineNumber());
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleLeave(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

    @Override
    public void handleChangePassword(VoiceMessageBean messageBean) {
        String password = messageBean.payload.optString(VoiceConstants.PASSWORD);
        setPassword(password);
    }

    @Override
    public void handleUpdateTitle(VoiceMessageBean messageBean) {
        String title = VoiceRoomConfig.getRoomTitle();
        String type = VoiceRoomConfig.getRoomType();
        updateTitle(title, type);
    }

    @Override
    public void handleForceSeat(VoiceMessageBean messageBean) {
        handleUpSeat(messageBean);
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        if (PositionHelper.isSelf(userId)) {
            //防止server返回非法数据
            if (PositionHelper.isUpSeat()) {
                showBeUpSeat();
            }
        }
    }

    //提示被抱上麦
    private void showBeUpSeat() {
        if (dialog == null) {
            dialog = new BasicTipDialog(mContext);
            dialog.setTitle(MessageUtils.getString(R.string.str_voice_tips))
                    .setMsg(MessageUtils.getString(R.string.str_be_up_seat))
                    .setImage(R.mipmap.vocie_be_seat_bg)
                    .positive();
        }
        if (dialog.isShowing()) {
            return;
        }
        dialog.show();
    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        voiceTitleViewUI.showMasterUI();
    }

    @Override
    public void handleKickOut(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

    @Override
    public void handleGetWords(VoiceMessageBean messageBean) {
        VoiceWords words = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceWords.class);
        if (voiceSpySettingDialog != null && voiceSpySettingDialog.isShowing()) {
            voiceSpySettingDialog.handleGetSystemWords(words);
        }
    }

    @Override
    public void handleSpyGameStart(VoiceMessageBean messageBean) {
        if (!VoiceRoomConfig.isIsPlaying()) {
            return;
        }
        //开始游戏就不能再设置
        voiceTitleViewUI.hideSpyButton();
        voiceTitleViewUI.hideSetting();

        //房主显示状态卡
        if (PositionHelper.isMaster()) {
            voiceTitleViewUI.showMasterCard(MessageUtils.getString(R.string.str_voice_moment_describe), VoiceRoomConfig.SpyConfig.getFirst(), VoiceRoomConfig.SpyConfig.getSecond(), 5);
        } else if (!PositionHelper.isUpSeat()) {
            //麦下人视角
            voiceTitleViewUI.showTipMessage(MessageUtils.getString(R.string.str_voice_moment_describe), MessageUtils.getString(R.string.str_voice_player_des_ing), null);
        }
    }

    @Override
    public void handleAssignedRole(VoiceMessageBean messageBean) {
        VoiceAssignedRole role = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceAssignedRole.class);
        //玩家显示状态卡
        if (PositionHelper.isPlayer()) {
            voiceTitleViewUI.showPlayerCard(MessageUtils.getString(R.string.str_voice_moment_describe), role.role, false, role.duration / 1000);
        }
    }

    @Override
    public void handleSpyGameSpeech(VoiceMessageBean messageBean) {
        if (PositionHelper.isMaster() || !PositionHelper.isUpSeat()) {
            showStatusCardByRole(MessageUtils.getString(R.string.str_voice_moment_describe), 0);
        } else {
            String role = PersonalDataHelper.getPersonalData().getSelfRole();
            voiceTitleViewUI.showPlayerCard(MessageUtils.getString(R.string.str_voice_moment_describe), role, false, -1);
            VoiceSpySpeech spySpeech = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpySpeech.class);
            if (PositionHelper.isSelfPosition(spySpeech.position)) {
                if (!spySpeech.dead) {
                    voiceTitleViewUI.showPlayerCard(MessageUtils.getString(R.string.str_voice_moment_describe), role, true, spySpeech.duration / 1000);
                }
            }
        }
    }

    @Override
    public void handleBeforeVote(VoiceMessageBean messageBean) {
        voiceTitleViewUI.cancelAlarm();//取消计时器
        VoiceSpyBeforeVote vote = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyBeforeVote.class);
        String role = PersonalDataHelper.getPersonalData().getSelfRole();
        if (PositionHelper.isPlayer()) {
            //玩家视角
            voiceTitleViewUI.showPlayerCard(MessageUtils.getString(R.string.str_voice_moment_vote), role, true, vote.duration / 1000);
        } else if (PositionHelper.isMaster()) {
            //房主视角
            voiceTitleViewUI.showMasterCard(MessageUtils.getString(R.string.str_voice_moment_vote), VoiceRoomConfig.SpyConfig.getFirst(), VoiceRoomConfig.SpyConfig.getSecond(), vote.duration / 1000);
        } else {
            //麦下人视角
            voiceTitleViewUI.showTipMessage(MessageUtils.getString(R.string.str_voice_moment_vote), MessageUtils.getString(R.string.str_voice_player_vote_ing), null);
        }
    }

    @Override
    public void handleSpyGameVoteResult(VoiceMessageBean messageBean) {
        //只有在麦上的人才能看到
        VoiceSpyVoteResult voteResult = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyVoteResult.class);
        if (TextUtils.equals(voteResult.type, "death")) {
            if (voteResult.finished) {
                voiceTitleViewUI.getStatusCardView().setVisibility(View.GONE);//隐藏掉投票进度
                int duration = voteResult.duration / 1000;
                if (voteResult.positions != null && voteResult.positions.size() > 0) {
                    if (voteResult.need_pk) {
                        if (PositionHelper.isPlayer()) {
                            VoiceSpySystemMsgDialog spySystemMsgDialog = new VoiceSpySystemMsgDialog(mContext);
                            spySystemMsgDialog.showPK(voteResult.positions);
                            spySystemMsgDialog.startCount(duration, () -> spySystemMsgDialog.dismiss());
                        }

                        //更改状态卡
                        voiceTitleViewUI.showPK(voteResult.positions, duration,
                                () -> showStatusCardByRole(MessageUtils.getString(R.string.str_voice_moment_describe), duration));
                    } else {
                        //有人死亡，跳转到death_info处理

                    }
                } else {
                    //无人投票
                    PLog.e(TAG, "give up vote: " + duration);
                    voiceTitleViewUI.showVoteResult(-1, "", duration,
                            () -> {
                                //voiceTitleViewUI.getStatusCardView().setVisibility(View.GONE);
                                showStatusCardByRole(MessageUtils.getString(R.string.str_voice_moment_describe), duration);
                            });
                    if (PositionHelper.isPlayer()) {
                        VoiceSpySystemMsgDialog spySystemMsgDialog = new VoiceSpySystemMsgDialog(mContext);
                        spySystemMsgDialog.showGiveUp();
                        spySystemMsgDialog.startCount(duration, () -> spySystemMsgDialog.dismiss());
                    }
                }
            }
        }
    }

    @Override
    public void handleDeathInfo(VoiceMessageBean messageBean) {
        VoiceSpyGameDeathInfo deathInfo = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameDeathInfo.class);
        if (deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
            VoiceSpyGameDeathInfo.DeathInfo info = deathInfo.death_info.get(0);
            int outPosition = info.killed;
            int duration = deathInfo.duration / 1000;
            String role = TextUtils.equals(VoiceConstants.SPY_TAG, info.role) ?
                    MessageUtils.getString(R.string.str_voice_role_spy) :
                    MessageUtils.getString(R.string.str_voice_role_people);
            voiceTitleViewUI.showVoteResult(outPosition, role, duration,
                    () -> showStatusCardByRole(MessageUtils.getString(R.string.str_voice_moment_describe), duration));
            if (PositionHelper.isPlayer()) {
                VoiceSpySystemMsgDialog spySystemMsgDialog = new VoiceSpySystemMsgDialog(mContext);
                spySystemMsgDialog.showOut(outPosition, role);
                spySystemMsgDialog.startCount(duration, () -> spySystemMsgDialog.dismiss());
            }
        }
    }

    @Override
    public void handleSpyGameOver(VoiceMessageBean messageBean) {
        VoiceRoomConfig.setIsPlaying(false);
        //游戏结束可以设置
        voiceTitleViewUI.showMasterUI();
        voiceTitleViewUI.getStatusCardView().setVisibility(View.GONE);

        VoiceSpyGameOverBean gameOverBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGameOverBean.class);
        if (TextUtils.equals(VoiceConstants.SPY_TAG, gameOverBean.win_type)) {
            voiceTitleViewUI.gameOverSpyWin();
        } else if (TextUtils.equals(VoiceConstants.GOOD_TAG, gameOverBean.win_type)) {
            voiceTitleViewUI.gameOverGoodWin();
        }
    }

    @Override
    public void handleSpyGameGuessWord(VoiceMessageBean messageBean) {
        VoiceSpyGuessWord guessWord = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGuessWord.class);
        voiceTitleViewUI.showBombWord(guessWord.duration / 1000, () -> {
            //空方法,覆盖
        });
        if (PositionHelper.isSelfPosition(guessWord.position)) {
            VoiceSpySystemMsgDialog dialog = new VoiceSpySystemMsgDialog(mContext);
            dialog.showGuessWord();
            dialog.startCount(guessWord.duration / 1000, () -> {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            });
        }
    }

    @Override
    public void handleGuessWordResult(VoiceMessageBean messageBean) {
        VoiceSpyGuessWordResult wordResult = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpyGuessWordResult.class);
        voiceTitleViewUI.showBombWordResult(wordResult.duration / 1000, wordResult.success, () -> {
            //空方法,覆盖
        });
    }

    @Override
    public void handleUpdateConfig(VoiceMessageBean messageBean) {
        updateMasterUI();
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        voiceTitleViewUI.resetStatusCard();//重制状态卡
        voiceTitleViewUI.showMasterUI();//更新房主特权控件
        RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
        if (result.room_info != null) {
            curTitle = VoiceRoomConfig.getRoomTitle();
            titleType = VoiceRoomConfig.getRoomType();
            curPassword = VoiceRoomConfig.getPsw();

            updateTitle(curTitle, titleType);
            updateOnLineNumber(getOnLineNumber());
        }
        restoreSpeech(result.game_info);
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        updateOnLineNumber(getOnLineNumber());
    }

    //复原说话状态
    private void restoreSpeech(RestoreResult.RestoreRoleMessage game_info) {
        //正在游戏中才处理
        if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
            if (PositionHelper.isMaster() || !PositionHelper.isUpSeat()) {
                showStatusCardByRole(MessageUtils.getString(R.string.str_voice_moment_describe), 0);
            } else {
                if (game_info != null) {
                    if (game_info.speech_info != null && game_info.speech_info.current != null) {
                        int position = game_info.speech_info.current.position;//此人正在说话
                        int duration = game_info.speech_info.duration / 1000;
                        if (PositionHelper.isSelfPosition(position)) {
                            String role = PersonalDataHelper.getPersonalData().getSelfRole();
                            voiceTitleViewUI.showPlayerCard(MessageUtils.getString(R.string.str_voice_moment_describe), role, true, duration);
                        }
                    }
                }
            }
        }
    }

    //根据不同角色展示状态卡
    private void showStatusCardByRole(String title, int duration) {
        if (PositionHelper.isPlayer()) {
            String role = PersonalDataHelper.getPersonalData().getSelfRole();
            voiceTitleViewUI.showPlayerCard(title, role, false, duration);
        } else if (PositionHelper.isMaster()) {
            voiceTitleViewUI.showMasterCard(title, VoiceRoomConfig.SpyConfig.getFirst(), VoiceRoomConfig.SpyConfig.getSecond(), duration);
        } else {
            voiceTitleViewUI.showTipMessage(MessageUtils.getString(R.string.str_voice_moment_describe), MessageUtils.getString(R.string.str_voice_player_des_ing), null);
        }
    }

    @Override
    public void handleUnAdmin(VoiceMessageBean messageBean) {
        updateMasterUI();
    }

    @Override
    public void handleAdminList(VoiceMessageBean messageBean) {
        updateMasterUI();
    }

}
