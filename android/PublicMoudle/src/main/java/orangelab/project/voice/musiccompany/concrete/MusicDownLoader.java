package orangelab.project.voice.musiccompany.concrete;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.StreamToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.transport.TransportCompat;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;

import orangelab.project.MainApplication;
import orangelab.project.common.db.entity.MusicDataEntity;
import orangelab.project.voice.ds.Queue;
import orangelab.project.voice.musiccompany.service.MusicDownloadService;

/**
 *
 */
public class MusicDownLoader implements Keepable, Destroyable {

    private static final String TAG = "MusicDownLoader";

    //最大同时下载数量
    private static final int MAX_DOWNLOAD = 8;

    //连接/读取的超时时间
    private static final int TIMEOUT = 10000;

    //发布进度的间隔,越大更新的越快，越小越卡顿
    private static final int PROGRESS_INTERVAL = 20;

    //等待下载的队列
    private static Queue<MusicDownLoadTask> tasks = new Queue<>();
    //正在下载的队列
    private static Queue<MusicDownLoadAsyncTask> downloadingTasks = new Queue<>();

    /**
     * 启动服务去下载
     */
    public static void download(MusicDataEntity data, int position) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MainApplication.getInstance(), MusicDownloadService.class);
            intent.putExtra(MusicDownloadService.SERVICE_EXTRA_SIGNAL, data);
            intent.putExtra(MusicDownloadService.POSITION, position);
            MainApplication.getInstance().startService(intent);
        } else {
            ToastToolKit.showShort(R.string.str_music_can_not_down_load);
        }
    }

    /**
     * 真正的下载实现
     */
    public static synchronized boolean download(String url, MusicDownLoadCallback callback, int position) {
        MusicDownLoadTask task = new MusicDownLoadTask(url, callback, position);
        tasks.enQueue(task);
        if (canDownLoad()) {
            downLoadNext();
            return true;
        } else {
            task.callback.onWait();
        }
        return false;
    }

    /**
     * 进行下载
     */
    private static synchronized void downLoadNext() {
        if (canDownLoad()) {
            if (tasks.size() > 0) {
                MusicDownLoadAsyncTask task = new MusicDownLoadAsyncTask();
                downloadingTasks.enQueue(task);
                MusicDownLoadTask downLoadTask = tasks.poll();
                if (downLoadTask != null) {
                    task.executeOnExecutor(Executors.newFixedThreadPool(MAX_DOWNLOAD), downLoadTask);
                } else {
                    PLog.e(TAG, "download a null task");
                }
            }
        }
    }

    /**
     * 是否满足下载条件
     */
    private static synchronized boolean canDownLoad() {
        return downloadingTasks.size() <= MAX_DOWNLOAD;
    }

    /**
     * 销毁就退出下载
     */
    @Override
    public void destroy() {
        tasks.clear();
        downloadingTasks.clear();
    }

    static class MusicDownLoadAsyncTask extends AsyncTask<MusicDownLoadTask, Integer, byte[]> {

        private MusicDownLoadCallback callback;

        @Override
        protected byte[] doInBackground(MusicDownLoadTask... musicDownLoadTasks) {

            PLog.e(TAG, "come in doInBackground...");

            if (musicDownLoadTasks[0] != null) {
                callback = musicDownLoadTasks[0].callback;
                String path = musicDownLoadTasks[0].url;
                int position = musicDownLoadTasks[0].position;
                if (!TextUtils.isEmpty(path)) {

                    callback.onStart();
                    PLog.e(TAG, "start download...");
                    HttpURLConnection connection = null;
                    InputStream inputStream = null;
                    byte[] data = null;
                    try {
                        URL url = new URL(path);
                        connection = (HttpURLConnection) url.openConnection();
                        TransportCompat.compatURLConnection(connection);
                        connection.setRequestMethod("GET");
                        connection.setRequestProperty("Charset", "UTF-8");
                        connection.setRequestProperty("User-Agent", "ting_4.1.7(MI2, Android" + Build.VERSION.SDK_INT + ")");
                        connection.setConnectTimeout(TIMEOUT);
                        connection.setReadTimeout(TIMEOUT);
                        connection.setInstanceFollowRedirects(true);

                        //设置可读写
                        //connection.setDoInput(true);
                        //connection.setDoOutput(true);

                        //进行连接
                        connection.connect();
                        if (connection.getResponseCode() == 200) {
                            inputStream = connection.getInputStream();
                            int total = connection.getContentLength();
                            ByteArrayOutputStream baos = null;
                            if (inputStream != null) {
                                //更改buffer大小加快进度更新
                                byte[] buff = new byte[2048];
                                baos = new ByteArrayOutputStream();
                                int length = -1;
                                int count = 0;
                                try {
                                    while ((length = inputStream.read(buff)) != -1) {
                                        baos.write(buff, 0, length);

                                        count += length;

                                        publishProgress(count, total, position);
                                        //发布进度条间隔
                                        //Thread.sleep(PROGRESS_INTERVAL);
                                    }
                                    data = baos.toByteArray();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        baos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        StreamToolKit.close(inputStream);
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                    return data;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            if (callback != null && bytes != null) {
                callback.onSuccess(bytes);
            }
            downloadingTasks.poll();
            downLoadNext();

            PLog.e(TAG, "finish download...");
        }

        @Override
        protected void onCancelled() {
            callback.onFailure("task be cancelled!");
            downloadingTasks.poll();
            downLoadNext();
            PLog.e(TAG, "cancel download...");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            double count = values[0].doubleValue();
            double total = values[1].doubleValue();
            int progress = (int) ((count / total) * 100);
            int position = values[2];
            callback.onProgressUpdate(progress, position);
            PLog.e(TAG, "downloading..., current progress: " + progress);
        }
    }

    /**
     * 音乐下载任务
     */
    static class MusicDownLoadTask {
        public String url;
        public MusicDownLoadCallback callback;
        public int position;

        public MusicDownLoadTask() {
        }

        public MusicDownLoadTask(String url, MusicDownLoadCallback callback) {
            this.url = url;
            this.callback = callback;
        }

        public MusicDownLoadTask(String url, MusicDownLoadCallback callback, int position) {
            this.url = url;
            this.callback = callback;
            this.position = position;
        }
    }

    /**
     * 下载完毕回调
     */
    public interface MusicDownLoadCallback {

        void onStart();

        void onWait();

        void onPause();

        void onSuccess(byte[] data);

        void onFailure(String message);

        void onProgressUpdate(int progress, int position);
    }
}
