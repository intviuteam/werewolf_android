package orangelab.project.voice.musiccompany

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import kotlinx.android.synthetic.main.activity_music_stock.*
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.musiccompany.adapter.MusicStockFragmentAdapter
import orangelab.project.voice.musiccompany.api.MusicSocketHelper
import orangelab.project.voice.musiccompany.concrete.MusicCompanyDataSource
import orangelab.project.voice.musiccompany.event.MusicEvent
import orangelab.project.voice.musiccompany.fragment.MusicOrderedFragment
import orangelab.project.voice.musiccompany.fragment.MusicStockFragment
import orangelab.project.voice.musiccompany.model.MusicOrderSong
import orangelab.project.voice.utils.TransferClassHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 库存列表: 曲库/已点
 */
class MusicStockActivity : AppCompatActivity() {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    companion object {
        const val TAG = "MusicStockActivity"

        fun Launch(context: Context) {
            context.startActivity(Intent(context, MusicStockActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_stock)

        EventBus.getDefault().register(this)
        initView()
        initWindow()
        initTabLayout()
        initViewPager()
    }

    private fun initView() {
        this.tabLayout = this.findViewById(R.id.tl_music_stock_tab_layout) as TabLayout
        this.viewPager = this.findViewById(R.id.vp_music_stock_view_pager) as ViewPager

        this.iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initWindow() {
        //屏幕常亮
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
    }

    private fun initTabLayout() {
        this.tabLayout.tabMode = TabLayout.MODE_FIXED
        this.tabLayout.setupWithViewPager(viewPager)
        tabLayout.addTab(tabLayout.newTab())
        tabLayout.addTab(tabLayout.newTab())
    }

    private fun initViewPager() {
        val fragments: ArrayList<Fragment> = ArrayList()
        fragments.add(MusicStockFragment())
        fragments.add(MusicOrderedFragment())
        var adapter = MusicStockFragmentAdapter(supportFragmentManager, fragments)
        adapter.titles = arrayListOf(MessageUtils.getString(R.string.str_music_song_stock),
                MessageUtils.getString(R.string.str_music_stock_local))
        viewPager.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    /**
     * 点击了排麦按钮
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MusicEvent.MusicOrderEvent) {

        //添加到歌单
        val song: MusicOrderSong = event.orderSong

        PLog.e(TAG, "send order song: $song")


    }
}
