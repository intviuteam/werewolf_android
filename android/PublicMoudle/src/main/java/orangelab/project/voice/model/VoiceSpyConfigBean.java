package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * "谁是卧底"配置类
 */

public class VoiceSpyConfigBean implements Serializable, Keepable {

    public boolean more_undercover;//是否添加了卧底
    public boolean has_ghost;//是否设置卧底为幽灵
    public boolean can_cut_speaker;//可否插麦
    public boolean can_guess;//可否猜词
    public String first;
    public String second;

    public VoiceSpyConfigBean(boolean more_undercover, boolean has_ghost, boolean can_cut_speaker, boolean can_guess) {
        this.more_undercover = more_undercover;
        this.has_ghost = has_ghost;
        this.can_cut_speaker = can_cut_speaker;
        this.can_guess = can_guess;
    }

    public VoiceSpyConfigBean() {
    }

    @Override
    public String toString() {
        return "VoiceSpyConfigBean{" +
                "more_undercover=" + more_undercover +
                ", has_ghost=" + has_ghost +
                ", can_cut_speaker=" + can_cut_speaker +
                ", can_guess=" + can_guess +
                ", first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
