package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 04/12/2017.
 */

public class VoiceSpySpeech implements Serializable, Keepable {

    public int position;
    public int duration;
    public boolean dead;
    public String type;
    public int lefts;
    public int loop_index;

    @Override
    public String toString() {
        return "VoiceSpySpeech{" +
                "position=" + position +
                ", duration=" + duration +
                ", dead=" + dead +
                ", type='" + type + '\'' +
                ", lefts=" + lefts +
                ", loop_index=" + loop_index +
                '}';
    }
}
