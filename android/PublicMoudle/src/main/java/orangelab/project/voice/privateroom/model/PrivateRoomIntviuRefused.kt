package orangelab.project.voice.privateroom.model

import com.toolkit.action.Keepable

/**
 *game_werewolf
 *2018/4/16 下午5:11
 *Mystery
 */
class PrivateRoomIntviuRefused : Keepable {

    var type: String = "invite_be_refused"
    var invite_user_id: String = ""
    var target_user_id: String = ""
    var room_type = "privacy"
    var target_user: PrivateRoomIntviuTarget? = null


    class PrivateRoomIntviuTarget : Keepable {
        var id: String = ""
        var name: String = ""
        var avatar: String = ""
        var sex: Int = 1
    }
}