package orangelab.project.voice.privateroom

import android.Manifest
import android.content.Intent
import android.graphics.Color
import android.graphics.PointF
import android.os.Build
import android.speech.tts.Voice
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import cn.intviu.orbit.OrbitError
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.RxToolKit
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.view.ScreenUtils
import com.datasource.GlobalUserState
import orangelab.project.common.floatwindow.FloatWindowCommander
import orangelab.project.common.floatwindow.model.FloatUser
import orangelab.project.MainApplication
import com.R
import com.androidtoolkit.transport.TransportHelper
import com.networktoolkit.transport.Constant
import orangelab.project.common.activity.SafeActivity
import orangelab.project.common.engine.IOrbitEngineObserver
import orangelab.project.common.event.SocketEvent
import orangelab.project.common.exhibition.ExhibitionsActivity
import orangelab.project.common.effect.gifts.GiftAnimConstant
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.model.action.*
import orangelab.project.common.tool.DialogManager
import orangelab.project.common.tool.WrapImagePicker
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.voice.activity.VoiceRoomBackGroundActivity
import orangelab.project.voice.api.VoiceSocketHelper
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.manager.VoiceRoomCommander
import orangelab.project.voice.model.VoiceRoomBackGroundApiResult
import orangelab.project.voice.privateroom.model.*
import orangelab.project.voice.privateroom.view.*
import com.tbruyelle.rxpermissions.RxPermissions
import com.toolkit.ToolKit
import com.toolkit.action.Action
import com.toolkit.action.Destroyable
import orangelab.project.ProjectConfig
import orangelab.project.common.effect.AnimationViewGroup
import orangelab.project.common.effect.EffectsUtils
import orangelab.project.common.effect.emotion.EmotionManiFest
import orangelab.project.common.effect.event.RoomEffectEvent
import orangelab.project.common.engine.GlobalSocketEngine
import orangelab.project.game.dialog.WereWolfSystemMsgDialog
import orangelab.project.game.utils.WereWolfString

import orangelab.project.common.share.ShareBridgeData
import orangelab.project.common.share.SharePoolManager
import orangelab.project.common.tool.PopWindowManager
import orangelab.project.common.utils.KeyBoardChangeListener
import orangelab.project.voice.dialog.*
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent
import org.greenrobot.eventbus.EventBus
import rx.functions.Action1

/**
 *game_werewolf
 *2018/3/19 下午12:07
 *Mystery
 *
 * 统一的UI管理器
 */

class PrivateRoomController : PrivateRoomContextObserver, Destroyable, PrivateRoomObserver, IOrbitEngineObserver {

    companion object {
        val USER_ACTION_DIALOG = "USER_ACTION_DIALOG"
        val USER_EXIT_DIALOG = "USER_EXIT_DIALOG"
        val USER_FREESTYLE_DIALOG = "USER_FREESTYLE_DIALOG"
        val TEMP_PICTURE_DIALOG = "TEMP_PICTURE_DIALOG"
        val SYSTEM_INFO_DIALOG = "SYSTEM_INFO_DIALOG"

        val GAME_EMOTICON_POPWINDOW = "GAME_EMOTICON_POPWINDOW"
        val VOICE_GAME_SCORE_DIALOG = "VOICE_GAME_SCORE_DIALOG"
        val VOICE_GAME_APPLAUSE_DIALOG = "VOICE_GAME_APPLAUSE_DIALOG"

    }

    private val FORCE_EXIT_REASON_NONE = 0
    private val FORCE_EXIT_REASON_RECONNECT_FAILED = 1
    private val FORCE_EXIT_REASON_KICK_OUT = 2
    private val FORCE_EXIT_REASON_PERMISSION = 3

    private var mActivity: SafeActivity?
    private var mRootView: ViewGroup?
    private var mMainView: ViewGroup?
    private var mPrivateRoomLightContext: PrivateRoomContext?

    private var mGiftAnimLayer: AnimationViewGroup?
    private var mGiftBoardLayer: Destroyable?
    private var mBackGround: ImageView

    private var mPrivateRoomUITitle: PrivateRoomTitleView
    private var mPrivateRoomUISpeak: PrivateRoomSpeakView
    private var mPrivateRoomMessageView: PrivateRoomMessageView
    private var mPrivateRoomMembersView: PrivateRoomMembersView
    //    private var mPrivateRoomLikeView: PrivateRoomLikeView
    private var mPrivateRoomPaperPlaneView: PrivateRoomPaperPlaneView
    private var mPrivateRoomToolBoxView: PrivateRoomToolBoxView

    private var mRxPermissions: RxPermissions? = null

    private var mSpeakViewPopWindow: PrivateRoomSpeakPopWindow? = null

    private var mDialogManager: DialogManager
    private var mPopWindowManager: PopWindowManager

    private var mImagePicker: WrapImagePicker = WrapImagePicker()


    private var mKeyBoardListener: KeyBoardChangeListener? = null

    constructor(activity: SafeActivity, rootView: ViewGroup, privateRoomLightContext: PrivateRoomContext) {
        mActivity = activity;
        mRootView = rootView
        mMainView = mRootView!!.findViewById<ViewGroup>(R.id.id_main_view) as ViewGroup
        mDialogManager = DialogManager()
        mPopWindowManager = PopWindowManager()
        mPrivateRoomLightContext = privateRoomLightContext
        mPrivateRoomLightContext?.bindObserver(this)
        mPrivateRoomLightContext?.registerAudioObserver(this)
        mImagePicker.initImageConfig(700, 700)

        /**
         * 初始化窗口
         */
        initWindow()

        mGiftAnimLayer = rootView.findViewById<AnimationViewGroup>(R.id.giftAnimationView) as AnimationViewGroup
        /**
         * 恢复加好友过滤名单
         */
        mGiftAnimLayer?.requestFriendManager?.restoreUserFilter(privateRoomLightContext.getRequestFriendUserFilter())

        mGiftBoardLayer = (rootView.findViewById<View>(R.id.secondGiftAnimation)) as Destroyable
        mBackGround = rootView.findViewById<ImageView>(R.id.id_background) as ImageView


        /**
         * 初始化头部布局
         */
        mPrivateRoomUITitle = PrivateRoomTitleView(mRootView!!, this)

        mPrivateRoomUITitle.setLikeNumber(privateRoomLightContext.getLikeCount())
        mPrivateRoomUITitle.setRoomId(privateRoomLightContext.getRoomId())
        mPrivateRoomUITitle.setOnlineNumber(privateRoomLightContext.getOnlineNumber())
        mPrivateRoomUITitle.audioWifiState(!privateRoomLightContext.isAudioConnected())
        if (!privateRoomLightContext.isMiniEnable()) {
            mPrivateRoomUITitle.goneMiniMode()
        }

        /**
         * 初始化人员管理器
         */
        mPrivateRoomMembersView = PrivateRoomMembersView()
        mPrivateRoomMembersView.init(mRootView!!, privateRoomLightContext, this)

        /**
         * 初始化聊天信息布局
         */
        mPrivateRoomMessageView = PrivateRoomMessageView(mRootView!!, mDialogManager, privateRoomLightContext.getChatMessageList(), this)
        mPrivateRoomMessageView.refresh()

        /**
         * 初始化like
         */
//        mPrivateRoomLikeView = PrivateRoomLikeViewImpl(mRootView!!, this)

        /**
         * 初始化底部布局
         */
        mPrivateRoomUISpeak = PrivateRoomSpeakView(mRootView!!, this)

        /**
         * 纸飞机管理器
         */
        mPrivateRoomPaperPlaneView = PrivateRoomPaperPlaneViewImpl(mRootView!!.findViewById<View>(R.id.id_paper_plane_manager) as ViewGroup, this)

        /**
         * 初始化房间背景
         */
        updateBackGround(mPrivateRoomLightContext!!.getBackGroundType(), mPrivateRoomLightContext!!.getBackGroundUrl())

        /**
         * 工具包
         */
        mPrivateRoomToolBoxView = PrivateRoomToolBoxViewImpl(mRootView!!, this)
        onToolBoxUpdate()
        onToolBoxItemUpdate()

        /**
         * 如果在麦上则底部是 按下说话，否则底部为输入框
         */
        if (PrivateRoomTools.PositionInUpSeat(getSelfPosition())) {
            mPrivateRoomUISpeak.turnIntoSpeakByMouse()
        } else {
            mPrivateRoomUISpeak.turnIntoSpeakByHand()
        }

        if (mPrivateRoomLightContext!!.isSelfBanMic()) {
            mPrivateRoomUISpeak.turnIntoBanMic(true)
        }

        if (mPrivateRoomLightContext!!.isSelfFreeStyle()) {
            mPrivateRoomUISpeak.turnIntoFreeStyle(true)
        }

        initListener()

        updateRoomTitleBar()

        updateRoomTag()
        /**
         * 绑定图片选择CallBack
         */
        mImagePicker.bindCallBack(object : WrapImagePicker.EmptyImagePickerObserver() {
            override fun onPicked(path: String?) {
                mPrivateRoomLightContext?.exec(Runnable {
                    printlnLog("select path:${path}")

                    if (TextUtils.isEmpty(path)) {
                        return@Runnable
                    }


                    /**
                     * 开始执行上传操作
                     */
                    mImagePicker.startUpLoad(mPrivateRoomLightContext?.getRoomId(), path, mPrivateRoomLightContext?.getHandler())

                    /**
                     * 本地首先发送一条
                     */
                    val selfMember = mPrivateRoomLightContext?.getSelfMember()

                    selfMember ?: return@Runnable

                    val tempPictureMessageItem =
                            TempPictureMessageItem(
                                    selfMember.getMemberName() ?: "", selfMember.getMemberPosition(), mPrivateRoomLightContext?.getRoomConfig()?.image_expire_duration
                                    ?: 30000, path!!, RoomSocketEngineHelper.serverTime(), true)
                    mPrivateRoomLightContext?.addTempPictureItem(tempPictureMessageItem)

                })
            }

            override fun onUploadProgress(path: String?, percent: Double) {
                /**
                 * 上传图片的进度回调
                 */
                mPrivateRoomLightContext?.exec(Runnable {
                    printlnLog("upload progress ${percent}")
                    val progress = (percent.toInt() * 100 % 5).toInt()
                    if (progress == 0) {
                        mPrivateRoomMessageView.updateTempPictureUploadProgress(path, (percent * 100).toInt())
                    }
                })
            }

            override fun onUploadStart(path: String?) {
                printlnLog("upload start")

            }

            override fun onUploadSuccess(path: String?, key: String?, url: String?) {
                mPrivateRoomLightContext?.exec(Runnable {
                    printlnLog("upload Success key:${key},url:${url}")
                    RoomSocketEngineHelper.sendImageChatMessage(key, url, mPrivateRoomLightContext?.getRoomConfig()?.image_expire_duration ?: 30000)
                })
            }

            override fun onUploadError(path: String?, e: java.lang.Exception?) {
                mPrivateRoomLightContext?.exec(Runnable {
                    printlnLog("upload error ${e?.message}")
                    mPrivateRoomMessageView.updateTempPictureUploadError(path)
                })
            }
        })

        /**
         * 启动音频
         */
        mRxPermissions = RxPermissions(mActivity!!)

        mRxPermissions!!.request(Manifest.permission.RECORD_AUDIO).subscribe(Action1 {
            if (it) {
                /**
                 * 连接语音服务器
                 */
                if (mPrivateRoomLightContext?.getMinimize() ?: false) {

                } else {
                    mPrivateRoomLightContext?.openAudioEngine()
                    //RxToolKit.Emit(SocketEvent.BootFinishEvent(SocketEngine.PRIVACY))
                    if (MainApplication.getInstance().socketEngineHelper != null) {
                        MainApplication.getInstance().socketEngineHelper.startPollMessage()
                    }
                }
                mPrivateRoomLightContext?.setMinimize(false)

                /**
                 * 判断是否有邀请的列表
                 */
                try {
                    val userList: ArrayList<String>? = TransportHelper.getTransport(PrivateRoomTools.PRIVATE_ROOM_INVITE_LIST)
                    if (!ToolKit.isEmpty(userList)) {
                        /**
                         * 需要发送邀请人员列表
                         */
                        GlobalSocketEngine.INSTANCE.sendPrivateRoomMultiInvite(
                                mPrivateRoomLightContext!!.getRoomId(),
                                mPrivateRoomLightContext!!.getRoomType(),
                                mPrivateRoomLightContext!!.getRoomPassword(),
                                /**
                                 * 需要邀请的UserList
                                 */
                                userList
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                /**
                 * 没有给语音权限,直接退出
                 */
                //todo 这里需要弹出提示框 UI待定
                destroy(FORCE_EXIT_REASON_PERMISSION)
            }
        })

//        mPrivateRoomLikeView.startLoad()

        restoreData()

        /**
         * 键盘监听器
         */
        mKeyBoardListener = KeyBoardChangeListener(mActivity!!)
        mKeyBoardListener!!.setKeyBoardListener { isShow, keyboardHeight ->
            if (isShow) {
                printlnLog("key board show")
            } else {
                printlnLog("key board dismiss")
            }
        }

        /**
         * 通知EffectView 更新数据
         */
        val mSelfPosition = mPrivateRoomLightContext?.getSelfPosition() ?: -1
        EffectsUtils.postUpdateContextData(mSelfPosition, PrivateRoomTools.MASTER_POSITION, PrivateRoomTools.PositionInUpSeat(mSelfPosition))
    }

    private fun restoreData() {
        try {
            mPrivateRoomMessageView.restoreData(mPrivateRoomLightContext?.getRestoreData(PrivateRoomMessageView::class.java.name) as PrivateRoomMessageView.RestoreData)
        } catch (e: Exception) {
            /**
             * 恢复消息数据出问题了
             */
            e.printStackTrace()
        }
        mPrivateRoomLightContext!!.restoreContext()
    }


    private fun initWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val decorView = mActivity!!.window.getDecorView()
            val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            decorView.setSystemUiVisibility(option)
            mActivity!!.window.setStatusBarColor(Color.TRANSPARENT)
        } else {

        }
    }

    private fun updateBackGround(type: String, url: String) {
        if (TextUtils.equals(type, VoiceRoomBackGroundApiResult.BLUR_AVATAR)) {
            VoiceRoomCommander.BackGroundManager.BlurBackGround(url, mBackGround, mPrivateRoomLightContext!!.getHandler())
        } else {
            PicassoUtils.loadImage(mActivity!!, url, mBackGround, PrivateRoomTools.DEFAULT_BACKGROUND_WIDTH, PrivateRoomTools.DEFAULT_BACKGROUND_HEIGHT)
        }
    }

    private fun initListener() {
        RxToolKit.Build(this, SocketEvent.SocketSendCardEvent::class.java).action { socketSendCardEvent ->
            mPrivateRoomLightContext?.exec(Runnable {
                RoomSocketEngineHelper.SendCardMessage(socketSendCardEvent.getPeer_id(), socketSendCardEvent.getGift_type(),
                        { result1 ->
                            mPrivateRoomLightContext?.exec(Runnable {
                                if (socketSendCardEvent.getSuccessCallBack() != null) {
                                    socketSendCardEvent.getSuccessCallBack().func(result1)
                                }
                            })
                        }
                ) { result12 ->
                    mPrivateRoomLightContext?.exec(Runnable {
                        if (socketSendCardEvent.getFailedCallBack() != null) {
                            socketSendCardEvent.getFailedCallBack().func(result12)
                        }
                    })
                }
            })
        }.register()

        RxToolKit.Build(this, SocketEvent.SocketSendGiftEvent::class.java).action { socketSendGiftEvent ->
            mPrivateRoomLightContext?.exec(Runnable {
                RoomSocketEngineHelper.SendGiftMessage(socketSendGiftEvent.peer_id, socketSendGiftEvent.gift_type,
                        { result1 ->
                            mPrivateRoomLightContext?.exec(Runnable {
                                if (socketSendGiftEvent.successCallBack != null) {
                                    socketSendGiftEvent.successCallBack.func(result1)
                                }
                            })
                        }
                ) { result12 ->
                    mPrivateRoomLightContext?.exec(Runnable {
                        if (socketSendGiftEvent.failedCallBack != null) {
                            socketSendGiftEvent.failedCallBack.func(result12)
                        }
                    })
                }
            })
        }.register()

        RxToolKit.Build(this, OrbitError::class.java).action(
                {
                    mPrivateRoomLightContext?.exec(Runnable {
                        when (it.errorCode) {
                            OrbitError.AUDIO_RECORD_BEEN_RACE or OrbitError.AUDIO_RECORD_NO_PERMISSION
                            -> {
                                destroy(FORCE_EXIT_REASON_PERMISSION)
                            }
                        }
                    })
                }
        ).register()
    }

    private fun getSelfPosition(): Int {
        return mPrivateRoomLightContext?.getSelfPosition() ?: -1
    }

    private fun getString(resouceId: Int): String {
        return MessageUtils.getString(resouceId)
    }

    private fun getString(resouceId: Int, args: Array<Any>): String {
        return MessageUtils.getString(resouceId, args)
    }

    private fun turnIntoOnlineMember() {
        val payload = PrivateRoomOnLineMembersActivity.PayLoad()
        payload.mIsForceUpSeat = false
        payload.mIsTransferMaster = false
        payload.mTitle = MessageUtils.getString(R.string.string_game_online_members)
        payload.mOnLineMembers = mPrivateRoomLightContext!!.getAllMembersForShow()
        PrivateRoomOnLineMembersActivity.Launch(mActivity!!, payload)
    }

    private fun turnIntoForceUpSeatForDepty(position: Int, positionIsLock: Boolean) {
        val payload = PrivateRoomOnLineMembersActivity.PayLoad()
        payload.mIsForceUpSeat = true
        payload.mSeatPosition = position
        payload.mSeatLocked = positionIsLock
        payload.mIsTransferMaster = false
        payload.mTitle = MessageUtils.getString(R.string.string_game_online_members)
        payload.mOnLineMembers = mPrivateRoomLightContext!!.getAllMembersWithOutMySelf()
        PrivateRoomOnLineMembersActivity.Launch(mActivity!!, payload)
    }

    private fun turnIntoForceUpSeat(position: Int, positionIsLock: Boolean) {
        val payload = PrivateRoomOnLineMembersActivity.PayLoad()
        payload.mIsForceUpSeat = true
        payload.mSeatPosition = position
        payload.mSeatLocked = positionIsLock
        payload.mIsTransferMaster = false
        payload.mTitle = MessageUtils.getString(R.string.string_game_online_members)
        payload.mOnLineMembers = mPrivateRoomLightContext!!.getAllMembersOutSeats()
        PrivateRoomOnLineMembersActivity.Launch(mActivity!!, payload)
    }

    private fun turnIntoTransferMaster() {
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_choose_new_master))
        val payload = PrivateRoomOnLineMembersActivity.PayLoad()
        payload.mIsForceUpSeat = false
        payload.mIsTransferMaster = true
        payload.mTitle = MessageUtils.getString(R.string.str_choose_new_master)
        payload.mOnLineMembers = mPrivateRoomLightContext!!.getAllMembersWithOutMySelf()
        PrivateRoomOnLineMembersActivity.Launch(mActivity!!, payload)
    }

    private fun updateRoomTitleBar() {
        val isMaster = mPrivateRoomLightContext?.isMaster() ?: false
        mPrivateRoomUITitle.updateTitle(isMaster)
    }

    private fun updateRoomTag() {

        val isMaster = mPrivateRoomLightContext?.isMaster() ?: false
        val tag = mPrivateRoomLightContext?.getTag() ?: ""
        val tagColor = mPrivateRoomLightContext?.getTagColor() ?: PrivateRoomTools.DEFAULT_TAG_COLOR
        val tagIntroduce = mPrivateRoomLightContext?.getRoomIntroduce() ?: ""

        if (TextUtils.isEmpty(tag)) {
            /**
             * 没有Tag
             */
            if (isMaster) {
                mPrivateRoomUITitle.defaultTag()
            } else {
                mPrivateRoomUITitle.goneTag()
            }
        } else {
            /**
             * 有Tag更新Tag
             */
            mPrivateRoomUITitle.updateTag(tag, tagIntroduce, tagColor)
        }
    }

    override fun onFreeStyleRequest(serverActionRequestFreeMode: ServerActionRequestFreeMode) {
        if (mPrivateRoomLightContext?.isSelfPosition(serverActionRequestFreeMode.position) ?: false && mPrivateRoomLightContext?.isMaster() ?: false) {
            VoiceSocketHelper.acceptFreeMode()
            return
        }
        if (mPrivateRoomLightContext?.isSelfPosition(serverActionRequestFreeMode.position) ?: false && mPrivateRoomLightContext?.isDeputy() ?: false) {
            VoiceSocketHelper.acceptFreeMode()
            return
        }
        if (mPrivateRoomLightContext?.isSelfPosition(serverActionRequestFreeMode.position)
                        ?: false) {
            val dialog = VoiceFreeStyleDialog(mActivity!!, serverActionRequestFreeMode.duration / 1000, serverActionRequestFreeMode.position)
            mDialogManager.putAndShow(USER_FREESTYLE_DIALOG, dialog)
        }
    }

    override fun onSelfTurnIntoBanMic(flag: Boolean) {
        mPrivateRoomUISpeak.turnIntoBanMic(flag)
    }

    override fun onSelfTurnIntoFreeStyle(flag: Boolean) {
        mPrivateRoomUISpeak.turnIntoFreeStyle(flag)
    }

    override fun onSelfDownSeat() {
        mPrivateRoomUISpeak.turnIntoSpeakByHand()
        mPrivateRoomToolBoxView.goneToolBox()
        mPrivateRoomToolBoxView.closeToolBoxItem()
    }

    override fun onSelfUpSeat() {
        val selfMember = mPrivateRoomLightContext?.getSelfMember()
        selfMember ?: return
        mPrivateRoomToolBoxView.showToolBox()
        mPrivateRoomToolBoxView.closeToolBox()
        if (selfMember.memberIsFreeStyle()) {
            mPrivateRoomUISpeak.turnIntoFreeStyle(true)
            return
        }
        if (selfMember.memberIsBanMic()) {
            mPrivateRoomUISpeak.turnIntoBanMic(true)
            return
        }
        mPrivateRoomUISpeak.turnIntoSpeakByMouse()
    }

    private fun refreshBackGroundIfNeed() {
        /**
         * 处理房间背景的变化
         */
        try {
            if (mPrivateRoomLightContext!!.isBackGroundChanged()) {
                updateBackGround(mPrivateRoomLightContext!!.getBackGroundType(), mPrivateRoomLightContext!!.getBackGroundUrl())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onKickOut(member: PrivateRoomMemberData) {
        if (member.memberIsMaster() && member.memberIsSelf()) {
            destroy()
            return
        }
        if (member.memberIsSelf()) {
            destroy(FORCE_EXIT_REASON_KICK_OUT)
            return
        }
    }

    override fun onShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon) {
        val memberView = mPrivateRoomMembersView.getPrivateRoomMemberView(serverActionShowEmoticon.position)
        memberView ?: return
        val memberData = mPrivateRoomLightContext?.getMember(serverActionShowEmoticon.position)
        memberData ?: return
        /**
         * 在不在麦上
         */
        if (memberData.memberIsUpSeat()) {
            EffectsUtils.postEmotionEffects(
                    serverActionShowEmoticon.position,
                    memberView.getMemberPositionInScreen(),
                    memberView.getMemberWidth(),
                    memberView.getMemberHeight(),
                    serverActionShowEmoticon.mark,
                    serverActionShowEmoticon.url,
                    2000
            )
        }
    }

    override fun onShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion) {
        /**
         * 处理小游戏
         */
        val code = serverActionShowGameEmotion.data.code
        val msg = serverActionShowGameEmotion.data.msg

        if (code == 1000) {

            if (!PrivateRoomTools.PositionInUpSeat(serverActionShowGameEmotion.position)) {
                return
            }

            val member = mPrivateRoomLightContext?.getMember(serverActionShowGameEmotion.position)
            member ?: return

            val memberView = mPrivateRoomMembersView.getPrivateRoomMemberView(serverActionShowGameEmotion.position)
            memberView ?: return

            val event = RoomEffectEvent.RoomMemberEffect()

            with(event)
            {
                eventType = RoomEffectEvent.SMALL_GAME
                type = serverActionShowGameEmotion.mark

                position = serverActionShowGameEmotion.position
                point = memberView.getMemberPositionInScreen()
                width = memberView.getMemberWidth()
                height = memberView.getMemberHeight()

                if (serverActionShowGameEmotion.data != null) {
                    try {
                        repeatTime = 1
                        backgroundMusic = serverActionShowGameEmotion.data.sound
                        lastFrameDuration = serverActionShowGameEmotion.data.stay_time * 1000.toLong()
                        if (!TextUtils.isEmpty(serverActionShowGameEmotion.data.res)) {
                            lastFrame = serverActionShowGameEmotion.data.res.toInt()
                        }
                    } catch (e: Exception) {
                        /**
                         * 说明对于数据处理出了问题
                         */
                    }
                } else {

                }
            }
            EventBus.getDefault().post(event)
        } else {
            ToastToolKit.showShort(msg)
        }
    }

    override fun onShowGiftAnim(fromPosition: Int, toPosition: Int, type: String) {
        val from = fromPosition
        val to = toPosition

        var fromPointF: PointF
        var toPointF: PointF

        var fromWidth = 0
        var fromHeight = 0
        var toWidth = 0
        var toHeight = 0
        val defaultHeight = mPrivateRoomMembersView.getPrivateRoomMemberView(0)!!.getMemberHeight()
        val defaultWidth = mPrivateRoomMembersView.getPrivateRoomMemberView(0)!!.getMemberWidth()

        if (PrivateRoomTools.PositionInUpSeat(from)) {
            fromPointF = mPrivateRoomMembersView.getPrivateRoomMemberView(from)!!.getMemberPositionInScreen()
            fromWidth = mPrivateRoomMembersView.getPrivateRoomMemberView(from)!!.getMemberWidth()
            fromHeight = mPrivateRoomMembersView.getPrivateRoomMemberView(from)!!.getMemberHeight()
        } else {
            fromPointF = PointF(ScreenUtils.getCurrentScreenWidth().toFloat(), ScreenUtils.getCurrentScreenHeight().toFloat())
        }

        if (PrivateRoomTools.PositionInOutSeat(to)) {
            /**
             * 给麦下送的 不处理
             */
            return
        }

        toPointF = mPrivateRoomMembersView.getPrivateRoomMemberView(to)!!.getMemberPositionInScreen()
        toWidth = mPrivateRoomMembersView.getPrivateRoomMemberView(to)!!.getMemberWidth()
        toHeight = mPrivateRoomMembersView.getPrivateRoomMemberView(to)!!.getMemberHeight()

        /**
         * 校准礼物坐标
         */
        if (fromWidth != 0 && fromHeight != 0) {
            /**
             * 需要校准
             */
            if (fromWidth == defaultWidth && fromHeight == defaultHeight) {
                /**
                 * 不用校准
                 */
            } else {
                fromPointF = PointF((fromPointF.x - (defaultWidth - fromWidth) / 2).toFloat(), (fromPointF.y - (defaultHeight - fromHeight) / 2).toFloat())
            }
        }

        /**
         * 校准礼物坐标
         */
        if (toWidth != 0 && toHeight != 0) {
            /**
             * 需要校准
             */
            if (toWidth == defaultWidth && toHeight == defaultHeight) {
                /**
                 * 不用校准
                 */
            } else {
                toPointF = PointF((toPointF.x - (defaultWidth - toWidth) / 2).toFloat(), (toPointF.y - (defaultHeight - toHeight) / 2).toFloat())
            }
        }

        val fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT
        val toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fromPointF = PointF(fromPointF.x, fromPointF.y)
            toPointF = PointF(toPointF.x, toPointF.y)
        } else {
            fromPointF = PointF(fromPointF.x, fromPointF.y - ScreenUtils.dip2px(25f))
            toPointF = PointF(toPointF.x, toPointF.y - ScreenUtils.dip2px(25f))
        }

        EffectsManager.DispatchAnimEvent(mActivity, GiftAnimConstant.GIFT_FROM_TYPE_VOICE, type, fromPointF, toPointF, fromO, toO, defaultHeight, defaultWidth, fromPosition, toPosition)
    }

    override fun onTempPictureMessage(tempPictureMessageItem: TempPictureMessageItem?) {
        tempPictureMessageItem ?: return
        if (mPrivateRoomLightContext?.isSelfPosition(tempPictureMessageItem!!.position) ?: false) {

        } else {
            val dialog = PrivateRoomTempPictureDialog(mActivity!!, tempPictureMessageItem!!.picture, (tempPictureMessageItem!!.countdown / 1000).toInt(), false)
            mDialogManager?.putAndShow(PrivateRoomController.TEMP_PICTURE_DIALOG, dialog)
        }
    }

    override fun onShowPaperPlane(paperPlane: PrivateRoomPaperPlane) {
        mPrivateRoomPaperPlaneView.addPaperPlane(paperPlane)
    }

    override fun onShowPaperPlane(paperPlaneList: List<PrivateRoomPaperPlane>) {
        mPrivateRoomPaperPlaneView.addPaperPlane(paperPlaneList)
    }

    override fun onSeatUIRefresh(position: Int) {
        mPrivateRoomMembersView.refresh(position)
    }

    override fun onSeatStateRefresh(position: Int) {
        mPrivateRoomMembersView.refreshMemberState(position)
    }


    override fun onSeatLocked(position: Int, isLocked: Boolean) {
        mPrivateRoomMembersView.lock(position, isLocked)
    }

    override fun onSeatUIAllRefresh() {
        mPrivateRoomMembersView.refreshAll()
    }

    override fun onSpeaking(position: Int) {
        mPrivateRoomMembersView.speak(position, true)
    }

    override fun onUnSpeaking(position: Int) {
        mPrivateRoomMembersView.speak(position, false)
    }

    override fun onSocketConnect() {

    }

    override fun onSocketDisconnect() {
        mPrivateRoomUISpeak.endSpeak()
    }

    override fun onSocketClose() {
        destroy(FORCE_EXIT_REASON_RECONNECT_FAILED)
    }

    override fun onLikeNumber(number: Long) {
        mPrivateRoomUITitle.setLikeNumber(number)
    }

    override fun onOnlineNumber(number: Int) {
        mPrivateRoomUITitle.setOnlineNumber(number)
    }

    override fun onUpdateRoomTag() {
        updateRoomTag()
    }

    override fun onUpdateRoomTitle() {
    }

    override fun onUpdateRoomTitleBar() {
        updateRoomTitleBar()
    }

    override fun onUpdateBackGround() {
        refreshBackGroundIfNeed()
    }

    override fun onMessageListChange() {
        mPrivateRoomMessageView.refresh()
    }

    override fun onMinimize() {
        /**
         * 最小化功能
         */

        /**
         * 设置Context为小窗模式
         */
        mPrivateRoomLightContext?.setMinimize(true)

        /**
         * 开启服务
         */
        val floatUser = FloatUser()
        val masterMember = mPrivateRoomLightContext?.getMasterMember()
        if (masterMember != null) {
            floatUser.userHead = masterMember.getMemberIcon()
            floatUser.userId = masterMember.getMemberId()
            floatUser.userName = masterMember.getMemberName()
        }

        FloatWindowCommander.StartService(mActivity!!, floatUser, {
            /**
             * 进入小窗模式
             */
            if (it == 0) {
                destroy()
            }
        })

    }

    override fun onInvite() {

        val data = ShareBridgeData(ShareBridgeData.SHARE_FROM_CUSTOM)
        data.userName = GlobalUserState.getGlobalState().userName
        data.fromType = Constant.GAME_TYPE_PRIVACY
        data.roomId = mPrivateRoomLightContext!!.getRoomId()
        data.password = mPrivateRoomLightContext!!.getRoomPassword()
        SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF)?.onPrivacyRoomShare(mActivity!!, data)
    }

    override fun onExit() {
        /**
         *  提示对话框 并退出
         */
        if (mPrivateRoomLightContext?.isMaster() ?: false) {
            if (mPrivateRoomLightContext?.getOnlineNumber() ?: 0 > 1) {
                turnIntoTransferMaster()
            } else {
                val dialog = VoiceLeaveDialog(mActivity!!,
                        MessageUtils.getString(R.string.dialog_hint),
                        MessageUtils.getString(R.string.leave_the_room),
                        { v ->
                            mPrivateRoomLightContext?.setMinimize(false)
                            if (mPrivateRoomLightContext?.isMaster() ?: false) {
                                destroy()
                            } else {
                                destroy(FORCE_EXIT_REASON_NONE)
                            }

                        },
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                        },
                        false
                )
                dialog.negative(true)
                mDialogManager.putAndShow(USER_EXIT_DIALOG, dialog)
            }
        } else {
            val dialog = VoiceLeaveDialog(mActivity!!,
                    MessageUtils.getString(R.string.dialog_hint),
                    MessageUtils.getString(R.string.leave_the_room),
                    { v ->
                        mPrivateRoomLightContext?.setMinimize(false)
                        if (mPrivateRoomLightContext?.isMaster() ?: false) {
                            destroy()
                        } else {
                            destroy(FORCE_EXIT_REASON_NONE)
                        }

                    },
                    { v ->
                        mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                    },
                    false
            )
            dialog.negative(true)
            mDialogManager.putAndShow(USER_EXIT_DIALOG, dialog)
        }
    }

    override fun onManageRoom() {
        launchSettingActivity()
    }

    override fun onManageTag() {
        launchSettingActivity()
    }

    override fun onOnlineNumber() {
        turnIntoOnlineMember()
    }

    override fun onSendEmotion(emotion: EmotionManiFest.Emotion) {
        //处理发送表情的消息
        //TODO socket
//        RoomSocketEngineHelper.sendEmotion(emotion.type, emotion.url, mPrivateRoomLightContext!!.getSelfPosition())
    }

    override fun onShowGiftDialog() {
        if (GlobalUserState.getGlobalState().isTourist) {
            ToastToolKit.showLong(R.string.gifts_preset_from_tourist)
        } else {
            val dialog = VoiceDialogSendGift(mActivity!!, PrivateRoomTools.PrivateRoomMembersTurnIntoGameMembers(mPrivateRoomLightContext!!.getAllMembers()))
            dialog.show()
        }
    }

    override fun onBeforeSendEmotion(): Boolean {
        if (!PrivateRoomTools.PositionInUpSeat(getSelfPosition())) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_private_room_upseat_emotion))
            return false
        }
        return true
    }

    override fun onSwitchToHand(): Boolean {
        return true
    }

    override fun onSwitchToMouse(): Boolean {
        if (!PrivateRoomTools.PositionInUpSeat(getSelfPosition())) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_can_send_voice))
            return false
        }
        if (mPrivateRoomLightContext?.isSelfBanMic() ?: false) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.has_been_mute))
            return false
        }
        return true
    }

    override fun onChangeBackGround() {
        /**
         * 更换背景图片
         */
        VoiceRoomBackGroundActivity.Launch(mActivity!!, mPrivateRoomLightContext!!.getRoomId(), mPrivateRoomLightContext!!.getRoomType())
    }

    override fun onSendImage() {
        /**
         * 执行发送图片
         */
        mImagePicker.startPick(mActivity!!)
    }

    override fun onSendPaperPlane() {
        launchPaperPlaneActivty(mPrivateRoomLightContext!!.getSelfPosition())
    }

    override fun onMemberClickAtPerson(position: Int) {
        printlnLog("onMemberClickAtPerson(${position})")

        /**
         * 点击到了一个有人的位置上
         */
        val member: PrivateRoomMemberData? = mPrivateRoomLightContext?.getMember(position)
        if (member == null) {
            return
        }

        fun clickIsSelf(): Boolean {
            return (mPrivateRoomLightContext?.isSelfPosition(member.getMemberPosition()) ?: false)
        }

        fun selfIsUpSeat(): Boolean {
            return mPrivateRoomLightContext?.isSelfUpSeat() ?: false
        }

        fun selfIsOutSeat(): Boolean {
            return mPrivateRoomLightContext?.isSelfOutSeat() ?: false
        }

        fun selfIsMaster(): Boolean {
            return mPrivateRoomLightContext?.isMaster() ?: false
        }

        fun selfIsDeputy(): Boolean {
            return mPrivateRoomLightContext?.isDeputy() ?: false
        }

        fun clickIsMaster(): Boolean {
            return member.memberIsMaster()
        }

        fun clickIsDeputy(): Boolean {
            return member.memberIsDeputy()
        }

        val actions = ArrayList<PrivateRoomUserActionItem>()

        /**
         * 送礼物
         */
        actions.add(PrivateRoomUserActionItem(R.mipmap.ico_private_room_send_gift, getString(R.string.str_send_gift), Action {
            if (Utils.checkUserIdIsTourist(member.getMemberId()) && TextUtils.equals(member.getMemberId(), GlobalUserState.getGlobalState().userId)) {
                /**
                 * 自己是游客
                 */
                ToastToolKit.showLong(R.string.gifts_preset_from_tourist)
                return@Action
            }

            if (Utils.checkUserIdIsTourist(member.getMemberId())) {
                /**
                 * 送给游客
                 */
                ToastToolKit.showLong(R.string.gifts_preset_to_tourist)
                return@Action
            }
            ExhibitionsActivity.startActivityFromGame(mActivity, member.getMemberId(), member.getMemberName(), member.getMemberSex(), member.getMemberIcon(), member.getMemberPosition())
            it.dismiss()
        }))

        /**
         * 纸飞机,自己在麦上,并且点击的人不是自己 才可以显示
         */
        if (selfIsUpSeat()) {
            actions.add(PrivateRoomUserActionItem(R.mipmap.ico_private_room_paper_plane, getString(R.string.str_private_room_paper_plane), Action {

                if (clickIsSelf()) {
                    launchPaperPlaneActivty(mPrivateRoomLightContext!!.getSelfPosition())
                } else {
                    val payload: PrivateRoomPaperPlaneActivity.PayLoad = PrivateRoomPaperPlaneActivity.PayLoad()
                    payload.background = mPrivateRoomLightContext!!.getBackGroundUrl()
                    payload.blur = TextUtils.equals(mPrivateRoomLightContext!!.getBackGroundType(), VoiceRoomBackGroundApiResult.BLUR_AVATAR)

                    val result: MutableList<PrivateRoomPaperPlaneActivity.DataHolder> = ArrayList()
                    mPrivateRoomLightContext?.getSeatsData()?.forEach {
                        if (it.getSeatMemberData() == null) {
                            result.add(PrivateRoomPaperPlaneActivity.EMPTY_DATA_HOLDER)
                        } else {
                            if (it.getSeatPosition() == position) {
                                result.add(PrivateRoomPaperPlaneActivity.TurnIntoDataHolder(it.getSeatMemberData()!!, true))
                            } else {
                                result.add(PrivateRoomPaperPlaneActivity.TurnIntoDataHolder(it.getSeatMemberData()!!, false))
                            }
                        }
                        if (it.getSeatPosition() == PrivateRoomTools.DEPUTY_POSITION) {
                            /**
                             * 填充数据
                             */
                            result.add(PrivateRoomPaperPlaneActivity.EMPTY_PLACE_HOLDER)
                            result.add(PrivateRoomPaperPlaneActivity.EMPTY_PLACE_HOLDER)
                        }
                    }
                    payload.mUpSeatsData = result
                    payload.mFrom = mPrivateRoomLightContext?.getSelfMember()
                    PrivateRoomPaperPlaneActivity.Launch(mActivity!!, payload)
                }
                it.dismiss()
            }))
        }

        /**
         * 禁言功能，自己是房主或者副房主，点击的不是自己才可以显示
         */
        if ((selfIsMaster() || selfIsDeputy()) && !clickIsMaster() && !clickIsSelf() && !clickIsDeputy()) {
            if (member.memberIsBanMic()) {
                /**
                 * 解禁麦
                 */
                actions.add(PrivateRoomUserActionItem(R.mipmap.ico_voice_un_ban_mic, getString(R.string.str_voice_un_ban_mic), Action {
                    RoomSocketEngineHelper.sendUnBanMic(position)
                    it.dismiss()
                }))
            } else {
                /**
                 * 禁麦
                 */
                actions.add(PrivateRoomUserActionItem(R.mipmap.ico_voice_ban_mic, getString(R.string.str_voice_ban_mic), Action {
                    RoomSocketEngineHelper.sendBanMic(position)
                    it.dismiss()
                }))
            }
        }

        /**
         * 自由麦功能，自己是房主或者副房主，点击的不是自己才可以显示
         */
        if (
                (selfIsMaster())
                || (selfIsDeputy() && !clickIsMaster())
        ) {
            if (!member.memberIsFreeStyle()) {
                /**
                 * 开自由麦
                 */
                actions.add(PrivateRoomUserActionItem(R.mipmap.ico_voice_chair_free, getString(R.string.str_open_free_style), Action {
                    VoiceSocketHelper.requestFreeMode(position)
                    it.dismiss()
                }))
            } else {
                /**
                 * 关启自由麦
                 */
                actions.add(PrivateRoomUserActionItem(R.mipmap.ico_voice_chair_unfree, getString(R.string.str_close_free_style), Action {
                    RoomSocketEngineHelper.sendUnFreeStyle(position)
                    it.dismiss()
                }))
            }
        }


        if ((selfIsMaster() && !clickIsMaster()) || (selfIsDeputy() && !clickIsMaster() && !clickIsSelf())) {
            /**
             * 设置为旁听
             */
            actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_listen, getString(R.string.str_set_down_seat), Action {
                RoomSocketEngineHelper.sendDownSeat(member.getMemberPosition())
                it.dismiss()
            }))

            /**
             * 封闭麦
             */
            actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_lock_seat, getString(R.string.str_lock_seat), Action {
                RoomSocketEngineHelper.sendDownSeat(member.getMemberPosition())
                RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.LOCK, member.getMemberPosition() + 1)
                it.dismiss()
            }))

        }

        if ((!selfIsMaster() && !selfIsDeputy() && clickIsSelf()) || (selfIsDeputy() && clickIsSelf())) {
            actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_listen, getString(R.string.str_down_to_side), Action {
                RoomSocketEngineHelper.sendDownSeat(member.getMemberPosition())
                it.dismiss()
            }))
        }

        var needKickOut = false

        /**
         * 只有房主可以踢人
         */
        if ((selfIsMaster() && !clickIsSelf())) {
            needKickOut = true
        }

        val dialog = PrivateRoomUserDialog(mActivity!!, mPrivateRoomLightContext!!, member, PrivateRoomUserDialog.Config(false, needKickOut), actions)
        mDialogManager.putAndShow(USER_ACTION_DIALOG, dialog)
    }

    override fun onMemberClickAtNoPerson(position: Int) {
        printlnLog("onMemberClickAtNoPerson(${position})")
        /**
         * 点击了一个没有人位置
         * 逻辑如下：
         * 1。如果自己在麦下 则执行 上麦操作
         * 2。如果自己在麦上 则判断自己是不是房主，如果是房主则弹出 房主对话框，如果不是房主则执行 换位置操作
         */
        val tempP = getSelfPosition()
        if (tempP == -1) return

        if (PrivateRoomTools.PositionInUpSeat(tempP)) {
            /**
             * 在麦上的情况
             */

            /**
             * 如果自己在麦上并且自己不是房主，点击的是副房主的位置则直接结束
             */
            if (mPrivateRoomLightContext!!.isDeputyPosition(position) && !mPrivateRoomLightContext!!.isMaster()) {
                return
            }

            if (mPrivateRoomLightContext!!.isMaster() || mPrivateRoomLightContext!!.isDeputy()) {
                /**
                 * 自己是房主
                 */
                val actions = ArrayList<PrivateRoomUserActionItem>()
                actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_profile, getString(R.string.str_carry_to_seat), Action {
                    /**
                     * 抱用户上麦
                     */
                    if (mPrivateRoomLightContext!!.isDeputyPosition(position)) {
                        turnIntoForceUpSeatForDepty(position, false)
                    } else {
                        turnIntoForceUpSeat(position, false)
                    }
                    it.dismiss()
                }))
                actions.add(PrivateRoomUserActionItem.EmptyAction)
                actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_lock_seat, getString(R.string.str_lock_seat), Action {
                    /**
                     * 锁位置
                     */
                    RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.LOCK, position + 1)
                    it.dismiss()
                }))
                val dialog = PrivateRoomUserDialog(mActivity!!, mPrivateRoomLightContext!!, null, PrivateRoomUserDialog.Config(true), actions)
                mDialogManager.putAndShow(USER_ACTION_DIALOG, dialog)
            } else {
                /**
                 * 自己是不是房主
                 */
                PrivateRoomTools.ExecActionUpSeat(position)
            }
        } else {
            /**
             * 在麦下
             */
            if (position == PrivateRoomTools.DEPUTY_POSITION) {
                return
            }
            PrivateRoomTools.ExecActionUpSeat(position)
        }
    }

    override fun onMemberClickAtLock(position: Int) {
        printlnLog("onMemberClickAtLock(${position})")
        /**
         * 点击了一个被锁住的位置，如果是房主，则弹出操作框，如果不是房主 什么也不做
         */
        if (mPrivateRoomLightContext?.isMaster() ?: false || mPrivateRoomLightContext?.isDeputy() ?: false) {
            /**
             * 自己是房主
             */
            val actions = ArrayList<PrivateRoomUserActionItem>()

            actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_profile, getString(R.string.str_carry_to_seat), Action {
                /**
                 * 抱用户上麦
                 */
                if (mPrivateRoomLightContext!!.isDeputyPosition(position)) {
                    turnIntoForceUpSeatForDepty(position, true)
                } else {
                    turnIntoForceUpSeat(position, true)
                }
                it.dismiss()
            }))

            actions.add(PrivateRoomUserActionItem.EmptyAction)

            actions.add(PrivateRoomUserActionItem(R.mipmap.ic_voice_unlock, getString(R.string.str_unlock_seat), Action {
                /**
                 * 解锁位置
                 */
                RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.UNLOCK, position + 1)
                it.dismiss()
            }))
            val dialog = PrivateRoomUserDialog(mActivity!!, mPrivateRoomLightContext!!, null, PrivateRoomUserDialog.Config(true), actions)
            mDialogManager.putAndShow(USER_ACTION_DIALOG, dialog)
        } else {
            return
        }
    }

    override fun onPaperPlaneReplay(privateRoomPaperPlane: PrivateRoomPaperPlane?) {
        if (privateRoomPaperPlane == null) return
        launchPaperPlaneActivty(privateRoomPaperPlane.fromPosition)
    }

    override fun onEngineDisConnect() {
        printlnLog("Audio DisConnect")
        mPrivateRoomUITitle?.audioWifiState(true)
    }

    override fun onEngineConnect() {
        printlnLog("Audio Connect")
        mPrivateRoomUITitle?.audioWifiState(false)
    }

    override fun onLike() {
        RoomSocketEngineHelper.sendLikeRoom()
    }

    override fun onSystemMsg(serverActionSystemMsg: ServerActionSystemMsg) {
        if (TextUtils.equals(serverActionSystemMsg.type, ServerActionSystemMsg.DIALOG)) {
            /**
             * 处理系统提示Dialog
             */
            val dialog = WereWolfSystemMsgDialog(mActivity!!)
            dialog.setMessage(serverActionSystemMsg.content)
            dialog.setButtonType(WereWolfSystemMsgDialog.ONE_BUTTON_TYPE)
            val midButton = dialog.oneMid
            midButton.text = WereWolfString.getString(R.string.confirm)
            midButton.setOnClickListener { v -> dialog.dismiss() }
            dialog.showDialog(serverActionSystemMsg.duration / 1000)
            mDialogManager.put(SYSTEM_INFO_DIALOG, dialog)
        } else if (TextUtils.equals(serverActionSystemMsg.type, ServerActionSystemMsg.TOAST)) {
            ToastToolKit.showShort(serverActionSystemMsg.content)
        }
    }


    override fun onToolBoxItemUpdate() {
        /**
         * 外放Item 更新了
         */
        val context = mPrivateRoomLightContext

        context ?: return

        if (context.isSelfOutSeat()) {
            mPrivateRoomToolBoxView.closeToolBoxItem()
            return
        } else {
            /**
             * 自己在麦上
             */
            if (context.isSendTempImageEnable()) {
                /**
                 * 打开发送临时图片功能
                 */
                mPrivateRoomToolBoxView.openToolBoxItemSendTempImage()
            } else {
                mPrivateRoomToolBoxView.closeToolBoxItem()
            }
        }
    }


    override fun onToolBoxUpdate() {
        /**
         * 工具栏状态有更新
         */
        val context = mPrivateRoomLightContext

        context ?: return

        if (context.isMaster()) {
            /**
             *  只有房主才处理这个东西
             */

            mPrivateRoomToolBoxView.turnIntoMasterToolBox()

            if (context.isSendTempImageEnable()) {
                mPrivateRoomToolBoxView.showCloseSendTempImage()
            } else {
                mPrivateRoomToolBoxView.showOpenSendTempImage()
            }
        } else {
            if (context.isSelfUpSeat()) {
                /**
                 * 自己在麦上,展示工具栏并且里面只有纸飞机
                 */
                mPrivateRoomToolBoxView.turnIntoUpSeatToolBox()
                if (context.isSendTempImageEnable()) {
                    mPrivateRoomToolBoxView.openToolBoxItemSendTempImage()
                } else {
                    mPrivateRoomToolBoxView.closeToolBoxItem()
                }
            } else {
                /**
                 * 自己在麦下
                 */
                mPrivateRoomToolBoxView.turnIntoOutSeatToolBox()
                mPrivateRoomToolBoxView.goneToolBox()
                mPrivateRoomToolBoxView.closeToolBoxItem()
            }
        }


    }

    fun dispatchTouchEvent(ev: MotionEvent?): Boolean {

        if (mPrivateRoomToolBoxView.dispatchTouchEvent(ev)) {
            return true
        }
        return false
    }

    fun onActivityResume() {

    }

    fun onActivityStart() {

    }

    fun onActivityStop() {

    }

    fun onActivityPause() {
        val member = mPrivateRoomLightContext?.getSelfMember()
        if (member?.memberIsSpeaking() ?: false && !(member?.memberIsFreeStyle() ?: false)) {
            mPrivateRoomUISpeak?.endSpeak()
        }
    }

    override fun onSpeakTouchDown(): Boolean {
        if (mPrivateRoomLightContext?.isAudioConnected() ?: false && mPrivateRoomLightContext?.isSocketConnected() ?: false) {
            return true
        }
        return false
    }

    override fun onSpeakTouchUp(): Boolean {
        return true
    }

    fun onBackPressed() {

        val event = RoomEffectEvent.RoomMemberEffect()
        with(event)
        {
            eventType = RoomEffectEvent.SMALL_GAME
            type = VoiceConstants.POKER
            position = mPrivateRoomLightContext?.getSelfPosition() ?: 0
            point = mPrivateRoomMembersView.getPrivateRoomMemberView(position)?.getMemberPositionInScreen()
            width = mPrivateRoomMembersView.getPrivateRoomMemberView(position)?.getMemberWidth() ?: 0
            height = mPrivateRoomMembersView.getPrivateRoomMemberView(position)?.getMemberHeight() ?: 0

            lastFrameDuration = 0
            lastFrame = 1
        }
        EventBus.getDefault().post(event)
    }

    private fun showExitReasonIfNeed(reason: Int) {
        when (reason) {
            FORCE_EXIT_REASON_NONE -> {
                finishActivity()
                printlnLog("End Destroy Controller")
                return
            }

            FORCE_EXIT_REASON_KICK_OUT -> {
                val dialog = VoiceLeaveDialog(mActivity!!,
                        MessageUtils.getString(R.string.dialog_hint),
                        MessageUtils.getString(R.string.you_have_been_kick_out),
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        false
                )
                mDialogManager.putAndShow(USER_EXIT_DIALOG, dialog)
            }

            FORCE_EXIT_REASON_PERMISSION -> {
                val dialog = VoiceLeaveDialog(mActivity!!,
                        MessageUtils.getString(R.string.dialog_hint),
                        MessageUtils.getString(R.string.audio_permission_error_not_in_gaming),
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        false
                )
                mDialogManager.putAndShow(USER_EXIT_DIALOG, dialog)
            }

            FORCE_EXIT_REASON_RECONNECT_FAILED -> {
                val dialog = VoiceLeaveDialog(mActivity!!,
                        MessageUtils.getString(R.string.dialog_hint),
                        MessageUtils.getString(R.string.you_have_been_disconnected),
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        { v ->
                            mDialogManager.removeAndDismiss(USER_EXIT_DIALOG)
                            finishActivity()
                            printlnLog("End Destroy Controller")
                        },
                        false
                )
                mDialogManager.putAndShow(USER_EXIT_DIALOG, dialog)
            }
        }
    }

    fun destroy(reason: Int) {

        printlnLog("Begin Destroy Controller")

        RxToolKit.UnRegister(this)

        mDialogManager.destroy()
        mPopWindowManager.destroy()

        mMainView?.removeAllViews()
        mMainView = null
        mRootView = null

        /**
         * 解除Context Process
         */
        mPrivateRoomLightContext?.unregisterAudioObserver(this)
        mPrivateRoomLightContext?.unBindObserver(this)

        /**
         * 清理所有UI数据
         */
        mGiftAnimLayer?.destroy()
        mGiftAnimLayer = null

        mGiftBoardLayer?.destroy()
        mGiftBoardLayer = null

        mPrivateRoomUITitle?.destroy()
        mPrivateRoomUISpeak?.destroy()

        /**
         * 生成消息列表的存储状态数据
         */
        mPrivateRoomLightContext?.restoreData(PrivateRoomMessageView::class.java.name, mPrivateRoomMessageView.getRestoreData())
        mPrivateRoomMessageView.destroy()

        mPrivateRoomMembersView.destroy()

//        mPrivateRoomLikeView.destroy()
        mPrivateRoomPaperPlaneView.destroy()
        mPrivateRoomToolBoxView.destroy()

        mSpeakViewPopWindow?.dismissPopWindow()
        mSpeakViewPopWindow = null

        mImagePicker.cancelUpload()
        mImagePicker.destroy()

        mKeyBoardListener?.destroy()

        EventBus.getDefault().post(FinishConversationEvent())

        VoiceRoomCommander.ActivityManager.ReleaseActivities()

        if (reason == FORCE_EXIT_REASON_NONE) {
            val isMinimize = mPrivateRoomLightContext?.getMinimize() ?: false
            if (!isMinimize) {
                VoiceRoomCommander.ContextManager.DestroyPrivateRoomContext(Runnable {
                    showExitReasonIfNeed(reason)
                })
            } else {
                finishActivity()
            }
        } else {
            VoiceRoomCommander.ContextManager.DestroyPrivateRoomContext(Runnable {
                showExitReasonIfNeed(reason)
            })
        }
    }

    override fun destroy() {
        destroy(FORCE_EXIT_REASON_NONE)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mImagePicker.onActivityResult(mActivity!!, requestCode, resultCode, data)
    }

    private fun finishActivity() {
        mDialogManager.destroy()
        mActivity?.finish()
        mActivity = null
    }

    private fun launchSettingActivity() {

        val context = mPrivateRoomLightContext

        context ?: return

        if (context.isMaster()) {
            val payload = PrivateRoomSettingActivity.PayLoad()

            payload.password = context.getRoomPassword()
            payload.description = context.getRoomIntroduce()
            payload.title = context.getRoomTitle()
            payload.color = context.getTagColor()
            payload.tag = context.getTag()
            payload.roomId = context.getRoomId()
            payload.roomType = context.getRoomType()

            PrivateRoomSettingActivity.Launch(mActivity ?: return, payload)
        }
    }

    private fun launchPaperPlaneActivty(fromPosition: Int) {
        /**
         * 发送纸飞机
         */
        val member = mPrivateRoomLightContext?.getMember(fromPosition)

        member ?: return

        val payload = PrivateRoomPaperPlaneActivity.PayLoad()
        payload.background = mPrivateRoomLightContext?.getBackGroundUrl() ?: ""
        payload.blur = TextUtils.equals(mPrivateRoomLightContext?.getBackGroundType()
                ?: "", VoiceRoomBackGroundApiResult.BLUR_AVATAR)
        val dataHolder = ArrayList<PrivateRoomPaperPlaneActivity.DataHolder>()
        mPrivateRoomLightContext?.getSeatsData()?.forEach {
            if (it.getSeatMemberData() == null) {
                dataHolder.add(PrivateRoomPaperPlaneActivity.EMPTY_DATA_HOLDER)
            } else {
                if (TextUtils.equals(it.getSeatMemberData()!!.getMemberId(), member.getMemberId()) && !member.memberIsSelf()) {
                    dataHolder.add(PrivateRoomPaperPlaneActivity.TurnIntoDataHolder(it.getSeatMemberData()!!, true))
                } else {
                    dataHolder.add(PrivateRoomPaperPlaneActivity.TurnIntoDataHolder(it.getSeatMemberData()!!, false))
                }
            }
            if (it.getSeatPosition() == PrivateRoomTools.DEPUTY_POSITION) {
                /**
                 * 填充数据
                 */
                dataHolder.add(PrivateRoomPaperPlaneActivity.EMPTY_PLACE_HOLDER)
                dataHolder.add(PrivateRoomPaperPlaneActivity.EMPTY_PLACE_HOLDER)
            }
        }
        payload.mUpSeatsData = dataHolder
        payload.mFrom = member
        PrivateRoomPaperPlaneActivity.Launch(mActivity!!, payload)
    }

    override fun onBeforeSendGameEmotion(): Boolean {
        if (!PrivateRoomTools.PositionInUpSeat(getSelfPosition())) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_private_room_upseat_enable))
            return false
        }
        return true
    }

    override fun onShowGameEmotion(target: View) {

    }

    private fun printlnLog(info: String) {
        Log.i("PrivateRoom", info)
    }
}