package orangelab.project.voice.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * windows metro风格的ImageView
 */

public class MetroStyleImageView extends android.support.v7.widget.AppCompatImageView implements Destroyable {

    private static final int SCALE_REDUCE = 10001;//减小
    private static final int SCALE_ADD = 10002;//放大

    private float baseValue = 0.8f;//缩放基准值,0-1之间
    private int maxScaleCount = 5;//缩放梯度最大值
    private int scaleCount = 0;//当前缩放梯度
    private float scale = baseValue;//当前缩放基准值

    private int mWidth;
    private int mHeight;
    private int mCenterWidth;
    private int mCenterHeight;

    private OnClickListener onClickListener;

    public MetroStyleImageView(Context context) {
        this(context, null);
    }

    public MetroStyleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MetroStyleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                scaleCount = 0;
                scale = baseValue;
                scaleHandler.removeMessages(SCALE_ADD);
                scaleHandler.sendEmptyMessage(SCALE_REDUCE);
                break;
            case MotionEvent.ACTION_UP:
                scaleCount = maxScaleCount - scaleCount;
                scale = baseValue;
                scaleHandler.removeMessages(SCALE_REDUCE);
                scaleHandler.sendEmptyMessage(SCALE_ADD);
                break;
        }

        return true;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {

            mWidth = getWidth() - getPaddingLeft() - getPaddingRight();
            mHeight = getHeight() - getPaddingTop() - getPaddingBottom();
            mCenterWidth = mWidth >> 1;
            mCenterHeight = mHeight >> 1;

            Drawable drawable = getDrawable();
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            bitmapDrawable.setAntiAlias(true);
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler scaleHandler = new Handler() {

        Matrix matrix = new Matrix();

        @Override
        public void handleMessage(Message msg) {

            matrix.set(getImageMatrix());
            switch (msg.what) {
                case SCALE_REDUCE:
                    //缩小图片
                    if (scaleCount < maxScaleCount) {
                        scale = (float) Math.sqrt(Math.sqrt(baseValue));
                        doScale(matrix, scale);
                        scaleCount++;
                        scaleHandler.sendEmptyMessage(SCALE_REDUCE);
                    }
                    break;
                case SCALE_ADD:
                    //放大图片
                    if (scaleCount < maxScaleCount) {
                        scale = (float) Math.sqrt(Math.sqrt(1.0f / baseValue));
                        doScale(matrix, scale);
                        scaleCount++;
                        scaleHandler.sendEmptyMessage(SCALE_ADD);
                    } else {
                        if (onClickListener != null) {
                            onClickListener.onClick(MetroStyleImageView.this);
                        }
                    }
                    break;
            }
        }
    };

    private synchronized void doScale(Matrix matrix, float scale) {
        matrix.postScale(scale, scale, mCenterWidth, mCenterHeight);
        setImageMatrix(matrix);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        if (!isClickable()) {
            setClickable(true);
        }
        this.onClickListener = l;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        scaleHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void destroy() {
        scaleHandler.removeCallbacksAndMessages(null);
    }
}
