package orangelab.project.voice.component;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;

import orangelab.project.MainApplication;
import orangelab.project.common.PersonalDataHelper;

import com.R;

import org.greenrobot.eventbus.EventBus;

import orangelab.project.common.effect.event.RoomEffectConsoleViewEvent;
import orangelab.project.common.event.DebugEvent;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.ExhibitionsEvent;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.handler.AbstractSocketMessageHandler;
import orangelab.project.voice.handler.VoiceMessageBean;
import orangelab.project.voice.handler.VoiceServerMessageHandler;
import orangelab.project.voice.manager.VoiceEmotionManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomAudioSocketManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.model.VoiceSpySpeech;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.popwindow.EmotionPopWindow;
import orangelab.project.voice.utils.PositionHelper;

import cn.intviu.support.GsonHelper;

/**
 * Created by lloydfinch on 11/09/2017.
 * <p>
 * (2018/01/26)模块重构，减轻逻辑，函数单一职责
 */

public class VoiceSpeakView extends AbstractSocketMessageHandler implements VoiceSpeakViewComponent, TextWatcher, View.OnClickListener, View.OnTouchListener {

    private static final String TAG = "VoiceSpeakView";

    private ViewGroup container;
    private ImageView ivSwitchVoiceOrChat;//切换语音/打字按钮
    private ImageView ivOpenCloseMic;//自由模式下展示的左侧的麦克风
    private View speakByMouse;//按住说话
    private TextView speakByMouseText;//按住说话的文本框
    private EditText speakByHand;//打字
    private View btnEmotion;//表情按钮
    private Button sendGift;//送礼按钮
    private Button sendMessage;//发送按钮
    private Context mContext;
    private boolean isSpeak = false;

    public VoiceSpeakView(ViewGroup container) {
        super();
        mContext = container.getContext();
        this.container = container;
        initBaseView(container);
        initListener();
    }

    private void initBaseView(View view) {
        speakByMouse = view.findViewById(R.id.speak_by_mouth);
        speakByHand = (EditText) view.findViewById(R.id.speak_by_hand);
        sendMessage = (Button) view.findViewById(R.id.send_message);
        sendGift = (Button) view.findViewById(R.id.gift_button);
        speakByMouseText = (TextView) view.findViewById(R.id.speak_by_mouth_text);
        ivSwitchVoiceOrChat = (ImageView) view.findViewById(R.id.switch_speak_way);
        speakByHand.setImeOptions(EditorInfo.IME_ACTION_SEND);
        speakByHand.setFilters(new InputFilter[]{new InputFilter.LengthFilter(500)});
        ivOpenCloseMic = (ImageView) view.findViewById(R.id.iv_voice_mic);
        btnEmotion = view.findViewById(R.id.btn_send_emotion);
    }

    private void initListener() {
        speakByMouse.setOnTouchListener(this);
        ivSwitchVoiceOrChat.setOnClickListener(this);
        sendMessage.setOnClickListener(this);
        sendGift.setOnClickListener(this);
        ivOpenCloseMic.setOnClickListener(this);
        btnEmotion.setOnClickListener(this);
        speakByHand.addTextChangedListener(this);

        initHideKeyboardEvent();
        initKeyboardListener();

        registerKtvMode();
    }

    private void registerKtvMode() {
        if (VoiceRoomConfig.isIsKTV()) {
            VoiceServerMessageHandler.getInstance().registerUIHandler(observerKTV);
        }
    }

    private void initKeyboardListener() {
        container.getViewTreeObserver().addOnGlobalLayoutListener(
                () -> {
                    Rect r = new Rect();
                    container.getWindowVisibleDisplayFrame(r);
                    int screenHeight = container.getRootView().getHeight();
                    int heightDifference = screenHeight - (r.bottom);
                    if (heightDifference > 200) {
                        sendGift.setVisibility(View.GONE);
                        btnEmotion.setVisibility(View.GONE);
                        sendMessage.setVisibility(View.VISIBLE);
                        speakByHand.requestFocus();
                    } else {
                        sendGift.setVisibility(View.VISIBLE);
                        btnEmotion.setVisibility(View.VISIBLE);
                        sendMessage.setVisibility(View.GONE);
                        speakByHand.clearFocus();
                    }
                });
    }

    private void initHideKeyboardEvent() {
        RxToolKit.Build(this, ViewEvent.HideKeyBoardEvent.class)
                .action(hideKeyBoardEvent -> hideKeyboard())
                .register();
    }

    private void hideKeyboard() {
        if (speakByHand != null && speakByHand.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) speakByHand
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(speakByHand.getWindowToken(), 0);
            }
            speakByHand.clearFocus();
        }

        btnEmotion.setVisibility(View.VISIBLE);
        sendGift.setVisibility(View.VISIBLE);
        sendMessage.setVisibility(View.GONE);
    }

    private void showKeyboard() {
        if (speakByHand != null && speakByHand.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) speakByHand
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(speakByHand, InputMethodManager.SHOW_FORCED);
            }
            speakByHand.requestFocus();
        }

        btnEmotion.setVisibility(View.GONE);
        sendGift.setVisibility(View.GONE);
        sendMessage.setVisibility(View.VISIBLE);
    }

    private void startSpeak(int event) {
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(event));
        RoomSocketEngineHelper.startSpeak();
    }

    private void endSpeak(int event) {
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(event));
        RoomSocketEngineHelper.endSpeak();
    }

    //外界控制开关
    private boolean canSpeak = false;

    @Override
    public void displayData() {
        handleSpeak(true);
        restoreFormSmallWindow();
    }

    @Override
    public void handleSpeak(boolean canSpeak) {
        PLog.e(TAG, "canSpeak:" + canSpeak);
        this.canSpeak = canSpeak;
        if (canSpeak && PositionHelper.isUpSeat()) {
            normalCanSpeak();
        } else {
            normalNoSpeak();
        }
    }

    @Override
    public void handleForceSeat(VoiceMessageBean messageBean) {
        handleUpSeat(messageBean);
    }

    //被禁言
    private void banSpeak() {
        handleSpeak(false);
        ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ico_voice_ban_speak_mic);
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
    }

    //解除禁言
    private void canSpeak() {
        handleSpeak(true);
        ivSwitchVoiceOrChat.setBackgroundResource(R.drawable.bg_voice_message);
    }

    private void onTouchDownSpeaking() {
        speakByMouseText.setText(R.string.str_up_end);
        speakByMouse.setBackgroundResource(R.drawable.bg_voice_speak_view_press);
    }

    //自己点击禁麦
    private void onTouchUpEndSpeak() {
        speakByMouseText.setText(R.string.str_press_speak);
        speakByMouse.setBackgroundResource(R.drawable.bg_voice_speak_view);
    }

    private void sendStartSpeak() {
        boolean audioServerIsConnected = VoiceRoomAudioSocketManager.getInstance().isConnected();
        PLog.e(TAG, "sendStartSpeak: " + audioServerIsConnected);
        if (audioServerIsConnected) {
            startSpeak(MotionEvent.ACTION_DOWN);
        }
    }

    private void sendEndSpeak() {
        boolean audioServerIsConnected = VoiceRoomAudioSocketManager.getInstance().isConnected();
        PLog.e(TAG, "sendEndSpeak: " + audioServerIsConnected);
        //保险处理，不再判断是否连接
        endSpeak(MotionEvent.ACTION_UP);
    }

    private void switchMode() {
        if (speakByHand.getVisibility() != View.VISIBLE) {
            speakByHand();
        } else {
            speakByMouse();
        }
    }

    //说话
    private void speakByMouse() {
        RxToolKit.Emit(new ViewEvent.GiftButtonViewEvent.RequestShow());
        normalCanSpeak();
    }

    //打字
    private void speakByHand() {
        RxToolKit.Emit(new ViewEvent.GiftButtonViewEvent.RequestDismiss());
        normalNoSpeak();
        speakByHand.setVisibility(View.VISIBLE);
        ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ic_voice_radio);
        speakByHand.requestFocus();
        showKeyboard();
    }

    @Override
    public void destroy() {
        VoiceServerMessageHandler.getInstance().unRegisterUIHandler(observerKTV);
        RxToolKit.UnRegister(this);
//        destroyWindow();
        if (container != null) {
            container.removeAllViews();
        }
        VoiceEmotionManager.getInstance().destroy();
    }

//    private void destroyWindow() {
//        if (window != null) {
//            if (window.isShowing()) {
//                window.dismiss();
//            }
//            window.destroy();
//            window = null;
//        }
//    }

    private void sendMessage(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            RoomSocketEngineHelper.sendChatMessage(msg);
            speakByHand.setText("");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void handleSendMessage() {
        String message = speakByHand.getText().toString().trim();
        if (TextUtils.equals(message, "/1727")) {
            RxToolKit.Emit(new DebugEvent.DeveloperShowDebugEvent());
            return;
        }
        sendMessage(message);
    }

    //自由模式下点击消息按钮
    private void showInput() {
        RxToolKit.Emit(new ViewEvent.GiftButtonViewEvent.RequestDismiss());
        speakByMouse.setVisibility(View.GONE);
        speakByHand.setVisibility(View.VISIBLE);
        ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ic_voice_radio);
        speakByHand.requestFocus();
        showKeyboard();
    }

    @Override
    public void handleServerEvent(ServerMessageEventOver event) {
        PLog.e(TAG, "event:" + event);
        handleMessageEvent(event);
    }

    @Override
    public void handleJoin(VoiceMessageBean messageBean) {
        ServerMessageEnterResult item = GsonHelper.getGson().fromJson
                (messageBean.payload.toString(), ServerMessageEnterResult.class);
        if (TextUtils.equals(item.user.id, GlobalUserState.getGlobalState().getUserId())) {
            handleSpeak();
        }
    }

    @Override
    public void handleUpSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        if (PositionHelper.isSelf(userId)) {
            restoreFormSmallWindow();
        }
    }

    @Override
    public void restoreFormSmallWindow() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        //防止server的非法返回
        if (personalData != null && personalData.getSelfPosition() <= VoiceRoomConfig.getMaxChairNumber()) {
            if (personalData.isFreeStyle()) {
                showFreeStyle();
                if (personalData.isSpeaking()) {
                    RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_DOWN));
                } else {
                    sendEndSpeak();
                    unSpeak();
                }
            } else {
                normalCanSpeak();
                if (personalData.isBanMic()) {
                    banSpeak();
                } else {
                    canSpeak();
                }
            }
        }

        //断线重连：游戏正在进行，不能插麦，不是房主就不能说话
        if (VoiceRoomConfig.isIsPlaying() && !VoiceRoomConfig.SpyConfig.isCanInsertMic() && !PositionHelper.isMaster()) {
            noInsertMic();
        }
    }

    @Override
    public void handleDownSeat(VoiceMessageBean messageBean) {
        String userId = messageBean.payload.optString(VoiceConstants.USER_ID_LOWER);
        if (PositionHelper.isSelf(userId)) {
            PersonalDataHelper.getPersonalData().setBanMic(false);
            PersonalDataHelper.getPersonalData().setFreeStyle(false);
            handleSpeak();
            ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ic_voice_radio);
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
        }
    }

    @Override
    public void handleChangeRoomOwner(VoiceMessageBean messageBean) {
        if (PositionHelper.isMaster()) {
            return;
        }
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        if (position == PersonalDataHelper.getPersonalData().getSelfPosition()) {
            handleSpeak();
        }
    }

    @Override
    public void handleRestoreRoom(VoiceMessageBean messageBean) {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            isSpeak = personalData.isSpeaking();
            //这里加个保险，防止服务器不推送
            if (!isSpeak) {
                endSpeak(MotionEvent.ACTION_UP);
            }

            if (personalData.isFreeStyle()) {
                showFreeStyle();
                if (isSpeak) {
                    //通知语音服务器重新连接
                    RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.RECONNECT_FROM_AUDIO_SERVER));
                    ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_speak_mic);
                    startSpeak(MotionEvent.ACTION_DOWN);
                } else {
                    ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_ban_speak_mic);
                    //再加个保险
                    endSpeak(MotionEvent.ACTION_UP);
                }
            } else {
                handleSpeak();
            }
        }
        if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
            RestoreResult result = GsonHelper.getGson().fromJson(messageBean.payload.toString(), RestoreResult.class);
            if (VoiceRoomConfig.SpyConfig.isCanInsertMic() || PositionHelper.isMaster()) {
                canInsertMic();
            } else {
                noInsertMic();
            }
            if (personalData.isOut()) {
                noInsertMic();
            } else {
                restoreSpeech(result.game_info);
            }
        }
    }

    private void restoreSpeech(RestoreResult.RestoreRoleMessage game_info) {
        if (game_info != null) {
            if (game_info.speech_info != null && game_info.speech_info.current != null) {
                int position = game_info.speech_info.current.position;//此人正在说话
                if (PositionHelper.isSelfPosition(position)) {
                    canInsertMic();
                }
            }
        }
    }

    //根据位置判断显示speakview的UI
    private void handleSpeak() {
        if (PersonalDataHelper.getPersonalData().getSelfPosition() <= VoiceRoomConfig.getMaxChairNumber()) {
            if (!PersonalDataHelper.getPersonalData().isBanMic()) {
                handleSpeak(true);
            } else {
                handleSpeak(false);
            }
        } else {
            handleSpeak(false);
        }
    }

    @Override
    public void handleChangeState(VoiceMessageBean messageBean) {
        int position = RoomSocketEngineHelper.getPositionFromJSON(messageBean.payload);
        String model = messageBean.payload.optString(VoiceConstants.MODEL);
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null && position == personalData.getSelfPosition()) {

            if (TextUtils.equals(VoiceConstants.LIMIT, model)) {
                if (personalData.isBanMic()) {
                    PLog.e(TAG, "isBanMic");
                    banSpeak();
                    RoomSocketEngineHelper.endSpeak();
                }
            } else if (TextUtils.equals(VoiceConstants.FREE, model)) {
                //2018-02-05 不再接受老版本的自由模式消息
                if (personalData.isFreeStyle()) {
                    PLog.e(TAG, "isFreeStyle");
                    showFreeStyle();
                    sendStartSpeak();
                }
            } else if (TextUtils.equals(VoiceConstants.SPACE_STR, model) || TextUtils.isEmpty(model)) {
                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                if (!personalData.isFreeStyle()) {
                    PLog.e(TAG, "isNOFreeStyle");
                    normalCanSpeak();
                    RoomSocketEngineHelper.endSpeak();
                }
                if (!personalData.isBanMic()) {
                    PLog.e(TAG, "isNOBanMic");
                    canSpeak();
                    handleSpeak(true);
                }
            }
        }
    }

    @Override
    public void handleSpyGameStart(VoiceMessageBean messageBean) {
        if (!VoiceRoomConfig.isIsPlaying()) {
            return;
        }
        //游戏开始，不是房主，且不可插麦，就不能说话
        if (!PositionHelper.isMaster() && !VoiceRoomConfig.SpyConfig.isCanInsertMic()) {
            noInsertMic();
        }
    }

    @Override
    public void handleSpyGameSpeech(VoiceMessageBean messageBean) {
        VoiceSpySpeech spySpeech = GsonHelper.parseJsonToBean(messageBean.payload.toString(), VoiceSpySpeech.class);
        if (PositionHelper.isSelfPosition(spySpeech.position)) {
            if (!spySpeech.dead) {
                //轮到自己发言，且自己没出局，就显示为：正常可发言模式(无视自由模式和禁言)
                canInsertMic();
            }
        } else {
            //不是轮到自己说话
            if (VoiceRoomConfig.SpyConfig.isCanInsertMic()) {
                //可插麦
                canInsertMic();
            } else {
                //不可插麦,是房主才可以说话
                if (!PositionHelper.isMaster()) {
                    noInsertMic();
                }
            }
        }
    }

    //原来放在handleSpyGameVoteResult中，放在这里处理
    @Override
    public void handleDeathInfo(VoiceMessageBean messageBean) {
        if (PositionHelper.isMaster()) {
            return;
        }
        if (PersonalDataHelper.getPersonalData().isOut()) {
            //自己被票出
            noInsertMic();
        }
    }

    @Override
    public void handleBeforeVote(VoiceMessageBean messageBean) {
        //不能插麦，不是房主，就不能说话
        if (!VoiceRoomConfig.SpyConfig.isCanInsertMic() && !PositionHelper.isMaster()) {
            noInsertMic();
        }
    }

    @Override
    public void handleSpyGameOver(VoiceMessageBean messageBean) {
        VoiceRoomConfig.setIsPlaying(false);
        //游戏结束可以说话
        canInsertMic();
    }

    private void canInsertMic() {
        //添加"可以插麦"情况下，出局也不能说话
        if (PositionHelper.isUpSeat() && !PersonalDataHelper.getPersonalData().isOut()) {
            normalCanSpeak();
            ivSwitchVoiceOrChat.setEnabled(true);
        } else {
            noInsertMic();
        }
    }

    private void noInsertMic() {
        sendEndSpeak();
        if (PositionHelper.isUpSeat()) {
            normalNoSpeak();
            ivSwitchVoiceOrChat.setEnabled(false);
        }
    }

    //自由模式
    private void showFreeStyle() {
        ivOpenCloseMic.setVisibility(View.VISIBLE);
        speakByHand.setVisibility(View.VISIBLE);
        speakByHand.clearFocus();
        speakByMouse.setVisibility(View.GONE);
        ivSwitchVoiceOrChat.setVisibility(View.GONE);
        btnEmotion.setBackgroundResource(R.drawable.bg_voice_emotion);
        btnEmotion.setClickable(true);

        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            isSpeak = personalData.isSpeaking();
            if (personalData.isSpeaking()) {
                ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_speak_mic);
            } else {
                ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_ban_speak_mic);
            }
        }
    }

    private void normalNoSpeak() {
        ivOpenCloseMic.setVisibility(View.GONE);
        speakByHand.setVisibility(View.VISIBLE);
        ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ic_voice_radio);
        speakByMouse.setVisibility(View.GONE);
        ivSwitchVoiceOrChat.setVisibility(View.VISIBLE);
        btnEmotion.setVisibility(View.VISIBLE);
        sendGift.setVisibility(View.VISIBLE);
        sendMessage.setVisibility(View.GONE);

        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null && personalData.isBanMic()) {
            ivSwitchVoiceOrChat.setBackgroundResource(R.mipmap.ico_voice_ban_speak_mic);
        }
        if (personalData != null && personalData.getSelfPosition() <= VoiceRoomConfig.getMaxChairNumber()) {
            btnEmotion.setBackgroundResource(R.drawable.bg_voice_emotion);
            btnEmotion.setClickable(true);
        } else {
            btnEmotion.setBackgroundResource(R.mipmap.ico_voice_speak_emotion);
            btnEmotion.setClickable(false);
        }
    }

    private void normalCanSpeak() {
        normalNoSpeak();
        speakByMouse.setVisibility(View.VISIBLE);
        speakByHand.setVisibility(View.INVISIBLE);
        ivSwitchVoiceOrChat.setBackgroundResource(R.drawable.bg_voice_message);
        btnEmotion.setBackgroundResource(R.drawable.bg_voice_emotion);
        btnEmotion.setClickable(true);

        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null && personalData.getSelfPosition() <= VoiceRoomConfig.getMaxChairNumber()) {
            btnEmotion.setBackgroundResource(R.drawable.bg_voice_emotion);
            btnEmotion.setClickable(true);
        } else {
            btnEmotion.setBackgroundResource(R.mipmap.ico_voice_speak_emotion);
            btnEmotion.setClickable(false);
        }
    }

    //发过消息就还原为说话状态
    private void hideInput() {
        if (PersonalDataHelper.getPersonalData().isFreeStyle()) {
            //speakByHand.setVisibility(View.INVISIBLE);
            ivSwitchVoiceOrChat.setBackgroundResource(R.drawable.bg_voice_message);
            speakByMouse.setVisibility(View.GONE);
            sendGift.setVisibility(View.GONE);
        } else {
            if (canSpeak && PositionHelper.isUpSeat()) {
                normalCanSpeak();
            } else {
                normalNoSpeak();
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!judgeCanSpeak()) {
                    return true;
                }
                onTouchDownSpeaking();
                sendStartSpeak();
                break;

            case MotionEvent.ACTION_MOVE:
                //这里加保险处理
                if (!judgeCanSpeak()) {
                    sendEndSpeak();
                }
                break;
            case MotionEvent.ACTION_UP:
                onTouchUpEndSpeak();
                sendEndSpeak();
                break;
        }
        return true;
    }

    /**
     * 判断能否说话
     * 能说话，在坐上，才能说话
     */
    private boolean judgeCanSpeak() {
        boolean can = canSpeak &&
                PositionHelper.isUpSeat() &&
                VoiceRoomGameSocketManager.getInstance().isConnected();
        return can;
    }

//    private EmotionPopWindow window;
//
//    private void showEmotion() {
//        destroyWindow();
//        window = new EmotionPopWindow(mContext);
//        window.setFocusable(true);
//        window.setTouchable(true);
//        window.showAtLocation(container, Gravity.BOTTOM, 0, 0);
//    }

    private void unSpeak() {
        isSpeak = false;
        ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_ban_speak_mic);
    }

    private void speak() {
        isSpeak = true;
        ivOpenCloseMic.setImageResource(R.mipmap.ico_voice_speak_mic);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.send_message) {
            handleSendMessage();
            hideKeyboard();
            //"谁是卧底"、正在游戏、不可插麦、不是房主:发过消息后speak保持不变
            if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying() && !VoiceRoomConfig.SpyConfig.isCanInsertMic() && !PositionHelper.isMaster()) {
                return;
            }
            //已经出局，不变
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData == null || personalData.isOut()) {
                return;
            }
            hideInput();
        } else if (id == R.id.switch_speak_way) {
            if (PersonalDataHelper.getPersonalData().isBanMic()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.has_been_mute));
            } else {
                //只有能说话的人才能切换模式
                if (canSpeak && PositionHelper.isUpSeat()) {
                    switchMode();
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_can_send_voice));
                }
            }
        } else if (id == R.id.gift_button) {
            if (GlobalUserState.getGlobalState().isTourist()) {
                ToastToolKit.showShort(R.string.gifts_preset_from_tourist);
            } else {
                RxToolKit.Emit(new ExhibitionsEvent.ExhibitionsByGameEvent());
            }
        } else if (id == R.id.iv_voice_mic) {
            if (isSpeak) {
                sendEndSpeak();
                unSpeak();
            } else {
                sendStartSpeak();
                speak();
            }
        } else if (id == R.id.btn_send_emotion) {
            EventBus.getDefault().post(new RoomEffectConsoleViewEvent(VoiceConstants.SHOW_EMOTION));
        }
    }


    private AbstractSocketMessageHandler observerKTV = new AbstractSocketMessageHandler() {

        @Override
        public void handleAuthorizeSing(VoiceMessageBean messageBean) {

        }

        @Override
        public void handleStartSing(VoiceMessageBean messageBean) {
            String id = MusicCompanyConfig.singerId;
            if (PositionHelper.isSelf(id)) {
                //自己开启了伴奏，等价于开启自由模式
                showFreeStyle();
            }
        }

        @Override
        public void handleStopSing(VoiceMessageBean messageBean) {

            //停止了伴奏,等价于关了自由模式
            normalCanSpeak();
            //sendEndSpeak();
            //RoomSocketEngineHelper.endSpeak();
        }

        @Override
        public void destroy() {

        }
    };
}
