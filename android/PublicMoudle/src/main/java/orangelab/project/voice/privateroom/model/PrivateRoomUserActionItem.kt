package orangelab.project.voice.privateroom.model

import com.toolkit.action.Action
import com.toolkit.action.ActionDismiss
import com.toolkit.action.Keepable

/**
 * game_werewolf
 * 2018/3/22 下午2:51
 * Mystery
 */

class PrivateRoomUserActionItem : Keepable {

    var actionResourceId: Int = -1
    var actionText: String = ""
    var actionRunnable: Action<ActionDismiss>? = null

    constructor(actionResourceId: Int, actionText: String, actionRunnable: Action<ActionDismiss>?) {
        this.actionResourceId = actionResourceId
        this.actionText = actionText
        this.actionRunnable = actionRunnable
    }

    companion object {
        val EmptyAction: PrivateRoomUserActionItem = PrivateRoomUserActionItem(-1, "", null)
    }
}
