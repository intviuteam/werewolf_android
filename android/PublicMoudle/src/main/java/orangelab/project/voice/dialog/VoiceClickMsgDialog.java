package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.R;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.utils.PositionHelper;

/**
 * Created by lloydfinch on 09/01/2018.
 */

public class VoiceClickMsgDialog extends SafeDialog {

    public static final String TAG = "VoiceClickMsgDialog";

    private View btnSendGift;
    private View btnKickOut;
    private View btn_close;
    private View btnCancel;
    private UserHeadView mHeadView;
    private TextView userName;
    private TextView userLevel;
    private TextView userPopularity;
    private View llUserInfoContainer;

    private Context mContext;
    private EnterRoomResult.EnterRoomUserItem user;

    public VoiceClickMsgDialog(@NonNull Context context, EnterRoomResult.EnterRoomUserItem user) {
        this(context, R.style.DarkDialog, user);
    }

    public VoiceClickMsgDialog(@NonNull Context context, @StyleRes int themeResId, EnterRoomResult.EnterRoomUserItem user) {
        super(context, themeResId);
        this.user = user;
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
        initData();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_click_msg_tag, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llUserInfoContainer = contentView.findViewById(R.id.rl_user_detail_container);
        btnSendGift = contentView.findViewById(R.id.btn_send_gift);
        btnKickOut = contentView.findViewById(R.id.btn_add_friend);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        mHeadView = (UserHeadView) findViewById(R.id.user_head_view);
        userName = (TextView) findViewById(R.id.voice_user_name);
        userLevel = (TextView) findViewById(R.id.voice_user_level);
        userPopularity = (TextView) findViewById(R.id.voice_user_popularity);
        btn_close = findViewById(R.id.ibtn_close);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        params.gravity = Gravity.BOTTOM;
        params.y = ScreenUtils.dip2px(80);
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSendGift.setOnClickListener(v -> {
            sendGift();
            dismiss();
        });
        btnKickOut.setOnClickListener(v -> addBlack());
        llUserInfoContainer.setOnClickListener(v -> {
            toPersonalInfoActivity();
        });
        btn_close.setOnClickListener(v -> dismiss());
        btnCancel.setOnClickListener(v -> dismiss());
    }

    private void toPersonalInfoActivity() {
        if (user != null) {
            if (Utils.checkUserIdIsTourist(user.id)) {
                ToastToolKit.showShort(R.string.str_login_info);
                return;
            }
            PersonalInfoActivity.Launch(mContext, user.id);
        }
    }

    private void getUserInfo(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        try {
            ApiManager.GetUserInfoApi(userId, (userInfoResult, e)
                    -> runSafely(() -> updateData(userInfoResult)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        if (user != null) {
            getUserInfo(user.id);
        }
    }

    private void updateData(UserInfoResult result) {
        mHeadView.setUserSex(result.sex);
        mHeadView.setUserHeadImageUrl(result.image);
        userName.setText(result.name);
        userLevel.setText(
                MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level))
        );
        userPopularity.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));
    }

    //送礼物
    private void sendGift() {
        if (user == null) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
        }
        if (GlobalUserState.getGlobalState().isTourist()) {
            ToastToolKit.showShort(R.string.str_login_send_gift);
            return;
        }
        if (Utils.checkUserIdIsTourist(user.id)) {
            ToastToolKit.showShort(R.string.str_login_send_gift_2);
            return;
        }
        ExhibitionsActivity.startActivityFromGame(
                mContext, user.id, user.name, user.sex, user.avatar, user.position);
    }

    //拉黑
    private void addBlack() {
        if (user != null) {
            if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
                //"谁是卧底"正在进行中
                if (PositionHelper.isUpSeat(user.position)) {
                    //麦上玩家不可踢出
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_kickout));
                } else {
                    if (TextUtils.equals(user.id, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                        ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
                    } else {
                        RoomSocketEngineHelper.addVoiceBlack(user.id);
                    }
                }
            } else {
                //GameHelper.addVoiceBlack(userItem.id);
                if (TextUtils.equals(user.id, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
                } else {
                    RoomSocketEngineHelper.addVoiceBlack(user.id);
                }
            }
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave));
        }
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    protected void release() {
        mContext = null;
    }


}
