package orangelab.project.voice.privateroom

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.PicassoUtils
import com.R
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.utils.CountDownTaskManager
import orangelab.project.common.utils.MessageUtils
import com.squareup.picasso.Callback

/**
 *game_werewolf
 *2018/4/1 下午5:23
 *Mystery
 */
class PrivateRoomTempPictureDialog : SafeDialog {

    private var mUrl: String
    private var mImageView: ImageView
    private var mLoading: View
    private var mCountDownText: TextView

    private var mCountDownTask: CountDownTaskManager.CountDownTask? = null

    constructor(context: Context, imageUrl: String, countDownSeconds: Int, crash: Boolean) : super(context, R.style.radius_dialog) {
        setContentView(R.layout.layout_private_room_temp_picture)
        mUrl = imageUrl
        mImageView = findViewById<ImageView>(R.id.dialog_picture) as ImageView
        mCountDownText = findViewById<TextView>(R.id.dialog_countdown) as TextView
        mLoading = findViewById(R.id.id_loading)

        findViewById<ImageView>(R.id.dialog_close).setOnClickListener { v ->
            dismiss()
        }

        if (crash) {
            crashPicture()
        } else {

            /**
             * 开始加载图片
             */
            mCountDownText.setText(R.string.str_private_room_temp_picture_loading)
            mLoading.visibility = View.VISIBLE

            PicassoUtils.loadImage(getContext(), mUrl, mImageView, 500, 500, object : Callback {
                override fun onSuccess() {
                    runSafely({
                        mLoading.visibility = View.GONE
                        startCountDown(countDownSeconds)
                    })
                }

                override fun onError(e:Exception) {
                    runSafely({
                        mLoading.visibility = View.GONE
                        failedPicture()
                    })
                }
            })
        }
    }

    private fun startCountDown(start: Int) {
        mCountDownTask = CountDownTaskManager.CountDownTask(start, object : CountDownTaskManager.CountDownTaskListener {
            override fun onTick(seconds: Int) {
                runSafely({
                    counting(seconds)
                })
            }

            override fun onStart(startSeconds: Int) {
                runSafely({
                    counting(startSeconds)
                })
            }

            override fun onFinish() {
                runSafely({
                    crashPicture()
                })
            }
        })
        mCountDownTask!!.startTask()
    }

    private fun counting(second: Int) {
        mCountDownText.setText(MessageUtils.getString(R.string.str_private_room_temp_picture_countdown, second.toString()))
    }

    private fun failedPicture() {
        mCountDownText.setText(R.string.str_private_room_temp_picture_failed)
        mImageView.setImageResource(R.mipmap.ico_private_room_temp_picture_crash)
    }

    private fun crashPicture() {
        mCountDownText.setText(R.string.str_private_room_temp_picture_crash)
        mImageView.setImageResource(R.mipmap.ico_private_room_temp_picture_crash)
    }

    override fun dismiss() {
        super.dismiss()
    }

    override fun release() {
        mCountDownTask?.cancelTask()
        mCountDownTask?.destroyTask()
        mCountDownTask = null
    }
}