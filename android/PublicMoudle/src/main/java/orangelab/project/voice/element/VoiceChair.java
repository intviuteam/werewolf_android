package orangelab.project.voice.element;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.R;
import com.androidtoolkit.HttpToolkit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.StreamToolKit;
import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;
import com.squareup.picasso.Callback;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.effect.AnimationEvent;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.dialog.VoiceSeatDialog;
import orangelab.project.voice.ds.Queue;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.gif.BasicComponent;
import orangelab.project.voice.manager.GameScoreReflection;
import orangelab.project.voice.manager.SmallGameManager;
import orangelab.project.voice.manager.VoiceEmotionManager;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;
import orangelab.project.voice.musiccompany.api.MusicSocketHelper;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.utils.PositionHelper;
import orangelab.project.voice.utils.SystemScreenUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lloydfinch on 30/08/2017.
 * 语音房座位
 * //(20180126)逻辑简化;区分本地操作和server操作(是否更改属性);职责划分，大逻辑提取
 */

public class VoiceChair extends BasicComponent implements RoomChair {

    private static final String TAG = "VoiceChair";

    private VoiceChairUI chairUI;

    private boolean isLocked = false;//是否加锁
    private boolean isSated = false;//是否有人
    private boolean isBanMic = false;//是否被禁麦
    private boolean isFreeStyle = false;//是否是自由模式
    private boolean isMaster = false;//是否是房主的位置

    private int mPosition;
    private int desPosition;
    public EnterRoomResult.EnterRoomUserItem user;
    private Context mContext;
    private VoiceSeatDialog voiceSeatDialog;

    public VoiceChair(ViewGroup chairContainer, int position) {
        mPosition = position;
        desPosition = mPosition;
        mContext = chairContainer.getContext();
        this.chairUI = new VoiceChairUI(chairContainer, position);

        initListener();
    }

    private void initListener() {
        initSocketListener();
        initEventListener();
        initViewListener();
    }

    private void initSocketListener() {
        RxToolKit.Build(this, VoiceEvent.GameSocketReconnectEvent.class)
                .action(event -> {
                    if (event.isReconnected) {
                        //do nothing
                    } else {
                        //断网就停止说话
                        onPause();
                    }
                }).register();
    }

    private void initEventListener() {
        //用来接收从在线用户界面传递过来的被点击的用户对象
        RxToolKit.Build(this, VoiceEvent.ResponseToNewChairEvent.class)
                .action(event -> {
                    if (event.responseChairPosition == mPosition) {
                        //抱用户上麦前先发解锁请求
                        RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.UNLOCK, mPosition + 1);
                        //抱用户上麦
                        RoomSocketEngineHelper.sendForceSeat(
                                event.selectedUser.id,
                                event.selectedUser.position,
                                mPosition);
                    }
                }).register();
    }

    private void initViewListener() {
        setOnChairClickListener();
        this.chairUI.setOnPrepareClickListener(view -> clickPrepare());
        this.chairUI.setOnVoteClickListener(view -> vote(mPosition));
        this.chairUI.setEndSpeechListener(view -> sendEndSpeech());
    }

    private void setOnChairClickListener() {
        this.chairUI.setOnChairClickListener(v -> {
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData == null) {
                return;
            }

            isMaster = PositionHelper.isMaster();

            //不是房主，且麦位已被锁，就显示提示信息
            if (!isMaster && isLocked) {
                showTips();
            } else {
                if (!isLocked && !isSated && !isMaster) {
                    if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
                        //"谁是卧底"游戏中
                        return;
                    }
                    if (judgeSocketConnect()) {
                        if (isCanOpSeat()) {
                            if (canUpSeat()) {
                                RoomSocketEngineHelper.sendUpSeat(mPosition);
                            }
                        }
                    }
                } else {
                    if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying() && !isSated) {
                        //"谁是卧底"游戏中,并且是空位
                        return;
                    }
                    showPopWindow();
                }
            }
        });
    }

    private void showPopWindow() {

        if (voiceSeatDialog != null) {
            if (voiceSeatDialog.isShowing()) {
                voiceSeatDialog.dismiss();
            }
            voiceSeatDialog = null;
        }

        voiceSeatDialog = new VoiceSeatDialog(mContext, user == null ? "" : user.id);
        voiceSeatDialog.setMaster(isMaster);
        voiceSeatDialog.setLocked(isLocked);
        voiceSeatDialog.setSeated(isSated);
        voiceSeatDialog.setBanMic(isBanMic);
        voiceSeatDialog.setFreeStyle(isFreeStyle);
        voiceSeatDialog.setPosition(mPosition);
        if (user != null) {
            //伴奏状态或者唱歌状态都视为KTV状态，只能关闭伴奏
            voiceSeatDialog.setKtv(user.canSing || user.isSing);
        } else {
            voiceSeatDialog.setKtv(false);
        }
        if (this.user == null) {
            voiceSeatDialog.setSelf(false);
        } else {
            boolean isSelf = PositionHelper.isSelf(user.id);
            voiceSeatDialog.setSelf(isSelf);
        }

        //抱用户上麦
        voiceSeatDialog.setOnCarryUpSeatListener((view) -> {

            if (judgeSocketConnect()) {
                if (!isSated) {
                    if (isCanOpSeat()) {
                        RxToolKit.Emit(new VoiceEvent.RequestToNewChairEvent(mPosition));
                    }
                } else {
                    showSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //设为旁听
        voiceSeatDialog.setOnSetDownSeatListener((view) -> {

            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isCanOpSeat()) {
                        RoomSocketEngineHelper.sendDownSeat(mPosition);
                    }
                } else {
                    showNoSeated();
                }
            }

            voiceSeatDialog.dismiss();
        });

        //下麦
        voiceSeatDialog.setOnDownSeatListener((view) -> {
            if (judgeSocketConnect()) {
                if (isCanOpSeat()) {
                    sendUnPrepare();//通知其他人取消准备
                    RoomSocketEngineHelper.endSpeak();
                    RoomSocketEngineHelper.sendDownSeat(mPosition);
                }
            }
            voiceSeatDialog.dismiss();
        });

        //游戏房间有的交互需要将position+1

        //踢人
        voiceSeatDialog.setOnKickOutListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated && user != null) {
                    if (isCanOpSeat()) {
                        if (TextUtils.equals(user.id, VoiceRoomConfig.getRoomOwnerId()) && VoiceRoomConfig.isIsPurchased()) {
                            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_can_not_kick_deed_owner));
                        } else {
                            RoomSocketEngineHelper.kickOut(mPosition, user.id);
                        }
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //锁座位
        voiceSeatDialog.setOnLockSeatListener((view) -> {
            if (judgeSocketConnect()) {
                if (isCanOpSeat()) {
                    RoomSocketEngineHelper.sendDownSeat(mPosition);
                    RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.LOCK, mPosition + 1);
                }
            }
            voiceSeatDialog.dismiss();
        });

        //解锁座位
        voiceSeatDialog.setOnUnLockSeatListener((view) -> {
            if (judgeSocketConnect()) {
                if (isCanOpSeat()) {
                    RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.UNLOCK, mPosition + 1);
                }
            }
            voiceSeatDialog.dismiss();
        });

        //查看资料
        voiceSeatDialog.setOnPersonalInfoListener((view) -> {
            if (judgeSocketConnect()) {
                if (Utils.checkUserIdIsTourist(user.id)) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_info));
                    return;
                }
                PersonalInfoActivity.Launch(mContext, user.id);
            }
            voiceSeatDialog.dismiss();
        });

        //送礼
        voiceSeatDialog.setOnSendGiftListener((view) -> {
            if (judgeSocketConnect()) {
                if (GlobalUserState.getGlobalState().isTourist()) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift));
                    return;
                }
                if (Utils.checkUserIdIsTourist(user.id)) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_login_send_gift_2));
                    return;
                }
                ExhibitionsActivity.startActivityFromGame(
                        mContext, user.id, user.name, user.sex, user.avatar, user.position);
            }
            voiceSeatDialog.dismiss();
        });

        //加好友 不需要socket连接
        voiceSeatDialog.setOnAddFriendListener((view) -> {
            ApiManager.AddFriendApi(user.id, value -> postSafely(() -> {
                voiceSeatDialog.dismiss();
            }), value -> postSafely(() -> {
                voiceSeatDialog.dismiss();
            }), value -> postSafely(() -> {
                voiceSeatDialog.dismiss();
            }));
        });

        //禁麦
        voiceSeatDialog.setOnBanSeatListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (!isBanMic) {
                        if (isCanOpSeat()) {
                            RoomSocketEngineHelper.sendBanMic(mPosition);
                        }
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //解除禁麦
        voiceSeatDialog.setOnUnBanSeatListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isBanMic) {
                        if (isCanOpSeat()) {
                            RoomSocketEngineHelper.sendUnBanMic(mPosition);
                        }
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //设为自由模式
        voiceSeatDialog.setOnFreeStyleListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isCanOpSeat()) {
                        //用新版的处理,这里不再对老版做兼容，server已经做了处理
                        VoiceSocketHelper.requestFreeMode(mPosition);
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //解除自由模式
        voiceSeatDialog.setOnUnFreeStyleListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isCanOpSeat()) {
                        RoomSocketEngineHelper.sendUnFreeStyle(mPosition);
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //打开伴奏
        voiceSeatDialog.setOnOpenKtvListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isCanOpSeat()) {
                        MusicSocketHelper.INSTANCE.sendAuthorizeSing(user.id);
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //关闭伴奏
        voiceSeatDialog.setOnCloseKtvListener((view) -> {
            if (judgeSocketConnect()) {
                if (isSated) {
                    if (isCanOpSeat()) {
                        MusicSocketHelper.INSTANCE.sendStopSing();
                    }
                } else {
                    showNoSeated();
                }
            }
            voiceSeatDialog.dismiss();
        });

        //取消
        voiceSeatDialog.setOnCancelClickListener((view) -> voiceSeatDialog.dismiss());
        voiceSeatDialog.setCanceledOnTouchOutside(true);
        voiceSeatDialog.build().show();
    }

    @Override
    public void upSeat(EnterRoomResult.EnterRoomUserItem user) {

        if (user == null) {
            return;
        }

        if (isLocked || isSated) {
            return;
        }

        this.user = user;
        this.isSated = true;
        if (!TextUtils.isEmpty(user.avatar)) {
            PicassoUtils.loadImage(mContext, user.avatar, chairUI.ivHead.getRealHeadImage(), new Callback() {
                @Override
                public void onSuccess() {
                    //头像加载过来之后，人走了
                    if (!isSated) {
                        postSafely(() -> {
                            chairUI.ivHead.setImageResource(R.mipmap.ico_voice_add);
                        });
                    }
                }

                @Override
                public void onError(Exception e) {
                    postSafely(() -> {
                        if (!isSated) {
                            chairUI.ivHead.setImageResource(R.mipmap.ico_voice_add);
                        } else {
                            chairUI.ivHead.setImageResource(R.mipmap.default_head);
                        }
                    });
                }
            });
        } else {
            chairUI.ivHead.setImageResource(R.mipmap.default_head);
        }
        chairUI.tvName.setText(" " + user.name + " ");
        chairUI.tvName.handleVipInfo(user.vipInfo);
        VoiceRoomConfig.addOnSeatNumber();

        /**
         * 用于初始化用户头像框
         */
        chairUI.ivHead.setHeadDecorate(user.avatar_box);

        this.isBanMic = user.isBanMic();
        this.isFreeStyle = user.isFreeStyle();

        if (user.speaking) {
            speak();
        }
        if (user.isBanMic()) {
            banMic();
        } else if (user.isFreeStyle()) {
            openFreeStyle();
        }

        updateOffline();
        tipPrepare();
        setMasterStatus();

        if (user.canSing) {
            showKTV();
            if (user.isSing) {
                hideKTV();
                sing();
            } else {
                showKTV();
                stopSing();
            }
        } else {
            hideKTV();
        }
    }

    @Override
    public void downSeat(int desPosition) {

        if (!canDownSeat()) {
            return;
        }

        chairUI.ivHead.setImageResource(R.mipmap.ico_voice_add);
        chairUI.ivHead.clearHeadDecorate();
        chairUI.tvName.setText("");
        VoiceRoomConfig.subOnSeatNumber();
        endEmotion();
        this.desPosition = desPosition;
        //自由模式状态下临时切换麦位后才可以说话
        boolean canSpeak = (TextUtils.equals(user.state, VoiceConstants.FREE) && this.desPosition == VoiceConstants.TEMPORARY_POSITION);
        if (!canSpeak) {
            if (PersonalDataHelper.getPersonalData() != null && PersonalDataHelper.getPersonalData().getSelfPosition() == mPosition) {
                RoomSocketEngineHelper.endSpeak();
            }
        }

        unSpeak();
        openMic();
        closeFreeStyle();
        if (desPosition != VoiceConstants.TEMPORARY_POSITION) {
            sendUnPrepare();//通知其他人取消准备
        }
        clearPrepareUI();
        this.desPosition = mPosition;
        this.isSated = false;
        canDownSeat = false;

        EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(mPosition));

        hideKTV();
        stopSing();
    }

    private boolean canDownSeat = false;//下麦的例外操作

    //是否能执行下麦
    private boolean canDownSeat() {
        if (canDownSeat) {
            return true;
        }
        if (user == null || user.position != mPosition || !isSated) {
            return false;
        }
        return true;
    }

    @Override
    public void lockSeat() {
        if (isLocked) {
            return;
        }
        chairUI.ivHead.setImageResource(R.mipmap.lock_voice_chair);
        chairUI.ivHead.clearHeadDecorate();
        this.isLocked = true;
        EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(mPosition));
    }

    @Override
    public void unLockSeat() {
        if (!isLocked) {
            return;
        }
        chairUI.ivHead.setImageResource(R.mipmap.ico_voice_add);
        chairUI.ivHead.clearHeadDecorate();
        this.isLocked = false;
    }

    @Override
    public void speak() {
        if (user == null || !isSated || isLocked) {
            return;
        }
        if (isBanMic) {
            return;
        }

        startAnimation();
    }

    @Override
    public void unSpeak() {
        if (!canUnSpeak()) {
            return;
        }

        //是自己的位置才发停止说话
        if (PositionHelper.isSelfPosition(mPosition)) {
            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
        }
        user.speaking = false;
        stopAnimation();
    }

    private boolean canUnSpeak() {
        if (canDownSeat) {
            return true;
        }
        if (user == null || user.position != mPosition) {
            return false;
        }
        return true;
    }

    private void startAnimation() {
        chairUI.llSpeakContainer.setVisibility(View.VISIBLE);

        //ktv房间正在唱歌显示荧光棒
        if (VoiceRoomConfig.isIsKTV() && MusicCompanyConfig.isSing) {
            chairUI.ivSpeak.setImageResource(R.drawable.drawable_bg_music_mic_sing);
        } else {
            chairUI.ivSpeak.setImageResource(R.drawable.drawable_bg_voice_mic);
        }

        AnimationDrawable drawable = (AnimationDrawable) chairUI.ivSpeak.getDrawable();
        if (drawable != null) {
            drawable.start();
        }
    }

    private void stopAnimation() {
        AnimationDrawable drawable = (AnimationDrawable) chairUI.ivSpeak.getDrawable();
        if (drawable != null) {
            drawable.stop();
        }
        chairUI.llSpeakContainer.setVisibility(View.GONE);
    }

    @Override
    public void banMic() {
        if (user == null) {
            return;
        }
        if (user.position == VoiceConstants.MASTER_POSITION) {
            return;
        }
        if (user != null && user.position == mPosition) {
            closeFreeStyle();
        }

        if (!isLocked) {
            chairUI.banMic();
            this.isBanMic = true;
            this.isFreeStyle = false;
        }
    }

    @Override
    public void openMic() {

        if (!canOpenMic()) {
            return;
        }
        if (!isLocked) {
            chairUI.openMic();
            this.isBanMic = false;
        }
    }

    private boolean canOpenMic() {
        if (canDownSeat) {
            return true;
        }
        if (user == null || user.position != mPosition) {
            return false;
        }
        return true;
    }

    @Override
    public void openFreeStyle() {
        if (user != null && user.position == mPosition) {
            openMic();
            user.state = VoiceConstants.FREE;
            PersonalData personalData = PersonalDataHelper.getPersonalData();
            if (personalData != null) {
                if (PositionHelper.isSelf(user.id)) {
                    //1 断网前非自由断网后自由就开启说话
                    if (!personalData.isFreeStyle()) {
                        RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.RECONNECT_FROM_AUDIO_SERVER));
                        RoomSocketEngineHelper.startSpeak();
                        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_DOWN));
                        PersonalDataHelper.getPersonalData().setSpeaking(true);
                    } else {
                        if (personalData.isSpeaking()) {
                            RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.RECONNECT_FROM_AUDIO_SERVER));
                            RoomSocketEngineHelper.startSpeak();
                            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_DOWN));
                        } else {
                            RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.RECONNECT_FROM_AUDIO_SERVER));
                            RoomSocketEngineHelper.endSpeak();
                            RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                        }
                    }
                    PersonalDataHelper.getPersonalData().setFreeStyle(true);
                    PersonalDataHelper.getPersonalData().setBanMic(false);
                }
            }
        }

        if (!isLocked) {
            chairUI.openFreeStyle();
            this.isFreeStyle = true;
            this.isBanMic = false;
        }
    }

    @Override
    public void closeFreeStyle() {
        if (!canCloseFreeStyle()) {
            return;
        }
        if (desPosition != VoiceConstants.TEMPORARY_POSITION) {
            unSpeak();
            if (user.position == PersonalDataHelper.getPersonalData().getSelfPosition()) {
                PersonalDataHelper.getPersonalData().setFreeStyle(false);
            }
        }

        if (!isLocked) {
            chairUI.closeFreeStyle();
            stopAnimation();
            this.isFreeStyle = false;
        }
    }

    private boolean canCloseFreeStyle() {
        if (canDownSeat) {
            return true;
        }
        if (user == null || user.position != mPosition) {
            return false;
        }
        return true;
    }

    @Override
    public void likeRoom() {
        chairUI.likeRoom();
    }

    private volatile boolean isSafeLy = true;//播放表情和播放游戏不可同时进行,所以用同一个标记位
    private Queue<VoiceEmotionManager.Emotions.Emotion> emotionQueue = new Queue<>();
    private SafeHandler emotionHandler = new SafeHandler() {
        @Override
        public void handleMessage(Message msg) {
            VoiceEmotionManager.Emotions.Emotion emotion = emotionQueue.poll();
            if (emotion != null) {
                startEmotion(emotion);
            } else {
                if (isSafeLy) {
                    gameHandler.sendEmptyMessage(0);
                }
            }
        }
    };

    private Queue<SmallGameManager.SmallGame> smallGameQueue = new Queue<>();
    private SafeHandler gameHandler = new SafeHandler() {
        @Override
        public void handleMessage(Message msg) {
            SmallGameManager.SmallGame smallGame = smallGameQueue.poll();
            if (smallGame != null) {
                playGame(smallGame);
            } else {
                if (isSafeLy) {
                    emotionHandler.sendEmptyMessage(0);
                }
            }
        }
    };

    @Override
    public void showEmotion(VoiceEmotionManager.Emotions.Emotion emotion) {
        emotionQueue.enQueue(emotion);
        if (isSafeLy) {
            emotionHandler.sendEmptyMessage(0);
        }
    }

    @Override
    public void showGame(SmallGameManager.SmallGame smallGame) {
        smallGameQueue.enQueue(smallGame);
        if (isSafeLy) {
            gameHandler.sendEmptyMessage(0);
        }
    }

    private void playGame(SmallGameManager.SmallGame smallGame) {
        isSafeLy = false;

        //不支持的游戏类型,就播放下一个
        if (!SmallGameManager.getInstance().supportGameType(smallGame.getMark())) {
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
            return;
        }

        if (TextUtils.equals(VoiceConstants.MIC, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                chouMic(smallGame);
            });
        } else if (TextUtils.equals(VoiceConstants.OCTOPUS, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                octopusResult(smallGame);
            });
        } else if (TextUtils.equals(VoiceConstants.FINGER_GUESSING, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                showGuess(smallGame);
            });
        } else if (TextUtils.equals(VoiceConstants.COINS, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                showCoins(smallGame);
            });
        } else if (TextUtils.equals(VoiceConstants.DICE, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                showDice(smallGame);
            });
        } else if (TextUtils.equals(VoiceConstants.POINTS, smallGame.getMark())) {
            showScore(smallGame);
            return;
        } else if (TextUtils.equals(VoiceConstants.APPLAUSE, smallGame.getMark())) {
            chairUI.ivGame.setOnPlayEndListener(() -> {
                isSafeLy = true;
                gameHandler.sendEmptyMessage(0);
            });
        } else {
            if (!TextUtils.equals(VoiceConstants.LIGHT, smallGame.getMark())) {
                //亮灯没有延迟结果
                chairUI.ivGame.setDelayTime(smallGame.getDelayTime() * 1000);
                chairUI.ivGame.setPauseFrame(smallGame.getLastFrame());
            } else {
                chairUI.ivGame.setDelayTime(0);

            }
            chairUI.ivGame.setOnPlayEndListener(() -> {
                isSafeLy = true;
                gameHandler.sendEmptyMessage(0);
            });
        }
        String path = SmallGameManager.getInstance().findAnimPathByMark(smallGame);
        Observable.just(path).map(s -> {
            if (!TextUtils.isEmpty(path)) {
                try {
                    return StreamToolKit.readStream(mContext.getApplicationContext().getAssets().open(path));
                } catch (IOException e) {
                    e.printStackTrace();
                    PLog.e(TAG, e.getMessage());
                }
            }
            return null;
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> postSafely(() -> {
                    if (bytes != null) {
                        chairUI.ivGame.setBytes(bytes);
                        chairUI.ivGame.start();
                    } else {

                        //播放数据为空直接播放下一个
                        isSafeLy = true;
                        gameHandler.sendEmptyMessage(0);
                    }
                }));
    }

    private void startEmotion(VoiceEmotionManager.Emotions.Emotion emotion) {
        isSafeLy = false;
        chairUI.ivEmotion.setImageBitmap(null);
        chairUI.ivEmotion.setVisibility(View.VISIBLE);
        Observable.just(emotion).map(s -> {
            byte[] data = VoiceEmotionManager.getInstance().findEmotionGif(emotion);
            if (data != null) {
                return data;
            } else {
                return HttpToolkit.doGet(emotion.getUrl());
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> postSafely(() -> {
                    if (bytes != null) {
                        try {
                            chairUI.ivEmotion.setBytes(bytes);
                            chairUI.ivEmotion.startAnimation();
                            postDelaySafely(() -> endEmotion(), 2000);
                        } catch (Exception e) {
                            PLog.e(TAG, "error:" + e.getMessage());
                        }
                    } else {

                        //播放数据为空直接播放下一个
                        isSafeLy = true;
                        emotionHandler.sendEmptyMessage(0);
                    }
                }));
    }

    private void endEmotion() {
        postSafely(() -> {
            chairUI.ivEmotion.setVisibility(View.GONE);
            chairUI.ivEmotion.clearAnimation();
            chairUI.ivEmotion.clear();

            //播完一个后通知播放下一个
            isSafeLy = true;
            emotionHandler.sendEmptyMessage(0);
        });
    }

    //显示抽麦序结果
    private void chouMic(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.tvChouMic.setText(String.valueOf(result));
        chairUI.viewChouMic.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_small_game_chou_mic);
        animation.setFillAfter(true);
        chairUI.tvChouMic.startAnimation(animation);
        postDelaySafely(() -> {
            chairUI.viewChouMic.setVisibility(View.GONE);
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }

    //显示章鱼机结果
    private void octopusResult(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.octopusView.setResult(result);
        chairUI.octopusView.setVisibility(View.VISIBLE);
        chairUI.octopusView.jump();
        postDelaySafely(() -> {
            chairUI.octopusView.setVisibility(View.GONE);
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }

    //猜拳结果
    private void showGuess(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.ivGameResult.setImageResource(GameScoreReflection.guessToImageId(result));
        chairUI.ivGameResult.setVisibility(View.VISIBLE);
        postDelaySafely(() -> {
            chairUI.ivGameResult.setVisibility(View.GONE);
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }

    //抛硬币结果
    private void showCoins(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.ivGameResult.setImageResource(GameScoreReflection.coinToImageId(result));
        chairUI.ivGameResult.setVisibility(View.VISIBLE);
        postDelaySafely(() -> {
            chairUI.ivGameResult.setVisibility(View.GONE);
            isSafeLy = true;
            PLog.e(TAG, "delay:" + delay);
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }

    //骰子结果
    private void showDice(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.ivGameResult.setImageResource(GameScoreReflection.diceToImageId(result));
        chairUI.ivGameResult.setVisibility(View.VISIBLE);
        postDelaySafely(() -> {
            chairUI.ivGameResult.setVisibility(View.GONE);
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }


    //打分结果
    private void showScore(SmallGameManager.SmallGame smallGame) {
        int result = smallGame.getLastFrame();
        int delay = smallGame.getDelayTime() * 1000;
        chairUI.ivGameResult.setImageResource(GameScoreReflection.scoreToImageId(result));
        chairUI.ivGameResult.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_game_score);
        animation.setFillAfter(true);
        chairUI.ivGameResult.startAnimation(animation);
        postDelaySafely(() -> {
            chairUI.ivGameResult.setVisibility(View.GONE);
            chairUI.ivGameResult.clearAnimation();
            isSafeLy = true;
            gameHandler.sendEmptyMessage(0);
        }, delay);
    }

    @Override
    public void tipPrepare() {
        if (user == null || !isSated) {
            return;
        }
        //未准备
        if (!user.prepared) {
            if (PositionHelper.isSelfPosition(mPosition)) {
                chairUI.tipPrepare();
            }
        } else {
            //已准备，未开始游戏
            if (VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying()) {
                updateStatus(VoiceConstants.USER_STATUS_PREPARE);
            } else {
                //TODO 已准备,已开始游戏,根据游戏阶段展示,需要server提供阶段信息: stage/moment
            }
        }
    }

    //展示房主的状态
    private void setMasterStatus() {
        //"谁是卧底"房、没开始游戏(防止restore)、是房主 才展示
        if (VoiceRoomConfig.isSpyRoom() && !VoiceRoomConfig.isIsPlaying() && PositionHelper.isMaster(mPosition)) {
            if (VoiceRoomConfig.SpyConfig.isIsConfiging()) {
                updateStatus(VoiceConstants.MASTER_STATUS_SETTING);
            } else if (VoiceRoomConfig.SpyConfig.isIsConfiged()) {
                updateStatus(VoiceConstants.MASTER_STATUS_SETTING_OVER);
            }
        }
    }

    //更新在线状态
    private void updateOffline() {
        if (user != null) {
            if (PositionHelper.isSelf(user.id)) {
                //自己肯定在线
                chairUI.hideUserStatus();
                user.is_disconnected = false;
                PersonalDataHelper.getPersonalData().setOffline(false);
            } else {
                if (user.is_disconnected) {
                    handleOffline();
                } else {
                    chairUI.hideUserStatus();
                }
            }
        }

        stopSing();
    }

    @Override
    public void unPrepare() {
        clearPrepareUI();
        if (user != null) {
            user.prepared = false;
            if (PositionHelper.isSelfPosition(user.position)) {
                PersonalDataHelper.getPersonalData().setPrepared(false);
            }
        }
    }

    //清除准备相关的UI信息
    private void clearPrepareUI() {
        chairUI.clearStatus();
        chairUI.clearTipPrepare();
    }

    private void sendUnPrepare() {
        if (PositionHelper.isSelfPosition(mPosition)) {
            VoiceSocketHelper.sendSpyGameUnPrepare();
        }
    }

    //点击准备
    public void clickPrepare() {
        VoiceSocketHelper.sendSpyGamePrepare();
    }

    //状态变化
    @Override
    public void updateStatus(int code) {
        if (user == null) {
            return;
        }
        if (code == VoiceConstants.USER_STATUS_OFFLINE) {
            handleOffline();
        }
        if (user.is_disconnected || user.isOut) {
            return;
        }
        switch (code) {
            case VoiceConstants.MASTER_STATUS_SETTING:
                chairUI.momentSetting();
                break;
            case VoiceConstants.MASTER_STATUS_SETTING_OVER:
                chairUI.momentSetFinished();
                break;
            case VoiceConstants.USER_STATUS_PREPARE:
                handlePrepare();
                break;
            case VoiceConstants.USER_STATUS_VOTE:
                chairUI.momentVote();
                break;
            default:
                chairUI.momentNothing();
                break;
        }
    }

    private void handleOffline() {
        chairUI.momentOffLine();
        if (user != null) {
            user.is_disconnected = true;
        }
    }

    private void handlePrepare() {
        if (user == null) {
            return;
        }
        chairUI.momentPrepare();
        user.prepared = true;
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (user.position == personalData.getSelfPosition()) {
            personalData.setPrepared(true);
        }
    }

    //投票给position
    public void vote(int position) {
        VoiceSocketHelper.sendSpyGameVote(position);
    }

    //通知server结束发言
    public void sendEndSpeech() {
        VoiceSocketHelper.sendEndSpeech();
    }

    @Override
    public void showVote(int duration) {
        if (user == null) {
            return;
        }
        if (!user.isOut) {
            chairUI.showVote();
            postDelaySafely(() -> {
                chairUI.hideVote();
            }, duration);
        }
    }

    @Override
    public void endSpeech() {
        chairUI.endSpeech();
    }

    @Override
    public void hideVote() {
        chairUI.hideVote();
    }

    @Override
    public void clearVoteInfo() {
        chairUI.clearVoteInfo();
    }

    //被position投票
    @Override
    public void beVote(int position) {
        chairUI.beVote(position);
    }

    @Override
    public void hideReady() {
        chairUI.hideReady();
    }

    @Override
    public void hideStatus() {
        if (user != null && user.is_disconnected) {
            return;
        }
        chairUI.hideUserStatus();
    }

    @Override
    public void resetStatus() {
        if (user != null) {
            user.prepared = false;
            user.isOut = false;
            if (PositionHelper.isSelf(user.id)) {
                PersonalDataHelper.getPersonalData().setPrepared(false);
                PersonalDataHelper.getPersonalData().setOut(false);
            }
        }
        chairUI.clearStatus();
    }

    @Override
    public void showKTV() {
        chairUI.showKTV();
    }

    @Override
    public void hideKTV() {
        chairUI.hideKTV();
    }

    @Override
    public void sing() {
        chairUI.sing();
    }

    @Override
    public void stopSing() {
        chairUI.stopSing();
    }

    //轮到说话,展示进度条
    @Override
    public void turns(int duration) {
        if (user == null) {
            //服务器已经做了处理，这里再判断一下，防止意外
            return;
        }
        if (!user.isOut) {
            //离线中也显示倒计时
            chairUI.turns(duration);
        }
    }

    @Override
    public void out() {
        chairUI.out();
        if (user == null) {
            return;
        }
        user.isOut = true;
        if (PositionHelper.isSelf(user.id)) {
            PersonalDataHelper.getPersonalData().setOut(true);
        }
    }

    @Override
    public void reConnected() {
        chairUI.hideUserStatus();
        restoreStatus();
        if (user != null) {
            user.is_disconnected = false;
            if (PositionHelper.isSelf(user.id)) {
                PersonalDataHelper.getPersonalData().setOffline(false);
            }
        }
    }

    private void restoreStatus() {
        if (user != null) {
            if (VoiceRoomConfig.isSpyRoom()) {
                if (!VoiceRoomConfig.isIsPlaying()) {
                    if (PositionHelper.isMaster(mPosition)) {
                        //房主位置
                        setMasterStatus();
                    } else {
                        //一般玩家位置
                        if (user.prepared) {
                            handlePrepare();
                        } else {
                            tipPrepare();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void reset() {
        chairUI.ivHead.setImageResource(R.mipmap.ico_voice_add);
        chairUI.ivHead.clearHeadDecorate();
        chairUI.tvName.setText("");
        stopAnimation();
        chairUI.openMic();
        chairUI.closeFreeStyle();
        chairUI.clearTipPrepare();
        chairUI.clearStatus();

        isMaster = false;
        isLocked = false;
        isSated = false;
        isBanMic = false;
        isFreeStyle = false;
        canDownSeat = false;
        user = null;
    }

    @Override
    public void onResume() {
        if (user != null && user.position == mPosition && TextUtils.equals(user.state, VoiceConstants.FREE) && user.speaking) {
            startAnimation();
        }
    }

    private void closeFree() {
        if (!isLocked) {
            chairUI.closeFreeStyle();
            stopAnimation();
            this.isFreeStyle = false;
        }
    }

    @Override
    public void onPause() {
        unSpeak();
        openMic();
        closeFree();
        stopAnimation();
        endEmotion();
        stopSing();
    }

    @Override
    public PointF getPositionOnScreen() {
        int[] location = new int[2];
        chairUI.userContainer.getLocationOnScreen(location);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            location[1] -= SystemScreenUtils.getStatusBarHeight();
        }
        PointF pointF = new PointF(location[0], location[1]);
        return pointF;
    }

    @Override
    public PointF getEmotionHeadPositionOnScreen() {
        int[] location = new int[2];
        chairUI.ivEmotion.getLocationOnScreen(location);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            location[1] -= SystemScreenUtils.getStatusBarHeight();
        }
        PointF pointF = new PointF(location[0], location[1]);
        return pointF;
    }


    @Override
    public int getHeadWidth() {
        return chairUI.userContainer.getWidth();
    }

    @Override
    public int getHeadHeight() {
        return chairUI.userContainer.getHeight();
    }

    @Override
    public int getEmotionHeadWidth() {
        return chairUI.ivEmotion.getWidth();
    }

    @Override
    public int getEmotionHeadHeight() {
        return chairUI.ivEmotion.getHeight();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        super.destroy();
        if (voiceSeatDialog != null) {
            if (voiceSeatDialog.isShowing()) {
                voiceSeatDialog.dismiss();
            }
            voiceSeatDialog.destroy();
            voiceSeatDialog = null;
        }
        if (emotionQueue != null) {
            emotionQueue.clear();
        }
        if (emotionHandler != null) {
            emotionHandler.release();
        }
        if (smallGameQueue != null) {
            smallGameQueue.clear();
        }
        if (gameHandler != null) {
            gameHandler.release();
        }
        stopAnimation();
        chairUI.destroy();
    }

    //判断是否连接
    private boolean judgeSocketConnect() {
        boolean isConnected = VoiceRoomGameSocketManager.getInstance().isConnected();
        if (!isConnected) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.socket_disconnect));
        }
        return isConnected;
    }

    //判断是否能进行麦位操作
    private boolean isCanOpSeat() {
        if (VoiceRoomConfig.isSpyRoom() && VoiceRoomConfig.isIsPlaying()) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_playing_can_not_op));
            return false;
        }

        return true;
    }

    /**
     * 能否上麦
     */
    private boolean canUpSeat() {

        //大厅不能上麦，因为只有一个位置
        if (VoiceRoomConfig.isLobby()) {
            return false;
        }
        //ktv房间，正在唱歌的，不能上其他麦
        if (VoiceRoomConfig.isIsKTV() && PositionHelper.isSinger()) {
            ToastToolKit.showShort(R.string.str_music_singer_can_no_jump_seat);
            return false;
        }

        return true;
    }

    private void showTips() {
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_has_been_locked));
    }

    private void showLocked() {
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_lock_no_seat));
    }

    private void showSeated() {
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_seated_no_seat));
    }

    private void showNoSeated() {
        ToastToolKit.showShort(MessageUtils.getString(R.string.str_has_no_user));
    }

    public VoiceChairUI getChairUI() {
        return chairUI;
    }
}
