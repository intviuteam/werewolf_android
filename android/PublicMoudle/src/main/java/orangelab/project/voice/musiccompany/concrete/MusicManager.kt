package orangelab.project.voice.musiccompany.concrete

import android.graphics.PointF
import android.text.TextUtils
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.common.utils.SafeHandler
import orangelab.project.voice.config.VoiceRoomConfig
import orangelab.project.voice.musiccompany.api.MusicApiConstants
import orangelab.project.voice.musiccompany.api.MusicCompanyApi
import orangelab.project.voice.musiccompany.component.MusicStockDisplayer
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig
import orangelab.project.voice.musiccompany.event.MusicEvent
import org.greenrobot.eventbus.EventBus
import java.net.URLEncoder

/**
 * 音乐管理器，对场景、数据统一管理
 */
class MusicManager private constructor() : Destroyable {

    //懒汉式单例======================================================================================//
    private object SingleInstance {
        val INSTANCE = MusicManager()
    }

    companion object {
        const val TAG = "MusicManager"
        val instance: MusicManager by lazy { SingleInstance.INSTANCE }
        //val instance: MusicManager = SingleInstance.INSTANCE
    }
    //===============================================================================================//

    private var safeHandler = SafeHandler()
    private var isPrepared = false //是否经过了准备阶段

    /**
     * 曲库展示器，处理下载音乐的UI
     */
    private lateinit var musicStockDisplayer: MusicStockDisplayer

    /**
     * 播放信息展示器
     */
    private lateinit var musicPlayerDisplayer: MusicPlayerDisplayer

    fun refreshMusicStockDisplayer(musicStockDisplayer: MusicStockDisplayer) {
        this.musicStockDisplayer = musicStockDisplayer
    }

    fun refreshMusicPLayerDisplayer(musicPlayerDisplayer: MusicPlayerDisplayer) {
        this.musicPlayerDisplayer = musicPlayerDisplayer
        refreshTitle()
    }

    /**
     * 加载推荐音乐
     */
    fun loadCommendMusic() {
        musicStockDisplayer.loadCommendMusic()
    }

    /**
     * 刷新曲库/推荐歌曲
     */
    fun refreshStock(page: Int, size: Int) {
        var url: String = MusicApiConstants.getRecommendList(page, size)
        musicStockDisplayer.showLoading()
        try {
            MusicCompanyApi.getRecommendSong(url,
                    { result, _ ->
                        PLog.e(TAG, "getRecommendSong: $result")
                        safeHandler.postSafely {
                            PLog.e(TAG, "getRecommendSong, result is $result")
                            if (result != null) {
                                if (result.docs.isEmpty()) {
                                    ToastToolKit.showShort(R.string.recycler_swipe_more_not)
                                }
                                MusicCompanyDataSource.refreshStock(result.docs)

                            }
                            musicStockDisplayer.refreshStockByCommend()
                            musicStockDisplayer.dismissLoading()
                        }
                        PLog.e(TAG, "getRecommendSong after : $result")
                    })
        } catch (e: Exception) {
            PLog.e(TAG, "error occur when refreshStock, error is ${e.message}")
            musicStockDisplayer.dismissLoading()
        }
    }

    /**
     * 刷新曲库
     */
    fun refreshStock(keyword: String, page: Int, size: Int) {
        var key = keyword
        try {
            key = URLEncoder.encode(keyword, "UTF-8")
        } catch (e: Exception) {

        }
        var url: String = MusicApiConstants.getSongStockUrl(key, page, size)
        musicStockDisplayer.showLoading()
        try {
            MusicCompanyApi.getSongStockByStep(url,
                    { result, _ ->
                        safeHandler.postSafely {
                            if (result != null) {
                                if (result.docs.isEmpty()) {
                                    ToastToolKit.showShort(R.string.recycler_swipe_more_not)
                                }
                                MusicCompanyDataSource.refreshStock(result.docs)
                            }
                            musicStockDisplayer.refreshStock()
                            musicStockDisplayer.dismissLoading()
                        }
                    })
        } catch (e: Exception) {
            PLog.e(TAG, "error occur when refreshStock, error is ${e.message}")
            musicStockDisplayer.dismissLoading()
        }
    }

    /**
     * 刷新已点
     */
    fun refreshOrdered() {
        MusicCompanyDataSource.refreshOrdered()
    }

    /**
     * 刷新状态
     */
    fun refreshMusicState(musicData: MusicDataEntity, position: Int) {

        //刷新数据文件
        MusicCompanyDataSource.refresh(musicData, position)
        //刷新UI状态
        musicStockDisplayer.refresh(musicData, position)
    }

    /**
     * 点歌
     */
    fun orderSong(musicData: MusicDataEntity, position: Int) {
        MusicDownLoader.download(musicData, position)
    }

    /**
     * 加载歌曲信息
     */
    fun prepareSong(lyricUrl: String) {
        isPrepared = true
        PLog.e(TAG, "prepareSong: $lyricUrl")
        val musicData = MusicDataEntity()
        try {
            if (!TextUtils.isEmpty(lyricUrl)) {
                //歌词不为空就请求歌词
                MusicCompanyApi.getLyric(lyricUrl) { result, _ ->
                    safeHandler.postSafely {
                        musicData.lyrics = result
                        musicPlayerDisplayer.prepare(musicData)
                    }
                }
            } else {
                //歌词为空，直接开始
                safeHandler.postSafely {
                    musicPlayerDisplayer.prepare(musicData)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 加载歌曲信息, 添加回调，适用于enter和restore不需要准备就直接播放
     */
    fun prepareSong(lyricUrl: String, runnable: Runnable) {
        isPrepared = true
        PLog.e(TAG, "prepareSong: $lyricUrl")
        val musicData = MusicDataEntity()
        try {
            if (!TextUtils.isEmpty(lyricUrl)) {
                //歌词不为空就请求歌词
                MusicCompanyApi.getLyric(lyricUrl) { result, _ ->
                    safeHandler.postSafely {
                        musicData.lyrics = result
                        musicPlayerDisplayer.prepare(musicData)
                        runnable.run()
                    }
                }
            } else {
                //歌词为空，直接开始
                safeHandler.postSafely {
                    musicPlayerDisplayer.prepare(musicData)
                    runnable.run()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 开始播放
     */
    fun playSong() {
        PLog.e(TAG, "start:")
        try {
            if (isPrepared) {
                //如果已经准备，直接开始
                safeHandler.postSafely {
                    musicPlayerDisplayer.start()
                }
            } else {
                //否则准备后才开始
                prepareSong(MusicCompanyConfig.lyricUrl, Runnable {
                    safeHandler.postSafely {
                        musicPlayerDisplayer.start()
                    }
                })
            }
        } finally {
            isPrepared = false
        }
    }

    /**
     * 暂停
     */
    fun pause() {
        PLog.e(TAG, "pause:")
        musicPlayerDisplayer.pause()
    }

    /**
     * 复原
     */
    fun resume() {
        PLog.e(TAG, "resume:")
        musicPlayerDisplayer.resume()
    }

    /**
     * 停止
     */
    fun stop() {
        isPrepared = false
        PLog.e(TAG, "stop:")
        musicPlayerDisplayer.stop()
    }

    /**
     * 刷新播放器标题
     */
    fun refreshTitle() {
        musicPlayerDisplayer.refreshTitle(MusicCompanyConfig.getTitle())
    }

    /**
     * 获取唱歌在播放器上的头像
     */
    fun getAlbumPosition(): PointF {
        return musicPlayerDisplayer.getPosition()
    }

    /**
     *  获取播放器上头像的大小
     */
    fun getAlbumWidth(): Int {
        return musicPlayerDisplayer.getAlbumWidth()
    }

    /**
     * 展示下一首按钮
     */
    fun showNextButton() {
        musicPlayerDisplayer.showNextSong()
    }

    /**
     * 展示关闭按钮
     */
    fun showBoot() {
        musicPlayerDisplayer.showBoot()
    }

    /**
     * 授权就重置prepare状态
     */
    fun resetPrepare() {
        isPrepared = false
    }

    override fun destroy() {
        PLog.e(TAG, "called destroy in MusicManager")
        safeHandler.removeCallbacksAndMessages(null)
        isPrepared = false
        try {
            MusicCompanyDataSource.destroy()
            stop()
            musicPlayerDisplayer.destroy()
            musicStockDisplayer.destroy()
        } catch (e: Exception) {
            PLog.e(TAG, "error occur when destroy music manager, error is ${e.message}")
            e.printStackTrace()
        }
    }

}