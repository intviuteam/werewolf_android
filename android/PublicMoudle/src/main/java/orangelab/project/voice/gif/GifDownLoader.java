package orangelab.project.voice.gif;


import android.os.AsyncTask;

import orangelab.project.voice.ds.Queue;
import com.androidtoolkit.HttpToolkit;
import com.androidtoolkit.PLog;

/**
 * Created by lloydfinch on 07/11/2017.
 */

public class GifDownLoader {

    private static final String TAG = "GifDownLoader";

    private static Queue<GifTask> tasks = new Queue<>();
    private static boolean isDownLoading = false;

    public static void downLoad(String url, CallBack callBack) {
        tasks.enQueue(new GifTask(url, callBack));
        notifyNext();
    }

    private static void notifyNext() {
        if (!isDownLoading) {
            GifTask task = tasks.poll();
            if (task != null) {
                //退出房间依然更新
                new GifAsyncTask().execute(task);
                isDownLoading = true;
            }
        }
    }

    private static class GifAsyncTask extends AsyncTask<GifTask, Integer, byte[]> {

        private CallBack callBack;

        @Override
        protected byte[] doInBackground(GifTask... params) {

            GifTask task = params[0];
            if (task != null) {
                PLog.e(TAG, "data: " + task.url);
                this.callBack = task.callBack;
                return HttpToolkit.doGet(task.url);
            }

            return null;
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            if (this.callBack != null) {
                callBack.onSuccess(bytes);
            }
            isDownLoading = false;
            notifyNext();
        }
    }

    private static class GifTask {
        String url;
        CallBack callBack;

        public GifTask(String url, CallBack callBack) {
            this.url = url;
            this.callBack = callBack;
        }
    }

    public interface CallBack {
        void onSuccess(byte[] data);

        void onFailure();
    }
}
