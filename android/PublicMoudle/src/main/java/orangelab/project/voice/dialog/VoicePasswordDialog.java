package orangelab.project.voice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.engine.RoomSocketEngineHelper;

/**
 * Created by lloydfinch on 14/10/2017.
 */

public class VoicePasswordDialog extends Dialog {

    private static final String TAG = "RoomPasswordDialog";

    private TextView tvSwitch;
    private Switch passwordSwitch;
    private EditText editText;
    private View passwordContainer;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TextView text4;
    private TextView tvPswTip;

    private View btnSure;
    private View btnCancel;

    private Context mContext;
    private String password;

    public VoicePasswordDialog(Context context, String password) {
        super(context, R.style.radius_dialog);
        mContext = context;
        this.password = password;
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_password, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        editText = (EditText) contentView.findViewById(R.id.editText);
        passwordContainer = contentView.findViewById(R.id.password_container);
        passwordSwitch = (Switch) contentView.findViewById(R.id.password_switch);
        text1 = (TextView) contentView.findViewById(R.id.text1);
        text2 = (TextView) contentView.findViewById(R.id.text2);
        text3 = (TextView) contentView.findViewById(R.id.text3);
        text4 = (TextView) contentView.findViewById(R.id.text4);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
        tvSwitch = (TextView) contentView.findViewById(R.id.tv_switch);
        tvPswTip = (TextView) contentView.findViewById(R.id.tv_psw_tip);

        initEditText(editText);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendChangePassword(editText.getText().toString());
            hideKeyboard();
            dismiss();
        });

        btnCancel.setOnClickListener(v -> dismiss());

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null) {
                    return;
                }
                String input = s.toString();
                Editable editable = editText.getText();
                int len = input.length();
                if (len > 4) {
                    int selEndIndex = Selection.getSelectionEnd(editable);
                    String str = editable.toString();
                    input = str.substring(0, 4);
                    editText.setText(input);
                    editable = editText.getText();
                    int newLen = editable.length();
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length();
                    }
                    Selection.setSelection(editable, selEndIndex);

                }
                handleInput(input);
                PLog.i(TAG, "onTextChanged: " + input.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                PLog.i(TAG, "afterTextChanged: ");
            }
        });
        passwordSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                openSwitch();
            } else {
                closeSwitch();
            }
            handleInput("");
            editText.setText("");
        });
        passwordContainer.setOnClickListener(v -> {
            editText.requestFocus();
            showKeyboard();
        });
        if (TextUtils.isEmpty(password)) {
            closeSwitch();
            editText.setText("");
            handleInput("");
        } else {
            openSwitch();
            handleInput(password);
            editText.setText(password);
        }
        editText.requestFocus();
    }

    private void openSwitch() {
        passwordSwitch.setChecked(true);
        //passwordSwitch.setBackgroundResource(R.mipmap.ic_voice_switch_on);
        tvSwitch.setText(R.string.str_open_password);
        passwordContainer.setVisibility(View.VISIBLE);
        tvPswTip.setVisibility(View.VISIBLE);
    }

    private void closeSwitch() {
        passwordSwitch.setChecked(false);
        //passwordSwitch.setBackgroundResource(R.mipmap.ic_voice_switch_off);
        tvSwitch.setText(R.string.str_close_psw);
        passwordContainer.setVisibility(View.GONE);
        tvPswTip.setVisibility(View.INVISIBLE);
    }

    private void clearInput() {
        text1.setText("");
        text2.setText("");
        text3.setText("");
        text4.setText("");
    }

    private void handleInputInner(String input) {
        try {
            text1.setText(input.substring(0, 1));
            text2.setText(input.substring(1, 2));
            text3.setText(input.substring(2, 3));
            text4.setText(input.substring(3, 4));
        } catch (Exception e) {
            //password length < 4
            e.printStackTrace();
        }
    }

    private void handleInput(String input) {
        clearInput();
        if (TextUtils.isEmpty(input)) {
        } else {
            if (input.length() > 4) {
                String result = input.substring(0, 4);
                handleInputInner(result);
            } else {
                handleInputInner(input);
            }

        }
    }

    private void initEditText(EditText editText) {
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

        });
        editText.setLongClickable(false);
    }

    private void showKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e) {
            PLog.e(TAG, "hide keyboard without not open it!");
            e.printStackTrace();
        }
    }
}
