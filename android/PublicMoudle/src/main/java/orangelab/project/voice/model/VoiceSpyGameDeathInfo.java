package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lloydfinch on 11/12/2017.
 */

public class VoiceSpyGameDeathInfo implements Serializable, Keepable {

    public List<DeathInfo> death_info;
    public int duration;

    @Override
    public String toString() {
        return "VoiceSpyGameDeathInfo{" +
                "death_info=" + death_info +
                ", duration=" + duration +
                '}';
    }

    public static class DeathInfo implements Serializable {
        public int killed;
        public String reason;
        public String role;

        @Override
        public String toString() {
            return "DeathInfo{" +
                    "killed=" + killed +
                    ", reason='" + reason + '\'' +
                    ", role='" + role + '\'' +
                    '}';
        }
    }
}
