package orangelab.project.voice.privateroom

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.ToastToolKit
import com.R
import orangelab.project.common.activity.PersonalInfoActivity
import orangelab.project.common.dialog.ReportDialog
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.manager.ApiManager
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData
import orangelab.project.voice.privateroom.model.PrivateRoomUserActionItem
import com.toolkit.action.ActionDismiss
import com.toolkit.action.ApiResult
import com.toolkit.action.Destroyable
import orangelab.project.common.view.HeadDecorView
import orangelab.project.common.view.VipNameView

/**
 *game_werewolf
 *2018/3/22 下午1:35
 *Mystery
 */
class PrivateRoomUserDialog : SafeDialog, View.OnClickListener, ActionDismiss {

    class Config {

        var onlyActions = false
        var needKickOut = false

        constructor(onlyActions: Boolean) {
            this.onlyActions = onlyActions
            needKickOut = false
        }

        constructor(onlyActions: Boolean, needKickOut: Boolean) {
            this.onlyActions = onlyActions
            this.needKickOut = needKickOut
        }
    }


    private var mAddFriend: View
    private var mReport: View
    private var mBlock: View

    private var mUserContainer: View
    private var mUserHead: HeadDecorView
    private var mUserName: VipNameView
    private var mUserPopular: TextView

    private var mUserActionsView: GridView
    private var mUserActionsAdapter: Adapter

    private var mKickOut: View
    private var mConfig: Config

    private var mPrivateRoomMember: PrivateRoomMemberData? = null

    constructor(context: Context, privateContext: PrivateRoomContext, privateRoomMember: PrivateRoomMemberData?, config: Config, actions: MutableList<PrivateRoomUserActionItem>) : super(context, R.style.radius_dialog) {

        setContentView(R.layout.layout_private_room_user_detail_dialog)

        mConfig = config
        mPrivateRoomMember = privateRoomMember

        mAddFriend = findViewById(R.id.id_add)
        mAddFriend.setOnClickListener(this)
        mReport = findViewById(R.id.id_report)
        mReport.setOnClickListener(this)
        mBlock = findViewById(R.id.id_block)
        mBlock.setOnClickListener(this)

        mUserContainer = findViewById(R.id.id_user_container)
        mUserContainer.setOnClickListener(this)

        mUserHead = findViewById<HeadDecorView>(R.id.id_user_head) as HeadDecorView

        mUserName = findViewById<VipNameView>(R.id.id_user_name) as VipNameView


        mUserPopular = findViewById<TextView>(R.id.id_user_popular) as TextView

        mUserActionsView = findViewById<GridView>(R.id.id_recycleview) as GridView
        mUserActionsAdapter = Adapter(context, actions)
        mUserActionsView.adapter = mUserActionsAdapter
        mUserActionsView.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val tempAction = mUserActionsAdapter.getItem(position)
                tempAction?.actionRunnable?.func(this@PrivateRoomUserDialog)
            }
        })

        mKickOut = findViewById(R.id.id_kickout)
        mKickOut.setOnClickListener(this)
        mKickOut.visibility = View.GONE
        if (mConfig.needKickOut) {
            mKickOut.visibility = View.VISIBLE
        } else {
            mKickOut.visibility = View.GONE
        }

        if (mConfig.onlyActions) {
            mAddFriend.visibility = View.GONE
            mReport.visibility = View.GONE
            mBlock.visibility = View.GONE

            mUserContainer.visibility = View.GONE
            mKickOut.visibility = View.GONE

        } else {
            if (privateRoomMember == null)
                return
            mAddFriend.visibility = View.INVISIBLE
            mReport.visibility = View.INVISIBLE
            mBlock.visibility = View.INVISIBLE
            mUserContainer.visibility = View.VISIBLE

            PicassoUtils.loadImage(context, privateRoomMember!!.getMemberIcon(), mUserHead.realHeadImage, R.mipmap.default_head)
            mUserHead.setHeadDecorate(privateRoomMember!!.memberHeadDecor())
            mUserName.setText(privateRoomMember!!.getMemberName())
            mUserName.handleVipInfo(privateRoomMember!!.memberVipInfo(), Color.BLACK)

            if (privateRoomMember!!.memberIsSelf()) {

                mAddFriend.visibility = View.INVISIBLE
                mReport.visibility = View.INVISIBLE
                mBlock.visibility = View.INVISIBLE
                mUserContainer.visibility = View.VISIBLE

            } else {
                if (Utils.checkUserIdIsTourist(privateRoomMember.getMemberId())) {
                    /**
                     * 游客不能被举报与拉黑
                     */
                    mAddFriend.visibility = View.INVISIBLE
                    mReport.visibility = View.INVISIBLE
                    mBlock.visibility = View.INVISIBLE
                    return
                } else {
                    mAddFriend.visibility = View.GONE
                    mReport.visibility = View.VISIBLE
                }
            }

            ApiManager.GetUserInfoApi(privateRoomMember.getMemberId(), ApiResult { result, e ->
                runSafely(Runnable {
                    if (e == null) {

                        mUserPopular.setText(MessageUtils.getString(R.string.str_private_room_popular, result.popular))

                        if (privateRoomMember.memberIsSelf()) {

                            return@Runnable
                        } else {

                            if (result.is_friend) {
                                mAddFriend.visibility = View.GONE
                            } else {
                                mAddFriend.visibility = View.VISIBLE
                            }

                            if (result.isBlock) {
                                mBlock.visibility = View.GONE
                            } else {
                                mBlock.visibility = View.VISIBLE
                            }

                        }

                        mUserHead.setHeadDecorate(result.avatar_box)

                    } else {
                        ToastToolKit.showShort(e.message)
                    }
                })
            })
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.id_add -> {
                ApiManager.AddFriendApi(mPrivateRoomMember!!.getMemberId(), null, null, null)
            }
            R.id.id_report -> {
                val reportDialog = ReportDialog(context, mPrivateRoomMember!!.getMemberId(), mPrivateRoomMember!!.getMemberName())
                reportDialog.show()
            }
            R.id.id_user_container -> {
                PersonalInfoActivity.Launch(context, mPrivateRoomMember!!.getMemberId())
                dismiss()
            }
            R.id.id_kickout -> {
                RoomSocketEngineHelper.kickOut(mPrivateRoomMember!!.getMemberPosition(), mPrivateRoomMember!!.getMemberId())
                dismiss()
            }
            R.id.id_block -> {
                ApiManager.AddToBlackApi(mPrivateRoomMember?.getMemberId(), { result, e ->
                    runSafely({
                        if (e == null) {
                            ToastToolKit.showShort(result)
                            dismiss()
                        } else {
                            ToastToolKit.showShort(e.message)
                        }
                    })
                })
            }
        }
    }

    override fun release() {

    }

    class Adapter : BaseAdapter, Destroyable {
        private var context: Context
        private var actions: MutableList<PrivateRoomUserActionItem>

        constructor(context: Context, actions: MutableList<PrivateRoomUserActionItem>) : super() {
            this.context = context
            this.actions = actions
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var tempView: View? = convertView
            var holder: Holder? = null
            if (tempView == null) {
                tempView = View.inflate(context, R.layout.layout_private_room_user_detail_action, null)
                holder = Holder()
                holder.root = tempView!!
                holder.text = tempView!!.findViewById<TextView>(R.id.text) as TextView
                holder.icon = tempView!!.findViewById<ImageView>(R.id.icon) as ImageView
                tempView!!.tag = holder
            } else {
                holder = tempView!!.tag as Holder
            }
            val tempItem = getItem(position)
            holder?.icon?.setImageResource(tempItem.actionResourceId)
            holder?.text?.setText(tempItem.actionText)
            return tempView
        }

        override fun getItem(position: Int): PrivateRoomUserActionItem {
            return actions.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return actions.size
        }

        override fun destroy() {
            actions.clear()
        }

    }

    class Holder {
        var root: View? = null
        var icon: ImageView? = null
        var text: TextView? = null
    }
}