package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2018/4/12 下午2:08
 * Mystery
 */

public class VoiceRoomConfigAPI implements Keepable {

    public VoiceRoomConfigDefault default_config;

    public static class VoiceRoomConfigDefault implements Keepable {
        public ArrayList<PrivateRoomTagItem> tags;
    }

    public static class PrivateRoomTagItem implements Keepable {
        public String text;
        public String color;
    }
}
