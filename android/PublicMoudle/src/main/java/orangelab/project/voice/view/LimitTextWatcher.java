package orangelab.project.voice.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by lloydfinch on 16/12/2017.
 */

public class LimitTextWatcher implements TextWatcher {

    private int mMaxLength = 14;//能输入的最大长度，中文按照两个字符，英文按照一个字符
    private int mCurrentLength = 0;

    /**
     * et
     */
    private EditText et = null;
    /**
     * 筛选条件
     */
    private String regex;
    /**
     * 默认的筛选条件(正则:只能输入中文)
     */
    private String DEFAULT_REGEX = "[^\u4E00-\u9FA5]";

    /**
     * 构造方法
     *
     * @param et
     */
    public LimitTextWatcher(EditText et) {
        this.et = et;
        this.regex = DEFAULT_REGEX;
    }

    /**
     * 构造方法
     *
     * @param et    et
     * @param regex 筛选条件
     */
    public LimitTextWatcher(EditText et, String regex) {
        this.et = et;
        this.regex = regex;

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (et == null) {
            return;
        }
        if (count != 0) {//增加了新内容，注意count和before的取值范围为>=1

            for (int index = start; index < start + count; index++) {

                if ((char) (byte) s.charAt(index) != s.charAt(index)) {//判断输入的是不是中文
                    mCurrentLength += 2;
                } else {
                    mCurrentLength += 1;
                }

                if (mCurrentLength > mMaxLength) {//输入的字符长度大于最大长度
                    et.removeTextChangedListener(this);

                    String leftString = s.subSequence(0, index).toString();//获取最大合法长度的内容
                    et.setText(leftString);
                    et.setSelection(index);

                    et.addTextChangedListener(this);

                    if ((char) (byte) s.charAt(index) != s.charAt(index)) {
                        mCurrentLength -= 2;
                    } else {
                        mCurrentLength -= 1;
                    }
                    return;
                }//end mCurrentLength > mMaxLength
            }// end for
        }//end count != 0

        mCurrentLength = 0;
        //重新计算当前的总长度
        char[] chars = s.toString().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if ((char) (byte) chars[i] != chars[i]) {
                mCurrentLength += 2;
            } else {
                mCurrentLength += 1;
            }
        }
    }


    @Override
    public void afterTextChanged(Editable editable) {
        String str = editable.toString();
        String inputStr = clearLimitStr(regex, str);
        et.removeTextChangedListener(this);
        // et.setText方法可能会引起键盘变化,所以用editable.replace来显示内容
        editable.replace(0, editable.length(), inputStr.trim());
        et.addTextChangedListener(this);

    }

    /**
     * 清除不符合条件的内容
     *
     * @param regex
     * @return
     */
    private String clearLimitStr(String regex, String str) {
        return str.replaceAll(regex, "");
    }
}
