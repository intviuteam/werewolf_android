package orangelab.project.voice.musiccompany.component

import orangelab.project.common.db.entity.MusicDataEntity

abstract class MusicStateSwitcher {

    /**
     * 切歌
     */
    open fun switchMusic(musicData: MusicDataEntity): MusicStateSwitcher {
        return this
    }

    /**
     * 切换到"点歌"
     */
    open fun switchToOrder() {}

    /**
     * 切换到"下载中"
     */
    open fun switchToDownLoading() {}

    /**
     * 切换到"下载暂停"
     */
    open fun switchToDownPause() {}

    /**
     * 切换到"下载完成"
     */
    open fun switchToDownLoaded() {}

    /**
     * 切换到"下载失败"
     */
    open fun switchToDownFailed() {}

    /**
     * 切换下载进度
     */
    open fun switchProgress(progress: Int) {}

    /**
     * 切换到"未点击"(等价于下载完成，目前没什么卵用)
     */
    open fun switchToNon() {}

    /**
     * 切换到"播放中"
     */
    open fun switchToPlaying() {}

    /**
     * 切换到"播放暂停"
     */
    open fun switchToPlayPause() {}

    /**
     * 切换到"播放停止"
     */
    open fun switchToPlayStop() {}

    /**
     * 切换到"循环播放"
     */
    open fun switchToPlayLoop() {}

    /**
     * 根据下载的文件刷新状态
     */
    open fun refresh(musicData: MusicDataEntity, position: Int) {

    }
}