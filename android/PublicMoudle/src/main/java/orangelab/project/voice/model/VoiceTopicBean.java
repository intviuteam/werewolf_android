package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 07/12/2017.
 */

public class VoiceTopicBean implements Serializable, Keepable {
    public String title;
    public String type;
    public String url;//自定义背景传入的值
    public Theme theme;

    public static class Theme {
        public int version;
        public String background;
    }
}
