package orangelab.project.voice.musiccompany.tecent.music;


import android.content.Intent;

import com.tencent.TMG.ITMGContext;
import com.toolkit.action.Keepable;

/**
 * Created by baixs on 2017/11/20.
 */

public interface TMGDispatcherBase extends Keepable {
    void OnEvent(ITMGContext.ITMG_MAIN_EVENT_TYPE type, Intent data);
}
