package orangelab.project.voice.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.R;
import com.androidtoolkit.PicassoUtils;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.MainApplication;
import orangelab.project.common.activity.PictureBrowseActivity;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.utils.IntentDataHelper;

public class VoiceSeatPhotoAdapter extends RecyclerView.Adapter<VoiceSeatPhotoAdapter.PhotoViewHolder> {

    private Context context;
    private List<UserInfoResult.PhotoItem> photoItems = new ArrayList<>();

    public VoiceSeatPhotoAdapter(Context context, List<UserInfoResult.PhotoItem> photoItems) {
        this.context = context;
        this.photoItems = photoItems;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.item_voice_seat_photo, parent, false);
        return new PhotoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        UserInfoResult.PhotoItem photoItem = photoItems.get(position);
        if (photoItem != null) {
            PicassoUtils.loadImageByDefault(MainApplication.getInstance(),
                    photoItem.thumbnail_url, holder.ivPhoto, R.mipmap.ic_me_photo_default);

            holder.ivPhoto.setOnClickListener((view) -> {
                Intent intent = new Intent(context, PictureBrowseActivity.class);
                IntentDataHelper.setImageList(intent, getPhotoUrls());
                IntentDataHelper.setImageCurPosition(intent, position);
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return photoItems.size();
    }

    private ArrayList<String> getPhotoUrls() {
        ArrayList<String> urls = new ArrayList<>();
        for (UserInfoResult.PhotoItem item : photoItems) {
            if (!TextUtils.isEmpty(item.url)) {
                urls.add(item.url);
            }
        }

        return urls;
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivPhoto;

        public PhotoViewHolder(View itemView) {
            super(itemView);

            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_photo_thumb);
        }
    }

}
