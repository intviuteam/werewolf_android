package orangelab.project.voice.dialog

import android.content.Context
import android.text.Editable
import android.text.Selection
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.TextView
import com.R
import com.androidtoolkit.view.ScreenUtils
import com.toolkit.action.Action
import com.toolkit.action.ActionDismiss
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.utils.MessageUtils

/**
 *game_werewolf
 *2018/4/11 上午10:49
 *Mystery
 */
class RoomPasswordDialog : SafeDialog {

    private var mPassword: String

    private var editText: EditText
    private var passwordContainer: View
    private var text1: TextView
    private var text2: TextView
    private var text3: TextView
    private var text4: TextView
    private var tvPswTip: TextView

    private var btnSure: View
    private var btnCancel: View

    private val mContext: Context
    private var onConfirm: Action<String>?
    private var onCancel: Action<String>? = null

    constructor(titleString: String, context: Context, mPassword: String, onConfirm: Action<String>?, onCancel: Action<String>?) : super(context, R.style.DarkDialog) {
        this.mContext = context
        this.mPassword = mPassword
        this.onConfirm = onConfirm
        this.onCancel = onCancel
        val contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_private_room_password, null)
        contentView.isFocusable = true
        contentView.isFocusableInTouchMode = true
        setContentView(contentView)

        val title = contentView.findViewById(R.id.level_limit) as TextView
        title.setText(titleString)
        editText = contentView.findViewById(R.id.editText) as EditText
        passwordContainer = contentView.findViewById(R.id.password_container)
        text1 = contentView.findViewById(R.id.text1) as TextView
        text2 = contentView.findViewById(R.id.text2) as TextView
        text3 = contentView.findViewById(R.id.text3) as TextView
        text4 = contentView.findViewById(R.id.text4) as TextView
        btnSure = contentView.findViewById(R.id.btn_sure)
        btnCancel = contentView.findViewById(R.id.btn_cancel)
        tvPswTip = contentView.findViewById(R.id.tv_psw_tip) as TextView

        initEditText(editText)
        initWindow()
        initListener()
        setCancelable(false)
    }

    constructor(context: Context, mPassword: String, onConfirm: Action<String>?) : this(MessageUtils.getString(R.string.str_voice_change_psw), context, mPassword, onConfirm, null)

    private fun initWindow() {
        val dialogMaxWidth = ScreenUtils.dip2px(360f)
        val params = window!!
                .attributes
        val dur = (ScreenUtils.getScreenWidth() * 0.25f).toInt()
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth
        } else {
            params.width = (ScreenUtils.getScreenWidth() * 0.95f).toInt()
        }
        window!!.attributes = params
    }

    private fun initListener() {
        btnSure.setOnClickListener { v ->
            val password = editText.text.toString()
            if (password.isNullOrBlank() || password.length != 4) {
                return@setOnClickListener
            }
            onConfirm?.func(password)
            hideKeyboard(editText)
            dismiss()

        }

        btnCancel.setOnClickListener { v ->
            onCancel?.func("")
            dismiss()
        }

        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s == null) {
                    return
                }
                var input = s.toString()
                var editable = editText.text
                val len = input.length
                if (len > 4) {
                    var selEndIndex = Selection.getSelectionEnd(editable)
                    val str = editable.toString()
                    input = str.substring(0, 4)
                    editText.setText(input)
                    editable = editText.text
                    val newLen = editable.length
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length
                    }
                    Selection.setSelection(editable, selEndIndex)

                }
                handleInput(input)
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        passwordContainer.setOnClickListener { v ->
            editText.requestFocus()
            showKeyboard(editText)
        }
        if (TextUtils.isEmpty(mPassword)) {
            editText.setText("")
            handleInput("")
        } else {
            handleInput(mPassword)
            editText.setText(mPassword)
        }
        editText.requestFocus()
    }

    private fun clearInput() {
        text1.text = ""
        text2.text = ""
        text3.text = ""
        text4.text = ""
    }

    private fun handleInputInner(input: String) {
        try {
            text1.text = input.substring(0, 1)
            text2.text = input.substring(1, 2)
            text3.text = input.substring(2, 3)
            text4.text = input.substring(3, 4)
        } catch (e: Exception) {
            //password length < 4
            e.printStackTrace()
        }

    }

    private fun handleInput(input: String) {
        clearInput()
        if (TextUtils.isEmpty(input)) {
        } else {
            if (input.length > 4) {
                val result = input.substring(0, 4)
                handleInputInner(result)
            } else {
                handleInputInner(input)
            }

        }
    }

    private fun initEditText(editText: EditText) {
        editText.customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(actionMode: ActionMode, menuItem: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode) {}

        }
        editText.isLongClickable = false
    }

    override fun release() {
        onConfirm = null
        onCancel = null
    }
}