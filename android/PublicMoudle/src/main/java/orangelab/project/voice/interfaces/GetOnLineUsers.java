package orangelab.project.voice.interfaces;

import orangelab.project.common.model.EnterRoomResult;
import com.toolkit.action.Keepable;

import java.util.List;

/**
 * Created by lloydfinch on 31/01/2018.
 */

//获取在线用户的接口
public interface GetOnLineUsers extends Keepable {
    List<EnterRoomResult.EnterRoomUserItem> getOnLineUsers();
}
