package orangelab.project.voice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.model.VoiceSimpleUserBean;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 10/01/2018.
 */

public class VoiceSpinnerAdapter extends BaseAdapter implements Destroyable {

    private List<VoiceSimpleUserBean> users = new ArrayList<>();
    private Context mContext;

    public VoiceSpinnerAdapter(List<VoiceSimpleUserBean> users, Context mContext) {
        this.mContext = mContext;
        setUsers(users);
    }

    public void setUsers(List<VoiceSimpleUserBean> users) {
        this.users.clear();
        this.users.addAll(users);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView == null ? LayoutInflater.from(mContext).inflate(R.layout.layout_item_spinner_user, null) : convertView;
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.ivHead = (ImageView) view.findViewById(R.id.iv_head);
            holder.tvNameId = (TextView) view.findViewById(R.id.tv_name_id);
            view.setTag(holder);
        }
        VoiceSimpleUserBean user = users.get(position);
        if (user != null) {
            PicassoUtils.loadImage(mContext, user.headUrl, holder.ivHead, R.mipmap.default_head);
            String name = "";
            if (user.userName != null) {
                if (user.userName.length() <= 5) {
                    name = user.userName;
                } else {
                    name = user.userName.substring(0, 4) + "...";
                }
            }
            

            holder.tvNameId.setText(MessageUtils.getString(R.string.str_voice_item_deed, name, user.uid));
        }

        return view;
    }

    @Override
    public void destroy() {
        users.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView ivHead;
        TextView tvNameId;
    }
}
