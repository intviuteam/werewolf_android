package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * "谁是卧底"分配词语结构
 */

public class VoiceAssignedRole implements Serializable, Keepable {

    public String role = "";
    public int duration = 0;

    @Override
    public String toString() {
        return "VoiceAssignedRole{" +
                "role='" + role + '\'' +
                ", duration=" + duration +
                '}';
    }
}
