package orangelab.project.voice.privateroom.model

import orangelab.project.common.effect.msgdecor.MsgDecorManiFest
import orangelab.project.common.model.UserVipInfo

/**
 * game_werewolf
 * 2018/3/21 下午5:55
 * Mystery
 */

open class ChatMessageItem : SystemMessageItem {
    var position = -1
    var name = ""
    var msgDecor: MsgDecorManiFest.MsgDecorManiFestItemResource? = null;
    var vipInfo: UserVipInfo? = null

    constructor(textColorResource: Int, message: String, position: Int, name: String) : super(textColorResource, message) {
        this.position = position
        this.name = name
    }

    constructor(message: String, position: Int, name: String) : super(message) {
        this.position = position
        this.name = name
    }


}
