package orangelab.project.voice.privateroom.view

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.R
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.view.ScreenUtils
import com.toolkit.action.Destroyable
import orangelab.project.common.activity.PersonalInfoActivity
import orangelab.project.common.manager.ApiManager
import orangelab.project.common.union.UnifiedBridgeHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.RunnableFactory
import orangelab.project.common.utils.SafeHandler
import orangelab.project.common.view.UserHeadView
import orangelab.project.game.view.IFriendRequestView
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData

/**
 *game_werewolf
 *2018/4/23 下午2:51
 *Mystery
 */
class PrivateRoomFriendRequestView : Destroyable, IFriendRequestView, View.OnClickListener {


    private var mUserData: PrivateRoomMemberData
    private val mRootView: View
    private val mContext: Context
    private val mUserHead: UserHeadView
    private val mUserName: TextView
    private val mUserLevel: TextView
    private val mUserContainer: View
    private val mClose: View
    private val mApprove: Button
    private val mRefuse: Button

    private val mMemo: MutableSet<String>
    private val mSafeHandler: SafeHandler = SafeHandler()
    private var mDismissListener: Runnable? = null

    constructor(mUserData: PrivateRoomMemberData, mContext: Context, memo: MutableSet<String>) {
        this.mUserData = mUserData
        this.mContext = mContext
        this.mMemo = memo
        val root = View.inflate(mContext, R.layout.layout_dialog_voice_add_friend_view, null)
        mRootView = root
        mUserHead =root.findViewById<UserHeadView>(R.id.user_head_view) as UserHeadView
        mUserName = root.findViewById<TextView>(R.id.voice_user_name) as TextView
        mUserLevel = root.findViewById<TextView>(R.id.voice_user_level) as TextView
        mUserContainer = root.findViewById(R.id.rl_user_detail_container)

        mUserHead.setUserHeadImageUrl(mUserData.getMemberIcon() ?: "")
        mUserName.setText(mUserData.getMemberName())
        mUserLevel.setText("LV ${mUserData.getMemberLevel()}")

        mClose = root.findViewById(R.id.ibtn_close)
        mApprove = root.findViewById<Button>(R.id.btn_sure) as Button
        mRefuse = root.findViewById<Button>(R.id.btn_cancel) as Button

        mClose.setOnClickListener(this)
        mApprove.setOnClickListener(this)
        mRefuse.setOnClickListener(this)
        mUserContainer.setOnClickListener(this)
    }

    override fun destroy() {
        mSafeHandler.release()
    }

    override fun getHeight(): Int {
        return ScreenUtils.dip2px(165f)
    }

    override fun getWidth(): Int {
        return ScreenUtils.dip2px(300f)
    }

    override fun getX(): Int {
        return (ScreenUtils.getCurrentScreenWidth() - getWidth()) / 2
    }

    override fun getY(): Int {
        return (ScreenUtils.getCurrentScreenHeight() - ScreenUtils.dip2px(110f) - getHeight())
    }

    override fun getView(): View {
        return mRootView
    }

    override fun getUserId(): String {
        return mUserData?.getMemberId() ?: ""
    }

    override fun setDismissListener(dismissListener: Runnable?) {
        mDismissListener = dismissListener
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v.id) {
            R.id.ibtn_close -> {
                dismiss()
//                putIntoMemo()
            }
            R.id.btn_sure -> {
                ApiManager.AcceptFriend(mUserData.getMemberId()) { s, e ->
                    mSafeHandler.postSafely {
                        if (e == null) {
                            ToastToolKit.showShort(MessageUtils.getString(R.string.add_friend_success))
                            UnifiedBridgeHelper.SendAddFriendAction(mUserData.getMemberId())
                            dismiss()
                            putIntoMemo()
                        } else {
                            ToastToolKit.showShort(e.message)
                        }
                    }
                }
            }
            R.id.btn_cancel -> {
                ApiManager.RefuseFriend(mUserData.getMemberId(), null)
                mSafeHandler.postSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.refuse_friend_success)))
                dismiss()
                putIntoMemo()
            }
            R.id.rl_user_detail_container -> {
                PersonalInfoActivity.Launch(mContext, mUserData.getMemberId())
            }
        }
    }

    private fun putIntoMemo() {
        mMemo.add(mUserData.getMemberId() ?: "")
    }


    fun dismiss() {
        mSafeHandler.postSafely {
            mDismissListener?.run()
            mRootView.setVisibility(View.GONE)
        }
    }
} 