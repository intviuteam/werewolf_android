package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.voice.activity.VoiceRoomBackGroundActivity;
import orangelab.project.voice.activity.VoiceRoomBlackListActivity;
import orangelab.project.voice.api.VoiceSocketHelper;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.event.VoiceEvent;
import orangelab.project.voice.utils.PositionHelper;

import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lloydfinch on 12/10/2017.
 * Modify by hwx on 24/7/2018
 */

public class VoiceNewSettingDialog extends SafeDialog implements Destroyable {

    private View mChangeTopic;
    private View mSetBlackList;
    private View mSetAdmin;
    private View mSetGuest;
    private View mSetMicPermission;

    private View mChangePassword;//修改房间密码
    private TextView mChangePasswordText;
    private View mChangeBackground;//修改房间背景
    private TextView mChangeBackgroundText;
    private View mBeMaster;//成为房主
    private TextView mBeMasterText;
    private View mChangeDeed;//转让房契
    private TextView mChangeDeedText;
    private View mApplyDeed;//申请房契
    private TextView mApplyDeedText;

    private CheckBox mHiddenChatMessage;
    private CheckBox mHiddenJoinMessage;
    private CheckBox mHiddenGiftMessage;

    private View mBtnCancel;

    private Context mContext;
    private String mTopic;
    private String mPassword;
    private String mTopicType;

    private VoiceChangeTitleDialog mVoiceChangeTitleDialog;
    private VoicePasswordDialog mPasswordDialog;

    public VoiceNewSettingDialog(@NonNull Context context) {
        this(context, R.style.radius_dialog);
    }

    public VoiceNewSettingDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mContext = context;
        init();
    }

    private void init() {
        initView();
        initListener();
        WereWolfDialogDecorate.DecorateDialog(getWindow());
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_voice_new_setting, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
            }
            return false;
        });
        setContentView(contentView);

        mChangeTopic = contentView.findViewById(R.id.voice_room_theme);
        mSetBlackList = contentView.findViewById(R.id.voice_room_blacklist);
        mSetAdmin = contentView.findViewById(R.id.voice_room_admin);
        mSetGuest = contentView.findViewById(R.id.voice_room_guest);
        mSetMicPermission = contentView.findViewById(R.id.voice_room_mic_permission);

        mChangePassword = contentView.findViewById(R.id.voice_room_change_password);
        mChangePasswordText = (TextView) contentView.findViewById(R.id.voice_room_change_password_text);
        mChangeBackground = contentView.findViewById(R.id.voice_room_change_background);
        mChangeBackgroundText = (TextView) contentView.findViewById(R.id.voice_room_change_background_text);
        mBeMaster = contentView.findViewById(R.id.voice_room_be_master);
        mBeMasterText = (TextView) contentView.findViewById(R.id.voice_room_be_master_text);
        mChangeDeed = contentView.findViewById(R.id.voice_room_change_deed);
        mChangeDeedText = (TextView) contentView.findViewById(R.id.voice_room_change_deed_text);
        mApplyDeed = contentView.findViewById(R.id.voice_room_apply_deed);
        mApplyDeedText = (TextView) contentView.findViewById(R.id.voice_room_apply_deed_text);

        mHiddenChatMessage = (CheckBox) contentView.findViewById(R.id.check_box_chat_info);
        mHiddenJoinMessage = (CheckBox) contentView.findViewById(R.id.check_box_join_info);
        mHiddenGiftMessage = (CheckBox) contentView.findViewById(R.id.check_box_gift_info);

        mBtnCancel = contentView.findViewById(R.id.btn_cancel);

        if (PositionHelper.isMaster() || PositionHelper.isAdmin()) {
            mBeMaster.setVisibility(View.GONE);
            mBeMasterText.setVisibility(View.GONE);
            mChangeTopic.setVisibility(View.VISIBLE);
            mChangePassword.setVisibility(View.VISIBLE);
            mSetBlackList.setVisibility(View.VISIBLE);
            if (VoiceRoomConfig.isIsSupportChangeBg()) {
                mChangeBackground.setVisibility(View.VISIBLE);
            } else {
                mChangeBackground.setVisibility(View.GONE);
            }
        } else {
            mChangeTopic.setVisibility(View.GONE);
            mChangePassword.setVisibility(View.GONE);
            mSetBlackList.setVisibility(View.GONE);
            mChangeBackground.setVisibility(View.GONE);
            if (VoiceRoomConfig.isIsSupportDeed()) {
                if (VoiceRoomConfig.isIsOwnerRoom() && VoiceRoomConfig.isIsPurchased()) {
                    mBeMaster.setVisibility(View.VISIBLE);
                    mBeMasterText.setVisibility(View.VISIBLE);
                } else {
                    mBeMaster.setVisibility(View.GONE);
                    mBeMasterText.setVisibility(View.GONE);
                }
            } else {
                mBeMaster.setVisibility(View.GONE);
            }
        }

        if (VoiceRoomConfig.isIsSupportDeed()) {
            if (VoiceRoomConfig.isIsOwnerRoom()) {
                if (VoiceRoomConfig.isIsPurchased()) {
                    mApplyDeed.setVisibility(View.GONE);
                    mApplyDeedText.setVisibility(View.GONE);
                    mChangeDeed.setVisibility(View.VISIBLE);
                    mChangeDeedText.setVisibility(View.VISIBLE);
                } else {
                    mApplyDeed.setVisibility(View.VISIBLE);
                    mApplyDeedText.setVisibility(View.VISIBLE);
                    mChangeDeed.setVisibility(View.GONE);
                    mChangeDeedText.setVisibility(View.GONE);
                }
            } else {
                //owner_id不是自己，不可购买也不可转让
                mApplyDeed.setVisibility(View.GONE);
                mApplyDeedText.setVisibility(View.GONE);
                mChangeDeed.setVisibility(View.GONE);
                mChangeDeedText.setVisibility(View.GONE);
            }
        } else {
            mApplyDeed.setVisibility(View.GONE);
            mApplyDeedText.setVisibility(View.GONE);
            mChangeDeed.setVisibility(View.GONE);
            mChangeDeedText.setVisibility(View.GONE);
        }


        /**
         * 如果是游戏大厅
         * 隐藏房契，隐藏改麦位，隐藏改密码
         */
        if (VoiceRoomConfig.isLobby()) {
            mApplyDeed.setVisibility(View.GONE);
            mApplyDeedText.setVisibility(View.GONE);
            mChangePassword.setVisibility(View.GONE);
        }
    }

    private void initListener() {
        mChangeTopic.setOnClickListener((v) -> {
            mVoiceChangeTitleDialog = new VoiceChangeTitleDialog(mContext, mTopic, mTopicType);
            mVoiceChangeTitleDialog.show();
            dismiss();
        });
        mChangePassword.setOnClickListener((v) -> {
            mPasswordDialog = new VoicePasswordDialog(mContext, mPassword);
            mPasswordDialog.show();
            dismiss();
        });

        mSetBlackList.setOnClickListener((v) -> {
            VoiceRoomBlackListActivity.launch(mContext);
            dismiss();
        });

        mApplyDeed.setOnClickListener(v -> {
            VoiceDeedDialog voiceDeedDialog = new VoiceDeedDialog(mContext, VoiceDeedDialog.APPLY_DEED);
            voiceDeedDialog.show();
            dismiss();
        });

        mChangeDeed.setOnClickListener(v -> {
            VoiceDeedDialog voiceDeedDialog = new VoiceDeedDialog(mContext, VoiceDeedDialog.CHANGE_DEED);
            voiceDeedDialog.show();
            dismiss();
        });

        mBeMaster.setOnClickListener(v -> {
            VoiceSocketHelper.requestMaster();
            dismiss();
        });

        mChangeBackground.setOnClickListener(v -> {
            if (VoiceRoomConfig.isIsSupportChangeBg()) {
                if (canChangeBackground()) {
                    VoiceRoomBackGroundActivity.Companion.Launch(mContext, VoiceRoomConfig.getRoomId(), VoiceRoomConfig.getRoomType());
                    dismiss();
                }
            } else {
                ToastToolKit.showShort(R.string.str_voice_bg_no_permission);
            }
        });

        mBtnCancel.setOnClickListener(v -> dismiss());

        mHiddenChatMessage.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                EventBus.getDefault().post(new VoiceEvent.ChatMessageHiddenEvent(true));
            } else {
                EventBus.getDefault().post(new VoiceEvent.ChatMessageHiddenEvent(false));
            }
        });

        mHiddenGiftMessage.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                EventBus.getDefault().post(new VoiceEvent.GiftMessageHiddenEvent(true));
            } else {
                EventBus.getDefault().post(new VoiceEvent.GiftMessageHiddenEvent(false));
            }
        });

        mHiddenJoinMessage.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                EventBus.getDefault().post(new VoiceEvent.JoinMessageHiddenEvent(true));
            } else {
                EventBus.getDefault().post(new VoiceEvent.JoinMessageHiddenEvent(false));
            }
        });
    }

    public VoiceNewSettingDialog setTopic(String topic) {
        this.mTopic = topic;
        return this;
    }

    public VoiceNewSettingDialog setTopicType(String topicType) {
        this.mTopicType = topicType;
        return this;
    }

    public VoiceNewSettingDialog setPassword(String password) {
        this.mPassword = password;
        return this;
    }

    private boolean canChangeBackground() {
        if (PositionHelper.isMaster()) {
            return true;
        }
        return false;
    }

    @Override
    protected void release() {

    }

    @Override
    public void destroy() {
        if (mVoiceChangeTitleDialog != null) {
            if (mVoiceChangeTitleDialog.isShowing()) {
                mVoiceChangeTitleDialog.dismiss();
            }
            mVoiceChangeTitleDialog.destroy();
            mVoiceChangeTitleDialog = null;
        }
        if (mPasswordDialog != null) {
            if (mPasswordDialog.isShowing()) {
                mPasswordDialog.dismiss();
            }
            mPasswordDialog = null;
        }
    }
}
