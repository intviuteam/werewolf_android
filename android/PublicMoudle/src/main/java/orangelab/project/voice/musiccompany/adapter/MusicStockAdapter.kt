package orangelab.project.voice.musiccompany.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.PLog
import kotlinx.android.synthetic.main.layout_voice_music_stock_item.view.*
import orangelab.project.common.db.entity.MusicDataEntity
import orangelab.project.voice.musiccompany.item.ItemDisplayer

/**
 * 曲库适配器
 */
class MusicStockAdapter(musicList: ArrayList<MusicDataEntity>) : RecyclerView.Adapter<MusicStockAdapter.MusicStockViewHolder>() {

    private val TAG = "MusicStockAdapter"

    private var musicList = musicList

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MusicStockViewHolder {

        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.layout_voice_music_stock_item, null)

        return MusicStockViewHolder(view)
    }

    override fun getItemCount(): Int {
        return musicList.size
    }

    override fun onBindViewHolder(holder: MusicStockViewHolder, position: Int) {
        PLog.e(TAG, "onBindViewHolder called!")
        ItemDisplayer.displayMusicItem(holder.itemView, musicList[position], position)
    }

    /**
     * 更新曲库
     */
    fun updateStock(musicList: ArrayList<MusicDataEntity>) {
        this.musicList = musicList
        notifyDataSetChanged()
    }

    inner class MusicStockViewHolder(contentView: View) : RecyclerView.ViewHolder(contentView) {

        init {
            itemView.progress_music_stock.max = 100
        }
    }
}