package orangelab.project.voice.musiccompany.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.view.ScreenUtils;
import com.tencent.TMG.ITMGContext;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.voice.musiccompany.config.MusicCompanyConfig;
import orangelab.project.voice.musiccompany.tecent.music.MusicCompanyManager;
import orangelab.project.voice.utils.PositionHelper;

/**
 * 伴奏，均衡器对话框
 */

public class MusicRoomControlDialog extends SafeDialog {

    private ImageView ivClose;
    private SeekBar seekVoiceHeadSeat;
    private SeekBar seekVoiceMic;
    private RadioButton cbDefault;
    private RadioButton cbLow;
    private RadioButton cbMiddle;
    private RadioButton cbHigh;
    private View rlMusicClose;

    private Context mContext;

    public MusicRoomControlDialog(@NonNull Context context) {

        super(context, R.style.radius_dialog);
        mContext = context;
        setCancelable(false);
        setCanceledOnTouchOutside(true);

        init();
    }

    private void init() {
        initView();
        initWindow();
        initData();
        initListener();
    }

    @Override
    protected void release() {

    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_music_room_control, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        ivClose = (ImageView) contentView.findViewById(R.id.iv_music_close);
        seekVoiceHeadSeat = (SeekBar) contentView.findViewById(R.id.seek_music_voice_headset);
        seekVoiceMic = (SeekBar) contentView.findViewById(R.id.seek_music_voice_mic);
        cbDefault = (RadioButton) contentView.findViewById(R.id.radio_music_default);
        cbLow = (RadioButton) contentView.findViewById(R.id.radio_music_low);
        cbMiddle = (RadioButton) contentView.findViewById(R.id.radio_music_middle);
        cbHigh = (RadioButton) contentView.findViewById(R.id.radio_music_high);
        rlMusicClose = contentView.findViewById(R.id.rl_music_close);

        seekVoiceMic.setMax(MusicCompanyConfig.micMax - MusicCompanyConfig.micMin);
        seekVoiceHeadSeat.setMax(MusicCompanyConfig.companyVolumeMax - MusicCompanyConfig.companyMin);
    }

    private void initListener() {
        rlMusicClose.setOnClickListener(v -> dismiss());

        seekVoiceMic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                try {
                    MusicCompanyConfig.mic = progress + MusicCompanyConfig.micMin;
                    if (PositionHelper.isSinger()) {
                        MusicCompanyManager.getInstance(mContext).changeMicVolume(MusicCompanyConfig.mic);
                    } else {
                        //如果不是唱歌的，恶意改到mic，永远是0
                        MusicCompanyManager.getInstance(mContext).changeMicVolume(0);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekVoiceHeadSeat.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                try {
                    MusicCompanyConfig.companyVolume = progress + MusicCompanyConfig.companyMin;
                    MusicCompanyManager.getInstance(mContext).changeCompanyVolume(MusicCompanyConfig.companyVolume);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        cbDefault.setOnCheckedChangeListener((buttonView, isChecked) -> {
            try {
                if (isChecked) {
                    MusicCompanyConfig.voiceQuality = ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY;
                    MusicCompanyManager.getInstance(mContext).changeRoomType(ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        cbLow.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                try {
                    MusicCompanyConfig.voiceQuality = ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY;
                    MusicCompanyManager.getInstance(mContext).changeRoomType(ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cbMiddle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                try {
                    MusicCompanyConfig.voiceQuality = ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY;
                    MusicCompanyManager.getInstance(mContext).changeRoomType(ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cbHigh.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                try {
                    MusicCompanyConfig.voiceQuality = ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY;
                    MusicCompanyManager.getInstance(mContext).changeRoomType(ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initData() {
        PLog.e("voiceQuality", "voiceQuality = " + MusicCompanyConfig.voiceQuality);
        if (MusicCompanyConfig.voiceQuality == ITMGContext.ITMG_ROOM_TYPE_FLUENCY) {
            cbLow.setChecked(true);
        } else if (MusicCompanyConfig.voiceQuality == ITMGContext.ITMG_ROOM_TYPE_STANDARD) {
            cbMiddle.setChecked(true);
        } else if (MusicCompanyConfig.voiceQuality == ITMGContext.ITMG_ROOM_TYPE_HIGHQUALITY) {
            cbHigh.setChecked(true);
        } else {
            cbDefault.setChecked(true);
        }

        seekVoiceMic.setProgress(MusicCompanyConfig.mic - MusicCompanyConfig.micMin);
        seekVoiceHeadSeat.setProgress(MusicCompanyConfig.companyVolume - MusicCompanyConfig.companyMin);
    }

}
