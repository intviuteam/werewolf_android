package orangelab.project.voice.privateroom

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.transport.TransportHelper
import com.androidtoolkit.transport.Transportable
import com.androidtoolkit.view.SwitchButton
import com.R
import com.androidtoolkit.ToastToolKit
import com.toolkit.ToolKit
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.voice.manager.VoiceRoomCommander
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData

/**
 *game_werewolf
 *2018/3/27 下午3:16
 *Mystery
 */
class PrivateRoomPaperPlaneActivity : PrivateRoomBaseActivity(), AdapterView.OnItemClickListener, TextWatcher, CompoundButton.OnCheckedChangeListener {


    class PayLoad : Transportable {
        var background: String = ""
        var blur = false
        var mUpSeatsData: MutableList<DataHolder> = ArrayList(0)
        var mFrom: PrivateRoomMemberData? = null
    }

    companion object {

        val TAG = "PrivateRoomPaperPlaneActivity"
        val EMPTY_DATA_HOLDER = DataHolder(false, null)
        val EMPTY_PLACE_HOLDER = DataHolder(false, true, null)

        fun Launch(context: Context, payload: PayLoad) {
            val intent = Intent(context, PrivateRoomPaperPlaneActivity::class.java)
            TransportHelper.putTransportable(TAG, payload)
            context.startActivity(intent)
        }

        fun TurnIntoDataHolder(data: PrivateRoomMemberData, isCheck: Boolean): DataHolder {
            return DataHolder(isCheck, data)
        }
    }

    private var mPayload: PayLoad? = null
    private lateinit var mGridView: GridView
    private lateinit var mEditText: EditText
    private lateinit var mEditTextWatch: TextView
    private lateinit var mAdapter: Adapter
    private lateinit var mSwitch: SwitchButton
    private var mNeedOutSeats = false

    private var mSelect: MutableList<DataHolder> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPayload = TransportHelper.getTransport(TAG)

        if (mPayload == null) {
            closeActivity()
            return
        }

        mPayload?.mUpSeatsData?.forEach {
            if (it.mIsCheck) {
                mSelect.add(it)
            }
        }

        setContentView(R.layout.layout_private_room_paper_plane)

        initWindow()

        initBackGround()

        (findViewById<TextView>(R.id.title_view_main_title) as TextView).setText(getString(R.string.str_private_room_paper_plane))

        findViewById<ImageView>(R.id.title_view_action_back).setOnClickListener({ v ->
            /**
             * 点击了返回
             */
            closeActivity()
        })

        findViewById<TextView>(R.id.title_send).setOnClickListener({ v ->
            /**
             * 点击了发送
             */
            handleSendPaperPlane()
        })

        mGridView = findViewById<GridView>(R.id.gridview) as GridView
        mAdapter = Adapter(this, mPayload!!.mUpSeatsData)
        mGridView.adapter = mAdapter
        mGridView.setOnItemClickListener(this)

        mEditText = findViewById<EditText>(R.id.id_edittext) as EditText
        mEditTextWatch = findViewById<TextView>(R.id.id_edittext_watch) as TextView
        mEditText.addTextChangedListener(this)
        mEditText.setText("")

        mSwitch = findViewById<SwitchButton>(R.id.switch_button) as SwitchButton
        mSwitch.setOnCheckChangeListener(this)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val dataHolder = mAdapter.getItem(position)
        if (dataHolder.mData == null) {
            return
        }
        if (dataHolder.mData?.memberIsSelf() ?: false) {
            /**
             * 不处理
             */
            return
        } else {
            if (dataHolder.mIsCheck) {
                dataHolder.mIsCheck = false
                mSelect.remove(dataHolder)
                mAdapter.refresh()
            } else {
                dataHolder.mIsCheck = true
                mSelect.add(dataHolder)
                mAdapter.refresh()
            }
        }
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val input: String = s?.toString() ?: ""

        mEditTextWatch.setText(MessageUtils.getString(R.string.str_private_room_paper_plane_watch, input.length.toString()))
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        mNeedOutSeats = isChecked
    }

    private fun initBackGround() {
        val background = findViewById<ImageView>(R.id.background) as ImageView
        if (mPayload?.blur ?: false) {
            VoiceRoomCommander.BackGroundManager.BlurBackGround(mPayload?.background ?: "", background, null)
        } else {
            val path = mPayload?.background ?: ""
            if (!TextUtils.isEmpty(path)) {
                PicassoUtils.loadImage(this, path, background)
            }
        }
    }

    private fun initWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val decorView = window.getDecorView()
            val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            decorView.setSystemUiVisibility(option)
            window.setStatusBarColor(resources.getColor(R.color.color_private_room_paper_plane_title_color))
        } else {

        }
    }

    override fun onBackPressed() {

    }

    override fun closeActivity() {
        hideKeyBoard(mEditText)
        super.closeActivity()
    }

    private fun handleSendPaperPlane() {
        if (mPayload == null || mPayload?.mFrom == null) {
            return
        }

        val sendToUsers: MutableList<PrivateRoomMemberData> = ArrayList()
        mSelect.forEach {
            if (it.mData != null) {
                sendToUsers.add(it.mData!!)
            }
        }

        if (RoomSocketEngineHelper.sendPaperPlane(mPayload?.mFrom, sendToUsers, mEditText.text.toString(), mNeedOutSeats)) {
            ToastToolKit.showShort(R.string.str_private_room_paper_plane_has_send)
            closeActivity()
        } else {
            ToastToolKit.showShort(R.string.str_private_room_paper_plane_no_target)
        }
    }


    private class Adapter : BaseAdapter {

        var mContext: Context
        var mData: MutableList<DataHolder>

        constructor(mContext: Context, mData: MutableList<DataHolder>) : super() {
            this.mContext = mContext
            this.mData = mData
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            var holder: ViewHolder? = null
            if (view == null) {
                view = View.inflate(mContext, R.layout.layout_private_room_paper_plane_item, null)
                holder = ViewHolder()
                holder!!.mHead = view!!.findViewById<ImageView>(R.id.userHead) as ImageView
                holder!!.mName = view!!.findViewById<TextView>(R.id.userName) as TextView
                holder!!.mCheck = view!!.findViewById<ImageView>(R.id.check) as ImageView
                view!!.setTag(holder)
            } else {
                holder = view!!.getTag() as ViewHolder
            }
            val dataHolder = getItem(position)
            if (dataHolder.mPlaceHolder) {
                holder.mName?.visibility = View.INVISIBLE
                holder.mCheck?.visibility = View.INVISIBLE
                holder.mHead?.visibility = View.INVISIBLE
            } else {
                if (dataHolder.mData == null) {
                    holder.mName?.visibility = View.INVISIBLE
                    holder.mCheck?.visibility = View.INVISIBLE
                    holder.mHead?.setImageResource(R.mipmap.ico_private_room_no_person)
                } else {
                    holder.mName?.visibility = View.VISIBLE
                    holder.mCheck?.visibility = View.VISIBLE
                    holder.mHead?.visibility = View.VISIBLE
                    holder.mName?.setText(dataHolder?.mData?.getMemberName() ?: "")
                    PicassoUtils.loadImage(holder.mHead!!.context, dataHolder?.mData?.getMemberIcon() ?: "", holder.mHead!!, R.mipmap.default_head)
                    if (dataHolder?.mIsCheck) {
                        holder.mCheck?.setImageResource(R.drawable.ico_private_room_check_true)
                    } else {
                        holder.mCheck?.setImageResource(R.drawable.ico_private_room_check_false)
                    }
                    if (dataHolder?.mData?.memberIsSelf() ?: false) {
                        holder.mCheck?.visibility = View.GONE
                    } else {
                        holder.mCheck?.visibility = View.VISIBLE
                    }
                }
            }
            return view!!
        }

        fun refresh() {
            notifyDataSetChanged()
        }

        override fun getItem(position: Int): DataHolder {
            return mData.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mData.size
        }

    }

    class DataHolder {
        var mIsCheck = false
        var mPlaceHolder = false
        var mData: PrivateRoomMemberData? = null

        constructor(mIsCheck: Boolean, mData: PrivateRoomMemberData?) {
            this.mIsCheck = mIsCheck
            this.mData = mData
        }

        constructor(mData: PrivateRoomMemberData?) {
            this.mData = mData
        }

        constructor()

        constructor(mIsCheck: Boolean, mPlaceHolder: Boolean, mData: PrivateRoomMemberData?) {
            this.mIsCheck = mIsCheck
            this.mPlaceHolder = mPlaceHolder
            this.mData = mData
        }

    }

    class ViewHolder {
        var mHead: ImageView? = null
        var mName: TextView? = null
        var mCheck: ImageView? = null
    }


}