package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * Created by lloydfinch on 30/11/2017.
 * <p>
 * 获取词库
 */

public class VoiceWords implements Serializable, Keepable {

    public String first = "";
    public String second = "";

    @Override
    public String toString() {
        return "VoiceWords{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
