package orangelab.project.voice.privateroom.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.engine.GlobalSocketConstant;

/**
 * game_werewolf
 * 2018/5/23 下午1:56
 * Mystery
 */
public class PrivateRoomMultiIntviu implements Keepable {

    public String room_type;
    public String room_id;
    public String room_pw;
    public List<String> target_user_id;
    public String type = GlobalSocketConstant.INVITE;
}
