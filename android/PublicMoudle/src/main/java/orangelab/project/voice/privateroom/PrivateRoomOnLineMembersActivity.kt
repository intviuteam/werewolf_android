package orangelab.project.voice.privateroom

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.RxToolKit
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.transport.TransportHelper
import com.androidtoolkit.transport.Transportable
import com.R
import orangelab.project.common.activity.PersonalInfoActivity
import orangelab.project.common.activity.SafeActivity
import orangelab.project.common.context.EmptyServerMessageProcess
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.common.engine.RoomSocketEngineHelper
import orangelab.project.common.utils.MessageUtils
import orangelab.project.common.utils.Utils
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.manager.VoiceRoomCommander
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData
import java.util.ArrayList

/**
 *game_werewolf
 *2018/3/25 上午10:22
 *Mystery
 */
class PrivateRoomOnLineMembersActivity : SafeActivity() {

    companion object {
        val TAG = "PrivateRoomOnLineMembersFragment"

        fun Launch(context: Context, payload: PayLoad) {
            val intent = Intent(context, PrivateRoomOnLineMembersActivity::class.java)
            TransportHelper.putTransportable(PrivateRoomOnLineMembersActivity.TAG, payload)
            context.startActivity(intent)
        }
    }

    class PayLoad : Transportable {
        lateinit var mOnLineMembers: MutableList<PrivateRoomMemberData>
        var mIsForceUpSeat = false
        var mIsTransferMaster = false
        var mTitle: String = ""
        var mSeatPosition = -1
        var mSeatLocked = false
    }

    private lateinit var mBack: View
    private lateinit var mTitle: TextView
    private lateinit var mData: RecyclerView
    private lateinit var mPayLoad: PayLoad
    private lateinit var mAdapter: Adapter
    private var mSocketIsDisConnect = false
    private var mServerMessageProcess = object : EmptyServerMessageProcess() {
        override fun handleServerSocketConnect() {
            mSocketIsDisConnect = true
        }

        override fun handleServerSocketDisconnect() {
            mSocketIsDisConnect = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        VoiceRoomCommander.ActivityManager.RegisterActivity(this)
        setContentView(R.layout.activity_on_line_users)

        Utils.setWindowStatusBarColor(this, R.color.vocie_status_bar)

        VoiceRoomCommander.ContextManager.GetPrivateRoomContext()?.registerProcess(mServerMessageProcess)

        mPayLoad = TransportHelper.getTransport(TAG)
        mBack = findViewById(R.id.iv_back)
        mTitle = findViewById<TextView>(R.id.tv_title) as TextView
        mData = findViewById<RecyclerView>(R.id.list_online_users) as RecyclerView


        mTitle.setText(mPayLoad.mTitle)

        mBack.setOnClickListener { v ->
            closeActivity()
        }

        mAdapter = Adapter(this, mPayLoad.mOnLineMembers)
        mAdapter.setOnItemClickListener(object : Adapter.OnItemClickListener {
            override fun onItemClick(position: Int) {

                if (mSocketIsDisConnect) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.network_disconnect))
                    return
                }

                val member = mPayLoad.mOnLineMembers?.get(position)
                if (member == null) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.str_voice_already_leave))
                } else {
                    if (mPayLoad.mIsForceUpSeat) {
                        /**
                         * 抱用户上麦 1。检查位置是否被锁住
                         */
                        if (mPayLoad.mSeatPosition == -1) {
                            /**
                             * 错误数据
                             */
                            return
                        }

                        if (mPayLoad.mSeatLocked) {
                            RoomSocketEngineHelper.sendPositionMessage(VoiceConstants.UNLOCK, mPayLoad.mSeatPosition + 1)
                        }

                        RoomSocketEngineHelper.sendForceSeat(member.getMemberId(), member.getMemberPosition(), mPayLoad.mSeatPosition)
                        closeActivity()
                        return
                    }

                    if (mPayLoad.mIsTransferMaster) {
                        RoomSocketEngineHelper.sendHandOverRoomOwner(member.getMemberPosition())
                        closeActivity()
                        return
                    }

                    PersonalInfoActivity.Launch(this@PrivateRoomOnLineMembersActivity, member.getMemberId())
                }
            }
        })
        mData.layoutManager = (WrapContentLinearLayoutManager(this))
        mData.adapter = mAdapter

    }

    override fun onBackPressed() {

    }

    override fun onActivityWindowInitFinish() {

    }

    override fun closeActivity() {
        VoiceRoomCommander.ActivityManager.UnRegisterActivity(this)
        finish()
    }

    override fun finish() {
        super.finish()
        RxToolKit.UnRegister(this)
        mAdapter.destroy()
        VoiceRoomCommander.ContextManager.GetPrivateRoomContext()?.unRegisterProcess(mServerMessageProcess)
    }

    class Adapter : RecyclerView.Adapter<Adapter.MyViewHolder> {

        private val mContext: Context
        private var mMembers: MutableList<PrivateRoomMemberData> = ArrayList()
        private var onItemClickListener: OnItemClickListener? = null

        constructor (mContext: Context, gameMembers: MutableList<PrivateRoomMemberData>) : super() {
            this.mContext = mContext
            this.mMembers = gameMembers
        }

        fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
            this.onItemClickListener = onItemClickListener
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_online_user, null))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val gameMember = mMembers[position]
            if (gameMember != null) {
                holder.tvName.text = gameMember.getMemberName()
                holder.tvMotto.text = gameMember.getMemberSignature()
                if (!TextUtils.isEmpty(gameMember.getMemberIcon())) {
                    PicassoUtils.loadImage(mContext, gameMember.getMemberIcon(), holder.ivHead, R.mipmap.default_head)
                }
                if (gameMember.memberIsMaster()) {
                    holder.tvMaster.visibility = View.VISIBLE
                    holder.tvUpSeat.visibility = View.GONE
                    holder.btnAddBlack.visibility = View.GONE
                } else {
                    holder.tvMaster.visibility = View.GONE
                    if (gameMember.memberIsUpSeat()) {
                        holder.tvUpSeat.visibility = View.VISIBLE
                    } else {
                        holder.tvUpSeat.visibility = View.GONE
                    }
                }
                holder.btnAddBlack.visibility = View.GONE
            }
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItemCount(): Int {
            return mMembers.size
        }

        fun destroy() {
            mMembers.clear()
            notifyDataSetChanged()
        }

        inner class MyViewHolder(contentView: View) : RecyclerView.ViewHolder(contentView) {
            var ivHead: ImageView
            var tvName: TextView
            var tvMaster: TextView
            var tvUpSeat: TextView
            var tvMotto: TextView
            var btnAddBlack: Button

            init {
                ivHead = contentView.findViewById<ImageView>(R.id.iv_online_user) as ImageView
                tvName = contentView.findViewById<TextView>(R.id.tv_online_name) as TextView
                tvMaster = contentView.findViewById<TextView>(R.id.tv_online_master) as TextView
                tvUpSeat = contentView.findViewById<TextView>(R.id.tv_online_up_seat) as TextView
                tvMotto = contentView.findViewById<TextView>(R.id.tv_online_motto) as TextView
                btnAddBlack = contentView.findViewById<Button>(R.id.btn_add_black) as Button
                btnAddBlack.visibility = View.GONE
                contentView.setOnClickListener { v ->
                    if (onItemClickListener != null) {
                        onItemClickListener!!.onItemClick(position)
                    }
                }
            }
        }

        interface OnItemClickListener {
            fun onItemClick(position: Int)
        }
    }

}