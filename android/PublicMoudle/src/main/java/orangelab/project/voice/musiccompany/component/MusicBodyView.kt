package orangelab.project.voice.musiccompany.component

import android.content.Context
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import cn.intviu.support.GsonHelper
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.ToastToolKit
import com.toolkit.action.Destroyable
import orangelab.project.voice.handler.AbstractSocketMessageHandler
import orangelab.project.voice.handler.VoiceMessageBean
import orangelab.project.voice.musiccompany.concrete.MusicManager
import orangelab.project.voice.musiccompany.concrete.MusicPlayerDisplayer
import orangelab.project.voice.musiccompany.event.MusicEvent
import orangelab.project.voice.musiccompany.model.PrepareSingBean
import orangelab.project.voice.utils.PositionHelper
import org.greenrobot.eventbus.EventBus

/**
 * 音乐伴奏主题view
 */
class MusicBodyView(context: Context, contentView: ViewGroup) : Destroyable {

    private val TAG = "MusicBodyView"

    private var musicCompanyView: View = contentView.findViewById(R.id.rl_music_player_container)

    init {
        PLog.e(TAG, "MusicBodyView init")
    }

    fun showPlayerView() {
        musicCompanyView.visibility = View.VISIBLE
        val musicPlayerDisplayer = MusicPlayerDisplayer(musicCompanyView)
        MusicManager.instance.refreshMusicPLayerDisplayer(musicPlayerDisplayer)
    }

    fun hidePlayerView() {
        musicCompanyView.visibility = View.GONE
    }

    override fun destroy() {
        //release something
    }

    /**
     * 消息处理
     */
    val observer = object : AbstractSocketMessageHandler() {

        override fun handleBookSong(messageBean: VoiceMessageBean) {
            //展示信息
            if (!TextUtils.isEmpty(messageBean.message)) {
                ToastToolKit.showShort(messageBean.message)
            }
            val bean: PrepareSingBean = GsonHelper.parseJsonToBean(messageBean.payload.toString(), PrepareSingBean::class.java)
            if (bean.song != null) {
                if (PositionHelper.isSelf(bean.song!!.booker)) {
                    //ToastToolKit.showShort(R.string.str_music_song_order_success)
                }
            }
            PLog.e(TAG, "handleBookSong: ${messageBean.toString()}")
        }

        override fun destroy() {

        }

    }
}