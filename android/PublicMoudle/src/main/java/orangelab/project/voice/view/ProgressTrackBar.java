package orangelab.project.voice.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 28/11/2017.
 * <p>
 * //单向进度条
 */

public class ProgressTrackBar extends View {

    private static final String TAG = "ProgressTrackBar";

    private Paint mPaint;
    private float startAngle = 0;
    private int firstLayerColor = Color.WHITE;
    private int secondLayerColor = Color.parseColor("#fd14a8");

    private float maxProgress = 20;
    private float currentProgress = 0;
    private int speed = 1000;
    private int progressWidth = ScreenUtils.dip2px(2);

    private OnProgressFinished onProgressFinished;

    private Handler taskHandler;

    public ProgressTrackBar(Context context) {
        super(context);
        init();
    }

    public ProgressTrackBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressTrackBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(progressWidth);
    }

    public void setFirstLayerColor(int firstLayerColor) {
        this.firstLayerColor = firstLayerColor;
    }

    public void setSecondLayerColor(int secondLayerColor) {
        this.secondLayerColor = secondLayerColor;
    }

    public void setMaxProgress(float maxProgress) {
        this.maxProgress = maxProgress;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setStartAngle(float startAngle) {
        this.startAngle = startAngle;
    }

    public void setProgressWidth(int progressWidth) {
        this.progressWidth = progressWidth;
    }

    @SuppressLint("HandlerLeak")
    private void initTask() {
        taskHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (currentProgress <= maxProgress) {
                    currentProgress++;
                    postInvalidate();
                    taskHandler.sendEmptyMessageDelayed(0, speed);
                } else {
                    stopTask();
                    setVisibility(GONE);
                }
            }
        };
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth() >> 1;
        int y = getHeight() >> 1;
        int center = Math.min(x, y);
        int radius = center - progressWidth;

        int left = center - radius;
        int top = center - radius;
        int right = center + radius;
        int bottom = center + radius;
        RectF oval = new RectF(left, top, right, bottom);

        mPaint.setColor(firstLayerColor);
        canvas.drawCircle(center, center, radius, mPaint);

        mPaint.setColor(secondLayerColor);
        float sweepAngle = (currentProgress / maxProgress) * 360;
        canvas.drawArc(oval, startAngle, sweepAngle, false, mPaint);
    }

    public void startTask(int progress) {
        reset();
        currentProgress = progress;
        initTask();
        taskHandler.sendEmptyMessage(0);
    }

    public void startTask(int progress, OnProgressFinished onProgressFinished) {
        this.onProgressFinished = onProgressFinished;
        reset();
        currentProgress = progress;
        initTask();
        taskHandler.sendEmptyMessage(0);
    }

    public void stopTask() {
        if (onProgressFinished != null) {
            onProgressFinished.onFinished();
        }
        if (taskHandler != null) {
            taskHandler.removeCallbacksAndMessages(null);
        }
    }

    private void reset() {
        stopTask();
        startAngle = 0;
        currentProgress = 0;
    }

    //被系统强制移除停止任务
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopTask();
    }

    public interface OnProgressFinished {
        void onFinished();
    }
}
