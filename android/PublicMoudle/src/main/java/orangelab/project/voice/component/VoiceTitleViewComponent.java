package orangelab.project.voice.component;

import android.view.View;

import orangelab.project.common.model.ServerMessageEventOver;
import orangelab.project.voice.interfaces.GetOnLineUsers;

import com.toolkit.action.Destroyable;

/**
 * Created by lloydfinch on 25/09/2017.
 */

public interface VoiceTitleViewComponent extends Destroyable {

    void init(String roomId, String title, String password, String titleType);

    //展示数据到UI
    void displayData();

    void handleServerEvent(ServerMessageEventOver event);

    void setRoomId(String roomId);

    void updateTitle(String title, String type);

    void updateOnLineNumber(int number);

    void updateWifiState(boolean isConnected);

    void onBackPressed(View.OnClickListener onBackPressedListener);

    void onOnLineNumberClicked(View.OnClickListener onOnLineNumberClickedListener);

    void onSettingClicked(View.OnClickListener onSettingClickedListener);

    void onShareClicked(View.OnClickListener onShareClickedListener);

    void setPassword(String password);

    int getOnLineNumber();

    String getTitle();

    String getTitleType();

    void updateMasterUI();
}
