package orangelab.project.voice.privateroom.view

import android.view.View
import orangelab.project.common.effect.emotion.EmotionManiFest

/**
 *game_werewolf
 *2018/3/21 上午11:52
 *Mystery
 */
interface PrivateRoomSpeakObserver {

    fun onBeforeSendEmotion(): Boolean

    fun onBeforeSendGameEmotion(): Boolean

    fun onSendEmotion(emotion: EmotionManiFest.Emotion)

    fun onShowGiftDialog()

    fun onShowGameEmotion(target: View)

    fun onSwitchToMouse(): Boolean

    fun onSwitchToHand(): Boolean

    fun onSpeakTouchDown(): Boolean

    fun onSpeakTouchUp(): Boolean
}