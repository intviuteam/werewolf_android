package orangelab.project.voice.model;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2018/3/29 下午6:22
 * Mystery
 */

public class VoiceRoomBackGroundApiResult implements Keepable {

    public static final String BLUR_AVATAR = "blur_avatar";
    public static final String DEFAULT = "default";
    public static final String CUSTOM = "custom";

    public ArrayList<String> default_bg;
    public ArrayList<ImageItem> images;
    public CurImageItem current;

    public int upload_limit = 0;
    public int upload_times = 0;

    public class ImageItem implements Keepable {
        public String url;
        public String image_id;
    }

    public class CurImageItem implements Keepable {
        public String type;
        public String url;
        public String image_id;

        public boolean isBlur() {
            return TextUtils.equals(BLUR_AVATAR, type);
        }

        public boolean isSystem() {
            return TextUtils.equals(DEFAULT, type);
        }

        public boolean isCustom() {
            return TextUtils.equals(CUSTOM, type);
        }
    }
}
