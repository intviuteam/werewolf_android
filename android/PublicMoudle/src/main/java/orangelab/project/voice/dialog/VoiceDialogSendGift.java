package orangelab.project.voice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.R;
import orangelab.project.game.component.GameMember;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;

import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 16/10/2017.
 */

public class VoiceDialogSendGift extends Dialog {

    private ListView listView;
    private TextView placeHolder;
    private View placeHolderView;
    private TextView placeHolderTextView;

    private LinearLayout llContentViewContainer;
    private View btnSure;
    private View btnCancel;
    private Context mContext;

    public VoiceDialogSendGift(Context context, List<GameMember.GameMemberData> gameMemberDataList) {
        super(context, R.style.radius_dialog);
        mContext = context;

        View contentView = LayoutInflater.from(context).inflate(R.layout.layout_dialog_voice_send_gift, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        contentView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss();
                return true;
            }
            return false;
        });
        setContentView(contentView);

        llContentViewContainer = (LinearLayout) contentView.findViewById(R.id.ll_content_view_container);
        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);


        View view = LayoutInflater.from(context).inflate(R.layout.layout_intviufriend, null);
        placeHolder = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolder.setVisibility(View.GONE);
        placeHolderView = view.findViewById(R.id.intviufriend_placeholder_iv);
        placeHolderView.setBackgroundResource(R.mipmap.voice_friend_empty);
        placeHolderView.setVisibility(View.GONE);
        placeHolderTextView = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolderTextView.setText(MessageUtils.getString(R.string.gifts_present_dialog_no_people));
        placeHolderTextView.setTextColor(mContext.getResources().getColor(R.color.color_voice_empty_friend));
        listView = (ListView) view.findViewById(R.id.intviu_friend);
        listView.setVisibility(View.VISIBLE);
        listView.setDividerHeight(0);
        if (gameMemberDataList != null && gameMemberDataList.size() > 0) {
            listView.setVisibility(View.VISIBLE);
            placeHolderView.setVisibility(View.GONE);
            placeHolderTextView.setVisibility(View.GONE);
        } else {
            listView.setVisibility(View.GONE);
            placeHolderView.setVisibility(View.VISIBLE);
            placeHolderTextView.setVisibility(View.VISIBLE);
        }
        VoiceDialogSendGift.GamePresentGiftAdapter adapter = new VoiceDialogSendGift.GamePresentGiftAdapter(context, gameMemberDataList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            GameMember.GameMemberData gameMemberData = adapter.getItem(position);
            if (Utils.checkUserIdIsTourist(gameMemberData.userId)) {
                ToastToolKit.showShort(R.string.gifts_preset_to_tourist);
            } else {
                adapter.setSelectPosition(position);
                GameMember.GameMemberData result = adapter.getSelectPosition();
                if (result != null) {
                    ExhibitionsActivity.startActivityFromGame(
                            context,
                            result.userId,
                            result.userName,
                            result.userSex,
                            result.userImage,
                            result.position
                    );
                }
                dismiss();
            }
        });


        initWindow();

        changeContentView(view);
        btnSure.setOnClickListener(v -> {
            dismiss();
        });
        btnCancel.setOnClickListener(v -> {
            dismiss();
        });
    }


    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void changeContentView(View contentView) {
        llContentViewContainer.removeAllViews();
        llContentViewContainer.addView(contentView);
    }

    private static class GamePresentGiftAdapter extends BaseAdapter {

        private Context mContext;
        private List<GameMember.GameMemberData> mGameMemberDatas;
        private int selectPosition = -1;

        public GamePresentGiftAdapter(Context context, List<GameMember.GameMemberData> gameMemberDatas) {
            mContext = context;
            mGameMemberDatas = gameMemberDatas;
        }

        public void setSelectPosition(int selectPosition) {
            this.selectPosition = selectPosition;
            notifyDataSetChanged();
        }

        public GameMember.GameMemberData getSelectPosition() {
            if (selectPosition != -1) {
                return mGameMemberDatas.get(selectPosition);
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return mGameMemberDatas.size();
        }

        @Override
        public GameMember.GameMemberData getItem(int position) {
            return mGameMemberDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            VoiceDialogSendGift.ViewHolder holder = null;
            if (mContext == null) {
                return convertView;
            }
            if (convertView == null) {
                holder = new VoiceDialogSendGift.ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_presentgift_item, null);
                holder.userhead = (UserHeadView) convertView.findViewById(R.id.intviufriend_ic);
                holder.name = (TextView) convertView.findViewById(R.id.intviufriend_name);
                holder.radio = (RadioButton) convertView.findViewById(R.id.intviufriend_check);
                convertView.setTag(holder);
            } else {
                holder = (VoiceDialogSendGift.ViewHolder) convertView.getTag();
            }
            GameMember.GameMemberData item = getItem(position);
            holder.radio.setFocusable(false);
            holder.radio.setClickable(false);
            if (item != null) {
                holder.userhead.setUserHeadImageUrl(item.userImage);
                holder.userhead.setUserSex(UserHeadView.NONE_SEX);
                holder.name.setText(item.userName);
            }
            if (selectPosition == position) {
                holder.radio.setChecked(true);
            } else {
                holder.radio.setChecked(false);
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        public UserHeadView userhead;
        public TextView name;
        public RadioButton radio;
    }
}
