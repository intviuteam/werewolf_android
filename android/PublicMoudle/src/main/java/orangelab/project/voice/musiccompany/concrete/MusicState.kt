package orangelab.project.voice.musiccompany.concrete

/**
 * 音乐状态
 */
interface MusicState {

    companion object {

        /**
         * 下载状态
         */
        val STATE_DOWN_NON = 0x100011       //未下载
        val STATE_DOWN_ING = 0x100000       //下载中
        val STATE_DOWN_PAUSE = 0x100001     //下载完成
        val STATE_DOWN_FINISH = 0x100002    //下载暂停
        val STATE_DOWN_FAILED = 0x100003    //下载失败
        val STATE_DOWN_WAIT = 0x100004      //等待中


        /**
         * 播放状态，均未已下载
         */
        val STATE_PLAY_NON = 0x200000       //未点击
        val STATE_PLAY_ING = 0x200001       //播放
        val STATE_PLAY_PAUSE = 0x200002     //暂停
        val STATE_PLAY_STOP = 0x200003      //停止
        val STATE_PLAY_LOOP = 0x200004      //循环播放
    }

}