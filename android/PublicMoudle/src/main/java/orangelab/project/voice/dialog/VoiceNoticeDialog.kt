package orangelab.project.voice.dialog

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.R
import com.androidtoolkit.ToastToolKit
import com.androidtoolkit.view.ScreenUtils
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.tool.WrapContentLinearLayoutManager
import orangelab.project.voice.adapter.VoicePopularAdapter
import orangelab.project.voice.model.VoicePopularBean

/**
 * 房间 "公告&贡献榜"弹框
 */
class VoiceNoticeDialog(context: Context) : SafeDialog(context, R.style.radius_dialog) {

    private lateinit var viewNotice: View                  //"房间公告"界面
    private lateinit var viewPopular: View                 //"人气贡献榜"界面

    private lateinit var btnLeft: Button                   //"房间公告"按钮
    private lateinit var btnRight: Button                  //"人气贡献榜"按钮

    private lateinit var btnPackUp: TextView                  //收起按钮
    private lateinit var btnEdit: TextView                    //编辑按钮
    private lateinit var editNotice: EditText                //编辑框
    private lateinit var rlPopular: RecyclerView             //贡献列表

    init {
        initView(context)
        initListener()
    }

    private fun initView(context: Context) {
        val contentView = LayoutInflater.from(context).inflate(R.layout.layout_dialog_voice_notice, null)

        viewNotice = contentView.findViewById(R.id.ll_voice_notice_left)
        viewPopular = contentView.findViewById(R.id.rl_voice_notice_right)

        btnLeft = contentView.findViewById(R.id.btn_voice_notice_left) as Button
        btnRight = contentView.findViewById(R.id.btn_voice_notice_right) as Button

        btnPackUp = contentView.findViewById(R.id.tv_voice_notice_pack_up) as TextView

        btnEdit = viewNotice.findViewById(R.id.tv_voice_notice_edit) as TextView
        editNotice = viewNotice.findViewById(R.id.edit_voice_notice) as EditText

        rlPopular = viewPopular.findViewById(R.id.rl_voice_popular_list) as RecyclerView
        rlPopular.layoutManager = WrapContentLinearLayoutManager(rlPopular.context)

        setContentView(contentView)

        //尺寸初始化
        val dialogMaxWidth = ScreenUtils.dip2px(360f)
        val params = window!!.attributes
        val dur = (ScreenUtils.getScreenWidth() * 0.25f).toInt()
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth
        } else {
            params.width = (ScreenUtils.getScreenWidth() * 0.95f).toInt()
        }
        window!!.attributes = params

        checkLeft()
        unCheckRight()
        showNotice()
        hidePopular()
    }

    private fun initListener() {
        btnLeft.setOnClickListener {
            checkLeft()
            unCheckRight()
            showNotice()
            noEditable()
            hidePopular()
        }

        btnRight.setOnClickListener {
            checkRight()
            unCheckLeft()
            showPopular()
            hideNotice()
        }

        btnPackUp.setOnClickListener { dismiss() }
        btnEdit.setOnClickListener {
            //TODO 发送到server
            ToastToolKit.showShort("编辑成功")
        }


        editNotice.setOnFocusChangeListener { _, hasFocus ->
            editNotice.isCursorVisible = hasFocus
        }
        //清除焦点
        noEditable()
    }

    /**
     * 加载左边数据
     */
    private fun loadLeftData() {
        //TODO 加载server数据，填充左边UI
    }

    /**
     * 加载右边数据
     */
    private fun loadRightData() {
        //TODO 加载server数据，填充右边UI

        val list: ArrayList<VoicePopularBean> = ArrayList()
        //TODO 测试代码
        for (i in 1..10) {
            val bean = VoicePopularBean()
            bean.sortNumber = i
            bean.name = "test$i"
            bean.popularity = ((i shl 10) + i shl 4)
            bean.headImg = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1532174818887&di=11d4bfe7dc77cc33f5facf5fd0ec3ded&imgtype=0&src=http%3A%2F%2Fp3.gexing.com%2FG1%2FM00%2FB5%2F65%2FrBACE1Odseazq3OQAAAP3FOHIWI121_200x200_3.jpg%3Frecache%3D20131108"

            list.add(bean)
        }

        val adapter = VoicePopularAdapter(list)
        rlPopular.adapter = adapter
    }

    private fun noEditable() {
        editNotice.isCursorVisible = false
        editNotice.clearFocus()
    }

    /**
     * 选中"房间公告"
     */
    private fun checkLeft() {
        btnLeft.setTextColor(Color.parseColor("#000000"))
        btnLeft.setBackgroundResource(R.drawable.bg_voice_notice_left_checked)
    }

    /**
     * 取消选中"房间公告"
     */
    private fun unCheckLeft() {
        btnLeft.setTextColor(Color.parseColor("#bbbbbb"))
        btnLeft.setBackgroundResource(R.drawable.bg_voice_notice_left_un_checked)
    }

    /**
     * 选中"人气贡献榜"
     */
    private fun checkRight() {
        btnRight.setTextColor(Color.parseColor("#000000"))
        btnRight.setBackgroundResource(R.drawable.bg_voice_notice_right_checked)
    }

    /**
     * 取消选中"人气贡献榜"
     */
    private fun unCheckRight() {
        btnRight.setTextColor(Color.parseColor("#bbbbbb"))
        btnRight.setBackgroundResource(R.drawable.bg_voice_notice_right_un_checked)
    }

    /**
     * 展示"房间公告"界面
     */
    private fun showNotice() {
        viewNotice.visibility = View.VISIBLE
        loadLeftData()
    }

    /**
     * 隐藏"房间公告"界面
     */
    private fun hideNotice() {
        viewNotice.visibility = View.GONE
    }

    /**
     * 展示"人气贡献榜"界面
     */
    private fun showPopular() {
        viewPopular.visibility = View.VISIBLE
        loadRightData()
    }

    /**
     * 隐藏"人气贡献榜"界面
     */
    private fun hidePopular() {
        viewPopular.visibility = View.GONE
    }

    override fun release() {

    }

}