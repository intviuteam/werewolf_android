package orangelab.project.voice.dialog;

import android.content.Context;
import android.support.annotation.NonNull;

import com.androidtoolkit.view.ScreenUtils;
import com.R;
import orangelab.project.common.dialog.SafeDialog;
import com.toolkit.InitFormater;

/**
 * Created by lloydfinch on 24/11/2017.
 * <p>
 * 模板模式dialog格式化器
 */

public abstract class BasicFormatDialog extends SafeDialog implements InitFormater {

    protected Context mContext;

    public BasicFormatDialog(@NonNull Context context) {
        this(context, R.style.DarkDialog);
        this.mContext = context;
    }

    public BasicFormatDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
        init();
    }

    @Override
    public void init() {
        beforeInit();
        initView();
        initWindow();
        initListener();
        initEvent();
        initData();
    }

    protected void beforeInit() {

    }

    @Override
    public void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(360);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }
}
