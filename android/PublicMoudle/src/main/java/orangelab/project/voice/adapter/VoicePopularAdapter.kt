package orangelab.project.voice.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.PicassoUtils
import orangelab.project.MainApplication
import orangelab.project.voice.model.VoicePopularBean

class VoicePopularAdapter(list: List<VoicePopularBean>) : RecyclerView.Adapter<VoicePopularAdapter.VoicePopularViewHolder>() {

    //列表数据
    private var popularBeanList: List<VoicePopularBean> = list

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VoicePopularViewHolder {

        val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.layout_item_voice_ppular, parent, false)

        return VoicePopularViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return popularBeanList.size
    }

    override fun onBindViewHolder(holder: VoicePopularViewHolder?, position: Int) {

        val popularBean: VoicePopularBean = popularBeanList[position]

        holder!!.tvNumber.text = popularBean.sortNumber.toString()
        holder.tvName.text = popularBean.name.toString()
        holder.tvPopularity.text = popularBean.popularity.toString()

        //加载等级勋章
        when (popularBean.sortNumber) {
            1 -> {
                holder.ivLevel.visibility = View.VISIBLE
                holder.ivLevel.setImageResource(R.mipmap.ic_voice_notice_gold)
            }
            2 -> {
                holder.ivLevel.visibility = View.VISIBLE
                holder.ivLevel.setImageResource(R.mipmap.ic_voice_notice_silver)
            }
            3 -> {
                holder.ivLevel.visibility = View.VISIBLE
                holder.ivLevel.setImageResource(R.mipmap.ic_voice_notice_coprum)
            }
            else -> {
                holder.ivLevel.visibility = View.INVISIBLE
            }
        }

        //加载头像
        PicassoUtils.loadImageByDefault(MainApplication.getInstance(),
                popularBean.headImg, holder.ivHead, R.mipmap.default_head)
    }


    inner class VoicePopularViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvNumber: TextView = itemView.findViewById(R.id.tv_voice_popular_number) as TextView
        val ivLevel: ImageView = itemView.findViewById(R.id.iv_voice_popular_level) as ImageView
        val ivHead: ImageView = itemView.findViewById(R.id.iv_voice_popular_head) as ImageView
        val tvName: TextView = itemView.findViewById(R.id.tv_voice_popular_name) as TextView
        val tvPopularity: TextView = itemView.findViewById(R.id.tv_voice_popular_popularity) as TextView

    }
}