package orangelab.project.voice.manager;

import android.content.Context;
import android.content.Intent;

import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.game.service.BackGroundMediaService;

/**
 * Created by lloydfinch on 02/11/2017.
 */

public class VoiceRoomManager {

    public static void playSound(Context context, String type, int dur) {
        Intent intent = new Intent(context, BackGroundMediaService.class);
        IntentDataHelper.setMediaType(intent, type);
        IntentDataHelper.setMediaDurTime(intent, dur);
        context.startService(intent);
    }
}