package orangelab.project.voice.lobby.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * server返回的邀请的实体类
 */
public class LobbyInviteGameBean implements Serializable, Keepable {

    public Game game;
    public Inviter inviter;
    public int position;

    public static class Game implements Serializable, Keepable {
        public String type = "";
        public String name = "";
        public String icon = "";
        public String uuid = "";
        public int expire_time = 10000;//这里添加个默认时间10s，防止server无返回的情况
        public String invite_id = "";


        @Override
        public String toString() {
            return "Game{" +
                    "type='" + type + '\'' +
                    ", name='" + name + '\'' +
                    ", icon='" + icon + '\'' +
                    ", uuid='" + uuid + '\'' +
                    ", expire_time=" + expire_time +
                    ", invite_id='" + invite_id + '\'' +
                    '}';
        }
    }

    public static class Inviter implements Serializable, Keepable {
        public String name = "";
        public String id = "";
        public String avatar = "";

        @Override
        public String toString() {
            return "Inviter{" +
                    "name='" + name + '\'' +
                    ", id='" + id + '\'' +
                    ", avatar='" + avatar + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LobbyInviteGameBean{" +
                "game=" + game +
                ", inviter=" + inviter +
                ", position=" + position +
                '}';
    }
}
