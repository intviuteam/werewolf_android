package orangelab.project.voice.privateroom.view

import android.util.SparseArray
import android.view.View
import com.androidtoolkit.AndroidToolKit
import com.R
import orangelab.project.voice.manager.VoiceEmotionManager
import orangelab.project.voice.privateroom.PrivateRoomContext
import orangelab.project.voice.privateroom.PrivateRoomTools
import com.toolkit.action.Action
import com.toolkit.action.Destroyable
import orangelab.project.voice.manager.SmallGameManager

/**
 *game_werewolf
 *2018/3/21 下午7:35
 *Mystery
 */
class PrivateRoomMembersView : Destroyable {

    private var mPrivateRoomMember: SparseArray<PrivateRoomMemberView> = SparseArray()

    constructor()

    fun init(rootView: View, context: PrivateRoomContext, privateRoomMemberViewObserver: PrivateRoomMemberViewObserver) {
        /**
         * 初始化View数据
         */
        for (i: Int in PrivateRoomTools.MIN_POSITION until PrivateRoomTools.MAX_POSITION_REAL) {
            val target = "position_${i}_container"
            try {

                val resourceId = R.id::class.java.getField(target).getInt(null)
                val rootView: View = rootView.findViewById(resourceId)
                val seat = context.getSeatData(i)
                val privateRoomMemberView = PrivateRoomMemberViewImpl(i, rootView, seat!!)
                privateRoomMemberView.refresh()
                privateRoomMemberView.setObserver(privateRoomMemberViewObserver)
                mPrivateRoomMember.put(i, privateRoomMemberView)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun refreshAll() {
        for (i: Int in PrivateRoomTools.MIN_POSITION until PrivateRoomTools.MAX_POSITION_REAL) {
            val memberView = mPrivateRoomMember.valueAt(i)
            memberView.refreshForce()
        }
    }

    fun refresh(position: Int) {
        mPrivateRoomMember.get(position)?.refresh()
    }

    fun refreshMemberState(position: Int) {
        mPrivateRoomMember.get(position)?.refreshMemberState()
    }

    fun speak(position: Int, isSpeaking: Boolean) {
        mPrivateRoomMember.get(position)?.turnIntoSpeaking(isSpeaking)
    }

    fun lock(position: Int, isLock: Boolean) {
        if (isLock) {
            mPrivateRoomMember.get(position)?.turnIntoLock()
        } else {
            mPrivateRoomMember.get(position)?.turnIntoUnLock()
        }
    }

    fun getPrivateRoomMemberView(position: Int): PrivateRoomMemberView? {
        return mPrivateRoomMember.get(position)
    }

    override fun destroy() {
        /**
         * 释放所有资源
         */
        AndroidToolKit.TravelSparseArray(mPrivateRoomMember, Action {
            it.destroy()
        })
        mPrivateRoomMember.clear()
    }
}