package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lloydfinch on 06/12/2017.
 * <p>
 * "谁是卧底"游戏结束对话框专用实体类
 */

public class VoiceSAFBean implements Serializable, Keepable {

    public boolean isSpy;//自己是否是卧底
    public boolean victory;//自己是否赢了
    public String spyWord;
    public String goodWord;
    public List<VoiceSimpleUserBean> first;
    public List<VoiceSimpleUserBean> second;
}
