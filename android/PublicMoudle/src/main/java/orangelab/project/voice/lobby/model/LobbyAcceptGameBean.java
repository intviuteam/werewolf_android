package orangelab.project.voice.lobby.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * server返回的接受邀请的实体
 */
public class LobbyAcceptGameBean implements Serializable, Keepable {

    public String invite_id = "";
    public int position;
    public String type = "";
    public String send_id = "";
    public Acceptor acceptor;

    public static class Acceptor implements Serializable, Keepable {
        public String id = "";
        public String name = "";
        public String avatar = "";
    }

}
