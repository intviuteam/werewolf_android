package orangelab.project.voice.musiccompany.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.R
import com.androidtoolkit.PLog
import orangelab.project.voice.musiccompany.component.MusicOredredDisplayer
import orangelab.project.voice.musiccompany.concrete.MusicManager
import orangelab.project.voice.musiccompany.concrete.MusicState
import orangelab.project.voice.musiccompany.event.MusicEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * 已点列表
 */
class MusicOrderedFragment : Fragment() {

    private val TAG: String = "MusicOrderedFragment"

    private lateinit var contentView: View
    private lateinit var displayer: MusicOredredDisplayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        PLog.e(TAG, "onCreateView")
        contentView = inflater.inflate(R.layout.fragment_music_ordered, container, false)

        init()

        return contentView
    }

    private fun init() {
        EventBus.getDefault().register(this)
        MusicManager.instance.refreshOrdered()
        displayer = MusicOredredDisplayer(this.contentView)

        refresh()
    }

    override fun onResume() {
        super.onResume()
        PLog.e(TAG, "onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDownLoadFinishedEvent(event: MusicEvent.MusicDownLoadEvent) {
        if (event.data != null) {
            if (event.data.state == MusicState.STATE_DOWN_FINISH) {
                PLog.e(TAG, "refresh, onDownLoadFinishedEvent")
                MusicManager.instance.refreshOrdered()
                refresh()
            }
        }
    }

    fun refresh() {
        PLog.e(TAG, "refresh")
        displayer.refresh()
    }
}
