package orangelab.project.voice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.voice.model.VoiceBlackListMode;
import com.androidtoolkit.PicassoUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 29/10/2017.
 */

public class VoiceRoomBlackListAdapter extends RecyclerView.Adapter<VoiceRoomBlackListAdapter.BlackListViewHolder> implements Destroyable {

    private List<VoiceBlackListMode> blackList = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener onItemClickListener;

    public VoiceRoomBlackListAdapter(Context context, List<VoiceBlackListMode> blackList) {
        this.mContext = context;
        this.blackList = blackList;
    }

    public void updateBlackList(List<VoiceBlackListMode> blackList) {
        this.blackList.clear();
        this.blackList.addAll(blackList);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public BlackListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_voice_room_black_list, null);
        return new BlackListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BlackListViewHolder holder, int position) {
        VoiceBlackListMode user = blackList.get(position);
        if (user != null) {
            holder.tvName.setText(user.name);
            if (!TextUtils.isEmpty(user.image)) {
                PicassoUtils.loadImage(mContext, user.image, holder.ivHead);
            }
        }
    }

    @Override
    public int getItemCount() {
        return blackList.size();
    }

    class BlackListViewHolder extends RecyclerView.ViewHolder {

        ImageView ivHead;
        TextView tvName;
        View btnAdd;
        View btnRemove;
        View btnEdit;

        public BlackListViewHolder(View itemView) {
            super(itemView);

            ivHead = (ImageView) itemView.findViewById(R.id.iv_black_list_user);
            tvName = (TextView) itemView.findViewById(R.id.tv_black_list_name);
            btnAdd = itemView.findViewById(R.id.btn_black_list_add);
            btnRemove = itemView.findViewById(R.id.btn_black_list_remove);
            btnEdit = itemView.findViewById(R.id.btn_black_list_edit);

            btnRemove.setOnClickListener(v -> {
                VoiceBlackListMode userItem = blackList.get(getPosition());
                if (userItem != null) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(userItem);
                    }
                }
                //new VoiceBlackDialog(mContext, userItem.id).show();
            });
            itemView.setOnClickListener(v -> {
                VoiceBlackListMode userItem = blackList.get(getPosition());
                if (userItem != null) {
                    PersonalInfoActivity.Launch(mContext, userItem.id);
                }
            });
        }
    }

    @Override
    public void destroy() {
        blackList.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(VoiceBlackListMode mode);
    }
}
