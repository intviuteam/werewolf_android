package orangelab.project.voice.view;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.viewpagergroup.GridViewPagerGroup;
import com.androidtoolkit.view.viewpagergroup.GridViewPagerGroupHolderCreator;
import com.toolkit.action.Destroyable;

import java.io.InputStream;
import java.util.Locale;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

public class EmotionViewPagerGroup extends GridViewPagerGroup<EmotionManiFest.Emotion, EmotionViewHolder> implements Destroyable {

    private Context mContext;
    private SafeHandler mSafeHandler = new SafeHandler();
    private OnItemClickedListener mOnItemClickedListener;

    public EmotionViewPagerGroup(@NonNull Context context) {
        this(context, null);
    }

    public EmotionViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmotionViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

        setViewPagerGroupHolderCreator(new GridViewPagerGroupHolderCreator<EmotionManiFest.Emotion, EmotionViewHolder>() {
            @Override
            public EmotionViewHolder createViewHolder(View view) {
                EmotionViewHolder holder = new EmotionViewHolder();
                holder.mImageView = (ImageView) view.findViewById(R.id.iv_voice_emotion);
                holder.mTextView = (TextView) view.findViewById(R.id.tv_voice_emotion_name);
                return holder;
            }

            @Override
            public void onBindViewHolder(EmotionManiFest.Emotion emotion, EmotionViewHolder holder) {
                if (emotion != null && holder != null) {
                    if (Locale.getDefault().toString().contains("CN")) {
                        holder.mTextView.setText(emotion.mark.j);
                    } else {
                        holder.mTextView.setText(emotion.mark.f);
                    }
                    int resId = ApkResourceManager.GetApkImageResource(emotion.type);
                    if (resId > 0) {
                        holder.mImageView.setImageResource(resId);
                    } else {
                        InputStream inputStream = EffectsManager.GetEmotionInputStream(emotion.thumbnail);
                        if (inputStream != null) {
                            holder.mImageView.setImageBitmap(BitmapFactory.decodeStream(inputStream));
                        } else {
                            PicassoUtils.loadImage(mContext, emotion.thumbnail, holder.mImageView);
                        }
                    }
                    holder.mImageView.setOnClickListener((v) -> {
                        if (mOnItemClickedListener != null) {
                            mOnItemClickedListener.onItemClicked(emotion);
                        }
                    });
                }
            }

            @Override
            public void onDestroy() {

            }

            @Override
            public View createViewHolderView() {
                return View.inflate(mContext, R.layout.layout_item_voice_emotion, null);
            }
        });
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        mOnItemClickedListener = onItemClickedListener;
    }

    @Override
    public void destroy() {
        mSafeHandler.release();
    }

    public interface OnItemClickedListener {
        void onItemClicked(EmotionManiFest.Emotion emotion);
    }
}
