package orangelab.project.voice.manager;

import android.text.TextUtils;

import com.R;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.constants.VoiceConstants;

import com.networktoolkit.transport.Constant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 30/10/2017.
 */

public class SmallGameManager implements Destroyable {

    private static final String TAG = "SmallGameManager";

    private static volatile SmallGameManager mInstance;

    private List<SmallGame> smallGameList = new ArrayList<>();

    private SmallGameManager() {
        init();
    }

    public static SmallGameManager getInstance() {
        if (mInstance == null) {
            synchronized (SmallGameManager.class) {
                if (mInstance == null) {
                    mInstance = new SmallGameManager();
                }
            }
        }
        return mInstance;
    }

    public List<SmallGame> getSmallGameList() {
        return smallGameList;
    }

    public void init() {
        smallGameList.add(new SmallGame(VoiceConstants.DICE, MessageUtils.getString(R.string.str_game_dice), R.mipmap.ico_voice_game_touzi, "gif/game/game_dice.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.LIGHT, MessageUtils.getString(R.string.str_game_light), R.mipmap.ico_voice_game_bulb, "gif/game/game_light.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.HAND_UP, MessageUtils.getString(R.string.str_game_hand_up), R.mipmap.ico_voice_game_hand, "gif/game/hand_up.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.FINGER_GUESSING, MessageUtils.getString(R.string.str_game_caiquan), R.mipmap.ico_voice_game_caiquan, "gif/game/caiquan.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.POINTS, MessageUtils.getString(R.string.str_game_score), R.mipmap.ico_voice_game_score, "gif/game/score.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.OCTOPUS, MessageUtils.getString(R.string.str_game_zhangyuji), R.mipmap.ico_voice_game_zhang, "gif/game/zhangyuji.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.COINS, MessageUtils.getString(R.string.str_game_coin), R.mipmap.ico_voice_game_coin, "gif/game/coin.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.MIC, MessageUtils.getString(R.string.str_game_choumic), R.mipmap.ico_voice_game_mic_order, "gif/game/chou_mic.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.GIFTS, MessageUtils.getString(R.string.str_game_get_gift), R.mipmap.ico_voice_game_gift, "gif/game/request_gift.gif"));
        smallGameList.add(new SmallGame(VoiceConstants.APPLAUSE, MessageUtils.getString(R.string.str_game_clap), R.mipmap.ico_voice_game_clap, "gif/game/game_applause.gif"));
    }

    public String findAnimPathByMark(SmallGame smallGame) {
        if (smallGame == null) {
            return "";
        }
        for (SmallGame game : smallGameList) {
            if (TextUtils.equals(smallGame.getMark(), game.getMark())) {
                return game.getPath();
            }
        }
        return "";
    }

    public SmallGame findGameByMark(String mark) {
        if (smallGameList == null || TextUtils.isEmpty(mark)) {
            return null;
        }

        for (SmallGame game : smallGameList) {
            if (TextUtils.equals(game.getMark(), mark)) {
                return game;
            }
        }

        return null;
    }

    public boolean supportGameType(String mark) {
        if (TextUtils.isEmpty(mark)) {
            return false;
        }

        for (SmallGame game : smallGameList) {
            if (TextUtils.equals(game.getMark(), mark)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void destroy() {
        if (smallGameList != null) {
            smallGameList.clear();
        }
        mInstance = null;
    }

    public static class SmallGame implements Serializable, Keepable {

        private String mark;
        private String name;
        private int imageId;//缩略图标
        private String path;//动态GIF本地路径
        private int lastFrame;//最后停留的帧数/结果
        private int delayTime;//结果停留的时间

        public SmallGame() {
        }

        public SmallGame(String name, int imageId) {
            this.mark = name;
            this.imageId = imageId;
        }

        public SmallGame(String mark, int imageId, String path) {
            this.mark = mark;
            this.imageId = imageId;
            this.path = path;
        }

        //本地使用
        public SmallGame(String mark, String name, int imageId, String path) {
            this.mark = mark;
            this.name = name;
            this.imageId = imageId;
            this.path = path;
        }

        //server数据
        public SmallGame(String mark, int lastFrame, int delayTime) {
            this.mark = mark;
            this.lastFrame = lastFrame;
            this.delayTime = delayTime;
        }

        public String getMark() {
            return mark;
        }

        public void setMark(String mark) {
            this.mark = mark;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getImageId() {
            return imageId;
        }

        public void setImageId(int imageId) {
            this.imageId = imageId;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getLastFrame() {
            return lastFrame;
        }

        public void setLastFrame(int lastFrame) {
            this.lastFrame = lastFrame;
        }

        public int getDelayTime() {
            return delayTime;
        }

        public void setDelayTime(int delayTime) {
            this.delayTime = delayTime;
        }
    }
}
