package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lloydfinch on 01/12/2017.
 */

public class VoiceSpyVoteResult implements Serializable, Keepable {

    public String type;//no egg use
    public HashMap<Integer, List<Integer>> vote_info;
    public int duration = -1;//最终结果才会有
    public boolean need_pk;//是否进入PK阶段
    public List<Integer> positions;//票死的人
    public boolean finished;//投票结束

    @Override
    public String toString() {
        return "VoiceSpyVoteResult{" +
                "type='" + type + '\'' +
                ", vote_info=" + vote_info +
                ", duration=" + duration +
                ", need_pk=" + need_pk +
                ", positions=" + positions +
                ", finished=" + finished +
                '}';
    }
}
