package orangelab.project.voice.privateroom.holder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.R
import com.androidtoolkit.view.ScreenUtils
import orangelab.project.voice.privateroom.PrivateRoomTools
import orangelab.project.voice.privateroom.model.ChatMessageItem
import de.hdodenhof.circleimageview.CircleImageView
import me.drakeet.multitype.ItemViewBinder
import orangelab.project.common.effect.msgdecor.MsgDecorToolKit
import orangelab.project.common.view.VipNameView

/**
 * game_werewolf
 * 2018/3/21 下午6:35
 * Mystery
 */

class ChatMsgItemViewBinder : ItemViewBinder<ChatMessageItem, ChatMsgItemViewBinder.ChatMsgItemViewHolder>() {

    companion object {
        val PADDING = ScreenUtils.dip2px(4f)
    }

    class ChatMsgItemViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var message: TextView
        var position: CircleImageView
        var name: VipNameView

        init {
            message = itemView!!.findViewById(R.id.tv_item_msg) as TextView
            name = itemView!!.findViewById(R.id.tv_item_name) as VipNameView
            position = itemView!!.findViewById(R.id.iv_item_position) as CircleImageView
        }
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ChatMsgItemViewHolder {
        val view = inflater.inflate(R.layout.layout_private_room_chat_msg, parent, false)
        return ChatMsgItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatMsgItemViewHolder, item: ChatMessageItem) {
        holder.name.setText(item.name)
        holder.message.setText(item.message)
        holder.position.setImageResource(PrivateRoomTools.MessageItemPositionIv(item.position))
        if (item.textColorResource != -1) {
            holder.message.setTextColor(holder.message.context.resources.getColor(item.textColorResource))
        }
        holder.message.setPadding(PADDING, PADDING, PADDING, PADDING)
        holder.message.setBackgroundResource(R.drawable.bg_msg)
        MsgDecorToolKit.setNinePatchForViewBackGround(holder.message, holder.message, item.msgDecor)
        holder.name.handleVipInfo(item.vipInfo)
    }
}
