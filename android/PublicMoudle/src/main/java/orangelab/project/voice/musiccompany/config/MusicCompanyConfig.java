package orangelab.project.voice.musiccompany.config;

import android.os.Environment;
import android.text.TextUtils;

import com.BuildConfig;
import com.R;
import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.ProjectConfig;
import orangelab.project.common.PersonalDataHelper;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.musiccompany.model.MusicOrderSong;

/**
 * 音乐伴奏配置信息
 */
public class MusicCompanyConfig implements Keepable {

    public static final int PAGE_STEP = 10; //搜索音乐默认每页的长度

    public static final String ORDERED_PATH = Environment.getExternalStorageDirectory() + "/orange_lab/werewolf"; //已点歌曲在本地的存储列表

    public static final List<String> SUPPORT_TYPES = new ArrayList<>(); //支持的音乐类型列表

    public static boolean isRecord = false;
    public static int mic = 120;                    //麦克风音量大小
    public final static int micMax = 150;           //麦克风音量最大值
    public static final int micMin = 90;            //麦克风最小声音
    public static int voice = 120;                  //音量大小 0-150(目前用不到)
    public final static int speakerMax = 150;       //扬声器音量最大值
    public static int companyVolume = 160;          //伴奏音量
    public static int companyVolumeMax = 200;       //伴奏音量最大值
    public static final int companyMin = 120;       //伴奏最小声音
    public static int voiceQuality;                 //声音质量
    public static boolean isPrepare = false;        //是否在准备
    public static boolean isSing = false;           //是否正在唱歌

    public static String authorizedID = "";         //能唱歌的人的id
    public static String singerId = "";             //唱歌人的id

    public static String lyricUrl = "";             //即将播放的歌词地址
    public static String playUrl = "";              //即将播放的歌曲地址
    public static String albumUrl = "";             //即将播放的专辑图片地址
    public static String musicId = "";              //即将播放的音乐的id
    public static String orderId = "";              //即将播放的歌曲点歌人的id
    public static String songName = "";             //即将播放的歌曲名字
    public static String orderName = "";            //即将唱歌的人的名字
    public static String orderImg = "";             //即将唱歌的人的头像
    public static boolean isPause = false;          //是否暂停状态
    public static long startTime;                   //音乐开始播放的时间
    public static long timeLength;                  //音乐时长

    public static long enterTime = 0;               //enter room的时间

    //tencent sdk使用
    public static final String AppID = ProjectConfig.KTVAppId;          //app id
    public static final String key = ProjectConfig.KTVAppKey;           //认证密钥
    public static final String AccountType = ProjectConfig.KTVAppKey;   //
    public static String appVersion = BuildConfig.VERSION_NAME; //appversion查log和bug使用
    public static String roomID = "";   //伴奏房间号，取当前房间号
    public static final String roomType = "3";       //房间音频类型 1流畅 2标准 3高清
    public static String strMaxMixCount = "3"; //最大混音数
    public static final String strSpeakerType = "2"; //当前扬声器类型  1 通话 2 媒体
    public static final String strTestEnv = "0";     //环境变量 0 正常环境 1 测试环境

    public static MusicOrderSong currentSong = null;
    public static MusicOrderSong nextSong = null;

    //认证信息
    public static String getIdentifier() {
        PersonalData personalData = PersonalDataHelper.getPersonalData();
        if (personalData != null) {
            int uid = personalData.getUid();
            if (uid > 0) {
                return uid + "";
            }
        }
        return System.currentTimeMillis() % 1000000 + "";
    }

    static {
        SUPPORT_TYPES.add(".mp3"); //默认支持mp3文件
    }

    /**
     * 添加支持的音乐类型
     */
    public static void addSupportType(String type) {
        SUPPORT_TYPES.add(type);
    }

    public static String getTitle() {
        //默认为没有歌曲
        String title = MessageUtils.getString(R.string.str_music_player_title_no_sing_no_next);
        if (isSing) {
            //当前有唱歌，有下一首
            if (currentSong != null && nextSong != null) {
                title = MessageUtils.getString(R.string.str_music_player_title_sing_has_next, currentSong.order, currentSong.songName, nextSong.order, nextSong.songName);
            }
            //当前有唱歌，没下一首
            if (currentSong != null && nextSong == null) {
                title = MessageUtils.getString(R.string.str_music_player_title_sing_no_next, currentSong.order, currentSong.songName);
            }
        } else {
            //当前没唱歌，有下一首
            if (nextSong != null) {
                title = MessageUtils.getString(R.string.str_music_player_title_no_sing_has_next, nextSong.order, nextSong.songName);
            }
            //当前没唱歌，没下一首
            if (nextSong == null) {
                title = MessageUtils.getString(R.string.str_music_player_title_no_sing_no_next);
            }
        }

        return title;
    }

    /**
     * 获取最大可点歌数量
     */
    public static int maxSongMenuCapacity() {
        int number = 30;
        VoiceTopicManager.Params topic = VoiceTopicManager.getInstance().getTopic();
        if (topic != null) {
            if (topic.ktv != null) {
                number = topic.ktv.max_count;
            }
        }

        return number;
    }

    /**
     * 每人最大可点歌数量
     */
    public static int maxOrderSongCapacity() {
        int number = 5;
        VoiceTopicManager.Params topic = VoiceTopicManager.getInstance().getTopic();
        if (topic != null) {
            if (topic.ktv != null) {
                number = topic.ktv.per_user_count;
            }
        }

        return number;
    }

    /**
     * 获取已经播放过的时间
     */
    public static long passTime() {
        long time = enterTime - startTime;
        return time > 0 ? time : 0;
    }

    /**
     * 是否已经授权，能唱歌
     */
    public static boolean hasAuthorized() {
        return TextUtils.equals(authorizedID, orderId);
    }

    /**
     * 清除本次播放的音乐信息
     */
    public static void clearMusicInfo() {
        isSing = false;
        isRecord = false;
        singerId = "";
        clearMusicLenInfo();
        nextSong = null;
    }

    public static void clearMusicLenInfo() {
        playUrl = "";
        lyricUrl = "";
        albumUrl = "";
        musicId = "";
        singerId = "";
        orderId = "";
        currentSong = null;
        isPause = false;
        isPrepare = false;
        songName = "";
        orderName = "";
        authorizedID = "";
        orderImg = "";
        startTime = 0;
        timeLength = 0;
        isSing = false;
        enterTime = 0;
    }

    public static void clearPrepareMusic() {
        playUrl = "";
        lyricUrl = "";
        albumUrl = "";
        musicId = "";
        orderId = "";
        isPrepare = false;
        songName = "";
        orderName = "";
        authorizedID = "";
        orderImg = "";
        startTime = 0;
        timeLength = 0;
        enterTime = 0;
    }

    public static void destroy() {
        clearMusicInfo();
    }
}
