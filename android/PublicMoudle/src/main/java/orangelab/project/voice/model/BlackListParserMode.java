package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * Created by lloydfinch on 10/11/2017.
 */

public class BlackListParserMode implements Keepable {
    public List<VoiceBlackListMode> users;

    @Override
    public String toString() {
        return "BlackListParserMode{" +
                "users=" + users +
                '}';
    }
}
