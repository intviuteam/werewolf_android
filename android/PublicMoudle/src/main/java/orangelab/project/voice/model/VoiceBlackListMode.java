package orangelab.project.voice.model;

import com.toolkit.action.Keepable;

/**
 * Created by lloydfinch on 06/11/2017.
 */

public class VoiceBlackListMode implements Keepable {
    public String id;
    public String name;
    public int sex;//1男 2女
    public String image;

    @Override
    public String toString() {
        return "VoiceBlackListMode{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", image='" + image + '\'' +
                '}';
    }
}
