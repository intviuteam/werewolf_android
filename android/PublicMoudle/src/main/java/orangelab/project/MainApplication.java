package orangelab.project;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.ApkToolKit;
import com.androidtoolkit.Environment;
import com.androidtoolkit.PLog;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.feedback.FeedbackAgent;
import com.avos.avoscloud.im.v2.AVIMOptions;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.GlobalDecorateManager;
import com.networktoolkit.transport.RequestTaskUtils;
import com.squareup.leakcanary.LeakCanary;
import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;
import com.toolkit.action.TravelMap;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.zxy.tiny.Tiny;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.intviu.orbit.manager.OrbitManager;
import cn.intviu.orbit.manager.OrbitManagerConfig;
import cn.intviu.support.IntviuUncaughtExceptionHandler;
import cn.intviu.support.ReportRobot;
import io.realm.react.RealmReactPackage;
import orangelab.project.common.bridge.RNBaseApplication;
import orangelab.project.common.bridge.RNFetchBlob.RNFetchBlobPackage;
import orangelab.project.common.bridge.blurview.BlurViewPackage;
import orangelab.project.common.bridge.channel.GameReactPackage;
import orangelab.project.common.bridge.channel.NativeJSModule;
import orangelab.project.common.bridge.channel.WeChatPackage;
import orangelab.project.common.bridge.deviceInfo.RNDeviceInfo;
import orangelab.project.common.bridge.headdecorview.HeadDecorViewPackage;
import orangelab.project.common.bridge.imageResizer.ImageResizerPackage;
import orangelab.project.common.bridge.imagepicker.PickerModule;
import orangelab.project.common.bridge.imagepicker.PickerPackage;
import orangelab.project.common.bridge.minigame.MiniGameDownloadViewPackage;
import orangelab.project.common.bridge.qqLogin.QQPackage;
import orangelab.project.common.bridge.rnfs.RNFSPackage;
import orangelab.project.common.bridge.splashscreen.SplashScreenReactPackage;
import orangelab.project.common.db.DBConstant;
import orangelab.project.common.db.DaoHelper;
import orangelab.project.common.db.GooglePaySubsOrderAssitHelper;
import orangelab.project.common.db.MiniGameAIRecordDaoHelper;
import orangelab.project.common.db.MiniGameLaunchRecordDaoHelper;
import orangelab.project.common.db.MiniGameRecordDaoHelper;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;
import orangelab.project.common.engine.MiniGameSocketEngine;
import orangelab.project.common.engine.RoomSocketEngine;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.listener.SocketStatusRecorder;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.share.PublicShareManager;
import orangelab.project.common.system.SystemController;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;
import orangelab.thirdparty.leancloud.chatkit.LCChatKit;
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;

public class MainApplication extends RNBaseApplication {

    private static final String TAG = "MainApplication";
    private OrbitManager orbitManager;

    private final GameReactNativeHost mReactNativeHost = new GameReactNativeHost(this);
    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    public static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    public static class GameReactNativeHost extends ReactNativeHost {
        List<ReactPackage> packages = new ArrayList<>();

        protected GameReactNativeHost(Application application) {
            super(application);
            packages.add(new MainReactPackage());
            packages.add(new GameReactPackage());
            packages.add(new PickerPackage());
            packages.add(new WeChatPackage());
            packages.add(new QQPackage());
            packages.add(new SplashScreenReactPackage());
            packages.add(new RNFetchBlobPackage());
            packages.add(new ImageResizerPackage());
            packages.add(new RealmReactPackage());
            packages.add(new BlurViewPackage());
            packages.add(new RNDeviceInfo());
            packages.add(new HeadDecorViewPackage());
            packages.add(new FBSDKPackage(mCallbackManager));
            packages.add(new RNFSPackage());
            packages.add(new MiniGameDownloadViewPackage());
        }

        @Override
        public boolean getUseDeveloperSupport() {
            return ProjectConfig.DEBUG_ABLE;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return packages;
        }

        /**
         * 由于GamePackage已经确定是第2个，所以直接强转
         *
         * @return
         */
        public GameReactPackage getGamePackage() {
            return (GameReactPackage) packages.get(1);
        }

        /**
         * TODO wangxu 使用到图片选取的库
         *
         * @return
         */
        public PickerPackage getPickerPackage() {
            return (PickerPackage) packages.get(2);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "onConfigurationChanged: ");
    }

    public NativeJSModule getNativeJSModule() {
        if (mReactNativeHost != null) {
            return mReactNativeHost.getGamePackage().getNativeJSModule();
        }
        return null;
    }

    public PickerModule getPickerModule() {
        if (mReactNativeHost != null) {
            return mReactNativeHost.getPickerPackage().getPickerModule();
        }
        return null;
    }

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }


    private RoomSocketEngine helper;
    private MiniGameSocketEngine mMiniGameHelper;
    private DaoHelper mDaoHelper;
    private Map<String, Destroyable> mDBHelperMap = new HashMap<>();


//    private MiniGameRecordDaoHelper mMiniGameRecordDaoHelper;
//    private MiniGameAIRecordDaoHelper mMiniGameAIRecordDaoHelper;
//    private MiniGameLaunchRecordDaoHelper mMiniGameLaunchRecordDaoHelper;
//    private UserPayOrderDaoV2Helper mUserPayOrderDaoV2Helper;


    protected void onPreInitProjectConfig() {

    }

    protected void onAfterApplicationCreate() {

    }

    @Override
    public void onCreate() {
        onPreInitProjectConfig();
        super.onCreate();

        /**
         * 初始化 微信和QQ分享功能
         */
        //微信 appID appSecret
        PlatformConfig.setWeixin(ProjectConfig.WECHAT_APPID, ProjectConfig.WECHAT_APPKEY);
        //QQ appID  appKey
        PlatformConfig.setQQZone(ProjectConfig.QQ_APPID, ProjectConfig.QQ_APPKEY);

        /**
         * 初始化一些基本的应用数据
         */
        {
            PLog.needLog = ProjectConfig.DEBUG_ABLE;
            Environment.SetUpContext(this);
            RequestTaskUtils.initAppType(ProjectConfig.APP_TYPE);
            Constant.setHttpUrl(ProjectConfig.APP_API);
            // TODO: 2017/10/14 这是一个检测内存泄漏的工具,在打线上包的时候，可以去掉这个代码
            if (PLog.needLog && ProjectConfig.DEBUG_ABLE) {
                LeakCanary.install(this);
                SocketStatusRecorder.init();
            }
        }

        Constant.init(this);
        mainApplication = this;
        //facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        SystemController.bindApplicationContext(getApplicationContext());
        ApkToolKit.Bind(this);
        // If you want to use AppEventsLogger to log events.
        AppEventsLogger.activateApp(this);
        Constant.init();
        ToastToolKit.bind(this);
        ScreenUtils.initialization(this);
        MessageUtils.setBindContext(this);
        NotificationUtils.IMPL();

        /**
         * 初始化头部信息
         */
        GlobalDecorateManager.INSTANCE.setHeaderDecorate(new HeaderDecorate());

        initQinNiu();
        //初始化leancloud
        initLeanCloud();
        //初始化友盟分享
        initUmengShare();
        //友盟崩溃日志上报
        setUncaughtExceptionHandler();
        initRobot();
        //图片处理
        Tiny.getInstance().debug(false).init(this);
        orbitManager = OrbitManager.getInstance();
        OrbitManagerConfig config = new OrbitManagerConfig();
        config.setVideoEnable(false);
        orbitManager.initGlobal(this, config);

        BlackListManager.Init(this);

        EffectsManager.Init(this);


        helper = new RoomSocketEngine();
        mMiniGameHelper = new MiniGameSocketEngine();

        initDBHelper();

        //加载分享内容
        PublicShareManager.INSTANCE.initShareContent(Constant.PUBLIC_SHARE_CASE);
        onAfterApplicationCreate();

        //加载表情,此版本不加(2018-07-25)
        //VoiceEmotionManager.getInstance().syncEmotions();

    }

//    private void initCache() {
//        /**
//         * 从 Picasso 中 copy 出来的算法 为了让位给Picasso主要的缓存，这里将使用Picasso使用的缓存大小的 1／10
//         */
//        int size = calculateMemoryCacheSize(this) / 10;
//        mHeadDecorateLruCache = new LruCache(size);
//        PLog.i(TAG, "create HeadDecorateLruCache [" + size + "]");
//    }

    private void initDBHelper() {
        /**
         * 初始化数据库帮助类
         */
        mDaoHelper = new DaoHelper(this);
        /**
         * 小游戏成绩记录数据库
         */
//        mMiniGameRecordDaoHelper = new MiniGameRecordDaoHelper(mDaoHelper.getMiniGameEntityDao());
        mDBHelperMap.put(DBConstant.MINI_GAME_RECORD_HELPER, new MiniGameRecordDaoHelper(mDaoHelper.getMiniGameEntityDao()));
        /**
         * 小游戏单机AI记录
         */
//        mMiniGameAIRecordDaoHelper = new MiniGameAIRecordDaoHelper(mDaoHelper.getMiniGameAIEntityDao());
        mDBHelperMap.put(DBConstant.MINI_GAME_AI_RECORD_HELPER, new MiniGameAIRecordDaoHelper(mDaoHelper.getMiniGameAIEntityDao()));
        /**
         * 小游戏单机Launch记录
         */
//        mMiniGameLaunchRecordDaoHelper = new MiniGameLaunchRecordDaoHelper(mDaoHelper.getMiniGameLaunchRecord());
        mDBHelperMap.put(DBConstant.MINI_GAME_LAUNCH_HELPER, new MiniGameLaunchRecordDaoHelper(mDaoHelper.getMiniGameLaunchRecord()));
        /**
         * 支付数据库V2
         */
//        mUserPayOrderDaoV2Helper = new UserPayOrderDaoV2Helper(mDaoHelper.getUserPayOrderEntity());
        mDBHelperMap.put(DBConstant.PAY_ORDER_RECORD_HELPER, new UserPayOrderDaoV2Helper(mDaoHelper.getUserPayOrderEntity()));

        /**
         * 订阅订单辅助数据库
         */
        mDBHelperMap.put(DBConstant.PAY_SUBS_OERDER_ASSIT_HELPER, new GooglePaySubsOrderAssitHelper(mDaoHelper.getGoogleSubsOrderAssitEntity()));
    }

    private void initQinNiu() {

    }

    private void initRobot() {
        try {
            if (Build.CPU_ABI.contains("86")) {
                ReportRobot.robot.reportAudioErrorX86();
            }
        } catch (Exception e) {
            /**
             * ignore
             */
        }
    }

    private void initUmengShare() {
        //初始化umneng分享的API
        UMShareAPI.get(this);
        //umeng分享的debug模式
        Config.DEBUG = false;
        ReportEventUtils.bindContext(this);
    }

    private void initLeanCloud() {

        AVOSCloud.setDebugLogEnabled(PLog.needLog);
        AVOSCloud.setLogLevel(AVOSCloud.LOG_LEVEL_VERBOSE);
        LCChatKit.getInstance().setProfileProvider((userIdList, profilesCallBack) -> {
            PLog.i(TAG, "ProfileProvider");
        });

        String userUsCloud = "";

        if (ProjectConfig.LEANCLOUD_SUPPORT_CUSTOM_SERVER) {
            /**
             * 根据配置文件判断是否需要配置Api 与 Rtm
             */
            String api = Utils.getLeanCloudServerApi(this);
            String rtm = Utils.getLeanCloudServerRtm(this);

            PLog.i(TAG, "cur leancloud config-> api:" + api + ",rtm:" + rtm);

            if (TextUtils.equals(api, Constant.LEANCLOUD_SERVER_NONE)) {
                /**
                 * 此时还没有配置过api
                 */
                api = Constant.LEANCLOUD_SERVER_API_DEFAULT;
                Utils.saveLeanCloudServerApi(this, api);
            }


            if (TextUtils.equals(rtm, Constant.LEANCLOUD_SERVER_NONE)) {
                /**
                 * 此时还没有配置过rtm
                 */
                rtm = Constant.LEANCLOUD_SERVER_RTM_DEFAULT;
                Utils.saveLeanCloudServerRtm(this, rtm);
            }

            PLog.i(TAG, "cur leancloud config-> api:" + api + ",rtm:" + rtm);
            if (!TextUtils.isEmpty(api)) {
                AVOSCloud.setServer(AVOSCloud.SERVER_TYPE.API, api);
                PLog.i(TAG, "leancoud api config set finish");
            }

            if (!TextUtils.isEmpty(rtm)) {
                AVIMOptions.getGlobalOptions().setRTMServer(rtm);
                PLog.i(TAG, "leancoud rtm config set finish");
            }
        } else {
            /**
             * do nothing
             */
        }

        /**
         * 根据配置文件判断是否使用美国节点
         */
        userUsCloud = Utils.getLeanCloudNode(this);
        if (TextUtils.isEmpty(userUsCloud)) {
            /**
             * 如果这个此时得到的数据为空，那么说明此次应用还没有在配置文件中配置，那么基于一个CN的默认节点
             */
            Utils.saveLeanCloudNode(this, Constant.LEANCLOUD_CN);
            userUsCloud = Constant.LEANCLOUD_CN;
        }
        boolean isUs = false;
        if (TextUtils.equals(userUsCloud, Constant.LEANCLOUD_US)) {
            isUs = true;
        }
        if (isUs) {

            LCChatKit.getInstance().init(getApplicationContext(), ProjectConfig.LEANCLOUD_US_APPID, ProjectConfig.LEANCLOUD_US_APPKEY, isUs);
        } else {

            LCChatKit.getInstance().init(getApplicationContext(), ProjectConfig.LEANCLOUD_APPID, ProjectConfig.LEANCLOUD_APPKEY, isUs);
        }
        FeedbackAgent.setPushEnabled(false);
    }

    private void setUncaughtExceptionHandler() {
        Thread.UncaughtExceptionHandler oldHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (oldHandler instanceof IntviuUncaughtExceptionHandler) {
            oldHandler = null;
        }
        Thread.UncaughtExceptionHandler handler = new ReportableUncaughtExceptionHandler(oldHandler);
        Thread.setDefaultUncaughtExceptionHandler(handler);
        Thread.currentThread().setUncaughtExceptionHandler(handler);
    }

    private class ReportableUncaughtExceptionHandler extends IntviuUncaughtExceptionHandler {

        public ReportableUncaughtExceptionHandler(Thread.UncaughtExceptionHandler handler) {
            super(handler);
        }

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            MobclickAgent.reportError(MainApplication.this, ex);
            super.uncaughtException(thread, ex);
        }
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public RoomSocketEngine getSocketEngineHelper() {
        return helper;
    }

    public MiniGameSocketEngine getMiniGameSocketEngineHelper() {
        return mMiniGameHelper;
    }

    public boolean socketIsExit() {
        boolean result = false;
        if (helper != null) {
            if (helper.getEngine() != null) {
                result = true;
            }
        }
        return result;
    }

    private static MainApplication mainApplication;

    public static MainApplication getInstance() {
        return mainApplication;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i(TAG, "onTerminate: ");
        ToolKit.travelMap(mDBHelperMap, (key, value) -> {
            value.destroy();
        });
        if (mDaoHelper != null) {
            mDaoHelper.destroy();
        }
    }

    public MiniGameRecordDaoHelper getMiniGameRecordDaoHelper() {

        try {
            return (MiniGameRecordDaoHelper) mDBHelperMap.get(DBConstant.MINI_GAME_RECORD_HELPER);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public MiniGameAIRecordDaoHelper getMiniGameAIRecordDaoHelper() {
        try {
            return (MiniGameAIRecordDaoHelper) mDBHelperMap.get(DBConstant.MINI_GAME_AI_RECORD_HELPER);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public MiniGameLaunchRecordDaoHelper getMiniGameLaunchRecordDaoHelper() {
        try {
            return (MiniGameLaunchRecordDaoHelper) mDBHelperMap.get(DBConstant.MINI_GAME_LAUNCH_HELPER);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public UserPayOrderDaoV2Helper getUserPayOrderDaoV2Helper() {
        try {
            return (UserPayOrderDaoV2Helper) mDBHelperMap.get(DBConstant.PAY_ORDER_RECORD_HELPER);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public GooglePaySubsOrderAssitHelper getGooglePaySubsOrderAssitHelper() {
        try {
            return (GooglePaySubsOrderAssitHelper) mDBHelperMap.get(DBConstant.PAY_SUBS_OERDER_ASSIT_HELPER);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    /**
//     * 计算内存缓存可用空间
//     *
//     * @param context
//     * @return
//     */
//    public static int calculateMemoryCacheSize(Context context) {
//        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
//        boolean largeHeap = (context.getApplicationInfo().flags & FLAG_LARGE_HEAP) != 0;
//        int memoryClass = largeHeap ? am.getLargeMemoryClass() : am.getMemoryClass();
//        // Target ~15% of the available heap.
//        return (int) (1024L * 1024L * memoryClass / 7);
//    }
}
