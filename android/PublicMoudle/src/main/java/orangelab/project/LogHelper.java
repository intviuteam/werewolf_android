package orangelab.project;

import com.androidtoolkit.PLog;

/**
 * game_werewolf
 * 2018/5/30 上午10:24
 * Mystery
 */
public class LogHelper {


    private static final String MINI_GAME_LOG_TAG = "MiNi_Game";

    public static void miniGameLog(String msg) {
        PLog.i(MINI_GAME_LOG_TAG, msg);
    }
}
