package orangelab.project.game.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.R;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.voice.gif.BasicComponent;

/**
 * Created by lloydfinch on 02/02/2018.
 * <p>
 * //游戏房顶部在线人数的view
 */

public class WereWolfObserverView extends BasicComponent {

    private View contentView;
    private ImageView ivLeft;
    private ImageView ivRight;
    private TextView tvOnlineNumber;//在线人数
    private TextView tvObserveNumber;//观战人数

    private int onLineNumber;//在线人数
    private int observeNumber;//观战人数

    public WereWolfObserverView(Context context, View view) {
        this.contentView = view;
        init();
    }

    private void init() {
        initView();
    }

    private void initView() {

        ivLeft = (ImageView) contentView.findViewById(R.id.iv_left);
        ivRight = (ImageView) contentView.findViewById(R.id.iv_right);
        tvOnlineNumber = (TextView) contentView.findViewById(R.id.tv_num_online);
        tvObserveNumber = (TextView) contentView.findViewById(R.id.tv_num_observe);
    }

    //设置在线人数
    public void setOnLineNumber(int number) {
        onLineNumber = number;
        updateOnLineNumber();
    }

    //设置观战人数
    public void setObserveNumber(int number) {
        observeNumber = number;
        updateObserveNumber();
    }

    //在线人数 +1
    public void addOnlineNumber() {
        onLineNumber++;
        updateOnLineNumber();
    }

    //在线人数 -1
    public void subOnLineNumber() {
        onLineNumber--;
        updateOnLineNumber();
    }

    //观战人数 +1
    public void addObserveNumber() {
        observeNumber++;
        updateObserveNumber();
    }

    //观战人数 -1
    public void subObserveNumber() {
        observeNumber--;
        updateObserveNumber();
    }

    //获取在线人数
    public int getOnLineNumber() {
        return onLineNumber;
    }

    //获取观战人数
    public int getObserveNumber() {
        return observeNumber;
    }

    private void updateOnLineNumber() {
        tvOnlineNumber.setText(MessageUtils.getString(R.string.string_game_online_number, String.valueOf(onLineNumber)));

    }

    private void updateObserveNumber() {
        tvObserveNumber.setText(MessageUtils.getString(R.string.string_game_observe_number, String.valueOf(observeNumber)));
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        contentView.setOnClickListener(onClickListener);
    }
}
