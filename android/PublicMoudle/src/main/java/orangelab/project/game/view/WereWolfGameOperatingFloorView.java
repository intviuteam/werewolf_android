package orangelab.project.game.view;

import android.content.Context;
import android.graphics.PointF;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.R;
import com.androidtoolkit.AndroidToolKit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Filter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.ProjectConfig;
import orangelab.project.common.adapter.ChooseTypeAdapter;
import orangelab.project.common.dialog.DebugDialog;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.event.DebugEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.exhibition.card.IWereWolfCardService;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.ActionsDetector;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.WereWolfGameProcessLifeCycle;
import orangelab.project.game.dialog.WereWolfGamePresentGiftDialog;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.game.model.WereWolfObserverMember;

/**
 * game_werewolf
 * 2017/12/3 上午10:33
 * Mystery
 */

public class WereWolfGameOperatingFloorView extends WereWolfGameProcess implements Destroyable, ChooseTypeAdapter.OnChooseTypeListener, View.OnClickListener, View.OnTouchListener, TextWatcher,
        WereWolfGameProcessLifeCycle {

    private static final String TAG = "WereWolfConsoleView";

    public class ViewGlobalObserver implements ViewTreeObserver.OnGlobalLayoutListener {

        private View mView;
        private PointF mLastPoint;

        public ViewGlobalObserver(View view) {
            mView = view;
            mLastPoint = AndroidToolKit.getPointByView(mView);
        }

        public void setUp() {
            if (mView != null) {
                mView.getViewTreeObserver().addOnGlobalLayoutListener(this);
            }
        }

        public void destroy() {
            if (mView != null) {
                mView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }

        @Override
        public void onGlobalLayout() {
            PointF tempPoint = AndroidToolKit.getPointByView(mView);
            if (tempPoint != null) {
                if (tempPoint.equals(mLastPoint)) {
                    PLog.i(TAG, "point is equals");
                } else {
                    PLog.i(TAG, "point is not equals");
                    /**
                     * 判断Y值是否大于一个定值
                     */
//                    if (tempPoint.y - mLastPoint.y >= 300) {
//                        PLog.i(TAG, "键盘收起来");
//                    } else {
//                        PLog.i(TAG, "键盘弹出来");
//                    }
                    RxToolKit.Emit(new CardEvent.WereWolfKeyBoardChangeEvent(AndroidToolKit.getPointByView(mSpeakView)));
                }
                mLastPoint = tempPoint;
            }
        }
    }

    private WereWolfGameContext mWolfGameContext;
    private View mRootView;

    private Button mActionLeft;
    private Button mActionRight;
    private Button mActionMid;
    private ImageView mSendGift;
    private ImageView mSwitchButton;

    private Button mSendMessage;


    private Button mPrepareButton;
    private Button mMuteButton;
    private Button mQuitElectionButton;

    private View mSpeakView;
    private TextView mSpeakViewText;
    private ImageView mSpeakViewIco;

    private EditText mTypeWritingView;
    private ChooseTypeButton mChooseTypeButton;

    private Button btnUpSeat;//上座
    private Button btnToObserve;//去观战

    private boolean mSwitchType = false; //false is use audio, true is use typewriting
    private boolean mIsSendMuteAll = false;
    private boolean mCanSpeakInNight = false;
    private boolean mIsActionDown = false;
    private PointF mSpeakPointF = null;
    private Filter mSpeakEnableFilter = () -> {
        /**
         * if we are not in Gaming,we can speak
         */
        if (mWolfGameContext.getCurSpeechPosition() == -1 && !mWolfGameContext.isGaming()) {
            return true;
        }
        if (mCanSpeakInNight) {
            return true;
        }
        if (mWolfGameContext.isGaming()) {
            if (mWolfGameContext.getCurSpeechPosition() == mWolfGameContext.getPersonalPosition()) {
                return true;
            } else {
                try {
                    mWolfGameContext.check().post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.can_cut_speaker_txt)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }
        return true;
    };

    private boolean mHasJoinElection = false;
    private DebugDialog debugDialog;

    private IWereWolfCardService mWereWolfCardService = null;

    private Runnable canSpeakDismissTask = () -> {
        mCanSpeakInNight = false;
        turnIntoGaming();
        if (mWolfGameContext.isSunSet()) {
            turnIntoSunSet();
        }
        endSpeak(MotionEvent.ACTION_UP);
    };

    private ViewGlobalObserver mViewGlobalObserver;

    public WereWolfGameOperatingFloorView(WereWolfGameContext wolfGameContext) {
        /**
         * 需要在构造函数中注册 LifeCycle listener
         */
        mWolfGameContext = wolfGameContext;
        mWolfGameContext.addProcessLifeCycle(this);

        mRootView = mWolfGameContext.getGameRootView().findViewById(R.id.id_werewolf_game_bottom_container);

        mActionLeft = (Button) mRootView.findViewById(R.id.id_werewolf_action_left);

        mActionMid = (Button) mRootView.findViewById(R.id.id_werewolf_action_mid);

        mActionRight = (Button) mRootView.findViewById(R.id.id_werewolf_action_right);

        mChooseTypeButton = (ChooseTypeButton) mRootView.findViewById(R.id.id_werewolf_choose_mode);


        List<ChooseTypeButton.PopItem> popItems = new ArrayList<>();
        popItems.add(new ChooseTypeButton.PopItem(6, MessageUtils.getString(R.string.text_game_type_6)));
        popItems.add(new ChooseTypeButton.PopItem(7, MessageUtils.getString(R.string.text_game_type_7)));
        popItems.add(new ChooseTypeButton.PopItem(8, MessageUtils.getString(R.string.text_game_type_8)));
        popItems.add(new ChooseTypeButton.PopItem(9, MessageUtils.getString(R.string.text_game_type_9)));
        popItems.add(new ChooseTypeButton.PopItem(10, MessageUtils.getString(R.string.text_game_type_10)));
        mChooseTypeButton.setPopItems(popItems);
        mChooseTypeButton.setOnChooseTypeListener(this);

        mSendGift = (ImageView) mRootView.findViewById(R.id.id_werewolf_game_sendgift);
        mSendGift.setOnClickListener(this);

        mSendMessage = (Button) mRootView.findViewById(R.id.id_werewolf_game_send_message);
        mSendMessage.setOnClickListener(this);

        mSwitchButton = (ImageView) mRootView.findViewById(R.id.id_werewolf_game_switch);
        mSwitchButton.setOnClickListener(this);

        mTypeWritingView = (EditText) mRootView.findViewById(R.id.id_werewolf_game_tiping);

        mSpeakView = mRootView.findViewById(R.id.id_werewolf_game_speak);
        mSpeakView.setOnTouchListener(this);

        mSpeakViewText = (TextView) mRootView.findViewById(R.id.id_werewolf_game_speak_text);
        mSpeakViewIco = (ImageView) mRootView.findViewById(R.id.id_werewolf_game_speak_ico);

        //2018-02-05 wwq 上座、去观战按钮 start//
        btnUpSeat = (Button) mRootView.findViewById(R.id.btn_up_seat);
        initUpSeatEvent();
        btnToObserve = (Button) mRootView.findViewById(R.id.btn_to_observe);
        initToObserveEvent();
        //2018-02-05 wwq 上座、去观战按钮 end//

        debugDialog = new DebugDialog(wolfGameContext.getAndroidContext());

        mViewGlobalObserver = new ViewGlobalObserver(mSwitchButton);
        mViewGlobalObserver.setUp();

        if (mWolfGameContext.isObserver()) {
            mWolfGameContext.registerProcessListener(mObserverProcess);
            turnIntoObserverMode();
        } else {
            mWolfGameContext.registerProcessListener(this);
            init();
            initListener();
        }

    }

    private void turnIntoObserverMode() {

        //wwq 2018-02-05 自己是观战模式,隐藏掉"去观战"、显示"上座位"
        btnToObserve.setVisibility(View.GONE);
        showUpSeatButton();

        mSwitchType = true;
        updateSwitchView(mSwitchType, false);

        mSendGift.setVisibility(View.GONE);
        mActionLeft.setVisibility(View.GONE);
        mActionMid.setVisibility(View.GONE);
        mActionRight.setVisibility(View.GONE);
    }

    //wwq 2018-02-05 添加"去观战"、"上座"按钮的显示 start//
    //显示"上座"按钮
    private void showUpSeatButton() {
        if (canUpSeat()) {
            btnUpSeat.setVisibility(View.VISIBLE);
        } else {
            btnUpSeat.setVisibility(View.GONE);
        }
    }

    //是否能够上座,目前逻辑:有空位，是观战者，不是游戏中才可上座
    private boolean canUpSeat() {
        int allSeat = mWolfGameContext.getUnLockPositionsSize();
        if (allSeat > 0 && mWolfGameContext.isObserver() && !mWolfGameContext.isGaming()) {
            return true;
        }

        return false;
    }

    //显示"去观战"按钮
    private void turnIntoObserverButton() {
        if (isCanTurnIntoObserver()) {
            btnToObserve.setVisibility(View.VISIBLE);
        } else {
            btnToObserve.setVisibility(View.GONE);
        }
    }

    //是否能够去观战,目前逻辑:房主或已是观战者或游戏已经开始就不能去观战
    private boolean isCanTurnIntoObserver() {
        if (mWolfGameContext.isRoomOwner() || mWolfGameContext.isObserver() || mWolfGameContext.isGaming()) {
            return false;
        }
        return true;
    }
    //wwq 2018-02-05 添加"去观战"、"上座"按钮的显示 end//

    //wwq 2108-02-11 断网重连控制按钮状态的判断//
    private void restoreControlButton() {
        if (mWolfGameContext.isObserver()) {
            turnIntoObserverMode();
        }
        showUpSeatButton();
        turnIntoObserverButton();
    }

    private void hideAllObserverButton() {
        btnUpSeat.setVisibility(View.GONE);
        btnToObserve.setVisibility(View.GONE);
    }

    private void initListener() {
        RxToolKit.Build(this, CardEvent.WereWolfTryUseCheckCardEvent.class).action(v -> {
            tryShowCheckCard();
        }).register();
    }

    private void hideKeyboard() {
        if (mTypeWritingView != null && mTypeWritingView.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) mTypeWritingView
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mTypeWritingView.getWindowToken(), 0);
        }
    }

    private void showKeyboard() {
        if (mTypeWritingView != null && mTypeWritingView.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) mTypeWritingView
                    .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mTypeWritingView, InputMethodManager.SHOW_FORCED);
        }
    }

    private void turnIntoStartButton(Button button) {
        button.setVisibility(View.VISIBLE);
        button.setBackgroundResource(R.drawable.drawable_werewolf_start);
        button.setText(R.string.text_start);
        button.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendGameStart(null, result -> {
                if (mWolfGameContext != null) {
                    mWolfGameContext.post(() -> {
                        Toast.makeText(mWolfGameContext.getAndroidContext(), result, Toast.LENGTH_SHORT).show();
                    });
                }
            });
        });
    }

    private void turnIntoPrepareButton(Button button) {
        mPrepareButton = button;
        button.setVisibility(View.VISIBLE);
        button.setBackgroundResource(R.drawable.drawable_werewolf_prepare);
        button.setText(R.string.text_prepare);
        button.setOnClickListener(v -> {
            if (mWolfGameContext.isPrepare()) {
                RoomSocketEngineHelper.sendEmptyType(Constant.TYPE_UNPREPARE);
            } else {
                RoomSocketEngineHelper.sendEmptyType(Constant.TYPE_PREPARE);
            }
        });
    }

    private void turnIntoChooseTypeButton(ChooseTypeButton button) {
        button.setVisibility(View.VISIBLE);
    }

    private void turnIntoIntviuTypeButton(Button button) {
        button.setVisibility(View.VISIBLE);
        button.setBackgroundResource(R.drawable.drawable_werewolf_intviu);
        button.setText(R.string.werewolf_game_intviu);//wwq 2018-02-05 这里改成邀请(原来是邀请好友)
        button.setOnClickListener(v -> {

            ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_FROM_GANE);
            data.userName = GlobalUserState.getGlobalState().getUserName();
            data.roomId = mWolfGameContext.getRoomId();
            data.password = mWolfGameContext.getPassword();

            Context mContext = mWolfGameContext.getAndroidContext();
            if (mContext != null) {
                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onGameRoomShare(mContext, data);
            }
        });
    }

    private void turnIntoMuteButton(Button button) {
        mMuteButton = button;
        button.setVisibility(View.VISIBLE);
        button.setText(MessageUtils.getString(R.string.mute_all_string));
        button.setBackgroundResource(R.drawable.drawable_werewolf_start);
        button.setOnClickListener(v -> {
            if (mIsSendMuteAll) {
                RoomSocketEngineHelper.sendEmptyType(Constant.UNMUTE_ALL);
            } else {
                RoomSocketEngineHelper.sendEmptyType(Constant.MUTE_ALL);
            }
        });
    }

    private void turnIntoOverSpeakButton(Button button) {
        button.setVisibility(View.VISIBLE);
        button.setText(R.string.werewolf_game_over_speak);
        button.setOnClickListener(v -> {
            ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
            RoomSocketEngineHelper.sendEmptyType(Constant.ENDSPEECH);
        });
    }

    private void speakButtonTurnIntoPress() {
        mSpeakView.setEnabled(true);
        mSpeakView.setBackgroundResource(R.drawable.ico_werewolf_speak_press);
        mSpeakViewText.setText(MessageUtils.getString(R.string.action_up));
        mSpeakViewText.setTextColor(mRootView.getResources().getColor(R.color.color_werewolf_speak_text_press));
        mSpeakViewIco.setImageResource(R.drawable.ico_werewolf_speak_mic_press);
    }

    private void speakButtonTurnIntoUnPress() {
        mSpeakView.setEnabled(true);
        mSpeakView.setBackgroundResource(R.drawable.ico_werewolf_speak_unpress);
        mSpeakViewText.setText(MessageUtils.getString(R.string.action_down));
        mSpeakViewText.setTextColor(mRootView.getResources().getColor(R.color.color_werewolf_speak_text_unpress));
        mSpeakViewIco.setImageResource(R.drawable.ico_werewolf_speak_mic_unpress);
    }

    private void speakButtonTurnIntoMute() {
        mSpeakView.setEnabled(false);
        mSpeakView.setBackgroundResource(R.drawable.ico_werewolf_speak_mute);
        mSpeakViewText.setText(MessageUtils.getString(R.string.has_been_mute));
        mSpeakViewText.setTextColor(mRootView.getResources().getColor(R.color.color_werewolf_speak_text_unpress));
        mSpeakViewIco.setImageResource(R.drawable.ico_werewolf_speak_mic_unpress);
    }

    /**
     * 转换为被禁言状态
     */
    private void turnIntoMute() {
        if (!mWolfGameContext.isDead()) {
            Toast.makeText(mWolfGameContext.getAndroidContext(), R.string.mute_all, Toast.LENGTH_SHORT).show();
        }
        endSpeak(MotionEvent.ACTION_UP);
        speakButtonTurnIntoMute();
    }

    private void turnIntoUnMute(boolean needToast) {
        if (needToast) {
            Toast.makeText(mWolfGameContext.getAndroidContext(), R.string.ummute_all, Toast.LENGTH_SHORT).show();
        }
        speakButtonTurnIntoUnPress();
    }

    private void turnIntoQuitElectionButton(Button button) {
        mQuitElectionButton = button;
        if (mWolfGameContext.isDead()) {
            return;
        }
        button.setVisibility(View.VISIBLE);
        button.setText(R.string.werewolf_game_quit_election);
        button.setOnClickListener(v -> {
            ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
            RoomSocketEngineHelper.sendEmptyType(Constant.GIVE_UP_APPLY);
        });
    }

    private void disableQuitElectionButton() {
        if (mQuitElectionButton != null) {
            mQuitElectionButton.setVisibility(View.GONE);
            mQuitElectionButton = null;
        }
    }

    private void init() {
        turnIntoPreGameButton();
    }

    private void turnIntoPreGameButton() {
        mRootView.setVisibility(View.VISIBLE);
        goneAllActionButton();
        mSwitchType = false;
        updateSwitchView(mSwitchType);
        /**
         * init Button By Game Type and RoomOwner
         */
        switch (mWolfGameContext.getGameType()) {
            case WereWolfConstant.GAME_TYPE_PRE_SIMPLE:
                if (mWolfGameContext.isRoomOwner()) {
                    turnIntoStartButton(mActionLeft);
                    turnIntoChooseTypeButton(mChooseTypeButton);
                    turnIntoIntviuTypeButton(mActionMid);
                } else {
                    turnIntoPrepareButton(mActionLeft);
                    turnIntoIntviuTypeButton(mActionRight);
                }
                break;
            case WereWolfConstant.GAME_TYPE_PRE_SIMPLE_NEW:
                if (mWolfGameContext.isRoomOwner()) {
                    turnIntoStartButton(mActionLeft);
                    turnIntoIntviuTypeButton(mActionRight);
                } else {
                    turnIntoPrepareButton(mActionLeft);
                    turnIntoIntviuTypeButton(mActionRight);
                }
                break;
            default:
                if (mWolfGameContext.isRoomOwner()) {
                    turnIntoStartButton(mActionLeft);
                    turnIntoIntviuTypeButton(mActionRight);
                } else {
                    turnIntoPrepareButton(mActionLeft);
                    turnIntoIntviuTypeButton(mActionRight);
                }
                break;
        }

        //更新一下"去观战"按钮的状态
        turnIntoObserverButton();
    }

    /**
     * 切换为自己发言状态
     */
    private void turnIntoSelfSpeech() {
        mRootView.setVisibility(View.VISIBLE);
        goneAllActionButton();
        if (mWolfGameContext.isCanCutSpeak()) {
            turnIntoMuteButton(mActionLeft);
            turnIntoOverSpeakButton(mActionRight);
        } else {
            turnIntoOverSpeakButton(mActionMid);
        }
        mSwitchButton.setVisibility(View.VISIBLE);
        mSwitchType = false;
        updateSwitchView(mSwitchType);
        mSendGift.setVisibility(View.VISIBLE);
        turnIntoUnMute(false);
    }

    private void turnIntoQuitJoinElectionButton() {
        goneAllActionButton();
        turnIntoQuitElectionButton(mActionMid);
    }

    private void turnIntoNormalForNightWolf() {
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
            return;
        }
        mSwitchType = false;
        updateSwitchView(mSwitchType);
        mRootView.setVisibility(View.VISIBLE);
        goneAllActionButton();
        turnIntoUnMute(false);
    }

    /**
     * 注意：此表示在游戏已经开始的情况
     * <p>
     * 正常情况下的底部操作版展示情况
     * 1。消失掉所有的按钮
     * 2。如果为可插麦模式，则保留 切换说话模式功能
     * 3。如果为不可插麦模式，则仅仅保留送礼物
     */
    private void turnIntoGaming() {

        /**
         * 隐藏"去观战"、"上座"按钮
         */
        btnToObserve.setVisibility(View.GONE);
        btnUpSeat.setVisibility(View.GONE);

        if (mWolfGameContext.isDead()) {
            turnIntoDead();
            return;
        }

        mRootView.setVisibility(View.VISIBLE);

        goneAllActionButton();

        turnIntoUnMute(false);

        tryDismissCheckCard();
    }

    private void turnIntoSunSet() {
        if (mWolfGameContext.isObserver()) {
            return;
        }
        mRootView.setVisibility(View.INVISIBLE);
    }

    private void turnIntoDead() {
        mRootView.setVisibility(View.INVISIBLE);
        tryDismissDelayCard();
        tryShowCheckCard();
    }

    private CardEvent.WereWolfRequestShowCardEvent getWereWolfShowCardEvent(String cardType) {
        PointF pointF = null;
        if (mSpeakPointF == null) {
            int[] location = new int[2];
            mRootView.getLocationOnScreen(location);
            mSpeakPointF = new PointF(location[0], location[1]);
        }
        pointF = mSpeakPointF;
        return new CardEvent.WereWolfRequestShowCardEvent(cardType, pointF);
    }

    private void tryShowDelayCard() {
        tryShowCard(ExhibitionsConstant.CARD_DELAY_TIME);
    }

    private void tryDismissDelayCard() {
        tryDismissCard(ExhibitionsConstant.CARD_DELAY_TIME);
    }

    private void tryShowCheckCard() {
        tryShowCard(ExhibitionsConstant.CARD_CHECK);
    }

    private void tryDismissCheckCard() {
        tryDismissCard(ExhibitionsConstant.CARD_CHECK);
    }

    private void tryDismissCard(String card) {
        // RxToolKit.Emit(new CardEvent.WereWolfDismissCardEvent(card));
        if (mWolfGameContext != null) {
            if (mWereWolfCardService == null) {
                mWereWolfCardService = (IWereWolfCardService) mWolfGameContext.getService(IWereWolfCardService.WEREWOLF_CARD_SERVICE);
            }
            mWereWolfCardService.dismissCard(new CardEvent.WereWolfDismissCardEvent(card));
        }
    }

    private void tryDismissAllCard() {
        tryDismissCard(ExhibitionsConstant.ALL_CARD);
    }

    private void tryShowCard(String cardType) {
        if (TextUtils.isEmpty(cardType)) {
            return;
        }
        if (mWolfGameContext != null) {
            //wwq 2018-03-12 游戏结束收到一个展示查验卡，这里加一下过滤//
            if (!mWolfGameContext.isGaming()) {
                return;
            }
            if (mWereWolfCardService == null) {
                mWereWolfCardService = (IWereWolfCardService) mWolfGameContext.getService(IWereWolfCardService.WEREWOLF_CARD_SERVICE);
            }
            mWereWolfCardService.showCard(getWereWolfShowCardEvent(cardType));
        }
    }

    private void goneAllActionButton() {
        mActionLeft.setVisibility(View.GONE);
        mActionRight.setVisibility(View.GONE);
        mActionMid.setVisibility(View.GONE);
        mChooseTypeButton.setVisibility(View.GONE);
    }

    //wwq 2018-02-24//
    //是否显示说话按钮
    private void showSpeakView() {
        if (canShowSpeakView()) {
            mSpeakView.setVisibility(View.VISIBLE);
            /**
             * 这里注意 按住说话 和 打字说话 不可以共存
             */
            mTypeWritingView.setVisibility(View.GONE);
        } else {
            mSpeakView.setVisibility(View.GONE);
            endSpeak(MotionEvent.ACTION_UP);
        }
    }

    //是否能够显示说话按钮
    private boolean canShowSpeakView() {
        if (mWolfGameContext.isObserver()) {
            return false;
        }
        return true;
    }
    //wwq 2018-02-24//

    @Override
    public void destroy() {
        if (debugDialog != null) {
            debugDialog.destroy();
            debugDialog = null;
        }
        if (mViewGlobalObserver != null) {
            mViewGlobalObserver.destroy();
            mViewGlobalObserver = null;
        }
        ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
        RxToolKit.UnRegister(this);
        mWolfGameContext.unRegisterProcessListener(this);
        mWolfGameContext.unRegisterProcessListener(mObserverProcess);
        /**
         * 这里注意 由于由mWolfGameContext统一管理ProcessLeftCycle所以这里不需要解除注册，由WolfGameContext 统一执行clear
         */
        //mWolfGameContext.removeProcessLeftCycle(this);
        mWolfGameContext = null;
    }

    @Override
    public void chooseType(int id) {
        PLog.i(TAG, "choose Type id =" + id);
        int sitedPositions = mWolfGameContext.getSitedPositionsSize();
        int unLockedPositions = mWolfGameContext.getUnLockPositionsSize();
        int lockedPositions = mWolfGameContext.getLockPositionsSize();
        int sum = sitedPositions + unLockedPositions; //未加锁位置总和

        if (sitedPositions > id) {
            /**
             * 提示无法选择当前模式
             */
            try {
                mWolfGameContext.check().post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.text_game_type_unavailable)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (sum >= id) {
            if (lockPositions(sum - id)) {

            } else {

            }
        } else {
            if (unLockPositions(id - sum)) {

            } else {

            }
        }
    }

    private boolean unLockPositions(int numbers) {
        boolean succeed = false;
        if (numbers == 0) {
            return succeed;
        }
        List<Integer> list = mWolfGameContext.getLockPositions();
        if (Utils.targetIsNull(list)) {
            return succeed;
        }
        if (list.size() >= numbers) {
            int temp = 0;
            for (Integer integer : list) {
                if (temp == numbers) {
                    break;
                }
                RoomSocketEngineHelper.sendPositionMessage(Constant.UNLOCK, integer + 1);
                temp++;
            }
            succeed = true;
        } else {
            succeed = false;
        }
        return succeed;
    }

    private boolean lockPositions(int numbers) {
        boolean succeed = false;
        if (numbers == 0) {
            return succeed;
        }
        List<Integer> list = mWolfGameContext.getUnLockPosition();
        if (Utils.targetIsNull(list)) {
            return succeed;
        }
        if (list.size() >= numbers) {
            for (int i = list.size() - 1; i >= list.size() - numbers; i--) {
                RoomSocketEngineHelper.sendPositionMessage(Constant.LOCK, list.get(i));
            }
            succeed = true;
        } else {
            succeed = false;
        }
        return succeed;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_werewolf_game_send_message) {

            ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
            handleSendMessage();

        } else if (id == R.id.id_werewolf_game_switch) {

            //wwq 2018-02-09 如果是观战者,就不能切换
            if (mWolfGameContext.isObserver()) {
                return;
            }

            if (mWolfGameContext.isSunSet()) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_sunset_switch_text)).run();
                return;
            }
            handleSwitch();

        } else if (id == R.id.id_werewolf_game_sendgift) {
            if (mWolfGameContext.isObserver()) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.game_str_observer_cannot_send_gift)).run();
                return;
            }
            if (mWolfGameContext.isSunSet()) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_sunset_gift_button_text)).run();
                return;
            }

            if (mWolfGameContext.isDead()) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.werewolf_game_send_gift_dead)).run();
                return;
            }

            if (GlobalUserState.getGlobalState().isTourist()) {
                Toast.makeText(mWolfGameContext.getAndroidContext(), R.string.gifts_preset_from_tourist, Toast.LENGTH_SHORT).show();
            } else {
                WereWolfGamePresentGiftDialog dialog = new WereWolfGamePresentGiftDialog(mWolfGameContext.getAndroidContext(), mWolfGameContext.getAllGameMember());
                dialog.showDialog(0);
            }
        }
    }

    private void handleSwitch() {
        mSwitchType = !mSwitchType;
        updateSwitchView(mSwitchType);
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
    }

    private void updateSwitchView(boolean mSwitchType, boolean keyboardShow) {
        if (mSwitchType) {
            /**
             * 切换按钮变为 语音
             */
            mSwitchButton.setImageResource(R.drawable.drawable_werewolf_btn_switch_audio);
            mSwitchButton.setVisibility(View.VISIBLE);


            mSpeakView.setVisibility(View.GONE);
            endSpeak(MotionEvent.ACTION_UP);
            mSendGift.setVisibility(View.GONE);
            mTypeWritingView.setVisibility(View.VISIBLE);
            mSendMessage.setVisibility(View.VISIBLE);
            if (keyboardShow) {
                showKeyboard();
            }
        } else {
            /**
             * 切换按钮变为 键盘
             */
            mSwitchButton.setImageResource(R.drawable.drawable_werewolf_btn_switch_send_message);
            mSwitchButton.setVisibility(View.VISIBLE);


            showSpeakView();
            mSendGift.setVisibility(View.VISIBLE);
            mTypeWritingView.setVisibility(View.GONE);
            mSendMessage.setVisibility(View.GONE);
            hideKeyboard();
        }
    }

    private void updateSwitchView(boolean mSwitchType) {
        updateSwitchView(mSwitchType, true);
    }

    private void handleSendMessage() {
        String message = mTypeWritingView.getText().toString().trim();
        if (TextUtils.equals(message, "/1727")) {
            RxToolKit.Emit(new DebugEvent.DeveloperShowDebugEvent());
            return;
        }
        if (TextUtils.equals(message, "/openDebug1727")) {
            PLog.needLog = true;
            return;
        }
        if (TextUtils.equals(message, "/closeDebug1727")) {
            PLog.needLog = false;
            return;
        }
        sendMessage(message);
    }

    private void sendMessage(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            RoomSocketEngineHelper.sendChatMessage(msg);
            mTypeWritingView.setText("");
        }
    }

    @Override
    public boolean handleGameSocketDisconnect() {
        endSpeak(MotionEvent.ACTION_UP);
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
                //加保护，如果是观战者，直接返回
                if (mWolfGameContext.isObserver()) {
                    return true;
                }
                mIsActionDown = true;
                if (!mWolfGameContext.isCanCutSpeak()) {
                    if (!mSpeakEnableFilter.filter()) {
                        return true;
                    }
                }
                if (!mWolfGameContext.isAudioDisconnect() && !mWolfGameContext.isSocketDisconnect()) {
                    RoomSocketEngineHelper.startSpeak();
                    startSpeak(MotionEvent.ACTION_DOWN);
                }
                break;
            case MotionEvent.ACTION_UP:
                mIsActionDown = false;
                endSpeak(MotionEvent.ACTION_UP);
                break;
            case MotionEvent.ACTION_CANCEL:
                mIsActionDown = false;
                speakButtonTurnIntoUnPress();
                break;
        }
        return true;
    }

    private void startSpeak(int event) {
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(event));
        speakButtonTurnIntoPress();
    }

    private void endSpeak(int event) {
        endSpeak(event, true);
    }

    private void endSpeak(int event, boolean needReportServer) {
        mIsActionDown = false;
        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(event));
        if (needReportServer) {
            RoomSocketEngineHelper.endSpeak();
        }
        speakButtonTurnIntoUnPress();
    }

    private void changeButtonPrepare(boolean isPrepare) {
        if (mPrepareButton != null) {
            if (isPrepare) {
                if (mPrepareButton != null) {
                    mPrepareButton.setText(R.string.text_unprepare);
                }
            } else {
                if (mPrepareButton != null) {
                    mPrepareButton.setText(R.string.text_prepare);
                }
            }
        }
    }

    private void initUpSeatEvent() {
        btnUpSeat.setOnClickListener(v -> {
            if (canUpSeat()) {
                RoomSocketEngineHelper.sendGameUpSeat();
                ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
            }
        });
    }

    private void initToObserveEvent() {
        btnToObserve.setOnClickListener(v -> {
            if (isCanTurnIntoObserver()) {
                endSpeak(MotionEvent.ACTION_UP);//点击去观战就结束说话
                RoomSocketEngineHelper.sendEmptyType(Constant.TYPE_UNPREPARE);//下座前取消准备
                RoomSocketEngineHelper.sendToObserve(mWolfGameContext.getPersonalPosition() - 1);
                ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.string_game_can_not_op));
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        if (mWolfGameContext.isObserver()) {
            turnIntoObserverMode();
        } else {
            tryDismissAllCard();
            turnIntoGaming();
        }
        //开始游戏强制关闭所有up/down操作
        hideAllObserverButton();
        return false;
    }

    @Override
    public boolean handleCheckCardResult(String type, JSONObject jsonObject) {
        tryShowCheckCard();
        return false;
    }

    @Override
    public boolean handleAppendTimeResult(String type, JSONObject jsonObject) {
        tryShowDelayCard();
        return false;
    }

    @Override
    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        }
        if (position == mWolfGameContext.getPersonalPosition()) {
            if (mWolfGameContext.isGaming()) {

            } else {
                init();
            }
        } else {
            init();
            changeButtonPrepare(mWolfGameContext.isPrepare());
        }
        return false;
    }

    @Override
    public boolean handlePrepare(int position, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            changeButtonPrepare(true);
        }
        return false;
    }

    @Override
    public boolean handleUnPrepare(int position, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            changeButtonPrepare(false);
        }
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        turnIntoSunSet();
        tryDismissDelayCard();
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        mCanSpeakInNight = false;
        turnIntoGaming();
        return false;
    }

    @Override
    public boolean handleGiveUpApply(int position, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            mHasJoinElection = false;
            if (mQuitElectionButton != null) {
                mQuitElectionButton.setVisibility(View.GONE);
                mQuitElectionButton = null;
            }
        }
        return false;
    }

    @Override
    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        mHasJoinElection = false;
        disableQuitElectionButton();
        if (!mWolfGameContext.isDead()) {
            turnIntoGaming();
        }
        tryDismissDelayCard();
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo voteResultInfo) {
        if (voteResultInfo.need_pk) {
            /**
             * 处于pk状态下
             */
            WereWolfGameMember member = mWolfGameContext.getGameMember(mWolfGameContext.getPersonalPosition());
            if (member != null) {
                if (member.isHandup) {
                    mHasJoinElection = true;
                } else {
                    mHasJoinElection = false;
                }
            }
        }
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }
        return false;
    }

    @Override
    public boolean handleDeathInfo(DeathInfo deathInfo) {
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }
        return false;
    }

    @Override
    public boolean handleSpeech(int position, int duration, boolean isDead, String type, List<Integer> lefts, JSONObject jsonObject) {
        handleSpeech(position);
        return false;
    }

    private void addSpeechCheckAction() {
        ActionsDetector.add(ActionsDetector.ACTION_SPEECH_CHECK, new ActionsDetector.ActionAdapter() {
            @Override
            public void onTime() {
                Toast.makeText(mWolfGameContext.getAndroidContext(), MessageUtils.getString(R.string.over_speak_danger_alert), Toast.LENGTH_SHORT).show();
                RoomSocketEngineHelper.sendEmptyType(Constant.ENDSPEECH);
            }
        }, ActionsDetector.ACTION_SPEECH_CHECK_TIME);
    }

    private void handleSpeech(int position) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            /**
             * 添加超时检测
             */
            addSpeechCheckAction();

            turnIntoSelfSpeech();

            tryShowDelayCard();

            tryDismissCheckCard();

        } else {
            /**
             * other member speech
             */
            ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);

            turnIntoGaming();

            if (mHasJoinElection) {
                turnIntoQuitJoinElectionButton();
            } else {
                disableQuitElectionButton();
            }

            endSpeak(MotionEvent.ACTION_UP);

            if (!mWolfGameContext.isDead()) {
                tryShowDelayCard();
                tryDismissCheckCard();
            } else {
                tryDismissDelayCard();
            }
        }
    }

    @Override
    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            /**
             * 这里 需要一个判断 handleSpeech 触发在 actionDown 的时候 不在产生为操作校验
             */
            if (!mIsActionDown) {
                addSpeechCheckAction();
            }
        }
        return false;
    }

    @Override
    public boolean handleMuteAll(int position, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            mIsSendMuteAll = true;
            if (mMuteButton != null) {
                mMuteButton.setText(R.string.text_unmute);
            }
        } else {
            turnIntoMute();
        }
        return false;
    }

    @Override
    public boolean handleUnMuteAll(int position, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            mIsSendMuteAll = false;
            if (mMuteButton != null) {
                mMuteButton.setText(R.string.text_mute);
            }
        } else {
            turnIntoUnMute(true);
        }
        return false;
    }

    @Override
    public boolean handleTakeAwayResult(DeathInfo deathInfo) {
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        mHasJoinElection = false;
        disableQuitElectionButton();
        tryDismissDelayCard();
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }

        return false;
    }

    @Override
    public boolean handleApplyResult(ApplyResult applyResult) {
        if (applyResult != null && applyResult.positions != null && applyResult.positions.contains(mWolfGameContext.getPersonalPosition() - 1)) {
            mHasJoinElection = true;
            goneAllActionButton();
            turnIntoQuitJoinElectionButton();
        } else {
            mHasJoinElection = false;
            disableQuitElectionButton();
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {

        mHasJoinElection = false;

        disableQuitElectionButton();

        turnIntoGaming();

        endSpeak(MotionEvent.ACTION_UP);

        if (!mWolfGameContext.isDead()) {
            tryShowDelayCard();
            tryDismissCheckCard();
        } else {
            tryDismissDelayCard();
        }
        return false;
    }


    @Override
    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        if (mWolfGameContext.isDead()) {
            turnIntoDead();
        }
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        /**
         * 恢复竞选消息
         */
        WereWolfGameMember member = mWolfGameContext.getGameMember(mWolfGameContext.getPersonalPosition());
        if (member != null) {
            if (member.isHandup) {
                mHasJoinElection = true;
            }
        }

        if (mWolfGameContext.isGaming()) {
            if (mWolfGameContext.isSunSet()) {
                turnIntoSunSet();
                tryDismissDelayCard();
                if (mWolfGameContext.isDead()) {
                    turnIntoDead();
                }
            } else {
                mCanSpeakInNight = false;
                if (mWolfGameContext.isDead()) {
                    turnIntoDead();
                } else {
                    turnIntoGaming();
                }
            }
            if (restoreResult.game_info.speech_info != null) {
                handleSpeech(restoreResult.game_info.speech_info.current.position + 1);
            }
        } else {
            tryDismissAllCard();
            init();
            if (mWolfGameContext.isPrepare()) {
                changeButtonPrepare(true);
            }
        }

        //wwq 2018-02-11断网重连按钮状态的恢复
        restoreControlButton();
        return false;
    }

    @Override
    public boolean handleWolfCanSpeak(int dur, JSONObject jsonObject) {
        mCanSpeakInNight = true;
        try {
            /**
             * show ui for werewolf
             */
            if (!mWolfGameContext.isDead()) {
                turnIntoNormalForNightWolf();
                mWolfGameContext.check().remove(canSpeakDismissTask);
                mWolfGameContext.check().post(canSpeakDismissTask, dur * 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean handleDemonCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        mWolfGameContext.check().remove(canSpeakDismissTask);
        if (canSpeakDismissTask != null) {
            canSpeakDismissTask.run();
        }
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        mCanSpeakInNight = false;
        tryDismissAllCard();
        //wwq 2018-02-24//
        if (mWolfGameContext.isObserver()) {
            turnIntoObserverMode();
        } else {
            turnIntoPreGameButton();
        }
        //wwq 2018-02-24//
        return false;
    }

    @Override
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        if (!mWolfGameContext.isGaming()) {
            if (member != null && TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), member.memberId)) {
                btnUpSeat.setVisibility(View.GONE);
                init();
            }
            showUpSeatButton();
            turnIntoObserverButton();
        }
        return false;
    }

    @Override
    public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
        if (!mWolfGameContext.isGaming()) {
            if (member != null && TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), member.memberId)) {
                turnIntoObserverMode();
            }
            showUpSeatButton();
        }
        return false;
    }

    //更改玩家身份
    private void updatePersonalIdentity() {
        if (mWolfGameContext.isObserver()) {
            mWolfGameContext.unRegisterProcessListener(this);
            mWolfGameContext.registerProcessListener(mObserverProcess);
        } else {
            mWolfGameContext.unRegisterProcessListener(mObserverProcess);
            mWolfGameContext.registerProcessListener(this);
        }
    }

    //观战者的事件处理类
    private WereWolfGameProcess mObserverProcess = new WereWolfGameProcess() {

        @Override
        public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
            if (!mWolfGameContext.isGaming()) {
                if (member != null && TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), member.memberId)) {
                    btnUpSeat.setVisibility(View.GONE);
                    init();
                }
                showUpSeatButton();
                turnIntoObserverButton();
            }
            return false;
        }

        @Override
        public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
            if (!mWolfGameContext.isGaming()) {
                if (member != null && TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), member.memberId)) {
                    turnIntoObserverMode();
                }
                showUpSeatButton();
            }
            return false;
        }

        @Override
        public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
            if (mWolfGameContext.isObserver()) {
                //有人加入并且自己是观战者,检测一下是否可上座
                showUpSeatButton();
            }
            return false;
        }

        @Override
        public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
            if (mWolfGameContext.isObserver()) {
                //有人离开并且自己是观战者,检测一下是否可上座
                showUpSeatButton();
            }
            return false;
        }

        @Override
        public boolean handleLock(List<Integer> lockPosition, JSONObject jsonObject) {
            if (mWolfGameContext.isObserver()) {
                showUpSeatButton();
            }
            return false;
        }

        @Override
        public boolean handleGameOver(GameOverResult gameOverResult) {
            mCanSpeakInNight = false;
            tryDismissAllCard();
            //wwq 2018-02-24//
            if (mWolfGameContext.isObserver()) {
                turnIntoObserverMode();
            } else {
                turnIntoPreGameButton();
            }
            //wwq 2018-02-24//
            return false;
        }

        @Override
        public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
            if (mWolfGameContext.isObserver()) {
                showUpSeatButton();
            }
            return false;
        }

        @Override
        public boolean handleRestoreRoom(RestoreResult restoreResult) {
            //wwq 2018-02-11断网重连按钮状态的恢复
            restoreControlButton();
            return false;
        }
    };

    @Override
    public boolean onProcessStart(String type, JSONObject jsonObject) {
        return false;
    }

    @Override
    public boolean onProcessEnd(String type, JSONObject jsonObject) {
        /**
         * 根据不同的type做不同的处理
         */
        if (shouldHandle(type)) {
            updatePersonalIdentity();
        }
        return false;
    }

    @Override
    public boolean onProcessError(Exception e) {
        return false;
    }

    private boolean shouldHandle(String type) {
        if (TextUtils.equals(type, Constant.GAME_UP_SEAT) || TextUtils.equals(type, Constant.GAME_DOWN_SEAT)) {
            return true;
        }
        return false;
    }
}