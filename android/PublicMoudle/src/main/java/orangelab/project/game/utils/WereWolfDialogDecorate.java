package orangelab.project.game.utils;

import android.view.ViewGroup;
import android.view.Window;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2018/1/22 下午6:11
 * Mystery
 */

public class WereWolfDialogDecorate {

    private static final int MAX_DIALOG_WIDTH = ScreenUtils.dip2px(320);

    public static void DecorateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) return;
        if (ScreenUtils.getScreenWidth() > MAX_DIALOG_WIDTH) {
            layoutParams.width = MAX_DIALOG_WIDTH;
        } else {
            layoutParams.width = (int) (ScreenUtils.getScreenWidth() * 0.90f);
        }
    }

    public static void DecorateDialog(Window window) {
        try {
            android.view.WindowManager.LayoutParams params = window.getAttributes();
            if (ScreenUtils.getScreenWidth() > MAX_DIALOG_WIDTH) {
                params.width = MAX_DIALOG_WIDTH;
            } else {
                params.width = (int) (ScreenUtils.getScreenWidth() * 0.90f);
            }
            window.setAttributes(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
