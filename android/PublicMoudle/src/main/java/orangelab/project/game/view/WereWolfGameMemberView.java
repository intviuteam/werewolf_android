package orangelab.project.game.view;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.effect.AnimationEvent;
import orangelab.project.common.model.UserVipInfo;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.common.view.HeadDecorView;
import orangelab.project.common.view.VipNameView;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.config.WereWolfConfig;
import orangelab.project.game.dialog.WereWolfUserDetailDialog;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

import com.squareup.picasso.Callback;
import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * game_werewolf
 * 2017/12/2 上午11:14
 * Mystery
 */

public class WereWolfGameMemberView extends WereWolfGameProcess implements Destroyable, View.OnClickListener {

    private static final String TAG = "WereWolfGameMemberView";

    private WereWolfGameContext mWolfGameContext;

    private View mRootView;
    private int mRootViewId;

    private Context mAndroidContext;

    /**
     * 位置座位号
     */
    private int mPosition;
    private TextView mLeftPositionView;
    private TextView mRightPositionView;
    private TextView mPositionView;

    /**
     * 位置头部Container
     */
    private View mMainContainer;

    /**
     * Vip
     */
    private View mVip;

    /**
     * 用户头像View
     */
    private HeadDecorView mMemberHead;

    /**
     * 位置状态,是否锁住，位置是否为空
     */
    private ImageView mPositionState;

    /**
     * 用户姓名
     */
    private VipNameView mPositionName = null;
    private View mPositionNameDead = null;
    /**
     * 用户即将发言的小点
     */
    private View mSpeechTurn = null;

    /**
     * 用户Tag，例如房主，角色，警长，发言等
     */

    private View mPrepareView;

    private ImageView mTag;    //用于标记房主，角色

    private ImageView mBottomLeftTag;  //用于标记 情侣
    private ImageView mBottomMidTag;   //用于标记 警长
    private ImageView mBottomRightTag; //用于标记 举手


    /**
     * 用户发言状态
     */
    private View mPositionSpeaking = null;
    private RoundProgressBar mPositionSpeakingProgress = null;
    private ImageView mPositionSpeakingAnim = null;
    private CountDownTaskManager.CountDownTask mCountDownTask = null;

    private View mPositionDead = null;

    private boolean isSpeechTime = false;
    private boolean hasPerson = false;

    private WereWolfGameMember mPositionMember = null;
    private SafeHandler mSafeHandler = new SafeHandler();

    public WereWolfGameMemberView(WereWolfGameContext wolfGameContext, int position) {
        mWolfGameContext = wolfGameContext;
        mAndroidContext = wolfGameContext.getAndroidContext();
        mPositionMember = mWolfGameContext.getGameMember(position);
        mPosition = position;
        View rootView = wolfGameContext.getGameRootView();
        /**
         * init PositionView
         */
        initView(rootView);

        initEnterData();

        mWolfGameContext.registerProcessListener(this);
    }


    private void initView(View rootView) {
        String targetId = "id_werewolf_game_position_" + mPosition;
        try {
            /**
             * 初始化根布局
             */
            mRootViewId = R.id.class.getField(targetId).getInt(null);
            ViewGroup positionRootView = (ViewGroup) rootView.findViewById(mRootViewId);
            mRootView = positionRootView;
            mRootView.setOnClickListener(this);

            /**
             * 初始化位置信息
             */
            mLeftPositionView = (TextView) positionRootView.findViewById(R.id.member_position_left);
            mRightPositionView = (TextView) positionRootView.findViewById(R.id.member_position_right);
            if (isLeft()) {
                mPositionView = mLeftPositionView;
                mPositionView.setVisibility(View.VISIBLE);
            } else {
                mPositionView = mRightPositionView;
                mPositionView.setVisibility(View.VISIBLE);
            }

            /**
             * 头部container
             */
            mMainContainer = positionRootView.findViewById(R.id.member_main_container);


            /**
             * 初始化Vip
             */
            mVip = positionRootView.findViewById(R.id.member_vip);

            /**
             * 用户头像View
             */
            mMemberHead = (HeadDecorView) positionRootView.findViewById(R.id.member_head);

            /**
             * 用户姓名
             */
            mPositionName = (VipNameView) positionRootView.findViewById(R.id.member_name);
            mPositionNameDead = positionRootView.findViewById(R.id.member_name_dead_container);

            /**
             * 位置状态
             */
            mPositionState = (ImageView) positionRootView.findViewById(R.id.member_state);

            /**
             * 准备情况
             */
            if (isLeft()) {
                mPrepareView = positionRootView.findViewById(R.id.member_prepare_right);
                mTag = (ImageView) positionRootView.findViewById(R.id.member_left_tag);
                mTag.setVisibility(View.VISIBLE);
            } else {
                mPrepareView = positionRootView.findViewById(R.id.member_prepare_left);
                mTag = (ImageView) positionRootView.findViewById(R.id.member_right_tag);
                mTag.setVisibility(View.VISIBLE);
            }

            mBottomLeftTag = (ImageView) positionRootView.findViewById(R.id.member_bottom_left_tag);
            mBottomMidTag = (ImageView) positionRootView.findViewById(R.id.member_bottom_mid_tag);
            mBottomRightTag = (ImageView) positionRootView.findViewById(R.id.member_bottom_right_tag);

            mPositionSpeaking = positionRootView.findViewById(R.id.member_speak);
            mPositionSpeakingProgress = (RoundProgressBar) positionRootView.findViewById(R.id.member_speak_progress);
            mPositionSpeakingAnim = (ImageView) positionRootView.findViewById(R.id.member_speaking);

            mPositionDead = positionRootView.findViewById(R.id.member_dead);

            mSpeechTurn = positionRootView.findViewById(R.id.id_werewolf_member_speech_turn);
            mSpeechTurn.setVisibility(View.GONE);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private boolean isLeft() {
        return mPosition <= 6;
    }

    private boolean isRight() {
        return !isLeft();
    }

    private void initEnterData() {
        WereWolfGameMember member = mWolfGameContext.getGameMember(mPosition);
        if (member != null) {
            turnIntoGaming(false);
        } else {
            turnIntoNoPerson();
        }

    }

    private void updateRoomMaster(boolean roomOwner) {
        if (mWolfGameContext.isGaming()) {
            return;
        } else {
            if (roomOwner) {
                if (mPrepareView.getVisibility() == View.VISIBLE) {
                    mPrepareView.setVisibility(View.GONE);
                }
                WereWolfMemberTagUtils.setRoomMasterTag(mTag, isLeft());
            }
        }
    }

    private void updatePrepare(boolean prepare) {
        if (mWolfGameContext.isGaming()) {
            return;
        } else {
            if (mPositionMember != null && mPositionMember.isRoomMaster) {
                return;
            }
            if (prepare) {
                mPrepareView.setVisibility(View.VISIBLE);
            } else {
                mPrepareView.setVisibility(View.GONE);
            }
        }
    }

    private void turnIntoGaming(boolean isGaming) {
        if (isGaming) {
            WereWolfMemberTagUtils.clearTag(mTag);
            clearBottomTag();
            mPrepareView.setVisibility(View.GONE);
            turnIntoHasPerson(mPositionMember);
        } else {
            dismissSpeechTurn();
            if (mPositionMember != null) {
                WereWolfMemberTagUtils.clearTag(mTag);
                WereWolfMemberTagUtils.shadowTag(mTag, isLeft(), false);
                clearBottomTag();
                turnIntoHasPerson(mPositionMember);
                updateRoomMaster(mPositionMember.isRoomMaster);
                updatePrepare(mPositionMember.isPrepare);
            } else {
                turnIntoNoPerson();
            }
        }
    }

    private void handleVipInfo(UserVipInfo vipInfo) {
        if (vipInfo != null && vipInfo.active) {
            mVip.setVisibility(View.VISIBLE);
        }
    }

    private void clearVipTag() {
        mVip.setVisibility(View.GONE);
    }

    private void turnIntoHasPerson(WereWolfGameMember member) {
        if (member != null) {
            gonePositionSpeakingContainer();
            mPositionView.setVisibility(View.VISIBLE);
            mPositionDead.setVisibility(View.GONE);
            badgeAlphaChange(false);
            mPositionNameDead.setVisibility(View.GONE);
            mPositionView.setText(Integer.toString(mPosition));
            mPositionName.setText(member.memberName);
            mPositionName.handleVipInfo(member.vipInfo);
            mPositionState.setVisibility(View.GONE);
            mMemberHead.setVisibility(View.VISIBLE);
            mMemberHead.setHeadDecorate(member.avatar_box);
            handleVipInfo(member.vipInfo);
            PicassoUtils.loadImage(mAndroidContext, member.memberHead, R.mipmap.default_head, mMemberHead.getRealHeadImage(), new Callback() {
                @Override
                public void onSuccess() {
                    if (mSafeHandler != null) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "Picasso onSuccess: ");
                            if (mPositionMember != null) {
                                //代表这个位置有人
                            } else {
                                //这个位置没有人了
                                turnIntoNoPerson();
                            }
                        });
                    }
                }

                @Override
                public void onError(Exception e) {
                    if (mSafeHandler != null) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "Picasso onError: ");
                            if (mPositionMember != null) {
                                //代表这个位置有人
                            } else {
                                //这个位置没有人了
                                turnIntoNoPerson();
                            }
                        });
                    }
                }
            });
            hasPerson = true;

            if (mWolfGameContext.isSelfPosition(mPosition)) {
                mMainContainer.setBackgroundResource(R.mipmap.ico_werewolf_member_self_main_container_bg);
            } else {
                mMainContainer.setBackgroundResource(R.mipmap.ico_werewolf_member_main_container_bg);
            }
        } else {
            turnIntoNoPerson();
        }
    }

    private void turnIntoNoPersonInner() {
        EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskRemoveEvent(mPosition));
        clearVipTag();
        mMainContainer.setBackgroundResource(R.mipmap.ico_werewolf_member_main_container_bg);
        mPositionState.setVisibility(View.VISIBLE);
        mMemberHead.setVisibility(View.GONE);
        mMemberHead.clearHeadDecorate();
        mPositionState.setImageResource(R.mipmap.ico_werewolf_member_empty);

        mPositionName.setText("");

        mPositionView.setVisibility(View.GONE);

        gonePositionSpeakingContainer();
        mPositionDead.setVisibility(View.GONE);
        mPositionNameDead.setVisibility(View.GONE);
        badgeAlphaChange(false);
        clearBottomTag();

        mPrepareView.setVisibility(View.GONE);
        WereWolfMemberTagUtils.clearTag(mTag);
        hasPerson = false;
        mPositionMember = null;
    }

    private void turnIntoNoPerson() {
        if (mWolfGameContext.positionIsLocked(mPosition)) {
            turnIntoNoPersonInner();
            turnIntoLock();
            return;
        }
        turnIntoNoPersonInner();
    }

    private void turnIntoLock() {

        mPositionState.setVisibility(View.VISIBLE);
        mMemberHead.setVisibility(View.GONE);
        mMemberHead.clearHeadDecorate();
        mPositionState.setImageResource(R.mipmap.ico_werewolf_member_lock);
        clearVipTag();
        gonePositionSpeakingContainer();
        mPositionDead.setVisibility(View.GONE);
        mPositionNameDead.setVisibility(View.GONE);
        mPrepareView.setVisibility(View.GONE);
        badgeAlphaChange(false);
        hasPerson = false;
        mPositionMember = null;
    }

    private void turnIntoSpeech(int duration) {
        if (mCountDownTask != null) {
            mCountDownTask.destroyTask();
            mCountDownTask.cancelTask();
            mCountDownTask = null;
        }
        if (duration > 0) {
            mPositionSpeakingProgress.setMax(duration * 1000);
            mCountDownTask = new CountDownTaskManager.CountDownTask(duration * 1000, 16, duration, new CountDownTaskManager.CountDownTaskListener() {
                @Override
                public void onTick(int seconds) {
                    if (mSafeHandler != null) {
                        mSafeHandler.postSafely(() -> {
                            int dur = mPositionSpeakingProgress.getMax() - seconds;
                            if (dur >= 0) {
                                mPositionSpeakingProgress.setProgress(dur);
                            }
                        });
                    }
                }

                @Override
                public void onStart(int startSeconds) {

                }

                @Override
                public void onFinish() {
                    if (mSafeHandler != null) {
                        mSafeHandler.postSafely(() -> {
                            isSpeechTime = false;
                            mPositionSpeakingProgress.setVisibility(View.INVISIBLE);
                        });
                    }
                }
            });
            mCountDownTask.startTask();
            showPositionSpeakingContainer();
            mPositionSpeakingAnim.setVisibility(View.VISIBLE);
            mPositionSpeakingAnim.setImageResource(R.drawable.drawable_werewolf_member_speak);
            mPositionSpeakingProgress.setVisibility(View.VISIBLE);
        }
    }

    private void turnIntoUnSpeech() {
        if (mCountDownTask != null) {
            mCountDownTask.destroyTask();
            mCountDownTask.cancelTask();
        }
        turnIntoUnSpeak();
    }

    private void handleSpeak(boolean isSpeaking) {
        if (isSpeaking) {
            turnIntoSpeaking();
        } else {
            turnIntoUnSpeak();
        }
    }

    private void gonePositionSpeakingContainer() {
        mPositionSpeaking.setVisibility(View.GONE);
    }

    private void showPositionSpeakingContainer() {
        mPositionSpeaking.setVisibility(View.VISIBLE);
        if (mPositionMember == null) {
            return;
        }
        if (mPositionMember.isDead) {
            if (mWolfGameContext.isSelfPosition(mPosition)) {
                mPositionSpeaking.setBackgroundResource(R.drawable.drawable_werewolf_member_self_speaking);
            } else {
                mPositionSpeaking.setBackgroundResource(R.drawable.drawable_werewolf_member_other_speaking);
            }
        } else {
            mPositionSpeaking.setBackground(null);
        }
    }

    private void turnIntoSpeaking() {
        if (mPositionSpeaking.getVisibility() != View.VISIBLE) {
            showPositionSpeakingContainer();
        }
        if (isSpeechTime) {
            mPositionSpeakingProgress.setVisibility(View.VISIBLE);
        } else {
            mPositionSpeakingProgress.setVisibility(View.INVISIBLE);
        }
        mPositionSpeakingAnim.setVisibility(View.VISIBLE);
        mPositionSpeakingAnim.setImageResource(R.drawable.drawable_werewolf_member_speak);
        AnimationDrawable drawable = (AnimationDrawable) mPositionSpeakingAnim.getDrawable();
        if (drawable != null)
            drawable.start();
        if (mPositionMember != null && !mPositionMember.isDead) {
            WereWolfMemberTagUtils.shadowTag(mTag, isLeft(), true);
        }
    }

    private void turnIntoUnSpeak() {
        if (isSpeechTime) {
            showPositionSpeakingContainer();
            mPositionSpeakingProgress.setVisibility(View.VISIBLE);
            mPositionSpeakingAnim.setVisibility(View.VISIBLE);
        } else {
            gonePositionSpeakingContainer();
            mPositionSpeakingProgress.setVisibility(View.INVISIBLE);
            mPositionSpeakingAnim.setVisibility(View.GONE);
        }

        try {
            Drawable drawable = mPositionSpeakingAnim.getDrawable();
            if (drawable != null && drawable instanceof AnimationDrawable) {
                AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
                animationDrawable.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mPositionSpeakingAnim.setImageResource(R.mipmap.ico_werewolf_mic_level_0);
        if (mPositionMember != null && !mPositionMember.isDead) {
            WereWolfMemberTagUtils.shadowTag(mTag, isLeft(), false);
        }
    }

    private void updateRole(String role) {
        WereWolfMemberTagUtils.setRoleTag(mTag, isLeft(), role);
    }

    private void clearBottomTag() {
        mBottomRightTag.setVisibility(View.GONE);
        mBottomMidTag.setVisibility(View.GONE);
        mBottomRightTag.setVisibility(View.GONE);
    }

    private void showSpeechTurn() {
        mSpeechTurn.setVisibility(View.VISIBLE);
    }

    private void dismissSpeechTurn() {
        mSpeechTurn.setVisibility(View.GONE);
    }

    public void exec(Runnable runnable) {
        exec(runnable, 0);
    }

    public void exec(Runnable runnable, long delay) {
        try {
            mWolfGameContext.check().post(runnable, delay);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDead(boolean isDead, String role) {
        if (isDead) {
            mPositionDead.setVisibility(View.VISIBLE);
            mPositionNameDead.setVisibility(View.VISIBLE);
            WereWolfMemberTagUtils.shadowTag(mTag, isLeft(), true);
            if (TextUtils.isEmpty(role)) {

            } else {
                /**
                 * 这里要加一个判断，死后角色是否需要展示 
                 */
                if (WereWolfConfig.isShowRoleWhenDead(mWolfGameContext.getGameType())) {
                    updateRole(role);
                }
            }
            /**
             * 将所有徽章透明度更改
             */
            badgeAlphaChange(true);
        } else {
            mPositionDead.setVisibility(View.GONE);
            mPositionNameDead.setVisibility(View.GONE);
            WereWolfMemberTagUtils.shadowTag(mTag, isLeft(), false);
            badgeAlphaChange(false);
        }
    }

    private void badgeAlphaChange(boolean flag) {
        if (flag) {
            mBottomLeftTag.setAlpha(0.3f);
            mBottomMidTag.setAlpha(0.3f);
            mBottomRightTag.setAlpha(0.3f);
        } else {
            mBottomLeftTag.setAlpha(1f);
            mBottomMidTag.setAlpha(1f);
            mBottomRightTag.setAlpha(1f);
        }
    }


    public void setHandsUp(boolean flag) {
        if (flag) {
            mBottomRightTag.setVisibility(View.VISIBLE);
        } else {
            mBottomRightTag.setVisibility(View.GONE);
        }
    }

    public void setLover(boolean flag) {
        if (flag) {
            mBottomLeftTag.setVisibility(View.VISIBLE);
        } else {
            mBottomLeftTag.setVisibility(View.GONE);
        }
    }

    public void setSheriff(boolean flag) {
        if (flag) {
            mBottomMidTag.setVisibility(View.VISIBLE);
        } else {
            mBottomMidTag.setVisibility(View.GONE);
        }
    }

    @Override
    public void destroy() {
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
            mCountDownTask = null;
        }
        mSafeHandler.release();
        mSafeHandler = null;
        mWolfGameContext.unRegisterProcessListener(this);
        mPositionMember = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == mRootViewId) {
            if (mPositionMember != null && hasPerson) {
                if (mWolfGameContext.isObserver()) {
                    /**
                     * 观战模式点击用户头像，进入
                     */
                    if (Utils.checkUserIdIsTourist(mPositionMember.memberId)) {
                        ToastToolKit.showShort(WereWolfString.getString(R.string.werewolf_tourist_detail_error));
                        return;
                    } else {
                        PersonalInfoActivity.Launch(mWolfGameContext.getAndroidContext(), mPositionMember.memberId);
                    }
                } else {
                    WereWolfUserDetailDialog detailDialog = new WereWolfUserDetailDialog(mWolfGameContext, mPositionMember.memberId, mPosition);
                    detailDialog.showUserDetailDialog();
                }
            }
        }
    }

    public View getView() {
        return mRootView;
    }

    public int getPositionMemberWidth() {
        if (mRootView != null && mRootView.getVisibility() == View.VISIBLE) {
            return mRootView.getWidth();
        }
        return -1;
    }

    public int getPositionMemberHeight() {
        if (mRootView != null && mRootView.getVisibility() == View.VISIBLE) {
            return mRootView.getHeight();
        }
        return -1;
    }

    public PointF getPositionMemberPoint() {
        PointF result = null;
        if (mRootView != null && mRootView.getVisibility() == View.VISIBLE) {
            int[] location = new int[2];
            mRootView.getLocationOnScreen(location);
            result = new PointF(location[0], location[1]);
        }
        return result;
    }

    @Override
    public boolean handleLock(List<Integer> lockPosition, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        } else {
            if (mWolfGameContext.positionIsLocked(mPosition)) {
                turnIntoNoPersonInner();
                turnIntoLock();
            } else {
                turnIntoHasPerson(mPositionMember);
            }
        }
        return false;
    }

    @Override
    public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
        if (mPosition == member.memberPosition) {
            turnIntoNoPerson();
            turnIntoHasPerson(member);
            mPositionMember = member;
        }
        return false;
    }

    @Override
    public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
        if (mPosition == position) {
            if (mWolfGameContext.isGaming()) {
                if (RoomSocketEngineHelper.getLeaveByDeath(jsonObject)) {
                    if (jsonObject.has("role")) {
                        setDead(true, jsonObject.optString("role"));
                    } else {
                        setDead(true, "");
                    }
                }
            } else {
                turnIntoNoPerson();
            }
        }
        return false;
    }

    @Override
    public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
        if (mPosition == position) {
            turnIntoNoPerson();
        }
        return false;
    }

    @Override
    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        if (to == mPosition) {
            setSheriff(true);
        } else {
            setSheriff(false);
        }
        return false;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        turnIntoGaming(mWolfGameContext.isGaming());
        return false;
    }

    @Override
    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        if (!ToolKit.isEmpty(roleResult.teammate_roles)) {
            /**
             * 更新狼队的角色
             */
            Map<Integer, String> map = roleResult.teammate_roles;
            String role = map.get(mPosition - 1);
            if (TextUtils.isEmpty(role)) {

            } else {
                updateRole(role);
            }
        } else {
            /**
             * 更新自己的角色
             */
            if (mPosition == mWolfGameContext.getPersonalPosition()) {
                updateRole(roleResult.role);
            }
        }
        return false;
    }

    @Override
    public boolean handleLinkResult(LinkResult linkResult) {
        if (linkResult != null && linkResult.lovers.size() > 0) {
            if (mPositionMember != null) {
                updateRole(mPositionMember.memberRole);
                if (mPositionMember.isLover) {
                    setLover(true);
                }
            }
        }
        return false;
    }

    @Override
    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        if (position == mPosition) {
            turnIntoSpeech(leftTime);
        }
        return false;
    }

    @Override
    public boolean handleApplyResult(ApplyResult applyResult) {
        if (applyResult != null && applyResult.positions != null && applyResult.positions.size() > 0) {
            if (applyResult.positions.contains(mPosition - 1)) {
                setHandsUp(true);
            }
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        isSpeechTime = false;
        setHandsUp(false);
        turnIntoUnSpeech();
        if (position == mPosition) {
            setSheriff(true);
        }
        return false;
    }

    @Override
    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        if (mPosition == position) {
            updateRoomMaster(true);
        } else {
            updateRoomMaster(false);
        }
        return false;
    }

    @Override
    public boolean handleSpeech(int position, int duration, boolean isDead, String type, List<Integer> lefts, JSONObject jsonObject) {
        if (lefts.contains(mPosition - 1)) {
            showSpeechTurn();
        } else {
            dismissSpeechTurn();
        }
        if (mPosition == position) {
            isSpeechTime = true;
            turnIntoSpeech(duration);
        } else {
            isSpeechTime = false;
            turnIntoUnSpeech();
        }
        return false;
    }

    @Override
    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        isSpeechTime = false;
        dismissSpeechTurn();
        turnIntoUnSpeech();
        setHandsUp(false);
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo voteResultInfo) {
        if (mPositionMember != null) {
            if (voteResultInfo.need_pk) {
                /**
                 * 需要Pk的情况下 再次举手
                 */
                setHandsUp(mPositionMember.isHandup);
            } else {
                setHandsUp(false);
            }
        }
        if (mPositionMember != null && mPositionMember.isSheriff) {
            setSheriff(true);
        }
        if (mPositionMember != null && mPositionMember.isDead) {
            setDead(true, mPositionMember.memberRole);
        }
        return false;
    }

    @Override
    public boolean handleTakeAwayResult(DeathInfo deathInfo) {
        if (mPositionMember != null && mPositionMember.isDead) {
            setDead(true, mPositionMember.memberRole);
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        if (mPositionMember != null && mPositionMember.isDead) {
            setDead(true, mPositionMember.memberRole);
        }
        return false;
    }

    @Override
    public boolean handleDeathInfo(DeathInfo deathInfo) {
        if (mPositionMember != null && mPositionMember.isDead) {
            setDead(true, mPositionMember.memberRole);
        }
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        isSpeechTime = false;
        dismissSpeechTurn();
        turnIntoUnSpeech();
        if (mPositionMember != null) {
            if (mPositionMember.isDead) {
                setDead(true, "");
            }
        }
        return false;
    }

    @Override
    public boolean handleBoomAwayResult(DeathInfo info) {
        isSpeechTime = false;
        turnIntoUnSpeech();
        if (mPositionMember != null) {
            if (mPositionMember.isDead) {
                setDead(true, "");
            }
        }
        return false;
    }

    @Override
    public boolean handleSpeak(int position, JSONObject jsonObject) {
        if (mPosition == position) {
            handleSpeak(true);
        }
        return false;
    }

    @Override
    public boolean handleUnSpeak(int position, JSONObject jsonObject) {
        if (mPosition == position) {
            handleSpeak(false);
        }
        return false;
    }

    @Override
    public boolean handlePrepare(int position, JSONObject jsonObject) {
        if (mPosition == position) {
            updatePrepare(true);
        }
        return false;
    }

    @Override
    public boolean handleUnPrepare(int position, JSONObject jsonObject) {
        if (mPosition == position) {
            updatePrepare(false);
        }
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        isSpeechTime = false;
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        isSpeechTime = false;
        dismissSpeechTurn();
        turnIntoUnSpeech();
        return false;
    }

    @Override
    public boolean handleSpeechDirection(int dur, int left, int right, JSONObject jsonObject) {
        isSpeechTime = false;
        dismissSpeechTurn();
        turnIntoUnSpeech();
        return false;
    }


    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        /**
         * 执行到这里一定是基本状态已经初始化完毕
         */
        mPositionMember = mWolfGameContext.getGameMember(mPosition);
        if (mWolfGameContext.isGaming()) {
            turnIntoGaming(true);
            /**
             * 初始化一些游戏的信息
             */
            if (mPositionMember != null) {
                if (mPositionMember.isDead) {
                    setDead(true, mPositionMember.memberRole);
                } else {
                    setDead(false, "");
                }

                if (mPositionMember.isSpeaking) {
                    handleSpeak(true);
                } else {
                    handleSpeak(false);
                }

                try {
                    if (restoreResult.game_info != null) {
                        RestoreResult.SpeechInfo speechInfo = restoreResult.game_info.speech_info;
                        int speechPosition = speechInfo.current.position + 1;
                        if (speechPosition > 0) {
                            int duration = speechInfo.duration / 1000;
                            if (speechPosition == mPosition) {
                                isSpeechTime = true;
                                turnIntoSpeech(duration);
                            } else {
                                isSpeechTime = false;
                                turnIntoUnSpeech();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mPositionMember.isLover) {
                    setLover(true);
                } else {
                    setLover(false);
                }

                if (mPositionMember.isHandup) {
                    setHandsUp(true);
                } else {
                    setHandsUp(false);
                }

                if (mPositionMember.isSheriff) {
                    setSheriff(true);
                } else {
                    setSheriff(false);
                }

                if (!TextUtils.isEmpty(mPositionMember.memberRole)) {
                    updateRole(mPositionMember.memberRole);
                }
            }

            if (mPosition == mWolfGameContext.getPersonalPosition()) {
                /**
                 * 更新自己的角色
                 */
                updateRole(mWolfGameContext.getSelfRole());
            }
        } else {
            turnIntoGaming(false);
        }
        return false;
    }


    @Override
    public boolean handleGiveUpApply(int position, JSONObject jsonObject) {
        if (mPositionMember != null) {
            if (mPositionMember.isHandup) {
                setHandsUp(true);
            } else {
                setHandsUp(false);
            }
        }
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        dismissSpeechTurn();
        turnIntoGaming(false);
        return false;
    }


    @Override
    public boolean handleUpSeatView(WereWolfObserverMember member, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        }
        if (mPosition == member.memberPosition) {
            turnIntoNoPerson();
        }
        return false;
    }

    @Override
    public boolean handleToObserveView(WereWolfGameMember member, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        }
        if (mPosition == member.memberPosition) {
            turnIntoNoPerson();
        }
        return false;
    }

    @Override
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        }
        if (mPosition == member.memberPosition) {
            if (member instanceof WereWolfGameMember) {
                turnIntoHasPerson((WereWolfGameMember) member);
                mPositionMember = (WereWolfGameMember) member;
            }
        }
        return false;
    }
}
