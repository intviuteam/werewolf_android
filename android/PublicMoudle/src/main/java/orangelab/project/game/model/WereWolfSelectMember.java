package orangelab.project.game.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/11/29 上午11:25
 * Mystery
 */

public class WereWolfSelectMember implements Keepable {

    public int memberPosition = -1;
    public int lastProtectPosition = -1;
    public int myselfPosition = -1;
    public String memberName;
    public String memberImage;
    public String memberRole;
    public boolean setSelect = false;

    public boolean isSheriff = false;
    public boolean isLover = false;
    public boolean isWolf = false;
    public boolean isWolfKing = false;
    public boolean isDemon = false;
}
