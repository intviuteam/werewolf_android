package orangelab.project.game.model;

import com.toolkit.action.Keepable;

import java.util.List;
import java.util.Map;

import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;

/**
 * game_werewolf
 * 2017/12/3 下午12:16
 * Mystery
 */

public class WereWolfGameMessage implements Keepable {

    public static final String TYPE_MEMBER = "member";
    public static final String TYPE_MEMBER_GIFT = "member_gift";

    public static final String TYPE_JUDGE_VOTE = "judge_vote";
    public static final String TYPE_JUDGE_RED = "judge_red";
    public static final String TYPE_JUDGE_BLUE = "judge_blue";

    public static final String TYPE_OBSERVER_DEFAULT = "observer_default";//观战消息,默认白色

    public static final String TYPE_SYSTEM = "system";

    private boolean isSunSet = false;
    private String type;

    private int fromPosition;
    private String message;
    private String message_gift;

    private String message_type;

    private MsgDecorManiFest.MsgDecorManiFestItemResource messageDecor;

    private Map<Integer, List<Integer>> vote_info;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<Integer, List<Integer>> getVote_info() {
        return vote_info;
    }

    public void setVote_info(Map<Integer, List<Integer>> vote_info) {
        this.vote_info = vote_info;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public void setFromPosition(int fromPosition) {
        this.fromPosition = fromPosition;
    }

    public void setMessage_gift(String message_gift) {
        this.message_gift = message_gift;
    }

    public String getMessage_gift() {
        return message_gift;
    }

    public int getFromPosition() {
        return fromPosition;
    }

    public boolean isSunSet() {
        return isSunSet;
    }

    public void setSunSet(boolean sunSet) {
        isSunSet = sunSet;
    }

    public MsgDecorManiFest.MsgDecorManiFestItemResource getMessageDecor() {
        return messageDecor;
    }

    public void setMessageDecor(MsgDecorManiFest.MsgDecorManiFestItemResource messageDecor) {
        this.messageDecor = messageDecor;
    }

    public static WereWolfGameMessage CreateMemberMessage(int fromPosition, String message) {
        return CreateMemberMessage(fromPosition, message, null);
    }

    public static WereWolfGameMessage CreateMemberMessage(int fromPosition, String msg, MsgDecorManiFest.MsgDecorManiFestItemResource resource) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_MEMBER);
        wereWolfGameMessage.setMessage(msg);
        wereWolfGameMessage.setFromPosition(fromPosition);
        wereWolfGameMessage.setMessageDecor(resource);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateMemberMessageForGift(int fromPosition, String message, String giftType) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_MEMBER_GIFT);
        wereWolfGameMessage.setMessage(message);
        wereWolfGameMessage.setMessage_gift(giftType);
        wereWolfGameMessage.setFromPosition(fromPosition);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateJudgeMessageRed(String messageType, String message) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_JUDGE_RED);
        wereWolfGameMessage.setMessage_type(messageType);
        wereWolfGameMessage.setMessage(message);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateJudgeMessageBlue(String messageType, String message) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_JUDGE_BLUE);
        wereWolfGameMessage.setMessage_type(messageType);
        wereWolfGameMessage.setMessage(message);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateJudgeMessageVote(String messageType, Map<Integer, List<Integer>> vote_info) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_JUDGE_VOTE);
        wereWolfGameMessage.setMessage_type(messageType);
        wereWolfGameMessage.setVote_info(vote_info);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateSystemMessage(String msg) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_SYSTEM);
        wereWolfGameMessage.setMessage(msg);
        return wereWolfGameMessage;
    }

    public static WereWolfGameMessage CreateObserverMessage(int fromPosition, String msg) {
        return CreateObserverMessage(fromPosition, msg, null);
    }

    public static WereWolfGameMessage CreateObserverMessage(int fromPosition, String msg, MsgDecorManiFest.MsgDecorManiFestItemResource resource) {
        WereWolfGameMessage wereWolfGameMessage = new WereWolfGameMessage();
        wereWolfGameMessage.setType(TYPE_OBSERVER_DEFAULT);
        wereWolfGameMessage.setFromPosition(fromPosition);
        wereWolfGameMessage.setMessage(msg);
        wereWolfGameMessage.setMessageDecor(resource);
        return wereWolfGameMessage;
    }

}
