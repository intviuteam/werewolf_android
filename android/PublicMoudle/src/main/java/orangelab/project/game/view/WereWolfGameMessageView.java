package orangelab.project.game.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.model.UserVipInfo;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.adapter.WereWolfGameMessageAdapter;
import orangelab.project.game.dialog.WereWolfRoleIntroduceDialog;
import orangelab.project.game.model.WereWolfExchangeResult;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfGameMessage;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.CheckResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/12/3 上午11:02
 * Mystery
 */

public class WereWolfGameMessageView extends WereWolfGameProcess implements Destroyable {

    private WereWolfGameContext mWolfGameContext;

    private ListView mGameMessageList;
    private WereWolfGameMessageAdapter mMessageAdapter;

    private MessageRoleHolder mMessageRoleHolder;

    //wwq 2018-02-05//
    private View mGameMessageSwitchContainer;//消息切换按钮容器

    private Button btnPlayerMessage;//玩家消息
    private Button btnObserverMessage;//观战消息

    private boolean mShouldScrollAuto = true;
    private boolean mHasAddDisconnectMsg = false;


    public WereWolfGameMessageView(WereWolfGameContext wolfGameContext) {
        mWolfGameContext = wolfGameContext;

        mWolfGameContext.registerProcessListener(this);

        View mRootView = mWolfGameContext.getGameRootView();
        mMessageRoleHolder = new MessageRoleHolder(mRootView);
        mMessageRoleHolder.getRoleDetail().setOnClickListener(v -> {
            WereWolfRoleIntroduceDialog dialog = new WereWolfRoleIntroduceDialog(mWolfGameContext.getAndroidContext());
            dialog.showDialog(0);
        });

        mGameMessageList = (ListView) mRootView.findViewById(R.id.id_werewolf_msg_list);

        mGameMessageSwitchContainer = mRootView.findViewById(R.id.ll_message_switch_container);
        btnPlayerMessage = (Button) mGameMessageSwitchContainer.findViewById(R.id.btn_player_message);
        btnObserverMessage = (Button) mGameMessageSwitchContainer.findViewById(R.id.btn_observer_message);

        init();
        initListener();
        initData();
    }

    private void initData() {
        EnterRoomResult.RoomConfig roomConfig = mWolfGameContext.getRoomConfig();
        if (roomConfig != null) {
            String result = MessageUtils.getGameStartSystemMessageForBoard(roomConfig.role_map);
            if (!TextUtils.isEmpty(result)) {
                mMessageRoleHolder.setRoleMapText(result);
            }
        }
    }

    private void initListener() {

    }

    private void init() {
        mMessageAdapter = new WereWolfGameMessageAdapter(mWolfGameContext);
        mGameMessageList.setAdapter(mMessageAdapter);
        mGameMessageList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        mShouldScrollAuto = true;
                    } else {
                        mShouldScrollAuto = false;
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        //wwq 2018-02-08展示玩家消息//
        btnPlayerMessage.setOnClickListener(v -> {
            mMessageAdapter.switchToPlayerMessage();
            mGameMessageList.setSelection(mMessageAdapter.getCount() - 1);
            selectObserverMessageBtn(false);
            selectPlayerMessageBtn(true);
        });

        //wwq 2018-02-08展示观战者消息//
        btnObserverMessage.setOnClickListener(v -> {
            mMessageAdapter.switchToObserverMessage();
            mGameMessageList.setSelection(mMessageAdapter.getCount() - 1);
            selectObserverMessageBtn(true);
            selectPlayerMessageBtn(false);
        });

        updateSwitchMessageButton();
        initSwitchMessage();

        mMessageRoleHolder.turnIntoGaming(false);
    }

    private void initSwitchMessage() {
        /**
         * 默认选中玩家消息
         */
        selectPlayerMessageBtn(true);
        selectObserverMessageBtn(false);
    }

    private void selectPlayerMessageBtn(boolean flag) {
        if (flag) {
            btnPlayerMessage.setBackgroundResource(R.drawable.drawable_werewolf_messages_mode_selected);
            btnPlayerMessage.setTextColor(mWolfGameContext.getAndroidContext().getResources().getColor(R.color.color_werewolf_yellow));
        } else {
            btnPlayerMessage.setBackgroundColor(mWolfGameContext.getAndroidContext().getResources().getColor(android.R.color.transparent));
            btnPlayerMessage.setTextColor(mWolfGameContext.getAndroidContext().getResources().getColor(R.color.color_werewolf_messages_mode_text));
        }
    }

    private void selectObserverMessageBtn(boolean flag) {
        if (flag) {
            btnObserverMessage.setBackgroundResource(R.drawable.drawable_werewolf_messages_mode_selected);
            btnObserverMessage.setTextColor(mWolfGameContext.getAndroidContext().getResources().getColor(R.color.color_werewolf_yellow));
        } else {
            btnObserverMessage.setBackgroundColor(mWolfGameContext.getAndroidContext().getResources().getColor(android.R.color.transparent));
            btnObserverMessage.setTextColor(mWolfGameContext.getAndroidContext().getResources().getColor(R.color.color_werewolf_messages_mode_text));
        }
    }

    public void addMessage(WereWolfGameMessage message) {
        if (mMessageAdapter != null) {
            mMessageAdapter.addMessage(message);
            if (mShouldScrollAuto) {
                mGameMessageList.setSelection(mMessageAdapter.getCount() - 1);
            }
        }
    }

    //更新消息切换按钮,观战者可见，玩家不可见
    private void updateSwitchMessageButton() {
        if (mWolfGameContext.isObserver()) {
            mGameMessageSwitchContainer.setVisibility(View.VISIBLE);
        } else {
            mGameMessageSwitchContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (mMessageAdapter != null) {
            mMessageAdapter.destroy();
            mMessageAdapter = null;
        }
        mWolfGameContext.unRegisterProcessListener(this);
        mWolfGameContext = null;
    }

    @Override
    public boolean handleGameSocketConnect() {
        if (mHasAddDisconnectMsg) {
            addMessage(WereWolfGameMessage.CreateSystemMessage(WereWolfString.getString(R.string.socket_reconnect)));
            mHasAddDisconnectMsg = false;
        }
        return false;
    }

    @Override
    public boolean handleGameSocketDisconnect() {
        if (!mHasAddDisconnectMsg) {
            addMessage(WereWolfGameMessage.CreateSystemMessage(WereWolfString.getString(R.string.socket_disconnect)));
            mHasAddDisconnectMsg = true;
        }
        return false;
    }

    @Override
    public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateSystemMessage(WereWolfString.getString(R.string.werewolf_game_member_join, member.memberName)));
        updateSwitchMessageButton();
        return false;
    }

    @Override
    public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateSystemMessage(WereWolfString.getString(R.string.werewolf_game_member_leave, member.memberName)));
        return false;
    }

    @Override
    public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateSystemMessage(MessageUtils.getKickOutMessage(member.memberName)));
        return false;
    }

    @Override
    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateSystemMessage(MessageUtils.updateRoomOwner(mWolfGameContext, position)));
        return false;
    }

    @Override
    public boolean handleSystemMessage(String type, String content, JSONObject jsonObject) {
        if (TextUtils.equals(type, Constant.SYSTEM_MSG_TYPE_TEXT)) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", content));
        }
        return false;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        mMessageAdapter.clearAll();
        mMessageAdapter.updateDayState();
        String msgForBoard = MessageUtils.getGameStartSystemMessageForBoard(startResult);
        if (!TextUtils.isEmpty(msgForBoard)) {
            mMessageRoleHolder.setRoleMapText(msgForBoard);
        }
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", MessageUtils.getGameStartSystemMessage(jsonObject)));
        return false;
    }

    @Override
    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        mMessageRoleHolder.turnIntoGaming(true);
        mMessageRoleHolder.setCurRole(MessageUtils.getRole(roleResult.role));
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        mMessageAdapter.updateDayState();
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        mMessageAdapter.updateDayState();
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", MessageUtils.getSunSetSystemMessage(dayIndex)));
        return false;
    }

    @Override
    public boolean handleDeathInfo(DeathInfo deathInfo) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getDeathInfoSystemMessage(deathInfo)));
        return false;
    }

    @Override
    public boolean handleGift(String giftType, String message, int from, int to, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateMemberMessageForGift(from, message, giftType));
        return false;
    }

    @Override
    public boolean handleChat(int position, String message, JSONObject jsonObject) {
        String channel = Constant.MESSAGE_ALL;
        if (jsonObject != null) {
            channel = jsonObject.optString(Constant.MESSAGE_CHANNEL);
        }
        WereWolfObserverMember member = mWolfGameContext.getGameMember(position);
        if (member == null) {
            /**
             * 去观战中查询
             */
            member = mWolfGameContext.getObserverMember(position);
        }
        if (TextUtils.equals(Constant.MESSAGE_DEATH, channel)) {
            addObserverChatMsg(message, member);
        } else {
            addMemberChatMsg(message, member);
        }
        return false;
    }

    private void addMemberChatMsg(String message, WereWolfObserverMember member) {
        if (member == null) return;
        UserInfoResult.MessageBox messageBox = member.message_box;
        if (messageBox != null && !TextUtils.isEmpty(messageBox.message_box_type)) {
            MsgDecorManiFest.MsgDecorManiFestItem msgDecorManiFestItem = EffectsManager.GetMsgDecorItem(messageBox.message_box_type);
            if (msgDecorManiFestItem != null) {
                addMessage(WereWolfGameMessage.CreateMemberMessage(member.memberPosition, message, msgDecorManiFestItem.animate));
            }
        } else {
            addMessage(WereWolfGameMessage.CreateMemberMessage(member.memberPosition, message, null));
        }
    }

    private void addMemberChatMsg(String message, WereWolfGameMember member) {
        addMemberChatMsg(message, member);
    }

    private void addObserverChatMsg(String message, WereWolfObserverMember member) {
        if (member == null) return;
        UserInfoResult.MessageBox messageBox = member.message_box;
        if (messageBox != null && !TextUtils.isEmpty(messageBox.message_box_type)) {
            MsgDecorManiFest.MsgDecorManiFestItem msgDecorManiFestItem = EffectsManager.GetMsgDecorItem(messageBox.message_box_type);
            if (msgDecorManiFestItem != null) {
                addMessage(WereWolfGameMessage.CreateObserverMessage(member.memberPosition, message, msgDecorManiFestItem.animate));
            }
        } else {
            addMessage(WereWolfGameMessage.CreateObserverMessage(member.memberPosition, message, null));
        }
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        if (fightAwayResult != null && fightAwayResult.death_info != null && fightAwayResult.death_info.size() == 1) {
            DeathInfo.DeathInfoItem item = fightAwayResult.death_info.get(0);
            if (TextUtils.equals(item.reason, WereWolfConstant.FIGHT_APOLOGIZE_DEATH)) {
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue(
                        "", MessageUtils.getString(R.string.string_werewolf_knight_fight_people, Integer.toString(fightAwayResult.knight + 1), Integer.toString(item.killer + 1))));
            }
            if (TextUtils.equals(item.reason, WereWolfConstant.FIGHT_AWAY)) {
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue(
                        "", MessageUtils.getString(R.string.string_werewolf_knight_fight_wolf, Integer.toString(fightAwayResult.knight + 1), Integer.toString(item.killed + 1))));
            }
        }
        return false;
    }

    @Override
    public boolean handleCheckResult(CheckResult checkResult) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getCheckResultString(checkResult.position, checkResult.role)));
        return false;
    }

    @Override
    public boolean handleKillResult(int position, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getWolfKillMessage(position)));
        return false;
    }

    @Override
    public boolean handleTakeAwayResult(DeathInfo deathInfo) {
        String msg = MessageUtils.getTakeAwayResultSystemMessage(deathInfo);
        if (TextUtils.isEmpty(msg)) {
            return false;
        } else {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", msg));
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        if (position > 0) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getSheriffResultString(position)));
        } else {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getString(R.string.system_apply_no_result)));
        }
        return false;
    }

    @Override
    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        if (to == -1) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getHandoverResultNoSheriffSystemMessage()));
        } else {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getSheriffTurnResult(from, to)));
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getSheriffResultString(to)));
        }
        return false;
    }

    @Override
    public boolean handleLinkResult(LinkResult linkResult) {
        String msg = MessageUtils.getGameLinkResultMessage(mWolfGameContext, linkResult);
        if (TextUtils.isEmpty(msg)) {
            return false;
        }
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", msg));
        return false;
    }

    @Override
    public boolean handleApply(int durSeconds, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", MessageUtils.getApplySystemMessage()));
        return false;
    }

    @Override
    public boolean handleApplyResult(ApplyResult applyResult) {
        String msg = MessageUtils.getApplyResultSystemMessage(applyResult);
        if (TextUtils.isEmpty(msg)) {
            return false;
        }
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", msg));
        return false;
    }

    @Override
    public boolean handleGiveUpApply(int position, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", MessageUtils.getGiveUpPositionString(position)));
        return false;
    }

    @Override
    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        String msg = MessageUtils.getString(
                R.string.user_delay_time_card_hint,
                Integer.toString(position),
                Integer.toString(appendTime));
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", msg));
        return false;
    }

    @Override
    public boolean handleProtectResult(int position, JSONObject jsonObject) {
        String msg = MessageUtils.getString(R.string.protect_result_text, position + "");
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", msg));
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed("", MessageUtils.getString(R.string.werewolf_boom, position + "")));
        return false;
    }

    @Override
    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        return false;
    }

    @Override
    public boolean handleBoomAwayResult(DeathInfo info) {
        if (info != null && info.death_info != null && info.death_info.size() > 0) {
            for (DeathInfo.DeathInfoItem key : info.death_info) {
                if (key != null) {
                    addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getString(R.string.werewolf_boom_away, (key.killer + 1) + "", (key.killed + 1) + "")));
                } else {

                }
            }
        } else {

        }
        return false;
    }

    @Override
    public boolean handleRobCardResult(String type, JSONObject jsonObject) {
        return handleCardUseResult(type, jsonObject);
    }

    @Override
    public boolean handleAppendTimeResult(String type, JSONObject jsonObject) {
        return handleCardUseResult(type, jsonObject);
    }

    @Override
    public boolean handleCheckCardResult(String type, JSONObject jsonObject) {
        return handleCardUseResult(type, jsonObject);
    }

    @Override
    public boolean handleCardUseResult(String type, JSONObject jsonObject) {
        switch (type) {
            case Constant.NOT_ENOUGH_CARD:
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("",
                        MessageUtils.getString(R.string.apply_role_result_not_enough_card)
                ));
                break;
            case Constant.NOT_ENOUGH_ROLES:
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("",
                        MessageUtils.getString(R.string.apply_role_result_not_enough_roles)
                ));
                break;
            case Constant.FAILED:
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("",
                        MessageUtils.getString(R.string.apply_card_failed)
                ));
                break;
            case Constant.ILLEGAL_STATE:
                addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("",
                        MessageUtils.getString(R.string.apply_card_failed)
                ));
                break;
        }
        return false;
    }

    @Override
    public boolean handleSaveResult(int position, JSONObject jsonObject) {
        if (position != -1) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getSaveMemberString(position)));
        }
        return false;
    }

    @Override
    public boolean handlePoisonResult(int position, JSONObject jsonObject) {
        if (position != -1) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getPoisonMember(position)));
        }
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo voteResultInfo) {
        String messageType = "";
        if (voteResultInfo.type.equals(Constant.APPLY)) {
            messageType = WereWolfString.getString(R.string.werewolf_game_vote_type_apply);
        } else {
            messageType = WereWolfString.getString(R.string.werewolf_game_vote_type_death);
        }
        addMessage(WereWolfGameMessage.CreateJudgeMessageVote(messageType, voteResultInfo.vote_info));
        addMessage(WereWolfGameMessage.CreateJudgeMessageRed(messageType, MessageUtils.getVoteResultSystemMessage(voteResultInfo)));
        return false;
    }

    @Override
    public boolean handleExchangeResult(WereWolfExchangeResult exchangeResult, JSONObject jsonObject) {
        if (exchangeResult != null && exchangeResult.positions != null && exchangeResult.positions.size() > 0 && exchangeResult.positions.size() == 2) {
            addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("",
                    WereWolfString.getString(
                            R.string.werewolf_room_magician_result,
                            Integer.toString(exchangeResult.positions.get(0) + 1),
                            Integer.toString(exchangeResult.positions.get(1) + 1))
            ));
        }
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        mMessageAdapter.updateDayState();
        addMessage(WereWolfGameMessage.CreateJudgeMessageBlue("", MessageUtils.getGameOverSystemMessage(gameOverResult)));
        updateSwitchMessageButton();
        mMessageRoleHolder.turnIntoGaming(false);
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        mMessageRoleHolder.turnIntoGaming(mWolfGameContext.isGaming());
        String msgForBoard = MessageUtils.getGameStartSystemMessageForBoard(restoreResult.room_info.config.role_map);
        if (!TextUtils.isEmpty(msgForBoard)) {
            mMessageRoleHolder.setRoleMapText(msgForBoard);
        }
        String selfRole = mWolfGameContext.getSelfRole();
        if (!TextUtils.isEmpty(selfRole)) {
            mMessageRoleHolder.setCurRole(MessageUtils.getRole(selfRole));
        }
        updateSwitchMessageButton();
        return false;
    }

    @Override
    public boolean handleObserverJoin(WereWolfObserverMember member, JSONObject jsonObject) {
        updateSwitchMessageButton();
        return false;
    }

    @Override
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        updateSwitchMessageButton();
        mMessageAdapter.switchToPlayerMessage();
        return false;
    }

    @Override
    public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
        updateSwitchMessageButton();
        return false;
    }


    public static class MessageRoleHolder {

        private TextView id_werewolf_role_rule;
        private TextView game_cur_role;
        private TextView id_werewolf_game_role;
        private TextView id_werewolf_game_role_detail;

        public MessageRoleHolder(View rootView) {
            id_werewolf_role_rule = (TextView) rootView.findViewById(R.id.id_werewolf_role_rule);
            game_cur_role = (TextView) rootView.findViewById(R.id.game_cur_role);
            id_werewolf_game_role = (TextView) rootView.findViewById(R.id.id_werewolf_game_role);
            id_werewolf_game_role_detail = (TextView) rootView.findViewById(R.id.id_werewolf_game_role_detail);
        }

        public View getRoleDetail() {
            return id_werewolf_game_role_detail;
        }

        public void turnIntoGaming(boolean isGaming) {
            if (isGaming) {
                id_werewolf_game_role.setVisibility(View.VISIBLE);
                game_cur_role.setVisibility(View.VISIBLE);
                id_werewolf_game_role.setVisibility(View.VISIBLE);
                id_werewolf_game_role_detail.setVisibility(View.VISIBLE);
            } else {
                id_werewolf_game_role.setVisibility(View.VISIBLE);
                id_werewolf_game_role.setText("");
                game_cur_role.setVisibility(View.GONE);
                id_werewolf_game_role.setVisibility(View.GONE);
                id_werewolf_game_role_detail.setVisibility(View.GONE);
            }
        }

        public void setRoleMapText(String text) {
            id_werewolf_role_rule.setText(text);
        }

        public void setCurRole(String text) {
            id_werewolf_game_role.setText(text);
        }
    }
}
