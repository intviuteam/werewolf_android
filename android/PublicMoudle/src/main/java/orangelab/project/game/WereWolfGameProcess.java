package orangelab.project.game;

import orangelab.project.common.model.action.ServerActionFight;
import orangelab.project.common.model.action.ServerActionFightAway;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.common.model.action.ServerActionFightState;
import orangelab.project.game.model.WereWolfExchange;
import orangelab.project.game.model.WereWolfExchangeResult;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.CheckResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.common.model.WakeToKillResult;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;

import org.json.JSONObject;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/1 下午6:04
 * Mystery
 */

public abstract class WereWolfGameProcess {

    public boolean handlePreAllMessage(String type, JSONObject jsonObject) {
        return false;
    }

    public boolean handleGameSocketDisconnect() {
        return false;
    }

    public boolean handleAudioSocketConnect() {
        return false;
    }

    public boolean handleAudioSocketDisConnect() {
        return false;
    }

    public boolean handleGameSocketConnect() {
        return false;
    }

    public boolean handleUpdateConfig(EnterRoomResult.RoomConfig config) {
        return false;
    }

    public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
        return false;
    }

    public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
        return false;
    }

    public boolean handleLock(List<Integer> lockPosition, JSONObject jsonObject) {
        return false;
    }

    public boolean handleExport(String type, String title, JSONObject jsonObject) {
        return false;
    }

    public boolean handleResetMaster(int dur, JSONObject jsonObject) {
        return false;
    }

    public boolean handleRejectResetMaster(int dur, JSONObject jsonObject) {
        return false;
    }

    public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUnActiveWarning(int duration, JSONObject jsonObject) {
        return false;
    }

    public boolean handleGift(String giftType, String message, int from, int to, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUpdatePassword(String password, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSpeak(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUnSpeak(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handlePrepare(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUnPrepare(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleChat(int position, String message, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSystemMessage(String type, String content, JSONObject jsonObject) {
        return false;
    }

    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        return false;
    }

    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSeerCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        return false;
    }

    public boolean handleWakeToKill(WakeToKillResult wakeToKillResult) {
        return false;
    }

    public boolean handleMuteAll(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleUnMuteAll(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleDeathInfo(DeathInfo deathInfo) {
        return false;
    }

    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        return false;
    }

    public boolean handleVoteResult(VoteResultInfo voteResultInfo) {
        return false;
    }

    public boolean handleCheckResult(CheckResult checkResult) {
        return false;
    }

    public boolean handleKillResult(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleTakeAway(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        return false;
    }

    public boolean handleTakeAwayResult(DeathInfo deathInfo) {
        return false;
    }

    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        return false;
    }

    public boolean handleHandOver(WereWolfGameSurvivorsInfo info) {
        return false;
    }

    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        return false;
    }

    public boolean handleLink(int duration, JSONObject jsonObject) {
        return false;
    }

    public boolean handleLinkResult(LinkResult linkResult) {
        return false;
    }

    public boolean handleApply(int durSeconds, JSONObject jsonObject) {
        return false;
    }

    public boolean handleApplyResult(ApplyResult applyResult) {
        return false;
    }

    public boolean handleWolfCanSpeak(int dur, JSONObject jsonObject) {
        return false;
    }

    public boolean handleBoomState(boolean flag, JSONObject jsonObject) {
        return false;
    }

    public boolean handleGiveUpApply(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleCardUseResult(String type, JSONObject jsonObject) {
        return false;
    }

    public boolean handleCheckCardResult(String type, JSONObject jsonObject) {
        return false;
    }

    public boolean handleAppendTimeResult(String type, JSONObject jsonObject) {
        return false;
    }

    public boolean handleRobCardResult(String type, JSONObject jsonObject) {
        return false;
    }

    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        return false;
    }

    public boolean handleProtect(WereWolfGameSurvivorsInfo info, int lastProtect, JSONObject jsonObject) {
        return false;
    }

    public boolean handleProtectResult(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        return false;
    }

    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        return false;
    }

    public boolean handleBoomAwayResult(DeathInfo info) {
        return false;
    }

    public boolean handleSave(int position, boolean canSave, JSONObject jsonObject) {
        return false;
    }

    public boolean handlePoison(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSaveResult(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handlePoisonResult(int position, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSpeech(int position, int duration, boolean isDead, String type, List<Integer> lefts, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSpeechDirection(int dur, int leftPosition, int rightPosition, JSONObject jsonObject) {
        return false;
    }

    public boolean handleAddFriend(String userId, String userName, String userIv, JSONObject jsonObject) {
        return false;
    }

    public boolean handleSpeechDirectionResult(int dur, JSONObject jsonObject) {
        return false;
    }

    public boolean handleExchange(WereWolfExchange exchange, JSONObject jsonObject) {
        return false;
    }

    public boolean handleExchangeResult(WereWolfExchangeResult exchangeResult, JSONObject jsonObject) {
        return false;
    }

    public boolean handleDemonCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        return false;
    }

    public boolean handleDemonCheckResult(boolean is_god, int duration, JSONObject jsonObject) {
        return false;
    }

    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        return false;
    }

    public boolean handleGameOver(GameOverResult gameOverResult) {
        return false;
    }

    public boolean handleObserverJoin(WereWolfObserverMember member, JSONObject jsonObject) {
        return false;
    }

    public boolean handleObserverLeave(WereWolfObserverMember member, JSONObject jsonObject) {
        return false;
    }

    //wwq 2018-02-08 上座//
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        return false;
    }

    //wwq 2018-02-08 去观战//
    public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
        return false;
    }

    //wwq 2018-02-08 view上座//
    public boolean handleUpSeatView(WereWolfObserverMember member, JSONObject jsonObject) {
        return false;
    }

    //wwq 2018-02-08 view去观战//
    public boolean handleToObserveView(WereWolfGameMember member, JSONObject jsonObject) {
        return false;
    }

    public boolean handleServerActionFight(ServerActionFight fight) {
        return false;
    }

    public boolean handleServerActionFightAway(ServerActionFightAway fightAway) {
        return false;
    }

    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        return false;
    }

    public boolean handleServerActionFightState(ServerActionFightState fightState) {
        return false;
    }
}
