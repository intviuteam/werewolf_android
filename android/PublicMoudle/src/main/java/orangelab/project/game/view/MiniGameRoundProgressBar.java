package orangelab.project.game.view;

/**
 * game_werewolf
 * 2017/12/6 上午10:30
 * Mystery
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.R;
import com.androidtoolkit.view.ScreenUtils;


public class MiniGameRoundProgressBar extends View {

    /**
     * 画笔对象的引用
     */
    private Paint paint;

    /**
     * 圆环的颜色
     */
    private int outRoundColor;
    private int inRoundColor;

    /**
     * 圆环进度的颜色
     */
    private int roundProgressColor;

    private int roundRadio = ScreenUtils.dip2px(6);

    private int roundStroke = ScreenUtils.dip2px(6);


    /**
     * 圆环的宽度
     */
    private float roundRadius;

    /**
     * 最大进度
     */
    private int max;

    /**
     * 当前进度
     */
    private int progress;

    private boolean isClear = false;

    public MiniGameRoundProgressBar(Context context) {
        this(context, null);
    }

    public MiniGameRoundProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MiniGameRoundProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        paint = new Paint();


        //获取自定义属性和默认值
        outRoundColor = Color.parseColor("#00ffffff");
        inRoundColor = Color.parseColor("#bb000000");
        roundProgressColor = outRoundColor;
        max = 100;

    }

    public void setRoundRadio(int roundRadio) {
        this.roundRadio = roundRadio;
    }

    public void unclear() {
        isClear = false;
    }

    public void clear() {
        isClear = true;
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (isClear) {
            canvas.drawColor(Color.TRANSPARENT);
            return;
        }
        int centreX = getWidth() / 2; //获取圆心的x坐标
        int centreY = getHeight() / 2;


        // TODO: 2018/8/6 加载框的一个性能优化
        /**
         * 这里还可以再优化，放在后续
         */

        int layoutId = canvas.saveLayer(0, 0, getWidth(), getHeight(), null, Canvas.ALL_SAVE_FLAG);

        /**
         * 画背景
         */
        paint.setColor(inRoundColor);
        paint.setStrokeWidth(0);
        paint.setAntiAlias(true);  //消除锯齿
        canvas.drawRoundRect(new RectF(0, 0, getWidth(), getHeight()), roundRadio, roundRadio, paint);

        /**
         * 画最外层的圆
         */
        int paddingLeft = getPaddingLeft();
        int radius = (centreX - paddingLeft); //圆环的半径

        paint.setColor(outRoundColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);  //消除锯齿
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawCircle(centreX, centreY, radius, paint);

        roundRadius = radius - roundStroke;

        /**
         * 画进度
         */
        paint.setXfermode(null);
        paint.setStrokeWidth(roundRadius); //设置圆环的宽度
        paint.setColor(inRoundColor);  //设置进度的颜色
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        RectF oval = new RectF(centreX - roundRadius, centreY - roundRadius, centreX + roundRadius, centreY + roundRadius);

        if (progress != 0) {
            canvas.drawArc(oval, 270, -(360 * progress / max), true, paint);
        }

        canvas.restoreToCount(layoutId);
    }


    public synchronized int getMax() {
        return max;
    }

    /**
     * 设置进度的最大值
     *
     * @param max
     */
    public synchronized void setMax(int max) {
        if (max < 0) {
            throw new IllegalArgumentException("max not less than 0");
        }
        this.max = max;
    }

    /**
     * 获取进度.需要同步
     *
     * @return
     */
    public synchronized int getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        if (progress < 0) {
            throw new IllegalArgumentException("progress not less than 0");
        }
        if (progress > max) {
            progress = max;
        }
        if (progress <= max) {
            this.progress = max - progress;
            postInvalidate();
        }

    }

}

