package orangelab.project.game.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.R;

/**
 * game_werewolf
 * 2018/7/16 上午11:25
 * Mystery
 */
public class WereWolfDialogButton extends CardView {

    private Button mButton;

    public WereWolfDialogButton(@NonNull Context context) {
        this(context, null);
    }

    public WereWolfDialogButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WereWolfDialogButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        mButton = new Button(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(mButton, layoutParams);
        mButton.setGravity(Gravity.CENTER);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WereWolfDialogButton, defStyleAttr, 0);

        int textSize = a.getDimensionPixelSize(R.styleable.WereWolfDialogButton_button_textSize, 0);
        mButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

        int textColor = a.getColor(R.styleable.WereWolfDialogButton_button_textColor, Color.BLACK);
        mButton.setTextColor(textColor);

        String text = a.getString(R.styleable.WereWolfDialogButton_button_text);
        mButton.setText(text);


        Drawable mSrcDrawable = a.getDrawable(R.styleable.WereWolfDialogButton_button_background);
        if (mSrcDrawable != null) {
            mButton.setBackground(mSrcDrawable);
        }
        a.recycle();
    }

    public void setButtonBackGround(Drawable drawable) {
        mButton.setBackground(drawable);
    }

    public void setButtonBackGroundRestouce(int restouce) {
        mButton.setBackgroundResource(restouce);
    }

    public void setButtonText(String text) {
        mButton.setText(text);
    }

    public void setButtonTextColor(int color) {
        mButton.setTextColor(color);
    }

    public void setButtonOnClickListener(OnClickListener onClickListener) {
        mButton.setOnClickListener(onClickListener);
    }

    public Button getButton() {
        return mButton;
    }
}
