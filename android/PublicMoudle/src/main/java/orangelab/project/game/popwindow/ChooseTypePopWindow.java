package orangelab.project.game.popwindow;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.R;
import orangelab.project.common.adapter.ChooseTypeAdapter;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.ArrowRectangleViewGroup;
import orangelab.project.game.view.ChooseTypeButton;

import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 25/08/2017.
 */

public class ChooseTypePopWindow extends PopupWindow {

    private Context mContext;
    private ListView listType;

    private List<ChooseTypeButton.PopItem> popItems;
    private ChooseTypeAdapter adapter;
    private ChooseTypeAdapter.OnChooseTypeListener onChooseTypeListener;
    private ArrowRectangleViewGroup mGroup;

    private int mStroke;
    private int mArrowHeight;

    private int mWidth;
    private int mHeight;

    public int getPopWidth() {
        return mWidth;
    }

    public int getPopHeight() {
        return mHeight;
    }

    public ChooseTypePopWindow(Context context, List<ChooseTypeButton.PopItem> popItems) {
        super(context);
        this.mContext = context;
        this.popItems = popItems;
        this.mStroke = ScreenUtils.dip2px(2);
        this.mArrowHeight = 35;
        initView();
        initData();

        mGroup.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mWidth = mGroup.getMeasuredWidth();
        mHeight = Utils.getTotalHeightOfListView(listType) + 6 * mStroke + mArrowHeight;
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_choose_type, null);
        mGroup = (ArrowRectangleViewGroup) contentView.findViewById(R.id.container);
        mGroup.setNeedStroke(true);
        mGroup.setStrokeWidth(mStroke);
        mGroup.setStrokeColor(mContext.getResources().getColor(R.color.choose_type_stroke_color));
        mGroup.setPadding(mStroke, mStroke, mStroke, mStroke);
        mGroup.setArrowHeight(mArrowHeight);
        setContentView(contentView);
        setWidth(ScreenUtils.dip2px(70));
        setFocusable(true);
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        listType = (ListView) contentView.findViewById(R.id.list_dialog_choose_type);
        listType.setOnItemClickListener((parent, view, position, id) -> {
            ChooseTypeButton.PopItem popItem = popItems.get(position);
            if (onChooseTypeListener != null) {
                onChooseTypeListener.chooseType(popItem.id);
            }
            if (isShowing()) {
                dismiss();
            }
        });
    }

    private void initData() {
        adapter = new ChooseTypeAdapter(mContext, popItems);
        listType.setAdapter(adapter);
    }

    public void setOnChooseTypeListener(ChooseTypeAdapter.OnChooseTypeListener onChooseTypeListener) {
        this.onChooseTypeListener = onChooseTypeListener;
    }
}
