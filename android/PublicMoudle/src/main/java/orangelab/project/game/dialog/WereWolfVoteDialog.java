package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/1 上午11:08
 * Mystery
 */

public class WereWolfVoteDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfVoteDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.vote_title), Constant.VOTE, TWO_BUTTON);
    }
}
