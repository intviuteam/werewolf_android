package orangelab.project.game.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/12/3 下午2:51
 * Mystery
 */

public class WereWolfSystemMessageVoteItem implements Keepable {

    private int position;

    public WereWolfSystemMessageVoteItem(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
