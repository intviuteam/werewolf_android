package orangelab.project.game.dialog;

import android.content.Context;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import java.util.List;

/**
 * game_werewolf
 * 2017/11/29 下午3:46
 * Mystery
 */

public class WereWolfCupidSelectDialog extends WereWolfBaseSelectDialog {

    public WereWolfCupidSelectDialog(Context context, List<WereWolfSelectMember> gameMembers, String title) {
        super(context, gameMembers, 2);
        initView(title);
    }

    private void initView(String title) {
        setTitle(title);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            if (selectIslegal()) {
                RoomSocketEngineHelper.sendCoupleMessage(getRealPosition().get(0) - 1, getRealPosition().get(1) - 1);
                dismiss();
            }
        });
    }

    private boolean selectIslegal() {
        List<Integer> result = getRealPosition();
        if (result != null && result.size() == 2) {
            return true;
        } else {
            return false;
        }
    }
}
