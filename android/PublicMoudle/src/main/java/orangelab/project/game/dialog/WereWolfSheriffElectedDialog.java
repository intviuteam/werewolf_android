package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;

/**
 * game_werewolf
 * 2017/11/30 下午6:22
 * Mystery
 */

public class WereWolfSheriffElectedDialog extends WereWolfBaseDialog {

    public WereWolfSheriffElectedDialog(@NonNull Context context, WereWolfSelectMember member) {
        super(context);
        setTitle(WereWolfString.getString(R.string.werewolf_game_sheriff_handover));

        View view = View.inflate(context, R.layout.layout_werewolf_dialog_sheriff_elected, null);
        setTitle(WereWolfString.getString(R.string.werewolf_game_sheriff_elected_title));
        TextView text = (TextView) view.findViewById(R.id.id_werewolf_dialog_sheriff_text);


        TextView position = (TextView) view.findViewById(R.id.id_werewolf_dialog_member_position);
        ImageView imageView = (ImageView) view.findViewById(R.id.id_werewolf_dialog_member_head);
        TextView name = (TextView) view.findViewById(R.id.id_werewolf_dialog_member_name);

        TextView positionTextView = view.findViewById(R.id.id_werewolf_dialog_sheriff_position);
        positionTextView.setText(Integer.toString(member.memberPosition));

        text.setText(WereWolfString.getString(R.string.werewolf_game_sheriff_elected_text));

        position.setText(Integer.toString(member.memberPosition));
        name.setText(member.memberName);
        PicassoUtils.loadImage(context, member.memberImage, imageView, R.mipmap.default_head);

        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });

    }
}
