package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import com.networktoolkit.transport.Constant;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/12/1 上午11:05
 * Mystery
 */

public class WereWolfSheriffChooseHandleOverDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfSheriffChooseHandleOverDialog(Context context, ArrayList<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.handover_title), Constant.HAND_OVER, TWO_BUTTON);
    }
}
