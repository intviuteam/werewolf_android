package orangelab.project.game;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2018/3/13 下午6:40
 * Mystery
 * <p>
 * 服务器处理 消息的生命周期监听器 谨慎使用 boolean 返回为true的情况
 */

public interface WereWolfGameProcessLifeCycle {

    boolean onProcessStart(String type, JSONObject jsonObject);

    boolean onProcessEnd(String type, JSONObject jsonObject);

    boolean onProcessError(Exception e);
}
