package orangelab.project.game.view;

import android.view.View;

/**
 * game_werewolf
 * 2017/12/9 上午10:45
 * Mystery
 */

public interface IFriendRequestView {

    int getHeight();

    int getWidth();

    int getX();

    int getY();

    View getView();

    void destroy();

    String getUserId();

    void setDismissListener(Runnable dismissListener);
}
