package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;

import orangelab.project.game.model.WereWolfSelectMember;

/**
 * game_werewolf
 * 2017/12/14 下午1:54
 * Mystery
 */

public class WereWolfDemonCheckResultDialog extends WereWolfSeerCheckResultDialog {
    public WereWolfDemonCheckResultDialog(@NonNull Context context, String role, WereWolfSelectMember member) {
        super(context, role, member);
    }
}
