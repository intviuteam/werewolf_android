package orangelab.project.game.model;

import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.model.UserVipInfo;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/1/22 上午11:57
 * Mystery
 */

public class WereWolfObserverMember implements Keepable {

    public int memberPosition;
    public String memberId;
    public String memberName;
    public String memberHead;
    public int memberSex;
    public UserInfoResult.UserActiveInfo memberActive;
    public boolean isObserver;
    public String signature = "";//wwq 2018-02-06 个性签名

    public UserVipInfo vipInfo;
    public UserInfoResult.AvatarBox avatar_box;
    public UserInfoResult.MessageBox message_box;


    public static WereWolfObserverMember TurnIntoWereWolfObserverMember(EnterRoomResult.EnterRoomUserItem item) {
        WereWolfObserverMember member = new WereWolfObserverMember();
        member.memberHead = item.avatar;
        member.memberId = item.id;
        member.memberName = item.name;
        member.memberPosition = item.position + 1;
        member.isObserver = item.is_observer;
        member.memberActive = item.active;
        member.memberSex = item.sex;
        member.signature = item.signature;
        member.avatar_box = item.avatar_box;
        member.message_box = item.message_box;
        return member;
    }

    public static WereWolfGameMember TurnIntoGameMember(WereWolfObserverMember item) {
        WereWolfGameMember gameMember = new WereWolfGameMember();
        gameMember.memberHead = item.memberHead;
        gameMember.memberId = item.memberId;
        gameMember.memberName = item.memberName;
        gameMember.memberActive = item.memberActive;
        gameMember.memberPosition = item.memberPosition;
        gameMember.memberSex = item.memberSex;
        gameMember.isObserver = item.isObserver;
        gameMember.signature = item.signature;
        gameMember.message_box = item.message_box;
        gameMember.avatar_box = item.avatar_box;
        gameMember.vipInfo = item.vipInfo;
        return gameMember;
    }

    @Override
    public String toString() {
        return "WereWolfObserverMember{" +
                "memberPosition=" + memberPosition +
                ", memberId='" + memberId + '\'' +
                ", memberName='" + memberName + '\'' +
                ", memberHead='" + memberHead + '\'' +
                ", memberSex=" + memberSex +
                ", memberActive=" + memberActive +
                ", isObserver=" + isObserver +
                ", signature='" + signature + '\'' +
                '}';
    }
}
