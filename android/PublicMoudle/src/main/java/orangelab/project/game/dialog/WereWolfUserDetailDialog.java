package orangelab.project.game.dialog;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.family.FamilyInfoActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.FamilyTools;
import orangelab.project.common.view.HeadDecorView;
import orangelab.project.common.view.VipNameView;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.manager.WereWolfUserActiveManager;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.tool.UserRolesTools;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.FlowLayout;
import orangelab.project.game.view.WereWolfDialogButton;

import com.networktoolkit.transport.Constant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/12/7 下午1:31
 * Mystery
 */

public class WereWolfUserDetailDialog extends SafeDialog implements View.OnClickListener {
    private final int dialogMaxWidth = ScreenUtils.dip2px(345);
    private Context context;

    /**
     * 举报
     */
    private View reportView;

    /**
     * 个人详情信息部分
     */
    private View userDetailContainer;
    private HeadDecorView mHeadView;
    private VipNameView userName;
    private TextView userLevel;
    private TextView userPopular;
    private FlowLayout userRoles;
    private TextView userUid;

    private TextView userActiveTv;
    private ImageView userActiveIv;

    /**
     * 家族信息部分
     */
    private View mFamilyContainer;
    private ImageView mFamilyBadge;
    private View mFamilyPlaceHolder;
    private View mFamilyArrow;
    private TextView mFamilyName;

    /**
     * 战绩部分
     */
    private TextView userRate;
    private TextView userWins;
    private TextView userLoses;
    private TextView userEscapeRate;

    /**
     * 按钮部分
     */
    private WereWolfDialogButton mYellowButton;
    private WereWolfDialogButton mPinkButton;
    private WereWolfDialogButton mPurpleButton;

    private WereWolfDialogButton mAddFriendButton;
    private WereWolfDialogButton mKickOutButton;
    private WereWolfDialogButton mSendGiftButton;

    private String mUserId;
    private String mUserName;
    private String mUserIv;
    private int mUserSex;
    private int mSelectPosition = -1;

    private WereWolfGameContext mWolfGameContext = null;
    private UserInfoResult mUserInfoResult;

    public WereWolfUserDetailDialog(@NonNull Context context, String userId) {
        super(context, R.style.DarkDialogAllowDismiss);
        initView(context, userId);
    }


    public WereWolfUserDetailDialog(WereWolfGameContext context, String targetId, int selectPosition) {
        super(context.getAndroidContext(), R.style.DarkDialogAllowDismiss);
        mWolfGameContext = context;
        this.context = mWolfGameContext.getAndroidContext();
        mSelectPosition = selectPosition;
        initView(context.getAndroidContext(), targetId);
    }

    private void initView(Context context, String userId) {
        mUserId = userId;
        setContentView(R.layout.layout_werewolf_user_detail);
        initTitle();
        initUserDetailView();
        initFamilyView();
        initCombatView();
        initButton();
        handleUserInfo();
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        WereWolfDialogDecorate.DecorateDialog(getWindow());
    }

    private void initTitle() {
        reportView = findViewById(R.id.user_report_btn);
        reportView.setOnClickListener(this);
        reportView.setVisibility(View.GONE);
    }

    private void initUserDetailView() {
        userDetailContainer = findViewById(R.id.user_detail_container);
        mHeadView = (HeadDecorView) findViewById(R.id.user_head_view);
        userName = (VipNameView) findViewById(R.id.user_name);
        userName.setText("");
        userLevel = (TextView) findViewById(R.id.user_level);
        userLevel.setText("");
        userPopular = (TextView) findViewById(R.id.user_popular);
        userPopular.setVisibility(View.GONE);
        userRoles = (FlowLayout) findViewById(R.id.id_user_detail_flowlayout);
        userRoles.setVisibility(View.GONE);
        userUid = (TextView) findViewById(R.id.user_info_uid);

        userActiveIv = (ImageView) findViewById(R.id.user_active_iv);
        userActiveTv = (TextView) findViewById(R.id.user_active_tv);
    }

    private void initFamilyView() {
        mFamilyContainer = findViewById(R.id.id_user_detail_dialog_container);
        mFamilyName = (TextView) findViewById(R.id.id_user_detail_dialog_family_name);
        mFamilyArrow = findViewById(R.id.id_user_detail_dialog_arrow);
        mFamilyBadge = (ImageView) findViewById(R.id.id_user_detail_dialog_badge);
        mFamilyPlaceHolder = findViewById(R.id.id_user_detail_dialog_family_placeholder);
        setHasFamily(false);
    }


    private void initButton() {
        mYellowButton = (WereWolfDialogButton) findViewById(R.id.user_add_page);
        mYellowButton.setVisibility(View.GONE);
        mPinkButton = (WereWolfDialogButton) findViewById(R.id.user_give_gift);
        mPinkButton.setVisibility(View.GONE);
        mPurpleButton = (WereWolfDialogButton) findViewById(R.id.id_werewolf_kick_out_button);
        mPurpleButton.setVisibility(View.GONE);
    }

    private void initCombatView() {
        userRate = (TextView) findViewById(R.id.user_win_rate);
        userRate.setText("0%");
        userWins = (TextView) findViewById(R.id.wins);
        userWins.setText("0");
        userLoses = (TextView) findViewById(R.id.lose);
        userLoses.setText("0");
        userEscapeRate = (TextView) findViewById(R.id.user_escape_rate);
        userEscapeRate.setText("0%");
    }

    public void setHasFamily(boolean isHas) {
        if (isHas) {
            mFamilyArrow.setVisibility(View.VISIBLE);
            mFamilyBadge.setVisibility(View.VISIBLE);
            mFamilyPlaceHolder.setVisibility(View.GONE);
            mFamilyName.setVisibility(View.VISIBLE);
            mFamilyContainer.setOnClickListener(this);
        } else {
            mFamilyArrow.setVisibility(View.GONE);
            mFamilyBadge.setVisibility(View.GONE);
            mFamilyPlaceHolder.setVisibility(View.VISIBLE);
            mFamilyName.setVisibility(View.GONE);
        }
    }

    @Override
    protected void release() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.user_detail_container) {
            if (mSelectPosition == -1) {
                handUserMainPageFromOutSide();
            } else {
                handUserMainPage();
            }
            dismissUserDetailDialog();
        } else if (id == R.id.user_report_btn) {
            if (mWolfGameContext != null && mWolfGameContext.isObserver()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.werewolf_observer_can_not_report));
                return;
            }
            WereWolfReportDialog reportDialog = new WereWolfReportDialog(getContext(), mUserId, mUserName);
            reportDialog.show();
        }
    }

    private void handleUserInfo() {
        ApiManager.GetUserInfoApi(mUserId, (userInfoResult, e) -> runSafely(() -> {
            if (e == null) {
                handleUserInfoResult(userInfoResult);
            } else {
                ToastToolKit.showShort(e.getMessage());
            }
        }));
    }

    private void handleUserInfoResult(UserInfoResult response) {
        try {
            mUserInfoResult = response;
            initReportButton();
            handleUserDetail(response);
            handleFamilyDetail(response);
            handleCombatDetail(response);
            handleButton(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void turnIntoGift(WereWolfDialogButton button) {
        mSendGiftButton = button;
        button.setVisibility(View.VISIBLE);
        button.setButtonText(MessageUtils.getString(R.string.user_temp_dialog_give_gift));
        button.setButtonOnClickListener(v -> {
            if (mWolfGameContext != null) {
                if (mWolfGameContext.isDead()) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.werewolf_game_send_gift_dead)).run();
                    return;
                }
                if (mWolfGameContext.isSunSet()) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_sunset_gift_button_text)).run();
                    return;
                }
                if (mWolfGameContext.isObserver()) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.werewolf_observer_can_not_send_gift)).run();
                    return;
                }
            }
            if (mSelectPosition == -1) {
                ExhibitionsActivity.startActivityFromOutSide(context, mUserId, mUserName, mUserSex, mUserIv);
            } else {
                ExhibitionsActivity.startActivityFromGame(context, mUserId, mUserName, mUserSex, mUserIv, mSelectPosition);
            }
            dismissUserDetailDialog();
        });
    }

    private void sendGiftButtonShow(boolean flag) {
        if (flag) {
            if (mSendGiftButton != null) {
                mSendGiftButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (mSendGiftButton != null) {
                mSendGiftButton.setVisibility(View.GONE);
            }
        }
    }

    private void addFriendShow(boolean flag) {
        if (flag) {
            if (mAddFriendButton != null) {
                mAddFriendButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (mAddFriendButton != null) {
                mAddFriendButton.setVisibility(View.GONE);
            }
        }
    }

    private void kickButtonShow(boolean flag) {
        if (flag) {
            if (mKickOutButton != null) {
                mKickOutButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (mKickOutButton != null) {
                mKickOutButton.setVisibility(View.GONE);
            }
        }
    }


    private void turnIntoKickOut(WereWolfDialogButton button) {
        mKickOutButton = button;
        button.setVisibility(View.VISIBLE);
        button.setButtonText(MessageUtils.getString(R.string.dialog_user_detail_kick_page));
        button.setButtonOnClickListener(v -> {
            if (mSelectPosition != -1) {
                RoomSocketEngineHelper.sendPositionMessage(Constant.KICK_OUT, mSelectPosition);
                dismissUserDetailDialog();
            }
        });
    }

    private void turnIntoAddFriend(WereWolfDialogButton button) {
        mAddFriendButton = button;
        button.setVisibility(View.VISIBLE);
        button.setButtonText(MessageUtils.getString(R.string.dialog_user_detail_add_page));
        button.setButtonOnClickListener(v -> {
            ApiManager.AddFriendApi(mUserId, null, null, null);
        });
    }

    private void handleButton(UserInfoResult result) {
        /**
         * 定位加好友按钮
         */
        if (!result.is_friend) {
            /**
             * 如果这个人不是好友
             */
            turnIntoAddFriend(mYellowButton);
        }

        /**
         * 首先定位踢人按钮
         */
        if (mWolfGameContext != null) {
            if (mWolfGameContext.isGaming()) {

            } else {
                if (mWolfGameContext.isRoomOwner()) {
                    if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {

                    } else {
                        if (result.is_friend) {
                            if (Utils.checkUserIdIsTourist(mUserId)) {
                                turnIntoKickOut(mPurpleButton);
                            } else {
                                turnIntoKickOut(mYellowButton);
                            }
                        } else {
                            turnIntoKickOut(mPurpleButton);
                        }
                    }
                } else {
                    mPurpleButton.setVisibility(View.GONE);
                }
            }
        }

        /**
         * 定位发送礼物按钮
         */
        turnIntoGift(mPinkButton);

        /**
         * 处理踢人按钮是否显示
         */
        if (mWolfGameContext != null) {
            if (mWolfGameContext.isGaming()) {
                kickButtonShow(false);
            } else {
                if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
                    kickButtonShow(false);
                } else {
                    if (mWolfGameContext.isRoomOwner()) {
                        kickButtonShow(true);
                    } else {
                        kickButtonShow(false);
                    }
                }
            }
        } else {
            kickButtonShow(false);
        }

        /**
         * 处理送礼物按钮
         */
        if (mWolfGameContext != null) {
            if (mWolfGameContext.isGaming()) {

                if (mWolfGameContext.isSunSet()) {
                    sendGiftButtonShow(false);
                }

                if (mWolfGameContext.isDead()) {
                    sendGiftButtonShow(false);
                }

                if (Utils.checkUserIdIsTourist(mUserId)) {
                    sendGiftButtonShow(false);
                } else {
                    if (GlobalUserState.getGlobalState().isTourist()) {
                        sendGiftButtonShow(false);
                    } else {
                        sendGiftButtonShow(true);
                    }
                }
            } else {
                if (Utils.checkUserIdIsTourist(mUserId)) {
                    sendGiftButtonShow(false);
                } else {
                    if (GlobalUserState.getGlobalState().isTourist()) {
                        sendGiftButtonShow(false);
                    } else {
                        sendGiftButtonShow(true);
                    }
                }
            }
        }

        /**
         * 处理添加好友
         */
        if (result.is_friend) {
            addFriendShow(false);
        } else {
            if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
                addFriendShow(false);
            } else {
                addFriendShow(true);
            }
        }

    }

    private void handleUserDetail(UserInfoResult result) {
        mUserSex = result.sex;
        mUserName = result.name;
        mUserIv = result.image;
        mHeadView.setUserSex(result.sex);
        mHeadView.setUserHeadImageUrl(result.image);
        mHeadView.setHeadDecorate(result.avatar_box);
        mHeadView.getRealHeadImage().setBorderWidth(ScreenUtils.dip2px(2));
        mHeadView.getRealHeadImage().setBorderColor(context.getResources().getColor(R.color.werewolf_dialog_member_none_selected));
        userName.setText(result.name);
        userName.handleVipInfo(result.vipInfo, Color.BLACK);
        userLevel.setText(MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level)));
        try {
            userPopular.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));
            userPopular.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (result.uid == null) {
                userUid.setVisibility(View.GONE);
            } else {
                userUid.setVisibility(View.VISIBLE);
                userUid.setText(MessageUtils.getString(R.string.string_user_info_uid, Integer.toString(result.uid)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<String> roleData = new ArrayList<>();
            if (result.role != null && result.role.customs != null && result.role.customs.size() > 0) {
                roleData.addAll(result.role.customs);
            }
            if (roleData.size() > 0) {
                userRoles.setVisibility(View.VISIBLE);
                userRoles.setItemBackGround((index, size) -> UserRolesTools.getUserRolesForUserDetailBackGround(index, size));
                userRoles.addTextViewsWithVipInfo(roleData, Color.parseColor("#ffffff"), 10, true, result.vipInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!Utils.checkUserIdIsTourist(mUserId)) {
            userDetailContainer.setOnClickListener(this);
        }
        WereWolfUserActiveManager.FillActiveImageView(result.active, userActiveIv);
        WereWolfUserActiveManager.FillActiveTextView(result.active, userActiveTv);
    }

    private void handleFamilyDetail(UserInfoResult result) {
        if (result.group != null) {
            setHasFamily(true);

            FamilyTools.FillFamilyBadge(getContext(), result.group, mFamilyBadge);

            mFamilyName.setText(MessageUtils.getString(R.string.string_family_detail_dialog_shortname, result.group.short_name));
            mFamilyContainer.setOnClickListener(v -> {
                FamilyInfoActivity.Launch(getContext(), result.group.group_id);
                dismiss();
            });
            if (TextUtils.equals(result.id, GlobalUserState.getGlobalState().getUserId())) {
                FamilyMemo.UpdateMemo(result.group);
            }
        } else {
            setHasFamily(false);
        }
    }

    private void handleCombatDetail(UserInfoResult result) {
        float total = (result.game.lose + result.game.win + result.game.escape);
        float rate = total > 0 ? (result.game.win / (total)) * 100f : 0;
        float escapeRate = total > 0 ? (result.game.escape / (total)) * 100f : 0;
        String rateString = new BigDecimal(rate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        String escapeRateString = new BigDecimal(escapeRate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        userRate.setText(rateString + "%");
        userEscapeRate.setText(escapeRateString + "%");
        userWins.setText(Integer.toString(result.game.win));
        userLoses.setText(Integer.toString(result.game.lose));
    }


    private void initReportButton() {
        if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
            reportView.setVisibility(View.GONE);
        } else {
            reportView.setVisibility(View.VISIBLE);
        }
    }

    public void showUserDetailDialog() {
        if (!isShowing())
            show();
    }

    public void dismissUserDetailDialog() {
        if (isShowing())
            dismiss();
    }

    private void handUserMainPage() {
        if (mUserInfoResult != null) {
            PersonalInfoActivity.LaunchWithUserInfo(context, mUserInfoResult);
        } else {
            PersonalInfoActivity.Launch(context, mUserId);
        }
    }

    private void handUserMainPageFromOutSide() {
        PersonalInfoActivity.Launch(context, mUserId, true);
    }
}
