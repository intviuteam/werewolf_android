package orangelab.project.game.dialog;

/**
 * Author:1727
 * 2017/3/16
 */

public interface IGameDialog {


    public void showDialog(int dur);

    public void dismiss();
}
