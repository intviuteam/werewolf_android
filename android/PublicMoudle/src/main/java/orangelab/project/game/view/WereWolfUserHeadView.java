package orangelab.project.game.view;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import com.R;

import orangelab.project.common.view.UserHeadView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2017/12/7 下午1:39
 * Mystery
 */

public class WereWolfUserHeadView extends UserHeadView {

    private static final int DEFAULT_HEAD_RESOURCE = R.mipmap.default_head;
    private static final int MALE_SEX = 1;
    public static final int NONE_SEX = -1;


    public WereWolfUserHeadView(@NonNull Context context) {
        this(context, null);
    }

    public WereWolfUserHeadView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WereWolfUserHeadView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setUserHeadImageUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            setUserHeadImageResource(DEFAULT_HEAD_RESOURCE);
            return;
        }
        mHeadUrl = url;
        PicassoUtils.loadImage(getContext(), url, mHeadImage, R.mipmap.default_head);
    }

    public String getHeadUrl() {
        return mHeadUrl;
    }

    public CircleImageView getRealHeadImage() {
        return mHeadImage;
    }

    public void setUserHeadImageResource(int resource) {
        mHeadImage.setImageResource(resource);
    }

    public void setUserSex(int sex) {
        switch (sex) {
            case MALE_SEX:
                mSexImage.setVisibility(View.VISIBLE);
                mSexImage.setImageResource(R.mipmap.ic_male);
                break;
            case NONE_SEX:
                mSexImage.setVisibility(View.GONE);
                break;
            default:
                mSexImage.setVisibility(View.VISIBLE);
                mSexImage.setImageResource(R.mipmap.ic_female);
                break;
        }
    }
}
