package orangelab.project.game.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/12 下午3:57
 * Mystery
 */

public class WereWolfSpeechResult implements Keepable {

    public int duration;
    public boolean dead;
    public int position;
    public String type;
    public List<Integer> lefts;
}
