package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/6 下午9:37
 * Mystery
 */

public class WereWolfPoisonDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfPoisonDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.posion_title), Constant.POISON, TWO_BUTTON);
    }
}
