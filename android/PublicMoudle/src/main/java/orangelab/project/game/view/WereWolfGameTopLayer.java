package orangelab.project.game.view;

import android.view.View;
import android.view.ViewGroup;

import com.R;

import orangelab.project.common.effect.AnimationViewGroup;
import orangelab.project.common.effect.IBaseAnimationSurface;

import com.toolkit.action.Destroyable;

/**
 * game_werewolf
 * 2017/11/27 下午6:58
 * Mystery
 */

public class WereWolfGameTopLayer implements Destroyable {


    private AnimationViewGroup mGiftAnimationView;
    private IBaseAnimationSurface mSecondGiftAnimationView;
    private ViewGroup mGameFakerDialogViewContainer;

    public WereWolfGameTopLayer(View mRootView) {
        mGiftAnimationView = (AnimationViewGroup) mRootView.findViewById(R.id.giftAnimationView);
        mGameFakerDialogViewContainer = (ViewGroup) mRootView.findViewById(R.id.game_fake_dialog_container);
        mSecondGiftAnimationView = (IBaseAnimationSurface) mRootView.findViewById(R.id.secondGiftAnimation);
    }

    public void refreshCard() {
        if (mGiftAnimationView != null) {
            mGiftAnimationView.refreshCard();
        }
    }

    public AnimationViewGroup getGiftAnimationView() {
        return mGiftAnimationView;
    }

    public IBaseAnimationSurface getSecondGiftAnimationView() {
        return mSecondGiftAnimationView;
    }

    public ViewGroup getGameFakerDialogViewContainer() {
        return mGameFakerDialogViewContainer;
    }

    @Override
    public void destroy() {
        if (mGiftAnimationView != null) {
            mGiftAnimationView.destroy();
            mGiftAnimationView = null;
        }
        if (mGameFakerDialogViewContainer != null) {
            mGameFakerDialogViewContainer.removeAllViews();
            mGameFakerDialogViewContainer = null;
        }
        if (mSecondGiftAnimationView != null) {
            mSecondGiftAnimationView.destroy();
            mSecondGiftAnimationView = null;
        }
    }
}
