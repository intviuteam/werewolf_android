package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;

import orangelab.project.common.dialog.SafeDialog;

/**
 * game_werewolf
 * 2017/8/4 上午11:19
 * Mystery
 */

public class SystemMessageDialog extends SafeDialog implements View.OnClickListener {

    private TextView mTitle;
    private TextView mContent;
    private Button mButton;
    private Runnable onDismiss;

    public SystemMessageDialog(@NonNull Context context, Runnable onDismiss) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.layout_dialog_system);
        mTitle = (TextView) findViewById(R.id.system_dialog_title);
        mContent = (TextView) findViewById(R.id.system_dialog_msg);
        mContent.setTextColor(context.getResources().getColor(android.R.color.black));
        mButton = (Button) findViewById(R.id.system_dialog_button);
        mButton.setOnClickListener(this);
        this.onDismiss = onDismiss;
    }

    public void setTitle(String msg) {
        mTitle.setText(msg);
    }

    public void setContent(String msg) {
        mContent.setText(msg);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (this.onDismiss != null) {
            onDismiss.run();
        }
    }


    @Override
    protected void release() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.system_dialog_button) {
            dismiss();
        }
//        switch (v.getId()) {
//            case R.id.system_dialog_button:
//                dismiss();
//                break;
//        }
    }
}
