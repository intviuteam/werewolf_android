package orangelab.project.game.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.R;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/7 上午11:22
 * Mystery
 */

public class WereWolfGamePresentGiftDialog extends WereWolfBaseDialog {
    private ListView listView;
    private TextView placeHolder;
    private View placeHolderView;
    private TextView placeHolderTextView;

    public WereWolfGamePresentGiftDialog(Context context, List<WereWolfGameMember> gameMemberDataList) {
        super(context);
        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_werewolf_intviu_friend, null);

        setTitle(MessageUtils.getString(R.string.gift_game_room_present_string));

        View view = LayoutInflater.from(context).inflate(R.layout.layout_intviufriend, null);
        placeHolder = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolder.setVisibility(View.GONE);
        placeHolderView = view.findViewById(R.id.intviufriend_placeholder_iv);
        placeHolderView.setVisibility(View.GONE);
        placeHolderTextView = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolderTextView.setText(MessageUtils.getString(R.string.gifts_present_dialog_no_people));
        listView = (ListView) view.findViewById(R.id.intviu_friend);
        listView.setVisibility(View.VISIBLE);
        listView.setDividerHeight(0);
        if (gameMemberDataList != null && gameMemberDataList.size() > 0) {
            listView.setVisibility(View.VISIBLE);
            placeHolderView.setVisibility(View.GONE);
            placeHolderTextView.setVisibility(View.GONE);
        } else {
            listView.setVisibility(View.GONE);
            placeHolderView.setVisibility(View.VISIBLE);
            placeHolderTextView.setVisibility(View.VISIBLE);
        }
        WereWolfPresentGiftAdapter adapter = new WereWolfPresentGiftAdapter(context, gameMemberDataList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            WereWolfGameMember gameMemberData = adapter.getItem(position);
            if (Utils.checkUserIdIsTourist(gameMemberData.memberId)) {
                Toast.makeText(context, R.string.gifts_preset_to_tourist, Toast.LENGTH_SHORT).show();
            } else {
                adapter.setSelectPosition(position);
                WereWolfGameMember result = adapter.getSelectPosition();
                if (result != null) {
                    ExhibitionsActivity.startActivityFromGame(
                            context,
                            result.memberId,
                            result.memberName,
                            result.memberSex,
                            result.memberHead,
                            result.memberPosition
                    );
                }
                dismiss();
            }
        });
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setText(MessageUtils.getString(R.string.action_cancel));
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }

    private static class WereWolfPresentGiftAdapter extends BaseAdapter {

        private Context mContext;
        private List<WereWolfGameMember> mGameMemberDatas;
        private int selectPosition = -1;

        public WereWolfPresentGiftAdapter(Context context, List<WereWolfGameMember> gameMemberDatas) {
            mContext = context;
            mGameMemberDatas = gameMemberDatas;
        }

        public void setSelectPosition(int selectPosition) {
            this.selectPosition = selectPosition;
            notifyDataSetChanged();
        }

        public WereWolfGameMember getSelectPosition() {
            if (selectPosition != -1) {
                return mGameMemberDatas.get(selectPosition);
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return mGameMemberDatas.size();
        }

        @Override
        public WereWolfGameMember getItem(int position) {
            return mGameMemberDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (mContext == null) {
                return convertView;
            }
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_presentgift_item, null);
                holder.userhead = (UserHeadView) convertView.findViewById(R.id.intviufriend_ic);
                holder.name = (TextView) convertView.findViewById(R.id.intviufriend_name);
                holder.radio = (RadioButton) convertView.findViewById(R.id.intviufriend_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            WereWolfGameMember item = getItem(position);
            holder.radio.setFocusable(false);
            holder.radio.setClickable(false);
            if (item != null) {
                holder.userhead.setUserHeadImageUrl(item.memberHead);
                holder.userhead.setUserSex(UserHeadView.NONE_SEX);
                holder.name.setText(item.memberName);
            }
            if (selectPosition == position) {
                holder.radio.setChecked(true);
            } else {
                holder.radio.setChecked(false);
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        public UserHeadView userhead;
        public TextView name;
        public RadioButton radio;
    }
}
