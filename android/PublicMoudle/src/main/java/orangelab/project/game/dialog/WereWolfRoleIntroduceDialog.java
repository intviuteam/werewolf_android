package orangelab.project.game.dialog;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import orangelab.project.common.GlobalLanguage;
import com.R;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.game.view.WereWolfDialogButton;

/**
 * game_werewolf
 * 2017/12/16 下午2:06
 * Mystery
 */

public class WereWolfRoleIntroduceDialog extends SafeDialog implements WereWolfDialog {

    private WebView mWebView;
    private WereWolfDialogButton mButton;

    public WereWolfRoleIntroduceDialog(@NonNull Context context) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_werewolf_role_introduce);
        TextView textView = (TextView) findViewById(R.id.id_werewolf_base_dialog_title);
        textView.setText(WereWolfString.getString(R.string.role_introduction_title));

        mWebView = (WebView) findViewById(R.id.role_webview);
        mWebView.setDrawingCacheEnabled(true);
        mButton = (WereWolfDialogButton) findViewById(R.id.id_return_game);
        WebSettings settings = mWebView.getSettings();
        String ua = settings.getUserAgentString();
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setJavaScriptEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(false);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        mButton.setButtonOnClickListener(v -> {
            dismiss();
        });
        mWebView.setBackgroundColor(0); // 设置背景色
        mWebView.getBackground().setAlpha(0); // 设置填充透明度 范围：0-255
        mWebView.loadUrl("file:///android_asset/role_introduce.html?lg=" + GlobalLanguage.CurApkLanguage());
        WereWolfDialogDecorate.DecorateDialog(getWindow());
    }

    @Override
    protected void release() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
    }

    @Override
    public void showDialog(int seconds) {
        if (!isShowing()) {
            show();
        }
    }
}
