package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.utils.CountDownView;
import orangelab.project.game.view.WereWolfDialogButton;

/**
 * game_werewolf
 * 2017/11/29 上午10:58
 * Mystery
 */

public class WereWolfBaseDialog extends SafeDialog implements CountDownView.CountDownFinish, WereWolfDialog {

    private static final String TAG = "WereWolfBaseDialog";

    public static final int ONE_BUTTON = 0;
    public static final int TWO_BUTTON = 1;
    public static final int NONE_BUTTON = -1;

    private Context mContext;

    private TextView mTitle;
    private TextView mCountDown;
    private CountDownView mCountDownView;

    private ViewGroup mContentContainer;
    private ViewGroup mButtonContainer;

    private WereWolfDialogButton mOneButton;
    private WereWolfDialogButton mTwoButtonLeft;
    private WereWolfDialogButton mTwoButtonRight;

    private boolean mDismissWhenFinish = true;
    private boolean isCounting = false;

    private Runnable mCountDownFinish = null;

    public WereWolfBaseDialog(@NonNull Context context) {
        this(context, R.style.DarkDialog);
    }

    public WereWolfBaseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.layout_werewolf_base_dialog);
        mContext = context;
        mTitle = (TextView) findViewById(R.id.id_werewolf_base_dialog_title);
        mCountDown = (TextView) findViewById(R.id.id_werewolf_base_dialog_countdown);

        mContentContainer = (ViewGroup) findViewById(R.id.id_werewolf_base_dialog_content);
        mButtonContainer = (ViewGroup) findViewById(R.id.id_werewolf_base_dialog_button_container);

        WereWolfDialogDecorate.DecorateDialog(getWindow());

        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    protected void setDismissWhenFinish(boolean flag) {
        mDismissWhenFinish = false;
    }

    protected boolean isCounting() {
        return isCounting;
    }

    public void setButtonType(int buttonType) {
        switch (buttonType) {
            case TWO_BUTTON:
                mButtonContainer.addView(View.inflate(mContext, R.layout.layout_werewolf_two_button, null));
                mTwoButtonLeft = (WereWolfDialogButton) findViewById(R.id.id_werewolf_dialog_two_button_cancel);
                mTwoButtonRight = (WereWolfDialogButton) findViewById(R.id.id_werewolf_dialog_two_button_confirm);
                break;

            case ONE_BUTTON:
                mButtonContainer.addView(View.inflate(mContext, R.layout.layout_werewolf_one_button, null));
                mOneButton = (WereWolfDialogButton) findViewById(R.id.id_werewolf_dialog_one_button_confirm);
                break;

            case NONE_BUTTON:
            default:
                break;
        }
    }

    public void setTitle(String text) {
        mTitle.setText(text);
    }

    public void setTitle(int res) {
        mTitle.setText(WereWolfString.getString(res));
    }

    public void setDialogContentView(View view) {
        mContentContainer.addView(view);
    }

    public void setCountDownFinish(Runnable countDownFinish) {
        mCountDownFinish = countDownFinish;
    }

    public void showDialog(int seconds) {
        if (seconds >= 0) {
            if (!isShowing()) {
                show();
            }
            if (seconds != 0) {
                mCountDown.setVisibility(View.VISIBLE);
                mCountDownView = new CountDownView(mCountDown, seconds, this);
                mCountDownView.setCountDownRule(R.string.werewolf_game_countdown);
                mCountDownView.start();
                isCounting = true;
            } else {
                mCountDown.setVisibility(View.GONE);
                if (mCountDownFinish != null) {
                    mCountDownFinish.run();
                }
            }
        } else {
            /**
             * 如果传入负数则不显示对话框
             */
            Log.i(TAG, "showDialog: seconds < 0");
        }
    }

    public Button getOneButton() {
        return mOneButton.getButton();
    }

    public Button getTwoButtonLeft() {
        return mTwoButtonLeft.getButton();
    }

    public Button getTwoButtonRight() {
        return mTwoButtonRight.getButton();
    }

    @Override
    public void dismiss() {
        Log.i(TAG, "dismiss: ");
        super.dismiss();
        isCounting = false;
        mCountDownFinish = null;
    }

    @Override
    protected void release() {
        Log.i(TAG, "release: ");
        if (mCountDownView != null) {
            mCountDownView.cancel();
            mCountDownView = null;
        }
    }

    @Override
    public void onFinish() {
        Log.i(TAG, "onFinish: ");
        isCounting = false;
        if (mCountDownFinish != null) {
            mCountDownFinish.run();
        }
        /**
         * 倒计时结束
         */
        if (mDismissWhenFinish) {
            dismiss();
        }
    }
}
