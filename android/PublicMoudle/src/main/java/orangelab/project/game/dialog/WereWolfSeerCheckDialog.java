package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/1 上午11:00
 * Mystery
 */

public class WereWolfSeerCheckDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfSeerCheckDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.check_card_title), Constant.CHECK, ONE_BUTTON);
    }
}
