package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.R;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.engine.RoomSocketEngineHelper;

/**
 * game_werewolf
 * 2017/12/1 上午11:12
 * Mystery
 */

public class WereWolfRunInElectionDialog extends WereWolfBaseDialog {

    public WereWolfRunInElectionDialog(@NonNull Context context) {
        super(context);
        setTitle(WereWolfString.getString(R.string.election_sheriff));
        setDialogContentView(View.inflate(context, R.layout.layout_werewolf_dialog_run_in_election, null));
        setButtonType(TWO_BUTTON);

        Button left = getTwoButtonLeft();
        left.setText(R.string.cancel_election);
        left.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendElection(false);
            dismiss();
        });

        Button right = getTwoButtonRight();
        right.setText(R.string.confirm_election);
        right.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendElection(true);
            dismiss();
        });
    }
}
