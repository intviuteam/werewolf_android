package orangelab.project.game.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.SafeHandler;

import com.toolkit.action.Destroyable;

import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2017/12/9 上午10:47
 * Mystery
 */

public class WereWolfFriendRequestView implements Destroyable, IFriendRequestView, View.OnClickListener {

    private View mRoot;

    private CircleImageView mCircleImageView;
    private TextView mTextView;
    private Button mApprove;
    private Button mRefuse;

    private String mUserId;
    private String mUserName;
    private String mUserIv;
    private Set<String> mMemo;

    private SafeHandler mSafeHandler = new SafeHandler();

    private Runnable mDismissListener;

    public WereWolfFriendRequestView(Context context, String userId, String userName, String userIv, Set<String> memo) {
        mRoot = View.inflate(context, R.layout.layout_werewolf_friend_request, null);
        mCircleImageView = (CircleImageView) mRoot.findViewById(R.id.request_friend_iv);
        mTextView = (TextView) mRoot.findViewById(R.id.id_werewolf_user_name);
        mApprove = (Button) mRoot.findViewById(R.id.approve);
        mRefuse = (Button) mRoot.findViewById(R.id.refuse);
        mApprove.setOnClickListener(this);
        mRefuse.setOnClickListener(this);
        this.mUserId = userId;
        this.mUserName = userName;
        this.mUserIv = userIv;
        this.mMemo = memo;
        PicassoUtils.loadImage(context, this.mUserIv, mCircleImageView, R.mipmap.default_head);
        this.mTextView.setText(mUserName);
    }

    @Override
    public int getHeight() {
        return ScreenUtils.dip2px(110);
    }

    @Override
    public int getWidth() {
        return ScreenUtils.dip2px(200);
    }

    @Override
    public int getX() {
        return (ScreenUtils.getCurrentScreenWidth() - getWidth()) / 2;
    }

    @Override
    public int getY() {
        return (int) (ScreenUtils.getCurrentScreenHeight() * 0.21f);
    }

    @Override
    public View getView() {
        return mRoot;
    }

    @Override
    public void destroy() {
        mSafeHandler.release();
    }

    @Override
    public String getUserId() {
        return mUserId;
    }

    @Override
    public void setDismissListener(Runnable dismissListener) {
        mDismissListener = dismissListener;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.approve) {
            approve();
        } else if (id == R.id.refuse) {
            refuse();
        }
//        switch (v.getId()) {
//            case R.id.approve:
//                approve();
//                break;
//            case R.id.refuse:
//                refuse();
//                break;
//        }
    }

    private void putIntoMemo() {
        mMemo.add(mUserId);
    }

    private void approve() {
        ApiManager.AcceptFriend(mUserId, (s, e) -> {
            postSafely(() -> {
                if (e == null) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.add_friend_success));
                    UnifiedBridgeHelper.SendAddFriendAction(mUserId);
                    dismiss();
                    putIntoMemo();
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void refuse() {
        ApiManager.RefuseFriend(mUserId, null);
        postSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.refuse_friend_success)));
        dismiss();
        putIntoMemo();
    }

    public void postSafely(Runnable runnable) {
        mSafeHandler.postSafely(runnable);
    }

    public void dismiss() {
        postSafely(() -> {
            if (mDismissListener != null) {
                mDismissListener.run();
            }
            mRoot.setVisibility(View.GONE);
        });
    }
}
