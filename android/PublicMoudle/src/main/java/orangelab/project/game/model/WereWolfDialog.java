package orangelab.project.game.model;

/**
 * game_werewolf
 * 2017/11/30 下午3:10
 * Mystery
 */

public interface WereWolfDialog {

    void showDialog(int seconds);


    void dismiss();

}
