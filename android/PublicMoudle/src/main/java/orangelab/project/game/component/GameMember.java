package orangelab.project.game.component;


import java.io.Serializable;


/**
 * Author:1727
 * 2017/3/3
 */

public class GameMember implements Serializable {

    public static class GameMemberData {
        public int position;
        public String userName;
        public String userId;
        public String userImage;
        public String userLevel;
        public int userSex;
    }
}
