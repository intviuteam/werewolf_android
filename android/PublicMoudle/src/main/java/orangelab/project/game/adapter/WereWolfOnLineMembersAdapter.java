package orangelab.project.game.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import orangelab.project.MainApplication;
import com.R;
import orangelab.project.game.model.WereWolfObserverMember;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 02/02/2018.
 */

public class WereWolfOnLineMembersAdapter extends RecyclerView.Adapter<WereWolfOnLineMembersAdapter.OnLineMembersViewHolder> {

    private List<WereWolfObserverMember> memberList = new ArrayList<>();
    private Context mContext;

    private OnOnLineMemberClickedListener onLineMemberClickedListener;

    public WereWolfOnLineMembersAdapter(Context mContext, List<WereWolfObserverMember> memberList) {
        this.memberList = memberList;
        this.mContext = mContext;
    }

    public void setOnLineMemberClickedListener(OnOnLineMemberClickedListener onLineMemberClickedListener) {
        this.onLineMemberClickedListener = onLineMemberClickedListener;
    }

    public void updateMemberList(List<WereWolfObserverMember> memberList) {
        this.memberList = memberList;
        notifyDataSetChanged();
    }

    @Override
    public OnLineMembersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_game_online_member, null);
        return new OnLineMembersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OnLineMembersViewHolder holder, int position) {
        WereWolfObserverMember member = memberList.get(position);
        if (member != null) {
            PicassoUtils.loadImageByDefault(MainApplication.getInstance().getApplicationContext(), member.memberHead, holder.ivHead, R.mipmap.default_head);
            holder.tvName.setText(member.memberName);
            holder.tvMotto.setText(member.signature);
            holder.tvPosition.setText(String.valueOf(member.memberPosition));
            if (member.isObserver) {
                holder.tvObserver.setVisibility(View.VISIBLE);
                holder.tvUpSeat.setVisibility(View.GONE);
            } else {
                holder.tvObserver.setVisibility(View.GONE);
                holder.tvUpSeat.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }

    class OnLineMembersViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView ivHead;
        TextView tvPosition;//位置标号
        TextView tvMotto;//个性签名
        View tvUpSeat;//座上
        View tvObserver;//观战

        public OnLineMembersViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivHead = (ImageView) itemView.findViewById(R.id.iv_head);
            tvPosition = (TextView) itemView.findViewById(R.id.tv_position);
            tvMotto = (TextView) itemView.findViewById(R.id.tv_motto);
            tvUpSeat = itemView.findViewById(R.id.tv_upseat);
            tvObserver = itemView.findViewById(R.id.tv_observe);

            if (onLineMemberClickedListener != null) {
                itemView.setOnClickListener(v ->
                        onLineMemberClickedListener.onMemberClick(getPosition())
                );
            }
        }
    }

    public interface OnOnLineMemberClickedListener {
        void onMemberClick(int position);
    }
}
