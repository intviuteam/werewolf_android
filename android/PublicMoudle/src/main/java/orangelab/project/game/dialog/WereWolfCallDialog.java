package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;
import com.R;
import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import com.networktoolkit.transport.Constant;
import com.toolkit.ToolKit;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2017/12/1 下午4:38
 * Mystery
 */

public class WereWolfCallDialog extends WereWolfBaseDialog implements CompoundButton.OnCheckedChangeListener {

    public static final String TAG = "WereWolfCallDialog";

    private Context mContext;
    private Button btnSure;
    private Button btnCancel;
    private EditText txtTitle;
    private String lastTitle = "";
    private String selectType = "";
    private TextView callCondition;

    private String roomId;
    private Map<String, RadioButton> mRadioButtonMap = null;
    private EnterRoomResult.ExportConfig mConfig = null;

    private boolean isRequesting = false;

    public WereWolfCallDialog(@NonNull Context context, String lastTitle, String selectType, EnterRoomResult.ExportConfig config) {
        super(context);
        setTitle(WereWolfString.getString(R.string.call_dialog_title));
        mContext = context;
        mConfig = config;
        this.lastTitle = lastTitle;
        this.selectType = selectType;
        this.mRadioButtonMap = new HashMap<>();
        initWindow();
        initView();
    }

    private void initWindow() {
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        window.setAttributes(params);
    }

    private void clearRadio() {
        ToolKit.travelMap(mRadioButtonMap, (key, value) -> {
            value.setChecked(false);
        });
    }

    private void selectRadio(int id, boolean checked) {
        clearRadio();
        ToolKit.travelMap(mRadioButtonMap, (key, value) -> {
            if (value.getId() == id) {
                value.setChecked(checked);
                selectType = key;
            } else {
                value.setChecked(false);
            }
        });
    }

    private void selectRadio(String type) {
        ToolKit.travelMap(mRadioButtonMap, (key, value) -> {
            if (TextUtils.equals(type, key)) {
                value.setChecked(true);
            } else {
                value.setChecked(false);
            }
        });
    }

    private void initView() {
        View view = View.inflate(mContext, R.layout.layout_werewolf_call_dialog, null);
        setDialogContentView(view);
        setButtonType(TWO_BUTTON);
        btnSure = getTwoButtonRight();
        btnCancel = getTwoButtonLeft();

        btnSure.setOnClickListener(v -> {
            try {
                doCall();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnCancel.setOnClickListener(v -> {
            dismiss();
        });

        txtTitle = (EditText) view.findViewById(R.id.call_edittext);

        callCondition = (TextView) view.findViewById(R.id.call_condition);
        callCondition.setVisibility(View.GONE);

        Utils.runSafely(() -> {
            String costMsg = "";
            if (TextUtils.equals(mConfig.cost.type, ExhibitionsConstant.PRICE_TYPE_GOLD)) {
                costMsg = mConfig.cost.value + MessageUtils.getString(R.string.price_gold);
            } else if (TextUtils.equals(mConfig.cost.type, ExhibitionsConstant.PRICE_TYPE_DIAMOND)) {
                costMsg = mConfig.cost.value + MessageUtils.getString(R.string.price_dim);
            } else {
                return;
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                callCondition.setText(Html.fromHtml(MessageUtils.getString(R.string.werewolf_game_call_dialog_message, Integer.toString(mConfig.level), costMsg), Html.FROM_HTML_MODE_LEGACY));
            } else {
                callCondition.setText(Html.fromHtml(MessageUtils.getString(R.string.werewolf_game_call_dialog_message, Integer.toString(mConfig.level), costMsg)));
            }
            callCondition.setVisibility(View.VISIBLE);
        });

        mRadioButtonMap.put(Constant.CALL_TYPE_GAME, (RadioButton) view.findViewById(R.id.call_type_game));
        mRadioButtonMap.put(Constant.CALL_TYPE_HIGH_LEVEL, (RadioButton) view.findViewById(R.id.call_type_high));
        mRadioButtonMap.put(Constant.CALL_TYPE_JOY, (RadioButton) view.findViewById(R.id.call_type_enjoy));
        mRadioButtonMap.put(Constant.CALL_TYPE_LANGUAGE, (RadioButton) view.findViewById(R.id.call_type_language));

        /**
         * 遍历添加监听器
         */
        ToolKit.travelMap(mRadioButtonMap, (key, value) -> {
            value.setOnCheckedChangeListener(WereWolfCallDialog.this);
        });

        if (!TextUtils.isEmpty(lastTitle)) {
            txtTitle.setText(lastTitle);
        }
        if (!TextUtils.isEmpty(selectType)) {
            selectRadio(selectType);
        }
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    private void doCall() {
        if (isRequesting) {
            return;
        }
        String title = txtTitle.getText().toString().trim();
        if (TextUtils.isEmpty(title)) {
            runSafely(RunnableFactory.createMsgToastRunnable(mContext.getString(R.string.tip_content_empty)));
            return;
        }
        if (title.length() < 4) {
            runSafely(RunnableFactory.createMsgToastRunnable(mContext.getString(R.string.call_title_tooshort)));
            return;
        }
        if (TextUtils.isEmpty(selectType)) {
            runSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.call_type_isempty)));
            return;
        }
        ApiManager.GetUserInfoApi(GlobalUserState.getGlobalState().getUserId(), (userInfoResult, e) -> {
            runSafely(() -> {
                if (e == null) {
                    if (userInfoResult.money != null) {
                        long gold = userInfoResult.money.gold;
                        long dim = userInfoResult.money.dim;
                        int limitCost = mConfig.cost.value;
                        String limitType = mConfig.cost.type;
                        if (userInfoResult.game.level < mConfig.level) {
                            runSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.call_dialog_level_enough)));
                            return;
                        }
                        if (TextUtils.equals(limitType, ExhibitionsConstant.PRICE_TYPE_GOLD)) {
                            /**
                             * 如果是金币类型
                             */
                            if (gold < limitCost) {
                                /**
                                 * 金币不足，这里要判断下钻石数量
                                 */
                                int costMaxDim = ((int) limitCost / 66) + 1;//召集一次所需的最大钻石数量
                                if (dim < costMaxDim) {
                                    /**
                                     * 如果钻石也不足，那么提示错误信息
                                     */
                                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.export_call_money_error)).run();
                                    return;
                                }
                            }
                        }

                        if (TextUtils.equals(limitType, ExhibitionsConstant.PRICE_TYPE_DIAMOND)) {
                            if (dim < limitCost) {
                                /**
                                 * 钻石不足，提示错误信息
                                 */
                                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.export_call_money_error)).run();
                                return;
                            }
                        }

                        /**
                         * 如果以上判断均通过，那么开始进行召集操作
                         */
                        RoomSocketEngineHelper.sendExportMsgWithCallBack(title, selectType, result -> {
                            runSafely(() -> {
                                isRequesting = false;
                                AppManager.RoomCall();
                                dismiss();
                            });
                        }, result -> {
                            runSafely(() -> {
                                isRequesting = false;
                                RunnableFactory.createMsgToastRunnable(result).run();
                                AppManager.ClearRoomCall();
                            });
                        });

                    }
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        selectRadio(buttonView.getId(), isChecked);
    }

    @Override
    protected void release() {
        super.release();
    }
}
