package orangelab.project.game.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.R;

import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.ActionListener;
import com.toolkit.action.Destroyable;

import org.json.JSONObject;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/2 下午8:44
 * Mystery
 */

public class WereWolfGameNoticeBoardView extends WereWolfGameProcess implements Destroyable {

    private WereWolfGameContext mWolfGameContext;

    private View mSystemBoardContainer;
    private View mBoardDead = null;
    private TextView mSystemBoardTitle;
    private TextView mSystemBoardMessage;
    private TextView mSystemBoardCountDown;
    private CountDownTaskManager mTaskManager = null;

    public WereWolfGameNoticeBoardView(WereWolfGameContext wolfGameContext) {
        mWolfGameContext = wolfGameContext;
        mWolfGameContext.registerProcessListener(this);

        View rootView = mWolfGameContext.getGameRootView();
        mSystemBoardContainer = rootView.findViewById(R.id.id_werewolf_game_sys_board_container);
        mSystemBoardTitle = (TextView) rootView.findViewById(R.id.id_werewolf_game_sys_board_title);
        mSystemBoardMessage = (TextView) rootView.findViewById(R.id.id_werewolf_game_sys_board_msg);
        mSystemBoardCountDown = (TextView) rootView.findViewById(R.id.id_werewolf_game_sys_board_countdown);
        mBoardDead = rootView.findViewById(R.id.id_werewolf_board_dead);
        mTaskManager = new CountDownTaskManager();
        mTaskManager.bindTarget(mSystemBoardCountDown);
        mTaskManager.setTextDefaultSize(10);

        turnIntoPrepareState();

    }

    public void setSystemBoardTitle(String title) {
        mSystemBoardTitle.setText(title);
    }

    public void setSystemBoardMessage(String message) {
        mSystemBoardMessage.setText(message);
    }

    public void setSystemBoardCountDown(String type, int dur) {
        if (dur == 0) {
            if (mSystemBoardCountDown.getVisibility() != View.GONE) {
                mSystemBoardCountDown.setVisibility(View.GONE);
            }
            return;
        }
        if (mTaskManager != null) {
            mTaskManager.addCountDownTask(type, dur, new ActionListener() {
                @Override
                public void onStart() {
                    exec(() -> {
                        if (mSystemBoardCountDown.getVisibility() != View.VISIBLE) {
                            mSystemBoardCountDown.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void onComplete() {
                    exec(() -> {
                        if (mSystemBoardCountDown.getVisibility() != View.GONE) {
                            mSystemBoardCountDown.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onError(Throwable e) {

                }
            });
        }
    }

    public void updateBackGround(boolean isSunSet) {
//        if (isSunSet) {
//            mSystemBoardContainer.setBackgroundResource(R.mipmap.ico_werewolf_game_sysboard_sunset);
//        } else {
//            mSystemBoardContainer.setBackgroundResource(R.mipmap.ico_werewolf_game_sysboard_sunup);
//        }
    }

    public void turnIntoPrepareState() {
        String callTitle = mWolfGameContext.getCurCallTitle();
        if (TextUtils.isEmpty(callTitle)) {
            setSystemBoardTitle(WereWolfString.getString(R.string.werewolf_game_sys_board_prepare));
            setSystemBoardMessage("");
        } else {
            setSystemBoardTitle(WereWolfString.getString(R.string.werewolf_game_sys_board_call_title));
            setSystemBoardMessage(callTitle);
        }
        mBoardDead.setVisibility(View.GONE);
    }

    public void exec(Runnable runnable) {
        exec(runnable, 0);
    }

    public void exec(Runnable runnable, long delay) {
        try {
            mWolfGameContext.check().post(runnable, delay);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        mWolfGameContext.unRegisterProcessListener(this);
        if (mTaskManager != null) {
            mTaskManager.destroy();
            mTaskManager = null;
        }
        mWolfGameContext = null;
    }

    @Override
    public boolean handleExport(String type, String title, JSONObject jsonObject) {
        if (mWolfGameContext.isGaming()) {
            return false;
        }
        if (TextUtils.isEmpty(title)) {
            return false;
        }
        turnIntoPrepareState();
        return false;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        updateDead();
        setSystemBoardTitle(MessageUtils.getSystemBoardGameStartString());
        setSystemBoardMessage(MessageUtils.getString(R.string.dialog_start_title));
        setSystemBoardCountDown(Constant.START, startResult.duration / 1000);
        return false;
    }

    @Override
    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        setSystemBoardMessage(MessageUtils.getSystemBoardAssignedRoleString());
        setSystemBoardCountDown(Constant.ASSIGNED_ROLE, roleResult.duration / 1000);
        return false;
    }

    @Override
    public boolean handleLink(int duration, JSONObject jsonObject) {
        setSystemBoardMessage(MessageUtils.getSystemBoardLinkString());
        setSystemBoardCountDown(Constant.LINK, duration);
        return false;
    }

    @Override
    public boolean handleLinkResult(LinkResult linkResult) {
        setSystemBoardMessage(MessageUtils.getSystemBoardLinkResultString());
        setSystemBoardCountDown(Constant.LINK_RESULT, linkResult.duration / 1000);
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        updateDead();
        updateBackGround(true);
        setSystemBoardTitle(MessageUtils.getSystemBoardSunStateString(dayIndex, Constant.SUNSET));
        setSystemBoardMessage(MessageUtils.getBoardSunSetString());
        setSystemBoardCountDown(Constant.SUNSET, dur);
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        updateDead();
        updateBackGround(false);
        setSystemBoardTitle(MessageUtils.getSystemBoardSunStateString(dayIndex, Constant.SUNUP));
        return false;
    }

    @Override
    public boolean handleApply(int durSeconds, JSONObject jsonObject) {
        setSystemBoardMessage(MessageUtils.getSystemBoardApplyString());
        setSystemBoardCountDown(Constant.APPLY, durSeconds);
        return false;
    }

    @Override
    public boolean handleHandOver(WereWolfGameSurvivorsInfo info) {
        setSystemBoardMessage(MessageUtils.getSystemBoardHandOverMessage());
        setSystemBoardCountDown(Constant.HAND_OVER, info.duration / 1000);
        return false;
    }

    @Override
    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        final int dur = RoomSocketEngineHelper.getOptDurTime(jsonObject);
        if (to != -1) {
            setSystemBoardMessage(MessageUtils.getSheriffTurnResult(from, to));
            setSystemBoardCountDown(Constant.HAND_OVER_RESULT, dur / 2);
            mWolfGameContext.check().post(() -> {
                setSystemBoardMessage(MessageUtils.getSheriffResultString(to));
                setSystemBoardCountDown(Constant.HAND_OVER_RESULT, dur / 2);
            }, (dur / 2) * 1000);
        } else {
            setSystemBoardMessage(MessageUtils.getHandoverResultNoSheriffSystemMessage());
            setSystemBoardCountDown(Constant.HAND_OVER_RESULT, dur);
        }
        return false;
    }

    @Override
    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        setSystemBoardMessage(MessageUtils.getSystemBoardVoteString());
        setSystemBoardCountDown(Constant.VOTE, dur);
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo info) {
        updateDead();
        if (info.need_pk) {
            return false;
        }
        if (info.type.equals(Constant.APPLY)) {
            List<Integer> positions = info.positions;
            if (positions != null && positions.size() > 0) {
                int position = positions.get(0) + 1;
                String msg = MessageUtils.getSystemBoardVoteSheriffString(position);
                setSystemBoardMessage(msg);
            } else {
                setSystemBoardMessage(MessageUtils.getSystemBoardApplyResultNoSheriffString());
            }
            setSystemBoardCountDown(Constant.VOTE_RESULT, info.duration / 1000);
        } else {
            String msg = MessageUtils.getBoardVoteResult(info);
            if (msg != null) {
                setSystemBoardMessage(msg);
            }
            setSystemBoardCountDown(Constant.VOTE_RESULT, info.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleSpeech(int position, int duration, boolean isDead, String type, List<Integer> lefts, JSONObject jsonObject) {
        if (mWolfGameContext.isOverMaxPosition(position)) {
            return false;
        }
        if (TextUtils.equals(type, Constant.APPLY)) {
            setSystemBoardMessage(MessageUtils.getSystemBoardApplyResultString());
        }
        String msg = MessageUtils.getSystemBoardSpeechString(position, isDead);
        setSystemBoardMessage(msg);
        if (position == mWolfGameContext.getPersonalPosition()) {
            setSystemBoardCountDown(Constant.SELF_SPEECH, duration);
        } else {
            setSystemBoardCountDown(Constant.SPEECH, duration);
        }
        return false;
    }

    @Override
    public boolean handleDeathInfo(DeathInfo deathInfo) {
        updateDead();
        String msg = MessageUtils.getSystemBoardDeathInfoString(deathInfo);
        setSystemBoardMessage(msg);
        return false;
    }

    @Override
    public boolean handleTakeAway(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        updateDead();
        setSystemBoardMessage(MessageUtils.getSystemBoardTakeAwayString());
        setSystemBoardCountDown(Constant.TAKEAWAY, info.duration / 1000);
        return false;
    }

    @Override
    public boolean handleTakeAwayResult(DeathInfo death) {
        updateDead();
        if (death != null && death.death_info != null && death.death_info.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (DeathInfo.DeathInfoItem key : death.death_info) {
                if (key != null) {
                    if (key.reason.equals(Constant.STATE_TOKEN_AWAY)) {
                        sb.append(MessageUtils.getHunterKillerMessage(key.killer + 1, key.killed + 1));
                        continue;
                    }
                    if (key.reason.equals(Constant.STATE_LINKED)) {
                        sb.append("\n" + MessageUtils.getString(R.string.system_vote_death_by_link, Integer.toString(key.killed + 1)));
                        continue;
                    }
                } else {
                    setSystemBoardMessage("");
                    setSystemBoardCountDown(Constant.TAKE_AWAY_RESULT, 0);
                }
            }
            setSystemBoardMessage(sb.toString());
        } else {
            setSystemBoardMessage("");
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        if (position != -1) {
            setSystemBoardMessage(MessageUtils.getSheriffResultString(position));
            setSystemBoardCountDown(Constant.SHERIFF_RESULT, durSeconds);
        } else {
            setSystemBoardMessage(MessageUtils.getString(R.string.system_apply_no_result));
            setSystemBoardCountDown(Constant.SHERIFF_RESULT, durSeconds);
        }
        return false;
    }

    @Override
    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        if (position == mWolfGameContext.getPersonalPosition()) {
            setSystemBoardCountDown(Constant.SELF_SPEECH, leftTime);
        } else {
            setSystemBoardCountDown(Constant.SPEECH, leftTime);
        }
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        updateDead();
        return false;
    }

    @Override
    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        setSystemBoardMessage(MessageUtils.getString(R.string.wait_werewolf_king));
        setSystemBoardCountDown(Constant.BOOM_AWAY, survivorsInfo.duration / 1000);
        return false;
    }

    @Override
    public boolean handleBoomAwayResult(DeathInfo death) {
        updateDead();
        if (death != null && death.death_info != null && death.death_info.size() > 0) {
            for (DeathInfo.DeathInfoItem key : death.death_info) {
                if (key != null) {
                    setSystemBoardMessage(MessageUtils.getString(R.string.werewolf_king_kill, (key.killed + 1) + ""));
                    setSystemBoardCountDown(Constant.BOOM_AWAY_RESULT, death.duration / 1000);
                } else {
                    setSystemBoardCountDown(Constant.BOOM_AWAY_RESULT, 0);
                }
            }
        } else {
            setSystemBoardMessage("");
        }
        return false;
    }

    @Override
    public boolean handleSpeechDirection(int dur, int left, int right, JSONObject jsonObject) {
        if (dur > 0) {
            setSystemBoardMessage(MessageUtils.getString(R.string.wait_speech_direction));
            setSystemBoardCountDown(Constant.SPEECH_DIRECTION, dur);
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        updateDead();
        setSystemBoardMessage(MessageUtils.getString(R.string.string_werewolf_knight_title));
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        if (!mWolfGameContext.isGaming()) {
            turnIntoPrepareState();
            clearCountDown();
        } else {
            updateDead();
            if (restoreResult.game_info != null && restoreResult.game_info.speech_info != null) {
                RestoreResult.SpeechInfo speechInfo = restoreResult.game_info.speech_info;
                int speechPosition = speechInfo.current.position + 1;
                if (speechPosition > 0) {
                    int duration = speechInfo.duration / 1000;
                    String msg = MessageUtils.getSystemBoardSpeechString(speechPosition, false);
                    setSystemBoardMessage(msg);
                    if (speechPosition == mWolfGameContext.getPersonalPosition()) {
                        setSystemBoardCountDown(Constant.SELF_SPEECH, duration);
                    } else {
                        setSystemBoardCountDown(Constant.SPEECH, duration);
                    }
                }
            }
            if (restoreResult.game_info != null && restoreResult.game_info.day_info != null) {
                int dayIndex = restoreResult.game_info.day_info.day_index;
                int duration = restoreResult.game_info.day_info.duration / 1000;
                if (mWolfGameContext.isSunSet()) {
                    handleSunSet(dayIndex, duration, new JSONObject());
                } else {
                    handleSunUp(dayIndex, new JSONObject());
                }
            }
        }
        return false;
    }

    public void updateDead() {
        if (mWolfGameContext.isDead()) {
            mBoardDead.setVisibility(View.VISIBLE);
        } else {
            mBoardDead.setVisibility(View.GONE);
        }
    }

    private void clearCountDown() {
        mTaskManager.clear();
        mSystemBoardCountDown.setVisibility(View.GONE);
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        updateDead();
        setSystemBoardTitle(WereWolfString.getString(R.string.game_prepare));
        setSystemBoardMessage("");
        setSystemBoardCountDown(Constant.GAMEOVER, 0);
        clearCountDown();
        return false;
    }
}
