package orangelab.project.game.model;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.game.WereWolfConstant;

import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Keepable;

import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.game.view.WereWolfMemberTagUtils;

/**
 * game_werewolf
 * 2017/11/29 下午12:09
 * Mystery
 */

public class WereWolfSelectMemberHolder implements Keepable {

    private TextView mPosition;
    private CircleImageView mImageView;
    private View mLastSelectView;
    private TextView mName;
    private View mSelectView;
    private Context mContext;

    private ImageView mMidBadge;
    private ImageView mRightBadge;
    private ImageView mRoleTag;

    private int position = -1;
    private int lastProtectPosition = -1;
    private int myselfPosition = -1;


    public WereWolfSelectMemberHolder(View root) {
        mContext = root.getContext();
        mPosition = (TextView) root.findViewById(R.id.id_werewolf_dialog_member_position);
        mImageView = (CircleImageView) root.findViewById(R.id.id_werewolf_dialog_member_head);
        mName = (TextView) root.findViewById(R.id.id_werewolf_dialog_member_name);
        mSelectView = root.findViewById(R.id.id_werewolf_dialog_member_selected);
        mLastSelectView = root.findViewById(R.id.id_werewolf_dialog_member_last_select);
        mRoleTag = (ImageView) root.findViewById(R.id.id_werewolf_member_role);
        mMidBadge = (ImageView) root.findViewById(R.id.id_werewolf_member_badge_mid);
        mRightBadge = (ImageView) root.findViewById(R.id.id_werewolf_member_badge_right);
    }

    public void setPosition(int position) {
        this.position = position;
        mPosition.setText(Integer.toString(position));
    }

    public void setName(String name) {
        mName.setText(name);
    }

    public void setHead(String url) {
        PicassoUtils.loadImage(mContext, url, mImageView, R.mipmap.default_head);
    }

    public void setSelfPosition(int position) {
        this.myselfPosition = position;
    }

    public void setLastProtectPosition(int position) {
        this.lastProtectPosition = position;
        if (lastProtectPosition == this.position && lastProtectPosition != -1) {
            /**
             * 上次保护的是自己
             */
            mLastSelectView.setVisibility(View.VISIBLE);
        } else {
            mLastSelectView.setVisibility(View.GONE);
        }
    }

    public void updatePositionView() {
        if (myselfPosition == position) {
//            mPosition.setBackgroundResource(R.drawable.drawable_werewolf_dialog_member_position_text_bg);
//            mPosition.setTextColor(mContext.getResources().getColor(R.color.werewolf_dialog_member_myself_text));
            mImageView.setBorderColorResource(R.color.werewolf_dialog_member_myself);
        } else {
            mImageView.setBorderColorResource(R.color.werewolf_dialog_member_none_selected);
//            mPosition.setBackgroundResource(R.drawable.drawable_werewolf_dialog_member_position_text_bg);
//            mPosition.setTextColor(mContext.getResources().getColor(android.R.color.white));
        }
    }

    public void setSelect(boolean flag) {
        if (flag) {
            mSelectView.setVisibility(View.VISIBLE);
//            mImageView.setBorderWidth(ScreenUtils.dip2px(4));
//            mImageView.setBorderColorResource(R.color.werewolf_dialog_member_selected);
//            mPosition.setBackgroundResource(R.drawable.drawable_werewolf_member_position);
//            mPosition.setTextColor(mContext.getResources().getColor(android.R.color.white));
        } else {
            mSelectView.setVisibility(View.GONE);
            /**
             * 如果是自己那么展示黄色，否则展示蓝色
             */
//            mImageView.setBorderWidth(ScreenUtils.dip2px(2));
//            updatePositionView();
        }
    }

    public void setRole(String role) {
        if (!TextUtils.isEmpty(role)) {
            mRoleTag.setVisibility(View.VISIBLE);
            WereWolfMemberTagUtils.setRoleTag(mRoleTag, false, role);
            return;
        }
        mRoleTag.setVisibility(View.GONE);
//        if (isDemon) {
//            mRoleText.setVisibility(View.VISIBLE);
//            mRoleText.setText(MessageUtils.getRealRoleForMember(WereWolfConstant.ROLE_DEMON));
//            return;
//        }
//        if (isKing) {
//            mRoleText.setVisibility(View.VISIBLE);
//            mRoleText.setText(MessageUtils.getRealRoleForMember(Constant.ROLE_WEREWOLF_KING));
//            return;
//        }
//        if (isWolf) {
//            mRoleText.setVisibility(View.VISIBLE);
//            mRoleText.setText(MessageUtils.getRealRoleForMember(Constant.ROLE_WOLF));
//            return;
//        }
//        if (!TextUtils.isEmpty(role)) {
//            mRoleText.setVisibility(View.VISIBLE);
//            mRoleText.setText(MessageUtils.getRealRoleForMember(role));
//            return;
//        }
//        mRoleText.setVisibility(View.GONE);
    }

    public void setBadge(boolean isSheriff, boolean isLover) {
        if (isSheriff) {
            mRightBadge.setVisibility(View.VISIBLE);
            mRightBadge.setImageResource(R.mipmap.ico_werewolf_game_member_sheriff);
            if (isLover) {
                mMidBadge.setVisibility(View.VISIBLE);
                mMidBadge.setImageResource(R.mipmap.ico_werewolf_game_member_lovers);
            } else {
                mMidBadge.setVisibility(View.INVISIBLE);
            }
        } else {
            if (isLover) {
                mRightBadge.setVisibility(View.VISIBLE);
                mRightBadge.setImageResource(R.mipmap.ico_werewolf_game_member_lovers);
            } else {
                mRightBadge.setVisibility(View.INVISIBLE);
            }
            mMidBadge.setVisibility(View.INVISIBLE);
        }
    }

    public void showAsSelectMember(WereWolfSelectMember member) {
        setPosition(member.memberPosition);
        setHead(member.memberImage);
        setName(member.memberName);
    }
}
