package orangelab.project.game.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.R;

import orangelab.project.common.effect.msgdecor.MsgDecorHandler;
import orangelab.project.common.effect.msgdecor.MsgDecorToolKit;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.model.WereWolfGameMessage;
import orangelab.project.game.model.WereWolfGameMessageViewHolder;

import com.toolkit.action.Destroyable;

import java.util.LinkedList;
import java.util.List;

/**
 * game_werewolf
 * 2017/12/3 下午2:52
 * Mystery
 */

public class WereWolfGameMessageAdapter extends BaseAdapter implements Destroyable {

    private static final String TAG = "WereWolfGameMessageAdap";

    private final int mAllMessageMaxSize;//总消息最大条数
    private final int mPlayerMessageMaxSize;//玩家消息最大条数
    private final int mObserverMessageMaxSize;//观战消息最大条数

    private List<WereWolfGameMessage> nativeMessages = new LinkedList<>();//所有的消息列表,后续有新需求可直接用
    private List<WereWolfGameMessage> mData = new LinkedList<>();//展示消息列表
    private List<WereWolfGameMessage> playerMessages = new LinkedList<>();//玩家消息列表
    private List<WereWolfGameMessage> observerMessages = new LinkedList<>();//观战消息列表
    private boolean isObserver = false;//当前是否是观战消息列表

    private WereWolfGameContext mWolfGameContext;

    public WereWolfGameMessageAdapter(WereWolfGameContext wolfGameContext) {
        mWolfGameContext = wolfGameContext;
        mAllMessageMaxSize = mWolfGameContext.getWereWolfMessageMaxCount();
        mPlayerMessageMaxSize = mWolfGameContext.getmWereWolfPlayerMessageMaxCount();
        mObserverMessageMaxSize = mWolfGameContext.getmWereWolfObserverMessageMaxCount();
    }

    public void updateDayState() {
        for (WereWolfGameMessage message : mData) {
            message.setSunSet(mWolfGameContext.isSunSet());
        }
        notifyDataSetChanged();
    }

    public void addMessage(WereWolfGameMessage message) {
        if (message != null) {
            message.setSunSet(mWolfGameContext.isSunSet());
            if (TextUtils.equals(WereWolfGameMessage.TYPE_OBSERVER_DEFAULT, message.getType())) {
                //观战消息
                if (observerMessages.size() > mObserverMessageMaxSize) {
                    observerMessages.remove(0);
                }
                observerMessages.add(message);
                if (isObserver) {
                    mData = observerMessages;
                    notifyDataSetChanged();
                }
            } else {
                //玩家消息
                if (playerMessages.size() > mPlayerMessageMaxSize) {
                    playerMessages.remove(0);
                }
                playerMessages.add(message);
                if (!isObserver) {
                    mData = playerMessages;
                    notifyDataSetChanged();
                }
            }
        }
    }

    //wwq 2018-02-08 这里根据type切换为玩家消息列表//
    public void switchToPlayerMessage() {
        isObserver = false;
        mData = playerMessages;
        notifyDataSetChanged();
    }

    //wwq 2018-02-08 这里根据type切换为观战消息列表//
    public void switchToObserverMessage() {
        isObserver = true;
        mData = observerMessages;
        notifyDataSetChanged();
    }

    public void clearAll() {
        mData.clear();
        notifyDataSetChanged();

        nativeMessages.clear();
        playerMessages.clear();
        observerMessages.clear();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public WereWolfGameMessage getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * 处理消息View
         */
        WereWolfGameMessageViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(mWolfGameContext.getAndroidContext(), R.layout.layout_werewolf_msg_list_item, null);
            viewHolder = new WereWolfGameMessageViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (WereWolfGameMessageViewHolder) convertView.getTag();
        }
        WereWolfGameMessage message = getItem(position);
        String type = message.getType();
        if (TextUtils.equals(type, WereWolfGameMessage.TYPE_MEMBER)) {
            /**
             * 玩家消息
             */
            viewHolder.turnIntoMemberMessage();
            viewHolder.setMemberPosition(message.getFromPosition());
            viewHolder.setMemberMessage(message.getMessage());

            /**
             * 测试自定义聊天框
             */
            View bg = viewHolder.getMemberMessageInnerContainer();
            if (!MsgDecorToolKit.setNinePatchForViewBackGround(bg, viewHolder.getMemberMessage(), message.getMessageDecor())) {
                int padding = WereWolfGameMessageViewHolder.PADDING;
                viewHolder.getMemberMessage().setPadding(padding, padding, padding, padding);
            }

        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_MEMBER_GIFT)) {

            viewHolder.turnIntoMemberMessageForGift();
            viewHolder.setMemberPosition(message.getFromPosition());
            viewHolder.setMemberMessage(message.getMessage());
            viewHolder.setMemberGift(message.getMessage_gift());

        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_JUDGE_RED)) {

            viewHolder.turnIntoJudgeMessage();
            viewHolder.turnJudgeBackGroundRed();
            viewHolder.setJudgeMessage(message.getMessage());
            viewHolder.setJudgeTypeSunSet(mWolfGameContext.isSunSet());
            viewHolder.setJudgeTypeMessage(message.getMessage_type());

        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_JUDGE_BLUE)) {

            viewHolder.turnIntoJudgeMessage();
            viewHolder.turnJudgeBackGroundBlue();
            viewHolder.setJudgeMessage(message.getMessage());
            viewHolder.setJudgeTypeSunSet(mWolfGameContext.isSunSet());
            viewHolder.setJudgeTypeMessage(message.getMessage_type());

        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_JUDGE_VOTE)) {

            viewHolder.turnIntoJudgeMessageForVote();
            viewHolder.turnJudgeBackGroundBlue();
            viewHolder.setJudgeTypeMessage(message.getMessage_type());
            viewHolder.setJudgeTypeSunSet(mWolfGameContext.isSunSet());
            viewHolder.setJudgeVoteInfo(mWolfGameContext, message.getVote_info());

        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_SYSTEM)) {

            viewHolder.turnIntoSystemMessage();
            viewHolder.setSystemMessage(message.getMessage());


        } else if (TextUtils.equals(type, WereWolfGameMessage.TYPE_OBSERVER_DEFAULT)) {

            viewHolder.turnIntoMemberMessage();
            viewHolder.setMemberPosition(message.getFromPosition());
            viewHolder.setMemberMessage(message.getMessage());

            /**
             * 测试自定义聊天框
             */
            View bg = viewHolder.getMemberMessageInnerContainer();
            if (!MsgDecorToolKit.setNinePatchForViewBackGround(bg, viewHolder.getMemberMessage(), message.getMessageDecor())) {
                int padding = WereWolfGameMessageViewHolder.PADDING;
                viewHolder.getMemberMessageInnerContainer().setPadding(padding, padding, padding, padding);
            }

        }
        return convertView;
    }

    @Override
    public void destroy() {
        mData.clear();
        mWolfGameContext = null;
    }
}
