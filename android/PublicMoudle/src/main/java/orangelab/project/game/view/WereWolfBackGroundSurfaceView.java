package orangelab.project.game.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.R;
import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.utils.BitmapDecodeUtils;


/**
 * game_werewolf
 * 2018/6/13 下午2:57
 * Mystery
 */
public class WereWolfBackGroundSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "WereWolfBGSurfaceView";

    private SurfaceHolder mHolder;

    private HandlerThread mHandlerThread;
    private Handler mHandler;
    private boolean switchFlag = false;

    private int mScreenWidth = 0;
    private int mScreenHeight = 0;

    private Paint mPaint = new Paint();
    private long mAlphaChangeFrame = 50;
    private boolean mIsSunSet = false;

    /**
     * 背景区域
     */
    private Bitmap mBackGroundBitmap = null;
    private Rect mBackGroundRect;
    private int mBackGroundIndex = -1;
    private boolean mNeedRefreshBg = false;


    /**
     * 火焰
     */
    private Bitmap mFireBitmap = null;
    private Rect mFireRect = null;
    private int mFireIndex = -1;
    private boolean mNeedRefreshFire = false;
    private long mFireFrameTime = 100;

    /**
     * 灯光
     */
    private Bitmap mLightBitmap = null;
    private Rect mLightRect = null;
    private int mLightIndex = -1;
    private long mLightFrameTime = 1000;
    private boolean mNeedRefreshLight = false;


    /**
     * 鸟
     */
    private Bitmap mBirdBitmap = null;
    private Rect mBirdRect = null;
    private int mBirdIndex = -1;
    private long mBirdFrameTime = 200;
    private boolean mNeedRefreshBird = false;

    /**
     * 旗子
     */
    private Bitmap mFlagBitmap = null;
    private Rect mFlagRect = null;
    private int mFlagIndex = -1;
    private long mFlagFrameTime = mBirdFrameTime;
    private boolean mNeedRefreshFlag = false;

    private SparseArray<Bitmap> mBitmapSparseArray = new SparseArray<>();


    private int[] mBackGroundFrames = {
            R.mipmap.ico_werewolf_bg_1,
            R.mipmap.ico_werewolf_bg_2,
            R.mipmap.ico_werewolf_bg_3,
            R.mipmap.ico_werewolf_bg_4,
            R.mipmap.ico_werewolf_bg_5,
            R.mipmap.ico_werewolf_bg_6,
            R.mipmap.ico_werewolf_bg_7,
            R.mipmap.ico_werewolf_bg_8,
            R.mipmap.ico_werewolf_bg_9,
            R.mipmap.ico_werewolf_bg_10
    };


    private int[] mFiresFrames = {
            R.mipmap.ico_werewolf_bg_fire_1,
            R.mipmap.ico_werewolf_bg_fire_2,
            R.mipmap.ico_werewolf_bg_fire_3,
            R.mipmap.ico_werewolf_bg_fire_4,
            R.mipmap.ico_werewolf_bg_fire_5,
            R.mipmap.ico_werewolf_bg_fire_5,
            R.mipmap.ico_werewolf_bg_fire_4,
            R.mipmap.ico_werewolf_bg_fire_3,
            R.mipmap.ico_werewolf_bg_fire_2,
            R.mipmap.ico_werewolf_bg_fire_1,
    };


    private Runnable mFireDrawRunnable = new Runnable() {
        @Override
        public void run() {
            /**
             * 处理火焰帧数变动
             */
            drawFire(true);
        }
    };

    private int totalMemory = 0;

    private int[] mLightFrames = {
            R.mipmap.ico_werewolf_light_1,
            R.mipmap.ico_werewolf_light_2,
            R.mipmap.ico_werewolf_light_3,
            R.mipmap.ico_werewolf_light_4,
            R.mipmap.ico_werewolf_light_5,
    };


    private Runnable mLightDrawRunnable = new Runnable() {
        @Override
        public void run() {
            /**
             * 处理灯光帧数变动
             */
            drawLight(true);

        }
    };

    private int[] mBirdFrames = {
            R.mipmap.ico_werewolf_bird_1,
            R.mipmap.ico_werewolf_bird_2,
            R.mipmap.ico_werewolf_bird_3,
            R.mipmap.ico_werewolf_bird_4,
            R.mipmap.ico_werewolf_bird_5,
            R.mipmap.ico_werewolf_bird_6,
            R.mipmap.ico_werewolf_bird_7,
    };

    private Runnable mBirdDrawRunnable = new Runnable() {
        @Override
        public void run() {
            /**
             * 处理鸟帧数变动
             */
            drawBird(true);
        }
    };

    private int[] mFlagFrames = {
            R.mipmap.ico_werewolf_flag_1,
            R.mipmap.ico_werewolf_flag_2,
            R.mipmap.ico_werewolf_flag_3,
            R.mipmap.ico_werewolf_flag_4,
            R.mipmap.ico_werewolf_flag_5,
            R.mipmap.ico_werewolf_flag_6,
            R.mipmap.ico_werewolf_flag_7,
            R.mipmap.ico_werewolf_flag_8,
            R.mipmap.ico_werewolf_flag_9,
            R.mipmap.ico_werewolf_flag_10,
            R.mipmap.ico_werewolf_flag_11,
            R.mipmap.ico_werewolf_flag_12,
            R.mipmap.ico_werewolf_flag_13,
            R.mipmap.ico_werewolf_flag_14
    };

    private Runnable mFlagDrawRunnable = new Runnable() {
        @Override
        public void run() {
            /**
             * 处理旗子变动
             */
            drawFlag(true);
        }
    };


    public WereWolfBackGroundSurfaceView(Context context) {
        this(context, null);
    }

    public WereWolfBackGroundSurfaceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WereWolfBackGroundSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScreenWidth = ScreenUtils.getCurrentScreenWidth();
        mScreenHeight = ScreenUtils.getCurrentScreenHeight();

        initRect();

        mHolder = getHolder();
        mHolder.addCallback(this);
        setZOrderMediaOverlay(true);
        mHolder.setFormat(PixelFormat.TRANSPARENT);
        mHandlerThread = new HandlerThread(TAG);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    private void initRect() {
        /**
         * 计算背景用的区域
         */
        mBackGroundRect = new Rect(0, 0, mScreenWidth, mScreenHeight);

        /**
         * 计算火焰使用的区域
         */
        int fireLeft = mScreenWidth * 83 / 720;
        int fireTop = mScreenHeight * 879 / 1334;
        int fireRight = fireLeft + (mScreenWidth * 304 / 720);
        int fireBottom = fireTop + (mScreenHeight * 177 / 1334);

        mFireRect = new Rect(fireLeft, fireTop, fireRight, fireBottom);

        /**
         * 计算灯光使用区域
         */
        int lightLeft = mScreenWidth * 88 / 720;
        int lightTop = mScreenHeight * 765 / 1334;
        int lightRight = lightLeft + (mScreenWidth * 247) / 720;
        int lightBottom = lightTop + (mScreenHeight * 113) / 1334;

        mLightRect = new Rect(lightLeft, lightTop, lightRight, lightBottom);

        /**
         * 计算鸟的使用区域
         */
        int birdLeft = mScreenWidth * 338 / 720;
        int birdTop = mScreenHeight * 554 / 1334;
        int birdRight = birdLeft + (mScreenWidth * 156) / 720;
        int birdBottom = birdTop + (mScreenHeight * 54) / 1334;

        mBirdRect = new Rect(birdLeft, birdTop, birdRight, birdBottom);

        /**
         * 计算旗子使用区域
         */
        int flagLeft = mScreenWidth * 384 / 720;
        int flagTop = mScreenHeight * 690 / 1334;
        int flagRight = flagLeft + (mScreenWidth * 34) / 720;
        int flagBottom = flagTop + (mScreenHeight * 24) / 1334;

        mFlagRect = new Rect(flagLeft, flagTop, flagRight, flagBottom);

    }

    private void postIntoThread(Runnable runnable) {
        postIntoThread(runnable, 0);
    }

    private void postIntoThread(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(runnable, delay);
        }
    }

    private void removeFromThread(Runnable runnable) {
        if (mHandler != null) {
            mHandler.removeCallbacks(runnable);
        }
    }

    public void switchBackGround() {
        postIntoThread(new Runnable() {
            @Override
            public void run() {
                switchFlag = !switchFlag;
                if (switchFlag) {
                    turnIntoSunSet();
                } else {
                    turnIntoSunUp();
                }
            }
        });
    }

    private void pauseBitmap() {
        /**
         * 为了节省CPU，当SurfaceDestroy的时候需要暂停动画
         */
        clearLight();
        clearFire();
        clearBird();
        clearFlag();
    }

    private void resumeBitmap() {
        /**
         * 恢复Bitmap
         */
        postIntoThread(new Runnable() {
            @Override
            public void run() {
                if (mIsSunSet) {
                    /**
                     * 恢复天黑
                     */
                    resumeSunSet();
                } else {
                    /**
                     * 恢复天亮
                     */
                    resumeSunUp();
                }
            }
        });
    }

    private void resumeSunSet() {
        clearFire();
        clearLight();
        mBackGroundIndex = 0;
        mNeedRefreshBg = true;
        refreshDraw();
        mNeedRefreshBg = false;
        turnIntoSunSetAlphaChange(0, 255);
        mIsSunSet = true;
    }

    private void resumeSunUp() {
        clearFlag();
        clearBird();
        mBackGroundIndex = mBackGroundFrames.length - 1;
        mNeedRefreshBg = true;
        refreshDraw();
        mNeedRefreshBg = false;
        turnIntoSunUpAlphaChange(0, 255);
        mIsSunSet = false;
    }

    public void turnIntoSunUp() {
        postIntoThread(new Runnable() {
            @Override
            public void run() {
                if (!mIsSunSet) {
                    return;
                }
                turnIntoSunSetAlphaChange(255, 0);
                int N = mBackGroundFrames.length;
                for (int i = 0; i < N; i++) {
                    mNeedRefreshBg = true;
                    updateBackGroundIndex(i);
                    refreshDraw();
                    mNeedRefreshBg = false;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                turnIntoSunUpAlphaChange(0, 255);
                mIsSunSet = false;
            }
        });
    }

    public void turnIntoSunSet() {
        postIntoThread(new Runnable() {
            @Override
            public void run() {
                if (mIsSunSet) {
                    return;
                }
                turnIntoSunUpAlphaChange(255, 0);
                int N = mBackGroundFrames.length;
                for (int i = N - 1; i >= 0; i--) {
                    mNeedRefreshBg = true;
                    updateBackGroundIndex(i);
                    refreshDraw();
                    mNeedRefreshBg = false;
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                turnIntoSunSetAlphaChange(0, 255);
                mIsSunSet = true;
            }
        });
    }

    private void turnIntoSunUpAlphaChange(int fromAlpha, int toAlpha) {
        if (fromAlpha == toAlpha) return;
        /**
         * 计算过渡区
         */
        int i = 4; //过渡帧为4帧

        int delta = Math.abs((toAlpha - fromAlpha) / 4);

        if (fromAlpha > toAlpha) {
            delta = -delta;
        }
        int tempFromAlpha = fromAlpha;
        for (int j = 0; j < i; j++) {
            mPaint.setAlpha(tempFromAlpha);
            tempFromAlpha = tempFromAlpha + delta;
            drawBird(false);
            drawFlag(false);
            try {
                Thread.sleep(mAlphaChangeFrame);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        mPaint.setAlpha(toAlpha);
        if (fromAlpha > toAlpha) {
            /**
             * 此处为渐变消失
             */
            clearBird();
            clearFlag();
        } else {
            /**
             * 此处为渐变出现
             */
            startBird();
            startFlag();
        }
    }

    private void turnIntoSunSetAlphaChange(int fromAlpha, int toAlpha) {

        if (fromAlpha == toAlpha) return;
        /**
         * 计算过渡区
         */
        int i = 4; //过渡帧为4帧

        int delta = Math.abs((toAlpha - fromAlpha) / 4);

        if (fromAlpha > toAlpha) {
            delta = -delta;
        }
        int tempFromAlpha = fromAlpha;
        for (int j = 0; j < i; j++) {
            mPaint.setAlpha(tempFromAlpha);
            tempFromAlpha = tempFromAlpha + delta;
            drawFire(false);
            try {
                Thread.sleep(mAlphaChangeFrame);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        mPaint.setAlpha(toAlpha);
        if (fromAlpha > toAlpha) {
            /**
             * 此处为渐变消失
             */
            clearFire();
            clearLight();
        } else {
            /**
             * 此处为渐变出现
             */
            startFire();
            startLight();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "surfaceCreated");
        resumeBitmap();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(TAG, "surfaceChanged width=" + width + ",height= " + height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(TAG, "surfaceDestroyed");
        pauseBitmap();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.i(TAG, "onDetachedFromWindow");
        destroy();
    }

    private void destroy() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mHandlerThread != null) {
            mHandlerThread.quit();
            mHandlerThread = null;
        }
        mFireBitmap = null;
        mBackGroundBitmap = null;
        mBitmapSparseArray.clear();
    }

    private void startFire() {
        postIntoThread(mFireDrawRunnable, mFireFrameTime);
    }

    private void drawFire(boolean needRepeat) {
        mFireIndex++;
        mFireIndex = mFireIndex % mFiresFrames.length;
        mNeedRefreshFire = true;
        refreshDraw();
        mNeedRefreshFire = false;
        if (needRepeat) {
            startFire();
        }
    }

    private void clearFire() {
        removeFromThread(mFireDrawRunnable);
        mFireIndex = -1;
    }

    private void startLight() {
        postIntoThread(mLightDrawRunnable, mLightFrameTime);
    }

    private void drawLight(boolean needRepeat) {
        mLightIndex++;
        mLightIndex = mLightIndex % mLightFrames.length;
        mNeedRefreshLight = true;
        refreshDraw();
        mNeedRefreshLight = false;
        if (needRepeat) {
            startLight();
        }
    }

    private void clearLight() {
        removeFromThread(mLightDrawRunnable);
        mLightIndex = -1;
    }

    private void startBird() {
        postIntoThread(mBirdDrawRunnable, mBirdFrameTime);
    }

    private void drawBird(boolean needRepeat) {
        mBirdIndex++;
        mBirdIndex = mBirdIndex % mBirdFrames.length;
        mNeedRefreshBird = true;
        refreshDraw();
        mNeedRefreshBird = false;
        if (needRepeat) {
            startBird();
        }
    }

    private void clearBird() {
        removeFromThread(mBirdDrawRunnable);
        mBirdIndex = -1;
    }

    private void startFlag() {
        postIntoThread(mFlagDrawRunnable, mFlagFrameTime);
    }

    private void drawFlag(boolean needRepeat) {
        mFlagIndex++;
        mFlagIndex = mFlagIndex % mFlagFrames.length;
        mNeedRefreshFlag = true;
        refreshDraw();
        mNeedRefreshFlag = false;
        if (needRepeat) {
            startFlag();
        }
    }

    private void clearFlag() {
        removeFromThread(mFlagDrawRunnable);
        mFlagIndex = -1;
    }

    private void refreshDraw() {
        mainDraw();
    }

    private void updateBackGroundIndex(int index) {
        mBackGroundIndex = index;
    }

    private Bitmap decodeBitmap(int resource, boolean needCache) {
        Bitmap bitmap = null;
        /**
         * 先去缓存中找
         */
        if (needCache) {
            bitmap = mBitmapSparseArray.get(resource);
        }
        if (bitmap != null) {
            return bitmap;
        }

        /**
         * 缓存中没有使用Decode
         */
        bitmap = BitmapDecodeUtils.decodeBitmap(getContext(), resource);
        if (bitmap != null) {
            if (needCache) {
                mBitmapSparseArray.put(resource, bitmap);
                totalMemory += bitmap.getByteCount();
                Log.i(TAG, "decodeBitmap: totalMemory = " + totalMemory);
            }
            Log.i(TAG, "decodeBitmap: " + bitmap.getByteCount());
        }
        return bitmap;
    }

    private void mainBgDraw(Canvas canvas) {
        /**
         * 绘制背景
         */
        if (mBackGroundIndex != -1) {
            mBackGroundBitmap = decodeBitmap(mBackGroundFrames[mBackGroundIndex], false);
            canvas.drawBitmap(mBackGroundBitmap, null, mBackGroundRect, null);
        }
    }

    private void mainFireDraw(Canvas canvas) {
        /**
         * 根据需要绘制火焰
         */
        canvas.save();
        if (mFireIndex != -1) {
            mFireBitmap = decodeBitmap(mFiresFrames[mFireIndex], true);
            canvas.drawBitmap(mBackGroundBitmap, null, mBackGroundRect, null);
            canvas.drawBitmap(mFireBitmap, null, mFireRect, mPaint);
        }
        canvas.restore();
    }

    private void mainLightDraw(Canvas canvas) {
        /**
         * 根据需要绘制灯光
         */
        canvas.save();
        if (mLightIndex != -1) {
            mLightBitmap = decodeBitmap(mLightFrames[mLightIndex], true);
            canvas.drawBitmap(mBackGroundBitmap, null, mBackGroundRect, null);
            canvas.drawBitmap(mLightBitmap, null, mLightRect, mPaint);
        }
        canvas.restore();
    }

    private void mainBirdDraw(Canvas canvas) {
        /**
         * 根据需要绘制鸟
         */
        canvas.save();
        if (mBirdIndex != -1) {
            mBirdBitmap = decodeBitmap(mBirdFrames[mBirdIndex], true);
            canvas.drawBitmap(mBackGroundBitmap, null, mBackGroundRect, null);
            canvas.drawBitmap(mBirdBitmap, null, mBirdRect, mPaint);
        }
        canvas.restore();
    }

    private void mainFlagDraw(Canvas canvas) {
        /**
         * 根据需要绘制旗子
         */
        canvas.save();
        if (mFlagIndex != -1) {
            mFlagBitmap = decodeBitmap(mFlagFrames[mFlagIndex], true);
            canvas.drawBitmap(mBackGroundBitmap, null, mBackGroundRect, null);
            canvas.drawBitmap(mFlagBitmap, null, mFlagRect, mPaint);
        }
        canvas.restore();
    }

    /**
     * 主要的绘制算法
     */
    private void mainDraw() {
        Canvas canvas = null;

        long start = System.currentTimeMillis();

        if (mNeedRefreshBg) {
            canvas = mHolder.lockCanvas();
            if (canvas != null) {
                try {
                    mainBgDraw(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHolder.unlockCanvasAndPost(canvas);
            }
        }

        if (mNeedRefreshFire) {
            Rect rect = new Rect(mFireRect);
            canvas = mHolder.lockCanvas(rect);
            if (canvas != null) {
                try {
                    mainFireDraw(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHolder.unlockCanvasAndPost(canvas);
            }
        }

        if (mNeedRefreshLight) {
            Rect rect = new Rect(mLightRect);
            canvas = mHolder.lockCanvas(rect);
            if (canvas != null) {
                try {
                    mainLightDraw(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHolder.unlockCanvasAndPost(canvas);
            }
        }

        if (mNeedRefreshFlag) {
            Rect rect = new Rect(mFlagRect);
            canvas = mHolder.lockCanvas(rect);
            if (canvas != null) {
                try {
                    mainFlagDraw(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHolder.unlockCanvasAndPost(canvas);
            }
        }

        if (mNeedRefreshBird) {
            Rect rect = new Rect(mBirdRect);
            canvas = mHolder.lockCanvas(rect);
            if (canvas != null) {
                try {
                    mainBirdDraw(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mHolder.unlockCanvasAndPost(canvas);
            }
        }

        long end = System.currentTimeMillis();
//        Log.i(TAG, "mainDraw: all cost time " + (end - start));
    }
}
