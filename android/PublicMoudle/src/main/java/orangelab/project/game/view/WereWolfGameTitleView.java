package orangelab.project.game.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.common.model.action.ServerActionFight;
import orangelab.project.common.model.action.ServerActionFightState;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.dialog.WereWolfCallDialog;
import orangelab.project.game.dialog.WereWolfRoomSettingDialog;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.game.utils.WereWolfToolKit;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/11/27 下午5:35
 * Mystery
 */

public class WereWolfGameTitleView extends WereWolfGameProcess implements Destroyable, View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private WereWolfGameContext mWereWolfGameContext = null;

    private TextView mRoomId;
    private TextView mRoomType;
    private View mRoomLocked;
    private View mRoomBack;

    private ImageView mWifi;
    private ImageView mCall;
    private View mSetting;
    private TextView mLevelLimit;
    private Button mChangeRoomOwner;
    private View mWolfBoom;
    private View mKnightFight;

    private WereWolfObserverView observerView;//顶部观战人数的view

    public WereWolfGameTitleView(WereWolfGameContext context, View rootView) {

        this.mContext = context.getAndroidContext();
        mWereWolfGameContext = context;
        mWereWolfGameContext.registerProcessListener(this);
        mRootView = rootView;
        mRoomId = (TextView) mRootView.findViewById(R.id.id_werewolf_game_roomid);
        mRoomType = (TextView) mRootView.findViewById(R.id.id_werewolf_game_type);
        mRoomLocked = mRootView.findViewById(R.id.id_werewolf_game_lock);
        mRoomBack = mRootView.findViewById(R.id.id_werewolf_exit);
        mWifi = (ImageView) mRootView.findViewById(R.id.id_werewolf_game_wifi);
        mCall = (ImageView) mRootView.findViewById(R.id.id_werewolf_game_call);
        mSetting = mRootView.findViewById(R.id.id_werewolf_game_setting);
        mLevelLimit = (TextView) mRootView.findViewById(R.id.id_werewolf_level_limit);
        mChangeRoomOwner = (Button) mRootView.findViewById(R.id.id_werewolf_changeowner);
        mChangeRoomOwner.setOnClickListener(this);
        mWolfBoom = mRootView.findViewById(R.id.id_werewolf_boom);
        mKnightFight = mRootView.findViewById(R.id.id_werewolf_fight);
        mWolfBoom.setOnClickListener(this);
        mKnightFight.setOnClickListener(this);
        observerView = new WereWolfObserverView(mContext, mRootView.findViewById(R.id.observe_view));

        initBaseData();

        updateNetWork();

        initListener();
    }

    private void initBaseData() {
        try {
            mRoomId.setText(WereWolfString.getString(R.string.werewolf_room_id, mWereWolfGameContext.getRoomId()));
            mRoomType.setText(WereWolfToolKit.GetWereWolfGameTypeString(mWereWolfGameContext.getGameType()));

            updatePasswordLock();

            updateRoomCall();

            updateLevelLimit();

            updateChangeRoomOwner();

            updateSetting();

            updateOnLineMember();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //wwq 2018-02-02 更新在线人数 start//
    private void updateOnLineMember() {
        try {
            int count = mWereWolfGameContext.getObserverMembersCount() + mWereWolfGameContext.getSitedPositionsSize();
            observerView.setOnLineNumber(count);
            observerView.setObserveNumber(mWereWolfGameContext.getObserverMembersCount());//wwq 2018-02-02 更新观战人数
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //wwq 2018-02-02 更新在线人数 end//


    private void updateChangeRoomOwner() {
        if (mWereWolfGameContext.isGaming()) {
            mChangeRoomOwner.setVisibility(View.GONE);
        } else {
            if (mWereWolfGameContext.isObserver()) {
                mChangeRoomOwner.setVisibility(View.GONE);
            } else {
                if (mWereWolfGameContext.isRoomOwner()) {
                    mChangeRoomOwner.setVisibility(View.GONE);
                } else {
                    mChangeRoomOwner.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void updatePasswordLock() {
        setLock(mWereWolfGameContext.getPassword());
    }

    private void updateRoomCall() {
        if (mWereWolfGameContext.isObserver()) {
            mCall.setVisibility(View.GONE);
        } else {
            /**
             * 判断，新手6人模式以及新手6-10人模式不要召集令
             */
            if (TextUtils.equals(mWereWolfGameContext.getGameType(), Constant.GAME_TYPE_PRE_SIMPLE) ||
                    TextUtils.equals(mWereWolfGameContext.getGameType(), Constant.GAME_TYPE_PRE_SIMPLE_NEW)) {
                mCall.setVisibility(View.GONE);
            } else {
                if (mWereWolfGameContext.isRoomOwner()) {
                    mCall.setVisibility(View.VISIBLE);
                } else {
                    mCall.setVisibility(View.GONE);
                }
            }
        }
    }

    private void updateLevelLimit() {
        EnterRoomResult.RoomConfig roomConfig = mWereWolfGameContext.getRoomConfig();
        if (roomConfig.level == roomConfig.min_level) {
            /**
             * no level limit
             */
            mLevelLimit.setVisibility(View.INVISIBLE);
        } else {
            mLevelLimit.setVisibility(View.VISIBLE);
            mLevelLimit.setText(WereWolfString.getString(R.string.werewolf_game_level_limit, Integer.toString(roomConfig.level)));
        }
    }

    private void initListener() {
        mCall.setOnClickListener(this);
        mSetting.setOnClickListener(this);
        initToOnLineMemberListListener();
    }

    //wwq  2018-02-02 添加跳转到在线列表的点击事件 start//
    private void initToOnLineMemberListListener() {
        observerView.setOnClickListener(v -> {
            //直接强转
            android.support.v4.app.FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            WereWolfOnlineMembersFragment fragment = new WereWolfOnlineMembersFragment();
            fragment.setWereWolfGameContext(mWereWolfGameContext);
            fragmentManager.beginTransaction().replace(R.id.rl_fragment_container, fragment, fragment.getTAG()).addToBackStack(fragment.getTAG()).commit();

        });
    }
    //wwq  2018-02-02 添加跳转到在线列表的点击事件 end//

    private void updateNetWork() {
        if (mWereWolfGameContext.isAudioDisconnect()) {
            mWifi.setImageResource(R.mipmap.ico_werewolf_wifi_disconnect);
        } else {
            mWifi.setImageResource(R.mipmap.ico_werewolf_wifi_connect);
        }
    }

    public void setLock(String password) {
        if (TextUtils.isEmpty(password)) {
            mRoomLocked.setVisibility(View.GONE);
        } else {
            mRoomLocked.setVisibility(View.VISIBLE);
        }
    }

    public void setCloseAction(View.OnClickListener onClickListener) {
        mRoomBack.setOnClickListener(onClickListener);
    }


    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        mWereWolfGameContext.unRegisterProcessListener(this);
        mWereWolfGameContext = null;
        mRoomId = null;
        mRootView = null;
        mRoomBack = null;
        mRoomType = null;
        mRoomLocked = null;
    }

    @Override
    public boolean handleServerActionFightState(ServerActionFightState fightState) {
        if (fightState.can_fight) {
            mKnightFight.setVisibility(View.VISIBLE);
        } else {
            mKnightFight.setVisibility(View.GONE);
        }
        return false;
    }

    @Override
    public boolean handleObserverJoin(WereWolfObserverMember member, JSONObject jsonObject) {
        //updateObserverMember();
        updateOnLineMember();
        return false;
    }

    @Override
    public boolean handleObserverLeave(WereWolfObserverMember member, JSONObject jsonObject) {
        //updateObserverMember();
        updateOnLineMember();
        return false;
    }

    @Override
    public boolean handleAudioSocketConnect() {
        updateNetWork();
        return false;
    }

    @Override
    public boolean handleAudioSocketDisConnect() {
        updateNetWork();
        return false;
    }

    @Override
    public boolean handleUpdateConfig(EnterRoomResult.RoomConfig config) {
        requestUpdateTitleView();
        return false;
    }

    @Override
    public boolean handleUpdatePassword(String password, JSONObject jsonObject) {
        requestUpdateTitleView();
        return false;
    }

    @Override
    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        requestUpdateTitleView();
        setAvailableChangeRoomOwner(true);
        return false;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        requestUpdateTitleView();
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        requestUpdateTitleView();
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        requestUpdateTitleView();
        return false;
    }

    @Override
    public boolean handleBoomState(boolean flag, JSONObject jsonObject) {
        if (flag) {
            mWolfBoom.setVisibility(View.VISIBLE);
        } else {
            mWolfBoom.setVisibility(View.GONE);
        }
        return false;
    }

    @Override
    public boolean handleRejectResetMaster(int dur, JSONObject jsonObject) {
        mWereWolfGameContext.check().post(() -> {
            setAvailableChangeRoomOwner(true);
        }, dur * 1000);
        return false;
    }

    //wwq 2018-02-02 更新在线人数 start//

    @Override
    public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
        updateOnLineMember();
        return false;
    }

    @Override
    public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
        updateOnLineMember();
        return false;
    }

    @Override
    public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
        updateOnLineMember();
        return false;
    }

    @Override
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        updateOnLineMember();
        updateChangeRoomOwner();
        //updateObserverMember();
        return false;
    }

    @Override
    public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
        updateOnLineMember();
        updateChangeRoomOwner();
        //updateObserverMember();
        return false;
    }

    //wwq 2018-02-02 更新在线人数 end//

    private void requestUpdateTitleView() {
        if (mWereWolfGameContext.isGaming()) {
            mCall.setVisibility(View.GONE);
            mChangeRoomOwner.setVisibility(View.GONE);
            mSetting.setVisibility(View.GONE);
        } else {
            updatePasswordLock();
            updateRoomCall();
            updateLevelLimit();
            updateChangeRoomOwner();
            updateSetting();
            updateOnLineMember();
            mWolfBoom.setVisibility(View.GONE);
            mKnightFight.setVisibility(View.GONE);
        }
    }

    private void updateSetting() {
        if (mWereWolfGameContext.isObserver()) {
            mSetting.setVisibility(View.GONE);
        } else {
            if (TextUtils.equals(mWereWolfGameContext.getGameType(), Constant.GAME_TYPE_PRE_SIMPLE) ||
                    TextUtils.equals(mWereWolfGameContext.getGameType(), Constant.GAME_TYPE_PRE_SIMPLE_NEW)) {
                mSetting.setVisibility(View.GONE);
            } else {
                if (mWereWolfGameContext.isRoomOwner()) {
                    mSetting.setVisibility(View.VISIBLE);
                } else {
                    mSetting.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setAvailableChangeRoomOwner(boolean flag) {
        if (flag) {
            mChangeRoomOwner.setEnabled(true);
            mChangeRoomOwner.setBackgroundResource(R.drawable.drawable_werewolf_change_roomowner);
        } else {
            mChangeRoomOwner.setEnabled(false);
            mChangeRoomOwner.setBackgroundResource(R.drawable.drawable_werewolf_change_roomowner_press);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_werewolf_game_call) {
            if (!TextUtils.isEmpty(mWereWolfGameContext.getPassword())) {
                ToastToolKit.showShort(WereWolfString.getString(R.string.tips_call));
                return;
            }
            if (GlobalUserState.getGlobalState().isTourist()) {
                ToastToolKit.showShort(WereWolfString.getString(R.string.werewolf_tourist_call_error));
                return;
            }
            WereWolfCallDialog dialog = new WereWolfCallDialog(mContext, mWereWolfGameContext.getCurCallTitle(), mWereWolfGameContext.getCurCallType(), mWereWolfGameContext.getRoomCallConfig());
            dialog.showDialog(0);
        } else if (id == R.id.id_werewolf_game_setting) {
            WereWolfRoomSettingDialog dialog = new WereWolfRoomSettingDialog(mWereWolfGameContext, false);
            dialog.showDialog(0);
        } else if (id == R.id.id_werewolf_boom) {
            RoomSocketEngineHelper.sendEmptyType(Constant.BOOM);
        } else if (id == R.id.id_werewolf_changeowner) {
            if (mWereWolfGameContext.isObserver()) {
                ToastToolKit.showShort(WereWolfString.getString(R.string.game_str_observer_cannot_change_master));
                return;
            }
            RoomSocketEngineHelper.resetMaster();
            setAvailableChangeRoomOwner(false);
        } else if (id == R.id.id_werewolf_fight) {
            if (mWereWolfGameContext.isObserver()) {
                ToastToolKit.showShort(WereWolfString.getString(R.string.game_str_observer_cannot_change_master));
                return;
            }
            RoomSocketEngineHelper.sendEmptyType(WereWolfConstant.FIGHT);
        }
    }

}
