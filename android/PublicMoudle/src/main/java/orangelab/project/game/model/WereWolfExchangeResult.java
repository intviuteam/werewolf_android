package orangelab.project.game.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/14 下午1:35
 * Mystery
 */

public class WereWolfExchangeResult implements Keepable {
    public List<Integer> positions;
}
