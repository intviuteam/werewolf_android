package orangelab.project.game.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.game.dialog.IGameDialog;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.model.WereWolfKillMember;
import orangelab.project.game.model.WereWolfKillMemberHolder;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.KillInfoResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.utils.CountDownView;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/11/30 下午3:09
 * Mystery
 */

public class WereWolfKillView implements WereWolfDialog, CountDownView.CountDownFinish, IGameDialog {

    private static final String TAG = "WereWolfKillView";
    private ViewGroup mRootView;
    private View mChildView;
    private CountDownView mCountDownView;
    private SafeHandler mSafeHandler = new SafeHandler();

    private TextView mTitle;
    private TextView mTitleCountDown;

    private GridView mGridView;
    private WereWolfKillAdapter mWereWolfKillAdapter;

    private boolean isShowing = false;
    private final int dialogMaxWidth = ScreenUtils.dip2px(355);

    public WereWolfKillView(ViewGroup rootView, List<WereWolfKillMember> members) {
        mRootView = rootView;

        mChildView = View.inflate(rootView.getContext(), R.layout.layout_werewolf_dialog_wolfkill, null);
        View dialogRootView = mChildView.findViewById(R.id.id_werewolf_base_dialog_root);
        mTitle = (TextView) mChildView.findViewById(R.id.id_werewolf_base_dialog_title);
        mTitleCountDown = (TextView) mChildView.findViewById(R.id.id_werewolf_base_dialog_countdown);
        mTitle.setText(WereWolfString.getString(R.string.werewolf_game_wolf_kill_dialog));
        mGridView = (GridView) mChildView.findViewById(R.id.id_werewolf_base_dialog_gridview);
        mWereWolfKillAdapter = new WereWolfKillAdapter(rootView.getContext(), members);
        mGridView.setAdapter(mWereWolfKillAdapter);
        mGridView.setOnItemClickListener((parent, view, position, id) -> {
            mWereWolfKillAdapter.clearSelect();
            mWereWolfKillAdapter.setSelected(position);
            RoomSocketEngineHelper.sendKill(mWereWolfKillAdapter.getItem(position).memberPosition);
        });

        initListener();

        try {
            WereWolfDialogDecorate.DecorateLayoutParams(dialogRootView.getLayoutParams());
            dialogRootView.requestLayout();
        } catch (Exception e) {
            /**
             * just in case
             */
            e.printStackTrace();
        }
    }

    private void initListener() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(value -> {
            if (!value.isWereWolf()) {
                return;
            }
            if (value.type.equals(Constant.KILL)) {
                KillInfoResult result = GsonHelper.getGson().fromJson(value.payload.toString(), KillInfoResult.class);
                if (result.kill_info != null) {
                    mWereWolfKillAdapter.updateAllKillInfo(result.kill_info);
                } else {
                    mWereWolfKillAdapter.updateAllKillInfo(Collections.emptyMap());
                }
                if (result.finished) {
                    /**
                     * 准备结束Dialog
                     */
                    mSafeHandler.postDelaySafely(() -> {
                        dismiss();
                    }, 1500);
                }
            }
        }).register();
    }


    @Override
    public void showDialog(int seconds) {
        if (seconds >= 0) {
            if (!isShowing) {
                show();
            }
            if (seconds != 0) {
                mTitleCountDown.setVisibility(View.VISIBLE);
                mCountDownView = new CountDownView(mTitleCountDown, seconds, this);
                mCountDownView.setCountDownRule(R.string.werewolf_game_countdown);
                mCountDownView.start();
            } else {
                mTitleCountDown.setVisibility(View.GONE);
            }
        } else {
        }
    }

    private void show() {
        if (mRootView != null && mChildView != null && !isShowing) {
            if (mRootView.getVisibility() != View.VISIBLE) {
                mRootView.setVisibility(View.VISIBLE);
            }
            mRootView.addView(mChildView);
            isShowing = true;
        }
    }

    @Override
    public void dismiss() {
        RxToolKit.UnRegister(this);
        if (mCountDownView != null) {
            mCountDownView.cancel();
            mCountDownView = null;
        }
        if (mSafeHandler != null) {
            mSafeHandler.release();
        }
        if (mRootView != null && mChildView != null) {
            mRootView.removeView(mChildView);
            mRootView.setVisibility(View.GONE);
        }
        isShowing = false;
    }

    @Override
    public void onFinish() {
        dismiss();
    }

    public static class WereWolfKillAdapter extends BaseAdapter implements Destroyable {

        private Context mContext;
        private List<WereWolfKillMember> mData;

        public WereWolfKillAdapter(Context context, List<WereWolfKillMember> data) {
            mData = data;
            mContext = context;
        }

        public void clearSelect() {
            for (WereWolfKillMember member : mData) {
                member.memberSelect = false;
            }
            notifyDataSetChanged();
        }

        public List<WereWolfKillMember> getData() {
            return mData;
        }

        public void setSelected(int position) {
            getItem(position).memberSelect = true;
            notifyDataSetChanged();
        }

        public void updateAllKillInfo(Map<Integer, Integer> map) {
            for (WereWolfKillMember temp : mData) {
                temp.mKillMap = map;
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public WereWolfKillMember getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            WereWolfKillMemberHolder holder = null;
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.layout_werewolf_dialog_member_wolfkill_view, null);
                holder = new WereWolfKillMemberHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (WereWolfKillMemberHolder) convertView.getTag();
            }

            WereWolfKillMember member = getItem(position);

            holder.setName(member.memberName);
            holder.setHead(member.memberImage);
            holder.setSelect(member.memberSelect);
            holder.setPosition(member.memberPosition);
            holder.updateKillPositions(member.mKillMap);
            holder.setRole(member.memberRole);
            holder.setBadge(member.isSheriff, member.isLover);

            return convertView;
        }

        @Override
        public void destroy() {

        }
    }
}
