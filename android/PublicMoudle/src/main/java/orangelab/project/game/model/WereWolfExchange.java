package orangelab.project.game.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/12/14 下午1:34
 * Mystery
 */

public class WereWolfExchange implements Keepable {

    public List<Integer> alives;
    public List<Integer> last_exchanged = new ArrayList<>(0);
    public int magician_position;
    public int duration;
}
