package orangelab.project.game.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidtoolkit.view.ScreenUtils;
import com.R;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.common.utils.Utils;
import com.toolkit.action.Destroyable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * game_werewolf
 * 2017/12/3 下午3:30
 * Mystery
 */

public class WereWolfGameMessageVoteAdapter extends BaseAdapter implements Destroyable {

    private Map<Integer, List<Integer>> vote_info = new HashMap<>(0);
    private WereWolfGameContext mWolfGameContext;

    public WereWolfGameMessageVoteAdapter(Map<Integer, List<Integer>> vote_info, WereWolfGameContext wolfGameContext) {
        this.vote_info = vote_info;
        mWolfGameContext = wolfGameContext;
    }

    @Override
    public int getCount() {
        return vote_info.size();
    }

    @Override
    public Map.Entry<Integer, List<Integer>> getItem(int position) {
        Iterator<Map.Entry<Integer, List<Integer>>> iterator = vote_info.entrySet().iterator();
        int count = 0;
        while (iterator.hasNext()) {
            if (count == position) {
                return iterator.next();
            } else {
                count++;
                continue;
            }
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mWolfGameContext.getAndroidContext(), R.layout.layout_werewolf_msg_list_item_vote_item, null);
            holder = new ViewHolder();
            holder.targetPosition = (TextView) convertView.findViewById(R.id.id_werewolf_msg_list_item_vote_item_position);
            holder.fromPositions = (GridView) convertView.findViewById(R.id.id_werewolf_msg_list_item_vote_item_gridview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Map.Entry<Integer, List<Integer>> entry = getItem(position);
        int keyPosition = entry.getKey();
        List<Integer> valueList = entry.getValue();
        if (keyPosition != -1) {
            keyPosition = keyPosition + 1;
            holder.targetPosition.setText(Integer.toString(keyPosition));
        } else {
            /**
             * quit
             */
            holder.targetPosition.setBackgroundResource(R.mipmap.ico_werewolf_game_vote_quit);
        }

        GridView gridView = holder.fromPositions;
        WereWolfGameMessageVoteGridViewAdapter adapter = (WereWolfGameMessageVoteGridViewAdapter) gridView.getAdapter();
        if (adapter != null) {
            /**
             * 1. destroy this
             */
            adapter.destroy();
        }
        adapter = new WereWolfGameMessageVoteGridViewAdapter(valueList, mWolfGameContext);
        gridView.setAdapter(adapter);
        Utils.customGridViewAndChildHeightAndWidth(gridView, 5, ScreenUtils.dip2px(20), ScreenUtils.dip2px(15));
        return convertView;
    }

    @Override
    public void destroy() {
        vote_info.clear();
        mWolfGameContext = null;
    }

    public static class ViewHolder {
        public TextView targetPosition;
        public GridView fromPositions;
    }
}
