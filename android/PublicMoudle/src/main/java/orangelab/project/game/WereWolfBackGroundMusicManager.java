package orangelab.project.game;

import android.content.Context;
import android.text.TextUtils;

import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;

import java.util.Map;

/**
 * game_werewolf
 * 2018/1/22 上午10:47
 * Mystery
 */

public class WereWolfBackGroundMusicManager implements Destroyable {

    private Context mContext;
    private WereWolfGameContext mWolfGameContext;

    private String SUNSET_MEDIA_BG = "";

    public WereWolfBackGroundMusicManager(WereWolfGameContext context) {
        mWolfGameContext = context;
        mContext = mWolfGameContext.getAndroidContext();
    }


    private String getSunSetMedia(Map<String, Integer> roleMap) {
        String result = Constant.SUNSET_NORMAL_PEOPLE;
        try {
            if (roleMap.containsKey(Constant.ROLE_SEER) && roleMap.containsKey(Constant.ROLE_WOLF) && roleMap.containsKey(Constant.ROLE_GUARD) && roleMap.containsKey(Constant.ROLE_WITCH)) {
                result = Constant.MEDIA_SEER_GUARD_WITCH_WOLF;
            } else if (roleMap.containsKey(Constant.ROLE_SEER) && roleMap.containsKey(Constant.ROLE_WOLF) && roleMap.containsKey(Constant.ROLE_WITCH)) {
                result = Constant.MEDIA_SEER_WITCH_WOLF;
            } else if (roleMap.containsKey(Constant.ROLE_SEER) && roleMap.containsKey(Constant.ROLE_WOLF) && roleMap.containsKey(Constant.ROLE_GUARD)) {
                result = Constant.MEDIA_SEER_GUARD_WOLF;
            } else if (roleMap.containsKey(Constant.ROLE_SEER) && roleMap.containsKey(Constant.ROLE_WOLF)) {
                result = Constant.MEDIA_SEER_WOLF;
            } else {
                result = Constant.SUNSET_NORMAL_PEOPLE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = Constant.SUNSET_NORMAL_PEOPLE;
        }
        return result;
    }

    private String getSunSetMedia(StartResult startResult) {
        String result = Constant.SUNSET_NORMAL_PEOPLE;
        try {
            return getSunSetMedia(startResult.role_map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void handleStart(StartResult startResult) {
        try {
            SUNSET_MEDIA_BG = getSunSetMedia(startResult);
            RoomSocketEngineHelper.playMedia(mContext, Constant.START, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleAssignedRole() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.ASSIGNED_ROLE, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleSunSet() {
        try {
            if (!mWolfGameContext.isObserver()) {
                if (mWolfGameContext.getRoomConfig().night_speak) {
                    /**
                     * 夜间说话不播放音乐
                     */
                    if (TextUtils.equals(mWolfGameContext.getSelfRole(), Constant.ROLE_WEREWOLF_KING)
                            || TextUtils.equals(mWolfGameContext.getSelfRole(), Constant.ROLE_WOLF)
                            || TextUtils.equals(mWolfGameContext.getSelfRole(), WereWolfConstant.ROLE_DEMON)
                            || TextUtils.equals(mWolfGameContext.getSelfRole(), WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
                        /**
                         * 狼人与恶魔不播放音乐
                         */
                        return;
                    }
                }
            }
            if (!TextUtils.isEmpty(SUNSET_MEDIA_BG)) {
                RoomSocketEngineHelper.playMedia(mContext, SUNSET_MEDIA_BG, 0);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void handleSunUp() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.SUNUP, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleBoom() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.BOOM, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleLink() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.LINK, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleVote() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.VOTE, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleFightAwayResult() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, WereWolfConstant.FIGHT_AWAY_RESULT, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleRestoreRoom(RestoreResult restoreResult) {
        try {
            SUNSET_MEDIA_BG = getSunSetMedia(restoreResult.room_info.config.role_map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleGameOver(GameOverResult gameOverResult) {
        try {
            SUNSET_MEDIA_BG = "";
            if (gameOverResult.win_type.equals(Constant.WIN_PEOPELE)) {
                RoomSocketEngineHelper.playMedia(mContext, Constant.GAMEOVER_PEOPLE_WIN, 0);
            } else if (gameOverResult.win_type.equals(Constant.WIN_Wolf)) {
                RoomSocketEngineHelper.playMedia(mContext, Constant.GAMEOVER_WOLF_WIN, 0);
            } else if (gameOverResult.win_type.equals(Constant.WIN_THIRD)) {
                RoomSocketEngineHelper.playMedia(mContext, Constant.GAMEOVER_CUPID_WIN, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void destroy() {
        try {
            RoomSocketEngineHelper.playMedia(mContext, Constant.LEAVE, 0);
            if (mWolfGameContext != null) {
                mWolfGameContext = null;
            }
            if (mContext != null) {
                mContext = null;
            }
            SUNSET_MEDIA_BG = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
