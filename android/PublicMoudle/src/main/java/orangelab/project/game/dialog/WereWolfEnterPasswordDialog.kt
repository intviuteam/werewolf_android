package orangelab.project.game.dialog

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.Selection
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.view.ScreenUtils
import kotlinx.android.synthetic.main.layout_werewolf_dialog_enter_password.*
import orangelab.project.common.dialog.SafeDialog
import orangelab.project.common.utils.MessageUtils

class WereWolfEnterPasswordDialog(context: Context) : SafeDialog(context) {

    companion object {
        private const val TAG = "WereWolfEnterPasswordDialog"

        interface OnPasswordEnterResult {
            fun onPasswordEnterResult(right: Boolean)
        }
    }

    private lateinit var mRoomId: String
    private lateinit var mPassword: String
    private lateinit var callback: OnPasswordEnterResult

    constructor(context: Context, roomId: String, password: String, callback: OnPasswordEnterResult) : this(context) {
        this.mRoomId = roomId
        this.mPassword = password
        this.callback = callback

        init()
    }

    private fun init() {
        initView()
        initWindow()
        initListener()
    }

    @SuppressLint("InflateParams")
    private fun initView() {
        val contentView: View = LayoutInflater.from(context).inflate(R.layout.layout_werewolf_dialog_enter_password, null)
        contentView.isFocusable = true
        contentView.isFocusableInTouchMode = true
        contentView.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss()
                return@setOnKeyListener true
            }
            false
        }
        setContentView(contentView)

        tv_title.text = MessageUtils.getString(R.string.werewolf_enter_password, mRoomId)
        initEditText(editText_psw)
    }

    private fun initWindow() {
        val dialogMaxWidth = ScreenUtils.dip2px(360f)
        val params = window!!
                .attributes
        val dur = (ScreenUtils.getScreenWidth() * 0.25f).toInt()
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth
        } else {
            params.width = (ScreenUtils.getScreenWidth() * 0.95f).toInt()
        }
        window!!.attributes = params
    }

    private fun initListener() {
        btn_sure.setOnClickListener {
            callback.onPasswordEnterResult(TextUtils.equals(
                    this.mPassword,
                    editText_psw.text.toString()
            ))
            dismiss()
        }

        btn_cancel.setOnClickListener({
            callback.onPasswordEnterResult(right = false)//直接认为密码是错的
            dismiss()
        })

        editText_psw.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s == null) {
                    return
                }
                var input = s.toString()
                var editable = editText_psw.text
                val len = input.length
                if (len > 4) {
                    var selEndIndex = Selection.getSelectionEnd(editable)
                    val str = editable.toString()
                    input = str.substring(0, 4)
                    editText_psw.setText(input)
                    editable = editText_psw.text
                    val newLen = editable.length
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length
                    }
                    Selection.setSelection(editable, selEndIndex)
                }
                handleInput(input)
                PLog.i(TAG, "onTextChanged: $input")
            }

            override fun afterTextChanged(s: Editable) {
                PLog.i(TAG, "afterTextChanged: ")
            }
        })

        password_container.setOnClickListener({
            editText_psw.requestFocus()
            showKeyboard()
        })
    }

    private fun initEditText(editText_psw: EditText) {
        editText_psw.customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean {
                return false
            }

            override fun onActionItemClicked(actionMode: ActionMode, menuItem: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode) {}

        }
        editText_psw.isLongClickable = false

        editText_psw.setText("")
        handleInput("")
        editText_psw.requestFocus()
    }

    private fun clearInput() {
        text1.text = ""
        text2.text = ""
        text3.text = ""
        text4.text = ""
    }

    private fun handleInputInner(input: String) {
        try {
            text1.text = input.substring(0, 1)
            text2.text = input.substring(1, 2)
            text3.text = input.substring(2, 3)
            text4.text = input.substring(3, 4)
        } catch (e: Exception) {
            //password length < 4
            e.printStackTrace()
        }

    }

    private fun handleInput(input: String) {
        clearInput()
        if (TextUtils.isEmpty(input)) {
        } else {
            if (input.length > 4) {
                val result = input.substring(0, 4)
                handleInputInner(result)
            } else {
                handleInputInner(input)
            }

        }
    }

    private fun showKeyboard() {
        val imm = editText_psw.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED)
    }

    private fun hideKeyboard() {
        try {
            val imm = editText_psw.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText_psw.windowToken, 0)
        } catch (e: Exception) {
            PLog.e(TAG, "hide keyboard without not open it!")
            e.printStackTrace()
        }

    }

    override fun show() {
        if (TextUtils.isEmpty(mPassword)) {
            callback.onPasswordEnterResult(true)
        } else {
            super.show()
        }
    }

    override fun dismiss() {
        hideKeyboard()
        super.dismiss()
    }

    override fun release() {

    }

}