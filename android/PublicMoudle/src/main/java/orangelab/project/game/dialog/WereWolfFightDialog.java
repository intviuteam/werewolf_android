package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.common.utils.MessageUtils;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;

/**
 * game_werewolf
 * 2018/5/10 下午7:14
 * Mystery
 */

public class WereWolfFightDialog extends WereWolfBaseDialog {

    public WereWolfFightDialog(@NonNull Context context, WereWolfSelectMember member) {
        super(context);
        View view = View.inflate(context, R.layout.layout_werewolf_dialog_boom, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.id_werewolf_boom_iv);
        TextView textView = (TextView) view.findViewById(R.id.id_werewolf_boom_message);

        imageView.setImageResource(R.mipmap.room_role_knight);
        setTitle(MessageUtils.getString(R.string.dialog_hint));
        textView.setText(WereWolfString.getString(R.string.string_werewolf_game_fight, Integer.toString(member.memberPosition)));
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }
}
