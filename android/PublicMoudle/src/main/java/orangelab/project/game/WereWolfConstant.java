package orangelab.project.game;

/**
 * game_werewolf
 * 2017/11/27 下午6:01
 * Mystery
 */

public class WereWolfConstant {
    public static final String GAME_TYPE_SIMPLE = "simple";
    public static final String GAME_TYPE_NORMAL = "normal";
    public static final String GAME_TYPE_PRE_SIMPLE = "pre_simple";
    public static final String GAME_TYPE_PRE_SIMPLE_NEW = "pre_simple_new";
    public static final String GAME_TYPE_SIMPLE_6 = "simple_6";
    public static final String GAME_TYPE_SIMPLE_9 = "simple_9";
    public static final String GAME_TYPE_SIMPLE_10 = "simple_10";
    public static final String GAME_TYPE_NORMAL_GUARD = "normal_guard";
    public static final String GAME_TYPE_HIGH_KING = "high_king";
    public static final String GAME_FRIEND_REQUEST = "werewolf_friend_request";

    public static final String ROLE_DEMON = "demon";
    public static final String ROLE_MAGICIAN = "magician";
    public static final String ROLE_GOD = "role_god";
    public static final String ROLE_NOT_GOD = "role_not_god";
    public static final String ROLE_KNIGHT = "knight";
    public static final String ROLE_BLACK_WOLF_KING = "black_werewolf_king";

    public static final String ROLE_YNLS = "YNLS";//语音家 + 女巫 + 猎人 + 守卫
    public static final String ROLE_YNLM = "YNLM";//预言家 + 女巫 + 猎人 + 魔术师
    public static final String ROLE_LLLW = "LLLW";//狼人 + 狼人 + 狼人 + 狼王
    public static final String ROLE_LLLE = "LLLE";//狼人 + 狼人 + 狼人 + 恶魔
    public static final String ROLE_YNQS = "YNQS";//预言家 + 女巫 + 骑士 + 守卫
    public static final String ROLE_LLLH = "LLLH";//狼人 + 狼人+ 狼人+ 黑狼王

    public static final String UPDATE_CONFIG = "update_config";
    public static final String LEVEL = "level";
    public static final String CAN_CUT_SPEAKER = "can_cut_speaker";
    public static final String PEOPLE_PARTS = "people_parts";
    public static final String WEREWOLF_PARTS = "werewolf_parts";

    public static final String DEMON_CHECK = "demon_check";
    public static final String DEMON_CHECK_RESULT = "demon_check_result";

    public static final String EXCHANGE = "exchange";
    public static final String EXCHANGE_RESULT = "exchange_result";

    public static final String FIGHT = "fight";
    public static final String FIGHT_AWAY = "fight_away";
    public static final String FIGHT_AWAY_RESULT = "fight_away_result";
    public static final String FIGHT_STATE = "fight_state";
    public static final String FIGHT_APOLOGIZE_DEATH = "apologize_death";

    public static final String OBSERVER_LEAVE = "observer_leave";
    public static final String OBSERVER_JOIN = "observer_join";

}
