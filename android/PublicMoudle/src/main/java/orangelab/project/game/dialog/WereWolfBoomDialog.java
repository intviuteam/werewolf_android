package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/12/1 下午1:33
 * Mystery
 */

public class WereWolfBoomDialog extends WereWolfBaseDialog {

    public WereWolfBoomDialog(@NonNull Context context, boolean isWolfKing, WereWolfSelectMember member) {
        super(context);
        View view = View.inflate(context, R.layout.layout_werewolf_dialog_boom, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.id_werewolf_boom_iv);
        TextView textView = (TextView) view.findViewById(R.id.id_werewolf_boom_message);
        TextView positionView = (TextView) view.findViewById(R.id.id_member_position);

        if (isWolfKing) {
            imageView.setImageResource(R.mipmap.room_role_werewolf_king);
        } else {
            imageView.setImageResource(R.mipmap.room_role_wolf);
        }
        setTitle(MessageUtils.getString(R.string.string_werewolf_boom_dialog_title));
        positionView.setText(Integer.toString(member.memberPosition));
        textView.setText(WereWolfString.getString(R.string.werewolf_game_boom_message));
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }
}
