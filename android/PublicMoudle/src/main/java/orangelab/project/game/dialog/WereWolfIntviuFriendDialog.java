package orangelab.project.game.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.datasource.GlobalUserState;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.model.FriendsResult;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * game_werewolf
 * 2017/12/7 上午10:46
 * Mystery
 */

public class WereWolfIntviuFriendDialog extends WereWolfBaseDialog {

    private ListView listView;
    private TextView placeHolder;
    private View placeHolderView;
    private IntviuFriendAdapter adapter;
    private Activity activity;
    private boolean needBackAndDestroy = true;
    private String mAction;

    public void setNeedBackAndDestroy(boolean needBackAndDestroy) {
        this.needBackAndDestroy = needBackAndDestroy;
    }

    public WereWolfIntviuFriendDialog(String action, Activity activity) {
        super(activity);
        this.mAction = action;
        this.activity = activity;
        setTitle(MessageUtils.getString(R.string.intviu_friend_title));
        View view = LayoutInflater.from(activity).inflate(R.layout.layout_werewolf_intviu_friend, null);
        placeHolder = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolderView = view.findViewById(R.id.intviufriend_placeholder_iv);
        placeHolder.setVisibility(View.VISIBLE);
        listView = (ListView) view.findViewById(R.id.intviu_friend);
        adapter = new IntviuFriendAdapter(mAction, activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(0);
        listView.setOnItemClickListener((parent, view1, position, id) -> {

            IntviuFriendDialog.ViewHolder holder = (IntviuFriendDialog.ViewHolder) view1.getTag();
            IntviuFriendDialog.FriendItem item = adapter.getItem(position);
            if (item == null) {
                return;
            }

            if (TextUtils.equals(mAction, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST)) {
                if (item.hasCheck) {
                    item.hasCheck = false;
                    if (holder != null) {
                        holder.radio.setChecked(false);
                    }
                    adapter.removeSelected(item);
                } else {
                    item.hasCheck = true;
                    if (holder != null) {
                        holder.radio.setChecked(true);
                    }
                    adapter.addSelected(item);
                }
                return;
            }

            if (TextUtils.equals(mAction, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST_MINI_GAME)) {
                UserData leftUser = UserData.createUserData(
                        item.id,
                        item.name,
                        item.sex,
                        item.image
                );

                UserData rightUser = UserData.createUserData(
                        GlobalUserState.getGlobalState().getUserId(),
                        GlobalUserState.getGlobalState().getUserName(),
                        GlobalUserState.getGlobalState().getUserSex(),
                        GlobalUserState.getGlobalState().getUserIcon()
                );
                LCIMConversationActivity.Launch(getContext(), "", leftUser.userId, leftUser, rightUser, true, true);
                dimissAndDestroy();
                return;
            }
        });
        setDialogContentView(view);

        if (TextUtils.equals(mAction, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST)) {
            setButtonType(TWO_BUTTON);
            getTwoButtonRight().setText(MessageUtils.getString(R.string.werewolf_game_intviu_friend_btn_text));
            getTwoButtonLeft().setText(MessageUtils.getString(R.string.action_cancel));
            getTwoButtonRight().setOnClickListener(v -> {
                try {
                    if (adapter == null) return;
                    if (adapter.getCount() == 0) {
                        Toast.makeText(activity, R.string.intviu_friend_select, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (adapter.getSelected().size() > 0) {
                        LeanCloudChatHelper.sendIntviuToConversation(
                                GlobalUserState.getGlobalState().getCurRoomId(),
                                GlobalUserState.getGlobalState().getPassword(),
                                adapter.getSelected()
                        );
                        Toast.makeText(activity, R.string.intviu_friend_send, Toast.LENGTH_SHORT).show();
                        dimissAndDestroy();
                    } else {
                        Toast.makeText(activity, R.string.intviu_friend_select, Toast.LENGTH_SHORT).show();
                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            });
            getTwoButtonLeft().setOnClickListener(v -> {
                dimissAndDestroy();
            });
        } else {
            setButtonType(ONE_BUTTON);
            getOneButton().setBackgroundResource(R.drawable.drawable_werewolf_cancel_button);
            getOneButton().setText(MessageUtils.getString(R.string.action_cancel));
            getOneButton().setTextColor(getContext().getResources().getColor(R.color.color_dialog_cancel_text_color));
            getOneButton().setOnClickListener(v -> {
                dimissAndDestroy();
            });
        }

        UnifiedBridge.RegisterHandler(this, mAction, value -> {
            Utils.runSafely(() -> {
                FriendsResult result = GsonHelper.getGson().fromJson(value.getModel().params.toString(), FriendsResult.class);
                runSafely(() -> {
                    if (listView == null || adapter == null) {
                        showPlaceHolder();
                        return;
                    }
                    if (result != null && result.list != null && result.list.size() > 0) {
                        goneShowPlaceHolder();
                        if (adapter != null) {
                            adapter.updateDatas(result.list);
                        }
                    } else {
                        showPlaceHolder();
                    }
                });
            });
        });
        UnifiedBridge.Api().SendCommendIntoRN(mAction, true);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        dimissAndDestroy();
    }

    private void dimissAndDestroy() {
        onlyDismiss();
        destroy();
    }

    private void onlyDismiss() {
        dismiss();
    }

    private void destroy() {
        if (listView != null) {
            listView = null;
        }
        if (adapter != null) {
            adapter.destroy();
            adapter = null;
        }
        if (placeHolder != null) {
            placeHolder = null;
        }

        if (activity != null) {
            if (needBackAndDestroy) {
                activity.finish();
            }
            activity = null;
        }
        UnifiedBridge.UnRegisterHandler(this);
        EventBus.getDefault().unregister(this);
    }

    private void showPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.VISIBLE);
            placeHolder.setText(MessageUtils.getString(R.string.intviu_friend_empty));
            placeHolder.setVisibility(View.VISIBLE);
        }
        if (listView != null) {
            listView.setVisibility(View.GONE);
        }
    }

    private void goneShowPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.GONE);
            placeHolder.setVisibility(View.GONE);
        }
        if (listView != null) {
            listView.setVisibility(View.VISIBLE);
        }
    }

    private static class IntviuFriendAdapter extends BaseAdapter {

        private Context context;
        private List<IntviuFriendDialog.FriendItem> datas;
        private List<IntviuFriendDialog.FriendItem> selected;
        private String mAction;

        public IntviuFriendAdapter(String action, Context context) {
            this.context = context;
            mAction = action;
            datas = new ArrayList<>();
            selected = new ArrayList<>();
        }

        public void destroy() {
            datas.clear();
            context = null;
        }

        public List<IntviuFriendDialog.FriendItem> getSelected() {
            return selected;
        }

        public void addSelected(IntviuFriendDialog.FriendItem item) {
            if (selected.contains(item))
                return;
            selected.add(item);
        }

        public void removeSelected(IntviuFriendDialog.FriendItem item) {
            selected.remove(item);
        }

        public void updateDatas(List<IntviuFriendDialog.FriendItem> update) {
            if (update != null) {
                datas = update;
                notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public IntviuFriendDialog.FriendItem getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            IntviuFriendDialog.ViewHolder holder = null;
            if (context == null) {
                return convertView;
            }
            if (convertView == null) {
                holder = new IntviuFriendDialog.ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.layout_werewolf_intviu_friend_item, null);
                holder.userhead = (UserHeadView) convertView.findViewById(R.id.intviufriend_ic);
                holder.userhead.getRealHeadImage().setBorderWidth(2);
                holder.userhead.getRealHeadImage().setBorderColor(context.getResources().getColor(R.color.werewolf_dialog_member_none_selected));
                holder.level = (TextView) convertView.findViewById(R.id.intviufriend_level);
                holder.name = (TextView) convertView.findViewById(R.id.intviufriend_name);
                holder.radio = (RadioButton) convertView.findViewById(R.id.intviufriend_check);

                holder.littleOnline = (ImageView) convertView.findViewById(R.id.intviufriend_online);
                holder.littleRoom = (ImageView) convertView.findViewById(R.id.intviufriend_littleroom);
                holder.roomId = (TextView) convertView.findViewById(R.id.intviufriend_roomId);
                holder.status = (TextView) convertView.findViewById(R.id.intviufriend_status);

                convertView.setTag(holder);

            } else {
                holder = (IntviuFriendDialog.ViewHolder) convertView.getTag();
            }
            IntviuFriendDialog.FriendItem item = getItem(position);
            if (TextUtils.equals(mAction, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST_MINI_GAME)) {
                holder.radio.setVisibility(View.GONE);
            }
            holder.radio.setFocusable(false);
            holder.radio.setClickable(false);
            if (item != null) {
                holder.userhead.setUserHeadImageUrl(item.image);
                holder.userhead.setUserSex(item.sex);
                holder.name.setText(item.name);
                holder.level.setText("Lv." + item.game.level);
                holder.radio.setVisibility(View.VISIBLE);
                item.game_status = item.turnToGameStatus();
                if (item.game_status == 1) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.INVISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_online_txt));
                } else if (item.game_status == 2) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_prepare));
                } else if (item.game_status == 3) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_gaming));
                } else {
                    holder.littleOnline.setVisibility(View.GONE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.GONE);
                    holder.status.setVisibility(View.GONE);
                }

                holder.radio.setChecked(item.hasCheck);
            }
            return convertView;
        }
    }
}
