package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import com.networktoolkit.transport.Constant;

import java.util.List;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;

/**
 * game_werewolf
 * 2018/4/24 下午1:48
 * Mystery
 */

public class WereWolfFightAwayDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfFightAwayDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.string_werewolf_knight_dialog_title), WereWolfConstant.FIGHT_AWAY, ONE_BUTTON);
    }
}
