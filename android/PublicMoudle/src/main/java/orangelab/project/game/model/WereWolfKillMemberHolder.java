package orangelab.project.game.model;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2017/11/30 下午2:44
 * Mystery
 */

public class WereWolfKillMemberHolder implements Keepable, Destroyable {

    private LinearLayout mLinearLayout;
    private Context mContext;

    private int mPosition = -1;
    private List<Integer> mKillPositions;
    private WereWolfSelectMemberHolder mHolder;


    public WereWolfKillMemberHolder(View root) {
        mContext = root.getContext();
        mLinearLayout = (LinearLayout) root.findViewById(R.id.id_werewolf_dialog_wolf_member_container);
        mKillPositions = new ArrayList<>();
        mHolder = new WereWolfSelectMemberHolder(root);
    }

    public void setPosition(int position) {
        mPosition = position;
        mHolder.setPosition(position);
    }

    public int getPosition() {
        return mPosition;
    }

    public void setName(String name) {
        mHolder.setName(name);
    }

    public void setHead(String url) {
        mHolder.setHead(url);
    }

    public void setSelect(boolean flag) {
        mHolder.setSelect(flag);
    }

    public void setRole(String role) {
        mHolder.setRole(role);
    }

    public void setBadge(boolean isSheriff, boolean isLover) {
        mHolder.setBadge(isSheriff, isLover);
    }

    public void updateKillPositions(Map<Integer, Integer> map) {
        if (map == null) {
            return;
        }
        Iterator iterator = map.entrySet().iterator();
        mKillPositions.clear();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Integer> entry = (Map.Entry<Integer, Integer>) iterator.next();
            int kill = entry.getKey() + 1;
            int killed = entry.getValue() + 1;

            if (killed == mPosition) {
                /**
                 * 被杀的是这个人
                 */
                mKillPositions.add(kill);
            }
        }
        updateKillPositionsView(mKillPositions);
    }

    private void updateKillPositionsView(List<Integer> integers) {
        mLinearLayout.removeAllViews();
        for (Integer i : integers) {
            TextView t = (TextView) View.inflate(mContext, R.layout.layout_werewolf_dialog_kill_position, null);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ScreenUtils.dip2px(15), ScreenUtils.dip2px(15));
            t.setLayoutParams(layoutParams);
            t.setText(Integer.toString(i));
            mLinearLayout.addView(t);
        }
    }

    @Override
    public void destroy() {
        mKillPositions.clear();
    }

}
