package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/11/29 下午4:29
 * Mystery
 */

public class WereWolfSeerCheckResultDialog extends WereWolfBaseDialog {

    public WereWolfSeerCheckResultDialog(@NonNull Context context, String role, WereWolfSelectMember member) {
        super(context);
        setTitle(MessageUtils.getString(R.string.operation_check));
        View view = View.inflate(context, R.layout.layout_werewolf_check_result, null);

        WereWolfSelectMemberHolder holder = new WereWolfSelectMemberHolder(view);
        holder.setPosition(member.memberPosition);
        holder.setSelfPosition(-1);
        holder.setLastProtectPosition(-1);
        holder.setName(member.memberName);
        holder.setHead(member.memberImage);
        holder.setSelect(false);

        TextView textView = (TextView) view.findViewById(R.id.id_werewolf_checkresult_text);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(MessageUtils.getString(R.string.werewolf_game_check_result_text, WereWolfString.getString(MessageUtils.getManType(role))), Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(MessageUtils.getString(R.string.werewolf_game_check_result_text, WereWolfString.getString(MessageUtils.getManType(role)))));
        }
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }


}
