package orangelab.project.game.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.manager.AppManager;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.WereWolfGameProcess;
import orangelab.project.game.adapter.WereWolfOnLineMembersAdapter;
import orangelab.project.game.comparator.WereWolfObserverMemberComparator;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.view.RecycleViewDivider;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class WereWolfOnlineMembersFragment extends Fragment implements Destroyable, Keepable {

    private final String TAG = "WereWolfOnlineMembersFragment";

    private View ivBack;//回退键
    private TextView tvTitle;//标题栏
    private RecyclerView recyclerView;//成员列表
    private Context mContext;
    private WereWolfGameContext wereWolfGameContext;

    private List<WereWolfObserverMember> memberList = new ArrayList<>();
    private WereWolfOnLineMembersAdapter adapter;

    public WereWolfOnlineMembersFragment() {
        // Required empty public constructor
    }

    public void setWereWolfGameContext(WereWolfGameContext wereWolfGameContext) {
        this.wereWolfGameContext = wereWolfGameContext;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_online_members, container, false);

        /**
         * 这里目前做一个修正
         */
        View titleView = view.findViewById(R.id.ll_online_number_title);
        if (AppManager.GreaterLollipop()) {
            /**
             * 如果超过5.0并且需要为rootView设置一个padding
             */
            titleView.setPadding(0, ScreenUtils.dip2px(25), 0, 0);
        } else {

        }

        initView(view);
        initData();
        initListener();

        return view;
    }

    private void initView(View view) {
        ivBack = view.findViewById(R.id.iv_back);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_online_members);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(mContext));
        recyclerView.addItemDecoration(new RecycleViewDivider(mContext, LinearLayoutManager.HORIZONTAL, R.drawable.drawable_split_line_gray));
    }

    private void initData() {
        adapter = new WereWolfOnLineMembersAdapter(mContext, memberList);
        adapter.setOnLineMemberClickedListener((position) -> {
            if (position >= 0 && position < memberList.size()) {
                WereWolfObserverMember member = memberList.get(position);
                if (member != null) {
                    PersonalInfoActivity.Launch(mContext, member.memberId);
                }
            } else {
                PLog.e(TAG, "error, try to click an invalid position!");
            }
        });
        recyclerView.setAdapter(adapter);
        updatePlayerList();
    }

    private void initListener() {
        if (wereWolfGameContext != null) {
            wereWolfGameContext.registerProcessListener(observerHandler);
        }
        ivBack.setOnClickListener(v -> onBackPressed());
    }

    private void updatePlayerList() {
        if (wereWolfGameContext != null) {
            List<WereWolfGameMember> allGameMember = wereWolfGameContext.getAllGameMember();
            List<WereWolfObserverMember> allObserverMember = wereWolfGameContext.getAllObserverMember();
            allObserverMember.addAll(allGameMember);
            Collections.sort(allObserverMember, new WereWolfObserverMemberComparator());
            this.memberList = allObserverMember;
            if (adapter != null) {
                adapter.updateMemberList(memberList);
            }
        }
    }

    private WereWolfGameProcess observerHandler = new WereWolfGameProcess() {
        @Override
        public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleObserverJoin(WereWolfObserverMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleObserverLeave(WereWolfObserverMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
            updatePlayerList();
            return false;
        }

        @Override
        public boolean handleRestoreRoom(RestoreResult restoreResult) {
            updatePlayerList();
            return false;
        }
    };

    private void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public String getTAG() {
        return TAG;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroy();
    }

    @Override
    public void destroy() {
        if (wereWolfGameContext != null) {
            wereWolfGameContext.unRegisterProcessListener(observerHandler);
        }
        memberList.clear();
        adapter.notifyDataSetChanged();
    }
}
