package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/6 下午5:38
 * Mystery
 */

public class WereWolfHandOverDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfHandOverDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.handover_title), Constant.HAND_OVER, TWO_BUTTON);
    }
}
