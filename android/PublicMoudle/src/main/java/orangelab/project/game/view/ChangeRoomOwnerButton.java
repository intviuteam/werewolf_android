package orangelab.project.game.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.R;
import orangelab.project.common.engine.RoomSocketEngineHelper;

/**
 * game_werewolf
 * 2017/6/17 上午10:21
 * Mystery
 */

public class ChangeRoomOwnerButton extends TextView {

    private boolean available = true;

    public ChangeRoomOwnerButton(Context context) {
        this(context, null);
    }

    public ChangeRoomOwnerButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChangeRoomOwnerButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        available();
        setOnClickListener(v -> {
            if (available) {
                unavailable();
                /**
                 * 执行换房主
                 */
                RoomSocketEngineHelper.resetMaster();
            }
        });
    }

    public void setAvailable(boolean available) {
        this.available = available;
        if (available) {
            available();
        } else {
            unavailable();
        }
    }

    public void available() {
        available = true;
        setBackgroundResource(R.drawable.change_room_owner_available);
        setTextColor(getResources().getColor(R.color.changeroom_owner_available));
    }

    public void unavailable() {
        available = false;
        setBackgroundResource(R.drawable.change_room_owner_unavailable);
        setTextColor(getResources().getColor(R.color.changeroom_owner_unavailable));
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
    }
}
