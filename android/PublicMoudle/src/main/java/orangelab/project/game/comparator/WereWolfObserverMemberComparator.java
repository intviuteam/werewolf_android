package orangelab.project.game.comparator;

import orangelab.project.game.model.WereWolfObserverMember;
import com.toolkit.action.Keepable;

import java.util.Comparator;

/**
 * Created by lloydfinch on 05/02/2018.
 * <p>
 * //游戏房class比较器，位置小的排在上面
 */

public class WereWolfObserverMemberComparator implements Comparator<WereWolfObserverMember>, Keepable {


    @Override
    public int compare(WereWolfObserverMember o1, WereWolfObserverMember o2) {
        if (o1 == null || o2 == null) {
            return 0;
        }
        return o1.memberPosition - o2.memberPosition;
    }

}
