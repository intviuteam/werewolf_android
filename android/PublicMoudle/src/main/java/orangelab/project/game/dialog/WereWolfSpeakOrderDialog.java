package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/11/30 下午6:50
 * Mystery
 */

public class WereWolfSpeakOrderDialog extends WereWolfBaseDialog {

    public WereWolfSpeakOrderDialog(@NonNull Context context, WereWolfSelectMember left, WereWolfSelectMember right) {
        super(context);
        setTitle(WereWolfString.getString(R.string.choose_speech_direction));

        View view = View.inflate(context, R.layout.layout_werewolf_dialog_speakorder, null);

        View leftView = view.findViewById(R.id.id_werewolf_left_member);

        View rightView = view.findViewById(R.id.id_werewolf_right_member);

        WereWolfSelectMemberHolder holderLeft = new WereWolfSelectMemberHolder(leftView);
        holderLeft.showAsSelectMember(left);

        WereWolfSelectMemberHolder holderRight = new WereWolfSelectMemberHolder(rightView);
        holderRight.showAsSelectMember(right);

        setDialogContentView(view);
        setButtonType(TWO_BUTTON);

        Button leftBtn = getTwoButtonLeft();
        leftBtn.setBackgroundResource(R.drawable.drawable_werewolf_confirm_button);
        leftBtn.setText(WereWolfString.getString(R.string.left_speech));
        leftBtn.setTextColor(context.getResources().getColor(android.R.color.white));
        leftBtn.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendSpeechDirection(Constant.LEFT);
            dismiss();
        });

        Button rightBtn = getTwoButtonRight();
        rightBtn.setBackgroundResource(R.drawable.drawable_werewolf_confirm_button);
        rightBtn.setTextColor(context.getResources().getColor(android.R.color.white));
        rightBtn.setText(WereWolfString.getString(R.string.right_speech));
        rightBtn.setOnClickListener(v -> {
            RoomSocketEngineHelper.sendSpeechDirection(Constant.RIGHT);
            dismiss();
        });
    }
}
