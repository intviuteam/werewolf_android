package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/11/29 下午3:53
 * Mystery
 */

public class WereWolfIdentityDialog extends WereWolfBaseDialog {

    private ImageView roleImageView;
    private TextView skill_message;
    private TextView point_message;

    private static final int good_people_point = R.string.goodman_point;
    private static final int bad_people_point = R.string.badman_point;

    public WereWolfIdentityDialog(@NonNull Context context, String role) {
        super(context);
        setTitle(MessageUtils.getRoleIdentityDialogTitle());
        View view = View.inflate(context, R.layout.layout_werewolf_role_identity, null);
        roleImageView = (ImageView) view.findViewById(R.id.id_werewolf_role_imageview);
        skill_message = (TextView) view.findViewById(R.id.id_werewolf_role_skill);
        point_message = (TextView) view.findViewById(R.id.id_werewolf_role_target);
        setDialogContentView(view);
        initRoleMessage(role);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }

    private void initRoleMessage(String role) {
        if (!TextUtils.isEmpty(role)) {
            if (MessageUtils.getRoleImage(role) > 0) {
                roleImageView.setBackgroundResource(MessageUtils.getRoleImage(role));
            }
            if (role.equals(Constant.ROLE_SEER)) {
                skill_message.setText(R.string.skill_seek);
                point_message.setText(good_people_point);
            } else if (role.equals(Constant.ROLE_PEOPLE)) {
                skill_message.setText(R.string.skill_people);
                point_message.setText(good_people_point);
            } else if (role.equals(Constant.ROLE_WITCH)) {
                skill_message.setText(R.string.skill_witch);
                point_message.setText(good_people_point);
            } else if (role.equals(Constant.ROLE_HUNTER)) {
                skill_message.setText(R.string.skill_hunter);
                point_message.setText(good_people_point);
            } else if (role.equals(Constant.ROLE_WOLF)) {
                skill_message.setText(R.string.skill_wolf);
                point_message.setText(bad_people_point);
            } else if (role.equals(Constant.ROLE_CUPID)) {
                skill_message.setText(R.string.skill_cupid);
                point_message.setText(R.string.point_cupid);
            } else if (role.equals(Constant.ROLE_GUARD)) {
                skill_message.setText(MessageUtils.getString(R.string.role_guard_skill));
                point_message.setText(good_people_point);
            } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
                skill_message.setText(MessageUtils.getString(R.string.role_werewolf_king_skill));
                point_message.setText(bad_people_point);
            } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
                skill_message.setText(MessageUtils.getString(R.string.werewolf_role_demon_skill));
                point_message.setText(bad_people_point);
            } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
                skill_message.setText(MessageUtils.getString(R.string.werewolf_role_magician_skill));
                point_message.setText(good_people_point);
            } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
                skill_message.setText(MessageUtils.getString(R.string.werewolf_role_knight_skill));
                point_message.setText(R.string.werewolf_role_knight_point);
            } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
                skill_message.setText(MessageUtils.getString(R.string.werewolf_role_dark_wolf_king_skill));
                point_message.setText(R.string.werewolf_role_dark_wolf_king_point);
            }
        }
    }
}
