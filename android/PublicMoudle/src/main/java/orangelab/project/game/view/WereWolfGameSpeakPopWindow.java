package orangelab.project.game.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.androidtoolkit.view.ScreenUtils;
import com.R;

/**
 * game_werewolf
 * 2017/12/7 下午1:10
 * Mystery
 */

public class WereWolfGameSpeakPopWindow extends PopupWindow {

    private ImageView speak_iv;
    private AnimationDrawable drawable;

    public WereWolfGameSpeakPopWindow(Context context) {
        super(context);
        View rootView = LayoutInflater.from(context).inflate(R.layout.layout_speak_pop, null);
        speak_iv = (ImageView) rootView.findViewById(R.id.speak_iv);
        speak_iv.setImageResource(R.drawable.drawable_werewolf_speak_big);
        drawable = (AnimationDrawable) speak_iv.getDrawable();
        setContentView(rootView);
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(false);
        setWidth(ScreenUtils.dip2px(120));
        setHeight(ScreenUtils.dip2px(120));
        setBackgroundDrawable(new BitmapDrawable(context.getResources()));
    }

    public void showPopWindow(View parent) {
        if (!isShowing()) {
            showAtLocation(parent, Gravity.CENTER, 0, 0);
            if (drawable != null) {
                drawable.start();
            }
        }
    }

    public void dismissPopWindow() {
        if (isShowing()) {
            if (drawable != null) {
                drawable.stop();
            }
            dismiss();

        }
    }
}