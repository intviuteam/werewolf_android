package orangelab.project.game.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.config.GameRoomConfig;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.utils.GameShareFactory;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;

import com.networktoolkit.transport.Constant;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.ToolKit;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.ShareTools;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.android.schedulers.AndroidSchedulers;

/**
 * game_werewolf
 * 2017/12/8 上午11:48
 * Mystery
 */

public class WereWolfGameOverDialog extends SafeDialog implements View.OnClickListener, WereWolfDialog {

    private Activity mActivity;
    private GameOverResult mGameOverResult;
    private WereWolfGameContext mWolfGameContext;

    private ImageView mTitleImageView;
    private TextView mExperience;

    private WereWolfGameOverPartView mParty1;  //用于展示含有自己的
    private WereWolfGameOverPartView mParty2;  //
    private WereWolfGameOverPartView mParty3;  //

    private Button mConfirm;

    private GameShareFactory.Builder builder;
    private TextView shareTitle;
    private View shareWeChat;
    private View shareWeChatFriend;
    private View shareQQ;
    private View shareLine;
    private View shareFaceBook;
    private View shareWhatsApp;
    private View game_bg;//游戏局部背景
    private RxPermissions permissions;

    private String mSelfTeamType;
    private String mWinTeamType;
    private List<WereWolfSelectMember> mMember;
    private Map<String, List<WereWolfSelectMember>> mMembersMap;


    public WereWolfGameOverDialog(@NonNull WereWolfGameContext context, GameOverResult gameOverResult, List<WereWolfSelectMember> members) {
        super(context.getAndroidContext(), R.style.DarkDialog);
        this.mWolfGameContext = context;
        this.mActivity = context.getActivity();
        this.mGameOverResult = gameOverResult;
        this.mMember = members;
        this.permissions = new RxPermissions(mActivity);
        if (gameOverResult.roles.size() == 2) {
            setContentView(R.layout.layout_werewolf_gameover_2);
        } else {
            setContentView(R.layout.layout_werewolf_gameover);
        }
        builder = new GameShareFactory.Builder();
        builder.setImageUrl(GlobalUserState.getGlobalState().getUserIcon());
        builder.setUserName(GlobalUserState.getGlobalState().getUserName());
        builder.setGameType(getGameTypeStringForShare(mGameOverResult.roles));

        mTitleImageView = (ImageView) findViewById(R.id.id_werewolf_gameover_title);
        mExperience = (TextView) findViewById(R.id.id_werewolf_gameover_experience);
        mConfirm = (Button) findViewById(R.id.id_werewolf_gameover_confirm);

        mParty1 = new WereWolfGameOverPartView(findViewById(R.id.id_werewolf_gameover_part1));
        mParty2 = new WereWolfGameOverPartView(findViewById(R.id.id_werewolf_gameover_part2));
        mParty3 = new WereWolfGameOverPartView(findViewById(R.id.id_werewolf_gameover_part3));

        shareTitle = (TextView) findViewById(R.id.id_werewolf_gameover_call_friend);
        shareWeChat = findViewById(R.id.share_wechat);
        shareWeChatFriend = findViewById(R.id.share_wechat_friend);
        shareQQ = findViewById(R.id.share_qq);
        shareLine = findViewById(R.id.share_line);
        shareFaceBook = findViewById(R.id.share_facebook);
        shareWhatsApp = findViewById(R.id.share_whatsapp);
        shareLine.setVisibility(View.VISIBLE);
        shareFaceBook.setVisibility(View.VISIBLE);
        shareWhatsApp.setVisibility(View.GONE);

        game_bg = findViewById(R.id.game_bg);

        initDataAndView();

        mConfirm.setOnClickListener(this);
        shareWeChat.setOnClickListener(this);
        shareWeChatFriend.setOnClickListener(this);
        shareQQ.setOnClickListener(this);
        shareLine.setOnClickListener(this);
        shareFaceBook.setOnClickListener(this);
        shareWhatsApp.setOnClickListener(this);

        //国内版隐藏三个分享入口
        if (ProjectConfig.isKuaiWan()) {
            shareFaceBook.setVisibility(View.GONE);
            shareLine.setVisibility(View.GONE);
            shareWhatsApp.setVisibility(View.GONE);
        }

        resizeDialog(ScreenUtils.getCurrentScreenWidth(), WindowManager.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    private void initDataAndView() {
        mMembersMap = new HashMap<>();

        int myselfPosition = mWolfGameContext.getPersonalPosition();

        mWinTeamType = mGameOverResult.win_type;

        boolean mSelfIsWin = false;

        mSelfTeamType = "";

        List<Integer> winPositions = mGameOverResult.winners;

        /**
         * 确定自己是否是成功与失败
         */
        if (winPositions.contains(myselfPosition - 1)) {
            mSelfIsWin = true;
            builder.setWin(mSelfIsWin);
        } else {
            mSelfIsWin = false;
            builder.setWin(mSelfIsWin);
        }

        /**
         * 初始化分享信息
         */
        if (mSelfIsWin) {
            builder.setGameMessage(MessageUtils.getString(R.string.gameover_share_win_text));
            shareTitle.setText(R.string.gameover_share_title_win);
        } else {
            builder.setGameMessage(MessageUtils.getString(R.string.gameover_share_lose_text));
            shareTitle.setText(R.string.gameover_share_title_lose);
        }

        /**
         * 初始化经验信息
         */
        if (mSelfIsWin) {
            mExperience.setText(MessageUtils.getString(R.string.game_over_experience, mGameOverResult.experience.get("win") + ""));
        } else {
            mExperience.setText(MessageUtils.getString(R.string.game_over_experience, mGameOverResult.experience.get("lose") + ""));
        }

        /**
         * 确定自己属于那个阵营以及一些其他数据
         */
        ToolKit.travelMap(mGameOverResult.roles,
                (key, value) -> {
                    List<WereWolfSelectMember> members = new ArrayList<>();

                    for (GameOverResult.GameOverResultRoleItem item : value) {
                        if (item.position + 1 == myselfPosition) {
                            /**
                             *这个人是自己
                             */
                            mSelfTeamType = key;
                        }
                        WereWolfSelectMember member = getMemberFromMembers(item.position + 1);
                        if (member != null) {
                            member.memberRole = item.role;
                            members.add(member);
                        }
                    }

                    mMembersMap.put(key, members);
                });

        /**
         * 数据已经初始化完毕，准备初始化头部图片
         */
        if (mSelfIsWin) {
            if (TextUtils.equals(mSelfTeamType, Constant.WIN_PEOPELE)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_goodman_win_cn_tw);
            } else if (TextUtils.equals(mSelfTeamType, Constant.WIN_THIRD)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_thirdpart_win_cn_tw);
            } else if (TextUtils.equals(mSelfTeamType, Constant.WIN_Wolf)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_wolf_win_cn_tw);
            }

            game_bg.setBackgroundResource(R.mipmap.ic_bg_victory);
        } else {
            if (TextUtils.equals(mSelfTeamType, Constant.WIN_PEOPELE)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_goodman_lose_cn_tw);
            } else if (TextUtils.equals(mSelfTeamType, Constant.WIN_THIRD)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_thirdpart_lose_cn_tw);
            } else if (TextUtils.equals(mSelfTeamType, Constant.WIN_Wolf)) {
                mTitleImageView.setBackgroundResource(R.mipmap.ico_werewolf_wolf_lose_cn_tw);
            }

            game_bg.setBackgroundResource(R.mipmap.ic_bg_failure);
        }

        List<WereWolfSelectMember> members = mMembersMap.remove(mSelfTeamType);
        /**
         * 展示自己阵营数据
         */
        mParty1.needPart(true);
        mParty1.setWinOrLose(mSelfIsWin);
        mParty1.setTitle(MessageUtils.getTeamName(mSelfTeamType));
        mParty1.setData(members);

        if (mSelfIsWin) {
            /**
             * 如果自己赢了，那么下面的一定都是输的
             */
            if (mMembersMap.size() > 1) {
                /**
                 * 含有三方
                 */
                mParty2.needPart(true);
                mParty3.needPart(true);
                ToolKit.travelMap(mMembersMap, (key, value) -> {
                    WereWolfGameOverPartView view = null;
                    if (!mParty2.isUse()) {
                        view = mParty2;
                    } else {
                        view = mParty3;
                    }
                    view.setWinOrLose(false);
                    view.setData(value);
                    view.setTitle(MessageUtils.getTeamName(key));
                    view.setUse(true);
                });
            } else {
                /**
                 * 没有三方
                 */
                mParty2.needPart(true);
                mParty3.needPart(false);
                ToolKit.travelMap(mMembersMap, (key, value) -> {
                    mParty2.setWinOrLose(false);
                    mParty2.setTitle(MessageUtils.getTeamName(key));
                    mParty2.setData(value);
                    mParty2.setUse(true);
                });
            }
        } else {
            /**
             * 如果自己输了
             */
            if (mMembersMap.size() > 1) {
                /**
                 * 有三方，先找到胜利的那个
                 */
                mParty2.needPart(true);
                mParty3.needPart(true);
                /**
                 * mParty3
                 */
                List<WereWolfSelectMember> temp = mMembersMap.remove(mWinTeamType);
                mParty3.setTitle(MessageUtils.getTeamName(mWinTeamType));
                mParty3.setWinOrLose(true);
                mParty3.setData(temp);
                mParty3.setUse(true);
                /**
                 * mParty2
                 */
                ToolKit.travelMap(mMembersMap, (key, value) -> {
                    mParty2.setWinOrLose(false);
                    mParty2.setTitle(MessageUtils.getTeamName(key));
                    mParty2.setData(value);
                    mParty2.setUse(true);
                });
            } else {
                /**
                 * 没有三方
                 */
                mParty2.needPart(true);
                mParty3.needPart(false);
                ToolKit.travelMap(mMembersMap, (key, value) -> {
                    mParty2.setWinOrLose(true);
                    mParty2.setTitle(MessageUtils.getTeamName(key));
                    mParty2.setData(value);
                    mParty2.setUse(true);
                });
            }
        }

    }

    private WereWolfSelectMember getMemberFromMembers(int position) {
        for (WereWolfSelectMember member : mMember) {
            if (member.memberPosition == position) {
                return member;
            }
        }
        return null;
    }

    @Override
    protected void release() {
        mActivity = null;
        mGameOverResult = null;
    }

    /**
     * 分享的游戏类型
     *
     * @param roles
     * @return
     */
    private String getGameTypeStringForShare(Map<String, List<GameOverResult.GameOverResultRoleItem>> roles) {

        String gameType = GameRoomConfig.getGameTypeDescription();
        if (!TextUtils.isEmpty(gameType)) {
            return gameType;
        }

        int count = 0;
        if (roles != null) {
            Iterator iterator = roles.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                List<GameOverResult.GameOverResultRoleItem> value =
                        (List<GameOverResult.GameOverResultRoleItem>) entry.getValue();
                if (value != null) {
                    for (GameOverResult.GameOverResultRoleItem item : value) {
                        count++;
                    }
                }
            }
        }


        if (count == 12) {
            return MessageUtils.getString(R.string.gameover_gametype_normal, Integer.toString(count));
        } else {
            return MessageUtils.getString(R.string.gameover_gametype_simple, Integer.toString(count));
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_werewolf_gameover_confirm) {
            dismiss();
        } else if (id == R.id.share_wechat) {
            shareWechat();
        } else if (id == R.id.share_wechat_friend) {
            shareWeChatFriend();
        } else if (id == R.id.share_qq) {
            shareQQ();
        } else if (id == R.id.share_line) {
            shareLine();
        } else if (id == R.id.share_facebook) {
            shareFaceBook();
        } else if (id == R.id.share_whatsapp) {

        }
//        switch (v.getId()) {
//            case R.id.id_werewolf_gameover_confirm: {
//                dismiss();
//            }
//            break;
//            case R.id.share_wechat: {
//                shareWechat();
//            }
//            break;
//            case R.id.share_wechat_friend: {
//                shareWeChatFriend();
//            }
//            break;
//            case R.id.share_qq: {
//                shareQQ();
//            }
//            break;
//            case R.id.share_line: {
//                shareLine();
//            }
//            break;
//            case R.id.share_facebook: {
//                shareFaceBook();
//            }
//            break;
//            case R.id.share_whatsapp: {
//
//            }
//            break;
//        }
    }

    private void shareQQ() {

        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
        if (ShareTools.existsApp(mActivity, ShareTools.SHARE_QQ)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                shareSoftware(SHARE_MEDIA.QQ);
                                orangelab.project.common.utils.ShareTools.shareSuccessReported(Constant.SHARE_TO_QQ);
                            } else {
                                Toast.makeText(
                                        mActivity,
                                        R.string.permission_read_store_error,
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        });
            }
            //
        } else {
            Toast.makeText(mActivity, R.string.share_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void shareSoftware(SHARE_MEDIA sharemedia) {
        GameShareFactory.shareGameOver(mActivity, builder, sharemedia);
    }

    private void shareFaceBook() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
        if (ShareTools.existsApp(mActivity, ShareTools.SHARE_FACEBOOK)) {
            ThreadToolKit.Sync(() -> shareSoftware(SHARE_MEDIA.FACEBOOK));
        } else {
            Toast.makeText(mActivity, R.string.share_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void shareLine() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
        if (ShareTools.existsApp(mActivity, ShareTools.SHARE_LINE)) {
            if (permissions != null) {
                permissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                ThreadToolKit.Sync(() -> shareSoftware(SHARE_MEDIA.LINE));
                            } else {
                                Toast.makeText(
                                        mActivity,
                                        R.string.permission_read_store_error,
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        });
            }
        } else {
            Toast.makeText(mActivity, R.string.share_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void shareWechat() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
        if (ShareTools.existsApp(mActivity, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> shareSoftware(SHARE_MEDIA.WEIXIN));
        } else {
            Toast.makeText(mActivity, R.string.share_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void shareWeChatFriend() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
        if (ShareTools.existsApp(mActivity, ShareTools.SHARE_WEIXIN)) {
            ThreadToolKit.Sync(() -> shareSoftware(SHARE_MEDIA.WEIXIN_CIRCLE));
        } else {
            Toast.makeText(mActivity, R.string.share_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showDialog(int seconds) {
        if (!isShowing()) {
            show();
        }
    }

    public static class WereWolfGameOverPartView {

        private ImageView mImageView;
        private TextView mTextView;
        private GridView mGridView;
        private View mRootView;
        private boolean isUse;

        public WereWolfGameOverPartView(View view) {
            mRootView = view;
            mTextView = (TextView) view.findViewById(R.id.id_werewolf_gameover_part_name);
            mImageView = (ImageView) view.findViewById(R.id.id_werewolf_gameover_part_iv);
            mGridView = (GridView) view.findViewById(R.id.id_werewolf_gameover_gridview);
        }

        public void setData(List<WereWolfSelectMember> members) {
            WereWolfGameOverPartAdapter adapter = new WereWolfGameOverPartAdapter(mRootView.getContext(), members);
            mGridView.setAdapter(adapter);
            Utils.customGridViewAndChildHeightAndWidth(mGridView, members.size(), ScreenUtils.dip2px(51), ScreenUtils.dip2px(66));
        }

        public void needPart(boolean flag) {
            if (flag) {
                mRootView.setVisibility(View.VISIBLE);
            } else {
                mRootView.setVisibility(View.GONE);
            }
        }

        public boolean isUse() {
            return isUse;
        }

        public void setUse(boolean use) {
            isUse = use;
        }

        public void setTitle(String title) {
            mTextView.setText(title);
        }

        public void setWinOrLose(boolean flag) {
            if (flag) {
                mTextView.setTextColor(mTextView.getContext().getResources().getColor(R.color.werewolf_gameover_win_part_title));
                mImageView.setImageResource(R.mipmap.ico_werewolf_gameover_win);
            } else {
                mTextView.setTextColor(mTextView.getContext().getResources().getColor(R.color.werewolf_gameover_lose_part_title));
                mImageView.setImageResource(R.mipmap.ico_werewolf_gameover_lose);
            }
        }

        public void needIv(boolean flag) {
            if (flag) {
                mImageView.setVisibility(View.VISIBLE);
            } else {
                mImageView.setVisibility(View.GONE);
            }
        }
    }

    public static class WereWolfGameOverPartAdapter extends BaseAdapter {

        private Context mContext;
        private List<WereWolfSelectMember> mData;

        public WereWolfGameOverPartAdapter(Context context, List<WereWolfSelectMember> data) {
            mContext = context;
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public WereWolfSelectMember getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            WereWolfGameOverItemViewHolder holder = null;
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.layout_werewolf_gameover_item, null);
                holder = new WereWolfGameOverItemViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (WereWolfGameOverItemViewHolder) convertView.getTag();
            }
            WereWolfSelectMember selectMember = getItem(position);
            holder.setHead(selectMember.memberImage);
            holder.setName(selectMember.memberName);
            holder.setPosition(selectMember.memberPosition);
            holder.setRole(selectMember.memberRole);
            if (selectMember.memberPosition == selectMember.myselfPosition) {
                holder.setIsSelfPosition();
            }
            return convertView;
        }
    }

    public static class WereWolfGameOverItemViewHolder {

        private CircleImageView mCircleImageView;
        private TextView mPosition;
        private TextView mNameText;
        private TextView mRoleText;

        public WereWolfGameOverItemViewHolder(View view) {
            mCircleImageView = (CircleImageView) view.findViewById(R.id.id_werewolf_dialog_member_head);
            mPosition = (TextView) view.findViewById(R.id.id_werewolf_dialog_member_position);
            mNameText = (TextView) view.findViewById(R.id.id_werewolf_dialog_member_name);
            mRoleText = (TextView) view.findViewById(R.id.id_werewolf_dialog_member_role);
        }

        public void setName(String name) {
            mNameText.setText(name);
        }

        public void setPosition(int position) {
            mPosition.setText(Integer.toString(position));
        }

        public void setIsSelfPosition() {
            mPosition.setTextColor(mPosition.getContext().getResources().getColor(R.color.werewolf_dialog_member_myself_text));
            mPosition.setBackgroundResource(R.drawable.drawable_werewolf_member_my_position);
            mCircleImageView.setBorderColor(mCircleImageView.getContext().getResources().getColor(R.color.werewolf_dialog_member_myself));
        }

        public void setRole(String role) {
            mRoleText.setText(MessageUtils.getRole(role));
        }

        public void setHead(String head) {
            PicassoUtils.loadImage(mCircleImageView.getContext(), head, mCircleImageView, R.mipmap.default_head);
        }
    }
}
