package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/14 下午1:51
 * Mystery
 */

public class WereWolfDemonCheckDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfDemonCheckDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.check_card_title), WereWolfConstant.DEMON_CHECK, ONE_BUTTON);
    }
}
