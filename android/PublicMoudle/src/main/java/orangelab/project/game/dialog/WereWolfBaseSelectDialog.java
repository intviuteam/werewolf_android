package orangelab.project.game.dialog;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;

import orangelab.project.common.utils.Utils;

import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/11/29 上午11:22
 * Mystery
 */

public class WereWolfBaseSelectDialog extends WereWolfBaseDialog {

    private static final String TAG = "WereWolfBaseDialog";

    protected GridView mGridView;
    protected BaseSelectAdapter mSelectMemberAdapter;
    protected List<Integer> mSelectPosition = new ArrayList<>();

    private int mWantSelectCount = 1;


    public WereWolfBaseSelectDialog(Context context, List<WereWolfSelectMember> gameMembers, int wantSelectCount) {
        super(context);
        initView(context, wantSelectCount, gameMembers);
    }

    private void initView(Context context, int wantSelectCount, List<WereWolfSelectMember> gameMembers) {
        if (wantSelectCount > 0) {
            mWantSelectCount = wantSelectCount;
        }
        View rootView = View.inflate(context, R.layout.layout_werewolf_base_select, null);
        mGridView = (GridView) rootView.findViewById(R.id.id_werewolf_base_dialog_gridview);
        mSelectMemberAdapter = new BaseSelectAdapter(context, gameMembers);
        mGridView.setAdapter(mSelectMemberAdapter);
//        Utils.customGridViewAndChildHeightAndWidth(mGridView, 4, ScreenUtils.dip2px(70), ScreenUtils.dip2px(74));
        mGridView.setOnItemClickListener((parent, view, position, id) -> {
            WereWolfSelectMember member = mSelectMemberAdapter.getItem(position);
            if (member.lastProtectPosition == member.memberPosition) {
                return;
            }
            addSelect(position);
        });
        setDialogContentView(rootView);
    }

    private void addSelect(int position) {
        if (mSelectPosition.contains(position)) {
            mSelectPosition.remove(new Integer(position));
            updateSelect();
            return;
        }
        if (mSelectPosition.size() < mWantSelectCount) {
            mSelectPosition.add(position);
            updateSelect();
            return;
        }
        if (mSelectPosition.size() == mWantSelectCount) {
            mSelectPosition.remove(0);
            mSelectPosition.add(position);
            updateSelect();
            return;
        }
        if (mSelectPosition.size() > mWantSelectCount) {
            Log.i(TAG, "addSelect: select error");
            return;
        }
    }

    public List<Integer> getRealPosition() {
        if (mSelectPosition.size() > 0) {
            List<Integer> result = new ArrayList<>();
            for (Integer i : mSelectPosition) {
                result.add(mSelectMemberAdapter.getItem(i).memberPosition);
            }
            Log.i(TAG, "getRealPosition: " + result);
            return result;
        }
        return null;
    }

    private void updateSelect() {
        Log.i(TAG, "updateSelect: " + mSelectPosition);
        mSelectMemberAdapter.clearSelect();
        for (Integer integer : mSelectPosition) {
            mSelectMemberAdapter.setSelect(integer);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mSelectMemberAdapter != null) {
            mSelectMemberAdapter.destroy();
        }
    }

    @Override
    protected void release() {
        super.release();
        mSelectPosition.clear();
    }

    public static class BaseSelectAdapter extends BaseAdapter implements Destroyable {

        private Context mContext;
        private List<WereWolfSelectMember> mData;

        public BaseSelectAdapter(Context context, List<WereWolfSelectMember> data) {
            mContext = context;
            mData = data;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public WereWolfSelectMember getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void setSelect(int position) {
            WereWolfSelectMember member = getItem(position);
            member.setSelect = true;
            notifyDataSetChanged();
        }

        public List<WereWolfSelectMember> getAll() {
            return mData;
        }

        public void clearSelect() {
            for (WereWolfSelectMember member : mData) {
                member.setSelect = false;
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            WereWolfSelectMemberHolder holder = null;
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.layout_werewolf_dialog_member_view, null);
                holder = new WereWolfSelectMemberHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (WereWolfSelectMemberHolder) convertView.getTag();
            }
            WereWolfSelectMember member = getItem(position);

            holder.setHead(member.memberImage);
            holder.setName(member.memberName);
            holder.setPosition(member.memberPosition);
            holder.setSelfPosition(member.myselfPosition);
            holder.setLastProtectPosition(member.lastProtectPosition);
            holder.updatePositionView();
            holder.setSelect(member.setSelect);
            holder.setRole(member.memberRole);
            holder.setBadge(member.isSheriff, member.isLover);
            return convertView;
        }

        @Override
        public void destroy() {
            if (mData != null) {
                mData.clear();
            }
        }
    }
}
