package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import com.networktoolkit.transport.Constant;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/12/1 上午11:03
 * Mystery
 */

public class WereWolfWitchOperationDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfWitchOperationDialog(Context context, ArrayList<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, WereWolfString.getString(R.string.posion_title), Constant.POISON, TWO_BUTTON);
    }
}
