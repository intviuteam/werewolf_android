package orangelab.project.game;

import android.util.Log;

import orangelab.project.game.model.WereWolfKillMember;
import orangelab.project.game.model.WereWolfSelectMember;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * game_werewolf
 * 2017/12/21 上午10:37
 * Mystery
 */

public class WereWolfDebugConfig {
    private static final String TAG = "WereWolfDebugConfig";
    public static boolean DeveloperAble = false;
    public static int MessageCount = 1;
    public static boolean DeveloperAutoOverSpeech = false;
    public static boolean DeveloperAutoSelect = false;

    public static void clear() {
        DeveloperAutoSelect = false;
        DeveloperAutoOverSpeech = false;
    }

    public static List<Integer> GetSelectKillForDebug(List<WereWolfKillMember> members, int want) {
        List<Integer> result = new ArrayList<>();
        int lasts = -1;
        for (int i = 0; i < want; i++) {
            int size = members.size();
            Random random = new Random();
            int s = random.nextInt(size);
            while (s == lasts) {
                s = random.nextInt(size);
            }
            lasts = s;
            WereWolfKillMember member = members.get(s);
            result.add(member.memberPosition);
        }
        Log.i(TAG, "GetSelectForDebug: " + result);
        return result;
    }


    public static List<Integer> GetSelectForDebug(List<WereWolfSelectMember> members, int want) {
        List<Integer> result = new ArrayList<>();
        int lasts = -1;
        for (int i = 0; i < want; i++) {
            int size = members.size();
            Random random = new Random();
            int s = random.nextInt(size);
            while (s == lasts) {
                s = random.nextInt(size);
            }
            lasts = s;
            WereWolfSelectMember member = members.get(s);
            result.add(member.memberPosition);
        }
        Log.i(TAG, "GetSelectForDebug: " + result);
        return result;
    }
}
