package orangelab.project.game;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.engine.IOrbitEngine;
import orangelab.project.common.engine.OrbitEngineManager;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.model.action.ServerActionFight;
import orangelab.project.common.model.action.ServerActionFightAway;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.common.model.action.ServerActionFightState;
import orangelab.project.common.tool.GsonTurnToolKit;
import orangelab.project.game.dialog.WereWolfHintDialog;
import orangelab.project.game.model.WereWolfExchange;
import orangelab.project.game.model.WereWolfExchangeResult;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfKillMember;
import orangelab.project.game.model.WereWolfObserverMember;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSpeechResult;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.CheckResult;
import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.LockResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.ServerMessageEnterResult;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.common.model.WakeToKillResult;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.constants.VoiceConstants;

import com.networktoolkit.transport.Constant;
import com.toolkit.ToolKit;
import com.toolkit.action.Action;
import com.toolkit.action.Destroyable;
import com.toolkit.action.TravelMap;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/11/28 上午10:22
 * Mystery
 */

public class WereWolfGameContext extends WereWolfGameProcess implements Destroyable, Action<ServerMessageEvent> {

    private static final String TAG = "WereWolfGameContext";
    private static final int MAX_POSITIONS = 12;
    private int mWereWolfMessageMaxCount = 300;//总的消息最大条数
    private int mWereWolfPlayerMessageMaxCount = 200;//玩家消息最大条数
    private int mWereWolfObserverMessageMaxCount = 100;//观战消息最大条数

    private Context mContext;
    private Activity mActivity;
    private ViewGroup mGameRootView;

    private EnterRoomResult mEnterRoomResult;
    private EnterRoomResult.RoomConfig mRoomConfig;

    private SafeHandler mSafeHandler = new SafeHandler();

    private boolean isDestroy = false;

    private String mGameType = "";     //游戏类型
    private String mRoomId = "";       //房间号
    private String mPassword = "";     //房间密码

    private boolean mAudioDisconnect = true; //语音服务器是否链接
    private boolean mSocketDisconnect = false;//Game Socket is Connected;

    private boolean isRoomOwner = false; //是否是房主
    private boolean isGaming = false;
    private boolean isSunSet = false;
    private boolean isDead = false;
    private boolean isPrepare = false;
    private boolean isCanCutSpeak = true;
    private boolean isObserver = false;

    private int mPersonalPosition = -1;
    private int mCurSpeechPosition = -1;
    private String mCurCallTitle = "";
    private String mCurCallType = "";
    private String mSelfRole = "";

    private SparseArray<WereWolfGameMember> mRoomMembers = new SparseArray<>();
    private SparseArray<WereWolfObserverMember> mRoomObserverMembers = new SparseArray<>();

    private List<Integer> mRoomLockedPosition = new ArrayList<>();
    private List<WereWolfGameProcess> mProcessManager = new ArrayList<>(0);
    private List<WereWolfGameProcessLifeCycle> mProcessLifeCycleManager = new ArrayList<>(0);
    private Map<String, IWereWolfService> mInnerServiceMap = new HashMap<>();

    private Action<Boolean> mDestroyFatherAction = null;

    private WereWolfBackGroundMusicManager mBackGroundMusicManager;

//    private IOrbitEngine mOrbitEngine; //Context 进行 托管 OrbitEngine 由于 狼人夜间说话的改动

    public WereWolfGameContext(Activity activity, ViewGroup rootView, EnterRoomResult enterRoomResult) {
        mContext = activity;
        mActivity = activity;
        mEnterRoomResult = enterRoomResult;
        mRoomConfig = mEnterRoomResult.room.config;
        mGameRootView = rootView;
        /**
         * init data from EnterRoomResult
         */
        try {
            initEnterData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        initListener();

        mBackGroundMusicManager = new WereWolfBackGroundMusicManager(this);
    }

    private void initEnterData() {
        /**
         * init Game Type
         */
        mGameType = mEnterRoomResult.room.config.type;
        /**
         * init Room Id
         */
        mRoomId = mEnterRoomResult.room.room_id;
        /**
         * init Room Password
         */
        mPassword = mEnterRoomResult.room.password;
        updateRoomPassword(mPassword);
        /**
         * init my Position
         */
        mPersonalPosition = mEnterRoomResult.position + 1;

        isCanCutSpeak = mEnterRoomResult.room.config.can_cut_speaker;

        /**
         * init roomMember when you enter Room. And init RoomOwner.
         */
        Map<Integer, EnterRoomResult.EnterRoomUserItem> enterRoomUserItemMap = mEnterRoomResult.room.users;
        Iterator iterator1 = enterRoomUserItemMap.entrySet().iterator();
        while (iterator1.hasNext()) {
            Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem> temp = (Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem>) iterator1.next();
            int memberPosition = temp.getKey() + 1;
            EnterRoomResult.EnterRoomUserItem memberData = temp.getValue();
            if (memberPosition == mPersonalPosition) {
                /**
                 * This is me
                 */
                isRoomOwner = memberData.is_master;
            }
            mRoomMembers.put(memberPosition, WereWolfGameMember.FromEnterRoomResult(memberData));
        }

        /**
         * init observerMember when you enter Room,And Observer Member could be null
         */
        try {
            Map<Integer, EnterRoomResult.EnterRoomUserItem> enterRoomObserverUserItemMap = mEnterRoomResult.room.observers;
            Iterator iterator2 = enterRoomObserverUserItemMap.entrySet().iterator();
            while (iterator2.hasNext()) {
                Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem> temp = (Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem>) iterator2.next();
                int memberPosition = temp.getKey() + 1;
                EnterRoomResult.EnterRoomUserItem memberData = temp.getValue();
                mRoomObserverMembers.put(memberPosition, WereWolfObserverMember.TurnIntoWereWolfObserverMember(memberData));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * init some Data about Room Call
         */
        mCurCallTitle = mEnterRoomResult.room.title;
        mCurCallType = mEnterRoomResult.room.export_type;

        /**
         * init locked Data
         */
        mRoomLockedPosition = mEnterRoomResult.room.locked_positions;

        /**
         * init is observer
         */
        isObserver = mEnterRoomResult.is_observer;
    }

    private void initListener() {
        /**
         * init Server Message Event
         */
        RxToolKit.Build(this, ServerMessageEvent.class).action(this).register();

        initAudioSocketListener();
        initGameSocketListener();
        initSocketSendGiftListener();
    }

    private void initGameSocketListener() {
        RxToolKit.Build(this, SocketEvent.SocketConnectStateEvent.class)
                .actionFilter(value -> TextUtils.equals(value.getIntent(), SocketEngine.WEREWOLF))
                .action(value -> {
                    post(() -> {
                        switch (value.getAction()) {
                            case SocketEvent.SOCKET_CONNECT:
                                PLog.e(TAG, "SOCKET_CONNECT");
                                mSocketDisconnect = false;
                                for (WereWolfGameProcess temp : mProcessManager) {
                                    if (temp.handleGameSocketConnect()) {
                                        return;
                                    }
                                }
                                break;
                            case SocketEvent.SOCKET_DISCONNECT:
                                PLog.e(TAG, "SOCKET_DISCONNECT");
                                mSocketDisconnect = true;
                                RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                                for (WereWolfGameProcess temp : mProcessManager) {
                                    if (temp.handleGameSocketDisconnect()) {
                                        return;
                                    }
                                }
                                break;
                        }
                    });
                }).register();
    }

    private void initAudioSocketListener() {
        RxToolKit.Build(this, ServerEvent.AudioServerEvent.class).action(value -> {
            post(() -> {
                switch (value.getAction()) {
                    case ServerEvent.RECONNECT_FROM_AUDIO_SERVER:
                        mAudioDisconnect = false;
                        for (WereWolfGameProcess temp : mProcessManager) {
                            if (temp.handleAudioSocketConnect()) {
                                return;
                            }
                        }
                        break;
                    case ServerEvent.DISCONNECT_FROM_AUDIO_SERVER:
                        mAudioDisconnect = true;
                        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                        for (WereWolfGameProcess temp : mProcessManager) {
                            if (temp.handleAudioSocketDisConnect()) {
                                return;
                            }
                        }
                        break;
                }
            });
        }).register();
    }

    private void initSocketSendGiftListener() {
        RxToolKit.Build(this, SocketEvent.SocketSendGiftEvent.class).action(socketSendGiftEvent -> {
            post(() -> {
                if (isSunSet) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_sunset_gift_button_text)).run();
                    return;
                }
                RoomSocketEngineHelper.SendGiftMessage(socketSendGiftEvent.getPeer_id(), socketSendGiftEvent.getGift_type(),
                        result1 -> {
                            mSafeHandler.postSafely(() -> {
                                if (socketSendGiftEvent.getSuccessCallBack() != null) {
                                    socketSendGiftEvent.getSuccessCallBack().func(result1);
                                }
                            });
                        },
                        result12 -> {
                            mSafeHandler.postSafely(() -> {
                                if (socketSendGiftEvent.getFailedCallBack() != null) {
                                    socketSendGiftEvent.getFailedCallBack().func(result12);
                                }
                            });
                        }
                );
            });
        }).register();
        RxToolKit.Build(this, SocketEvent.SocketSendCardEvent.class).action(socketSendCardEvent -> {
            post(() -> {
                if (isSunSet) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_sunset_gift_button_text)).run();
                    return;
                }
                RoomSocketEngineHelper.SendCardMessage(socketSendCardEvent.getPeer_id(), socketSendCardEvent.getGift_type(),
                        result1 -> {
                            mSafeHandler.postSafely(() -> {
                                if (socketSendCardEvent.getSuccessCallBack() != null) {
                                    socketSendCardEvent.getSuccessCallBack().func(result1);
                                }
                            });
                        },
                        result12 -> {
                            mSafeHandler.postSafely(() -> {
                                if (socketSendCardEvent.getFailedCallBack() != null) {
                                    socketSendCardEvent.getFailedCallBack().func(result12);
                                }
                            });
                        }
                );
            });
        }).register();
    }

    private void processStart(String type, JSONObject jsonObject) {
        try {
            for (WereWolfGameProcessLifeCycle lifeCycle : mProcessLifeCycleManager) {
                if (lifeCycle.onProcessStart(type, jsonObject)) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processEnd(String type, JSONObject jsonObject) {
        try {
            for (WereWolfGameProcessLifeCycle lifeCycle : mProcessLifeCycleManager) {
                if (lifeCycle.onProcessEnd(type, jsonObject)) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processError(Exception e) {
        try {
            for (WereWolfGameProcessLifeCycle lifeCycle : mProcessLifeCycleManager) {
                if (lifeCycle.onProcessError(e)) {
                    return;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public WereWolfGameContext check() {
        if (isDestroy) {
            throw new IllegalArgumentException("Game Context is Destroy");
        }
        return this;
    }

    public void trusteeShipOrbit(IOrbitEngine engine) {
//        this.mOrbitEngine = engine;
    }

    public void orbitEngineAble(boolean able) {
        if (able) {
//            if (mOrbitEngine != null) {
//                mOrbitEngine.openSpeaker();
//            }
            OrbitEngineManager.INSTANCE.openSpeaker();
        } else {
//            if (mOrbitEngine != null && !isWolfRole()) {
//                mOrbitEngine.closeSpeaker();
//            }
            /**
             * 这里加一个判断，由于false情况下 是 天黑的时候，判断一下角色，玩家为狼人 恶魔 白狼王角色时不进行closeSpeaker操作
             */
            if (isWolfRole()) {
                /**
                 * 白狼王，恶魔，狼人不处理此消息
                 */
            } else {
                /**
                 * 其他角色处理此消息
                 */
                OrbitEngineManager.INSTANCE.closeSpeaker();
            }

        }
    }

    public Context getAndroidContext() {
        return mContext;
    }

    public View getGameRootView() {
        return mGameRootView;
    }

    public String getGameType() {
        return mGameType;
    }

    public String getRoomId() {
        return mRoomId;
    }

    public String getPassword() {
        return mPassword;
    }

    public boolean isAudioDisconnect() {
        return mAudioDisconnect;
    }

    public boolean isSocketDisconnect() {
        return mSocketDisconnect;
    }

    public int getCurSpeechPosition() {
        return mCurSpeechPosition;
    }

    public void post(Runnable runnable, long delay) {
        mSafeHandler.postDelaySafely(runnable, delay);
    }

    public void remove(Runnable runnable) {
        mSafeHandler.removeCallbacks(runnable);
    }

    public void post(Runnable runnable) {
        post(runnable, 0);
    }

    public void registerProcessListener(WereWolfGameProcess process) {
        if (process != null && !mProcessManager.contains(process)) {
            mProcessManager.add(process);
        }
    }

    public void unRegisterProcessListener(WereWolfGameProcess process) {
        if (process != null && mProcessManager.contains(process)) {
            mProcessManager.remove(process);
        }
    }

    public EnterRoomResult.ExportConfig getRoomCallConfig() {
        return mRoomConfig.export_request;
    }

    public EnterRoomResult.RoomConfig getRoomConfig() {
        return mRoomConfig;
    }

    public int getPersonalPosition() {
        return mPersonalPosition;
    }

    public boolean isRoomOwner() {
        return isRoomOwner;
    }

    public String getCurCallTitle() {
        return mCurCallTitle;
    }

    public String getCurCallType() {
        return mCurCallType;
    }

    public boolean isGaming() {
        return isGaming;
    }

    public boolean isSunSet() {
        return isSunSet;
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean isPrepare() {
        return isPrepare;
    }

    public boolean isCanCutSpeak() {
        return isCanCutSpeak;
    }

    public boolean isObserver() {
        return isObserver;

        //TODO 测试数据
        //return true;
    }

    public int getSelfPosition() {
        return mPersonalPosition;
    }

    public boolean isSelfPosition(int position) {
        return getSelfPosition() == position;
    }

    public String getSelfRole() {
        return mSelfRole;
    }

    public Activity getActivity() {
        return mActivity;
    }

    public void travelRoomMembers(Action<WereWolfGameMember> action) {
        travelSparseArray(mRoomMembers, action);
    }

    public void travelRoomObserverMembers(Action<WereWolfObserverMember> action) {
        travelSparseArray(mRoomObserverMembers, action);
    }

    public <T> void travelSparseArray(SparseArray<T> sparseArray, Action<T> action) {
        if (sparseArray == null) {
            return;
        }
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            int key = sparseArray.keyAt(i);
            T t = sparseArray.get(key);
            if (t != null && action != null) {
                action.func(t);
            }
        }
    }

    public boolean isOverMaxPosition(int position) {
        return position > MAX_POSITIONS;
    }

    public WereWolfGameMember getGameMember(int position) {
        if (position > MAX_POSITIONS) {
            return null;
        }
        WereWolfGameMember wereWolfGameMember = mRoomMembers.get(position);
        return wereWolfGameMember;
    }

    public WereWolfObserverMember getObserverMember(int position) {
        WereWolfObserverMember wereWolfObserverMember = mRoomObserverMembers.get(position);
        return wereWolfObserverMember;
    }

    //wwq 2018-02-11根据id找观战者
    public WereWolfObserverMember getObserverMember(String id) {
        if (mRoomObserverMembers == null || mRoomObserverMembers.size() == 0) {
            return null;
        }
        for (int i = 0; i < mRoomObserverMembers.size(); i++) {
            int key = mRoomObserverMembers.keyAt(i);
            WereWolfObserverMember member = mRoomObserverMembers.get(key);
            if (member != null) {
                if (TextUtils.equals(member.memberId, id)) {
                    return member;
                }
            }
        }

        return null;
    }

    public void bindDestroyFatherAction(Action<Boolean> runnable) {
        mDestroyFatherAction = runnable;
    }

    public boolean positionIsLocked(int position) {
        return mRoomLockedPosition.contains(position - 1);
    }

    public int getWereWolfMessageMaxCount() {
        return mWereWolfMessageMaxCount;
    }

    public int getmWereWolfPlayerMessageMaxCount() {
        return mWereWolfPlayerMessageMaxCount;
    }

    public int getmWereWolfObserverMessageMaxCount() {
        return mWereWolfObserverMessageMaxCount;
    }

    public int getSitedPositionsSize() {
        return mRoomMembers.size();
    }

    public int getObserverMembersCount() {
        return mRoomObserverMembers.size();
    }

    public List<Integer> getLockPositions() {
        Collections.sort(mRoomLockedPosition);
        return mRoomLockedPosition;
    }

    public int getUnLockPositionsSize() {
        return MAX_POSITIONS - getLockPositionsSize() - getSitedPositionsSize();
    }

    public List<Integer> getUnLockPosition() {
        List<Integer> result = new ArrayList<>();
        for (int i = 1; i <= MAX_POSITIONS; i++) {
            result.add(new Integer(i));
        }

        /**
         * remove locked
         */
        for (Integer i : mRoomLockedPosition) {
            result.remove(new Integer(i + 1));
        }

        /**
         * remove unlocked
         */
        int sitSize = getSitedPositionsSize();
        for (int i = 0; i < sitSize; i++) {
            int key = mRoomMembers.keyAt(i);
            WereWolfGameMember member = mRoomMembers.get(key);
            if (member != null) {
                result.remove(new Integer(member.memberPosition));
            }
        }
        return result;
    }

    public int getPositionByUserId(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return -1;
        }
        final int[] result = {-1};
        travelRoomMembers(value -> {
            if (TextUtils.equals(value.memberId, userId)) {
                result[0] = value.memberPosition;
            }
        });
        return result[0];
    }

    public int getLockPositionsSize() {
        return mRoomLockedPosition.size();
    }

    public List<WereWolfGameMember> getAllGameMember() {
        List<WereWolfGameMember> result = new ArrayList<>();
        travelRoomMembers(value -> {
            result.add(value);
        });
        return result;
    }

    public List<Integer> getAllGameMemberIndex() {
        List<Integer> result = new ArrayList<>();
        travelRoomMembers(value -> {
            result.add(value.memberPosition);
        });
        return result;
    }

    //wwq 2018-02-11获取观战列表
    public List<WereWolfObserverMember> getAllObserverMember() {
        List<WereWolfObserverMember> result = new ArrayList<>();
        travelRoomObserverMembers(value -> {
            result.add(value);
        });
        return result;
    }

    public List<WereWolfSelectMember> getSelectMemberForMagician(List<Integer> alives, List<Integer> lastExchange) {
        List<WereWolfSelectMember> result = new ArrayList<>();
        if (alives == null || alives.size() == 0) {
            return result;
        }
        travelRoomMembers(value -> {
            if (alives.contains(value.memberPosition - 1)) {
                /**
                 * alives 包含这个人
                 */
                WereWolfSelectMember wolfSelectMember = gameMemberTurnIntoSelectMember(value);
                if (lastExchange.contains(value.memberPosition - 1)) {
                    wolfSelectMember.lastProtectPosition = value.memberPosition;
                }
                result.add(wolfSelectMember);
            }
        });
        return result;
    }

    public List<WereWolfSelectMember> getSelectMemberForProtector(List<Integer> alives, int lastProtectPosition) {
        List<WereWolfSelectMember> result = new ArrayList<>();
        int size = mRoomMembers.size();
        for (int i = 0; i < size; i++) {
            int key = mRoomMembers.keyAt(i);
            WereWolfGameMember member = mRoomMembers.get(key);
            if (member != null) {
                for (Integer temp : alives) {
                    if (temp + 1 == member.memberPosition) {
                        WereWolfSelectMember wolfSelectMember = gameMemberTurnIntoSelectMember(member, lastProtectPosition);
                        result.add(wolfSelectMember);
                        break;
                    }
                }
            }
        }
        return result;
    }

    public List<WereWolfSelectMember> getSelectMemberAll() {
        List<WereWolfSelectMember> result = new ArrayList<>();
        int size = mRoomMembers.size();
        for (int i = 0; i < size; i++) {
            int key = mRoomMembers.keyAt(i);
            WereWolfGameMember member = mRoomMembers.get(key);
            if (member != null) {
                WereWolfSelectMember wolfSelectMember = gameMemberTurnIntoSelectMember(member);
                result.add(wolfSelectMember);
            }
        }
        return result;
    }

    public List<WereWolfSelectMember> getSelectMemberWithOutSelf(List<Integer> alives) {
        List<WereWolfSelectMember> result = new ArrayList<>();
        int size = mRoomMembers.size();
        for (int i = 0; i < size; i++) {
            int key = mRoomMembers.keyAt(i);
            WereWolfGameMember member = mRoomMembers.get(key);
            if (member != null && !(member.memberPosition == getPersonalPosition())) {
                for (Integer temp : alives) {
                    if (temp + 1 == member.memberPosition) {
                        WereWolfSelectMember wolfSelectMember = gameMemberTurnIntoSelectMember(member);
                        result.add(wolfSelectMember);
                        break;
                    }
                }
            }
        }
        return result;
    }

    public List<WereWolfSelectMember> getSelectMember(List<Integer> alives) {
        List<WereWolfSelectMember> result = new ArrayList<>();
        int size = mRoomMembers.size();
        for (int i = 0; i < size; i++) {
            int key = mRoomMembers.keyAt(i);
            WereWolfGameMember member = mRoomMembers.get(key);
            if (member != null) {
                for (Integer temp : alives) {
                    if (temp + 1 == member.memberPosition) {
                        WereWolfSelectMember wolfSelectMember = gameMemberTurnIntoSelectMember(member);
                        result.add(wolfSelectMember);
                        break;
                    }
                }
            }
        }
        return result;
    }

    public List<WereWolfKillMember> getWolfKillMemberRealPosition(List<Integer> alives) {
        List<WereWolfKillMember> members = new ArrayList<>();
        travelRoomMembers(value -> {
            if (alives.contains(value.memberPosition)) {
                WereWolfKillMember member = new WereWolfKillMember();
                member.memberSelect = false;
                member.mKillMap = null;
                member.memberPosition = value.memberPosition;
                member.memberName = value.memberName;
                member.memberImage = value.memberHead;
                member.memberRole = value.memberRole;
                member.isLover = value.isLover;
                member.isSheriff = value.isSheriff;
                members.add(member);
            }
        });
        return members;
    }

    public List<WereWolfKillMember> getWolfKillMember(List<Integer> alives) {
        List<WereWolfKillMember> members = new ArrayList<>();
        travelRoomMembers(value -> {
            if (alives.contains(value.memberPosition - 1)) {
                WereWolfKillMember member = new WereWolfKillMember();
                member.memberSelect = false;
                member.mKillMap = null;
                member.memberPosition = value.memberPosition;
                member.memberName = value.memberName;
                member.memberImage = value.memberHead;
                member.memberRole = value.memberRole;
                member.isLover = value.isLover;
                member.isSheriff = value.isSheriff;
                members.add(member);
            }
        });
        return members;
    }

    public WereWolfSelectMember getSelectMember(int position) {
        WereWolfGameMember member = mRoomMembers.get(position);
        if (member != null) {
            return gameMemberTurnIntoSelectMember(member);
        } else {
            return null;
        }
    }

    private WereWolfSelectMember gameMemberTurnIntoSelectMember(WereWolfGameMember member) {
        return gameMemberTurnIntoSelectMember(member, -1);
    }

    private WereWolfSelectMember gameMemberTurnIntoSelectMember(WereWolfGameMember member, int lastProtect) {
        WereWolfSelectMember wolfSelectMember = new WereWolfSelectMember();
        wolfSelectMember.lastProtectPosition = lastProtect;
        wolfSelectMember.myselfPosition = mPersonalPosition;
        wolfSelectMember.setSelect = false;
        wolfSelectMember.memberPosition = member.memberPosition;
        wolfSelectMember.memberImage = member.memberHead;
        wolfSelectMember.memberName = member.memberName;
        wolfSelectMember.isLover = member.isLover;
        wolfSelectMember.isSheriff = member.isSheriff;
//        wolfSelectMember.isWolf = member.isWolf;
//        wolfSelectMember.isWolfKing = member.isWolfKing;
//        wolfSelectMember.isDemon = member.isDemon;
        return wolfSelectMember;
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (isDestroy)
            return;
        isDestroy = true;

//        mOrbitEngine = null;

        if (mBackGroundMusicManager != null) {
            mBackGroundMusicManager.destroy();
            mBackGroundMusicManager = null;
        }
        mRoomMembers.clear();
        mRoomObserverMembers.clear();
        /**
         * 此处必须clear mProcessLifeCycleManager
         */
        mProcessLifeCycleManager.clear();
        mProcessManager.clear();

        /**
         * 清理服务
         */
        clearService();

//        mGameRootView.removeAllViews();
        mGameRootView = null;
        mSafeHandler.release();
        mContext = null;
        mActivity = null;
        mEnterRoomResult = null;
        mDestroyFatherAction = null;
    }

    private void addRoomObserverMembers(int position, WereWolfObserverMember member) {
        mRoomObserverMembers.put(position, member);
        Log.i(TAG, "addRoomObserverMembers: after add size is " + mRoomObserverMembers.size());
    }

    private void addRoomMembers(int position, WereWolfGameMember member) {
        mRoomMembers.put(position, member);
        Log.i(TAG, "addRoomMembers: after add size is " + mRoomMembers.size());
    }

    private WereWolfObserverMember removeObserverMember(int position) {
        WereWolfObserverMember member = mRoomObserverMembers.get(position);
        mRoomObserverMembers.remove(position);

        return member;
    }

    private WereWolfGameMember removeRoomMembers(int position) {
        WereWolfGameMember member = mRoomMembers.get(position);
        mRoomMembers.remove(position);
        Log.i(TAG, "removeRoomMembers: after remove size is " + mRoomMembers.size());

        return member;
    }

    private void updateRoomPassword(String password) {
        try {
            if (TextUtils.isEmpty(password)) {
                password = "";
            }
            PersonalDataHelper.getPersonalData().setCurPassword(password);
            GlobalUserState.getGlobalState().setPassword(password);
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * just in case
             */
        }
    }

    @Override
    public void func(ServerMessageEvent value) {
        mSafeHandler.postSafely(() -> {
            try {
                if (!value.isWereWolf()) {
                    return;
                }
                String type = value.type;
                JSONObject payload = value.payload;
                long t = System.currentTimeMillis();

                processStart(type, payload);

                switch (type) {
                    case Constant.TYPE_JOIN: {
                        ServerMessageEnterResult item = GsonHelper.getGson().fromJson(payload.toString(), ServerMessageEnterResult.class);
                        WereWolfGameMember member = WereWolfGameMember.FromEnterRoomResult(item.user);
                        addRoomMembers(member.memberPosition, member);
                        handleJoin(member, payload);
                    }
                    break;
                    case WereWolfConstant.OBSERVER_JOIN: {
                        ServerMessageEnterResult item = GsonHelper.getGson().fromJson(payload.toString(), ServerMessageEnterResult.class);
                        WereWolfObserverMember member = WereWolfObserverMember.TurnIntoWereWolfObserverMember(item.user);
                        addRoomObserverMembers(member.memberPosition, member);
                        handleObserverJoin(member, payload);
                    }
                    break;
                    case WereWolfConstant.OBSERVER_LEAVE: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        WereWolfObserverMember member = removeObserverMember(position);
                        handleObserverLeave(member, payload);
                    }
                    break;
                    case Constant.UPDATE_CONFIG: {
                        if (payload.has("config")) {
                            mRoomConfig = GsonHelper.getGson().fromJson(payload.optJSONObject("config").toString(), EnterRoomResult.RoomConfig.class);
                            mPassword = mRoomConfig.room_info.password;
                            updateRoomPassword(mPassword);
                            handleUpdateConfig(mRoomConfig);
                        }
                    }
                    break;
                    case Constant.TYPE_ADD_FRIEND: {
                        String userId = payload.optString(Constant.USER_ID_LOWER);
                        String name = payload.optString(Constant.NAME_LOWER);
                        String avatar = payload.optString(Constant.AVATAR_LOWER);
                        handleAddFriend(userId, name, avatar, payload);
                    }
                    break;
                    case Constant.LOCK: {
                        LockResult lockResult = GsonHelper.getGson().fromJson(payload.toString(), LockResult.class);
                        if (lockResult != null && lockResult.locked_positions != null && lockResult.locked_positions.size() > 0) {
                            mRoomLockedPosition = lockResult.locked_positions;
                            handleLock(lockResult.locked_positions, payload);
                        }
                    }
                    break;
                    case Constant.LEAVE: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        WereWolfGameMember member = removeRoomMembers(position);
                        handleLeave(position, member, payload);
                    }
                    break;
                    case Constant.REQUEST_RESET_MASTER: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleResetMaster(dur, payload);
                    }
                    break;
                    case Constant.REJECT_RESET_MASTER: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleRejectResetMaster(dur, payload);
                    }
                    break;
                    case Constant.CHANGE_PASSWORD: {
                        mPassword = payload.optString(Constant.PASSWORD);
                        updateRoomPassword(mPassword);
                        handleUpdatePassword(mPassword, payload);
                    }
                    break;
                    case Constant.TYPE_CHAT: {
                        JSONObject gift = payload.optJSONObject(Constant.GIFT_CHAT);
                        String message = payload.optString(Constant.MESSAGE);
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (gift != null) {
                            int from = gift.optInt(Constant.GIFT_FROM) + 1;
                            int to = gift.optInt(Constant.GIFT_TO) + 1;
                            String giftType = gift.optString(Constant.GIFT_TYPE);
                            handleGift(giftType, message, from, to, payload);
                        } else {
                            handleChat(position, message, payload);
                        }
                    }
                    break;
                    case Constant.WAKE_TO_KILL: {
                        WakeToKillResult result = GsonHelper.getGson().fromJson(payload.toString(), WakeToKillResult.class);
                        handleWakeToKill(result);
                    }
                    break;
                    case Constant.SYSTEM_MSG: {
                        String msgType = "";
                        if (payload.has(Constant.TYPE)) {
                            msgType = payload.optString(Constant.TYPE);
                        }
                        String msgContent = payload.optString(Constant.CONTENT);
                        handleSystemMessage(msgType, msgContent, payload);
                    }
                    break;
                    case Constant.EXPORT: {
                        mCurCallTitle = payload.optString(Constant.TITLE);
                        mCurCallType = payload.optString(Constant.TYPE);
                        handleExport(mCurCallType, mCurCallTitle, payload);
                    }
                    break;
                    case Constant.UNACTIVE_WARNING: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleUnActiveWarning(dur, payload);
                    }
                    break;
                    case Constant.KICK_OUT: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        WereWolfGameMember member = removeRoomMembers(position);
                        if (position == mPersonalPosition) {
                            Toast.makeText(mContext, R.string.you_have_been_kick_out, Toast.LENGTH_LONG).show();
                            if (mDestroyFatherAction != null) {
                                mDestroyFatherAction.func(false);
                            }
                        }
                        handleKickOut(position, member, payload);
                    }
                    break;
                    case Constant.SPEAK: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (getGameMember(position) != null) {
                            getGameMember(position).isSpeaking = true;
                        }
                        handleSpeak(position, payload);
                    }
                    break;
                    case Constant.UNSPEAK: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (getGameMember(position) != null) {
                            getGameMember(position).isSpeaking = false;
                        }
                        handleUnSpeak(position, payload);
                    }
                    break;
                    case Constant.PREPARE: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (mPersonalPosition == position) {
                            isPrepare = true;
                        }
                        if (getGameMember(position) != null) {
                            getGameMember(position).isPrepare = true;
                        }
                        handlePrepare(position, payload);
                    }
                    break;
                    case Constant.UNPREPARE: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (mPersonalPosition == position) {
                            isPrepare = false;
                        }
                        if (getGameMember(position) != null) {
                            getGameMember(position).isPrepare = false;
                        }
                        handleUnPrepare(position, payload);
                    }
                    break;
                    case Constant.TYPE_UPDATE_MASTER: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        travelRoomMembers(value19 -> {
                            WereWolfGameMember member = getGameMember(value19.memberPosition);
                            if (member != null) {
                                if (position == member.memberPosition) {
                                    member.isRoomMaster = true;
                                } else {
                                    member.isRoomMaster = false;
                                }
                            }
                        });
                        if (position == mPersonalPosition) {
                            isRoomOwner = true;
                        } else {
                            isRoomOwner = false;
                        }
                        handleUpdateMaster(position, payload);
                    }
                    break;
                    case Constant.START: {
                        mSelfRole = "";
                        mCurSpeechPosition = -1;
                        isGaming = true;
                        isPrepare = false;
                        isDead = false;
                        travelRoomMembers(value110 -> {
                            value110.clearWhenGameOver();
                        });
                        StartResult result = GsonHelper.getGson().fromJson(payload.toString(), StartResult.class);
                        mBackGroundMusicManager.handleStart(result);
                        handleStart(result, payload);
                        RxToolKit.Emit(new ViewEvent.SpeakViewEvent(MotionEvent.ACTION_UP));
                    }
                    break;
                    case Constant.BOOM_STATE: {
                        boolean boomEnable = payload.optBoolean(Constant.BOOM_ABLE);
                        handleBoomState(boomEnable, payload);
                    }
                    break;
                    case Constant.ASSIGNED_ROLE: {
                        AssginedRoleResult result = GsonHelper.getGson().fromJson(payload.toString(), AssginedRoleResult.class);
                        {
                            if (result.teammates != null && result.teammates.size() > 0) {
//                                for (Integer integer : result.teammates) {
//                                    getGameMember(integer + 1).isWolf = true;
//                                }
                            }

                            if (result.teammate_roles != null && result.teammate_roles.size() > 0) {
                                ToolKit.travelMap(result.teammate_roles, (key, value111) -> {
                                    WereWolfGameMember member = getGameMember(key + 1);
                                    if (member != null) {
                                        member.memberRole = value111;
                                    }
                                });
                            }

//                            if (result.king >= 0) {
//                                int king = result.king + 1;
//                                getGameMember(king).isWolfKing = true;
//                            }
//
//                            if (result.demon >= 0) {
//                                int demon = result.demon + 1;
//                                getGameMember(demon).isDemon = true;
//                            }

                        }
                        mSelfRole = result.role;
                        getGameMember(mPersonalPosition).memberRole = mSelfRole;
                        mBackGroundMusicManager.handleAssignedRole();
                        handleAssginedRole(result, payload);
                    }
                    break;
                    case Constant.SUNSET: {
                        isSunSet = true;
                        mCurSpeechPosition = -1;
                        int dayindex = payload.optInt(Constant.DAYINDEX);
                        int duration = RoomSocketEngineHelper.getOptDurTime(payload);
                        mBackGroundMusicManager.handleSunSet();
                        handleSunSet(dayindex, duration, payload);

                        orbitEngineAble(false);
                    }
                    break;
                    case Constant.CHECK: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handleSeerCheck(info, payload);
                    }
                    break;
                    case Constant.PROTECT: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        int lastProtected = -1;
                        if (payload.has(Constant.LAST_PROTECTED)) {
                            lastProtected = payload.optInt(Constant.LAST_PROTECTED) + 1;
                        }
                        handleProtect(info, lastProtected, payload);
                    }
                    break;
                    case Constant.SAVE: {
                        int position = payload.optInt(Constant.POSITION) + 1;
                        boolean can_Save = payload.optBoolean(Constant.CAN_SAVE);
                        handleSave(position, can_Save, payload);
                    }
                    break;
                    case Constant.POISON: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handlePoison(info, payload);
                    }
                    break;
                    case Constant.SUNUP: {
                        isSunSet = false;
                        int dayIndex = payload.optInt(Constant.DAYINDEX);
                        mBackGroundMusicManager.handleSunUp();
                        handleSunUp(dayIndex, payload);

                        orbitEngineAble(true);
                    }
                    break;
                    case Constant.DEATHINFO: {
                        DeathInfo deathInfo = GsonHelper.getGson().fromJson(payload.toString(), DeathInfo.class);
                        {
                            if (deathInfo != null && deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
                                ToolKit.travelCollection(deathInfo.death_info, infoItem -> {

                                    int position = infoItem.killed + 1;

                                    if (position == mPersonalPosition) {
                                        isDead = true;
                                    }

                                    WereWolfGameMember member = getGameMember(position);
                                    if (member != null) {
                                        member.isDead = true;
                                        member.memberRole = infoItem.role;
                                    }
                                });
                            }
                        }
                        handleDeathInfo(deathInfo);
                    }
                    break;
                    case Constant.VOTE: {
                        mCurSpeechPosition = -1;
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        mBackGroundMusicManager.handleVote();
                        handleVote(dur, info, payload);
                    }
                    break;
                    case Constant.VOTE_RESULT: {
                        VoteResultInfo info = GsonHelper.getGson().fromJson(payload.toString(), VoteResultInfo.class);
                        if (TextUtils.equals(info.type, Constant.TYPE_VOTE_RESULT_DEATH)) {
                            /**
                             * 投票致死
                             */
                            if (info != null && info.death_info != null && info.death_info.size() > 0) {
                                ToolKit.travelCollection(info.death_info, infoItem -> {

                                    int position = infoItem.killed + 1;

                                    if (position == mPersonalPosition) {
                                        isDead = true;
                                    }

                                    WereWolfGameMember member = getGameMember(position);
                                    if (member != null) {
                                        member.isDead = true;
                                        member.memberRole = infoItem.role;
                                    }
                                });
                            }
                        } else if (TextUtils.equals(info.type, Constant.VOTE_TYPE_APPLY)) {
                            /**
                             * 警长竞选
                             */
                            if (info.need_pk) {
                                List<Integer> positions = info.positions;
                                if (positions != null && positions.size() > 0) {
                                    travelRoomMembers(value16 -> {
                                        if (positions.contains(value16.memberPosition - 1)) {
                                            value16.isHandup = true;
                                        } else {
                                            value16.isHandup = false;
                                        }
                                    });
                                }
                            } else {
                                List<Integer> positions = info.positions;
                                if (positions != null && positions.size() > 0) {
                                    travelRoomMembers(value16 -> {
                                        if (positions.contains(value16.memberPosition - 1)) {
                                            value16.isSheriff = true;
                                        } else {
                                            value16.isSheriff = false;
                                        }
                                    });
                                }
                            }
                        }
                        handleVoteResult(info);
                    }
                    break;
                    case Constant.CHECKRESULT: {
                        CheckResult checkResult = GsonHelper.getGson().fromJson(payload.toString(), CheckResult.class);
                        if (checkResult != null) {
                            checkResult.position++;
                        }
                        handleCheckResult(checkResult);
                    }
                    break;
                    case Constant.KILL_RESULT: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        handleKillResult(position, payload);
                    }
                    break;
                    case Constant.TAKEAWAY: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handleTakeAway(info, payload);
                    }
                    break;
                    case Constant.TAKE_AWAY_RESULT: {
                        DeathInfo deathInfo = GsonHelper.getGson().fromJson(payload.toString(), DeathInfo.class);
                        {
                            if (deathInfo != null && deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
                                ToolKit.travelCollection(deathInfo.death_info, infoItem -> {

                                    int position = infoItem.killed + 1;

                                    if (position == mPersonalPosition) {
                                        if (infoItem.dead) {
                                            isDead = true;
                                        } else {
                                            if (infoItem.real_dead != -1 && (infoItem.real_dead + 1) == mPersonalPosition) {
                                                isDead = true;
                                            }
                                        }
                                    }

                                    WereWolfGameMember member = getGameMember(position);
                                    if (member != null) {
                                        if (infoItem.dead) {
                                            member.isDead = true;
                                        } else {
                                            if (infoItem.real_dead != -1) {
                                                WereWolfGameMember temp = getGameMember(infoItem.real_dead + 1);
                                                temp.isDead = true;
                                            }
                                        }
                                        member.memberRole = infoItem.role;
                                    }
                                });
                            }
                        }
                        handleTakeAwayResult(deathInfo);
                    }
                    break;
                    case Constant.SHERIFF_RESULT: {
                        int position = -1;

                        if (payload.has(Constant.POSITION)) {
                            position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        }
                        int seconds = RoomSocketEngineHelper.getOptDurTime(payload);
                        if (position > 0) {
                            WereWolfGameMember member = getGameMember(position);
                            member.isSheriff = true;
                            member.isHandup = false;

                        }
                        /**
                         * do some state change.... then dispatch this event
                         */
                        handleSheriffResult(position, seconds, payload);
                    }
                    break;
                    case Constant.HAND_OVER: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handleHandOver(info);
                    }
                    break;
                    case Constant.HAND_OVER_RESULT: {
                        int from = -1;
                        if (payload.has(Constant.FROM)) {
                            from = payload.optInt(Constant.FROM) + 1;
                            WereWolfGameMember gameMember = getGameMember(from);
                            if (gameMember != null) {
                                gameMember.isSheriff = false;
                            }
                        }

                        int to = -1;
                        if (payload.has(Constant.TO)) {
                            to = payload.optInt(Constant.TO) + 1;
                            WereWolfGameMember gameMember = getGameMember(to);
                            if (gameMember != null) {
                                gameMember.isSheriff = true;
                            }
                        }
                        handleHandOverResult(from, to, payload);
                    }
                    break;
                    case Constant.LINK: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        if (mBackGroundMusicManager != null) {
                            mBackGroundMusicManager.handleLink();
                        }
                        handleLink(dur, payload);
                    }
                    break;
                    case Constant.LINK_RESULT: {
                        LinkResult linkResult = null;
                        if (payload.has("lovers")) {
                            linkResult = GsonHelper.getGson().fromJson(payload.toString(), LinkResult.class);
                            ToolKit.travelMap(linkResult.lovers, (key, value12) -> {
                                int position = key + 1;
                                WereWolfGameMember member = getGameMember(position);
                                if (member != null) {
                                    member.isLover = true;
                                    if (value12) {
                                        //member.isWolf = true;
                                        member.memberRole = Constant.ROLE_WOLF;
                                    }
                                }
                            });
                        }
                        handleLinkResult(linkResult);
                    }
                    break;
                    case Constant.APPLY: {
                        int second = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleApply(second, payload);
                    }
                    break;
                    case Constant.APPLY_RESULT: {
                        ApplyResult applyResult = null;
                        if (payload.has(Constant.POSITIONS)) {
                            applyResult = GsonHelper.getGson().fromJson(payload.toString(), ApplyResult.class);
                            ApplyResult finalApplyResult = applyResult;
                            travelRoomMembers(value17 -> {
                                if (finalApplyResult.positions.contains(value17.memberPosition - 1)) {
                                    value17.isHandup = true;
                                } else {
                                    value17.isHandup = false;
                                }
                            });
                        }
                        handleApplyResult(applyResult);
                    }
                    break;
                    case Constant.GIVE_UP_APPLY: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        WereWolfGameMember member = getGameMember(position);
                        if (member != null) {
                            member.isHandup = false;
                        }
                        handleGiveUpApply(position, payload);
                    }
                    break;
                    case Constant.CARD_CHECK_RESULT: {
                        String userCardResultType = payload.optString(Constant.TYPE);
                        handleCheckCardResult(userCardResultType, payload);
                    }
                    break;
                    case Constant.APPEND_TIME_RESULT: {
                        String userCardResultType = payload.optString(Constant.TYPE);
                        handleAppendTimeResult(userCardResultType, payload);
                    }
                    break;
                    case Constant.APPLY_ROLE_RESULT: {
                        String userCardResultType = payload.optString(Constant.TYPE);
                        handleRobCardResult(userCardResultType, payload);
                    }
                    break;
                    case Constant.APPEND_TIME: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        int append_duration = payload.optInt(Constant.APPEND_DURATION) / 1000;
                        int append_time = payload.optInt(Constant.LEFT_TIME) / 1000;
                        handleAppendTime(position, append_duration, append_time, payload);
                    }
                    break;
                    case Constant.PROTECT_RESULT: {
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        if (position > 0) {
                            travelRoomMembers(value18 -> {
                                value18.memberLastProtectPosition = position;
                            });
                        }
                        handleProtectResult(position, payload);
                    }
                    break;
                    case Constant.BOOM: {
                        mCurSpeechPosition = -1;
                        int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        boolean iswolfking = payload.optBoolean("is_werewolf_king");
                        int duration = RoomSocketEngineHelper.getOptDurTime(payload);
                        if (position == mPersonalPosition) {
                            isDead = true;
                        }
                        WereWolfGameMember member = getGameMember(position);
                        if (member != null) {
                            member.isDead = true;
                        }
                        mBackGroundMusicManager.handleBoom();
                        handleBoom(position, iswolfking, duration, payload);
                    }
                    break;
                    case Constant.BOOM_AWAY: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handleBoomAway(info);
                    }
                    break;
                    case Constant.BOOM_AWAY_RESULT: {
                        DeathInfo deathInfo = GsonHelper.getGson().fromJson(payload.toString(), DeathInfo.class);
                        if (deathInfo != null && deathInfo.death_info != null && deathInfo.death_info.size() > 0) {
                            ToolKit.travelCollection(deathInfo.death_info, infoItem -> {

                                int position = infoItem.killed + 1;

                                if (position == mPersonalPosition) {
                                    isDead = true;
                                }

                                WereWolfGameMember member = getGameMember(position);
                                if (member != null) {
                                    member.isDead = true;
                                    member.memberRole = infoItem.role;
                                }
                            });
                        }
                        handleBoomAwayResult(deathInfo);
                    }
                    break;
                    case Constant.SAVE_RESULT: {
                        int position = -1;
                        if (payload.has(Constant.POSITION)) {
                            position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        }
                        handleSaveResult(position, payload);
                    }
                    break;
                    case Constant.POISON_RESULT: {
                        int position = -1;
                        if (payload.has(Constant.POSITION)) {
                            position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                        }
                        handlePoisonResult(position, payload);
                    }
                    break;
                    case Constant.SPEECH: {
                        WereWolfSpeechResult result = GsonHelper.getGson().fromJson(payload.toString(), WereWolfSpeechResult.class);
                        int dur = result.duration / 1000;
                        boolean isDead = result.dead;
                        int position = result.position + 1;
                        mCurSpeechPosition = position;
                        String speechType = result.type;
                        List<Integer> lefts = result.lefts;
                        if (lefts == null) {
                            lefts = new ArrayList<>();
                        }
                        handleSpeech(position, dur, isDead, speechType, lefts, payload);
                    }
                    break;
                    case Constant.SPEECH_DIRECTION: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        int left = -1;
                        if (payload.has(Constant.LEFT)) {
                            left = payload.optInt(Constant.LEFT) + 1;
                        }
                        int right = -1;
                        if (payload.has(Constant.RIGHT)) {
                            right = payload.optInt(Constant.RIGHT) + 1;
                        }
                        handleSpeechDirection(dur, left, right, payload);
                    }
                    break;
                    case Constant.SPEECH_DIRECTION_RESULT: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleSpeechDirectionResult(dur, payload);
                    }
                    break;
                    case Constant.MUTE_ALL: {
                        int position = payload.optInt(Constant.POSITION) + 1;
                        handleMuteAll(position, payload);
                    }
                    break;
                    case Constant.UNMUTE_ALL: {
                        int position = payload.optInt(Constant.POSITION) + 1;
                        handleUnMuteAll(position, payload);
                    }
                    break;
                    case Constant.RESTORE_ROOM: {
                        Log.i(TAG, "RESTORE_ROOM func: ");
                        RestoreResult result = GsonHelper.getGson().fromJson(payload.toString(), RestoreResult.class);
                        try {
                            mBackGroundMusicManager.handleRestoreRoom(result);
                            {
                                /**
                                 * 处理房间基本状态的恢复
                                 */
                                mRoomConfig = result.room_info.config;
                                mRoomId = result.room_info.room_id;
                                mPassword = result.room_info.password;
                                updateRoomPassword(mPassword);
                                isGaming = result.room_info.isPlaying;
                                isCanCutSpeak = result.room_info.can_cut_speaker;

                                /**
                                 * 清理所有的游戏数据，等待恢复
                                 */
                                travelRoomMembers(value14 -> {
                                    value14.clearGameData();
                                    value14.clearStateData();
                                });

                                /**
                                 * 召集房间的数据恢复
                                 */
                                mCurCallTitle = result.room_info.title;
                                mCurCallType = result.room_info.export_type;
                                /**
                                 * 锁住的位置数据恢复
                                 */
                                if (result.room_info.locked_positions != null) {
                                    mRoomLockedPosition = result.room_info.locked_positions;
                                }

                                /**
                                 * 处理房间人员的基本信息
                                 */
                                Map<Integer, EnterRoomResult.EnterRoomUserItem> enterRoomUserItemMap = result.room_info.users;
                                SparseArray<WereWolfGameMember> mTempNewGameMembers = new SparseArray<>();
                                travelRoomMembers(curGameMember -> {
                                    int position = curGameMember.memberPosition;
                                    EnterRoomResult.EnterRoomUserItem item = enterRoomUserItemMap.get(position - 1);
                                    if (item != null) {
                                        /**
                                         * 这里更新一下自己的位置
                                         */
                                        if (TextUtils.equals(item.id, GlobalUserState.getGlobalState().getUserId())) {
                                            mPersonalPosition = item.position + 1;
                                            isPrepare = item.prepared;
                                            isObserver = false;
                                            isRoomOwner = item.is_master;
                                        }
                                        /**
                                         * 代表这里有人
                                         */
                                        if (TextUtils.equals(item.id, curGameMember.memberId)) {
                                            /**
                                             * 是同一个人
                                             */
                                            curGameMember.isRoomMaster = item.is_master;
                                            curGameMember.isPrepare = item.prepared;
                                            curGameMember.isSpeaking = item.speaking;
                                            mTempNewGameMembers.put(position, curGameMember);

                                        } else {
                                            /**
                                             * 不是同一个人，又在同一个位置上，那么就需要更新一下这个数据
                                             */
                                            handleLeave(position, curGameMember, new JSONObject());

                                            /**
                                             * 重新添加一个新的人
                                             */
                                            WereWolfGameMember tempMember = WereWolfGameMember.FromEnterRoomResult(item);
                                            mTempNewGameMembers.put(position, tempMember);

                                            /**
                                             * 通知添加人员消息
                                             */
                                            handleJoin(tempMember, new JSONObject());
                                        }

                                    } else {
                                        /**
                                         * 代表这里已经没有人了
                                         */
                                        handleLeave(position, curGameMember, new JSONObject());
                                    }
                                });

                                //wwq 2018-03-14 修改遍历集合的方式======//
                                mTempNewGameMembers.clear();
                                Iterator<Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem>> iterator = enterRoomUserItemMap.entrySet().iterator();
                                while (iterator.hasNext()) {
                                    Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem> next = iterator.next();
                                    int position = next.getKey() + 1;
                                    EnterRoomResult.EnterRoomUserItem item = next.getValue();
                                    mTempNewGameMembers.put(position, WereWolfGameMember.FromEnterRoomResult(item));
                                }
                                //wwq 2018-03-14 修改遍历集合的方式======//

                                mRoomMembers.clear();
                                mRoomMembers = mTempNewGameMembers;

                                /**
                                 * 处理观战人员的基本信息
                                 */
                                try {
                                    Map<Integer, EnterRoomResult.EnterRoomUserItem> enterObserverRoomUserItemMap = result.room_info.observers;
                                    SparseArray<WereWolfObserverMember> mTempNewObserverMembers = new SparseArray<>();
                                    Iterator iterator2 = enterObserverRoomUserItemMap.entrySet().iterator();
                                    while (iterator2.hasNext()) {
                                        Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem> temp = (Map.Entry<Integer, EnterRoomResult.EnterRoomUserItem>) iterator2.next();
                                        int memberPosition = temp.getKey() + 1;
                                        EnterRoomResult.EnterRoomUserItem memberData = temp.getValue();
                                        /**
                                         * 在观战列表中寻找自己,代表自己是处于观战模式
                                         */
                                        if (TextUtils.equals(memberData.id, GlobalUserState.getGlobalState().getUserId())) {
                                            mPersonalPosition = memberData.position + 1;
                                            isObserver = true;
                                        }
                                        mTempNewObserverMembers.put(memberPosition, WereWolfObserverMember.TurnIntoWereWolfObserverMember(memberData));
                                    }

                                    mRoomObserverMembers.clear();
                                    mRoomObserverMembers = mTempNewObserverMembers;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                /**
                                 * 在这里之后所有的人员已经更换完毕，这里需要恢复每个人员对应的游戏状态
                                 */
                                if (result.game_info != null) {

                                    RestoreResult.RestoreRoleMessage gameInfo = result.game_info;

                                    mSelfRole = gameInfo.role;

                                    WereWolfGameMember tempMember = getGameMember(mPersonalPosition);
                                    if (tempMember != null) {
                                        tempMember.memberRole = mSelfRole;
                                    }

                                    /**
                                     * 恢复警长数据
                                     */
                                    if (gameInfo.sheriff >= 0) {
                                        int position = gameInfo.sheriff + 1;
                                        travelRoomMembers(value1 -> {
                                            if (value1.memberPosition == position) {
                                                value1.isSheriff = true;
                                            } else {
                                                value1.isSheriff = false;
                                            }
                                        });
                                    }
                                    /**
                                     * 恢复举手数据
                                     */
                                    if (gameInfo.apply != null && gameInfo.apply.size() > 0) {
                                        travelRoomMembers(value15 -> {
                                            if (gameInfo.apply.contains(value15.memberPosition - 1)) {
                                                value15.isHandup = true;
                                            } else {
                                                value15.isHandup = false;
                                            }
                                        });
                                    } else {
                                        travelRoomMembers(value16 -> {
                                            value16.isHandup = false;
                                        });
                                    }
                                    /**
                                     * 恢复狼人数据
                                     */
                                    if (gameInfo.teammates != null && gameInfo.teammates.size() > 0) {
                                        travelRoomMembers(member -> {
                                            if (gameInfo.teammates.contains(member.memberPosition - 1)) {
//                                                member.isWolf = true;
                                                member.memberRole = Constant.ROLE_WOLF;
                                            }
                                        });
                                    }
                                    if (gameInfo.teammate_roles != null && gameInfo.teammate_roles.size() > 0) {
                                        ToolKit.travelMap(gameInfo.teammate_roles, (key, value112) -> {
                                            WereWolfGameMember member = getGameMember(key + 1);
                                            if (member != null) {
                                                member.memberRole = value112;
                                            }
                                        });
                                    }

                                    /**
                                     * 恢复狼王数据
                                     */
                                    if (gameInfo.king >= 0) {
                                        WereWolfGameMember member = getGameMember(gameInfo.king + 1);
                                        if (member != null) {
                                            member.memberRole = Constant.ROLE_WEREWOLF_KING;
                                        }
                                    }
                                    /**
                                     * 恢复恶魔数据
                                     */
                                    if (gameInfo.demon >= 0) {
                                        WereWolfGameMember member = getGameMember(gameInfo.demon + 1);
                                        if (member != null) {
                                            member.memberRole = WereWolfConstant.ROLE_DEMON;
                                        }
                                    }

                                    /**
                                     * 恢复情侣数据
                                     */
                                    if (gameInfo.linked != null && gameInfo.linked.size() > 0) {
                                        ToolKit.travelMap(gameInfo.linked, (key, value12) -> {
                                            int position = key + 1;
                                            WereWolfGameMember member = getGameMember(position);
                                            if (member != null) {
                                                member.isLover = true;
                                                if (value12) {
//                                                    member.isWolf = true;
                                                    member.memberRole = Constant.ROLE_WOLF;
                                                }
                                            }
                                        });
                                    }
                                    /**
                                     * 恢复死亡信息
                                     */
                                    if (gameInfo.death != null && gameInfo.death.size() > 0) {
                                        travelRoomMembers(member -> {
                                            if (gameInfo.death.contains(member.memberPosition - 1)) {
                                                if (member.memberPosition == mPersonalPosition) {
                                                    isDead = true;
                                                }
                                                member.isDead = true;
                                            }
                                        });
                                    }
                                    /**
                                     * 恢复死亡以及显示角色信息数据
                                     */
                                    if (gameInfo.death_info != null && gameInfo.death_info.size() > 0) {
                                        ToolKit.travelCollection(gameInfo.death_info, value13 -> {
                                            int position = value13.position + 1;
                                            String role = value13.role;
                                            WereWolfGameMember member = getGameMember(position);
                                            if (member != null) {
                                                member.memberRole = role;
                                                member.isDead = true;
                                            }
                                        });
                                    }

                                    /**
                                     * 恢复Speech,放在子模块去恢复
                                     */
                                    if (gameInfo.speech_info != null) {
                                        mCurSpeechPosition = gameInfo.speech_info.current.position + 1;
                                    }
                                    /**
                                     *  恢复DayInfo
                                     */
                                    if (gameInfo.day_info != null) {
                                        isSunSet = gameInfo.day_info.is_night;
                                        if (isSunSet) {
                                            handleSunSet(gameInfo.day_info.day_index, gameInfo.day_info.duration / 1000, new JSONObject());
                                            orbitEngineAble(false);
                                        } else {
                                            handleSunUp(gameInfo.day_info.day_index, new JSONObject());
                                            orbitEngineAble(true);
                                        }
                                        if (gameInfo.day_info.can_speak) {
                                            handleWolfCanSpeak(gameInfo.day_info.can_speak_duration / 1000, new JSONObject());
                                        }
                                    }
                                    /**
                                     * 恢复是否可以自爆
                                     */
                                    handleBoomState(result.game_info.boomable, new JSONObject());
                                    /**
                                     * 恢复骑士决斗图标
                                     */
                                    ServerActionFightState fightState = new ServerActionFightState();
                                    fightState.can_fight = result.game_info.fightable;
                                    handleServerActionFightState(fightState);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /**
                         * 状态恢复完毕，进行向下传递
                         */
                        handleRestoreRoom(result);
                    }
                    break;
                    case Constant.CAN_SPEAK: {
                        int dur = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleWolfCanSpeak(dur, payload);
                    }
                    break;
                    case Constant.GAMEOVER: {
                        mCurSpeechPosition = -1;
                        isGaming = false;
                        isDead = false;
                        mSelfRole = "";
                        GameOverResult gameOverResult = GsonHelper.getGson().fromJson(payload.toString(), GameOverResult.class);
                        travelRoomMembers(value110 -> {
                            value110.clearWhenGameOver();
                        });
                        mBackGroundMusicManager.handleGameOver(gameOverResult);

                        orbitEngineAble(true);

                        handleGameOver(gameOverResult);
                    }
                    break;
                    case WereWolfConstant.EXCHANGE: {
                        WereWolfExchange wereWolfExchange = GsonHelper.getGson().fromJson(payload.toString(), WereWolfExchange.class);
                        handleExchange(wereWolfExchange, payload);
                    }
                    break;
                    case WereWolfConstant.EXCHANGE_RESULT: {
                        WereWolfExchangeResult wereWolfExchangeResult = GsonHelper.getGson().fromJson(payload.toString(), WereWolfExchangeResult.class);
                        handleExchangeResult(wereWolfExchangeResult, payload);
                    }
                    break;
                    case WereWolfConstant.DEMON_CHECK: {
                        WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(payload.toString(), WereWolfGameSurvivorsInfo.class);
                        handleDemonCheck(info, payload);
                    }
                    break;
                    case WereWolfConstant.DEMON_CHECK_RESULT: {
                        boolean isGod = payload.optBoolean("is_god");
                        int duration = RoomSocketEngineHelper.getOptDurTime(payload);
                        handleDemonCheckResult(isGod, duration, payload);
                    }
                    break;

                    case Constant.GAME_UP_SEAT:

                        //游戏已经开始就不处理
                        if (isGaming()) {
                            return;
                        }

                        String userId = payload.optString(VoiceConstants.USER_ID_LOWER);
                        int newPosition = payload.optInt("dest_position") + 1;//server 0-11,client 1-12
                        WereWolfObserverMember observerMember = getObserverMember(userId);
                        if (observerMember != null) {
                            removeObserverMember(observerMember.memberPosition);

                            observerMember.isObserver = false;
                            observerMember.memberPosition = newPosition;
                            handleUpSeatView(observerMember, payload);

                            if (TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), userId)) {
                                isObserver = false;
                                mPersonalPosition = newPosition;
                            }

                            //添加一个玩家
                            WereWolfGameMember member = WereWolfObserverMember.TurnIntoGameMember(observerMember);
                            addRoomMembers(observerMember.memberPosition, member);
                            handleUpSeat(member, payload);
                        } else {
                            PLog.e(TAG, "handleUpseat member is null");
                        }
                        break;
                    case Constant.GAME_DOWN_SEAT:

                        //游戏已经开始就不处理
                        if (isGaming()) {
                            return;
                        }

                        String id = payload.optString(VoiceConstants.USER_ID_LOWER);
                        int destPosition = payload.optInt("dest_position") + 1;
                        int position2 = getPositionByUserId(id);
                        if (position2 >= 0) {
                            WereWolfGameMember gameMember = getGameMember(position2);
                            if (gameMember != null) {
                                removeRoomMembers(gameMember.memberPosition);
                                handleToObserveView(gameMember, payload);

                                gameMember.isObserver = true;
                                gameMember.memberPosition = destPosition;
                                if (TextUtils.equals(GlobalUserState.getGlobalState().getUserId(), id)) {
                                    isObserver = true;
                                    mPersonalPosition = destPosition;
                                }
                                //添加一个观战着者
                                addRoomObserverMembers(gameMember.memberPosition, gameMember);
                                handleToObserve(gameMember, payload);
                            } else {
                                PLog.e(TAG, "handleDownseat member is null");
                            }
                        }
                        break;
                    case WereWolfConstant.FIGHT:
                        ServerActionFight fight = GsonTurnToolKit.turn(payload.toString(), ServerActionFight.class);
                        handleServerActionFight(fight);
                        break;
                    case WereWolfConstant.FIGHT_AWAY:
                        ServerActionFightAway fightAway = GsonTurnToolKit.turn(payload.toString(), ServerActionFightAway.class);
                        handleServerActionFightAway(fightAway);
                        break;
                    case WereWolfConstant.FIGHT_AWAY_RESULT:
                        mBackGroundMusicManager.handleFightAwayResult();
                        ServerActionFightAwayResult fightAwayResult = GsonTurnToolKit.turn(payload.toString(), ServerActionFightAwayResult.class);
                        if (fightAwayResult != null && fightAwayResult.death_info != null && fightAwayResult.death_info.size() > 0) {
                            ToolKit.travelCollection(fightAwayResult.death_info, infoItem -> {
                                int position = infoItem.killed + 1;
                                if (position == mPersonalPosition) {
                                    isDead = true;
                                }
                                WereWolfGameMember member = getGameMember(position);
                                if (member != null) {
                                    member.isDead = true;
                                    member.memberRole = infoItem.role;
                                }
                            });
                        }
                        handleServerActionFightAwayResult(fightAwayResult);
                        break;
                    case WereWolfConstant.FIGHT_STATE:
                        ServerActionFightState fightState = GsonTurnToolKit.turn(payload.toString(), ServerActionFightState.class);
                        handleServerActionFightState(fightState);
                        break;
                }

                processEnd(type, payload);

                Log.i(TAG, "func: handle " + type + " message. cost " + (System.currentTimeMillis() - t) + " ms");
            } catch (Exception e) {

                e.printStackTrace();

                processError(e);
            }
        });
    }

    public boolean isWolfRole() {
        if (TextUtils.equals(mSelfRole, Constant.ROLE_WOLF)
                || TextUtils.equals(mSelfRole, Constant.ROLE_WEREWOLF_KING)
                || TextUtils.equals(mSelfRole, WereWolfConstant.ROLE_DEMON)
                || TextUtils.equals(mSelfRole, WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
            return true;
        } else {
            return false;
        }
    }

    public void addProcessLifeCycle(WereWolfGameProcessLifeCycle lifeCycle) {
        if (lifeCycle != null && !mProcessLifeCycleManager.contains(lifeCycle)) {
            mProcessLifeCycleManager.add(lifeCycle);
        }
    }

    public void removeProcessLeftCycle(WereWolfGameProcessLifeCycle lifeCycle) {
        mProcessLifeCycleManager.remove(lifeCycle);
    }

    public void addService(String key, IWereWolfService service) {
        mInnerServiceMap.put(key, service);
    }

    public void removeService(String key) {
        mInnerServiceMap.remove(key);
    }

    public IWereWolfService getService(String key) {
        return mInnerServiceMap.get(key);
    }

    public void clearService() {
        mInnerServiceMap.clear();
    }


    @Override
    public boolean handleServerActionFight(ServerActionFight fight) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleServerActionFight(fight)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAway(ServerActionFightAway fightAway) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleServerActionFightAway(fightAway)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightState(ServerActionFightState fightState) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleServerActionFightState(fightState)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleServerActionFightAwayResult(fightAwayResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleJoin(WereWolfGameMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleJoin(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleAddFriend(String userId, String userName, String userIv, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleAddFriend(userId, userName, userIv, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleLeave(int position, WereWolfGameMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleLeave(position, member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUpdateConfig(EnterRoomResult.RoomConfig config) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUpdateConfig(config)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleExport(String type, String title, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleExport(type, title, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleChat(int position, String message, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleChat(position, message, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleGift(String giftType, String message, int from, int to, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleGift(giftType, message, from, to, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleKickOut(int position, WereWolfGameMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleKickOut(position, member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSpeak(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSpeak(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUnSpeak(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUnSpeak(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handlePrepare(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handlePrepare(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUnPrepare(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUnPrepare(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleLock(List<Integer> lockPosition, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleLock(lockPosition, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleStart(startResult, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleAssginedRole(roleResult, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSunSet(dayIndex, dur, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSunUp(dayIndex, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleDeathInfo(DeathInfo deathInfo) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleDeathInfo(deathInfo)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleVote(dur, info, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo voteResultInfo) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleVoteResult(voteResultInfo)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleCheckResult(CheckResult checkResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleCheckResult(checkResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSystemMessage(String type, String content, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSystemMessage(type, content, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUnActiveWarning(int duration, JSONObject jsonObject) {
        WereWolfHintDialog dialog = new WereWolfHintDialog(mContext, MessageUtils.getString(R.string.dialog_hint), MessageUtils.getString(R.string.active_warning_string), null, null, true, true);
        Button button1 = dialog.getTwoButtonLeft();
        button1.setText(MessageUtils.getString(R.string.werewolf_game_leave));
        button1.setOnClickListener(v -> {
            if (dialog != null) {
                dialog.dismiss();
            }
            if (mDestroyFatherAction != null) {
                mDestroyFatherAction.func(false);
            }
        });

        Button button2 = dialog.getTwoButtonRight();
        button2.setText(MessageUtils.getString(R.string.werewolf_game_continue));
        button2.setOnClickListener(v -> {
            if (dialog != null) {
                dialog.dismiss();
            }
            RoomSocketEngineHelper.sendEmptyType(Constant.ACTION_CONTINUE);
        });

        dialog.setCountDownFinish(() -> {
            if (dialog != null) {
                dialog.dismiss();
            }
            if (mDestroyFatherAction != null) {
                mDestroyFatherAction.func(false);
            }
        });

        dialog.showDialog(duration);
        return true;
    }

    @Override
    public boolean handleUpdateMaster(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUpdateMaster(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public boolean handleResetMaster(int dur, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleResetMaster(dur, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleRejectResetMaster(int dur, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleRejectResetMaster(dur, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handlePreAllMessage(String type, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handlePreAllMessage(type, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleWakeToKill(WakeToKillResult wakeToKillResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleWakeToKill(wakeToKillResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleKillResult(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleKillResult(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleTakeAway(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleTakeAway(info, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleTakeAwayResult(DeathInfo deathInfo) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleTakeAwayResult(deathInfo)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSheriffResult(position, durSeconds, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleHandOver(WereWolfGameSurvivorsInfo info) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleHandOver(info)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleHandOverResult(from, to, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleLink(int duration, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleLink(duration, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleLinkResult(LinkResult linkResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleLinkResult(linkResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleApply(int durSeconds, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleApply(durSeconds, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleApplyResult(ApplyResult applyResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleApplyResult(applyResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleGiveUpApply(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleGiveUpApply(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleCardUseResult(String type, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleCardUseResult(type, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleAppendTime(int position, int appendTime, int leftTime, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleAppendTime(position, appendTime, leftTime, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleProtectResult(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleProtectResult(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleBoom(position, isWolfKing, duration, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleBoomAway(survivorsInfo)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleBoomAwayResult(DeathInfo info) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleBoomAwayResult(info)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSaveResult(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSaveResult(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handlePoisonResult(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handlePoisonResult(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSpeech(int position, int duration, boolean isDead, String type, List<Integer> lefts, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSpeech(position, duration, isDead, type, lefts, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSpeechDirection(int dur, int leftPosition, int rightPosition, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSpeechDirection(dur, leftPosition, rightPosition, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSpeechDirectionResult(int dur, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSpeechDirectionResult(dur, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleMuteAll(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleMuteAll(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUnMuteAll(int position, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUnMuteAll(position, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUpdatePassword(String password, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUpdatePassword(password, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleRestoreRoom(restoreResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleWolfCanSpeak(int dur, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleWolfCanSpeak(dur, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleBoomState(boolean flag, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleBoomState(flag, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSeerCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSeerCheck(info, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleSave(int position, boolean canSave, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleSave(position, canSave, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handlePoison(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handlePoison(info, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleCheckCardResult(String type, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleCheckCardResult(type, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleRobCardResult(String type, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleRobCardResult(type, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleAppendTimeResult(String type, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleAppendTimeResult(type, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleProtect(WereWolfGameSurvivorsInfo info, int lastPosition, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleProtect(info, lastPosition, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleGameOver(gameOverResult)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleExchange(WereWolfExchange exchange, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleExchange(exchange, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleExchangeResult(WereWolfExchangeResult exchangeResult, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleExchangeResult(exchangeResult, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleDemonCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleDemonCheck(info, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleDemonCheckResult(boolean is_god, int duration, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleDemonCheckResult(is_god, duration, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleObserverJoin(WereWolfObserverMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleObserverJoin(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleObserverLeave(WereWolfObserverMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleObserverLeave(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUpSeat(WereWolfObserverMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUpSeat(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleToObserve(WereWolfGameMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleToObserve(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleUpSeatView(WereWolfObserverMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleUpSeatView(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean handleToObserveView(WereWolfGameMember member, JSONObject jsonObject) {
        for (WereWolfGameProcess temp : mProcessManager) {
            try {
                if (temp.handleToObserveView(member, jsonObject)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
