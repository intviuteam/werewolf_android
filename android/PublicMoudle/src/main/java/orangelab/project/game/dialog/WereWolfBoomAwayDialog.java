package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/14 上午10:25
 * Mystery
 */

public class WereWolfBoomAwayDialog extends WereWolfOnePersonSelectDialog {

    public WereWolfBoomAwayDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.takeaway_title), Constant.BOOM_AWAY, ONE_BUTTON);
    }
}