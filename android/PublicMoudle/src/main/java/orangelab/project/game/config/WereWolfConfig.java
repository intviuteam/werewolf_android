package orangelab.project.game.config;

import android.os.Build;
import android.text.TextUtils;

import com.R;
import com.networktoolkit.transport.Constant;

import orangelab.project.game.WereWolfConstant;

/**
 * game_werewolf
 * 2017/11/27 下午6:27
 * Mystery
 */

public class WereWolfConfig {

    public static boolean isWereWolfGame(String type) {
        if (TextUtils.isEmpty(type)) {
            return false;
        }
        if (TextUtils.equals(type, Constant.GAME_TYPE_SIMPLE)

                || TextUtils.equals(type, Constant.GAME_TYPE_SIMPLE_6)
                || TextUtils.equals(type, Constant.GAME_TYPE_SIMPLE_9)
                || TextUtils.equals(type, Constant.GAME_TYPE_SIMPLE_10)

                || TextUtils.equals(type, Constant.GAME_TYPE_PRE_SIMPLE)
                || TextUtils.equals(type, Constant.GAME_TYPE_PRE_SIMPLE_NEW)

                || TextUtils.equals(type, Constant.GAME_TYPE_NORMAL)
                || TextUtils.equals(type, Constant.GAME_TYPE_NORMAL_GUARD)
                || TextUtils.equals(type, Constant.GAME_TYPE_HIGH_KING)
                ) {

            return true;
        }
        return false;
    }

    public static boolean IsHighKingGame(String type) {
        if (TextUtils.equals(type, WereWolfConstant.GAME_TYPE_HIGH_KING)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isShowRoleWhenDead(String gameType) {
        if (TextUtils.equals(gameType, WereWolfConstant.GAME_TYPE_PRE_SIMPLE_NEW) ||
                TextUtils.equals(gameType, WereWolfConstant.GAME_TYPE_SIMPLE_6)) {
            return true;
        } else {
            return false;
        }
    }

}
