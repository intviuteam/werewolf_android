package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/12/9 下午1:35
 * Mystery
 */

public class WereWolfSystemMsgDialog extends SafeDialog {

    public static final int ONE_BUTTON_TYPE = 1;
    public static final int TWO_BUTTON_TYPE = 2;

    private TextView mMsg;
    private TextView mCountDown;
    private Button mLeft;
    private Button mRight;
    private CountDownTaskManager.CountDownTask mCountDownTask;

    private Button mTwoLeft;
    private Button mTwoRight;
    private Button mOneMid;

    public WereWolfSystemMsgDialog(@NonNull Context context) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_werewolf_system_msg);

        mMsg = (TextView) findViewById(R.id.id_werewolf_system_msg);
        mCountDown = (TextView) findViewById(R.id.dialog_count_down);

        mLeft = (Button) findViewById(R.id.cancel);
        mRight = (Button) findViewById(R.id.confirm);

//        resizeDialog(
//                ScreenUtils.getCurrentScreenWidth(),
//                WindowManager.LayoutParams.WRAP_CONTENT
//        );
        WereWolfDialogDecorate.DecorateDialog(getWindow());
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    public WereWolfSystemMsgDialog setMessage(String msg) {
        mMsg.setText(msg);
        return this;
    }

    public WereWolfSystemMsgDialog setButtonType(int type) {
        if (type == ONE_BUTTON_TYPE) {
            mLeft.setVisibility(View.VISIBLE);
            mRight.setVisibility(View.GONE);
            mOneMid = mLeft;
        } else {
            mLeft.setVisibility(View.VISIBLE);
            mRight.setVisibility(View.VISIBLE);
            mTwoLeft = mLeft;
            mTwoRight = mRight;
        }
        return this;
    }

    public Button getTwoLeft() {
        return mTwoLeft;
    }

    public Button getTwoRight() {
        return mTwoRight;
    }

    public Button getOneMid() {
        return mOneMid;
    }

    public void showDialog(int dur) {
        if (dur > 0) {
            if (mCountDownTask == null) {
                mCountDownTask = new CountDownTaskManager.CountDownTask(dur, new CountDownTaskManager.CountDownTaskListener() {
                    @Override
                    public void onTick(int seconds) {
                        runSafely(() -> {
                            mCountDown.setText(MessageUtils.getString(R.string.werewolf_game_countdown, Integer.toString(seconds)));
                        });
                    }

                    @Override
                    public void onStart(int startSeconds) {

                    }

                    @Override
                    public void onFinish() {
                        runSafely(() -> {
                            dismiss();
                        });
                    }
                });
                mCountDownTask.startTask();
                show();
            }
        } else {
            show();
        }
    }

    @Override
    protected void release() {
        if (mCountDownTask != null) {
            mCountDownTask.cancelTask();
            mCountDownTask.destroyTask();
        }
    }
}
