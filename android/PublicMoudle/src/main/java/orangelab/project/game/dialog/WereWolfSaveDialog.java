package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/12/6 下午9:10
 * Mystery
 */

public class WereWolfSaveDialog extends WereWolfBaseDialog {

    public WereWolfSaveDialog(@NonNull Context context, boolean canSave, WereWolfSelectMember member) {
        super(context);
        setTitle(MessageUtils.getString(R.string.operation_save_title));
        View view = View.inflate(context, R.layout.layout_werewolf_check_result, null);

        WereWolfSelectMemberHolder holder = new WereWolfSelectMemberHolder(view);
        holder.setPosition(member.memberPosition);
        holder.setSelfPosition(-1);
        holder.setLastProtectPosition(-1);
        holder.setName(member.memberName);
        holder.setHead(member.memberImage);
        holder.setSelect(false);

        TextView textView = (TextView) view.findViewById(R.id.id_werewolf_checkresult_text);
        setDialogContentView(view);
        if (canSave) {
            textView.setText(context.getString(R.string.get_killed, member.memberPosition + ""));
            setButtonType(TWO_BUTTON);
            getTwoButtonLeft().setText(MessageUtils.getString(R.string.operation_unsave));
            getTwoButtonRight().setText(MessageUtils.getString(R.string.operation_save));
            getTwoButtonLeft().setOnClickListener(v -> {
                RoomSocketEngineHelper.sendEmptyType(Constant.SAVE);
                dismiss();
            });
            getTwoButtonRight().setOnClickListener(v -> {
                RoomSocketEngineHelper.savePeople(member.memberPosition);
                dismiss();
            });
        } else {
            textView.setText(MessageUtils.getString(R.string.can_save_title));
            setButtonType(ONE_BUTTON);
            getOneButton().setOnClickListener(v -> {
                dismiss();
            });
        }


    }
}
