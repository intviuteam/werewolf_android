package orangelab.project.game.manager;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.androidtoolkit.RxToolKit;

import orangelab.project.common.event.ViewEvent;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.view.IFriendRequestView;
import orangelab.project.game.view.WereWolfFriendRequestView;
import orangelab.project.common.view.RequestFriendView;
import orangelab.project.voice.privateroom.PrivateRoomTools;
import orangelab.project.voice.privateroom.view.PrivateRoomFriendRequestView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * game_werewolf
 * 2017/8/22 下午2:39
 * Mystery
 */

public class FriendRequestViewManager implements Runnable {
    private static final String TAG = "RequestFriendManager";

    private Context mContext;
    private ViewGroup mRootView;
    private IFriendRequestView curRequestFriendView;
    private List<ViewEvent.ReceiveAddFriendEvent> mAllEvent;
    private Set<String> mUserIdFilter = null;
    private boolean useOuterSupportFilter = false;

    public FriendRequestViewManager(Context context, ViewGroup root, Set<String> supportUserIdFilter) {
        mContext = context;
        mRootView = root;
        mAllEvent = new ArrayList<>(0);
        if (supportUserIdFilter == null) {
            mUserIdFilter = new HashSet<>(0);
        } else {
            mUserIdFilter = supportUserIdFilter;
            useOuterSupportFilter = true;
        }
        initListener();
    }

    public void restoreUserFilter(Set<String> userIdFilter) {
        if (userIdFilter == null) {
            mUserIdFilter = new HashSet<>(0);
        } else {
            mUserIdFilter = userIdFilter;
            useOuterSupportFilter = true;
        }
    }

    public FriendRequestViewManager(Context context, ViewGroup root) {
        this(context, root, null);
    }

    private void initListener() {
        RxToolKit.Build(this, ViewEvent.ReceiveAddFriendEvent.class).action(requestFriendEvent -> {
            try {
                if (mUserIdFilter.contains(requestFriendEvent.getFromUserID())) {
                    /**
                     * 这个人的userID已经处理过了过滤掉
                     */
                    return;
                }
                tryCacheEvent(requestFriendEvent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).register();
    }

    private void addCacheEvent(ViewEvent.ReceiveAddFriendEvent event) {
        Log.i(TAG, "addCacheEvent: ");
        mAllEvent.add(event);
    }

    private ViewEvent.ReceiveAddFriendEvent getEventFromCache() {
        return mAllEvent.remove(0);
    }

    private void tryCacheEvent(ViewEvent.ReceiveAddFriendEvent event) {
        if (curRequestFriendView == null) {
            /**
             * 当前没有展示的好友请求，那么就直接创建一个
             */
            createFriendRequestView(event);
        } else {
            /**
             * 当前已经又一个显示了，那么这个就放入缓存
             */
            addCacheEvent(event);
        }
    }

    private void createFriendRequestView(ViewEvent.ReceiveAddFriendEvent event) {
        if (TextUtils.isEmpty(event.getFromType())) {
            curRequestFriendView = new RequestFriendView(mContext, event.getFromUserID(), event.getFromUserName(), event.getFromUserIv(), mUserIdFilter);
        } else if (TextUtils.equals(event.getFromType(), WereWolfConstant.GAME_FRIEND_REQUEST)) {
            curRequestFriendView = new WereWolfFriendRequestView(mContext, event.getFromUserID(), event.getFromUserName(), event.getFromUserIv(), mUserIdFilter);
        } else if (TextUtils.equals(event.getFromType(), PrivateRoomTools.INSTANCE.getREQUEST_FRIEND())) {
            if (event.getPrivateRoomMemberData() == null)
                return;
            curRequestFriendView = new PrivateRoomFriendRequestView(event.getPrivateRoomMemberData(), mContext, mUserIdFilter);
        }
        curRequestFriendView.setDismissListener(this);
        showCurRequestFriendView();
    }

    private void showCurRequestFriendView() {
        if (curRequestFriendView != null) {
            /**
             * show
             */
            if (mRootView != null) {
                int width = curRequestFriendView.getWidth();
                int height = curRequestFriendView.getHeight();
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                        width,
                        height
                );
                int x = curRequestFriendView.getX();
                int y = curRequestFriendView.getY();
                View view = curRequestFriendView.getView();
                view.setX(x);
                view.setY(y);
                mRootView.addView(view, layoutParams);
            }
        }
    }

    public void destroy() {
        RxToolKit.UnRegister(this);
        destroyCurRequestView();
        if (mContext != null) {
            mContext = null;
        }
        mAllEvent.clear();
        if (!useOuterSupportFilter) {
            mUserIdFilter.clear();
        }
        if (mRootView != null) {
            mRootView.removeAllViews();
            mRootView = null;
        }
    }

    private void destroyCurRequestView() {
        if (curRequestFriendView != null) {
            curRequestFriendView.destroy();
            if (mRootView != null) {
                mRootView.removeView(curRequestFriendView.getView());
            }
            curRequestFriendView = null;
        }
    }

    private void clearCache(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        Iterator<ViewEvent.ReceiveAddFriendEvent> iterator = mAllEvent.iterator();
        while (iterator.hasNext()) {
            try {
                ViewEvent.ReceiveAddFriendEvent temp = iterator.next();
                if (TextUtils.equals(temp.getFromUserID(), userId)) {
                    iterator.remove();
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
        //done
    }

    @Override
    public void run() {
        /**
         * 拿到这个UserId
         */
        try {
            if (curRequestFriendView != null) {
                String userId = curRequestFriendView.getUserId();
                /**
                 * 先处理一下缓存
                 */
                clearCache(userId);
            }
            /**
             * 有一个对话框消失了，先销毁这个View
             */
            destroyCurRequestView();

            /**
             * 从缓存中再那一个Event出来
             */
            ViewEvent.ReceiveAddFriendEvent event = getEventFromCache();
            if (event == null) {
                /**
                 * 缓存中没有数据
                 */
                return;
            }
            /**
             * 缓存中有数据那么就直接创建一个
             */
            createFriendRequestView(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
