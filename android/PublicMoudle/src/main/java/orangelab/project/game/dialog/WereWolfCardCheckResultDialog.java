package orangelab.project.game.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import orangelab.project.common.model.Message;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2017/12/6 下午9:06
 * Mystery
 */

public class WereWolfCardCheckResultDialog extends WereWolfBaseDialog {
    private TextView mTextView;
    private ImageView mImageView;

    public WereWolfCardCheckResultDialog(Context context, String role) {
        super(context);
        setTitle(MessageUtils.getString(R.string.operation_check));
        View view = View.inflate(context, R.layout.layout_werewolf_card_check_result, null);
        setDialogContentView(view);
        mTextView = (TextView) view.findViewById(R.id.checkcard_result_tv);
        mImageView = (ImageView) view.findViewById(R.id.checkcard_result_iv);
        int res = MessageUtils.getRoleImage(role);
        if (res != -1) {
            mImageView.setImageResource(res);
        }
        mTextView.setText(MessageUtils.getString(R.string.operation_check_result, MessageUtils.getRole(role)));
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }
}
