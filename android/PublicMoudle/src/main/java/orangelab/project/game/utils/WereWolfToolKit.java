package orangelab.project.game.utils;

import android.content.Context;
import android.widget.Button;

import com.R;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.dialog.WereWolfSystemMsgDialog;
import orangelab.project.game.model.WereWolfExchangeResult;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import com.networktoolkit.transport.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/11/27 下午5:54
 * Mystery
 */

public class WereWolfToolKit {


    public static String GetWereWolfGameTypeString(String gameType) {
        switch (gameType) {
            case WereWolfConstant.GAME_TYPE_PRE_SIMPLE:
                return WereWolfString.getString(R.string.werewolf_room_type_pre_simple);
            case WereWolfConstant.GAME_TYPE_PRE_SIMPLE_NEW:
                return WereWolfString.getString(R.string.werewolf_room_type_pre_simple_6);
            case WereWolfConstant.GAME_TYPE_SIMPLE:
                return WereWolfString.getString(R.string.werewolf_room_type_simple_10);
            case WereWolfConstant.GAME_TYPE_SIMPLE_6:
                return WereWolfString.getString(R.string.werewolf_room_type_simple_6);
            case WereWolfConstant.GAME_TYPE_SIMPLE_9:
                return WereWolfString.getString(R.string.werewolf_room_type_simple_9);
            case WereWolfConstant.GAME_TYPE_SIMPLE_10:
                return WereWolfString.getString(R.string.werewolf_room_type_simple_10);
            case WereWolfConstant.GAME_TYPE_NORMAL:
                return WereWolfString.getString(R.string.werewolf_room_type_normal_12_cupid);
            case WereWolfConstant.GAME_TYPE_NORMAL_GUARD:
                return WereWolfString.getString(R.string.werewolf_room_type_normal_12_guard);
            case WereWolfConstant.GAME_TYPE_HIGH_KING:
                return WereWolfString.getString(R.string.werewolf_room_type_high_12_werewolf_king);
        }
        return "";
    }

    public static void ShowResetMasterDialog(Context context, int dur) {
        WereWolfSystemMsgDialog dialog = new WereWolfSystemMsgDialog(context);
        dialog.setMessage(WereWolfString.getString(R.string.reset_master_dialog_text));
        dialog.setButtonType(WereWolfSystemMsgDialog.TWO_BUTTON_TYPE);
        Button leftButton = dialog.getTwoLeft();
        leftButton.setText(WereWolfString.getString(R.string.add_friend_refuse));
        leftButton.setOnClickListener(v -> {
            RoomSocketEngineHelper.ResetMasterAllowed(false);
            dialog.dismiss();
        });
        Button rightButton = dialog.getTwoRight();
        rightButton.setText(WereWolfString.getString(R.string.confirm));
        rightButton.setOnClickListener(v -> {
            RoomSocketEngineHelper.ResetMasterAllowed(true);
            dialog.dismiss();
        });
        dialog.showDialog(dur);
    }

    public static void ShowSystemMsg(Context context, String message, int dur) {
        WereWolfSystemMsgDialog dialog = new WereWolfSystemMsgDialog(context);
        dialog.setMessage(message);
        dialog.setButtonType(WereWolfSystemMsgDialog.ONE_BUTTON_TYPE);
        Button midButton = dialog.getOneMid();
        midButton.setText(WereWolfString.getString(R.string.confirm));
        midButton.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.showDialog(dur);
    }

    public static WereWolfGameSurvivorsInfo GetSurvivorsFromJSON(JSONObject p) {
        try {
            WereWolfGameSurvivorsInfo info = GsonHelper.getGson().fromJson(p.toString(), WereWolfGameSurvivorsInfo.class);
            if (info != null && info.alives != null && info.alives.size() > 0) {
                return info;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static void UpdateRoomSetting(String levelLimit, String password, boolean canCutSpeaker, String peopleParts, String werewolfParts) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(WereWolfConstant.LEVEL, levelLimit);
            jsonObject.put(Constant.PASSWORD, password);
            jsonObject.put(WereWolfConstant.CAN_CUT_SPEAKER, canCutSpeaker);
            jsonObject.put(WereWolfConstant.PEOPLE_PARTS, peopleParts);
            jsonObject.put(WereWolfConstant.WEREWOLF_PARTS, werewolfParts);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(WereWolfConstant.UPDATE_CONFIG, jsonObject);
    }

    public static void SendExchangePositions(int position1, int position2) {
        List<Integer> data = new ArrayList<>();
        data.add(position1);
        data.add(position2);
        WereWolfExchangeResult transport = new WereWolfExchangeResult();
        transport.positions = data;
        try {
            RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(WereWolfConstant.EXCHANGE, new JSONObject(GsonHelper.getGson().toJson(transport)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
