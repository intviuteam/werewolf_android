package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/6 下午9:41
 * Mystery
 */

public class WereWolfTakeAwayDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfTakeAwayDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.takeaway_title), Constant.TAKEAWAY, ONE_BUTTON);
    }
}
