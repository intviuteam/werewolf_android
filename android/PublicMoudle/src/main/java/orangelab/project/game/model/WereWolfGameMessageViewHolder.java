package orangelab.project.game.model;

import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidtoolkit.CodeTimeRecord;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.adapter.WereWolfGameMessageVoteAdapter;
import orangelab.project.game.adapter.WereWolfGameMessageVoteGridViewAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * game_werewolf
 * 2017/12/3 下午3:01
 * Mystery
 */

public class WereWolfGameMessageViewHolder {

    public static final int PADDING = ScreenUtils.dip2px(4);

    private View mMemberMessageContainer;
    private TextView mMemberPositionView;
    private TextView mMemberMessage;
    private View mMemberMessageInnerContainer;
    private ImageView mMemberGift;

    private View mSystemMessageContainer;
    private TextView mSystemMessage;

    private View mJudgeMessageContainer;
    private TextView mJudgeTypeMessage;
    private TextView mJudgeMessage;
    private LinearLayout mJudgeVoteMessage;

    public WereWolfGameMessageViewHolder(View rootView) {

        mMemberMessageContainer = rootView.findViewById(R.id.id_werewolf_msg_type_member);
        mMemberMessage = (TextView) rootView.findViewById(R.id.id_werewolf_msg_memebr);
        mMemberGift = (ImageView) rootView.findViewById(R.id.id_werewolf_msg_member_gift);
        mMemberPositionView = (TextView) rootView.findViewById(R.id.id_werewolf_msg_position);
        mMemberMessageInnerContainer = rootView.findViewById(R.id.id_werewolf_msg_container);

        mSystemMessageContainer = rootView.findViewById(R.id.id_werewolf_msg_type_system);
        mSystemMessage = (TextView) rootView.findViewById(R.id.id_werewolf_msg_system);

        mJudgeMessageContainer = rootView.findViewById(R.id.id_werewolf_msg_type_judge);
        mJudgeTypeMessage = (TextView) rootView.findViewById(R.id.id_werewolf_msg_type_judge_type);
        mJudgeMessage = (TextView) rootView.findViewById(R.id.id_werewolf_msg_type_judge_message);
        mJudgeVoteMessage = (LinearLayout) rootView.findViewById(R.id.id_werewolf_msg_type_judge_vote);
    }

    public void turnJudgeBackGroundRed() {
    }

    public void turnJudgeBackGroundBlue() {
    }

    public void turnIntoSystemMessage() {
        mMemberMessageContainer.setVisibility(View.GONE);
        mMemberGift.setVisibility(View.GONE);
        mSystemMessageContainer.setVisibility(View.VISIBLE);
        mJudgeMessageContainer.setVisibility(View.GONE);
        mJudgeVoteMessage.setVisibility(View.GONE);
    }

    public void turnIntoMemberMessage() {
        mMemberMessageContainer.setVisibility(View.VISIBLE);
        mMemberGift.setVisibility(View.GONE);
        mMemberMessageInnerContainer.setBackgroundResource(R.mipmap.ico_werewolf_messages_member_bg);
        mMemberMessage.setTextColor(mMemberMessage.getResources().getColor(R.color.werewolf_member_chat_color));
        mSystemMessageContainer.setVisibility(View.GONE);
        mJudgeMessageContainer.setVisibility(View.GONE);
        mJudgeVoteMessage.setVisibility(View.GONE);
        mJudgeMessage.setVisibility(View.GONE);
    }

    public void turnIntoMemberMessageForGift() {
        mMemberMessageContainer.setVisibility(View.VISIBLE);
        mMemberGift.setVisibility(View.VISIBLE);
        mMemberMessage.setTextColor(mMemberMessage.getResources().getColor(R.color.werewolf_member_chat_color));
        mMemberMessageInnerContainer.setBackgroundResource(R.drawable.drawable_werewolf_msg_judge_text);
        mMemberMessageInnerContainer.setPadding(PADDING, PADDING, PADDING, PADDING);
        mSystemMessageContainer.setVisibility(View.GONE);
        mJudgeMessageContainer.setVisibility(View.GONE);
        mJudgeVoteMessage.setVisibility(View.GONE);
        mJudgeMessage.setVisibility(View.GONE);
    }

    public void turnIntoJudgeMessage() {
        mMemberMessageContainer.setVisibility(View.GONE);
        mMemberGift.setVisibility(View.GONE);
        mSystemMessageContainer.setVisibility(View.GONE);
        mJudgeMessageContainer.setVisibility(View.VISIBLE);
        mJudgeVoteMessage.setVisibility(View.GONE);
        mJudgeMessage.setVisibility(View.VISIBLE);
    }

    public void turnIntoJudgeMessageForVote() {
        mMemberMessageContainer.setVisibility(View.GONE);
        mMemberGift.setVisibility(View.GONE);
        mSystemMessageContainer.setVisibility(View.GONE);
        mJudgeMessageContainer.setVisibility(View.VISIBLE);
        mJudgeVoteMessage.setVisibility(View.VISIBLE);
        mJudgeMessage.setVisibility(View.GONE);
    }

    public void setJudgeMessage(String message) {
        mJudgeMessage.setText(message);
    }

    public void setSystemMessage(String message) {
        mSystemMessage.setText(message);
    }

    public void setJudgeTypeMessage(String msg) {
        mJudgeTypeMessage.setText(msg);
    }

    public void setJudgeTypeSunSet(boolean isSunSet) {

    }

    public void setMemberPosition(int position) {
        mMemberPositionView.setText(Integer.toString(position));
    }

    public void setMemberMessage(String msg) {
        mMemberMessage.setText(msg);
    }

    public void setMemberGift(String giftType) {
        EffectsManager.FillGiftImageView(giftType, mMemberGift);
    }

    public void setJudgeVoteInfo(WereWolfGameContext gameContext, Map<Integer, List<Integer>> vote_info) {
        CodeTimeRecord.iRecord.recordCodeStart("setJudgeVoteInfo");
        mJudgeVoteMessage.removeAllViews();
        /**
         * 添加一个投票结果文字
         */
        TextView textView = new TextView(gameContext.getAndroidContext());
        final int padding = ScreenUtils.dip2px(6);
        textView.setPadding(padding, 0, padding, padding);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        textView.setText(MessageUtils.getString(R.string.string_werewolf_vote_publish));
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.WHITE);
        mJudgeVoteMessage.addView(textView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        List<WereWolfGameMessageVoteAdapter.ViewHolder> holderList = (List<WereWolfGameMessageVoteAdapter.ViewHolder>) mJudgeVoteMessage.getTag();
        if (holderList != null && holderList.size() > 0) {
            for (WereWolfGameMessageVoteAdapter.ViewHolder holder : holderList) {
                WereWolfGameMessageVoteGridViewAdapter adapter = (WereWolfGameMessageVoteGridViewAdapter) holder.fromPositions.getAdapter();
                if (adapter != null) {
                    adapter.destroy();
                }
            }
            holderList.clear();
        } else {
            holderList = new ArrayList<>();
        }

        Iterator<Map.Entry<Integer, List<Integer>>> iterator = vote_info.entrySet().iterator();
        int size = vote_info.size();
        int index = 0;
        while (iterator.hasNext()) {
            Map.Entry<Integer, List<Integer>> entry = iterator.next();

            WereWolfGameMessageVoteAdapter.ViewHolder holder = new WereWolfGameMessageVoteAdapter.ViewHolder();

            View view = View.inflate(gameContext.getAndroidContext(), R.layout.layout_werewolf_msg_list_item_vote_item, null);
            holder.targetPosition = (TextView) view.findViewById(R.id.id_werewolf_msg_list_item_vote_item_position);
            holder.fromPositions = (GridView) view.findViewById(R.id.id_werewolf_msg_list_item_vote_item_gridview);
            int keyPosition = entry.getKey();
            if (keyPosition != -1) {
                keyPosition = keyPosition + 1;
                holder.targetPosition.setText(Integer.toString(keyPosition));
            } else {
                holder.targetPosition.setText("");
                holder.targetPosition.setBackgroundResource(R.mipmap.ico_werewolf_game_vote_quit);
            }

            List<Integer> list = entry.getValue();
            int c = 6;
            int listSize = list.size();
            int b = listSize / 6 + (listSize % 6 == 0 ? 0 : 1);

            int gridWidth = 6 * ScreenUtils.dip2px(20);
            int gridHeight = b * ScreenUtils.dip2px(18);
            ViewGroup.LayoutParams layoutParams = holder.fromPositions.getLayoutParams();
            layoutParams.height = gridHeight;
            layoutParams.width = gridWidth;
            holder.fromPositions.setLayoutParams(layoutParams);

            WereWolfGameMessageVoteGridViewAdapter adapter = new WereWolfGameMessageVoteGridViewAdapter(entry.getValue(), gameContext);
            holder.fromPositions.setAdapter(adapter);

            holderList.add(holder);
            mJudgeVoteMessage.addView(view);

            /**
             * 添加一条线
             */
            if (index == size - 1) {
                /**
                 * 最后一条不处理
                 */
            } else {
                ImageView lineView = new ImageView(gameContext.getAndroidContext());
                lineView.setImageResource(R.mipmap.ico_werewolf_vote_line);
                lineView.setScaleType(ImageView.ScaleType.FIT_XY);
                lineView.setPadding(padding, padding, padding, padding);
                mJudgeVoteMessage.addView(lineView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            index++;
        }
        mJudgeVoteMessage.setTag(holderList);
        CodeTimeRecord.iRecord.recordCodeEnd("setJudgeVoteInfo");
    }


    public TextView getMemberMessage() {
        return mMemberMessage;
    }

    public View getMemberMessageInnerContainer() {
        return mMemberMessageInnerContainer;
    }
}
