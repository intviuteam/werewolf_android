package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.game.utils.WereWolfToolKit;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/14 下午1:39
 * Mystery
 */

public class WereWolfExchangeDialog extends WereWolfBaseSelectDialog {

    public WereWolfExchangeDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, 2);
        initView(WereWolfString.getString(R.string.werewolf_role_magician_title));
    }

    private void initView(String title) {
        setTitle(title);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            if (selectIslegal()) {
                WereWolfToolKit.SendExchangePositions(getRealPosition().get(0) - 1, getRealPosition().get(1) - 1);
                dismiss();
            }
        });
    }

    private boolean selectIslegal() {
        List<Integer> result = getRealPosition();
        if (result != null && result.size() == 2) {
            return true;
        } else {
            return false;
        }
    }
}