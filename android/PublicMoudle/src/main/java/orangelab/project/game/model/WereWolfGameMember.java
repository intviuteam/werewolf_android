package orangelab.project.game.model;

import orangelab.project.game.component.GameMember;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.ServerMessageEnterResult;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/12/1 下午5:42
 * Mystery
 */

public class WereWolfGameMember extends WereWolfObserverMember implements Keepable {

    public String memberRole = "";
    public int memberLastProtectPosition = -1;
    public boolean isRoomMaster = false;
    public boolean isPrepare = false;
    public boolean isSpeaking = false;
    public boolean isDead = false;
    public boolean isSheriff = false;
    public boolean isLover = false;
    public boolean isHandup = false;


    public void clearGameData() {
        isSheriff = false;
        isLover = false;
        isDead = false;
        isHandup = false;
        memberRole = "";
    }

    public void clearWhenGameOver() {
        clearGameData();
        memberLastProtectPosition = -1;
        isPrepare = false;
        isSpeaking = false;
    }

    public void clearStateData() {
        isRoomMaster = false;
        isPrepare = false;
        isSpeaking = false;
    }

    public static WereWolfGameMember FromEnterRoomResult(EnterRoomResult.EnterRoomUserItem item) {
        WereWolfGameMember gameMember = new WereWolfGameMember();
        gameMember.isRoomMaster = item.is_master;
        gameMember.memberHead = item.avatar;
        gameMember.memberId = item.id;
        gameMember.memberName = item.name;
        gameMember.isPrepare = item.prepared;
        gameMember.isSpeaking = item.speaking;
        gameMember.memberActive = item.active;
        gameMember.memberPosition = item.position + 1;
        gameMember.memberSex = item.sex;
        gameMember.signature = item.signature;
        gameMember.avatar_box = item.avatar_box;
        gameMember.message_box = item.message_box;
        gameMember.vipInfo = item.vipInfo;
        return gameMember;
    }

    public GameMember.GameMemberData turnIntoGameMemberData() {
        GameMember.GameMemberData gameMemberData = new GameMember.GameMemberData();
        gameMemberData.position = memberPosition;
        gameMemberData.userId = memberId;
        gameMemberData.userImage = memberHead;
        gameMemberData.userName = memberName;
        gameMemberData.userSex = memberSex;
        return gameMemberData;
    }

    @Override
    public String toString() {
        return "WereWolfGameMember{" +
                "memberPosition=" + memberPosition +
                ", memberId='" + memberId + '\'' +
                ", memberName='" + memberName + '\'' +
                ", isObserver=" + isObserver +
                ", isDead=" + isDead +
                '}';
    }
}
