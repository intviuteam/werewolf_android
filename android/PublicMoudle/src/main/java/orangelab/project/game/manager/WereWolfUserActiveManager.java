package orangelab.project.game.manager;

import android.widget.ImageView;
import android.widget.TextView;

import com.datasource.GlobalUserState;
import com.R;
import orangelab.project.common.effect.ApkResourceManager;

import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.web.WebViewActivity;
import com.networktoolkit.transport.TransportConfig;
import com.networktoolkit.transport.URLManager;

/**
 * game_werewolf
 * 2017/12/19 下午3:30
 * Mystery
 */

public class WereWolfUserActiveManager {


    public static void FillActiveImageView(UserInfoResult.UserActiveInfo info, ImageView imageView) {
        try {
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            String imageName = "level_" + info.type;
            int resource = ApkResourceManager.GetApkImageResource(imageName);
            if (resource != -1) {
                imageView.setImageResource(resource);
            } else {

            }
            imageView.setOnClickListener(v -> {
                WebViewActivity.load(imageView.getContext(), URLManager.GetDefaultURL() + "/rank_instruction?access_token="
                        + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String GetActiveText(UserInfoResult.UserActiveInfo info) {
        String title = info.title;
        int star = info.star;
        String textMsg = title + WereWolfString.getString(R.string.werewolf_user_active_level, Integer.toString(star));
        return textMsg;
    }

    public static void FillActiveTextViewWithOutBg(UserInfoResult.UserActiveInfo info, TextView textView) {
        textView.setTextColor(GetActiveTextColor(info, textView));
        textView.setText(GetActiveText(info));
        textView.setOnClickListener(v -> {
            WebViewActivity.load(textView.getContext(), URLManager.GetDefaultURL() + "/rank_instruction?access_token="
                    + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
        });
    }

    public static int GetActiveTextColor(UserInfoResult.UserActiveInfo info, TextView textView) {
        int level = info.index;
        int result = textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_1_text);
        if (level >= 1 && level <= 3) {
            result = textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_1_text);
        } else if (level > 3 && level <= 6) {
            result = (textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_2_text));
        } else if (level > 6 && level <= 9) {
            result = (textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_3_text));
        } else if (level > 9 && level <= 12) {
            result = (textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_4_text));
        } else if (level > 12 && level <= 15) {
            result = (textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_5_text));
        } else {
            result = (textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_6_text));
        }
        return result;
    }

    public static void FillActiveTextView(UserInfoResult.UserActiveInfo info, TextView textView) {
        try {
            int level = info.index;
            String title = info.title;
            int star = info.star;
            String textMsg = GetActiveText(info);
            textView.setText(textMsg);
            if (level >= 1 && level <= 3) {
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_1);
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_1_text));
            } else if (level > 3 && level <= 6) {
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_2);
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_2_text));
            } else if (level > 6 && level <= 9) {
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_3_text));
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_3);
            } else if (level > 9 && level <= 12) {
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_4);
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_4_text));
            } else if (level > 12 && level <= 15) {
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_5);
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_5_text));
            } else {
                textView.setBackgroundResource(R.drawable.drawable_werewolf_user_active_level_6);
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.werewolf_user_active_level_6_text));
            }
            textView.setOnClickListener(v -> {
                WebViewActivity.load(textView.getContext(), URLManager.GetDefaultURL() + "/rank_instruction?access_token="
                        + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
