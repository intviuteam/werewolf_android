package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.R;

/**
 * game_werewolf
 * 2017/12/2 下午5:36
 * Mystery
 */

public class WereWolfHintDialog extends WereWolfBaseDialog {

    private TextView mTextView;
    private boolean enableLeftWhenCounting;
    private boolean enableRightWhenCounting;


    public WereWolfHintDialog(@NonNull Context context) {
        super(context);
        View view = View.inflate(context, R.layout.layout_werewolf_dialog_hint, null);
        mTextView = (TextView) view.findViewById(R.id.id_werewolf_dialog_hint_message);
        setDialogContentView(view);
        setButtonType(TWO_BUTTON);
    }

    public WereWolfHintDialog setHintTitle(String title) {
        setTitle(title);
        return this;
    }

    public WereWolfHintDialog setHintMessage(String msg) {
        mTextView.setText(msg);
        return this;
    }

    public WereWolfHintDialog setHintButtonType(int type) {
        setButtonType(type);
        return this;
    }

    public WereWolfHintDialog setHintLeftButtonText(String text) {
        if (getTwoButtonLeft() != null) {
            getTwoButtonLeft().setText(text);
        }
        return this;
    }

    public WereWolfHintDialog setHintRightButtonText(String text) {
        if (getTwoButtonRight() != null) {
            getTwoButtonRight().setText(text);
        }
        return this;
    }

    public WereWolfHintDialog setLeftOnClick(View.OnClickListener onClick) {
        if (getTwoButtonLeft() == null) {
            return this;
        }
        getTwoButtonLeft().setOnClickListener(v -> {
            if (isCounting() && !this.enableLeftWhenCounting) {
                return;
            }
            if (onClick != null) {
                onClick.onClick(v);
            }
        });
        return this;
    }

    public WereWolfHintDialog setRightOnClick(View.OnClickListener onClick) {
        if (getTwoButtonRight() == null) {
            return this;
        }
        getTwoButtonRight().setOnClickListener(v -> {
            if (isCounting() && !this.enableRightWhenCounting) {
                return;
            }
            if (onClick != null) {
                onClick.onClick(v);
            }
        });
        return this;
    }

    public WereWolfHintDialog setDismissWhenCountDownFinish(boolean flag) {
        setDismissWhenFinish(flag);
        return this;
    }

    public WereWolfHintDialog setEnableLeftWhenCounting(boolean flag) {
        this.enableLeftWhenCounting = flag;
        return this;
    }

    public WereWolfHintDialog setEnableRightWhenCounting(boolean flag) {
        this.enableRightWhenCounting = false;
        return this;
    }

    public WereWolfHintDialog setHintDismissWhenFinish(boolean flag) {
        setDismissWhenFinish(flag);
        return this;
    }

    public WereWolfHintDialog setActionWhenCountDownFinish(Runnable runnable) {
        setCountDownFinish(runnable);
        return this;
    }

    public WereWolfHintDialog(@NonNull Context context,
                              String title,
                              String message,
                              View.OnClickListener right,
                              View.OnClickListener left,
                              boolean enableLeftWhenCounting,
                              boolean enableRightWhenCounting) {
        this(context);

        setTitle(title);

        setHintMessage(message);

        setEnableLeftWhenCounting(enableLeftWhenCounting);

        setEnableRightWhenCounting(enableRightWhenCounting);

        setDismissWhenFinish(false);

        setLeftOnClick(left);

        setRightOnClick(right);

        if (!enableRightWhenCounting) {
            getTwoButtonRight().setBackgroundResource(R.drawable.drawable_werewolf_confirm_button_press);
            getTwoButtonRight().setEnabled(false);
        }

        if (!enableLeftWhenCounting) {
            getTwoButtonRight().setBackgroundResource(R.drawable.drawable_werewolf_cancel_button_press);
            getTwoButtonRight().setEnabled(false);
        }

        setCountDownFinish(() -> {

            getTwoButtonRight().setBackgroundResource(R.drawable.drawable_werewolf_confirm_button);
            getTwoButtonRight().setEnabled(true);

            getTwoButtonLeft().setBackgroundResource(R.drawable.drawable_werewolf_cancel_button);
            getTwoButtonLeft().setEnabled(true);

        });
    }

    public WereWolfHintDialog(@NonNull Context context,
                              String title,
                              String message,
                              View.OnClickListener right,
                              View.OnClickListener left
    ) {
        this(context, title, message, right, left, true, false);
    }
}
