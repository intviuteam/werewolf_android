package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;
import orangelab.project.game.utils.WereWolfString;

/**
 * game_werewolf
 * 2017/11/30 下午6:44
 * Mystery
 */

public class WereWolfHandOverSheriffResultDialog extends WereWolfBaseDialog {

    public WereWolfHandOverSheriffResultDialog(@NonNull Context context, WereWolfSelectMember from, WereWolfSelectMember to) {
        super(context);
        View view = View.inflate(context, R.layout.layout_werewolf_dialog_handover_sheriff, null);
        WereWolfSelectMemberHolder holder = new WereWolfSelectMemberHolder(view);
        holder.showAsSelectMember(to);

        TextView leftView = (TextView) view.findViewById(R.id.id_werewolf_handover_from);
        TextView toView = (TextView) view.findViewById(R.id.id_werewolf_handover_to);

        leftView.setText(Integer.toString(from.memberPosition));
        toView.setText(Integer.toString(to.memberPosition));
        setTitle(WereWolfString.getString(R.string.sheriff_associate_title));
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            dismiss();
        });
    }
}
