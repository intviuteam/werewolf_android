package orangelab.project.game.config;

import android.text.TextUtils;
import android.view.View;

import orangelab.project.common.PersonalDataHelper;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import com.networktoolkit.transport.Constant;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/9/14 下午2:28
 * Mystery
 */

public class GameRuleConfig {

    private static volatile boolean canCutSpeaker = true;

    public static boolean isCanCutSpeaker() {
        return canCutSpeaker;
    }

    public static void setCanCutSpeaker(boolean canCutSpeaker) {
        GameRuleConfig.canCutSpeaker = canCutSpeaker;
    }

    public static boolean IsWolfTeam(String role) {
        if (TextUtils.equals(role, Constant.ROLE_WOLF) || TextUtils.equals(role, Constant.ROLE_WEREWOLF_KING)
                || TextUtils.equals(role, WereWolfConstant.ROLE_DEMON) || TextUtils.equals(role, WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
            return true;
        } else {
            return false;
        }
    }

    public static int GetMuteAllAvailable() {
        return GameRuleConfig.isCanCutSpeaker() ? View.VISIBLE : View.GONE;
    }

    public static boolean IsGameRuleActionAboutSelf(String type, JSONObject payload) {
        boolean result = false;
        if (TextUtils.isEmpty(type)) {
            return result;
        }
        try {
            if (TextUtils.equals(type, Constant.TYPE_JOIN) || TextUtils.equals(type, Constant.LEAVE)) {
                result = false;
            } else {
                if (payload.has(Constant.POSITION)) {
                    int position = RoomSocketEngineHelper.getPositionFromJSON(payload) + 1;
                    if (position == PersonalDataHelper.getPersonalData().getSelfPosition()) {
                        /**
                         * this message has Position and Position is MyselfPosition
                         */
                        result = true;
                    }
                } else {
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

}
