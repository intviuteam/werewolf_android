package orangelab.project.game.dialog;

import android.content.Context;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.engine.RoomSocketEngineHelper;

import java.util.List;

/**
 * game_werewolf
 * 2017/11/29 下午3:31
 * Mystery
 * 默认 选择 一个 人
 */

public class WereWolfOnePersonSelectDialog extends WereWolfBaseSelectDialog {

    public WereWolfOnePersonSelectDialog(
            Context context, List<WereWolfSelectMember> gameMembers, String title, String type, int buttonType) {
        super(context, gameMembers, 1);
        if (buttonType == ONE_BUTTON) {
            initViewOneButton(title, type);
        } else if (buttonType == TWO_BUTTON) {
            initViewTwoButton(title, type);
        }
    }

    private void initViewOneButton(String title, String type) {
        setTitle(title);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            if (selectIslegal()) {
                RoomSocketEngineHelper.sendPositionMessage(type, getRealPosition().get(0));
                dismiss();
            }
        });

    }

    private void initViewTwoButton(String title, String type) {
        setTitle(title);
        setButtonType(TWO_BUTTON);
        getTwoButtonLeft().setOnClickListener(v -> {
            RoomSocketEngineHelper.sendEmptyType(type);
            dismiss();
        });
        getTwoButtonRight().setOnClickListener(v -> {
            if (selectIslegal()) {
                RoomSocketEngineHelper.sendPositionMessage(type, getRealPosition().get(0));
                dismiss();
            }
        });
    }


    protected boolean selectIslegal() {
        List<Integer> result = getRealPosition();
        if (result != null && result.size() == 1) {
            return true;
        } else {
            return false;
        }
    }
}
