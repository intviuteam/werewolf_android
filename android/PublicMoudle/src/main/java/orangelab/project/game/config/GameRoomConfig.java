package orangelab.project.game.config;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;

import com.R;
import com.toolkit.action.Filter;
import com.networktoolkit.transport.Constant;
import com.datasource.GlobalUserState;

import orangelab.project.common.utils.MessageUtils;

/**
 * 根据房间类型 返回不同的配置
 */

public class GameRoomConfig {


    public static int GetLeftRoomSecondsByMembers(int members) {
        if (members >= 1 && members <= 3) {
            return 0;
        }
        if (members >= 4 && members <= 6) {
            return 3;
        }
        if (members >= 7 && members <= 12) {
            return 5;
        }
        return 0;
    }


    /**
     * 根据房间类型返回提示框文字颜色
     */
    public static int GetHintPopTextColor(String type) {
        if (TextUtils.isEmpty(type)) {
            return Color.WHITE;
        }
        switch (type) {
            case Constant.GAME_TYPE_PRE_SIMPLE:
            case Constant.GAME_TYPE_PRE_SIMPLE_NEW:

                return Color.parseColor("#7c4e0e");
            case Constant.GAME_TYPE_SIMPLE:
            case Constant.GAME_TYPE_NORMAL:
            case Constant.GAME_TYPE_NORMAL_GUARD:
            case Constant.GAME_TYPE_HIGH_KING:
            case Constant.GAME_TYPE_SIMPLE_6:
            case Constant.GAME_TYPE_SIMPLE_9:
            case Constant.GAME_TYPE_SIMPLE_10:

                return Color.WHITE;
        }
        return Color.WHITE;
    }

    /**
     * 根据房间类型返回提示框背景颜色
     */
    public static int GetHintPopBgColor(String type) {
        if (TextUtils.isEmpty(type)) {
            return Color.parseColor("#E7189780");
        }
        switch (type) {
            case Constant.GAME_TYPE_PRE_SIMPLE:
            case Constant.GAME_TYPE_PRE_SIMPLE_NEW:

                return Color.parseColor("#fda929");
            case Constant.GAME_TYPE_SIMPLE:
            case Constant.GAME_TYPE_NORMAL:
            case Constant.GAME_TYPE_NORMAL_GUARD:
            case Constant.GAME_TYPE_HIGH_KING:
            case Constant.GAME_TYPE_SIMPLE_6:
            case Constant.GAME_TYPE_SIMPLE_9:
            case Constant.GAME_TYPE_SIMPLE_10:

                return Color.parseColor("#E7189780");
        }
        return Color.parseColor("#E7189780");
    }

    /**
     * 是否是新手房
     */
    public static boolean IsPreSimple(String type) {
        return TextUtils.equals(type, Constant.GAME_TYPE_PRE_SIMPLE) || TextUtils.equals(type, Constant.GAME_TYPE_PRE_SIMPLE_NEW);
    }

    /**
     * 根据房间类型发言时候的边框颜色
     */
    public static int GetMyTurnShape(String type) {
        if (TextUtils.isEmpty(type)) {
            return R.drawable.member_myturn_shape;
        }
        switch (type) {
            case Constant.GAME_TYPE_PRE_SIMPLE:
            case Constant.GAME_TYPE_PRE_SIMPLE_NEW:
                return R.drawable.member_myturn_shape_pre_smple;
            case Constant.GAME_TYPE_NORMAL:
            case Constant.GAME_TYPE_NORMAL_GUARD:
                return R.drawable.member_myturn_shape_pre_smple;
            case Constant.GAME_TYPE_SIMPLE:
            case Constant.GAME_TYPE_SIMPLE_6:
            case Constant.GAME_TYPE_SIMPLE_9:
            case Constant.GAME_TYPE_SIMPLE_10:
                return R.drawable.member_myturn_shape_pre_smple;
            case Constant.GAME_TYPE_HIGH_KING:
                return R.drawable.member_myturn_shape_pre_smple;
        }
        return R.drawable.member_myturn_shape;
    }

    /**
     * 根据房间类型返回是否可以关闭座位
     */
    public static boolean CloseSitEnable(Filter filter) {
        String curGameType = GlobalUserState.getGlobalState().getCurGameType();
        switch (curGameType) {
            case Constant.GAME_TYPE_SIMPLE:
                /**
                 * 简单模式老版本 11 12 位置不能锁
                 */
                if (filter != null) {
                    return filter.filter();
                } else {
                    return true;
                }
            case Constant.GAME_TYPE_PRE_SIMPLE:
            case Constant.GAME_TYPE_PRE_SIMPLE_NEW:
            case Constant.GAME_TYPE_NORMAL:
            case Constant.GAME_TYPE_NORMAL_GUARD:
            case Constant.GAME_TYPE_HIGH_KING:
            case Constant.GAME_TYPE_SIMPLE_6:
            case Constant.GAME_TYPE_SIMPLE_9:
            case Constant.GAME_TYPE_SIMPLE_10:
                /**
                 * 新手房，6人守卫模式不再可以锁位置
                 */
                return false;
            default:
                return true;
        }
    }

    /**
     * 获取房间的类型描述
     *
     * @return
     */
    public static String getGameTypeDescription() {
        String curGameType = GlobalUserState.getGlobalState().getCurGameType();
        switch (curGameType) {
            case Constant.GAME_TYPE_PRE_SIMPLE_NEW:
                return MessageUtils.getString(R.string.string_game_type_6_hunk);
            case Constant.GAME_TYPE_PRE_SIMPLE:
                return MessageUtils.getString(R.string.string_game_type_6_10_normal);
            case Constant.GAME_TYPE_SIMPLE_6:
                return MessageUtils.getString(R.string.string_game_type_6_guard);
            case Constant.GAME_TYPE_SIMPLE_9:
                return MessageUtils.getString(R.string.string_game_type_9_kill_border);
            case Constant.GAME_TYPE_SIMPLE_10:
                return MessageUtils.getString(R.string.string_game_type_10_kill_city);
            case Constant.GAME_TYPE_NORMAL_GUARD:
                return MessageUtils.getString(R.string.string_game_type_12_guard);
            case Constant.GAME_TYPE_NORMAL:
                return MessageUtils.getString(R.string.string_game_type_12_love);
            case Constant.GAME_TYPE_HIGH_KING:
                return MessageUtils.getString(R.string.string_game_type_12_high_king);
            case Constant.GAME_TYPE_SIMPLE:
                return "";
            default:
                return "";
        }
    }
}
