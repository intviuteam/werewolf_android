package orangelab.project.game.dialog;

import android.content.Context;
import android.view.View;

import com.R;

import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/12/8 下午8:43
 * Mystery
 */

public class WereWolfSheriffSpeechOrderDialog extends WereWolfBaseDialog {

    public WereWolfSheriffSpeechOrderDialog(Context context, WereWolfSelectMember left, WereWolfSelectMember right) {
        super(context);

        setTitle(MessageUtils.getString(R.string.choose_speech_direction));

        View view = View.inflate(context, R.layout.layout_werewolf_sheriff_speech_order, null);

        View leftView = view.findViewById(R.id.id_werewolf_left_member);

        View rightView = view.findViewById(R.id.id_werewolf_right_member);

        WereWolfSelectMemberHolder holderLeft = new WereWolfSelectMemberHolder(leftView);
        holderLeft.showAsSelectMember(left);

        WereWolfSelectMemberHolder holderRight = new WereWolfSelectMemberHolder(rightView);
        holderRight.showAsSelectMember(right);

        setDialogContentView(view);
        setButtonType(TWO_BUTTON);

        getTwoButtonLeft().setText(MessageUtils.getString(R.string.left_speech));
        getTwoButtonLeft().setBackgroundResource(R.drawable.drawable_werewolf_confirm_button);
        getTwoButtonLeft().setTextColor(getContext().getResources().getColor(android.R.color.white));
        getTwoButtonLeft().setOnClickListener(v -> {
            RoomSocketEngineHelper.sendSpeechDirection(Constant.LEFT);
            dismiss();
        });

        getTwoButtonRight().setText(MessageUtils.getString(R.string.right_speech));
        getTwoButtonRight().setBackgroundResource(R.drawable.drawable_werewolf_confirm_button);
        getTwoButtonLeft().setTextColor(getContext().getResources().getColor(android.R.color.white));
        getTwoButtonRight().setOnClickListener(v -> {
            RoomSocketEngineHelper.sendSpeechDirection(Constant.RIGHT);
            dismiss();
        });

    }
}
