package orangelab.project.game.dialog;

import android.content.Context;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/6 下午7:42
 * Mystery
 */

public class WereWolfProtectDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfProtectDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.werewolf_game_protect_title), Constant.PROTECT, ONE_BUTTON);
    }
}
