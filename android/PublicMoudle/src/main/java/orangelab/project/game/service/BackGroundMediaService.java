package orangelab.project.game.service;

import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import orangelab.project.common.GlobalLanguage;

import com.R;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.utils.IntentDataHelper;

import com.androidtoolkit.PLog;

import cn.intviu.service.Service;
import orangelab.project.game.WereWolfConstant;

/**
 * Author:1727
 * 2017/3/20
 */

public class BackGroundMediaService extends Service {
    private static final String TAG = "BackGroundMediaService";
    private MediaPlayer mediaPlayer = null;
    private String curType = null;
    private HandlerThread handlerThread = null;
    private Handler handler = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent != null) {
            if (handlerThread == null) {
                handlerThread = new HandlerThread(TAG);
                handlerThread.start();
                handler = new Handler(handlerThread.getLooper());
                PLog.i(TAG, "onStartCommand: create HandlerThread");
            }
            final int dur = IntentDataHelper.getMediaDurTime(intent);
            if (handler != null) {
                handler.post(() -> {
                    PLog.i(TAG, "run: post Runnable");
                    curType = IntentDataHelper.getMediaType(intent);
                    if (TextUtils.equals(curType, Constant.STOP_MEDIA)) {
                        PLog.i(TAG, "run: stop media");
                        release();
                    }
                    if (curType.equals(Constant.LEAVE)) {
                        PLog.i(TAG, "run: Leaving room");
                        release();
                        leave();
                    }
                    if (!TextUtils.isEmpty(curType)) {
                        PLog.i(TAG, "run: play media");
                        playMedia(curType, dur);
                    }
                });
            }
        }
        return START_STICKY;
    }

    private void playMedia(String type, int seconds) {
        if (mediaPlayer != null) {
            release();
        }
        playMedia(transferType(type), seconds);
    }

    private int transferType(String type) {
        if (type.equals(Constant.SUNUP)) {
            return GlobalLanguage.GetSunUp();
        } else if (type.equals(Constant.SUNUP_WITH_SHERIFF)) {
            return R.raw.media_sunup_with_sheriff;
        } else if (type.equals(Constant.SUNSET_WITH_SEER)) {
            return GlobalLanguage.GetSunSetWithSeer();
        } else if (type.equals(Constant.SUNSET_WITHOUT_SEER)) {
            return GlobalLanguage.GetSunSetWithOutSeer();
        } else if (type.equals(Constant.SUNSET_NORMAL_PEOPLE)) {
            return GlobalLanguage.GetSunSetNormalPeople();
        } else if (type.equals(Constant.GAMEOVER_PEOPLE_WIN)) {
            return GlobalLanguage.GetGameOverPeopleWin();
        } else if (type.equals(Constant.GAMEOVER_CUPID_WIN)) {
            return GlobalLanguage.GetGameOverCupidWin();
        } else if (type.equals(Constant.GAMEOVER_WOLF_WIN)) {
            return GlobalLanguage.GetGameOverWolfWin();
        } else if (type.equals(Constant.LINK)) {
            return GlobalLanguage.GetLink();
        } else if (type.equals(Constant.START)) {
            return GlobalLanguage.GetStart();
        } else if (type.equals(Constant.VOTE)) {
            return GlobalLanguage.GetVote();
        } else if (type.equals(Constant.ASSIGNED_ROLE)) {
            return GlobalLanguage.GetAssginRole();
        } else if (type.equals(Constant.SUNSET_ONLY_SEER)) {
            return GlobalLanguage.GetSunSetOnlySeer();
        } else if (type.equals(Constant.SPEECH_DANGER_TIME)) {
            return R.raw.media_speech_danger_time;
        } else if (type.equals(Constant.MEDIA_RECEIVE_GOLD)) {
            return R.raw.media_receive_gold;
        } else if (type.equals(Constant.BOOM)) {
            return GlobalLanguage.GetBoom();
        } else if (type.equals(Constant.MEDIA_SEER_WOLF)) {
            return GlobalLanguage.GetSunSetSeerWolf();
        } else if (type.equals(Constant.MEDIA_SEER_WITCH_WOLF)) {
            return GlobalLanguage.GetSunSetSeerWitchWolf();
        } else if (type.equals(Constant.MEDIA_SEER_GUARD_WOLF)) {
            return GlobalLanguage.GetSunSetSeerGuardWolf();
        } else if (type.equals(Constant.MEDIA_SEER_GUARD_WITCH_WOLF)) {
            return GlobalLanguage.GetSunSetSeerGuardWitchWolf();
        } else if (type.equals(Constant.SOUND_APPLAUSE)) {
            return R.raw.voice_game_applause;
        } else if (type.equals(WereWolfConstant.FIGHT_AWAY_RESULT)) {
            return R.raw.media_fight_away_result;
        }
        return 0;
    }

    private void release() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            PLog.i(TAG, "release: MediaPlayer release");
        }
    }

    private void leave() {
        if (handlerThread != null) {
            handlerThread.quit();
            handlerThread = null;
            handler = null;
        }
    }

    private void playMedia(int resourceId, int seconds) {
        try {
            //AssetFileDescriptor file = getResources().openRawResourceFd(resourceId);
            if (resourceId <= 0) {
                return;
            }
            PLog.i(TAG, "playMedia: " + hashCode());
            try {
                if (mediaPlayer == null) {
                    PLog.i(TAG, "playMedia: mediaPlayer ==null create one");
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), resourceId);
//                    mediaPlayer.setDataSource(file.getFileDescriptor(),
//                            file.getStartOffset(), file.getLength());
//                    file.close();
//                    mediaPlayer.setVolume(0.50f, 0.50f);
                    mediaPlayer.setOnPreparedListener(mp -> {
                        mediaPlayer = mp;
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mediaPlayer.start();
                    });

                }
            } catch (Exception e) {
                e.printStackTrace();
                release();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
