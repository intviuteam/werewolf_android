package orangelab.project.game.event;

import org.json.JSONObject;

/**
 * Author:1727
 * 2017/3/6
 */

public class GameEvent {

//    {"type":"update_master","payload":{"position":0}}

    public static class MemberComingEvent {
        private int number;
        private String userId;
        private String userImage;
        private String userName;

        public MemberComingEvent(int number, String userId, String userName) {
            this.number = number;
            this.userId = userId;
            this.userName = userName;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public int getNumber() {
            return number;
        }

        public String getUserId() {
            return userId;
        }

        public String getUserName() {
            return userName;
        }
    }

    public static class UpdateRoomOwnerEvent {
        private int number;

        public UpdateRoomOwnerEvent(int number) {
            this.number = number;
        }

        public int getNumber() {
            return number;
        }
    }

    public static class GameMemberPreparedEvent {
        private boolean isPrepared = false;
        private int position;

        public GameMemberPreparedEvent(boolean isPrepared, int position) {
            this.isPrepared = isPrepared;
            this.position = position;
        }

        public boolean isPrepared() {
            return isPrepared;
        }

        public void setPrepared(boolean prepared) {
            isPrepared = prepared;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

    public static class GameStartEvent {

    }

    public static class GameOverEvent {

    }

    public static class IsWolfEvent {
        private int number;
        private int king = -1;

        public IsWolfEvent(int number, int king) {
            this.number = number;
            this.king = king;
        }

        public int getKing() {
            return king;
        }

        public int getNumber() {
            return number;
        }
    }

    public static class SpeechEvent {
        private int number;

        public SpeechEvent(int number) {
            this.number = number;
        }

        public int getNumber() {
            return number;
        }
    }

    public static class GameSelectMemberEvent {

        private String type;
        private int position;

        public GameSelectMemberEvent(int position, String type) {
            this.position = position;
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public int getPosition() {
            return position;
        }
    }

    public static class GameOverEventInner {

    }

    public static class MySelfIsDeadEvent {

    }

    public static class KickOutEvent {
        private int position = -1;

        public KickOutEvent(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

    public static class MemberJoinEvent {
        private JSONObject payload;

        public MemberJoinEvent(JSONObject payload) {
            this.payload = payload;
        }

        public JSONObject getPayload() {
            return payload;
        }
    }

    public static class MemberLeaveEvent {
        private boolean isDead;
        private String userName;
        private JSONObject payload;

        public MemberLeaveEvent(boolean isDead, String userName, JSONObject payload) {
            this.isDead = isDead;
            this.userName = userName;
            this.payload = payload;
        }

        public boolean isDead() {
            return isDead;
        }

        public String getUserName() {
            return userName;
        }

        public JSONObject getPayload() {
            return payload;
        }
    }

    public static class SendMessageEvent {

        public String message;

        public SendMessageEvent(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    /**
     * 代表用户做了任何操作
     */
    public static class ExecEvent {

    }
}
