package orangelab.project.game.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import orangelab.project.game.popwindow.ChooseTypePopWindow;
import orangelab.project.common.adapter.ChooseTypeAdapter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lloydfinch on 25/08/2017.
 */

public class ChooseTypeButton extends Button {

    private Context mContext;
    private ChooseTypeAdapter.OnChooseTypeListener onChooseTypeListener;
    private List<PopItem> popItems;

    public ChooseTypeButton(Context context) {
        this(context, null);

    }

    public ChooseTypeButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChooseTypeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }


    private void init() {
        this.setOnClickListener((view) -> {
            /**
             * 获取按钮在屏幕位置
             */
            int[] location = new int[2];
            getLocationOnScreen(location);
            ChooseTypePopWindow popWindow = new ChooseTypePopWindow(mContext, popItems);
            popWindow.setOnChooseTypeListener(onChooseTypeListener);
            /**
             * 根据按钮在屏幕的位置 展示提示
             */
            popWindow.showAtLocation(this, Gravity.NO_GRAVITY, location[0], location[1] - popWindow.getPopHeight());
        });
    }

    public void setPopItems(List<PopItem> popItems) {
        this.popItems = popItems;
    }

    public void setOnChooseTypeListener(ChooseTypeAdapter.OnChooseTypeListener onChooseTypeListener) {
        this.onChooseTypeListener = onChooseTypeListener;
    }

    public static class PopItem implements Serializable {
        public int id;
        public String msg;

        public PopItem() {
            super();
        }

        public PopItem(int id, String msg) {
            this.id = id;
            this.msg = msg;
        }
    }
}
