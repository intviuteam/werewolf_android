package orangelab.project.game.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.R;
import com.networktoolkit.transport.Constant;

import orangelab.project.game.WereWolfConstant;

/**
 * game_werewolf
 * 2018/7/10 上午10:58
 * Mystery
 */
public class WereWolfMemberTagUtils {

    public static void shadowTag(ImageView tag, boolean isLeft, boolean shadow) {
        if (tag.getBackground() != null) {
            /**
             * 说明Tag已经展示了
             */
            if (isLeft) {
                if (shadow) {
                    tag.setImageResource(R.mipmap.ico_werewolf_member_left_tag_layer);
                } else {
                    tag.setImageDrawable(null);
                }
            } else {
                if (shadow) {
                    tag.setImageResource(R.mipmap.ico_werewolf_member_right_tag_layer);
                } else {
                    tag.setImageDrawable(null);
                }
            }
        } else {
            tag.setImageDrawable(null);
        }

    }

    public static void clearTag(ImageView tag) {
        tag.setBackground(null);
    }

    public static void setRoomMasterTag(ImageView tag, boolean isLeft) {
        if (isLeft) {
            tag.setBackgroundResource(R.mipmap.ico_werewolf_member_left_tag_master);
        } else {
            tag.setBackgroundResource(R.mipmap.ico_werewolf_member_right_tag_master);
        }
    }

    public static void setRoleTag(ImageView tag, boolean isLeft, String role) {
        if (TextUtils.isEmpty(role)) return;
        int resource = getRoleResource(isLeft, role);
        if (resource == -1) {
            return;
        } else {
            tag.setBackgroundResource(resource);
        }
        return;
    }

    public static int getRoleResource(boolean isLeft, String role) {
        try {
            if (role.equals(Constant.ROLE_SEER)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_seer : R.mipmap.ico_werewolf_member_right_tag_seer;
            } else if (role.equals(Constant.ROLE_WOLF)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_werewolf : R.mipmap.ico_werewolf_member_right_tag_werewolf;
            } else if (role.equals(Constant.ROLE_HUNTER)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_hunter : R.mipmap.ico_werewolf_member_right_tag_hunter;
            } else if (role.equals(Constant.ROLE_WITCH)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_witch : R.mipmap.ico_werewolf_member_right_tag_witch;
            } else if (role.equals(Constant.ROLE_PEOPLE)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_people : R.mipmap.ico_werewolf_member_right_tag_people;
            } else if (role.equals(Constant.ROLE_CUPID)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_cupid : R.mipmap.ico_werewolf_member_right_tag_cupid;
            } else if (role.equals(Constant.ROLE_GUARD)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_guard : R.mipmap.ico_werewolf_member_right_tag_guard;
            } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_wolfking : R.mipmap.ico_werewolf_member_right_tag_wolfking;
            } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_demon : R.mipmap.ico_werewolf_member_right_tag_demon;
            } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_magic : R.mipmap.ico_werewolf_member_right_tag_magic;
            } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_blackwolf : R.mipmap.ico_werewolf_member_right_tag_blackwolf;
            } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
                return isLeft ? R.mipmap.ico_werewolf_member_left_tag_knight : R.mipmap.ico_werewolf_member_right_tag_knight;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}
