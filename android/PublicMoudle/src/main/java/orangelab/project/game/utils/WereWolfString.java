package orangelab.project.game.utils;

import com.androidtoolkit.Environment;
import com.R;

import orangelab.project.game.WereWolfConstant;

/**
 * game_werewolf
 * 2017/11/27 下午5:55
 * Mystery
 */

public class WereWolfString {

    public static String getString(int stringId) {
        return Environment.GetAppContext().getString(stringId);
    }

    public static String getString(int stringId, Object... data) {
        return Environment.GetAppContext().getString(stringId, data);
    }

    public static String getRoomSettingChooseRoleType(String type) {
        String result = "";
        switch (type) {
            case WereWolfConstant.ROLE_LLLW:
                result = getString(R.string.werewolf_role_lllw);
                break;
            case WereWolfConstant.ROLE_LLLE:
                result = getString(R.string.werewolf_role_llle);
                break;
            case WereWolfConstant.ROLE_YNLM:
                result = getString(R.string.werewolf_role_ynlm);
                break;
            case WereWolfConstant.ROLE_YNLS:
                result = getString(R.string.werewolf_role_ynls);
                break;
            case WereWolfConstant.ROLE_YNQS:
                result = getString(R.string.werewolf_role_ynqs);
                break;
            case WereWolfConstant.ROLE_LLLH:
                result = getString(R.string.werewolf_role_lllh);
                break;
        }
        return result;
    }
}
