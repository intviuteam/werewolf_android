package orangelab.project.game.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.androidtoolkit.view.ScreenUtils;
import com.R;
import com.androidtoolkit.view.SwitchButton;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.config.WereWolfConfig;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.game.utils.WereWolfToolKit;
import orangelab.project.common.hint.SpinnerPopWindow;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static orangelab.project.common.utils.MessageUtils.*;

/**
 * game_werewolf
 * 2017/12/5 下午7:11
 * Mystery
 */

public class WereWolfRoomSettingDialog extends WereWolfBaseDialog {
    private static final String TAG = "RoomPasswordDialog";

    private SwitchButton passwordSwitch;
    private EditText editText;
    private View passwordContainer;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TextView text4;

    private String password;

    private View limitTitle;
    private View chooseGod;
    private View chooseWolf;
    private TextView spinnerLimit;
    private TextView spinnerChooseGod;
    private TextView spinnerChooseWolf;


    private SpinnerPopWindow<Integer> mLevelLimitSpinner;
    private SpinnerPopWindow<String> mChooseGodSpinner;
    private SpinnerPopWindow<String> mChooseWolfSpinner;

    private List<String> mChooseGodList = new ArrayList<>(0);
    private List<String> mChooseWolfList = new ArrayList<>(0);

    {
        mChooseGodList.add(WereWolfConstant.ROLE_YNLM);
        mChooseGodList.add(WereWolfConstant.ROLE_YNLS);
        mChooseGodList.add(WereWolfConstant.ROLE_YNQS);

        mChooseWolfList.add(WereWolfConstant.ROLE_LLLE);
        mChooseWolfList.add(WereWolfConstant.ROLE_LLLW);
        mChooseWolfList.add(WereWolfConstant.ROLE_LLLH);
    }

    public WereWolfRoomSettingDialog(WereWolfGameContext context, boolean isVoiceRoom) {
        super(context.getAndroidContext());
        View view = LayoutInflater.from(context.getAndroidContext()).inflate(R.layout.layout_werewolf_room_setting, null);
        this.password = context.getPassword();
        editText = (EditText) view.findViewById(R.id.editText);
        passwordContainer = view.findViewById(R.id.password_container);
        passwordSwitch = (SwitchButton) view.findViewById(R.id.password_switch);
        text1 = (TextView) view.findViewById(R.id.text1);
        text2 = (TextView) view.findViewById(R.id.text2);
        text3 = (TextView) view.findViewById(R.id.text3);
        text4 = (TextView) view.findViewById(R.id.text4);
        limitTitle = view.findViewById(R.id.level_limit);
        spinnerLimit = (TextView) view.findViewById(R.id.level_limit_choose);

        chooseGod = view.findViewById(R.id.choose_god);
        chooseWolf = view.findViewById(R.id.choose_wolf);
        spinnerChooseGod = (TextView) view.findViewById(R.id.choose_god_spinner);
        spinnerChooseWolf = (TextView) view.findViewById(R.id.choose_wolf_spinner);
        if (isVoiceRoom) {
            limitTitle.setVisibility(View.GONE);
            spinnerLimit.setVisibility(View.GONE);
        }
        EnterRoomResult.RoomConfig config = context.getRoomConfig();
        if (config == null) {
            limitTitle.setVisibility(View.GONE);
            spinnerLimit.setVisibility(View.GONE);
        } else {

            if (WereWolfConfig.IsHighKingGame(context.getGameType())) {
                chooseGod.setVisibility(View.VISIBLE);
                chooseWolf.setVisibility(View.VISIBLE);
                spinnerChooseGod.setVisibility(View.VISIBLE);
                spinnerChooseWolf.setVisibility(View.VISIBLE);
                /**
                 * init choose type if need
                 */
                try {
                    String wereWolfType = config.high_king.werewolf_parts;
                    String godType = config.high_king.people_parts;
                    spinnerChooseWolf.setText(WereWolfString.getRoomSettingChooseRoleType(wereWolfType));
                    spinnerChooseGod.setText(WereWolfString.getRoomSettingChooseRoleType(godType));
                    spinnerChooseWolf.setTag(wereWolfType);
                    spinnerChooseGod.setTag(godType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                chooseGod.setVisibility(View.GONE);
                chooseWolf.setVisibility(View.GONE);
                spinnerChooseGod.setVisibility(View.GONE);
                spinnerChooseWolf.setVisibility(View.GONE);
            }

            mLevelLimitSpinner = new SpinnerPopWindow<Integer>(context.getAndroidContext(), config.level_limitations, (parent, view1, position, id) -> {
                mLevelLimitSpinner.dismiss();
                spinnerLimit.setTag(config.level_limitations.get(position).toString());
                spinnerLimit.setText(getLevelLimit(config.level_limitations.get(position).toString()));
            }, (item, textView) -> {
                textView.setText(MessageUtils.getLevelLimit(item.toString()));
            });

            mChooseGodSpinner = new SpinnerPopWindow<String>(context.getAndroidContext(), mChooseGodList, (parent, view12, position, id) -> {
                mChooseGodSpinner.dismiss();
                spinnerChooseGod.setTag(mChooseGodList.get(position));
                spinnerChooseGod.setText(WereWolfString.getRoomSettingChooseRoleType(mChooseGodList.get(position)));
            }, (item, textView) -> {
                textView.setText(WereWolfString.getRoomSettingChooseRoleType(item));
            });

            mChooseWolfSpinner = new SpinnerPopWindow<String>(context.getAndroidContext(), mChooseWolfList, (parent, view12, position, id) -> {
                mChooseWolfSpinner.dismiss();
                spinnerChooseWolf.setTag(mChooseWolfList.get(position));
                spinnerChooseWolf.setText(WereWolfString.getRoomSettingChooseRoleType(mChooseWolfList.get(position)));
            }, (item, textView) -> {
                textView.setText(WereWolfString.getRoomSettingChooseRoleType(item));
            });

            initSpinnerSkin(mChooseGodSpinner);
            initSpinnerSkin(mChooseWolfSpinner);
            initSpinnerSkin(mLevelLimitSpinner);

            spinnerChooseGod.setOnClickListener(v -> {
                mChooseGodSpinner.setWidth(spinnerChooseGod.getWidth());
                mChooseGodSpinner.showAsDropDown(spinnerChooseGod, 0, 0);
            });

            spinnerChooseWolf.setOnClickListener(v -> {
                mChooseWolfSpinner.setWidth(spinnerChooseWolf.getWidth());
                mChooseWolfSpinner.showAsDropDown(spinnerChooseWolf, 0, 0);
            });

            spinnerLimit.setOnClickListener(v -> {
                mLevelLimitSpinner.setWidth(spinnerLimit.getWidth());
                mLevelLimitSpinner.showAsDropDown(spinnerLimit, 0, 0);
            });

            Utils.runSafely(() -> {
                String levelLimit = Integer.toString(config.level);
                spinnerLimit.setText(getLevelLimit(levelLimit));
                spinnerLimit.setTag(levelLimit);
            });
        }
        initEditText(editText);
        setTitle(getString(R.string.setting));
        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        getOneButton().setOnClickListener(v -> {
            String pwd = editText.getText().toString();
            String level = (String) spinnerLimit.getTag();
            if (TextUtils.isEmpty(level)) {
                level = "";
            }

            String chooseGodType = (String) spinnerChooseGod.getTag();
            if (TextUtils.isEmpty(chooseGodType)) {
                chooseGodType = "";
            }

            String chooseWolfType = (String) spinnerChooseWolf.getTag();
            if (TextUtils.isEmpty(chooseWolfType)) {
                chooseWolfType = "";
            }

            WereWolfToolKit.UpdateRoomSetting(
                    level,
                    pwd,
                    context.isCanCutSpeak(),
                    chooseGodType,
                    chooseWolfType
            );

            try {
                /**
                 * 尝试隐藏键盘
                 */
                InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            dismiss();
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null) {
                    return;
                }
                String input = s.toString();
                Editable editable = editText.getText();
                int len = input.length();
                if (len > 4) {
                    int selEndIndex = Selection.getSelectionEnd(editable);
                    String str = editable.toString();
                    input = str.substring(0, 4);
                    editText.setText(input);
                    editable = editText.getText();
                    int newLen = editable.length();
                    if (selEndIndex > newLen) {
                        selEndIndex = editable.length();
                    }
                    Selection.setSelection(editable, selEndIndex);

                }
                handleInput(input);
            }

            @Override
            public void afterTextChanged(Editable s) {
                PLog.i(TAG, "afterTextChanged: ");
            }
        });
        passwordSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                passwordSwitch.setChecked(true);
                passwordContainer.setVisibility(View.VISIBLE);
            } else {
                passwordSwitch.setChecked(false);
                passwordContainer.setVisibility(View.GONE);
            }
            handleInput("");
            editText.setText("");
        });
        passwordContainer.setOnClickListener(v -> {
            editText.requestFocus();
            InputMethodManager imm =
                    (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
        });
        if (TextUtils.isEmpty(password)) {
            editText.setText("");
            passwordSwitch.setChecked(false);
            passwordContainer.setVisibility(View.GONE);
            handleInput("");
        } else {
            passwordSwitch.setChecked(true);
            passwordContainer.setVisibility(View.VISIBLE);
            handleInput(password);
            editText.setText(password);
        }
        editText.requestFocus();

    }

    private void initSpinnerSkin(SpinnerPopWindow spinnerPopWindow) {
        spinnerPopWindow.setTextColor("#73450d");
    }

    private void clearInput() {
        text1.setText("");
        text2.setText("");
        text3.setText("");
        text4.setText("");
    }

    private void handleInputInner(String input) {
        try {
            text1.setText(input.substring(0, 1));
            text2.setText(input.substring(1, 2));
            text3.setText(input.substring(2, 3));
            text4.setText(input.substring(3, 4));
        } catch (Exception e) {
            //ignore error
            e.printStackTrace();
        }
    }

    private void handleInput(String input) {
        clearInput();
        if (TextUtils.isEmpty(input)) {
        } else {
            if (input.length() > 4) {
                String result = input.substring(0, 4);
                handleInputInner(result);
            } else {
                handleInputInner(input);
            }

        }
    }

    private void initEditText(EditText editText) {
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

                return false;

            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {

                return false;

            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

                return false;

            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }

        });
        editText.setLongClickable(false);
    }
}
