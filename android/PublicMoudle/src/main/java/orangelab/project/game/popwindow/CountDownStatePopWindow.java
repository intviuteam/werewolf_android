package orangelab.project.game.popwindow;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.R;
import orangelab.project.common.utils.CountDownTaskManager;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/6/1 下午4:50
 * Mystery
 */

public class CountDownStatePopWindow extends PopupWindow {

    private TextView countDown;
    private CountDownTaskManager.CountDownTask countDownTask = null;

    public CountDownStatePopWindow(Context context) {
        super(context);
        View rootView = LayoutInflater.from(context).inflate(R.layout.layout_coundown_popwindow, null);
        countDown = (TextView) rootView.findViewById(R.id.pop_countdown);
        setContentView(rootView);
        setFocusable(false);
        setTouchable(false);
        setOutsideTouchable(false);
        setWidth(ScreenUtils.dip2px(120));
        setHeight(ScreenUtils.dip2px(120));
        setBackgroundDrawable(new BitmapDrawable(context.getResources()));
    }

    public void showPopWindow(View parent, int countDownTime) {
        if (!isShowing()) {
            showAtLocation(parent, Gravity.CENTER, 0, 0);
            if (countDown != null) {
                prepareCountDownTask(countDownTime);
                startCountDownTask();
            }
        }
    }

    private void showCountDown(int seconds) {
        if (countDown != null) {
            countDown.setText(seconds + "");
        }
    }

    private void prepareCountDownTask(int countDownTime) {
        if (countDownTask == null) {
            countDownTask = new CountDownTaskManager.CountDownTask(countDownTime, new CountDownTaskManager
                    .CountDownTaskListener() {
                @Override
                public void onTick(int seconds) {
                    showCountDown(seconds);
                }

                @Override
                public void onStart(int startSeconds) {
                    showCountDown(startSeconds);
                }

                @Override
                public void onFinish() {
                    showCountDown(0);
                    dismissPopWindow();
                }
            });
        }
    }

    private void startCountDownTask() {
        if (countDownTask != null) {
            countDownTask.startTask();
        }
    }

    public void dismissPopWindow() {
        try {
            if (isShowing()) {
                if (countDownTask != null) {
                    countDownTask.cancelTask();
                    countDownTask = null;
                }
                dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * 防止 已经Dismiss过之后 再次dimiss会出的崩溃问题，由于目前无法推断出合适dimiss会出错先在这里进行一个保护
             */


        }
    }
}
