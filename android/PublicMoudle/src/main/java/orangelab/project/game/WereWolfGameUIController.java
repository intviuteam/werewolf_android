package orangelab.project.game;

import android.content.Context;
import android.graphics.PointF;
import android.os.Build;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.exhibition.card.IWereWolfCardService;
import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.exhibition.gift.giftboard.GiftBoardEvent;
import orangelab.project.common.model.action.ServerActionFightAway;
import orangelab.project.common.model.action.ServerActionFightAwayResult;
import orangelab.project.common.model.action.ServerActionFightState;
import orangelab.project.game.component.GameMember;
import orangelab.project.game.dialog.WereWolfBoomAwayDialog;
import orangelab.project.game.dialog.WereWolfBoomDialog;
import orangelab.project.game.dialog.WereWolfCardCheckDialog;
import orangelab.project.game.dialog.WereWolfCardCheckResultDialog;
import orangelab.project.game.dialog.WereWolfDemonCheckDialog;
import orangelab.project.game.dialog.WereWolfDemonCheckResultDialog;
import orangelab.project.game.dialog.WereWolfExchangeDialog;
import orangelab.project.game.dialog.WereWolfFightAwayDialog;
import orangelab.project.game.dialog.WereWolfFightDialog;
import orangelab.project.game.dialog.WereWolfSeerCheckResultDialog;
import orangelab.project.game.dialog.WereWolfConfirmLoversDialog;
import orangelab.project.game.dialog.WereWolfCupidSelectDialog;
import orangelab.project.game.dialog.WereWolfGameOverDialog;
import orangelab.project.game.dialog.WereWolfHandOverDialog;
import orangelab.project.game.dialog.WereWolfHandOverSheriffResultDialog;
import orangelab.project.game.dialog.WereWolfIdentityDialog;
import orangelab.project.game.dialog.WereWolfPoisonDialog;
import orangelab.project.game.dialog.WereWolfProtectDialog;
import orangelab.project.game.dialog.WereWolfRobCardDialog;
import orangelab.project.game.dialog.WereWolfRunInElectionDialog;
import orangelab.project.game.dialog.WereWolfSaveDialog;
import orangelab.project.game.dialog.WereWolfSeerCheckDialog;
import orangelab.project.game.dialog.WereWolfSheriffElectedDialog;
import orangelab.project.game.dialog.WereWolfSheriffSpeechOrderDialog;
import orangelab.project.game.dialog.WereWolfTakeAwayDialog;
import orangelab.project.game.dialog.WereWolfVoteDialog;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.model.WereWolfExchange;
import orangelab.project.game.model.WereWolfExchangeResult;
import orangelab.project.game.model.WereWolfGameMember;
import orangelab.project.game.model.WereWolfGiftRebate;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.popwindow.CountDownStatePopWindow;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.game.utils.WereWolfToolKit;
import orangelab.project.game.view.WereWolfBackGroundSurfaceView;
import orangelab.project.game.view.WereWolfGameOperatingFloorView;
import orangelab.project.game.view.WereWolfGameMessageView;
import orangelab.project.game.view.WereWolfGameMemberView;
import orangelab.project.game.view.WereWolfGameTitleView;
import orangelab.project.game.view.WereWolfGameNoticeBoardView;
import orangelab.project.game.view.WereWolfKillView;
import orangelab.project.game.view.WereWolfGameSpeakPopWindow;
import orangelab.project.game.view.WereWolfGameTopLayer;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.model.AssginedRoleResult;
import orangelab.project.common.model.CheckResult;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.RestoreResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;
import orangelab.project.common.model.WakeToKillResult;
import orangelab.project.game.model.WereWolfGameSurvivorsInfo;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.Destroyable;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/11/27 下午7:39
 * Mystery
 * 游戏的View 控制器
 */

public class WereWolfGameUIController extends WereWolfGameProcess implements Destroyable, WereWolfGameProcessLifeCycle {
    private WereWolfGameContext mWereWolfGameContext;
    private Context mContext;
    private View mRootView;
    private WereWolfDialog mCurGameDialog = null;
    /**
     * 游戏的最上部分UI 例如卡片的展示，礼物特效的展示等等
     */
    private WereWolfGameTopLayer mWereWolfTopLayer = null;

    private WereWolfBackGroundSurfaceView mBackGroundSurfaceView = null;
    /**
     * 游戏的Title部分
     */
    private WereWolfGameTitleView mWereWolfTitleView = null;

    private WereWolfGameNoticeBoardView mWereWolfSystemBoardView = null;

    private WereWolfGameOperatingFloorView mWereWolfConsoleView = null;

    private WereWolfGameMessageView mWolfGameMessageView = null;

    private WereWolfGameSpeakPopWindow mWereWolfSpeakPopWindow = null;

    private SparseArray<WereWolfGameMemberView> mWolfGamePositionViewList = new SparseArray<>(0);

    private CountDownStatePopWindow countDownStatePopWindow = null;

    private boolean rnReportFlag;

    private WereWolfGameProcess mObserverProcess = new WereWolfGameProcess() {
        @Override
        public boolean handleGift(String giftType, String message, int from, int to, JSONObject jsonObject) {
            return handleGiftInner(giftType, message, from, to, jsonObject);
        }

        @Override
        public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
            initWereWolfBackGroundSunSet();
            return false;
        }

        @Override
        public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
            try {
                showBoomDialog(position, isWolfKing, duration, jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
            initWereWolfBackGroundSunUp();
            return false;
        }

        @Override
        public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
            try {
                showFightAwayResultDialog(fightAwayResult);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public boolean handleGameOver(GameOverResult gameOverResult) {
            initWereWolfBackGroundSunUp();
            return false;
        }
    };

    private void rootViewNegotiate(View rootView) {

        final float curHeight = ScreenUtils.getCurrentScreenHeight();
        final float curWidth = ScreenUtils.getCurrentScreenWidth();
        /**
         * 计算left 和 right的可视高度 这里会有一个宽度和高度的一个协商过程
         */
        View viewleft = mRootView.findViewById(R.id.members_left);
        View viewright = mRootView.findViewById(R.id.members_right);
        /**
         * 开始进行设定底部高度
         */
        View bottomContainer = mRootView.findViewById(R.id.id_werewolf_game_bottom_container);
        final float percentBottomHeight = 0.17f;
        ViewGroup.LayoutParams bottomParams = bottomContainer.getLayoutParams();
        bottomParams.height = (int) (curHeight * percentBottomHeight);

        /**
         * 开始进行玩家布局的协商高度
         */
        final float percentWidth = 0.19f;     //设定的宽度百分比
        final float radioWidth = 192;         //设定的标注宽度
        final float radioHeight = 230;        //设定的标注高度

        final int number = 6;
        /**
         * 可用宽度
         */
        int availableWidth = (int) (curWidth * percentWidth);
        int bottomHeight = (int) (curHeight * 0.17f);

        /**
         * 可用的高度
         */
        float availableHeight = curHeight - ScreenUtils.dip2px(80) - bottomHeight - ScreenUtils.dip2px(25);

        /**
         * 理想的高度
         */
        final float idealHeight = (availableWidth / radioWidth) * radioHeight * number;

        float resultWidth = 0;
        float resultHeight = 0;

        /**
         * 先进理想宽高的判断
         */
        if (idealHeight > availableHeight) {
            /**
             * 如果理想的高度比可用高度要大，那么最终的结果为可用高度
             */
            resultHeight = availableHeight;
            /**
             * 宽度需要重新计算
             */
            final float newIdealWidth = ((availableHeight / number) / radioHeight) * radioWidth;
            if (newIdealWidth > availableWidth) {
                resultWidth = availableWidth;
            } else {
                resultWidth = newIdealWidth;
            }
        } else {
            resultWidth = availableWidth;
            resultHeight = idealHeight;
        }

        ViewGroup.LayoutParams layoutParams1 = viewleft.getLayoutParams();
        layoutParams1.height = (int) resultHeight;
        layoutParams1.width = (int) resultWidth;

        ViewGroup.LayoutParams layoutParams2 = viewright.getLayoutParams();
        layoutParams2.height = (int) resultHeight;
        layoutParams2.width = (int) resultWidth;
    }


    public WereWolfGameUIController(WereWolfGameContext context) {
        mWereWolfGameContext = context;
        mWereWolfGameContext.addProcessLifeCycle(this);

        mContext = context.getAndroidContext();
        mRootView = context.getGameRootView();

        rootViewNegotiate(mRootView);

        mBackGroundSurfaceView = (WereWolfBackGroundSurfaceView) mRootView.findViewById(R.id.id_background);

        mWereWolfTopLayer = new WereWolfGameTopLayer(mRootView);

        mWereWolfTitleView = new WereWolfGameTitleView(mWereWolfGameContext, mRootView.findViewById(R.id.id_werewolf_game_title_container));

        mWereWolfSystemBoardView = new WereWolfGameNoticeBoardView(mWereWolfGameContext);

        mWereWolfConsoleView = new WereWolfGameOperatingFloorView(mWereWolfGameContext);

        mWolfGameMessageView = new WereWolfGameMessageView(mWereWolfGameContext);

        context.addService(IWereWolfCardService.WEREWOLF_CARD_SERVICE, mWereWolfTopLayer.getGiftAnimationView().getCardManager());

        //initWereWolfWindow();

        initWereWolfMembers();

        initWereWolfBackGround();

        initListener();

        if (mWereWolfGameContext.isObserver()) {
            mWereWolfGameContext.registerProcessListener(mObserverProcess);
        } else {
            mWereWolfGameContext.registerProcessListener(this);
        }
    }

    private void initListener() {
        RxToolKit.Build(this, ViewEvent.SpeakViewEvent.class).action(value -> {
            mWereWolfGameContext.check().post(() -> {
                switch (value.getMotionEvent()) {
                    case MotionEvent.ACTION_DOWN:
                        checkAndCreateSpeakPopWindow();
                        showSpeakPopWindow();
                        break;
                    case MotionEvent.ACTION_UP:
                        checkAndCreateSpeakPopWindow();
                        dismissSpeakPopWindow();
                        break;
                }
            });
        }).register();

        RxToolKit.Build(this, CardEvent.UseCardCheckEvent.class).action(v -> {
            mWereWolfGameContext.check().post(() -> {
                WereWolfCardCheckDialog dialog = new WereWolfCardCheckDialog(mWereWolfGameContext.getAndroidContext(), mWereWolfGameContext.getSelectMemberAll());
                dialog.showDialog(0);
            });
        }).register();

        RxToolKit.Build(this, ViewEvent.SpeechDangerTimeEvent.class).action(v -> {
            mWereWolfGameContext.check().post(() -> {
                if (countDownStatePopWindow == null) {
                    countDownStatePopWindow = new CountDownStatePopWindow(mWereWolfGameContext.getAndroidContext());
                }
                countDownStatePopWindow.showPopWindow(mRootView, Constant.DANGEROUS_TIME + 1);
            });
        }).register();

        RxToolKit.Build(this, ViewEvent.SpeechNoDangerTimeEvent.class).action(v -> {
            mWereWolfGameContext.check().post(() -> {
                if (countDownStatePopWindow != null) {
                    countDownStatePopWindow.dismissPopWindow();
                }
            });
        }).register();

        RxToolKit.Build(this, ViewEvent.RequestAddFriendEvent.class).action(v -> {
            if (!mWereWolfGameContext.isObserver()) {
                int position = mWereWolfGameContext.getPositionByUserId(v.getToUserId());
                if (position >= 1) {
                    RoomSocketEngineHelper.sendRequestFriendInGame(v.getToUserId(), position - 1);
                }
            } else {
                return;
            }
        }).register();
    }

    private void initWereWolfWindow() {
        try {
            View rootView = mWereWolfGameContext.getGameRootView();
            View gameRootView = rootView.findViewById(R.id.id_werewolf_game_main_root);
            if (AppManager.GreaterLollipop()) {
                /**
                 * 如果超过5.0并且需要为rootView设置一个padding
                 */
                gameRootView.setPadding(0, ScreenUtils.dip2px(25), 0, 0);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWereWolfBackGround() {
        initWereWolfBackGroundSunUp();
    }

    private void initWereWolfBackGroundSunUp() {
        mBackGroundSurfaceView.turnIntoSunUp();
    }

    private void initWereWolfBackGroundSunSet() {
        mBackGroundSurfaceView.turnIntoSunSet();
    }

    private void initWereWolfMembers() {
        for (int i = 1; i <= 12; i++) {
            WereWolfGameMemberView positionView = new WereWolfGameMemberView(mWereWolfGameContext, i);
            mWolfGamePositionViewList.put(i, positionView);
        }
    }

    public void setTitleCloseAction(View.OnClickListener onClickListener) {
        if (mWereWolfTitleView != null) {
            mWereWolfTitleView.setCloseAction(onClickListener);
        }
    }

    private GameMember.GameMemberData getGameMemberLightData(int from) {
        WereWolfGameMember gameMember = mWereWolfGameContext.getGameMember(from);
        if (gameMember != null) {
            return gameMember.turnIntoGameMemberData();
        } else {
            return null;
        }
    }

    private PointF getPositionMemberPoint(int position) {
        return mWolfGamePositionViewList.get(position).getPositionMemberPoint();
    }

    private int getPositionMemberWidth(int position) {
        return mWolfGamePositionViewList.get(position).getPositionMemberWidth();
    }

    private int getPositionMemberHeight(int position) {
        return mWolfGamePositionViewList.get(position).getPositionMemberHeight();
    }

    private void beforeShowGameDialog() {
        if (mCurGameDialog != null) {
            mCurGameDialog.dismiss();
        }
    }

    private void showGameDialog(int dur) {
        if (mCurGameDialog != null) {
            mCurGameDialog.showDialog(dur);
        }
    }

    private void checkAndCreateSpeakPopWindow() {
        if (mWereWolfSpeakPopWindow == null) {
            mWereWolfSpeakPopWindow = new WereWolfGameSpeakPopWindow(mContext);
        }
    }

    private void showSpeakPopWindow() {
        if (mWereWolfSpeakPopWindow != null && !mWereWolfSpeakPopWindow.isShowing()) {
            mWereWolfSpeakPopWindow.showPopWindow(mRootView);
        }
    }

    private void dismissSpeakPopWindow() {
        if (mWereWolfSpeakPopWindow != null && mWereWolfSpeakPopWindow.isShowing()) {
            mWereWolfSpeakPopWindow.dismissPopWindow();
        }
    }

    public void setBoardCountDown(int dur) {
        if (mWereWolfSystemBoardView != null) {
            mWereWolfSystemBoardView.setSystemBoardCountDown("setBoardCountDown", dur);
        }
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        if (mWereWolfTopLayer != null) {
            mWereWolfTopLayer.destroy();
            mWereWolfTopLayer = null;
        }
        if (mWereWolfTitleView != null) {
            mWereWolfTitleView.destroy();
            mWereWolfTitleView = null;
        }
        if (mWereWolfSystemBoardView != null) {
            mWereWolfSystemBoardView.destroy();
            mWereWolfSystemBoardView = null;
        }
        if (mWereWolfConsoleView != null) {
            mWereWolfConsoleView.destroy();
            mWereWolfConsoleView = null;
        }
        if (mWolfGameMessageView != null) {
            mWolfGameMessageView.destroy();
            mWolfGameMessageView = null;
        }
        if (mWolfGamePositionViewList != null) {
            for (int i = 1; i < 12; i++) {
                mWolfGamePositionViewList.get(i).destroy();
            }
            mWolfGamePositionViewList.clear();
        }
        if (mWereWolfGameContext.isObserver()) {
            mWereWolfGameContext.unRegisterProcessListener(mObserverProcess);
        } else {
            mWereWolfGameContext.unRegisterProcessListener(this);
        }
        mRootView = null;
    }

    @Override
    public boolean handleStart(StartResult startResult, JSONObject jsonObject) {
        if (!rnReportFlag) {
            RoomSocketEngineHelper.userStartGameReportRn();
            rnReportFlag = true;
        }
        if (startResult != null && startResult.optional != null) {
            List<String> data = startResult.optional;
            if (data.size() == 2) {
                beforeShowGameDialog();
                mCurGameDialog = new WereWolfRobCardDialog(mContext, data.get(0), "", data.get(1), mWereWolfTopLayer.getGiftAnimationView());
                showGameDialog(startResult.duration / 1000);
            }
            if (data.size() == 3) {
                beforeShowGameDialog();
                mCurGameDialog = new WereWolfRobCardDialog(mContext, data.get(0), data.get(1), data.get(2), mWereWolfTopLayer.getGiftAnimationView());
                showGameDialog(startResult.duration / 1000);
            }
        }
        if (mWereWolfTopLayer != null) {
            mWereWolfTopLayer.refreshCard();
        }
        return false;
    }

    @Override
    public boolean handleAssginedRole(AssginedRoleResult roleResult, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfIdentityDialog(mContext, roleResult.role);
        showGameDialog(roleResult.duration / 1000);
        return false;
    }

    @Override
    public boolean handleSpeechDirection(int dur, int leftPosition, int rightPosition, JSONObject jsonObject) {
        if (leftPosition == -1 && rightPosition == -1) {
            return false;
        }
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfSheriffSpeechOrderDialog(mContext, mWereWolfGameContext.getSelectMember(leftPosition), mWereWolfGameContext.getSelectMember(rightPosition));
        showGameDialog(dur);
        return false;
    }

    @Override
    public boolean handleWakeToKill(WakeToKillResult wakeToKillResult) {
        if (wakeToKillResult.alives != null) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfKillView(mWereWolfTopLayer.getGameFakerDialogViewContainer(), mWereWolfGameContext.getWolfKillMember(wakeToKillResult.alives));
            showGameDialog(wakeToKillResult.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightState(ServerActionFightState fightState) {
        try {
            if (!fightState.can_fight) {
                if (mCurGameDialog != null && mCurGameDialog instanceof WereWolfFightAwayDialog && ((WereWolfFightAwayDialog) mCurGameDialog).isShowing()) {
                    beforeShowGameDialog();
                }
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAway(ServerActionFightAway fightAway) {
        if (fightAway.alives != null) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfFightAwayDialog(mWereWolfGameContext.getAndroidContext(), mWereWolfGameContext.getSelectMemberWithOutSelf(fightAway.alives));
            showGameDialog((int) (fightAway.duration / 1000));
        }
        return false;
    }

    @Override
    public boolean handleServerActionFightAwayResult(ServerActionFightAwayResult fightAwayResult) {
        showFightAwayResultDialog(fightAwayResult);
        return false;
    }

    private void showFightAwayResultDialog(ServerActionFightAwayResult fightAwayResult) {
        if (fightAwayResult.knight >= 0) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfFightDialog(mWereWolfGameContext.getAndroidContext(), mWereWolfGameContext.getSelectMember(fightAwayResult.knight + 1));
            showGameDialog((int) (fightAwayResult.duration / 1000));
        }
    }

    @Override
    public boolean handleSave(int position, boolean canSave, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfSaveDialog(mContext, canSave, mWereWolfGameContext.getSelectMember(position));
        showGameDialog(RoomSocketEngineHelper.getOptDurTime(jsonObject));
        return false;
    }

    @Override
    public boolean handlePoison(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfPoisonDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives));
        showGameDialog(info.duration / 1000);
        return false;
    }

    @Override
    public boolean handleResetMaster(int dur, JSONObject jsonObject) {
        WereWolfToolKit.ShowResetMasterDialog(mContext, dur);
        return false;
    }

    @Override
    public boolean handleApply(int durSeconds, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfRunInElectionDialog(mContext);
        showGameDialog(durSeconds);
        return false;
    }

    @Override
    public boolean handleVoteResult(VoteResultInfo info) {
        String voteType = info.type;
        if (TextUtils.equals(voteType, Constant.APPLY)) {
            if (info.need_pk)
                return false;
            List<Integer> positions = info.positions;
            if (positions == null || positions.size() == 0) {
                return false;
            }
            int position = positions.get(0);
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfSheriffElectedDialog(mContext, mWereWolfGameContext.getSelectMember(position + 1));
            showGameDialog(info.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleSheriffResult(int position, int durSeconds, JSONObject jsonObject) {
        if (position != -1) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfSheriffElectedDialog(mContext, mWereWolfGameContext.getSelectMember(position));
            showGameDialog(durSeconds);
        }
        return false;
    }

    @Override
    public boolean handleSystemMessage(String type, String content, JSONObject jsonObject) {
        if (TextUtils.equals(type, Constant.SYSTEM_MSG_TYPE_DIALOG)) {
            WereWolfToolKit.ShowSystemMsg(mContext, content, RoomSocketEngineHelper.getOptDurTime(jsonObject));
        } else if (TextUtils.equals(type, Constant.SYSTEM_MSG_TYPE_TOAST)) {
            ToastToolKit.showShort(content);
        } else if (TextUtils.equals(type, Constant.SYSTEM_MSG_TYPE_TEXT)) {
            //doNothing
        }
        return false;
    }

    @Override
    public boolean handleHandOver(WereWolfGameSurvivorsInfo info) {
        if (info != null && !Utils.targetListIsNull(info.alives)) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfHandOverDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives));
            showGameDialog(info.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleSeerCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        if (info != null && info.alives != null && info.alives.size() > 0) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfSeerCheckDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives));
            showGameDialog(info.duration / 1000);
        }

        return false;
    }

    @Override
    public boolean handleCheckResult(CheckResult checkResult) {
        if (checkResult != null) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfSeerCheckResultDialog(mContext, checkResult.role, mWereWolfGameContext.getSelectMember(checkResult.position));
            showGameDialog(checkResult.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleHandOverResult(int from, int to, JSONObject jsonObject) {
        if (to != -1 && from != -1) {
            final int position1 = from;
            final int position2 = to;
            final int dur = RoomSocketEngineHelper.getOptDurTime(jsonObject);
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfHandOverSheriffResultDialog(mContext, mWereWolfGameContext.getSelectMember(position1), mWereWolfGameContext.getSelectMember(position2));
            showGameDialog((dur / 2));
            mWereWolfGameContext.check().post(() -> {
                beforeShowGameDialog();
                mCurGameDialog = new WereWolfSheriffElectedDialog(
                        mContext,
                        mWereWolfGameContext.getSelectMember(position2)
                );
                showGameDialog(dur / 2);
            }, (dur / 2) * 1000);
        } else {

        }
        return false;
    }

    @Override
    public boolean handleExchange(WereWolfExchange exchange, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfExchangeDialog(mWereWolfGameContext.getAndroidContext(), mWereWolfGameContext.getSelectMemberForMagician(exchange.alives, exchange.last_exchanged));
        showGameDialog(exchange.duration / 1000);
        return false;
    }

    @Override
    public boolean handleExchangeResult(WereWolfExchangeResult exchangeResult, JSONObject jsonObject) {
        return super.handleExchangeResult(exchangeResult, jsonObject);
    }

    @Override
    public boolean handleDemonCheck(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfDemonCheckDialog(mWereWolfGameContext.getAndroidContext(), mWereWolfGameContext.getSelectMember(info.alives));
        showGameDialog(info.duration / 1000);
        return false;
    }

    @Override
    public boolean handleDemonCheckResult(boolean is_god, int duration, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfDemonCheckResultDialog(
                mWereWolfGameContext.getAndroidContext(),
                is_god ? WereWolfConstant.ROLE_GOD : WereWolfConstant.ROLE_NOT_GOD,
                mWereWolfGameContext.getSelectMember(RoomSocketEngineHelper.getPositionFromJSON(jsonObject) + 1));
        showGameDialog(duration);
        return false;
    }

    @Override
    public boolean handleSunUp(int dayIndex, JSONObject jsonObject) {
        initWereWolfBackGroundSunUp();
        return false;
    }

    @Override
    public boolean handleSunSet(int dayIndex, int dur, JSONObject jsonObject) {
        initWereWolfBackGroundSunSet();
        return false;
    }

    @Override
    public boolean handleLink(int duration, JSONObject jsonObject) {
        if (TextUtils.equals(mWereWolfGameContext.getSelfRole(), Constant.ROLE_CUPID)) {
            WereWolfGameSurvivorsInfo info = WereWolfToolKit.GetSurvivorsFromJSON(jsonObject);
            if (info != null) {
                beforeShowGameDialog();
                mCurGameDialog = new WereWolfCupidSelectDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives), WereWolfString.getString(R.string.werewolf_game_link_title));
                showGameDialog(duration);
            }
        }
        return false;
    }

    @Override
    public boolean handleLinkResult(LinkResult linkResult) {
        Iterator iterator = linkResult.lovers.entrySet().iterator();
        int position1 = -1, position2 = -1;
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            int position = (int) entry.getKey();

            if (position1 == -1) {
                position1 = position;
            } else if (position2 == -1) {
                position2 = position;
            }
        }
        WereWolfSelectMember member1 = mWereWolfGameContext.getSelectMember(position1 + 1);
        WereWolfSelectMember member2 = mWereWolfGameContext.getSelectMember(position2 + 1);
        beforeShowGameDialog();
        if (RoomSocketEngineHelper.isCupid(mWereWolfGameContext.getSelfRole())) {
            mCurGameDialog = new WereWolfConfirmLoversDialog(
                    mContext,
                    MessageUtils.getString(R.string.link_result_title),
                    member1,
                    member2
            );
        } else {
            mCurGameDialog = new WereWolfConfirmLoversDialog(
                    mContext,
                    MessageUtils.getString(R.string.link_result_yourself_title),
                    member1,
                    member2
            );
        }
        showGameDialog(linkResult.duration / 1000);
        return false;
    }

    @Override
    public boolean handleTakeAway(WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        if (info != null && info.alives != null && info.alives.size() > 0) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfTakeAwayDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives));
            showGameDialog(info.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleBoom(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        showBoomDialog(position, isWolfKing, duration, jsonObject);
        return false;
    }

    private void showBoomDialog(int position, boolean isWolfKing, int duration, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfBoomDialog(mContext, isWolfKing, mWereWolfGameContext.getSelectMember(position));
        showGameDialog(duration);
    }

    @Override
    public boolean handleBoomAway(WereWolfGameSurvivorsInfo survivorsInfo) {
        if (survivorsInfo != null && survivorsInfo.alives != null && survivorsInfo.alives.size() > 0) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfBoomAwayDialog(mContext, mWereWolfGameContext.getSelectMember(survivorsInfo.alives));
            showGameDialog(survivorsInfo.duration / 1000);
        }
        return false;
    }

    @Override
    public boolean handleProtect(WereWolfGameSurvivorsInfo info, int lastProtect, JSONObject jsonObject) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfProtectDialog(mContext, mWereWolfGameContext.getSelectMemberForProtector(info.alives, lastProtect));
        showGameDialog(info.duration / 1000);
        return false;
    }

    @Override
    public boolean handleVote(int dur, WereWolfGameSurvivorsInfo info, JSONObject jsonObject) {
        if (info != null && info.alives != null && info.alives.size() > 0) {
            beforeShowGameDialog();
            mCurGameDialog = new WereWolfVoteDialog(mContext, mWereWolfGameContext.getSelectMember(info.alives));
            showGameDialog(dur);
        }
        return false;
    }

    @Override
    public boolean handleGift(String giftType, String message, int from, int to, JSONObject jsonObject) {
        return handleGiftInner(giftType, message, from, to, jsonObject);
    }

    private boolean handleGiftInner(String giftType, String message, int from, int to, JSONObject jsonObject) {
        if (EffectsManager.IsCardByGiftType(giftType)) {
            return false;
        }

        int fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        if (from > 6) {
            fromO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        } else {
            fromO = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;
        }

        int toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        if (to > 6) {
            toO = GiftAnimConstant.MEMBER_ORIENTATION_RIGHT;
        } else {
            toO = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;
        }
        PointF fromPoint = getPositionMemberPoint(from);
        PointF toPoint = getPositionMemberPoint(to);

        /**
         * 这里所有的点纵坐标都要加状态栏
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fromPoint.set(fromPoint.x, fromPoint.y);
            toPoint.set(toPoint.x, toPoint.y);
        } else {
            /**
             * do nothing
             */
            fromPoint.set(fromPoint.x, fromPoint.y - ScreenUtils.dip2px(25));
            toPoint.set(toPoint.x, toPoint.y - ScreenUtils.dip2px(25));
        }

        int startWidth = getPositionMemberWidth(from);
        int startHeight = getPositionMemberHeight(from);

        int endWidth = getPositionMemberWidth(to);
        int endHeight = getPositionMemberHeight(to);

        int resultWidth = (startWidth + endWidth) / 2;
        int resultHeight = (startHeight + endHeight) / 2;

        EffectsManager.DispatchAnimEvent(
                mContext, GiftAnimConstant.GIFT_FROM_TYPE_GAME, giftType, new PointF(fromPoint.x, fromPoint.y), new PointF(toPoint.x, toPoint.y), fromO, toO, resultWidth, resultHeight, from, to);
        try {
            if (EffectsManager.SupportThisGiftType(giftType)) {
                GameMember.GameMemberData fromData = getGameMemberLightData(from);
                GameMember.GameMemberData toData = getGameMemberLightData(to);
                GiftBoardEvent giftBoardEvent = new GiftBoardEvent(fromData, toData, giftType);
                if (giftBoardEvent != null) {
                    RxToolKit.Emit(giftBoardEvent);
                }
                JSONObject giftJsonObj = jsonObject.optJSONObject("gift");
                if (giftJsonObj.has("rebate")) {
                    /**
                     * 这里需要处理返现
                     */
                    WereWolfGiftRebate rebate = GsonHelper.getGson().fromJson(giftJsonObj.optJSONObject("rebate").toString(), WereWolfGiftRebate.class);
                    if (rebate != null) {
                        int dim = rebate.value;
                        String dimString = Integer.toString(dim);

                        if (TextUtils.isEmpty(dimString)) {
                            return false;
                        }

                        EffectsManager.DispatchRebateAnimEvent(mContext, dimString, new PointF(fromPoint.x, fromPoint.y), new PointF(toPoint.x, toPoint.y), fromO, toO, resultWidth, resultHeight);

                        if (!TextUtils.equals(toData.userId, GlobalUserState.getGlobalState().getUserId())
                                && !Utils.checkUserIdIsTourist(toData.userId)) {
                            /**
                             * 不能为游客，不能给自己
                             */
                            LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(giftType, toData.userId, toData.userName, toData.userImage, toData.userSex, dimString);
                        }
                    }
                } else {
                    /**
                     * ignore
                     */
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean handleAddFriend(String userId, String userName, String userIv, JSONObject jsonObject) {
        RxToolKit.Emit(new ViewEvent.ReceiveAddFriendEvent(userId, userName, userIv).setFromType(WereWolfConstant.GAME_FRIEND_REQUEST));
        return true;
    }

    @Override
    public boolean handleSpeechDirectionResult(int dur, JSONObject jsonObject) {
        return super.handleSpeechDirectionResult(dur, jsonObject);
    }

    @Override
    public boolean handleCheckCardResult(String type, JSONObject jsonObject) {
        String role = jsonObject.optString(Constant.ROLE);
        if (TextUtils.isEmpty(role)) {
            return false;
        }
        WereWolfCardCheckResultDialog dialog = new WereWolfCardCheckResultDialog(mWereWolfGameContext.getAndroidContext(), role);
        dialog.showDialog(0);
        return false;
    }

    @Override
    public boolean handleRestoreRoom(RestoreResult restoreResult) {
        return false;
    }

    @Override
    public boolean handleGameOver(GameOverResult gameOverResult) {
        beforeShowGameDialog();
        mCurGameDialog = new WereWolfGameOverDialog(mWereWolfGameContext, gameOverResult, mWereWolfGameContext.getSelectMemberAll());
        showGameDialog(0);
        return false;
    }

    //更改玩家身份
    private void updatePersonalIdentity() {
        if (mWereWolfGameContext.isObserver()) {
            mWereWolfGameContext.unRegisterProcessListener(this);
            mWereWolfGameContext.registerProcessListener(mObserverProcess);
        } else {
            mWereWolfGameContext.unRegisterProcessListener(mObserverProcess);
            mWereWolfGameContext.registerProcessListener(this);
        }
    }

    @Override
    public boolean onProcessStart(String type, JSONObject jsonObject) {
        return false;
    }

    @Override
    public boolean onProcessEnd(String type, JSONObject jsonObject) {
        if (shouldHandle(type)) {
            updatePersonalIdentity();
        }
        return false;
    }

    @Override
    public boolean onProcessError(Exception e) {
        return false;
    }

    private boolean shouldHandle(String type) {
        if (TextUtils.equals(type, Constant.GAME_UP_SEAT) || TextUtils.equals(type, Constant.GAME_DOWN_SEAT)) {
            return true;
        }
        return false;
    }

    public WereWolfGameTopLayer getWereWolfTopLayer() {
        return mWereWolfTopLayer;
    }

    public SparseArray<WereWolfGameMemberView> getWolfGamePositionViewList() {
        return mWolfGamePositionViewList;
    }
}
