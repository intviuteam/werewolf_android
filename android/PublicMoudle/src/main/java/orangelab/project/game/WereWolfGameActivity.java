package orangelab.project.game;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.transport.TransportHelper;
import com.datasource.GlobalUserState;
import com.BuildConfig;

import orangelab.project.MainApplication;
import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.activity.RoomSafeActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.engine.OrbitEngineManager;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.config.GameRoomConfig;
import orangelab.project.game.dialog.WereWolfHintDialog;
import orangelab.project.game.dialog.WereWolfVoteDialog;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;

import com.networktoolkit.transport.Constant;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Destroyable;
import com.umeng.socialize.UMShareAPI;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.orbit.OrbitError;
import cn.intviu.sdk.model.User;
import cn.intviu.support.ReportRobot;
import orangelab.project.game.view.WereWolfKillView;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * game_werewolf
 * 2017/11/27 下午2:34
 * Mystery
 */

public class WereWolfGameActivity extends RoomSafeActivity implements Destroyable {

    private static final String TAG = "WereWolfGameActivity";

    private EnterRoomResult mEnterRoomResult;

    /**
     * We use this Context to Control all Status in Game;
     */
    private WereWolfGameContext mWereWolfGameContext = null;

    /**
     * 负责 游戏的主要UI
     */
    private WereWolfGameUIController mWereWolfGameViewController = null;

    /**
     * 负责 语音交流
     */

    private boolean isDestroy = false;

    private RxPermissions mRxPermissions;

    private WereWolfHintDialog mExitDialog;

    private WereWolfHintDialog mPermissionDialog = null;
    private WereWolfHintDialog mDisConnectFromRoomDialog = null;
    private boolean mSocketReConnectFailed = false;

    private boolean mBackEnable = false;
    private String mSocketEngineToken = "";

    public static void Launch(Context context, String gameType, String password, EnterRoomResult enterRoomResult, final String socketEngineToken) {
        Intent intent = new Intent(context, WereWolfGameActivity.class);

        TransportHelper.putTransportable(TAG, enterRoomResult);

        IntentDataHelper.setUserName(intent, GlobalUserState.getGlobalState().getUserName());
        IntentDataHelper.setUserId(intent, GlobalUserState.getGlobalState().getUserId());
        IntentDataHelper.setUserSex(intent, GlobalUserState.getGlobalState().getUserSex());
        IntentDataHelper.setUserToken(intent, GlobalUserState.getGlobalState().getToken());
        IntentDataHelper.setUserAvater(intent, GlobalUserState.getGlobalState().getUserIcon());
        IntentDataHelper.setGameType(intent, gameType);
        IntentDataHelper.setRoomPassword(intent, password);
        IntentDataHelper.setUserExp(intent, GlobalUserState.getGlobalState().getUserExp());

        IntentDataHelper.setSocketEngineToken(intent, socketEngineToken);

        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (preInit()) return;

        EnterRoomResult temp = TransportHelper.getTransport(TAG);
        if (temp != null) {
            mEnterRoomResult = temp;
        }
        if (temp == null) {
            /**
             * 数据为空直接结束,先强制Destroy 然后 强制 Finish
             */
            destroy();
            return;
        }

        mSocketEngineToken = IntentDataHelper.getSocketEngineToken(getIntent());
        if (TextUtils.isEmpty(mSocketEngineToken)) {
            destroy();
            return;
        }

        initData();

        initWindow();

        setContentView(R.layout.layout_werewolf_game);

        initWereWolfContext();

        initView();

        initListener();

        initVoiceEngine();

        initBackEnable();

        ReportEventUtils.reportGameEnter();
    }

    private boolean preInit() {
        if (!MainApplication.getInstance().socketIsExit()) {
            finish();
            return true;
        }
        return false;
    }

    private void initData() {
        mRxPermissions = new RxPermissions(this);

        GlobalUserState.getGlobalState().setCurGameType(IntentDataHelper.getGameType(getIntent()));
        GlobalUserState.getGlobalState().setGaming(true);
        GlobalUserState.getGlobalState().setCurRoomId(mEnterRoomResult.room.room_id);

        NotificationUtils.IMPL().clearAllNotification(this);
        //----------------------------------------------------------------

        PersonalData myself = new PersonalData();

        myself.setCurPassword(mEnterRoomResult.room.password);
        myself.setRoomId(mEnterRoomResult.room.room_id);
        GlobalUserState.getGlobalState().setPassword(mEnterRoomResult.room.password);
        GlobalUserState.getGlobalState().setCurRoomId(mEnterRoomResult.room.room_id);

        PersonalDataHelper.setPersonalData(myself);


        //----------------------------------------------------------------
    }

    private void initWereWolfContext() {
        mWereWolfGameContext = new WereWolfGameContext(this, (ViewGroup) findViewById(R.id.id_werewolf_game_root), mEnterRoomResult);
        mWereWolfGameContext.bindDestroyFatherAction(value -> {
            intentDestroy(value);
        });
    }

    private void initWindow() {
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT);

        } else {
            //Utils.setWindowStatusBarColor(mWereWolfGameContext.getActivity(), WereWolfConfig.GetWindowSunUpBarStatusColor(mWereWolfGameContext.getGameType()));
        }
    }

    private void initView() {
        mWereWolfGameViewController = new WereWolfGameUIController(mWereWolfGameContext);
        mWereWolfGameViewController.setTitleCloseAction(v -> {
            if (mBackEnable) {
                intentDestroy(true);
            }
        });
    }

    private void initListener() {
        /**
         * init  Destroy Event Listener
         */
//        RxToolKit.Build(this, ViewEvent.DestroyEvent.class).action(value -> {
//            if (value.isNormal()) {
//                if (mSocketReConnectFailed) {
//                    mDisConnectFromRoomDialog = new WereWolfHintDialog(this);
//                    mDisConnectFromRoomDialog.setHintTitle(WereWolfString.getString(R.string.dialog_hint))
//                            .setHintMessage(WereWolfString.getString(R.string.you_have_been_disconnected))
//                            .setEnableRightWhenCounting(true)
//                            .setEnableLeftWhenCounting(true)
//                            .setDismissWhenCountDownFinish(false)
//                            .setLeftOnClick(v -> {
//                                mDisConnectFromRoomDialog.dismiss();
//                                finish();
//                            })
//                            .setRightOnClick(v -> {
//                                mDisConnectFromRoomDialog.dismiss();
//                                finish();
//                            })
//                            .showDialog(0);
//
//                } else {
//                    finish();
//                }
//            } else {
//                finish();
//            }
//        }).register();

        RxToolKit.Build(this, OrbitError.class).action(value -> {
            mWereWolfGameContext.check().post(() -> {
                switch (value.getErrorCode()) {
                    case OrbitError.AUDIO_RECORD_BEEN_RACE:
                    case OrbitError.AUDIO_RECORD_NO_PERMISSION: {
                        if (mPermissionDialog == null) {
                            buildPermissionDialogCaseNoPermission();
                            mPermissionDialog.showDialog(0);
                        } else {
                            if (mPermissionDialog.isShowing()) {
                                break;
                            } else {
                                buildPermissionDialogCaseNoPermission();
                                mPermissionDialog.showDialog(0);
                            }
                        }
                    }
                    ReportRobot.robot.report("AUDIO_PERMISSION_FAIL_ALL");
                    break;
                }
            });
        }).register();

        RxToolKit.Build(this, SocketEvent.ReConnectedFailedEvent.class)
                .actionFilter(value -> TextUtils.equals(value.getIntent(), SocketEngine.WEREWOLF))
                .action(value -> {
                    mSocketReConnectFailed = true;
                    intentDestroy(false);
                }).register();
//        RxToolKit.Build(this, GlobalEvent.GameDestroyEvent.class).action(value -> {
//            /**
//             * 外部收到的指令则进行销毁
//             */
//            destroy();
//        }).register();
    }

    private void buildPermissionDialogCaseNoPermission() {
        WereWolfHintDialog dialog = new WereWolfHintDialog(this);
        dialog.setHintTitle(MessageUtils.getString(R.string.dialog_hint))
                .setHintMessage(
                        mWereWolfGameContext.isGaming() ? MessageUtils.getString(R.string.audio_permission_error_in_gaming) : MessageUtils.getString(R.string.audio_permission_error_not_in_gaming))
                .setDismissWhenCountDownFinish(false)
                .setEnableLeftWhenCounting(true)
                .setEnableRightWhenCounting(true)
                .setLeftOnClick(v -> {
                    if (mWereWolfGameContext.isGaming()) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        intentDestroy(false);
                    }
                })
                .setRightOnClick(v -> {
                    if (mWereWolfGameContext.isGaming()) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        intentDestroy(false);
                    }
                });
        mPermissionDialog = dialog;
    }

    private void initVoiceEngine() {
        mRxPermissions.request(Manifest.permission.RECORD_AUDIO)
                .onBackpressureBuffer()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            // gameRoomEngine = new OrbitEngine(WereWolfGameActivity.this);
                            User user = new User();
                            String versionName = BuildConfig.VERSION_NAME;
                            if (Build.CPU_ABI.contains("86")) {
                                versionName = versionName + "-x86";
                            }
                            user.setName(versionName + "_" + GlobalUserState.getGlobalState().getUserName());
                            user.setID(GlobalUserState.getGlobalState().getUserId());
                            OrbitEngineManager.INSTANCE.start(WereWolfGameActivity.this, mWereWolfGameContext.getRoomId(), mEnterRoomResult.media_server, user);
                            MainApplication.getInstance().getSocketEngineHelper().startPollMessage();
                        } else {
                            RxToolKit.Emit(new OrbitError(OrbitError.AUDIO_RECORD_NO_PERMISSION));
                        }
                    }
                });
    }

    private void initBackEnable() {
        if (mWereWolfGameContext != null) {
            if (mWereWolfGameContext.isObserver()) {
                mBackEnable = true;
                return;
            } else {
                int backCount = GameRoomConfig.GetLeftRoomSecondsByMembers(mWereWolfGameContext.getSitedPositionsSize());
                if (backCount > 0) {
                    if (mWereWolfGameViewController != null) {
                        mWereWolfGameViewController.setBoardCountDown(backCount);
                    }

                    if (mWereWolfGameContext != null) {
                        mWereWolfGameContext.post(() -> {
                            mBackEnable = true;
                        }, backCount * 1000);
                    }
                } else {
                    mBackEnable = true;
                }
            }
        }
    }


    @Override
    public void finish() {
        super.finish();
        if (mPermissionDialog != null && mPermissionDialog.isShowing()) {
            mPermissionDialog.dismiss();
        }
        if (mDisConnectFromRoomDialog != null && mDisConnectFromRoomDialog.isShowing()) {
            mDisConnectFromRoomDialog.dismiss();
        }
        RxToolKit.UnRegister(this);
//        RxToolKit.Emit(new GlobalEvent.GameDestroyCallBackEvent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWereWolfGameContext != null) {
            mWereWolfGameContext.destroy();
            mWereWolfGameContext = null;
        }
        RxToolKit.UnRegister(this);
    }

    public void intentDestroy(boolean needDialog) {
        if (!needDialog) {
            destroy();
            return;
        }
        /**
         * show exit Dialog
         */
        if (mExitDialog == null) {
            if (mWereWolfGameContext.isGaming() && !mWereWolfGameContext.isObserver()) {
                if (mWereWolfGameContext.isDead()) {
                    mExitDialog = new WereWolfHintDialog(
                            this,
                            WereWolfString.getString(R.string.dialog_hint),
                            WereWolfString.getString(R.string.leave_dead),
                            v -> {
                                if (mExitDialog != null) {
                                    mExitDialog.dismiss();
                                }
                                destroy();
                            },
                            v -> {
                                if (mExitDialog != null) {
                                    mExitDialog.dismiss();
                                }
                            }
                    );
                    mExitDialog.showDialog(5);
                } else {
                    mExitDialog = new WereWolfHintDialog(
                            this,
                            WereWolfString.getString(R.string.dialog_hint),
                            WereWolfString.getString(R.string.leave_notdead_in_game),
                            v -> {
                                if (mExitDialog != null) {
                                    mExitDialog.dismiss();
                                }
                                destroy();
                            },
                            v -> {
                                if (mExitDialog != null) {
                                    mExitDialog.dismiss();
                                }
                            }
                    );
                    mExitDialog.showDialog(5);
                }
            } else {
                mExitDialog = new WereWolfHintDialog(
                        this,
                        WereWolfString.getString(R.string.dialog_hint),
                        WereWolfString.getString(R.string.leave_notdead_not_in_game),
                        v -> {
                            if (mExitDialog != null) {
                                mExitDialog.dismiss();
                            }
                            destroy();
                        },
                        v -> {
                            if (mExitDialog != null) {
                                mExitDialog.dismiss();
                            }
                        }
                );
                mExitDialog.showDialog(0);
            }
        } else {
            if (mExitDialog.isShowing()) {
                mExitDialog.dismiss();
            }
            mExitDialog = null;
            intentDestroy(true);
        }
    }

    @Override
    public void onBackPressed() {

//        WereWolfDialog mCurGameDialog = new WereWolfKillView(
//                mWereWolfGameViewController.getWereWolfTopLayer().getGameFakerDialogViewContainer(), mWereWolfGameContext.getWolfKillMemberRealPosition(mWereWolfGameContext.getAllGameMemberIndex
// ()));
//
//        mCurGameDialog.showDialog(30);

        try {
            ServerMessageEvent event = new ServerMessageEvent(SocketEngine.WEREWOLF, new JSONObject("{\"type\":\"vote_result\",\"payload\":{\"type\":\"death\",\"positions\":[5]," +
                    "\"vote_info\":{\"5\":[0],\"-1\":[1,2,3,4,5]},\"death_info\":[{\"killed\":5,\"reason\":\"voted\",\"dead\":true,\"role\":\"werewolf\"}],\"duration\":0,\"need_pk\":false," +
                    "\"msg_id\":1706928864803},\"msg_time\":1531805288648}"));
            RxToolKit.Emit(event);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void destroy() {
        if (!isDestroy) {
            RoomSocketEngineHelper.INSTANCE.nativeRequestMessage(Constant.LEAVE, null, null, null);
            RoomSocketEngineHelper.userLeaveReportRN();
            Log.i(TAG, "begin destroy: ");
            if (mWereWolfGameContext != null) {
                mWereWolfGameContext.destroy();
                mWereWolfGameContext = null;
            }
            if (mWereWolfGameViewController != null) {
                mWereWolfGameViewController.destroy();
                mWereWolfGameViewController = null;
            }
            OrbitEngineManager.INSTANCE.shutDown(() -> {
                runOnUiThreadSafely(() -> {
                    if (mSocketReConnectFailed) {
                        mDisConnectFromRoomDialog = new WereWolfHintDialog(this);
                        mDisConnectFromRoomDialog.setHintTitle(WereWolfString.getString(R.string.dialog_hint))
                                .setHintMessage(WereWolfString.getString(R.string.you_have_been_disconnected))
                                .setEnableRightWhenCounting(true)
                                .setEnableLeftWhenCounting(true)
                                .setDismissWhenCountDownFinish(false)
                                .setLeftOnClick(v -> {
                                    mDisConnectFromRoomDialog.dismiss();
                                    finish();
                                })
                                .setRightOnClick(v -> {
                                    mDisConnectFromRoomDialog.dismiss();
                                    finish();
                                })
                                .showDialog(0);

                    } else {
                        finish();
                    }
                });
            });
            /**
             * close Socket Engine
             */
            MainApplication.getInstance().getSocketEngineHelper().releaseEngine(mSocketEngineToken);
            /**
             * clear GlobalData about Game
             */
            GlobalUserState.getGlobalState().setGaming(false);
            GlobalUserState.getGlobalState().setCurGameType("");
            /**
             * post Finish Event when leave Game
             */
            EventBus.getDefault().post(new FinishConversationEvent());
            RxToolKit.Emit(new FinishConversationEvent());
            /**
             * Report to Umeng
             */
            ReportEventUtils.reportGameLeave();
            PersonalDataHelper.setPersonalData(null);
        } else {
            return;
        }
    }
}
