package orangelab.project.game.model;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/4 上午11:58
 * Mystery
 */

public class WereWolfGameSurvivorsInfo implements Keepable {

    public List<Integer> alives;
    public int duration;
}
