package orangelab.project.game.model;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2017/11/30 下午3:08
 * Mystery
 */

public class WereWolfKillMember {

    public int memberPosition = -1;
    public String memberName;
    public String memberImage;
    public String memberRole;
    public boolean memberSelect = false;

    public boolean isSheriff = false;
    public boolean isLover = false;
    public boolean isWolf = false;
    public boolean isWolfKing = false;
    public boolean isDemon = false;
    public Map<Integer, Integer> mKillMap = new HashMap<>();

}
