package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.effect.IAnimationSurface;
import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.exhibition.card.CardUtils;
import orangelab.project.game.model.WereWolfDialog;
import orangelab.project.game.utils.WereWolfDialogDecorate;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.utils.CountDownView;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;

import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/11/29 下午9:14
 * Mystery
 */

public class WereWolfRobCardDialog extends SafeDialog implements CountDownView.CountDownFinish, WereWolfDialog {

    private static final String TAG = "WereWolfRobCardDialog";
    private boolean robLock;

    private TextView mTextView;
    private TextView mCountDownTextView;
    private CountDownView mCountDownView;

    private ImageView mLeftCardIv;
    private ImageView mMidCardIv;
    private ImageView mRightCardIv;

    private Button mLeftCardBtn;
    private Button mMidCardBtn;
    private Button mRightCardBtn;

    private View mLeftContainer;
    private View mMidContainer;
    private View mRightContainer;

    private String mLeftCardType;
    private String mMidCardType;
    private String mRightCardType;

    private final int dialogMaxWidth = ScreenUtils.dip2px(355);
    private boolean hasSuccess;
    private boolean hasRunOutLeft;
    private boolean hasRunOutRight;
    private boolean hasRunOutMid;

    public WereWolfRobCardDialog(@NonNull Context context, String leftCardType, String midCardType, String rightCardType, IAnimationSurface animationView) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_werewolf_rob_card);
        mTextView = (TextView) findViewById(R.id.id_werewolf_base_dialog_title);
        mCountDownTextView = (TextView) findViewById(R.id.id_werewolf_base_dialog_countdown);
        mTextView.setText(MessageUtils.getString(R.string.rob_card_title_string));

        mLeftCardType = leftCardType;
        mMidCardType = midCardType;
        mRightCardType = rightCardType;

        mLeftContainer = findViewById(R.id.id_werewolf_left_card_container);
        mMidContainer = findViewById(R.id.id_werewolf_mid_card_container);
        mRightContainer = findViewById(R.id.id_werewolf_right_card_container);

        if (TextUtils.isEmpty(midCardType)) {
            mMidContainer.setVisibility(View.GONE);
        } else {
            mMidContainer.setVisibility(View.VISIBLE);
        }

        mLeftCardBtn = (Button) findViewById(R.id.id_werewolf_rob_left_card);
        mLeftCardIv = (ImageView) findViewById(R.id.id_werewolf_left_card);

        mMidCardBtn = (Button) findViewById(R.id.id_werewolf_rob_mid_card);
        mMidCardIv = (ImageView) findViewById(R.id.id_werewolf_mid_card);

        mRightCardBtn = (Button) findViewById(R.id.id_werewolf_rob_right_card);
        mRightCardIv = (ImageView) findViewById(R.id.id_werewolf_right_card);

        CardUtils.fillImageViewByCardRoleType(mLeftCardIv, leftCardType);
        CardUtils.fillImageViewByCardRoleType(mRightCardIv, rightCardType);
        CardUtils.fillImageViewByCardRoleType(mMidCardIv, midCardType);

        if (animationView.hasCard(leftCardType)) {
        } else {
            mLeftCardBtn.setVisibility(View.GONE);
        }
        if (animationView.hasCard(rightCardType)) {
        } else {
            mRightCardBtn.setVisibility(View.GONE);
        }
        if (animationView.hasCard(midCardType)) {
        } else {
            mMidCardBtn.setVisibility(View.GONE);
        }

        WereWolfDialogDecorate.DecorateDialog(getWindow());

        setCancelable(false);
        setCanceledOnTouchOutside(false);
        mLeftCardBtn.setOnClickListener(v -> {
            if (hasRunOutLeft) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.str_rob_card_failed)).run();
                return;
            }
            if (!robLock) {
                robLock = true;
                RoomSocketEngineHelper.robCardApplyRole(leftCardType);
                animationView.userCard(leftCardType, true);
            }
        });
        mMidCardBtn.setOnClickListener(v -> {
            if (hasRunOutMid) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.str_rob_card_failed)).run();
                return;
            }
            if (!robLock) {
                robLock = true;
                RoomSocketEngineHelper.robCardApplyRole(midCardType);
                animationView.userCard(midCardType, true);
            }
        });
        mRightCardBtn.setOnClickListener(v -> {
            if (hasRunOutRight) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.str_rob_card_failed)).run();
                return;
            }
            if (!robLock) {
                robLock = true;
                RoomSocketEngineHelper.robCardApplyRole(rightCardType);
                animationView.userCard(rightCardType, true);
            }
        });
        initListener();
    }

    private void initListener() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(value -> {
            runSafely(() -> {
                if (!value.isWereWolf()) {
                    return;
                }
                if (TextUtils.equals(value.type, Constant.APPLY_ROLE_RESULT)) {
                    String type = value.payload.optString(Constant.VOTE_TYPE);
                    String role = value.payload.optString(Constant.CARD_TYPE);
                    if (TextUtils.equals(role, mLeftCardType)) {
                        if (TextUtils.equals(type, Constant.SUCCESS)) {
                            hasSuccess = true;
                            disableLeft();
                            hasRobButton(mLeftCardBtn);
                        }

                        if (TextUtils.equals(type, Constant.NONE) && !hasSuccess) {
                            hasRunOutLeft = true;
                        }
                    }
                    if (TextUtils.equals(role, mRightCardType)) {
                        if (TextUtils.equals(type, Constant.SUCCESS)) {
                            hasSuccess = true;
                            disableRight();
                            hasRobButton(mRightCardBtn);
                        }
                        if (TextUtils.equals(type, Constant.NONE) && !hasSuccess) {
                            hasRunOutRight = true;
                        }
                    }
                    if (TextUtils.isEmpty(mMidCardType)) {
                        return;
                    } else {
                        if (TextUtils.equals(role, mMidCardType)) {
                            if (TextUtils.equals(type, Constant.SUCCESS)) {
                                hasSuccess = true;
                                disableMid();
                                hasRobButton(mMidCardBtn);
                            }
                            if (TextUtils.equals(type, Constant.NONE) && !hasSuccess) {
                                hasRunOutMid = true;
                            }
                        }
                    }
                }
            });
        }).register();
    }

    public void showDialog(int seconds) {
        if (seconds >= 0) {
            if (!isShowing()) {
                show();
            }
            if (seconds != 0) {
                mCountDownTextView.setVisibility(View.VISIBLE);
                mCountDownView = new CountDownView(mCountDownTextView, seconds, this);
                mCountDownView.setCountDownRule(R.string.werewolf_game_countdown);
                mCountDownView.start();
            } else {
                mCountDownTextView.setVisibility(View.GONE);
            }
        } else {
            /**
             * 如果传入负数则不显示对话框
             */
            Log.i(TAG, "showDialog: seconds < 0");
        }
    }

    private void hasRobButton(Button button) {
        if (button != null) {
            button.setText(R.string.apply_role_has_rob);
        }
    }

    private void disableMid() {
        mMidCardBtn.setEnabled(false);
        mMidCardBtn.setBackgroundResource(R.mipmap.ico_werewolf_card_robbed);
    }

    private void disableLeft() {
        mLeftCardBtn.setEnabled(false);
        mLeftCardBtn.setBackgroundResource(R.mipmap.ico_werewolf_card_robbed);
    }

    private void disableRight() {
        mRightCardBtn.setEnabled(false);
        mRightCardBtn.setBackgroundResource(R.mipmap.ico_werewolf_card_robbed);
    }

    @Override
    protected void release() {
        RxToolKit.UnRegister(this);
        if (mCountDownView != null) {
            mCountDownView.cancel();
            mCountDownView = null;
        }
    }

    @Override
    public void onFinish() {
        dismiss();
    }
}
