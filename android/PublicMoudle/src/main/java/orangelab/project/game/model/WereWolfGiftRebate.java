package orangelab.project.game.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/12/19 下午4:18
 * Mystery
 */

public class WereWolfGiftRebate implements Keepable {

    public int value;
    public String peer;
    public String type;

}
