package orangelab.project.game.dialog;

import android.content.Context;

import com.androidtoolkit.RxToolKit;
import com.R;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import com.networktoolkit.transport.Constant;

import java.util.List;

/**
 * game_werewolf
 * 2017/12/12 上午10:53
 * Mystery
 */

public class WereWolfCardCheckDialog extends WereWolfOnePersonSelectDialog {
    public WereWolfCardCheckDialog(Context context, List<WereWolfSelectMember> gameMembers) {
        super(context, gameMembers, MessageUtils.getString(R.string.check_card_title), Constant.CARD_CHECK, TWO_BUTTON);
        getTwoButtonLeft().setOnClickListener(v -> {
            RxToolKit.Emit(new CardEvent.WereWolfTryUseCheckCardEvent());
            dismiss();
        });
        getTwoButtonRight().setOnClickListener(v -> {
            if (selectIslegal()) {
                RoomSocketEngineHelper.sendPositionMessage(Constant.CARD_CHECK, getRealPosition().get(0));
                dismiss();
            }
        });
    }
}
