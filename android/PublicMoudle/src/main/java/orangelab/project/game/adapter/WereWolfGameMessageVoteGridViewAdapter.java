package orangelab.project.game.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.R;
import orangelab.project.game.WereWolfGameContext;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/12/3 下午3:36
 * Mystery
 */

public class WereWolfGameMessageVoteGridViewAdapter extends BaseAdapter implements Destroyable {

    private static final String TAG = "WereWolfGameMessageVote";

    private List<Integer> mIntegers = new ArrayList<>(0);
    private WereWolfGameContext mWolfGameContext;

    public WereWolfGameMessageVoteGridViewAdapter(List<Integer> integers, WereWolfGameContext wolfGameContext) {
        mIntegers = integers;
        mWolfGameContext = wolfGameContext;
    }

    @Override
    public int getCount() {
        return mIntegers.size();
    }

    @Override
    public Integer getItem(int position) {
        return mIntegers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i(TAG, "getView: position " + position);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mWolfGameContext.getAndroidContext(), R.layout.layout_werewolf_msg_list_item_vote_item_position, null);
            holder = new ViewHolder();
            holder.mPositionView = (TextView) convertView.findViewById(R.id.id_werewolf_msg_position);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        int p = getItem(position) + 1;
        holder.mPositionView.setText(Integer.toString(p));
        return convertView;
    }

    @Override
    public void destroy() {
        mWolfGameContext = null;
    }

    public static class ViewHolder {
        public TextView mPositionView;
    }
}
