package orangelab.project.game.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.R;
import orangelab.project.game.model.WereWolfSelectMember;
import orangelab.project.game.model.WereWolfSelectMemberHolder;

/**
 * game_werewolf
 * 2017/11/30 下午7:16
 * Mystery
 */

public class WereWolfConfirmLoversDialog extends WereWolfBaseDialog {

    public WereWolfConfirmLoversDialog(@NonNull Context context, String title, WereWolfSelectMember left, WereWolfSelectMember right) {
        super(context);

        setTitle(title);

        View view = View.inflate(context, R.layout.layout_werewolf_dialog_confirm_lovers, null);

        View leftView = view.findViewById(R.id.id_werewolf_left_member);

        View rightView = view.findViewById(R.id.id_werewolf_right_member);

        WereWolfSelectMemberHolder holderLeft = new WereWolfSelectMemberHolder(leftView);
        holderLeft.showAsSelectMember(left);

        WereWolfSelectMemberHolder holderRight = new WereWolfSelectMemberHolder(rightView);
        holderRight.showAsSelectMember(right);

        setDialogContentView(view);
        setButtonType(ONE_BUTTON);
        Button oneBtn = getOneButton();
        oneBtn.setOnClickListener(v -> {
            dismiss();
        });
    }
}
