package orangelab.project;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2018/4/8 下午7:50
 * Mystery
 */

public class ProjectConfig {

    public static boolean NEED_SHOW_PRIVATE_ROOM_BUTTON_IN_CHAT = true;
    public static String APP_TYPE_WEREWOLF = "werewolf";//记得修改这里
    public static String APP_TYPE_KUAIWAN = "werewolf_cn";

    public static String APP_NAME = "";
    public static String APP_TYPE = "werewolf";
    public static String APP_API = "";
    public static String APP_CHANNEL = "";
    public static String APP_FAMILY_MSG = "";
    public static int APP_ICON;
    public static int APP_SHARE_ICON;

    public static String WECHAT_APPID = "";
    public static String WECHAT_APPKEY = "";

    public static String QQ_APPID = "";
    public static String QQ_APPKEY = "";

    public static String LEANCLOUD_APPID = "";
    public static String LEANCLOUD_APPKEY = "";

    public static String LEANCLOUD_US_APPID = "";
    public static String LEANCLOUD_US_APPKEY = "";
    public static boolean LEANCLOUD_SUPPORT_CUSTOM_SERVER = true;

    public static boolean DEBUG_ABLE = false;

    public static String BG_SPLASH_NAME = "";

    public static int WATER_MARK = 0; //水印图标的id
    public static int QR_CODE_SHOW = 0; //秀一秀二维码
    public static String shareType = "";

    public static String KTVAppId = "";                 //ktv的appid
    public static String KTVAppKey = "";                //ktv的appkey
    public static String KTVAccountType = "";           //ktv的apptype


    public static List<String> EXCLUDE_USERID = new ArrayList<>();

    //app渠道检测 start======================================================================================//
    public static boolean isWereWolf() {
        return TextUtils.equals(APP_TYPE_WEREWOLF, APP_TYPE);
    }

    public static boolean isKuaiWan() {
        return TextUtils.equals(APP_TYPE_KUAIWAN, APP_TYPE);
    }
    //app渠道检测 end======================================================================================//
}
