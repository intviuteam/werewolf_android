package orangelab.project.common.effect.frames;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.effect.headdecor.HeadDecorAnimData;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.utils.BitmapDecodeUtils;

/**
 * game_werewolf
 * 2018/7/11 下午2:37
 * Mystery
 * <p>
 * 头像框帧动画
 */
public class HeadDecorFramesAnim extends BitmapFramesAnim<HeadDecorAnimData.HeadDecorFrame> implements BitmapFramesAnim.BitmapFramesAnimListener {

    private static final String TAG = "HeadDecorFramesAnim";

    private HeadDecorFramesAnim mNext;

    public HeadDecorFramesAnim(ImageView imageView) {
        super(imageView);
        setListener(this);
    }

    public void setNext(HeadDecorFramesAnim next) {
        mNext = next;
    }

    @Override
    protected Bitmap beforeFrameToBitmap(HeadDecorAnimData.HeadDecorFrame headDecorFrame) {

        return null;
    }

    @Override
    protected Bitmap onFrameToBitmap(Context context, HeadDecorAnimData.HeadDecorFrame headDecorFrame) {
        if (headDecorFrame != null) {
            String local = headDecorFrame.local;
            if (!TextUtils.isEmpty(local)) {
                int localResource = ApkResourceManager.GetApkImageResource(context, local);
                if (localResource > 0) {
                    /**
                     * 使用apk内部资源
                     */
                    return BitmapDecodeUtils.decodeBitmap(context, localResource);
                }
            }

            String remote = headDecorFrame.remote;
            if (!TextUtils.isEmpty(remote)) {
                /**
                 * 使用远程资源
                 */
                InputStream inputStream = null;
                try {
                    inputStream = EffectsMainFestManager.GetHeadDecorImageInputStream(remote);
                    if (inputStream != null) {
                        return BitmapDecodeUtils.decodeBitmap(context, inputStream);
                    }
                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected Bitmap afterFrameToBitmap(HeadDecorAnimData.HeadDecorFrame headDecorFrame, Bitmap bitmap) {
        Log.i(TAG, "afterFrameToBitmap: " + (bitmap == null ? 0 : bitmap.getByteCount()));
        return bitmap;
    }

    @Override
    public synchronized void stop() {
        super.stop();
    }

    @Override
    public synchronized void pause() {
        super.pause();
        if (mNext != null) {
            mNext.pause();
        }
    }

    @Override
    public synchronized void resume() {
        super.resume();
        if (mNext != null) {
            mNext.resume();
        }
    }

    public void stopAll() {
        stop();
        if (mNext != null) {
            mNext.stopAll();
        }
    }

    @Override
    public synchronized void release() {
        super.release();
    }

    public void releaseAll() {
        release();
        if (mNext != null) {
            mNext.releaseAll();
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        /**
         * 动画停止后要开始 Next特效
         */
        if (mNext != null) {
            mNext.start();
        }
    }

    @Override
    public void onRelease() {

    }
}
