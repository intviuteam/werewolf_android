package orangelab.project.common.system;

import android.content.Context;
import android.text.TextUtils;

import com.R;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import cn.intviu.support.SharePreStoreUtils;


/**
 * game_werewolf
 * 2017/8/4 上午11:40
 * Mystery
 * 系统控制器
 * 用于管理用户状态，禁玩或者其余状态
 */

public enum SystemController {

    INSTANCE;
    private static final String SYSTEM_NO_PLAY_TIME_START = "SYSTEM_NO_PLAY_TIME_START";
    private static final String SYSTEM_NO_PLAY_TIME_END = "SYSTEM_NO_PLAY_TIME_END";

    private Context mApplicationContext = null;

    private SystemController() {

    }

    public void bindContext(Context context) {
        this.mApplicationContext = context;
    }

    public void updateNoPlayTimeInner(String userId, long start, long end) {
        Utils.runSafely(() -> {
            if (TextUtils.isEmpty(userId)) {
                return;
            }
            SharePreStoreUtils.save(userId, mApplicationContext, SYSTEM_NO_PLAY_TIME_START, start);
            SharePreStoreUtils.save(userId, mApplicationContext, SYSTEM_NO_PLAY_TIME_END, end);
        });
    }

    public static SystemMessage createNoPlayMessage() {
        return new SystemMessage(
                MessageUtils.getString(R.string.system_hint_dialog_title_noplay),
                MessageUtils.getString(R.string.system_hint_dialog_msg_noplay)
        );
    }

    public SystemMessage isInNoPlayTimeInner(String userId, long target) {
        if (TextUtils.isEmpty(userId)) {
            return null;
        }
        long start = SharePreStoreUtils.getLong(userId, mApplicationContext, SYSTEM_NO_PLAY_TIME_START);
        long end = SharePreStoreUtils.getLong(userId, mApplicationContext, SYSTEM_NO_PLAY_TIME_END);
        if (target >= start && target <= end) {
            return createNoPlayMessage();
        }
        return null;
    }

    public void clearInner(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        SharePreStoreUtils.clear(userId, mApplicationContext);
    }

    public static void updateNoPlayTime(String userId, long start, long end) {
        INSTANCE.updateNoPlayTimeInner(userId, start, end);
    }

    public static SystemMessage isInNoPlayTime(String userId, long targetTime) {
        return INSTANCE.isInNoPlayTimeInner(userId, targetTime);
    }

    public static void bindApplicationContext(Context context) {
        INSTANCE.bindContext(context);
    }

    public static void clear(String userId) {
        INSTANCE.clearInner(userId);
    }

    public static void testNoPlayTime(String userId) {
        Utils.runSafely(() -> {
            String msgType = SystemConstant.MSG_TYPE_NOPLAY;
            long startTime = System.currentTimeMillis();
            long endTime = startTime + 1000 * 60;
            if (msgType.contains(SystemConstant.MSG_TYPE_NOPLAY)) {
                SystemController.updateNoPlayTime(userId, startTime, endTime);
            }
        });
    }
}
