package orangelab.project.common.effect;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import orangelab.project.common.effect.gifts.CustomGiftAnim;
import orangelab.project.common.utils.BitmapDecodeUtils;

import com.androidtoolkit.CodeTimeRecord;

import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/11/7 下午4:47
 * Mystery
 */

public class FasterAnimByCustomAnimFrame implements IFasterAnim {
    private static final String TAG = "FasterAnimByUrl";

    private class AnimationFrame {
        private CustomGiftAnim.CustomAnimFrame mFrame;
        private int mDuration;

        AnimationFrame(CustomGiftAnim.CustomAnimFrame frame, int duration) {
            mFrame = frame;
            mDuration = duration;
        }

        public CustomGiftAnim.CustomAnimFrame getFrame() {
            return mFrame;
        }

        public int getDuration() {
            return mDuration;
        }
    }

    private ArrayList<FasterAnimByCustomAnimFrame.AnimationFrame> mAnimationFrames; // list for all frames of animation
    private int mIndex; // index of current frame

    private boolean mShouldRun; // true if the animation should continue running. Used to stop the animation
    private boolean mIsRunning; // true if the animation prevents starting the animation twice
    private SoftReference<ImageView> mSoftReferenceImageView; // Used to prevent holding ImageView when it should be
    // dead.
    private Handler mHandler; // Handler to communication with UIThread
    private boolean needRepeat = true;

    private Bitmap mRecycleBitmap;  //Bitmap can recycle by inBitmap is SDK Version >=11

    private CustomGiftAnim.CustomAnimFrame mLastFrame = null;
    private FasterAnimByCustomAnimFrame.FramesSequenceAnimation mFramesSequenceAnimation;


    // Listeners
    private FasterAnimByCustomAnimFrame.OnAnimationStoppedListener mOnAnimationStoppedListener;
    private FasterAnimByCustomAnimFrame.OnAnimationFrameChangedListener mOnAnimationFrameChangedListener;

    private FasterAnimByCustomAnimFrame(ImageView imageView) {
        init(imageView);
        mRecycleBitmap = null;
    }

    private FasterAnimByCustomAnimFrame(ImageView imageView, boolean needRepeat) {
        init(imageView);
        mRecycleBitmap = null;
        this.needRepeat = needRepeat;
    }

    public static FasterAnimByCustomAnimFrame create(ImageView imageView) {
        return new FasterAnimByCustomAnimFrame(imageView);
    }

    public static FasterAnimByCustomAnimFrame create(ImageView imageView, boolean needRepeat) {
        return new FasterAnimByCustomAnimFrame(imageView, needRepeat);
    }

    public void init(ImageView imageView) {
        mAnimationFrames = new ArrayList<FasterAnimByCustomAnimFrame.AnimationFrame>();
        mSoftReferenceImageView = new SoftReference<ImageView>(imageView);

        mHandler = new Handler();
        if (mIsRunning == true) {
            stop();
        }
        mShouldRun = false;
        mIsRunning = false;
        mIndex = -1;
    }

    public void addAllFrames(List<CustomGiftAnim.CustomAnimFrame> frames, int interval) {
        /**
         * 如果帧数只有一张，那么就当作独立图片来解析
         */
        for (CustomGiftAnim.CustomAnimFrame frame : frames) {
            mAnimationFrames.add(new FasterAnimByCustomAnimFrame.AnimationFrame(frame, interval));
        }
    }

    private FasterAnimByCustomAnimFrame.AnimationFrame getNext() {
        mIndex++;
        if (mIndex >= mAnimationFrames.size()) {
            if (needRepeat) {
                mIndex = 0;
            } else {
                mIndex = mAnimationFrames.size() - 1;
            }
        }
        return mAnimationFrames.get(mIndex);
    }

    public interface OnAnimationStoppedListener {
        public void onAnimationStopped();
    }

    public interface OnAnimationFrameChangedListener {
        public void onAnimationFrameChanged(int index);
    }


    public void setOnAnimationStoppedListener(FasterAnimByCustomAnimFrame.OnAnimationStoppedListener listener) {
        mOnAnimationStoppedListener = listener;
    }

    public void setOnAnimationFrameChangedListener(FasterAnimByCustomAnimFrame.OnAnimationFrameChangedListener listener) {
        mOnAnimationFrameChangedListener = listener;
    }

    public synchronized void start() {
        mShouldRun = true;
        if (mIsRunning)
            return;
        mFramesSequenceAnimation = new FasterAnimByCustomAnimFrame.FramesSequenceAnimation();
        mHandler.post(mFramesSequenceAnimation);
        Log.i(TAG, "start: ");
    }

    public synchronized void stop() {
        mShouldRun = false;
        Log.i(TAG, "stop: stop This Anim");
    }

    public synchronized void release() {
        tryRecycleBitmap();
        mAnimationFrames.clear();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    private void tryRecycleBitmap() {
        if (mRecycleBitmap != null && !mRecycleBitmap.isRecycled()) {
//            mRecycleBitmap.recycle();
            mRecycleBitmap = null;
            Log.i(TAG, "onPostExecute: tryRecycleBitmap a bitmap");
            System.gc();
        }
    }

    private boolean isStop() {
        return !mShouldRun;
    }

    private class FramesSequenceAnimation implements Runnable {

        public void shutdown() {
            mIsRunning = false;
            mAnimationFrames.clear();
            if (mOnAnimationStoppedListener != null) {
                mOnAnimationStoppedListener.onAnimationStopped();
            }
        }

        @Override
        public void run() {
            ImageView imageView = mSoftReferenceImageView.get();
            if (isStop() || imageView == null) {
                shutdown();
                return;
            }
            mIsRunning = true;
            if (imageView.isShown()) {
                FasterAnimByCustomAnimFrame.AnimationFrame frame = getNext();
                FasterAnimByCustomAnimFrame.GetImageDrawableTask task = new FasterAnimByCustomAnimFrame.GetImageDrawableTask(imageView);
                task.execute(frame.getFrame());
                mHandler.postDelayed(this, frame.getDuration());
            }
        }
    }

    private class GetImageDrawableTask extends AsyncTask<CustomGiftAnim.CustomAnimFrame, Void, Drawable> {


        private ImageView mImageView;
        private Resources mResources;
        private Context mContext;

        public GetImageDrawableTask(ImageView imageView) {
            mImageView = imageView;
            mContext = imageView.getContext();
            mResources = mImageView.getContext().getResources();
        }

        @SuppressLint("NewApi")
        @Override
        protected Drawable doInBackground(CustomGiftAnim.CustomAnimFrame... params) {
            try {
                CodeTimeRecord.iRecord.recordCodeStart("doInBackground anim task ");
                /**
                 * 这里先获取url
                 */
                CustomGiftAnim.CustomAnimFrame curFrame = params[0];
                if (curFrame == null) {
                    CodeTimeRecord.iRecord.recordCodeEnd("doInBackground anim task ");
                    return null;
                }
                if (curFrame == mLastFrame) {
                    CodeTimeRecord.iRecord.recordCodeEnd("doInBackground anim task ");
                    return null;
                }
                if (TextUtils.isEmpty(curFrame.local) && TextUtils.isEmpty(curFrame.remote)) {
                    CodeTimeRecord.iRecord.recordCodeEnd("doInBackground anim task ");
                    return null;
                }

                /**
                 * 先去本地找资源
                 */
                int localResource = ApkResourceManager.GetApkImageResource(mContext, curFrame.local);
                if (localResource > 0) {
                    /**
                     * 找到了这个Id则采用本地的InputStream
                     */
                    mRecycleBitmap = BitmapDecodeUtils.decodeBitmap(mContext, localResource);
                    BitmapDrawable drawable = new BitmapDrawable(mResources, mRecycleBitmap);
                    mLastFrame = curFrame;
                    Log.i(TAG, "doInBackground: use local resource");
                    Log.i(TAG, "doInBackground: bitmap size " + mRecycleBitmap.getByteCount());
                    return drawable;
                }

                /**
                 * 如果本地没有则采用remote
                 */
                InputStream inputStream = EffectsMainFestManager.GetGiftImageInputStream(curFrame.remote);
                if (inputStream != null) {
                    mRecycleBitmap = BitmapDecodeUtils.decodeBitmap(mContext, inputStream);
                } else {
                    CodeTimeRecord.iRecord.recordCodeEnd("doInBackground anim task ");
                    return null;
                }
                BitmapDrawable drawable = new BitmapDrawable(mResources, mRecycleBitmap);
                mLastFrame = curFrame;
                /**
                 * close inputStream
                 */
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG, "doInBackground: use remote resource");
                CodeTimeRecord.iRecord.recordCodeEnd("doInBackground anim task ");
                Log.i(TAG, "doInBackground: bitmap size " + mRecycleBitmap.getByteCount());
                return drawable;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Drawable result) {
            super.onPostExecute(result);
            if (isStop()) {
                return;
            }
            if (result != null) {
                mImageView.setImageDrawable(result);
            }
            if (mOnAnimationFrameChangedListener != null)
                mOnAnimationFrameChangedListener.onAnimationFrameChanged(mIndex);
        }

    }
}
