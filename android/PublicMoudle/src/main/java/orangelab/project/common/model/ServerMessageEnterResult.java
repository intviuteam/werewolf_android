package orangelab.project.common.model;

/**
 * Author:1727
 * 2017/3/7
 */

public class ServerMessageEnterResult {

    public int position;

    public EnterRoomResult.EnterRoomUserItem user;
}
