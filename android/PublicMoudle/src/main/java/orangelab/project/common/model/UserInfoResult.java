package orangelab.project.common.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import orangelab.project.common.family.model.FamilyInfo;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;

/**
 * Author:1727
 * 2017/3/31
 */

public class UserInfoResult implements Serializable {

    public String id;
    public String name;
    public Integer uid;
    public boolean is_friend;
    public String status = "";
    public int sex;
    public String image;
    public String signature;
    public boolean isBlock = false;
    public int popular = 0;
    public int be_liked = 0;
    public UserInfoGameResult game;
    public List<PhotoItem> photos;
    public List<GiftItem> gift;
    public UserLocation location;
    public UserRoleInfo role;
    public UserMoneyInfo money;
    public FamilyInfo group;
    public UserActiveInfo active;
    public MiniGameDatas mini_game_datas;
    public Undercover undercover;
    public AvatarBox avatar_box;
    public MessageBox message_box;
    public UserVipInfo vipInfo;

    public boolean userHasInRoom() {
        try {
            String[] data = status.split(":");
            String roomId = data[2];
            if (TextUtils.isEmpty(roomId)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isCanEnterRoomType() {
        String roomType = getRoomType();
        if (TextUtils.isEmpty(roomType)) {
            return false;
        }
        if (roomType.startsWith("game_")) {
            return false;
        }
        return true;
    }

    public String getRoomType() {
        try {
            String[] data = status.split(":");
            String roomId = data[3];
            if (TextUtils.isEmpty(roomId)) {
                return "";
            } else {
                return roomId;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getRoomId() {
        try {
            String[] data = status.split(":");
            String roomId = data[2];
            if (TextUtils.isEmpty(roomId)) {
                return "";
            } else {
                return roomId;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static class AvatarBox implements Keepable, Serializable {
        public String avatar_box_type;
    }

    public static class MessageBox implements Keepable, Serializable {
        public String message_box_type;
    }

    public static class UserActiveInfo implements Keepable, Serializable {
        public int level = 1;
        public int star = 1;
        public String title = "";
        public int experience;
        public String type;
        public int index = 1;
    }

    public static class UserRoleInfo implements Keepable, Serializable {
        public int type;
        public String custom;
        public List<String> customs;
    }

    public static class UserMoneyInfo implements Keepable, Serializable {
        public long gold;
        public long dim;
    }

    public static class UserLocation implements Keepable, Serializable {
        public String address;
    }

    public static class GiftItem implements Keepable, Serializable {
        public String type;
        public int count;
    }

    public static class PhotoItem implements Keepable, Serializable {
        public String url;
        public String id;
        public String name;
        public String thumbnail_url;
    }

    public static class MiniGameDatas implements Keepable, Serializable {
        public List<MiniGameDatasPopItem> pop;
        public MiniGameRank rank;
    }


    public static class MiniGameDatasPopItem implements Keepable, Serializable, Comparable<MiniGameDatasPopItem> {
        public String gameType;
        public MiniGameDatasPopItemCount count;
        public MiniGameDatasPopItemLevel level;
        public String name = "";
        public String icon = "";


        @Override
        public int compareTo(@NonNull MiniGameDatasPopItem o) {
            int myLevelInt = 0;
            try {
                myLevelInt = level != null ? level.val : 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
            int oLevelInt = 0;
            try {
                oLevelInt = o != null ? (o.level != null ? o.level.val : 0) : 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return oLevelInt - myLevelInt;
        }
    }

    public static class MiniGameDatasPopItemCount implements Keepable, Serializable {
        public int lost;
        public int win;
        public int total;
    }

    public static class MiniGameDatasPopItemLevel implements Keepable, Serializable {
        public int score;
        public int star;
        public int val;
    }

    public static class UserInfoGameResult implements Keepable, Serializable {
        public int win;
        public int lose;
        public int experience;
        public int level;
        public int escape;

        @Override
        public String toString() {
            return "UserInfoGameResult{" +
                    "win=" + win +
                    ", lose=" + lose +
                    ", experience=" + experience +
                    ", level=" + level +
                    ", excape=" + escape +
                    '}';
        }
    }

    /**
     * 谁是卧底二期
     */
    public static class Undercover implements Serializable, Keepable {
        public int win;
        public int lose;
    }

    public static class MiniGameRank implements Keepable, Serializable {
        public List<MiniGameRankItem> all;
    }

    public static class MiniGameRankItem implements Keepable, Serializable {
        public String gameType;
        public int rank;
    }

    @Override
    public String toString() {
        return "UserInfoResult{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", is_friend=" + is_friend +
                ", sex=" + sex +
                ", image='" + image + '\'' +
                ", signature='" + signature + '\'' +
                ", isBlock=" + isBlock +
                ", popular=" + popular +
                ", game=" + game +
                ", photos=" + photos +
                ", gift=" + gift +
                ", location=" + location +
                ", role=" + role +
                ", money=" + money +
                ", group=" + group +
                '}';
    }
}
