package orangelab.project.common.tool;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.toolkit.action.Destroyable;

/**
 * game_werewolf
 * 2017/10/26 下午5:02
 * Mystery
 */

public class RecycleViewPageHelper implements Destroyable {

    private static final String TAG = "RecycleViewPageHelper";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecycleViewPageHelperConfig mConfig;

    private int mCurPage = 0;
    private int mPageSize = 0;
    private RecycleViewPageHelperAction mAction = null;
    private int mLastVisibleItem = 0;
    private boolean isLoadingMore = false;

    public RecycleViewPageHelper(RecyclerView recyclerView, RecyclerView.Adapter adapter, RecycleViewPageHelperConfig config) {
        mRecyclerView = recyclerView;
        mAdapter = adapter;
        mConfig = config;
        mCurPage = config.pageIndex;
        mPageSize = config.pageSize;
        mAction = config.action;

        initView();
    }

    private void initView() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.i(TAG, "onScrollStateChanged: " + recyclerView.getHeight());
//                if (mAdapter == null)
//                    return;
//                if (newState == RecyclerView.SCROLL_STATE_IDLE && mLastVisibleItem + 1 == mAdapter.getItemCount()) {
//                    mCurPage++;
//                    if (mAction != null) {
//                        mAction.onNextPage(mCurPage, mPageSize, mAdapter);
//                    }
//                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mRecyclerView == null) {
                    return;
                }
                if (mRecyclerView.getLayoutManager() instanceof LinearLayoutManager) {
//                    LinearLayoutManager l = (LinearLayoutManager) mRecyclerView.getLayoutManager();
//                    mLastVisibleItem = l.findLastVisibleItemPosition();

                    int lastVisibleItem = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    int totalItemCount = mRecyclerView.getLayoutManager().getItemCount();
                    if (lastVisibleItem >= totalItemCount - 3 && dy > 0) {
                        if (isLoadingMore) {
                        } else {
                            isLoadingMore = true;
                            if (mAction != null) {
                                mAction.onNextPage(mCurPage, mPageSize, mAdapter);
                            }
                        }
                    }
                }
                if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
                    GridLayoutManager l = (GridLayoutManager) mRecyclerView.getLayoutManager();
                    mLastVisibleItem = l.findLastVisibleItemPosition();
                }
            }
        });
    }

    public void loadMoreFinish() {
        isLoadingMore = false;
    }

    public void addPage() {
        mCurPage++;
    }

    public void refreshPage() {
        mCurPage = 0;
        if (mAction != null) {
            mAction.onRefresh(mCurPage, mPageSize, mAdapter);
        }
    }


    @Override
    public void destroy() {
        mAdapter = null;
        mRecyclerView = null;
    }


    public static class RecycleViewPageHelperConfig {
        public boolean nextPageEnable = true;
        public int pageSize = 20;
        public int pageIndex = 0;
        public RecycleViewPageHelperAction action;

        private RecycleViewPageHelperConfig(Builder builder) {
            nextPageEnable = builder.nextPageEnable;
            pageSize = builder.pageSize;
            pageIndex = builder.pageIndex;
            action = builder.action;
        }


        public static final class Builder {
            private boolean nextPageEnable;
            private int pageSize;
            private int pageIndex;
            private RecycleViewPageHelperAction action;

            public Builder() {
            }

            public Builder nextPageEnable(boolean val) {
                nextPageEnable = val;
                return this;
            }

            public Builder pageSize(int val) {
                pageSize = val;
                return this;
            }

            public Builder pageIndex(int val) {
                pageIndex = val;
                return this;
            }

            public Builder action(RecycleViewPageHelperAction val) {
                action = val;
                return this;
            }

            public RecycleViewPageHelperConfig build() {
                return new RecycleViewPageHelperConfig(this);
            }
        }
    }

    public static interface RecycleViewPageHelperAction<AdapterDataType extends RecyclerView.ViewHolder> {

        void onRefresh(int pageIndex, int pageSize, RecyclerView.Adapter<AdapterDataType> mAdapter);

        void onNextPage(int pageIndex, int pageSize, RecyclerView.Adapter<AdapterDataType> mAdapter);
    }
}
