package orangelab.project.common.pay.google;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import orangelab.project.common.db.UserPayOrderDaoHelper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.google.util.IabHelper;
import orangelab.project.common.pay.google.util.Purchase;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import com.datasource.GlobalUserState;
import rx.Observable;
import rx.Subscriber;

/**
 * game_werewolf
 * 2017/8/19 下午1:24
 * Mystery
 */

public class GooglePayTask {

    private static final String TAG = "GooglePayTask";
    private Context mContext = null;

    private IabHelper mIabHelper = null;
    private boolean isSetUp = false;
    private UserPayOrderDaoHelper mDaoHelper = null;
    private SafeHandler mSafeHandler = null;

    public GooglePayTask(Context context) {
        this.mContext = context;
        mIabHelper = new IabHelper(context, "");
        mIabHelper.enableDebugLogging(PLog.needLog, TAG);
        mDaoHelper = new UserPayOrderDaoHelper(context);
        mSafeHandler = new SafeHandler(Looper.getMainLooper());
    }

    private void startSetup() {
        if (Utils.targetIsNotNull(mIabHelper) && !mIabHelper.getAsyncInProgress()) {
            mIabHelper.startSetup(result -> {
                if (!result.isSuccess()) {
                    Log.i(TAG, "Error:" + result);
                    executeAsyncPayDataTask();
                    return;
                }
                if (mIabHelper == null) {
                    executeAsyncPayDataTask();
                    return;
                }
                isSetUp = true;
                querySKU();
            });
        } else {
            executeAsyncPayDataTask();
        }
    }

    private void consumeAsync(Purchase purchase) {
        if (Utils.targetIsNotNull(mIabHelper) && !mIabHelper.getAsyncInProgress()) {
            PLog.i(TAG, "we begin consumeAsync ");
            mIabHelper.consumeAsync(purchase, (purchase1, result) -> {
                if (mIabHelper == null) {
                    return;
                }
                if (result.isFailure()) {
                    return;
                }
                PLog.i(TAG, "we finish consumeAsync ");
            });
        }
    }

    private void threadInfo() {
        PLog.i(TAG, "Thread Name =" + Thread.currentThread().getName());
    }

    private void querySKU() {
        if (Utils.targetIsNotNull(mIabHelper) && !mIabHelper.getAsyncInProgress()) {
            PLog.i(TAG, "startTask: queryInventoryAsync start");
            mIabHelper.queryInventoryAsync((result, inv) -> {
                PLog.i(TAG, "startTask: queryInventoryAsync finish");
                if (mIabHelper == null) {
                    executeAsyncPayDataTask();
                    return;
                }
                if (!result.isSuccess()) {
                    PLog.i(TAG, "startTask: queryInventoryAsync failed " + result.getMessage());
                    executeAsyncPayDataTask();
                    return;
                }
                if (Utils.targetIsNotNull(inv)) {
                    List<Purchase> purchases = inv.getAllPurchases();
                    if (!Utils.targetListIsNull(purchases)) {
                        ThreadToolKit.Async(() -> {
                            Observable.from(purchases)
                                    .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                                    .subscribe(new Subscriber<Purchase>() {
                                        @Override
                                        public void onStart() {
                                            PLog.i(TAG, "onStart: ");
                                            threadInfo();
                                        }

                                        @Override
                                        public void onCompleted() {
                                            PLog.i(TAG, "onCompleted: ");
                                            threadInfo();
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            PLog.i(TAG, "onError: " + e.getMessage());
                                            threadInfo();
                                        }

                                        @Override
                                        public void onNext(Purchase purchase) {
                                            threadInfo();
                                            UserPayOrderEntity entity = mDaoHelper.getUserPayOrderByOrderId(purchase.getOrderId());
                                            String token = "";

                                            if (entity != null) {
                                                /**
                                                 * 数据库中有这笔订单
                                                 */
                                                PLog.i(TAG, "find a Order " + entity.toString());
                                                token = entity.getOtherData2();
                                            } else {
                                                token = GlobalUserState.getGlobalState().getToken();
                                            }

                                            if (TextUtils.isEmpty(token)) {
                                                /**
                                                 * 当前token为空
                                                 */
                                                GooglePayOrderTask.reportErrorOrder("TokenNull", entity);
                                                PLog.i(TAG, "CurOrder Token is null");
                                                request(1);
                                                return;
                                            }
                                            JSONObject json = new JSONObject();
                                            try {
                                                json.put(Constant.SIGNATURE, purchase.getSignature());
                                                json.put(Constant.DATA, purchase.getOriginalJson());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            RequestTask mRequestTask = RequestTask.create(
                                                    false,
                                                    Constant.GOOGLE_PAY,
                                                    token,
                                                    json,
                                                    new RequestTaskCallBack() {
                                                        @Override
                                                        public void onSuccess(String response) {
                                                            threadInfo();
                                                            PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                                                            if (result.purchase.size() == 0) {
                                                                return;
                                                            } else {
                                                                PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                                                switch (item.getOp_result()) {
                                                                    case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                                                        Utils.runSafely(() -> {
                                                                            if (mDaoHelper != null) {
                                                                                mDaoHelper.deleteData(
                                                                                        GlobalUserState.getGlobalState().getUserId(),
                                                                                        purchase.getOrderId()
                                                                                );
                                                                            }
                                                                        });
                                                                    }
                                                                    if (mSafeHandler != null) {
                                                                        mSafeHandler.postSafely(() -> {
                                                                            PLog.i(TAG, "we finish report " + purchase);
                                                                            consumeAsync(purchase);
                                                                        });
                                                                    }
                                                                    break;
                                                                    default:
                                                                        PLog.i(TAG, "we finish report but report failed");
                                                                        break;
                                                                }
                                                            }
                                                            request(1);
                                                        }

                                                        @Override
                                                        public void onFailed(int code, String failed) {
                                                            PLog.i(TAG, "we finish report but report failed");
                                                            threadInfo();
                                                            Utils.runSafely(() -> {
                                                                GooglePayOrderTask.reportErrorOrder(
                                                                        "ServerError",
                                                                        purchase.getOrderId(),
                                                                        entity.getUserId()
                                                                );
                                                            });
                                                            request(1);
                                                        }

                                                        @Override
                                                        public void onError(Exception e) {
                                                            PLog.i(TAG, "we finish report but report failed");
                                                            threadInfo();
                                                            Utils.runSafely(() -> {
                                                                GooglePayOrderTask.reportErrorOrder(
                                                                        "NetWorkError",
                                                                        purchase.getOrderId(),
                                                                        entity.getUserId()
                                                                );
                                                            });
                                                            request(1);
                                                        }
                                                    });
                                            mRequestTask.executeInCurThread();
                                        }
                                    });
                            /**
                             * 执行完汇报任务之后执行 强制一次数据库异步检查任务
                             */
                            PLog.i(TAG, "we finish server report. begin database check");
                            executeAsyncPayDataTask();
                        });
                    } else {
                        executeAsyncPayDataTask();
                    }
                } else {
                    executeAsyncPayDataTask();
                }
            });
        } else {
            executeAsyncPayDataTask();
        }
    }

    /**
     * 内部错误处理，当Google查询记录等操作因内部错误被终端时会触发此方法
     */
    private void executeAsyncPayDataTask() {
        PLog.i(TAG, "executeAsyncPayDataTask || Google Query Finish begin Async DataBaseTask");
        Utils.executeAsyncPayDataReportAll(mContext);
    }

    public void startTask() {
        try {
            PLog.i(TAG, "start GooglePayTask");
            if (mIabHelper != null) {
                if (mIabHelper.isSetupDone()) {
                    if (mIabHelper.isServiceIsDisconnect()) {
                        mIabHelper.unbindService();
                        startSetup();
                    } else {
                        querySKU();
                        return;
                    }
                } else {
                    startSetup();
                }
            } else {
                executeAsyncPayDataTask();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            executeAsyncPayDataTask();
        }
    }

    public void release() {
        if (mDaoHelper != null) {
            mDaoHelper.release();
            mDaoHelper = null;
        }
        if (mIabHelper != null && !mIabHelper.isDisposed()) {
            mIabHelper.dispose();
            mIabHelper = null;
        }
        if (mSafeHandler != null) {
            mSafeHandler.release();
            mSafeHandler = null;
        }
        if (mContext != null) {
            mContext = null;
        }
    }
}
