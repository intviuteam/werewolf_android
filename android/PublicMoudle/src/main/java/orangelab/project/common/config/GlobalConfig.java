package orangelab.project.common.config;

import android.content.Context;

import cn.intviu.support.SharePreStoreUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/10/18 下午6:28
 * Mystery
 */

public class GlobalConfig {

    public static final String GLOBAL_CONFIG = "GLOBAL_CONFIG";
    public static final String GROUP_NOTICE_ENABLE = "GROUP_NOTICE_ENABLE";

    private static String GetGlobalConfigNameByUserId() {
        String result = "";
        result = GlobalUserState.getGlobalState().getUserId() + GLOBAL_CONFIG;
        return result;
    }

    public static boolean GetGroupNoticeDisable(Context context) {
        return SharePreStoreUtils.getBoolean(GetGlobalConfigNameByUserId(), context, GROUP_NOTICE_ENABLE, false);
    }

    public static void UpdateGroupNoticeDisable(Context context, boolean enable) {
        SharePreStoreUtils.save(GetGlobalConfigNameByUserId(), context, GROUP_NOTICE_ENABLE, enable);
    }


}
