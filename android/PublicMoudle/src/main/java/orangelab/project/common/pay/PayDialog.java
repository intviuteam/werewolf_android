package orangelab.project.common.pay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.MessageUtils;

import com.androidtoolkit.ToastToolKit;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * game_werewolf
 * 2017/7/21 下午12:20
 * Mystery
 */

public class PayDialog extends SafeDialog implements View.OnClickListener {

    private static final String TAG = "PayDialog";

    private Runnable mDismissRunnable;

    private List<PayItemView> mAllPayView;
    private Button mConfirm;
    private PayItemView mWeChatPay;
    private PayItemView mGooglePay;
    private Action1<String> mTypeListener;
    private boolean needNotify = true;

    public PayDialog(@NonNull Context context, Runnable dismissRunnable, Action1<String> typeListener) {
        super(context, R.style.DarkDialogAllowDismiss);
        this.mDismissRunnable = dismissRunnable;
        this.mTypeListener = typeListener;
        setContentView(R.layout.dialog_pay_layout);
        mAllPayView = new ArrayList<>();
        mWeChatPay = (PayItemView) findViewById(R.id.wechat_pay);
        mWeChatPay.updateView(PayUtils.WECHAT, R.mipmap.icon_wechat_pay, R.string.pay_by_wechat);
        mWeChatPay.setVisibility(View.GONE);
        mGooglePay = (PayItemView) findViewById(R.id.google_pay);
        mGooglePay.updateView(PayUtils.GOOGLE, R.mipmap.icon_googleplay, R.string.pay_by_google);
        mConfirm = (Button) findViewById(R.id.pay_confirm);
        mAllPayView.add(mWeChatPay);
        mAllPayView.add(mGooglePay);

        mConfirm.setOnClickListener(this);
        mGooglePay.setOnClickListener(this);
        mWeChatPay.setOnClickListener(this);

        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (needNotify) {
            if (this.mDismissRunnable != null) {
                this.mDismissRunnable.run();
            }
        }
    }

    private void clearSelectedState() {
        for (PayItemView temp : mAllPayView) {
            temp.setSelect(false);
        }
    }

    private PayItemView getSelectedView() {
        PayItemView result = null;
        for (PayItemView temp : mAllPayView) {
            if (temp.isSelected()) {
                result = temp;
                break;
            }
        }
        return result;
    }

    @Override
    protected void release() {
        mAllPayView.clear();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.pay_confirm) {
            /**
             * 确认支付
             */
            PayItemView selectView = getSelectedView();
            if (mTypeListener != null) {
                if (selectView != null) {
                    needNotify = false;
                    mTypeListener.call(selectView.getType());
                } else {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.pay_select_error));
                    //dismiss();
                }
            } else {
                dismiss();
            }
        } else if (id == R.id.wechat_pay) {
            /**
             * 微信支付
             */
            clearSelectedState();
            mWeChatPay.setSelect(true);
        } else if (id == R.id.google_pay) {
            /**
             * Google支付
             */
            clearSelectedState();
            mGooglePay.setSelect(true);
        }
//        switch (v.getId()) {
//            case R.id.pay_confirm:
//                /**
//                 * 确认支付
//                 */
//                PayItemView selectView = getSelectedView();
//                if (mTypeListener != null) {
//                    if (selectView != null) {
//                        needNotify = false;
//                        mTypeListener.call(selectView.getType());
//                    } else {
//                        ToastToolKit.showShort(MessageUtils.getString(R.string.pay_select_error));
//                        //dismiss();
//                    }
//                } else {
//                    dismiss();
//                }
//                break;
//            case R.id.wechat_pay:
//                /**
//                 * 微信支付
//                 */
//                clearSelectedState();
//                mWeChatPay.setSelect(true);
//                break;
//            case R.id.google_pay:
//                /**
//                 * Google支付
//                 */
//                clearSelectedState();
//                mGooglePay.setSelect(true);
//                break;
//        }
    }
}
