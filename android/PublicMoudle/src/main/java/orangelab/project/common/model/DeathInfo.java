package orangelab.project.common.model;

import java.util.ArrayList;

/**
 * Author:1727
 * 2017/3/10
 */

public class DeathInfo {

    public ArrayList<DeathInfoItem> death_info;
    public int duration;

    public static class DeathInfoItem {
        public int killed;
        public boolean dead = true;
        public String reason;
        public String killed_role;
        public int killer;
        public int real_dead = -1; //当dead 为false的时候 可以选择性的返回real_dead
        public String role;
    }

}
