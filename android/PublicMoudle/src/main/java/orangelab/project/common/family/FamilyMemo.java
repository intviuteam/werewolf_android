package orangelab.project.common.family;

import android.text.TextUtils;

import com.networktoolkit.transport.Constant;
import orangelab.project.common.family.model.FamilyInfo;
import orangelab.project.common.family.model.FamilyInfoApiResult;

import com.datasource.GlobalUserState;


/**
 * game_werewolf
 * 2017/10/27 下午2:24
 * Mystery
 */

public enum FamilyMemo {


    iMemo;

    private String mFamilyId;
    private String mFamilyGid;
    private String mFamilyLcId;
    private String mFamilyName;
    private String mFamilyShortName;
    private boolean mIsOwner;  //是否是家族族长
    private boolean mHasJoinFamily = false;

    private FamilyMemo() {
        quitFamily();
    }

    public void quitFamily() {
        mIsOwner = false;
        mHasJoinFamily = false;
        mFamilyId = Constant.EMPTY;
        mFamilyGid = Constant.EMPTY;
        mFamilyName = Constant.EMPTY;
        mFamilyShortName = Constant.EMPTY;
    }

    public boolean isHasJoinFamily() {
        return mHasJoinFamily;
    }

    public void updateMyFamilyId(String id) {
        if (!TextUtils.isEmpty(id)) {
            mFamilyId = id;
            mHasJoinFamily = true;
        } else {
            mFamilyId = "";
            mHasJoinFamily = false;
        }
    }

    public void updateMyFamilyGid(String gid) {
        mFamilyGid = gid;
    }

    public void updateMyFamilyName(String name) {
        mFamilyName = name;
    }

    public void updateMyFamilyShortName(String name) {
        mFamilyShortName = name;
    }

    public void updateOwner(boolean isOwner) {
        mIsOwner = isOwner;
    }

    public String getFamilyId() {
        return mFamilyId;
    }

    public String getFamilyLcId() {
        return mFamilyLcId;
    }

    public String getFamilyGid() {
        return mFamilyGid;
    }

    public String getFamilyName() {
        return mFamilyName;
    }

    public String getFamilyShortName() {
        return mFamilyShortName;
    }

    public boolean isOwner() {
        return mIsOwner;
    }

    public static void UpdateMemo(FamilyInfoApiResult result) {
        if (TextUtils.equals(result.group.own_id, GlobalUserState.getGlobalState().getUserId())) {
            /**
             * 这个家族的族长是自己
             */
            iMemo.updateOwner(true);
            iMemo.updateMyFamilyGid(result.group.gid);
            iMemo.updateMyFamilyId(result.group._id);
            iMemo.updateMyFamilyName(result.group.name);
            iMemo.updateMyFamilyShortName(result.group.short_name);
            iMemo.updateMyFamilyLcId(result.group.lc_id);
        } else {

        }
    }

    public void updateMyFamilyLcId(String id) {
        mFamilyLcId = id;
    }

    public static void ClearFamilyMemo() {
        iMemo.updateOwner(false);
        iMemo.updateMyFamilyId("");
        iMemo.updateMyFamilyGid("");
        iMemo.updateMyFamilyName("");
        iMemo.updateMyFamilyShortName("");
        iMemo.updateMyFamilyLcId("");
    }

    public static void UpdateMemo(FamilyInfo info) {
        if (info != null) {
            iMemo.updateOwner(info.is_owner);
            iMemo.updateMyFamilyGid(info.gid);
            iMemo.updateMyFamilyId(info.group_id);
            iMemo.updateMyFamilyName(info.name);
            iMemo.updateMyFamilyShortName(info.short_name);
            iMemo.updateMyFamilyLcId(info.lc_id);
        } else {
            ClearFamilyMemo();
        }
    }

    public static boolean IsMyFamilyOwner(String familyId) {
        return IsMyFamily(familyId) && iMemo.isOwner();
    }

    public static boolean IsMyFamily(String familyId) {
        if (TextUtils.isEmpty(familyId)) {
            return false;
        }
        return TextUtils.equals(familyId, iMemo.getFamilyId());
    }


}
