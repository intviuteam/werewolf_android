package orangelab.project.common.floatwindow.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * game_werewolf
 * 2017/12/27 下午3:28
 * Mystery
 */

public class FloatUser implements Keepable, Serializable {

    public String userId;
    public String userHead;
    public String userName;
}
