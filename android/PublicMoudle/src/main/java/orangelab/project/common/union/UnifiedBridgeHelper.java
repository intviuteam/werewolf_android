package orangelab.project.common.union;

import com.R;

import orangelab.project.common.model.LeanCloudConfig;
import orangelab.project.common.model.UpLoadFileResultParams;
import orangelab.project.common.model.UpLoadFinishItem;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/9/21 下午4:16
 * Mystery
 */

public class UnifiedBridgeHelper {

    public static void SendDeleteFriendAction(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_DELETE_FRIEND, jsonObject);
    }

    public static void SendAddFriendAction(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_ADD_FRIEND, jsonObject);
    }

    public static void SendLeaveAudioRoomAction() {
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_LEAVE_AUDIO_ROOM);
    }

    public static void SendBlackListAddAction(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_BLACK_LIST_ADD, jsonObject);
    }

    public static void SendLeanCloudServerConfig(LeanCloudConfig.LeanCloudSeverConfig config) {
        JSONObject json = null;
        try {
            json = new JSONObject(GsonHelper.getGson().toJson(config));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (json != null) {
            UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_LEANCLOUD_SERVER_CONFIG, json);
        }
    }

    /**
     * 通知RN分享成功
     */
    public static void SendMiniGameReportToRn() {
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.EVENT_SHARE_MINI_GAME_SUCCESS);
    }

    public static void SendTaskTimeOut(IUnifiedBridge bridge, String action) {
        if (bridge != null)
            bridge.SendCommendIntoRN(action, CreateCommonRequestResultJSON(-1, MessageUtils.getString(R.string.string_action_timeout)), false, false, 0);
    }

    public static void SendRequestCommonResultError(IUnifiedBridge bridge, String action, int code, String message) {
        if (bridge != null)
            bridge.SendCommendIntoRN(action, CreateCommonRequestResultJSON(code, message), false, false, 0);
    }

    public static void SendQuitFamilyToRn() {
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_QUIT_FAMILY);
    }

    public static void SendUpLoadSuccessResultToRn(IUnifiedBridge bridge, String action, ArrayList<UpLoadFinishItem> url) {
        if (bridge != null) {
            UpLoadFileResultParams params = new UpLoadFileResultParams();
            params.code = 0;
            params.message = "";
            params.data = url;
            try {
                bridge.SendCommendIntoRN(action, new JSONObject(GsonHelper.getGson().toJson(params)), false, false, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static JSONObject CreateCommonRequestResultJSON(int code, String message) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(UnifiedConstant.CODE, code);
            jsonObject.put(UnifiedConstant.MESSAGE, message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
