package orangelab.project.common.effect;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.toolkit.ToolKit;
import com.toolkit.action.Cancelable;
import com.toolkit.action.Destroyable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * game_werewolf
 * 2017/6/21 下午1:57
 * Mystery
 * <p>
 * 基于属性动画实现的礼物特效View
 */

public class EffectsTaskManager {

    private static final String TAG = "AnimationManager";
    public static final String TASK_GIFT = "task_gift";
    public static final String TASK_REBATE = "task_rebate";
    public static final String TASK_PRAISE = "task_praise";
    public static final String TASK_REMAIN = "task_remain";

    /**
     * 残留的礼物特效
     */
    public static class RemainTask implements Cancelable, Destroyable {
        private int mPosition;
        private Animator mAnimator;
        private IFasterAnim mIFasterAnim;
        private View mView;
        private boolean isCancel = false;

        public RemainTask(int position) {
            mPosition = position;
        }

        public Animator getAnimator() {
            return mAnimator;
        }

        public void setAnimator(Animator animator) {
            mAnimator = animator;
        }

        public IFasterAnim getIFasterAnim() {
            return mIFasterAnim;
        }

        public void setIFasterAnim(IFasterAnim IFasterAnim) {
            mIFasterAnim = IFasterAnim;
        }

        public View getView() {
            return mView;
        }

        public void setView(View view) {
            mView = view;
        }

        public boolean isCancel() {
            return isCancel;
        }

        public int getPosition() {
            return mPosition;
        }

        @Override
        public void cancel() {
            if (isCancel) return;

            if (mAnimator != null && mAnimator.isRunning()) {
                mAnimator.cancel();
                mAnimator = null;
            }

            if (mIFasterAnim != null) {
                mIFasterAnim.stop();
                mIFasterAnim.release();
            }

            isCancel = true;
        }

        @Override
        public void destroy() {
            cancel();
            mView = null;
        }
    }

    public static class Task {
        public Animator mAnimator;
        public List<IFasterAnim> mIFasterAnim;
        public View mView;
        public boolean isCancel = false;
        private String type = TASK_GIFT;

        public void setType(String type) {
            this.type = type;
        }

        public boolean isGift() {
            return TextUtils.equals(type, TASK_GIFT);
        }

        public void setCancel(boolean cancel) {
            isCancel = cancel;
        }

        public Task() {
            mIFasterAnim = new ArrayList<>();
        }

        public void addIFasterAnim(IFasterAnim IFasterAnim) {
            mIFasterAnim.add(IFasterAnim);
        }

        public void addIFasterAnim(IFasterAnim... IFasterAnims) {
            mIFasterAnim.addAll(Arrays.asList(IFasterAnims));
        }

        public void addIFasterAnim(List<IFasterAnim> all) {
            mIFasterAnim.addAll(all);
        }

        public void clear() {
            for (IFasterAnim container : mIFasterAnim) {
                container.stop();
                container.release();
            }
            mIFasterAnim.clear();
        }

        public void cancelAllAnim() {
            if (mAnimator != null && mAnimator.isRunning()) {
                mAnimator.cancel();
            }
        }
    }

    private Context mContext;

    private ViewGroup rootView;

    private List<Task> animatorTasks = new LinkedList<>();
    private List<Task> animatorTasksCache = new LinkedList<>();

    private List<Task> otherAnimatorTasks = new LinkedList<>();
    private List<Task> otherAnimatorTasksCache = new LinkedList<>();

    private Map<Integer, RemainTask> mRemainTaskMap = new HashMap<>(0);

    private static final int SYNC_MAX_TASK = 3; //同时运行的动画最多数量
    private static final int CACHE_TASK_MAX_SIZE = 20;

    private int curRunningTask = 0;
    private int curOtherRunningTask = 0;

    private boolean isOnDestory;


    public EffectsTaskManager(Context mContext, ViewGroup rootView) {
        this.mContext = mContext;
        this.rootView = rootView;
    }

    private boolean addTaskIntoCacheIfNeed(Task task) {
        boolean result = false;

        if (task.isGift()) {
            if (curRunningTask >= SYNC_MAX_TASK) {
                /**
                 * 这里需要增加额外判断，如果当前缓存已经达到最大，那么就移除最前面的一个，然后添加进入缓存
                 */
                if (animatorTasksCache.size() >= CACHE_TASK_MAX_SIZE) {
                    animatorTasksCache.remove(0);
                    Log.i(TAG, "addTaskIntoCacheIfNeed: remove(0) from Gift cache");
                }
                animatorTasksCache.add(task);
                Log.i(TAG, "addTaskIntoCacheIfNeed: Gift cacheSize " + animatorTasksCache.size());
                result = true;
            }
        } else {
            if (curOtherRunningTask >= SYNC_MAX_TASK) {
                /**
                 * 这里需要增加额外判断，如果当前缓存已经达到最大，那么就移除最前面的一个，然后添加进入缓存
                 */
                if (otherAnimatorTasksCache.size() >= CACHE_TASK_MAX_SIZE) {
                    otherAnimatorTasksCache.remove(0);
                    Log.i(TAG, "addTaskIntoCacheIfNeed: remove(0) from Other cache");
                }
                otherAnimatorTasksCache.add(task);
                Log.i(TAG, "addTaskIntoCacheIfNeed: Other cacheSize " + otherAnimatorTasksCache.size());
                result = true;
            }
        }
        return result;
    }

    private void clearTaskWhenFinishOrCancel(Task task) {
        try {
            task.clear();
            if (task.isGift()) {
                if (!isOnDestory) {
                    animatorTasks.remove(task);
                    Log.i(TAG, "clearTaskWhenFinishOrCancel: remove a Gift task ");
                    curRunningTask--;
                    try {
                        rootView.removeView(task.mView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, "clearTaskWhenFinishOrCancel: clear a Gift Task Normal");
                    if (curRunningTask < SYNC_MAX_TASK) {
                        if (animatorTasksCache.size() > 0) {
                            addAnimation(animatorTasksCache.remove(0));
                        }
                    }
                }
            } else {
                if (!isOnDestory) {
                    otherAnimatorTasks.remove(task);
                    Log.i(TAG, "clearTaskWhenFinishOrCancel: remove a Other task ");
                    curOtherRunningTask--;
                    try {
                        rootView.removeView(task.mView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, "clearTaskWhenFinishOrCancel: clear a Other Task Normal");
                    if (curOtherRunningTask < SYNC_MAX_TASK) {
                        if (otherAnimatorTasksCache.size() > 0) {
                            addAnimation(otherAnimatorTasksCache.remove(0));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearRemainTaskAfterFinishOrCancel(RemainTask task) {
        try {
            if (!isOnDestory) {
                rootView.removeView(task.getView());
                task.destroy();
                mRemainTaskMap.remove(task.getPosition());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addRemainTask(RemainTask task) {
        final RemainTask finalRemainTask = task;
        mRemainTaskMap.put(finalRemainTask.getPosition(), finalRemainTask);
        finalRemainTask.getAnimator().addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                Log.i(TAG, "RemainTask onAnimationCancel[" + this.hashCode() + "]: ");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.i(TAG, "RemainTask onAnimationEnd[" + this.hashCode() + "]: ");
                clearRemainTaskAfterFinishOrCancel(finalRemainTask);
            }
        });
        rootView.addView(finalRemainTask.getView());
        finalRemainTask.getAnimator().start();
    }

    public void removeRemainAnim(int position) {
        final RemainTask mTemp = mRemainTaskMap.get(position);
        if (mTemp != null) {
            clearRemainTaskAfterFinishOrCancel(mTemp);
        }
    }

    public void addRemainAnim(RemainTask remainTask) {
        try {
            if (remainTask == null || remainTask.getView() == null || remainTask.getAnimator() == null) {
                return;
            }
            if (!isOnDestory) {
                /**
                 * 需要添加一个RemainTask
                 */
                final RemainTask mTemp = mRemainTaskMap.get(remainTask.getPosition());
                if (mTemp == null) {

                    /**
                     * 还没有添加过这个动画任务，则直接添加并开始
                     */
                    addRemainTask(remainTask);

                } else {

                    /**
                     * 已经添加过这个动画则先停止之前的动画
                     */
                    clearRemainTaskAfterFinishOrCancel(mTemp);

                    /**
                     * 添加这个任务
                     */
                    addRemainTask(remainTask);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addAnimation(Task task) {
        if (task == null || task.mView == null || task.mAnimator == null) {
            return;
        }
        if (!isOnDestory) {
            try {
                if (addTaskIntoCacheIfNeed(task)) {
                    /**
                     * 此处如果发现任务进入了缓存那么就直接结束
                     */
                    return;
                }
                task.mAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        clearTaskWhenFinishOrCancel(task);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        clearTaskWhenFinishOrCancel(task);
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                });
                try {
                    rootView.addView(task.mView);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                if (task.isGift()) {
                    animatorTasks.add(task);
                    task.mAnimator.start();
                    curRunningTask++;
                    Log.i(TAG, "addAnimation: normal start a Task");
                } else {
                    otherAnimatorTasks.add(task);
                    task.mAnimator.start();
                    curOtherRunningTask++;
                    Log.i(TAG, "addAnimation: other start a Task");
                }
            } catch (Exception e) {
                e.printStackTrace();
                clearTaskForce(task);
            }
        }
    }

    private void clearTaskForce(Task task) {
        try {
            if (task == null) {
                return;
            }
            task.clear();
            if (animatorTasks.contains(task)) {
                animatorTasks.remove(task);
                curRunningTask--;
            }
            if (otherAnimatorTasks.contains(task)) {
                otherAnimatorTasks.remove(task);
                curOtherRunningTask--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (task.mView != null && task.mView.getParent() != null) {
                /**
                 * 这个View已经被添加了
                 */
                rootView.removeView(task.mView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 销毁方法
     */
    public void onDestroy() {
        //标记已经取消了
        isOnDestory = true;
        //对所有的动画set进行取消
        for (Task task : animatorTasks) {
            task.cancelAllAnim();
            task.clear();
        }
        for (Task task : animatorTasksCache) {
            task.clear();
        }
        for (Task task : otherAnimatorTasks) {
            task.cancelAllAnim();
            task.clear();
        }
        for (Task task : otherAnimatorTasksCache) {
            task.clear();
        }

        ToolKit.travelMap(mRemainTaskMap, (key, value) -> {
            value.destroy();
        });
        mRemainTaskMap.clear();

        curOtherRunningTask = 0;
        curRunningTask = 0;

        animatorTasks.clear();
        animatorTasks = null;

        animatorTasksCache.clear();
        animatorTasksCache = null;

        otherAnimatorTasks.clear();
        otherAnimatorTasks = null;

        otherAnimatorTasksCache.clear();
        otherAnimatorTasksCache = null;

        mRemainTaskMap.clear();
        mRemainTaskMap = null;
        Log.i(TAG, "onDestroy: clear all task");
        //手动调用gc
        System.gc();
    }


}
