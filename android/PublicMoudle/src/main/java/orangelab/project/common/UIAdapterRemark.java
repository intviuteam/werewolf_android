package orangelab.project.common;

/**
 * game_werewolf
 * 2018/2/27 上午10:50
 * Mystery
 */

public interface UIAdapterRemark {
    /**
     * 一个标记 代表了 实现这个接口的类在将来 进行 适配方案有更换的时候 需要将实现接口的此类进行 特别处理
     */
}
