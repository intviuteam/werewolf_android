package orangelab.project.common.exhibition;

/**
 * game_werewolf
 * 2017/6/26 下午6:17
 * Mystery
 */

public interface IExhibition {

    String getExhibitionType();

    String getRealType();

    int getExhibitionBackGroundResource();

    String getExhibitionName();

    String getExhibitionPriceType();

    int getExhibitionPrice();

    void present(ExhibitionPackage toPosition);

    int getExhibitionCount();

    void setExhibitionTicket(String ticket);

    void setPopularValue(int value);

    int getPopularValue();

    String getDisCount();

    void setDisCount(String disCount);

    void decCount();

}
