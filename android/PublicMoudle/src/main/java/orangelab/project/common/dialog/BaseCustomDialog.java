package orangelab.project.common.dialog;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.R;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Author:1727
 * 2017/3/15
 * 自定义基本对话框
 */

public class BaseCustomDialog extends SafeDialog {

    private static final String TAG = "BaseCustomDialog";

    public static final int TWO_BUTTON = 2;
    public static final int ONE_BUTTON = 1;
    public static final int NONE_BUTTON = 0;
    public static final int THREE_BUTTON = 3;

    private ViewGroup title;
    private ViewGroup content;
    private ViewGroup background;
    private View dialogContainer;


    private View buttonContainer;

    private Button buttonConfirm;
    private Button buttonCancel;

    private Button buttonOnlyConfirm;

    private Button buttonLeft;
    private Button buttonRight;
    private Button buttonMid;

    private View oneButtonContainer;
    private View twoButtonContainer;
    private View threeButtonContainer;

    private final int dialogMaxWidth = ScreenUtils.dip2px(355);

    public void setConfirmString(String string) {
        buttonConfirm.setText(string);
    }

    public void setCancelString(String string) {
        buttonCancel.setText(string);
    }

    public void setOneButtonEnable(boolean flag) {
        buttonOnlyConfirm.setEnabled(flag);
    }

    public BaseCustomDialog(Context context, boolean disableAnim) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_base_custom_dialog);
        background = (ViewGroup) findViewById(R.id.base_custom_dialog_bg);
        content = (ViewGroup) findViewById(R.id.base_custon_dialog_content);
        title = (ViewGroup) findViewById(R.id.base_custon_dialog_title);
        dialogContainer = findViewById(R.id.dialog_container);
        buttonContainer = findViewById(R.id.dialog_button_container);

        buttonConfirm = (Button) findViewById(R.id.dialog_confirm);
        buttonCancel = (Button) findViewById(R.id.dialog_cancel);

        buttonOnlyConfirm = (Button) findViewById(R.id.dialog_only_confirm);

        buttonLeft = (Button) findViewById(R.id.three_button_left);
        buttonMid = (Button) findViewById(R.id.three_button_mid);
        buttonRight = (Button) findViewById(R.id.three_button_right);

        oneButtonContainer = findViewById(R.id.one_button_container);
        twoButtonContainer = findViewById(R.id.two_button_container);
        threeButtonContainer = findViewById(R.id.three_button_container);
        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();

        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.98f);
        }
        getWindow().setAttributes(params);
    }

    public BaseCustomDialog(Context context) {
        this(context, false);
    }

    @Override
    protected void release() {

    }

    public void setButtonConfirmText(String text) {
        buttonConfirm.setText(text);
    }

    public void setButtonCancelText(String text) {
        buttonCancel.setText(text);
    }

    public void setButtonOnlyConfirmText(String text) {
        buttonOnlyConfirm.setText(text);
    }

    public void setOnlyButtonListener(View.OnClickListener listener) {
        buttonOnlyConfirm.setOnClickListener(listener);
    }

    public void setConfirmListener(View.OnClickListener listener) {
        buttonConfirm.setOnClickListener(listener);
    }

    public void setCancelListener(View.OnClickListener listener) {
        buttonCancel.setOnClickListener(listener);
    }

    public void setButtonLeftListener(View.OnClickListener listener) {
        buttonLeft.setOnClickListener(listener);
    }

    public void setButtonMidListener(View.OnClickListener listener) {
        buttonMid.setOnClickListener(listener);
    }

    public void setButtonRightListener(View.OnClickListener listener) {
        buttonRight.setOnClickListener(listener);
    }

    public void setButtonType(int type) {
        switch (type) {
            case TWO_BUTTON:
                buttonContainer.setVisibility(View.VISIBLE);
                oneButtonContainer.setVisibility(View.GONE);
                twoButtonContainer.setVisibility(View.VISIBLE);
                threeButtonContainer.setVisibility(View.GONE);
                break;
            case ONE_BUTTON:
                buttonContainer.setVisibility(View.VISIBLE);
                oneButtonContainer.setVisibility(View.VISIBLE);
                twoButtonContainer.setVisibility(View.GONE);
                threeButtonContainer.setVisibility(View.GONE);
                break;
            case NONE_BUTTON:
                buttonContainer.setVisibility(View.GONE);
                oneButtonContainer.setVisibility(View.GONE);
                twoButtonContainer.setVisibility(View.GONE);
                threeButtonContainer.setVisibility(View.GONE);
                break;
            case THREE_BUTTON:
                buttonContainer.setVisibility(View.VISIBLE);
                oneButtonContainer.setVisibility(View.GONE);
                twoButtonContainer.setVisibility(View.GONE);
                threeButtonContainer.setVisibility(View.VISIBLE);
                break;
        }
    }

    public Button getButtonConfirm() {
        return buttonConfirm;
    }

    public Button getButtonCancel() {
        return buttonCancel;
    }

    public Button getButtonOnlyConfirm() {
        return buttonOnlyConfirm;
    }

    public Button getButtonLeft() {
        return buttonLeft;
    }

    public Button getButtonRight() {
        return buttonRight;
    }

    public Button getButtonMid() {
        return buttonMid;
    }

    public void addTitleView(View view) {
        if (view != null) {
            title.addView(view);
        }
    }

    public void addContentView(View view) {
        if (view != null) {
            content.addView(view);
        }
    }

    public void showDialog() {
        if (!isShowing()) {
            this.show();
        }
    }


}
