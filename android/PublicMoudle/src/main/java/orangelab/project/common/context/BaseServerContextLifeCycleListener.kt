package orangelab.project.common.context

import org.json.JSONObject

/**
 *game_werewolf
 *2018/3/19 上午11:01
 *Mystery
 */
interface BaseServerContextLifeCycleListener {

    fun onStart(type: String, json: JSONObject)

    fun onEnd(type: String, json: JSONObject)

    fun onError(e: Exception)
}