package orangelab.project.common.effect;

import android.content.Context;
import android.graphics.PointF;
import android.text.TextUtils;
import android.widget.ImageView;

import com.androidtoolkit.RxToolKit;
import com.facebook.react.bridge.Callback;
import com.R;

import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.exhibition.ExhibitionsUtils;
import orangelab.project.common.exhibition.card.CardUtils;
import orangelab.project.common.exhibition.gift.Gift;
import orangelab.project.common.effect.gifts.CustomGiftAnimPayload;
import orangelab.project.common.utils.MessageUtils;

import com.toolkit.action.Action;

import java.io.InputStream;
import java.util.List;

/**
 * game_werewolf
 * 2017/11/10 下午6:11
 * Mystery
 * <p>
 * 特效的总管理器
 * <p>
 */

public enum EffectsManager {


    INSTANCE;

    private static final String TAG = "EffectsManager";

    private GiftsEffectsManager mGiftsEffectsManager;
    private HeadDecorEffectsManager mHeadDecorEffectsManager;
    private MsgDecorEffectsManager mMsgDecorEffectsManager;
    private EmotionEffectsManager mEmotionEffectsManager;

    private EffectsManager() {
        mGiftsEffectsManager = new GiftsEffectsManager();
        mHeadDecorEffectsManager = new HeadDecorEffectsManager();
        mMsgDecorEffectsManager = new MsgDecorEffectsManager();
        mEmotionEffectsManager = new EmotionEffectsManager();
    }

    public static void Init(Context context) {
        /**
         * 初始化礼物特效
         */
        EffectsMainFestManager.Init(context);

        INSTANCE.mMsgDecorEffectsManager.bindHolder();
    }

    public static void RefreshEmotionManiFest(Action<Integer> callback) {
        INSTANCE.mEmotionEffectsManager.refreshEmotionManifest(callback);
    }

    public static void RefreshEmotionManifestForEmotions(Action<Integer> callback) {
        INSTANCE.mEmotionEffectsManager.refreshEmotionManifestForEmotions(callback);
    }

    public static void RefreshHeadDecorManiFest(Action<Integer> callback) {
        INSTANCE.mHeadDecorEffectsManager.refreshHeadDecorManifest(callback);
    }

    public static void RefreshGiftsManifestForExhibition(Action<Integer> callback) {
        INSTANCE.mGiftsEffectsManager.refreshGiftsManifestForExhibition(callback);
    }

    public static void RefreshGiftsManifest(Action<Integer> callback) {
        INSTANCE.mGiftsEffectsManager.refreshGiftsManifest(callback);
    }

    public static void RefreshMsgDecorManifest(Action<Integer> callback) {
        INSTANCE.mMsgDecorEffectsManager.refreshMsgDecorManifest(callback);
    }

    public static InputStream GetMsgDecorInputStream(String url) {
        return INSTANCE.mMsgDecorEffectsManager.getInputStream(url);
    }

    public static InputStream GetEmotionInputStream(String url) {
        return INSTANCE.mEmotionEffectsManager.getInputStream(url);
    }

    public static MsgDecorManiFest.MsgDecorManiFestItem GetMsgDecorItem(String type) {
        return INSTANCE.mMsgDecorEffectsManager.findItem(type);
    }

    public static void QueryGiftList(Action<List<Gift>> giftListCallback) {
        INSTANCE.mGiftsEffectsManager.queryGiftsManifest(giftListCallback);
    }

    public static void QueryEmotionList(Action<List<EmotionManiFest.Emotion>> emotionListCallback) {
        INSTANCE.mEmotionEffectsManager.queryEmotionManifest(emotionListCallback);
    }

    public static void FillGiftImageView(String giftType, ImageView imageView) {
        INSTANCE.mGiftsEffectsManager.fillGiftImageView(giftType, imageView);
    }

    public static String GetCardNameByType(String type) {
        return CardUtils.getCardName(type);
    }

    public static String GetGiftNameByType(String type) {
        return INSTANCE.mGiftsEffectsManager.getGiftName(type);
    }

    public static int GetGiftPopular(String type) {
        return INSTANCE.mGiftsEffectsManager.getGiftPopular(type);
    }

    public static String GetSupportedTypeName(String giftType) {
        String result = GetCardNameByType(giftType);
        if (TextUtils.isEmpty(result)) {
            result = GetGiftNameByType(giftType);
        }
        if (TextUtils.isEmpty(result)) {
            result = MessageUtils.getString(R.string.string_gift_unsupported);
        }
        return result;
    }

    public static boolean SupportThisCard(String cardType) {
        if (ApkResourceManager.GetApkCardResource(cardType) != -1) {
            return true;
        }
        return false;
    }

    public static boolean SupportThisGift(String giftType) {
        return INSTANCE.mGiftsEffectsManager.isSupportType(giftType);
    }

    public static boolean SupportThisGiftType(String giftType) {
        /**
         * 判断是否支持所有礼物
         */
        if (TextUtils.isEmpty(giftType)) {
            return false;
        }
        if (SupportThisCard(giftType)) {
            return true;
        }

        if (SupportThisGift(giftType)) {
            return true;
        }
        return false;
    }

    public static AnimationEvent.AnimationTaskEvent CreateRebateAnimEvent(Context context, String rebate, PointF startPoint, PointF endPoint, int startO, int endO, int width, int height) {
        CustomGiftAnimPayload giftAnimPayload = new CustomGiftAnimPayload();
        giftAnimPayload.start = startPoint;
        giftAnimPayload.end = endPoint;
        giftAnimPayload.startOrientation = startO;
        giftAnimPayload.endOrientation = endO;
        giftAnimPayload.viewHeight = height;
        giftAnimPayload.viewWidth = width;
        return EffectsTaskFactory.INSTANCE.createRebateAnim(context, rebate, giftAnimPayload);
    }

    public static AnimationEvent.AnimationTaskEvent CreatePriaseAnimEvent(Context context, PointF startPoint, PointF endPoint, int viewWidht, int viewHeight) {
        CustomGiftAnimPayload giftAnimPayload = new CustomGiftAnimPayload();
        giftAnimPayload.start = startPoint;
        giftAnimPayload.end = endPoint;
        giftAnimPayload.startOrientation = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;
        giftAnimPayload.endOrientation = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;
        giftAnimPayload.viewHeight = viewWidht;
        giftAnimPayload.viewWidth = viewHeight;
        return EffectsTaskFactory.INSTANCE.createPraiseAnim(context, giftAnimPayload);
    }

    public static AnimationEvent.AnimationTaskEvent CreateAnimEvent(Context context, String fromType, String giftType, PointF startPoint, PointF endPoint, int startO, int endO, int width, int
            height, int startPosition, int endPosition) {
        /**
         * 如果本地支持，那么就用本地的特效
         */
        if (!SupportThisGiftType(giftType)) {
            /**
             * 不支持
             */
            return null;
        }


        CustomGiftAnimPayload giftAnimPayload = new CustomGiftAnimPayload();
        giftAnimPayload.start = startPoint;
        giftAnimPayload.end = endPoint;
        giftAnimPayload.startOrientation = startO;
        giftAnimPayload.endOrientation = endO;
        giftAnimPayload.viewHeight = height;
        giftAnimPayload.viewWidth = width;
        giftAnimPayload.startPosition = startPosition;
        giftAnimPayload.endPosition = endPosition;
        return EffectsTaskFactory.INSTANCE.createAnimByGiftAnim(fromType, context, EffectsMainFestManager.FindGiftAnimFromGiftManifest(giftType), giftAnimPayload);
    }

    /**
     * @param context
     * @param fromType   从那个房间类型发送的
     * @param giftType   礼物类型
     * @param startPoint 起点坐标
     * @param endPoint   终点坐标
     * @param startO
     * @param endO
     * @param width      动画起始宽度
     * @param height     动画起始高度
     * @param startP     发送者的位置（position）*
     * @param endP       接收者的位置（position）*
     *                   <p>
     *                   带*的为后续的添加，目的是为了区分那个位置送给那个位置，将来拓展可以将position作为一个 start 和 end 的一个 key
     */
    public static void DispatchAnimEvent(Context context, String fromType, String giftType, PointF startPoint, PointF endPoint, int startO, int endO, int width, int height, int startP, int endP) {
        AnimationEvent.AnimationTaskEvent animEvent = CreateAnimEvent(context, fromType, giftType, startPoint, endPoint, startO, endO, width, height, startP, endP);
        if (animEvent != null) {
            RxToolKit.Emit(animEvent);
        }
    }

    public static void DispatchPraiseAnimEvent(Context context, PointF startPoint, PointF endPoint, int width, int height) {
        AnimationEvent.AnimationTaskEvent animEvent = CreatePriaseAnimEvent(context, startPoint, endPoint, width, height);
        if (animEvent != null) {
            RxToolKit.Emit(animEvent);
        }
    }

    public static void DispatchRebateAnimEvent(Context context, String rebate, PointF startPoint, PointF endPoint, int startO, int endO, int width, int height) {
        AnimationEvent.AnimationTaskEvent animEvent = CreateRebateAnimEvent(context, rebate, startPoint, endPoint, startO, endO, width, height);
        if (animEvent != null) {
            RxToolKit.Emit(animEvent);
        }
    }

    public static boolean IsCardByGiftType(String type) {
        return CardUtils.IsCard(type);
    }

    public static boolean IsCardByExhibitionsType(String type) {
        return ExhibitionsUtils.isCard(type);
    }

    public static void FindGiftInfoForReactNativeAsync(String giftType, Callback callback) {
        INSTANCE.mGiftsEffectsManager.findGiftForReactNative(giftType, callback);
    }
}
