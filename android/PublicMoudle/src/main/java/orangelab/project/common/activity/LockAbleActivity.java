package orangelab.project.common.activity;

import android.graphics.Rect;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * game_werewolf
 * 2017/10/27 下午3:07
 * Mystery
 */

public abstract class LockAbleActivity extends SafeActivity {

    private View.OnTouchListener mBlockListener = (v, event) -> true;

    protected boolean lockAble = false;
    protected List<Rect> unlockedRectList;
    protected Map<View, Rect> lockedRectList;
    protected ViewGroup mRootView;
    protected RelativeLayout mBlockLoadingView;

    protected void executeActivityDrawFinish(Runnable runnable) {
        getWindow().getDecorView().post(runnable);
    }

    protected void initLoading() {
        initLoading(0, 0, 0, 0);
    }

    protected void showBlockLoading() {
        if (mBlockLoadingView == null) {
            return;
        }
        try {
            if (mRootView != null) {
                if (mBlockLoadingView != null && mBlockLoadingView.getParent() != null) {
                    mRootView.removeView(mBlockLoadingView);
                }
                mRootView.addView(mBlockLoadingView);
            }
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * 如果这里抛出了异常,暂时不做处理
             */
        }
    }

    protected void dismissBlockLoading() {
        if (mRootView != null && mBlockLoadingView != null) {
            mRootView.removeView(mBlockLoadingView);
        }
    }

    protected void initLoading(int marginTop, int marginLeft, int marginRight, int marginBottom) {
        try {
            mRootView = ((ViewGroup) findViewById(android.R.id.content));
            mBlockLoadingView = (RelativeLayout) View.inflate(this, R.layout.layout_common_loading_view, null);
            mBlockLoadingView.setOnTouchListener(mBlockListener);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mBlockLoadingView.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
            layoutParams.gravity = Gravity.NO_GRAVITY;
            layoutParams.topMargin = marginTop;
            layoutParams.leftMargin = marginLeft;
            layoutParams.rightMargin = marginRight;
            layoutParams.bottomMargin = marginBottom;
            mBlockLoadingView.setLayoutParams(layoutParams);
            mBlockLoadingView.requestLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void addUnLockViewRect(View view) {
        if (view != null) {
            if (unlockedRectList == null) {
                unlockedRectList = new ArrayList<>();
            }
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int left = location[0];
            int top = location[1];
            int right = view.getWidth();
            int bottom = view.getHeight();
            Rect rect = new Rect(left, top, right, bottom);
            unlockedRectList.add(rect);
        }
    }

    protected void requestAllLock() {
        lockAble = true;
    }

    protected void requestAllUnLock() {
        if (lockAble) {
            lockAble = false;
            unlockedRectList.clear();
        }
    }

    protected void removeLockViewRect(View view) {
        if (lockedRectList != null) {
            lockedRectList.remove(view);
        }
    }

    protected void addLockViewRect(View view) {
        if (view != null) {
            if (lockedRectList == null) {
                lockedRectList = new HashMap<>();
            }
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int left = location[0];
            int top = location[1];
            int right = view.getWidth();
            int bottom = view.getHeight();
            Rect rect = new Rect(left, top, right, bottom);
            lockedRectList.put(view, rect);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!lockAble) {
            if (filterLock(ev)) {
                return true;
            } else {
                return super.dispatchTouchEvent(ev);
            }
        }

        if (filterUnLock(ev)) {
            return super.dispatchTouchEvent(ev);
        } else {
            /**
             * 直接消化掉事件
             */
            return true;
        }
    }

    protected boolean filterLock(MotionEvent ev) {
        float x = ev.getX();
        float y = ev.getY();
        boolean result = false;
        if (lockedRectList != null) {
            Iterator iterator = lockedRectList.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<View, Rect> entry = (Map.Entry<View, Rect>) iterator.next();
                Rect temp = entry.getValue();
                if (x - temp.left <= temp.right && y - temp.top <= temp.bottom) {
                    /**
                     * 触点点击到了lock位置
                     */
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    protected boolean filterUnLock(MotionEvent ev) {
        float x = ev.getX();
        float y = ev.getY();
        boolean result = false;
        for (Rect temp : unlockedRectList) {
            if (x - temp.left <= temp.right && y - temp.top <= temp.bottom) {
                /**
                 * 触点点击到了unlock位置
                 */
                result = true;
                break;
            }
        }

        return result;
    }

    @Override
    public void finish() {
        super.finish();
        requestAllUnLock();
        if (mRootView != null) {
            mRootView = null;
        }
        if (mBlockLoadingView != null) {
            mBlockLoadingView = null;
        }
    }
}
