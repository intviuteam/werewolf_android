package orangelab.project.common.family.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.common.family.FamilyConstant;
import orangelab.project.common.family.model.FamilyInfoApiResult;
import orangelab.project.common.utils.MessageUtils;

import com.androidtoolkit.PicassoUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/10/19 上午11:01
 * Mystery
 */

public class FamilyInfoMemberListAdapter extends BaseAdapter {

    private List<FamilyInfoApiResult.FamilyInfoMember> mData = new ArrayList<>();
    private int showMaxNumber = 30;
    private int column = 4;

    public FamilyInfoMemberListAdapter(int maxNumber, int column) {
        this.showMaxNumber = maxNumber;
        this.column = column;
    }

    public void updateData(List<FamilyInfoApiResult.FamilyInfoMember> updateData) {
        if (mData != null) {
            mData.clear();
        }
        mData = updateData;
        notifyDataSetChanged();
    }

//    @Override
//    public FamilyInfoShortListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new FamilyInfoShortListViewHolder(View.inflate(parent.getContext(), R.layout.layout_item_family_info_members, null));
//    }
//
//    @Override
//    public void onBindViewHolder(FamilyInfoShortListViewHolder holder, int position) {
//        FamilyInfoApiResult.FamilyInfoMember item = mData.get(position);
//        if (!TextUtils.isEmpty(item.image)) {
//            holder.loadImage(item.image);
//        }
//        holder.isOwner(TextUtils.equals(item.title, "owner"));
//    }
//
//    @Override
//    public int getItemCount() {
//        return mData.size();
//    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public FamilyInfoApiResult.FamilyInfoMember getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FamilyInfoShortListViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_item_family_info_members, null);
            viewHolder = new FamilyInfoShortListViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (FamilyInfoShortListViewHolder) convertView.getTag();
        }
        FamilyInfoApiResult.FamilyInfoMember member = getItem(position);
        viewHolder.setName(member.name);
        viewHolder.loadImage(member.image);
        viewHolder.setTitle(member.title);
        viewHolder.mArrow.setVisibility(View.GONE);
        return convertView;
    }

    public static class FamilyInfoShortListViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mTitle;
        public TextView mName;
        public View mArrow;

        public FamilyInfoShortListViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.id_family_info_short_list_item_image);
            mTitle = (TextView) itemView.findViewById(R.id.id_family_info_short_list_item_title);
            mName = (TextView) itemView.findViewById(R.id.id_family_info_short_list_item_name);
            mArrow = itemView.findViewById(R.id.id_family_info_member_arrow);
        }

        public void setName(String name) {
            mName.setText(name);
        }

        public void isOwner(boolean owner) {
            if (owner) {
                mTitle.setVisibility(View.VISIBLE);
            } else {
                mTitle.setVisibility(View.GONE);
            }
        }

        public void setTitle(String title) {
            if (TextUtils.isEmpty(title)) {
                mTitle.setVisibility(View.GONE);
            } else {
                if (TextUtils.equals(title, "owner")) {
                    mTitle.setText(MessageUtils.getString(R.string.string_family_boss));
                } else if (TextUtils.equals(title, FamilyConstant.FAMILY_DEPUTY)) {
                    mTitle.setText(MessageUtils.getString(R.string.string_family_deputy));
                } else if (TextUtils.equals(title, FamilyConstant.FAMILY_ELDER)) {
                    mTitle.setText(MessageUtils.getString(R.string.string_family_elder));
                } else if (TextUtils.equals(title, FamilyConstant.FAMILY_ELITE)) {
                    mTitle.setText(MessageUtils.getString(R.string.string_family_elite));
                } else {
                    mTitle.setVisibility(View.GONE);
                }
            }
        }

        public void loadImage(String url) {
            PicassoUtils.loadImage(mImageView.getContext(), url, mImageView, R.mipmap.default_head);
        }

    }
}
