package orangelab.project.common.bridge.headdecorview;

import android.text.TextUtils;
import android.util.Log;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import orangelab.project.common.view.HeadDecorView;

/**
 * game_werewolf
 * 2018/7/4 下午6:08
 * Mystery
 */
public class HeadDecorViewManager extends SimpleViewManager<HeadDecorView.HeadDecorateImageView> {

    public static final String RECAT_CLASS = "HeadDecorView";

    @Override
    public String getName() {
        return RECAT_CLASS;
    }

    @Override
    protected HeadDecorView.HeadDecorateImageView createViewInstance(ThemedReactContext reactContext) {
        HeadDecorView.HeadDecorateImageView imageView = new HeadDecorView.HeadDecorateImageView(reactContext);
        return imageView;
    }

    @ReactProp(name = "setDecor")
    public void setDecor(HeadDecorView.HeadDecorateImageView imageView, String decor) {
        /**
         * 根据不同type来设定
         */
        imageView.setDecorAnim(decor);
    }

    @ReactProp(name = "setAction")
    public void setAction(HeadDecorView.HeadDecorateImageView imageView, String action) {
        if (TextUtils.equals(action, "pause")) {
            imageView.pause();
        }

        if (TextUtils.equals(action, "resume")) {
            imageView.resume();
        }
    }
}
