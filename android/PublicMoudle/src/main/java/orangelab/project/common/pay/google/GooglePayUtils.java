package orangelab.project.common.pay.google;

import android.util.Log;

import com.R;
import com.android.billingclient.api.BillingClient;

import orangelab.project.common.pay.google.util.IabHelper;
import orangelab.project.common.pay.google.util.IabResult;
import orangelab.project.common.utils.MessageUtils;

import cn.intviu.connect.model.Message;
import cn.intviu.support.ReportRobot;

/**
 * game_werewolf
 * 2017/7/22 上午10:37
 * Mystery
 */

public class GooglePayUtils {

    public static final int SERVER_PAY_SUCCESS = 2000;
    public static final int SERVER_PAY_DOING = 2001;
    public static final int SERVER_PAY_UPDATE_FAIL = 2002;
    public static final int SERVER_PAY_SERVER_ERROR = 2003;
    public static final int SERVER_PAY_FAIL = 2004;
    public static final int SERVER_PAY_UNKNOWN_PRODUCT = 2005;
    public static final int SERVER_PAY_VERIFY_FAIL = 2006;

    public static final String getHintMessage(IabResult iabResult) {
        String result = MessageUtils.getString(R.string.pay_google_unknown);
        if (iabResult == null) {
            return result;
        }
        switch (iabResult.getResponse()) {
            case IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED:
                result = MessageUtils.getString(R.string.pay_google_user_cancel);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_USER_CANCELED");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE:
                result = MessageUtils.getString(R.string.pay_google_item_unavailable);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR:
                result = MessageUtils.getString(R.string.pay_google_init_failed);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_DEVELOPER_ERROR");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_ERROR:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_ERROR");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:
                result = MessageUtils.getString(R.string.pay_google_item_owned);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED:
                result = MessageUtils.getString(R.string.pay_google_init_failed);
                ReportRobot.robot.report("BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED");
                break;
            case IabHelper.BILLING_RESPONSE_RESULT_OK:
                break;
            case IabHelper.IABHELPER_BAD_RESPONSE:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("IABHELPER_BAD_RESPONSE");
                break;
            case IabHelper.IABHELPER_ERROR_BASE:
                result = MessageUtils.getString(R.string.pay_google_init_failed);
                ReportRobot.robot.report("IABHELPER_ERROR_BASE");
                break;
            case IabHelper.IABHELPER_INVALID_CONSUMPTION:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("IABHELPER_INVALID_CONSUMPTION");
                break;
            case IabHelper.IABHELPER_MISSING_TOKEN:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("IABHELPER_MISSING_TOKEN");
                break;
            case IabHelper.IABHELPER_REMOTE_EXCEPTION:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("IABHELPER_REMOTE_EXCEPTION");
                break;
            case IabHelper.IABHELPER_SEND_INTENT_FAILED:
                result = MessageUtils.getString(R.string.pay_google_service_connect_failed);
                ReportRobot.robot.report("IABHELPER_SEND_INTENT_FAILED");
                break;
            case IabHelper.IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE:
                result = MessageUtils.getString(R.string.pay_google_unknown);
                ReportRobot.robot.report("IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE");
                break;
            case IabHelper.IABHELPER_UNKNOWN_ERROR:
                result = MessageUtils.getString(R.string.pay_google_unknown);
                ReportRobot.robot.report("IABHELPER_UNKNOWN_ERROR");
                break;
            case IabHelper.IABHELPER_UNKNOWN_PURCHASE_RESPONSE:
                result = MessageUtils.getString(R.string.pay_google_unknown);
                ReportRobot.robot.report("IABHELPER_UNKNOWN_PURCHASE_RESPONSE");
                break;
            case IabHelper.IABHELPER_USER_CANCELLED:
                result = MessageUtils.getString(R.string.pay_google_user_cancel);
                ReportRobot.robot.report("IABHELPER_USER_CANCELLED");
                break;
            case IabHelper.IABHELPER_VERIFICATION_FAILED:
                result = MessageUtils.getString(R.string.pay_google_verification_failed);
                ReportRobot.robot.report("IABHELPER_VERIFICATION_FAILED");
                break;
        }
        return result;
    }

    public static final String getServerHint(int serverCode) {
        String result = "";
        switch (serverCode) {
            case SERVER_PAY_DOING:
                result = MessageUtils.getString(R.string.pay_server_error_2001);
                ReportRobot.robot.report("SERVER_PAY_DOING");
                break;
            case SERVER_PAY_UPDATE_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2002);
                ReportRobot.robot.report("SERVER_PAY_UPDATE_FAIL");
                break;
            case SERVER_PAY_SERVER_ERROR:
                result = MessageUtils.getString(R.string.pay_server_error_2003);
                ReportRobot.robot.report("SERVER_PAY_SERVER_ERROR");
                break;
            case SERVER_PAY_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2004);
                ReportRobot.robot.report("SERVER_PAY_FAIL");
                break;
            case SERVER_PAY_UNKNOWN_PRODUCT:
                result = MessageUtils.getString(R.string.pay_server_error_2005);
                ReportRobot.robot.report("SERVER_PAY_UNKNOWN_PRODUCT");
                break;
            case SERVER_PAY_VERIFY_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2006);
                ReportRobot.robot.report("SERVER_PAY_VERIFY_FAIL");
                break;
            default:
                result = MessageUtils.getString(R.string.pay_server_error_2003);
                ReportRobot.robot.report("SERVER_PAY_SERVER_ERROR");
                break;
        }
        return result;
    }


    public static final String getServerMessage(int serverCode) {
        String result = "";
        switch (serverCode) {
            case SERVER_PAY_DOING:
                result = MessageUtils.getString(R.string.pay_server_error_2001_msg);
                break;
            case SERVER_PAY_UPDATE_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2002_msg);
                break;
            case SERVER_PAY_SERVER_ERROR:
                result = MessageUtils.getString(R.string.pay_server_error_2003_msg);
                break;
            case SERVER_PAY_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2004_msg);
                break;
            case SERVER_PAY_UNKNOWN_PRODUCT:
                result = MessageUtils.getString(R.string.pay_server_error_2005_msg);
                break;
            case SERVER_PAY_VERIFY_FAIL:
                result = MessageUtils.getString(R.string.pay_server_error_2006_msg);
                break;
            default:
                result = MessageUtils.getString(R.string.pay_server_error_2003_msg);
                break;
        }
        return result;
    }


    public static String googleBillingResponseCodeTranslate(int errorCode) {
        if (errorCode == BillingClient.BillingResponse.USER_CANCELED) {
            return MessageUtils.getString(R.string.pay_google_user_cancel);
        } else if (errorCode == BillingClient.BillingResponse.SERVICE_UNAVAILABLE) {
            return MessageUtils.getString(R.string.pay_google_init_failed);
        } else if (errorCode == BillingClient.BillingResponse.BILLING_UNAVAILABLE) {
            return MessageUtils.getString(R.string.pay_google_service_unsupport);
        } else if (errorCode == BillingClient.BillingResponse.ITEM_UNAVAILABLE) {
            return MessageUtils.getString(R.string.pay_google_item_unavailable);
        } else if (errorCode == BillingClient.BillingResponse.DEVELOPER_ERROR) {
            return MessageUtils.getString(R.string.pay_google_unknown);
        } else if (errorCode == BillingClient.BillingResponse.ERROR) {
            return MessageUtils.getString(R.string.pay_google_unknown);
        } else if (errorCode == BillingClient.BillingResponse.ITEM_ALREADY_OWNED) {
            return MessageUtils.getString(R.string.pay_google_item_owned);
        } else if (errorCode == BillingClient.BillingResponse.ITEM_NOT_OWNED) {
            return MessageUtils.getString(R.string.pay_google_not_owned);
        } else if (errorCode == BillingClient.BillingResponse.FEATURE_NOT_SUPPORTED) {
            return MessageUtils.getString(R.string.pay_google_feature_not_support);
        } else if (errorCode == BillingClient.BillingResponse.SERVICE_DISCONNECTED) {
            return MessageUtils.getString(R.string.pay_google_service_connect_failed);
        }
        return MessageUtils.getString(R.string.pay_google_unknown);
    }
}
