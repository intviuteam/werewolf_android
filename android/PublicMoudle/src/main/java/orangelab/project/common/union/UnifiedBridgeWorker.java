package orangelab.project.common.union;

import com.androidtoolkit.ThreadToolKit;

/**
 * game_werewolf
 * 2017/10/24 上午10:30
 * Mystery
 */

public class UnifiedBridgeWorker {

    public static void Work(Runnable runnable) {
        Work(runnable, true);
    }

    public static void Work(Runnable runnable, boolean inUIThread) {
        if (inUIThread) {
            ThreadToolKit.Sync(runnable);
        } else {
            ThreadToolKit.Async(runnable);
        }
    }
}
