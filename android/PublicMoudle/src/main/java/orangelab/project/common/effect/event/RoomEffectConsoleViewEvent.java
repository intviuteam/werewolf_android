package orangelab.project.common.effect.event;

/**
 * Created by hwx on 30/7/2018.
 */
public class RoomEffectConsoleViewEvent {

    private String type;

    public RoomEffectConsoleViewEvent(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }

}
