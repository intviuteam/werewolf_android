package orangelab.project.common.family.manager;

import com.R;

import orangelab.project.common.family.model.CreateConversationResult;
import orangelab.project.common.union.IUnifiedBridge;
import orangelab.project.common.union.UnifiedBridgeWorker;
import orangelab.project.common.utils.MessageUtils;

import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/10/24 上午10:28
 * Mystery
 */

public class FamilyActionManager {

    /**
     * 处理从RN传入的创建家族消息
     *
     * @param bridge
     */
    public static void CreateFamilyActionFromRN(String action, IUnifiedBridge bridge) {
        /**
         * 首先移入通信桥主要工作线程
         */
        UnifiedBridgeWorker.Work(() -> {
            FamilyConversationManager.CreateFamilyConversation((code, result) -> UnifiedBridgeWorker.Work(() -> {
                CreateConversationResult createResult = new CreateConversationResult();
                createResult.reason = MessageUtils.getString(R.string.string_create_family_error_1);
                if (code == 0) {
                    /**
                     * 创建家族成功
                     */
                    createResult.conversationId = result;
                } else {
                    /**
                     * 创建家族失败
                     */
                    createResult.reason = result;
                }
                try {
                    JSONObject returnJson = new JSONObject(GsonHelper.getGson().toJson(createResult));
                    bridge.SendCommendIntoRN(action, returnJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }));
        });
    }
}
