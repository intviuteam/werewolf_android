package orangelab.project.common.manager;

import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.UIActuator;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.networktoolkit.transport.URLManager;
import com.toolkit.action.Action;
import com.toolkit.action.ApiResult;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.sdk.model.AppUpgradeInfo;
import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.common.effect.headdecor.HeadDecorManiFest;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.exception.ApiFailedException;
import orangelab.project.common.exception.NetWorkException;
import orangelab.project.common.exception.UnKnownException;
import orangelab.project.common.exhibition.gift.model.GiftManifest;
import orangelab.project.common.model.BlockListResult;
import orangelab.project.common.model.DisConnectInfoResult;
import orangelab.project.common.model.GiftHistoryResult;
import orangelab.project.common.model.LeanCloudConfig;
import orangelab.project.common.model.ServerGet;
import orangelab.project.common.model.UpLoadTokenResult;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.tool.GsonTurnToolKit;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.constants.VoiceConstants;

import java.util.HashMap;
import java.util.Map;

import static cn.intviu.sdk.AppMiddlewareApiDefines.PARAM_APP_UPDATE_CHANNEL;
import static cn.intviu.sdk.AppMiddlewareApiDefines.PARAM_APP_UPDATE_SYSTEM;
import static cn.intviu.sdk.AppMiddlewareApiDefines.PARAM_APP_UPDATE_VERSION;

/**
 * game_werewolf
 * 2017/10/11 下午2:44
 * Mystery
 */

public class ApiManager {

    private static final String TAG = "ApiManager";

    /**
     * 处理添加好友的所有操作
     *
     * @param userId
     */
    public static void AddFriendApi(String userId, Action<String> onSuccess, Action<String> onFailed, Action<String> onError) {
        if (GlobalUserState.getGlobalState().isTourist()) {
            /**
             * 游客情况下,提示相关信息
             */
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.tourist_friend)));
            return;
        }

        if (Utils.checkUserIdIsTourist(userId)) {
            /**
             * 对方是游客
             */
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.str_login_add_friend_2)));
            return;
        }

        if (BlackListManager.Has(userId)) {
            /**
             * 黑名单中存在此人
             */
            UIActuator.post(RunnableFactory.createMsgToastLongRunnable(MessageUtils.getString(R.string.action_add_friend_in_blacklist)));
            return;

        }
        JSONObject json = new JSONObject();
        try {
            json.put("friend_id", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create()
                .isGet(false)
                .request(Constant.ADD_FRIEND_REQUEST)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .load(json)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        RxToolKit.Emit(new ViewEvent.RequestAddFriendEvent(userId));
                        EventBus.getDefault().post(new ViewEvent.RequestAddFriendEvent(userId));
                        UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.add_success)));
                        if (onSuccess != null) {
                            onSuccess.func(response);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        UIActuator.post(RunnableFactory.createMsgToastRunnable(failed));
                        if (onFailed != null) {
                            onFailed.func(failed);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        UIActuator.post(RunnableFactory.createNetworkErrorRunnable());
                        if (onError != null) {
                            onError.func(e.getMessage());
                        }
                    }
                }).build().execute();
    }

    public static void GetUserInfoApi(String userId, ApiResult<UserInfoResult> apiResult) {
        GetUserInfoApi(userId, false, apiResult);
    }

    public static void GetUserInfoApi(String userId, boolean forMiniGame, ApiResult<UserInfoResult> apiResult) {
        if (TextUtils.isEmpty(userId)) {
            if (apiResult != null) {
                apiResult.onResult(null, new Exception("UserId is Null"));
            }
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.PARAMETER_USER_ID, userId);
            if (forMiniGame) {
                jsonObject.put("with_mg_ranks", "1");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request(Constant.USER_DETAIL_REQUEST).withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            UserInfoResult result = GsonHelper.getGson().fromJson(response, UserInfoResult.class);
                            if (apiResult != null) {
                                apiResult.onResult(result, null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (apiResult != null) {
                                apiResult.onResult(null, e);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new Exception(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                })
                .build().execute();
    }

    public static void DisConnectInfoApi(Action<DisConnectInfoResult> action) {
        RequestTask.create()
                .isGet(false)
                .request(URLManager.GetDisConnectInfoURL())
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            DisConnectInfoResult infoResult = GsonHelper.getGson().fromJson(response, DisConnectInfoResult.class);
                            if (action != null) {
                                action.func(infoResult);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        /**
                         * ignore
                         */
                    }

                    @Override
                    public void onError(Exception e) {
                        /**
                         * ignore
                         */
                    }
                }).build().execute();
    }

    public static void RefreshUpLoadToken(String roomId, ApiResult<UpLoadTokenResult> apiResult) {
//        JSONObject jsonObject = new JSONObject();
//        if (!TextUtils.isEmpty(roomId)) {
//            try {
//                jsonObject.put("type", "room/" + roomId);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        String url = "/image/token";
        if (!TextUtils.isEmpty(roomId)) {
            url = url + "?type=room/" + roomId;
        }
        RequestTask.create().isGet(true).withToken(GlobalUserState.getGlobalState().getToken()).request(url)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        UpLoadTokenResult result = GsonHelper.getGson().fromJson(response, UpLoadTokenResult.class);
                        try {
                            if (result != null) {
                                if (apiResult != null) {
                                    apiResult.onResult(result, null);
                                }
                            } else {
                                if (apiResult != null) {
                                    apiResult.onResult(null, new UnKnownException());
                                }
                            }
                        } catch (Exception e) {
                            if (apiResult != null) {
                                apiResult.onResult(null, new UnKnownException());
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void RefreshUpLoadToken(ApiResult<UpLoadTokenResult> apiResult) {
        RefreshUpLoadToken(null, apiResult);
    }


    public static void GetEmotionManiFest(int inputVersion, ApiResult<EmotionManiFest> apiResult) {
        RequestTask.create().isGet(true).urlMode(RequestTask.RequestTaskBuilder.URL_PARENT)
                .request("/config/audio_emoticon_V2?version=" + inputVersion)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Log.i("Api", "Emotion onSuccess: json->length[" + response.length() + "]");
                            EmotionManiFest maniFest = GsonTurnToolKit.turn(response, EmotionManiFest.class);
                            if (apiResult != null) {
                                apiResult.onResult(maniFest, null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (apiResult != null) {
                                apiResult.onResult(null, e);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, e);
                        }
                    }
                }).build().execute();
    }


    public static void GetMsgDecorManiFest(int inputVersion, ApiResult<MsgDecorManiFest> apiResult) {
        RequestTask.create().isGet(true).urlMode(RequestTask.RequestTaskBuilder.URL_PARENT)
                .request("/message_box/android?rev=" + inputVersion)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Log.i("Api", "MsgDecor onSuccess: json->length[" + response.length() + "]");
                            MsgDecorManiFest maniFest = GsonTurnToolKit.turn(response, MsgDecorManiFest.class);
                            if (apiResult != null) {
                                apiResult.onResult(maniFest, null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (apiResult != null) {
                                apiResult.onResult(null, e);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, e);
                        }
                    }
                }).build().execute();
    }

    public static void GetHeadDecorManiFest(int inputVersion, ApiResult<HeadDecorManiFest> apiResult) {
        RequestTask.create().isGet(true).urlMode(RequestTask.RequestTaskBuilder.URL_PARENT)
                .request("/avatar_box/android?rev=" + inputVersion)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Log.i("Api", "HeadDecor onSuccess: json->length[" + response.length() + "]");
                            HeadDecorManiFest maniFest = GsonTurnToolKit.turn(response, HeadDecorManiFest.class);
                            if (apiResult != null) {
                                apiResult.onResult(maniFest, null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (apiResult != null) {
                                apiResult.onResult(null, e);
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, e);
                        }
                    }
                }).build().execute();
    }

    public static void GetGiftManiFest(int inputVersion, Action<GiftManifest> result) {
        RequestTask.create().isGet(true).request("/gifts/android?rev=" + inputVersion).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                try {
                    Log.i("Api", "Gifts onSuccess: json->length[" + response.length() + "]");
                    GiftManifest manifest = GsonHelper.getGson().fromJson(response, GiftManifest.class);
                    if (result != null) {
                        result.func(manifest);
                    }
                } catch (Exception e) {
                    /**
                     * 这里的任何异常说明数据解析失败了
                     */
                    e.printStackTrace();
                    Utils.runSafely(() -> {
                        ReportEventUtils.reportException(e.getMessage());
                    });
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.func(null);
                }
            }

            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.func(null);
                }
            }
        }).build().execute();
    }

    public static void EscapeGameApi() {
        RequestTask.create().isGet(false).withToken(GlobalUserState.getGlobalState().getToken()).request(URLManager.GetEscapeURL()).build().execute();
    }

    /**
     * 分享
     */
    public static void DoShareApi(String type, int count) {
        DoShareApi(type, "", count, null);
    }

    public static void DoShareApi(String type, int count, String share_type) {
        DoShareApi(type, share_type, count, null);
    }

    public static void DoShareApi(String type, String share_type, int count, ApiResult<String> api) {
        DoShareApi(type, share_type, count, api, false);
    }

    /**
     * report to rn
     *
     * @param type
     * @param share_type
     * @param count
     * @param api
     * @param needReportRn
     */
    public static void DoShareApi(String type, String share_type, int count, ApiResult<String> api, boolean needReportRn) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.TYPE, type);
            jsonObject.put(VoiceConstants.COUNT, count);
            jsonObject.put(VoiceConstants.MINI_GAME_KEY, share_type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create(
                false,
                Constant.URI_TASK_DO,
                GlobalUserState.getGlobalState().getToken(),
                jsonObject,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(final String response) {
                        PLog.i("DoShareApi", "onSuccess: " + response);
                        if (api != null) {
                            api.onResult("", null);
                        }

                        //这里给rn发一条消息，通知体力值更新
                        if (needReportRn) {
                            UnifiedBridgeHelper.SendMiniGameReportToRn();
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        PLog.i("DoShareApi", "onFailed: " + failed);
                        if (api != null) {
                            api.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        PLog.e(TAG, "onError: ", e);
                        if (api != null) {
                            api.onResult(null, new NetWorkException());
                        }
                    }
                }).execute();
    }

    /**
     * 同意添加好友
     */
    public static void AcceptFriend(String userId, ApiResult<String> apiResult) {
        RequestTask requestTask = RequestTask.create(
                true,
                Constant.ACCEPT_FRIEND + userId,
                GlobalUserState.getGlobalState().getToken(),
                null,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        apiResult.onResult(MessageUtils.getString(R.string.add_friend_success), null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        apiResult.onResult(failed, new Exception(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        apiResult.onResult(MessageUtils.getString(R.string.network_is_unavailable), new NetWorkException(e));
                    }
                }
        );
        requestTask.execute();
    }

    /**
     * 拒绝添加好友
     */
    public static void RefuseFriend(String userId, ApiResult<String> apiResult) {
        RequestTask requestTask = RequestTask.create(
                true,
                Constant.REJECT_FRIEND + userId,
                GlobalUserState.getGlobalState().getToken(),
                null,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        apiResult.onResult(MessageUtils.getString(R.string.refuse_friend_success), null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        apiResult.onResult(failed, new Exception(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        apiResult.onResult(MessageUtils.getString(R.string.network_is_unavailable), new NetWorkException(e));
                    }
                }
        );
        requestTask.execute();
    }

    public static void DeleteFriend(String userId, ApiResult<String> result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt(Constant.FRIEND_ID, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask requestTask = RequestTask.create(
                false,
                Constant.FRIEND_DELETE,
                GlobalUserState.getGlobalState().getToken(),
                jsonObject,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (result != null) {
                            result.onResult(response, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (result != null) {
                            result.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (result != null) {
                            result.onResult(null, new NetWorkException());
                        }
                    }
                }
        );
        requestTask.execute();
    }

    /**
     * 拉黑
     */
    public static void AddToBlackApi(String userId, ApiResult<String> result) {
        RequestTask.create().isGet(true).request(URLManager.GetBlackListURL(userId)).withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        result.onResult(MessageUtils.getString(R.string.add_into_black_list_success), null);
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        result.onResult(null, new Exception(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        result.onResult(null, new NetWorkException(e));
                    }
                }).build().execute();
    }

    /**
     * 举报
     */
    public static void TipOff(String userId, String type, String message, ApiResult<String> result) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
            jsonObject.put(Constant.TYPE, type);
            jsonObject.put(Constant.MESSAGE, message);
            if (GlobalUserState.getGlobalState().isGaming()) {
                jsonObject.put(Constant.PARAMETER_ROOM_ID, GlobalUserState.getGlobalState().getCurRoomId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask requestTask = RequestTask.create(
                false,
                Constant.REPORT_URL,
                GlobalUserState.getGlobalState().getToken(),
                jsonObject,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (result != null) {
                            result.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (result != null) {
                            result.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (result != null) {
                            result.onResult(null, new NetWorkException());
                        }
                    }
                }
        );
        requestTask.execute();
    }

    public static void CheckLeanCloudNode(ApiResult<LeanCloudConfig> apiResult) {
        RequestTask.create(
                false,
                "/lean/config",
                "",
                new JSONObject(),
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            LeanCloudConfig config = GsonHelper.getGson().fromJson(response, LeanCloudConfig.class);
                            apiResult.onResult(config, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).execute();
    }

    public static void CheckUpdateApk(int version_code, String apk_channel, ApiResult<AppUpgradeInfo.AppUpgradeDetail> apiResult) {
        JSONObject json = new JSONObject();
        try {
            json.put(PARAM_APP_UPDATE_VERSION, version_code);
            json.put(PARAM_APP_UPDATE_SYSTEM, "android");
            json.put(PARAM_APP_UPDATE_CHANNEL, apk_channel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create(
                false,
                Constant.URI_APP_UPDATE,
                GlobalUserState.getGlobalState().getToken(),
                json,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        AppUpgradeInfo.AppUpgradeDetail info = GsonHelper.getGson().fromJson(response, AppUpgradeInfo.AppUpgradeDetail.class);
                        if (apiResult != null) {
                            apiResult.onResult(info, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }
        ).execute();
    }

    public static void ResumeAppInCurrThread() {
        RequestTask.create(
                true,
                "/user/status/resumeApp",
                GlobalUserState.getGlobalState().getToken(),
                null,
                null
        ).executeInCurThread();
    }

    public static void LeaveAppInCurrThread() {
        RequestTask.create(
                true,
                "/user/status/leaveApp",
                GlobalUserState.getGlobalState().getToken(),
                null,
                null
        ).executeInCurThread();
    }

    public static void GiftHistory(String userId, int curPage, boolean unique, ApiResult<GiftHistoryResult> apiResult) {
        JSONObject json = new JSONObject();
        try {
            json.put("id", userId);
            json.put("page", curPage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request("/gift/history").withToken(GlobalUserState.getGlobalState().getToken())
                .load(json).unique(unique).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                GiftHistoryResult giftHistoryResult = GsonHelper.getGson().fromJson(response, GiftHistoryResult.class);
                if (apiResult != null) {
                    apiResult.onResult(giftHistoryResult, null);
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (apiResult != null) {
                    apiResult.onResult(null, new ApiFailedException(failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (apiResult != null) {
                    apiResult.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

    public static void SearchRoom(String roomId, ApiResult<String> apiResult) {
        RequestTask.create(
                true,
                "/room/search?room_id=" + roomId + "&lc=" + GlobalUserState.getGlobalState().getUserLocation()
                        + "&" + Constant.USER_ID_LOWER + "=" + GlobalUserState.getGlobalState().getUserId(),
                GlobalUserState.getGlobalState().getToken(),
                null,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult(response, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }
        ).execute();
    }

    public static void ServerGet(String roomType, String roomId, String userId, String location, ApiResult<ServerGet> apiResult) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(roomId)) {
                jsonObject.put(Constant.PARAMETER_ROOM_ID, roomId);
            }
            jsonObject.put(Constant.SERVER_GET_LOCATION, location);
            jsonObject.put(Constant.USER_ID_LOWER, userId);
            jsonObject.put(Constant.GAME_LEVEL, roomType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create()
                .isGet(false)
                .request("/server/get")
                .load(jsonObject)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {

                            @Override
                            public void onSuccess(String response) {
                                ServerGet serverGet = GsonHelper.getGson().fromJson(response, ServerGet.class);
                                if (apiResult != null) {
                                    apiResult.onResult(serverGet, null);
                                }
                            }

                            @Override
                            public void onFailed(int code, String failed) {
                                if (apiResult != null) {
                                    apiResult.onResult(null, new ApiFailedException(code, failed));
                                }
                            }

                            @Override
                            public void onError(Exception e) {
                                if (apiResult != null) {
                                    apiResult.onResult(null, new NetWorkException());
                                }
                            }
                        }
                ).build().execute();
    }

    public static void BlockListAll(ApiResult<BlockListResult> apiResult) {
        RequestTask.create()
                .isGet(true)
                .request("/block/list/all")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        BlockListResult result = GsonHelper.getGson().fromJson(response, BlockListResult.class);
                        if (apiResult != null) {
                            apiResult.onResult(result, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(code, failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void userLike(String userId, ApiResult<Integer> apiResult) {
        if (TextUtils.isEmpty(userId) || Utils.checkUserIdIsTourist(userId)) {
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).request("/user/like").withToken(GlobalUserState.getGlobalState().getToken()).load(jsonObject).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                if (apiResult != null) {
                    try {
                        JSONObject json = new JSONObject(response);
                        int newVal = json.optInt("newVal");
                        apiResult.onResult(newVal, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (apiResult != null) {
                    apiResult.onResult(null, new ApiFailedException(code, failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (apiResult != null) {
                    apiResult.onResult(null, new NetWorkException());
                }
            }
        }).build().execute();
    }

    /**
     * 自定义模式的Url
     */
    public static void ApiTestCustomUrlAndGet() {
        Map<String, String> get = new HashMap<>();
        get.put("user_id", GlobalUserState.getGlobalState().getUserId());
        get.put("user_sex", GlobalUserState.getGlobalState().getUserSex() + "");

        RequestTask.create()
                /**
                 * 需要先设定mode GET模式
                 */
                .mode(RequestTask.RequestTaskBuilder.GET)
                /**
                 * 才可以装配Get数据
                 */
                .withGetPayload(get)
                /**
                 * 使用Custom模式 可以完全自定义url
                 */
                .urlMode(RequestTask.RequestTaskBuilder.URL_CUSTOM)
                /**
                 * 使用Custom模式 需要设定customUrl
                 */
                .customUrl("http://staging.intviu.cn:8201/user/info")

                .withToken(GlobalUserState.getGlobalState().getToken())

                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFailed(int code, String failed) {

                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }).build().execute();
    }

    /**
     * Post 请求方式实例
     */
    public static void ApiTestPost() {
        JSONObject postJSON = new JSONObject();
        RequestTask.create()
                /**
                 * 需要先设定mode 为POST模式
                 */
                .mode(RequestTask.RequestTaskBuilder.POST)
                /**
                 * 才可以装配Post数据
                 */
                .load(postJSON)
                /**
                 * 需要先设定urlmode
                 */
                .urlMode(RequestTask.RequestTaskBuilder.URL_PARENT)
                /**
                 * 才可以的设定request
                 */
                .request("/user/info")
                /**
                 * 设定报文头部装饰器
                 */
                .setHeadDecorator(builder -> {

                    builder.addHeader("customHeadKey", "customHeadValue");

                    return builder;
                })
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {

                    }

                    @Override
                    public void onFailed(int code, String failed) {

                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }).build().execute();
    }

}
