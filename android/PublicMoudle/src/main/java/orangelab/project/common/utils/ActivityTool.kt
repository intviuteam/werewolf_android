package orangelab.project.common.utils

import android.app.Activity
import com.toolkit.action.Destroyable
import com.toolkit.action.Keepable

/**
 * Created by lloydfinch on 15/03/2018.
 * 扩展了一些Activity的相关方法和属性
 */
class ActivityTool : Keepable, Destroyable {

    companion object {

        //重载此方法
        fun Activity.getTAG1(): String {
            return this.localClassName
        }

        //重载此属性
        val Activity.TAG: String
            get() = this.localClassName

    }

    override fun destroy() {
        //release some resource
    }
}