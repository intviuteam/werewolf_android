package orangelab.project.common.exception;

import android.support.annotation.NonNull;

import com.R;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/10/26 下午4:00
 * Mystery
 */

public class NetWorkException extends Exception {

    public NetWorkException() {
        super(MessageUtils.getString(R.string.network_is_unavailable));
    }

    public NetWorkException(@NonNull Exception e) {
        super(e.getMessage());
    }
}
