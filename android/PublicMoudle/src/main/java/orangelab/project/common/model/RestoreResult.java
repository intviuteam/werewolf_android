package orangelab.project.common.model;

import orangelab.project.voice.model.VoiceWords;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author:1727
 * 2017/3/28
 */

public class RestoreResult {

    public EnterRoomResult.EnterRoomMessage room_info;
    public RestoreRoleMessage game_info;
    public long enter_time = 0;
    public EnterRoomResult.KTV ktv;

    public static class GameInfoDeathInfo implements Serializable {
        public int position;
        public String role;
        public String type = "";

        @Override
        public String toString() {
            return "GameInfoDeathInfo{" +
                    "position=" + position +
                    ", role='" + role + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }

    public static class RestoreRoleMessage implements Serializable {
        public String role;
        public VoiceWords words;
        public int sheriff = -1;
        public List<Integer> teammates;
        public Map<Integer, String> teammate_roles;
        public int king = -1;
        public int demon = -1;
        public Map<Integer, Boolean> linked;
        public List<Integer> death;
        public SpeechInfo speech_info;
        public DayInfo day_info;
        public ArrayList<GameInfoDeathInfo> death_info;
        public boolean boomable;
        public boolean fightable;
        public List<Integer> apply;

        @Override
        public String toString() {
            return "RestoreRoleMessage{" +
                    "role='" + role + '\'' +
                    ", words=" + words +
                    ", sheriff=" + sheriff +
                    ", teammates=" + teammates +
                    ", king=" + king +
                    ", linked=" + linked +
                    ", death=" + death +
                    ", speech_info=" + speech_info +
                    ", day_info=" + day_info +
                    ", death_info=" + death_info +
                    ", boomable=" + boomable +
                    ", apply=" + apply +
                    '}';
        }
    }

    public static class DayInfo implements Serializable {
        public int day_index;
        public boolean is_night;
        public int duration;
        public boolean can_speak;
        public int can_speak_duration;

        @Override
        public String toString() {
            return "DayInfo{" +
                    "day_index=" + day_index +
                    ", is_night=" + is_night +
                    ", duration=" + duration +
                    '}';
        }
    }

    public static class SpeechInfo implements Serializable {
        public int duration;
        public SpeechInfoCurrent current;

        @Override
        public String toString() {
            return "SpeechInfo{" +
                    "duration=" + duration +
                    ", current=" + current +
                    '}';
        }
    }

    public static class SpeechInfoCurrent implements Serializable {
        public boolean appended;
        public int position;
        public long start_time;
        public long last_time;

        @Override
        public String toString() {
            return "SpeechInfoCurrent{" +
                    "appended=" + appended +
                    ", position=" + position +
                    ", start_time=" + start_time +
                    ", last_time=" + last_time +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RestoreResult{" +
                "room_info=" + room_info +
                ", game_info=" + game_info +
                '}';
    }
}
