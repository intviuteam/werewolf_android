package orangelab.project.common.effect.headdecor;

import com.toolkit.action.Keepable;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2018/7/11 下午2:19
 * Mystery
 */
public class HeadDecorAnimData implements Keepable {

    public List<HeadDecorFrame> repeatFrames;
    public long repeatIFS = 300;

    public List<HeadDecorFrame> preRepeatFrames;
    public long preRepeatIFS = 300;

    public static class HeadDecorFrame implements Keepable {
        public String remote;
        public String local;
    }

    public List<HeadDecorFrame> createCopy(List<HeadDecorFrame> input) {
        List<HeadDecorFrame> result = new ArrayList<>();
        for (HeadDecorFrame temp : input) {
            result.add(temp);
        }
        return result;
    }

}
