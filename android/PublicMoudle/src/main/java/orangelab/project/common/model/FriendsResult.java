package orangelab.project.common.model;

import orangelab.project.common.dialog.IntviuFriendDialog;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * game_werewolf
 * 2017/5/18 下午1:34
 * Mystery
 */

public class FriendsResult {
    public ArrayList<IntviuFriendDialog.FriendItem> list;

    @Override
    public String toString() {
        return "FriendsResult{" +
                "list=" + list +
                '}';
    }
}
