package orangelab.project.common.exhibition;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2017/6/28 下午5:03
 * Mystery
 */

public class ExhibitionsFMS {


    public static ExhibitionPackage wrapExhibition(String fromType, int from, int to, UserData toUser) {
        return new ExhibitionPackage(fromType, from, to, toUser);
    }

    public static ExhibitionPackage wrapExhibition(String fromType, String from, UserData toUser) {
        return new ExhibitionPackage(fromType, from, toUser);
    }

    public static ExhibitionPackage wrapExhibition(String fromType, int from, int to, UserData toUser, String displayType) {
        return new ExhibitionPackage(fromType, from, to, toUser, displayType);
    }

    /**
     * 游戏内发送
     * <p>
     * 1。打包
     * 2。发送
     *
     * @param exhibition
     * @param from
     * @param to
     */
    public static void dispatchExhibition(String fromType, IExhibition exhibition, int from, int to, UserData toUser) {
        ExhibitionPackage exhibitionPackage = wrapExhibition(fromType, from, to, toUser);
        exhibition.present(exhibitionPackage);
    }

    public static void dispatchExhibition(String fromType, IExhibition exhibition, String from, UserData toUser) {
        ExhibitionPackage exhibitionPackage = wrapExhibition(fromType, from, toUser);
        exhibition.present(exhibitionPackage);
    }

    public static void dispatchExhibitionWithType(String fromType, IExhibition exhibition, int from, int to, UserData toUser, String displayType) {
        ExhibitionPackage exhibitionPackage = wrapExhibition(fromType, from, to, toUser, displayType);
        exhibition.present(exhibitionPackage);
    }
}
