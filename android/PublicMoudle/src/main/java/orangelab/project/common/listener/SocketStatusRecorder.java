package orangelab.project.common.listener;


import android.net.TrafficStats;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.StorageToolKit;
import com.androidtoolkit.ThreadToolKit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import cn.intviu.support.NetworkHelpers;
import orangelab.project.MainApplication;
import orangelab.project.ProjectConfig;
import orangelab.project.common.event.SocketEvent;

/**
 * socket状态记录器
 * 当socket断开时，就记录下当前的网络状态信息
 */
public class SocketStatusRecorder {

    private static final String TAG = "SocketStatusRecorder";

    private static SocketStatusRecorder instance;

    private static SocketStatusRecorder getInstance() {
        if (instance == null) {
            synchronized (SocketStatusRecorder.class) {
                if (instance == null) {
                    instance = new SocketStatusRecorder();
                }
            }
        }
        return instance;
    }

    private SocketStatusRecorder() {
    }

    public static void destroy() {
        if (instance != null) {
            RxToolKit.UnRegister(instance);
            instance = null;
        }
    }

    public static void init() {
        if (instance == null) {
            instance = getInstance();
        }
        RxToolKit.Build(instance, SocketEvent.SocketConnectStateEvent.class)
                .action(event -> {
                    int action = event.getAction();
                    PLog.e(TAG, "================socket status ================= " + action);
                    if (action == SocketEvent.SOCKET_DISCONNECT) {
                        recordStatus();
                    }
                }).register();
    }

    /**
     * 记录当前的网络状态信息
     */
    public static void recordStatus() {

        String fileDirs = "net";
        String fileName = "net.txt";

        //只有在可以debug的情况下才记录
        if (ProjectConfig.DEBUG_ABLE) {
            MainApplication instance = MainApplication.getInstance();
            //是否连接
            boolean isConnected = NetworkHelpers.isNetworkAvailable(instance);
            //是否是移动网络
            boolean isMobile = NetworkHelpers.isMobile(instance);
            //是否是wifi
            boolean isWifi = NetworkHelpers.isWifi(instance);

            StringBuilder sb = new StringBuilder();

            //记录当前时间
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            sb.append("#===============")
                    .append(simpleDateFormat.format(date))
                    .append("================#")
                    .append("\n");

            //记录当前网络状态
            sb.append("current net is connect: ")
                    .append(isConnected)
                    .append("\n")
                    .append("current net is mobile: ")
                    .append(isMobile)
                    .append("\n")
                    .append("current net is wifi: ")
                    .append(isWifi)
                    .append("\n");

            //如果当前网路是连接状态，就计算一下当前的网速
            //当前进程的uid
            int uid = instance.getApplicationInfo().uid;

            //开启线程统计并记录当前网络信息
            ThreadToolKit.Async(() -> {
                //记录当前时间
                long starTime = System.currentTimeMillis();
                //获取当前进程收到的总流量 kb
                long rxBytes = TrafficStats.getUidRxBytes(uid);
                //获取当前进程发送的总流量 kb
                long txBytes = TrafficStats.getUidTxBytes(uid);
                if (txBytes <= 0) {
                    txBytes = getTotalSendBytes(uid);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //记录结束时间
                long endTime = System.currentTimeMillis();
                //获取当前进程收到的总流量 kb
                long rxBytes2 = TrafficStats.getUidRxBytes(uid);
                //获取当前进程发送的总流量 kb
                long txBytes2 = TrafficStats.getUidTxBytes(uid);
                if (txBytes2 <= 0) {
                    txBytes2 = getTotalSendBytes(uid);
                }

                //计算差值
                long delay = (endTime - starTime) / 1000;
                long rxBytesGap = rxBytes2 - rxBytes;
                long txBytesGap = txBytes2 - txBytes;

                long rxSpeed = rxBytesGap / delay; //接收速率 byte/s
                long txSpeed = txBytesGap / delay; //发送速率 byte/s

                long rxSpeedKbs = rxSpeed >> 10; //接收速率 kb/s
                long txSpeedKbs = txSpeed >> 10; //发送速率 kb/s

                //记录发送的总数据
                sb.append("total send data: ")
                        .append(txBytes2)
                        .append(" bytes")
                        .append("\n");

                //记录收到的总数据
                sb.append("total receive data: ")
                        .append(rxBytes2)
                        .append(" bytes")
                        .append("\n");

                //记录1s发送的数据量
                sb.append("send data in ")
                        .append(delay)
                        .append("s: ")
                        .append(txBytesGap)
                        .append(" bytes")
                        .append("\n");

                //记录1s接收的数据量
                sb.append("receive data in 1s: ")
                        .append(delay)
                        .append("s: ")
                        .append(rxBytesGap)
                        .append(" bytes")
                        .append("\n");

                //记录发送速度
                sb.append("current send speed: ")
                        .append(txSpeed)
                        .append(" byte/s, ")
                        .append(txSpeedKbs)
                        .append(" kb/s")
                        .append("\n");

                //记录接收速度
                sb.append("current receive speed: ")
                        .append(rxSpeed)
                        .append(" byte/s, ")
                        .append(rxSpeedKbs)
                        .append(" kb/s")
                        .append("\n");
                FileOutputStream out = null;
                PrintStream printStream = null;
                try {
                    String path = StorageToolKit.GetStoragePath(instance, fileDirs);
                    File fileDir = new File(path);
                    if (!fileDir.exists()) {
                        fileDir.mkdirs();
                    }
                    File file = new File(fileDir, fileName);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    out = new FileOutputStream(file, true);
                    printStream = new PrintStream(out);
                    printStream.print(sb.toString());
                    printStream.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (printStream != null) {
                            printStream.close();
                        }
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * 获取某一进程的发送和接收量
     */
    public static Long getTotalSendBytes(int localUid) {

        long sendData = 0;

        try {
            File dir = new File("/proc/uid_stat/");
            String[] children = dir.list();
            if (!Arrays.asList(children).contains(String.valueOf(localUid))) {
                return 0L;
            }
            File uidFileDir = new File("/proc/uid_stat/" + String.valueOf(localUid));
            File uidActualFileReceived = new File(uidFileDir, "tcp_rcv");
            File uidActualFileSent = new File(uidFileDir, "tcp_snd");

            //接收的数据
            String textReceived = "0";
            //发送的数据
            String textSent = "0";


            BufferedReader brReceived = new BufferedReader(new FileReader(uidActualFileReceived));
            BufferedReader brSent = new BufferedReader(new FileReader(uidActualFileSent));
            String receivedLine;
            String sentLine;

            if ((receivedLine = brReceived.readLine()) != null) {
                textReceived = receivedLine;
            }
            if ((sentLine = brSent.readLine()) != null) {
                textSent = sentLine;
            }

            sendData = Long.valueOf(textSent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sendData;

    }
}
