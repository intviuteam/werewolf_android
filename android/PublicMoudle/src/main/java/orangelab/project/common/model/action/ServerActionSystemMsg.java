package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/19 下午6:50
 * Mystery
 */

public class ServerActionSystemMsg extends ServerActionContainsDuration implements Keepable {
    public static final String DIALOG = "dialog";
    public static final String TOAST = "toast";
    public static final String TEXT = "text";

    public String type;
    public String content;
}
