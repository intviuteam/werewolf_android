package orangelab.project.common.pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.android.billingclient.api.BillingClient;
import com.androidtoolkit.RxToolKit;

import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.event.WebViewEvent;
import orangelab.project.common.pay.google.GooglePayHelper;
import orangelab.project.common.pay.google.GooglePayUtils;
import orangelab.project.common.pay.google.v2.GooglePayV2InAppHelper;
import orangelab.project.common.pay.google.v2.GooglePayV2SubscriptionsHelper;
import orangelab.project.common.pay.wechat.WeChatPayHelper;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2017/7/21 下午12:19
 * Mystery
 */

public class PayActivity extends SafeActivity {

    private static final String TAG = "PayActivity";
    private String mProductID = "";
    private String mProductType = "";
    private PayDialog mPayTypeSelectDialog; //支付选择对话框
    private PayLoadingDialog mPayLoadingDialog;//支付加载对话框
    private PayResultDialog mPayGoogleResultDialog; //谷歌支付出错提示框
    private PayResultDialog mPayServerResultDialog; //服务器支付出错提示框
    private PayHelper mPayHelper; //支付帮助类
    private PayHelperListener mGooglePayHelperListener = new PayHelperListener() {
        @Override
        public void onErrorFromGoogle(String reason) {
            Log.i(TAG, "onErrorFromGoogle: " + reason);
            runOnUiThreadSafely(() -> {
                setLoading(false);
                googlePayError(reason);
            });
        }

        @Override
        public void onErrorFromServer(int code, String reason) {
            Log.i(TAG, "onErrorFromServer: " + code + ", reason=" + reason);
            runOnUiThreadSafely(() -> {
                setLoading(false);
                serverPayError(code, reason);
            });
        }

        @Override
        public void onDestroy() {
            /**
             * 此方法暂时不用
             */
        }

        @Override
        public void onSuccess() {
            Log.i(TAG, "onSuccess: ");
            /**
             * 支付成功
             */
            runOnUiThreadSafely(() -> {
                RxToolKit.Emit(new WebViewEvent.WebViewPayResultEvent(0, "Success"));
                setLoading(false);
                successPay();
            });
        }

        @Override
        public void onStart() {
            Log.i(TAG, "onStart: ");
            /**
             * 支付开始
             */
            runOnUiThreadSafely(() -> {
                setLoading(true);
            });
        }
    };

    @Override
    protected void onActivityWindowInitFinish() {

    }

    private void setLoading(boolean flag) {
        if (mPayLoadingDialog != null) {
            if (flag && !mPayLoadingDialog.isShowing()) {
                mPayLoadingDialog.show();
                return;
            }
            if (!flag && mPayLoadingDialog.isShowing()) {
                mPayLoadingDialog.dismiss();
                return;
            }
        }
    }

    /**
     * 根据支付类型生成不同的支付帮助类
     *
     * @param type 支付类型
     * @return
     */
    private PayHelper createPayHelper(String type) {
        PayHelper payHelper = null;
        switch (type) {
            case PayUtils.GOOGLE:
                if (TextUtils.equals(mProductType, BillingClient.SkuType.INAPP)) {
                    payHelper = new GooglePayV2InAppHelper(this, mProductID, mGooglePayHelperListener);
                }
                if (TextUtils.equals(mProductType, BillingClient.SkuType.SUBS)) {
                    payHelper = new GooglePayV2SubscriptionsHelper(this, mProductID, mGooglePayHelperListener);
                }
                break;
            case PayUtils.WECHAT:
                payHelper = new WeChatPayHelper(this, PayUtils.PAY_DATE_PAYTEST, null);
                break;
        }
        return payHelper;
    }

    private void successPay() {
        if (mPayServerResultDialog != null && !mPayServerResultDialog.isShowing()) {
            mPayServerResultDialog.isSuccess(true);
            mPayServerResultDialog.show();
        }
    }

    private void googlePayError(String reason) {
        if (mPayGoogleResultDialog != null && !mPayGoogleResultDialog.isShowing()) {
            mPayGoogleResultDialog.isSuccess(false);
            mPayGoogleResultDialog.setReason(reason);
            mPayGoogleResultDialog.show();
        }
    }

    private void serverPayError(int code, String title) {
        if (mPayServerResultDialog != null && !mPayServerResultDialog.isShowing()) {
            mPayServerResultDialog.isSuccess(false);
            mPayServerResultDialog.setTitle(MessageUtils.getString(R.string.title_hint));
            mPayServerResultDialog.setReason(GooglePayUtils.getServerMessage(code));
            mPayServerResultDialog.needReTryButton(false);
            mPayServerResultDialog.show();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        mProductID = IntentDataHelper.getProductId(getIntent());
        mProductType = IntentDataHelper.getType(getIntent());
        mPayLoadingDialog = new PayLoadingDialog(this);

        mPayGoogleResultDialog = new PayResultDialog(this, () -> {
            finish();
        }, () -> {
            setLoading(true);
            if (mPayHelper != null) {
                /**
                 * 重新支付
                 */
                mPayHelper.repay();
            }
        });

        mPayServerResultDialog = new PayResultDialog(this, () -> {
            finish();
        }, null);

        /**
         * 1. 展示支付选择接口
         */
        mPayTypeSelectDialog = new PayDialog(this, () -> {
            Log.i(TAG, "onCreate: Dismiss Pay Dialog");
            /**
             * 由于用户没有选择支付方式而使得对话框消失
             */
            finish();
        }, s -> {
            Log.i(TAG, "onCreate: Select Type is " + s);
            if (mPayTypeSelectDialog != null) {
                mPayTypeSelectDialog.dismiss();
            }
            /**
             * 用户已经选择了支付模式并点击了确认支付
             */
            mPayHelper = createPayHelper(s);
            if (mPayHelper != null) {
                mPayHelper.pay();
            } else {
                /**
                 * 支付初始化失败，可能选择了不支持的支付类型
                 */
            }
        });
        mPayTypeSelectDialog.show();
    }

    public static void preparePay(Context context, String productID) {
        preparePay(context, productID, BillingClient.SkuType.INAPP);
    }

    public static void preparePay(Context context, String productID, String productType) {
        Intent intent = new Intent(context, PayActivity.class);
        IntentDataHelper.setProductId(intent, productID);
        IntentDataHelper.setType(intent, productType);
        context.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
        if (mPayHelper != null) {
            mPayHelper.release();
            mPayHelper = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mPayHelper != null) {
            if (mPayHelper.onActivityResult(requestCode, resultCode, data)) {
                return;
            } else {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
