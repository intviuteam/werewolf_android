package orangelab.project.common.event;

/**
 * Created by lloydfinch on 26/12/2017.
 */

public class SmallWindowEvent {

    //进入小窗模式
    public static class ComeInSWEvent {

    }

    //小窗模式恢复
    public static class RestoreRoomFromSWEvent {

    }

    //小窗->Room 销毁Room
    public static class DestroyRoomFromSwEvent {

    }

    //Room->小窗 socket释放完毕，可以进行其他操作
    public static class VoiceEngineReleasedEvent {

    }

    //Room -> 小窗 更换小窗头像
    public static class NotifySWChangeHead {
        public String headImgUrl;

        public NotifySWChangeHead() {
        }

        public NotifySWChangeHead(String headImgUrl) {
            this.headImgUrl = headImgUrl;
        }
    }

    //Room->小窗 销毁小窗
    public static class NotifyDesTorySW {

    }
}
