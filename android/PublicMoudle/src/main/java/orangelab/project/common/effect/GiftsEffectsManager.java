package orangelab.project.common.effect;

import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ThreadToolKit;
import com.facebook.react.bridge.Callback;
import com.toolkit.action.Action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.exhibition.gift.Gift;
import orangelab.project.common.exhibition.gift.model.GiftInfoForReact;
import orangelab.project.common.exhibition.gift.model.GiftManifest;
import orangelab.project.common.exhibition.gift.model.GiftManifestItem;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.utils.ReportEventUtils;

/**
 * game_werewolf
 * 2018/7/13 下午1:31
 * Mystery
 */
class GiftsEffectsManager {

    private static final String TAG = "GiftsEffectsManager";

    private boolean isChecking = false;

    boolean isChecking() {
        return isChecking;
    }

    void setChecking(boolean checking) {
        isChecking = checking;
    }

    public GiftsEffectsManager() {

    }

    public void queryGiftsManifest(Action<List<Gift>> giftListCallback) {
        ThreadToolKit.Async(() -> {
            GiftManifest giftManifest = EffectsMainFestManager.aManager.getGiftManifest();
            if (giftManifest != null) {
                /**
                 * 1。先校验一下本地礼物的完备性
                 */
                try {
                    Set<String> verifyResult = EffectsMainFestManager.aManager.verifyGiftDecorManifestData(giftManifest);
                    if (verifyResult != null) {
                        List<GiftManifestItem> giftList = giftManifest.gifts;
                        List<Gift> resultGiftList = new ArrayList<Gift>();
                        for (GiftManifestItem item : giftList) {
                            if (verifyResult.contains(item.gift_type) || TextUtils.isEmpty(item.image)) {
                                /**
                                 * 包含校验失败的礼物
                                 */
                                continue;
                            }
                            if (item.status != 1) {
                                /**
                                 * 这个礼物已经下架不显示
                                 */
                                continue;
                            }
                            /**
                             * 这里是正常的礼物
                             */
                            Gift gift = new Gift(item.gift_type, item.price.get(0).type, item.price.get(0).count);
                            gift.setGiftName(item.getName());
                            gift.setPopularValue(item.popular);
                            gift.setDisCount(item.rebateMsg);
                            resultGiftList.add(gift);
                        }
                        /**
                         * 返回本地存储的礼物列表
                         */
                        if (giftListCallback != null) {
                            giftListCallback.func(resultGiftList);
                        }
                        EffectsMainFestManager.aManager.downloadAndSaveGiftsManifestResource(giftManifest, false);
                    } else {
                        /**
                         * 校验失败,原因是没有礼物信息，进行一次比对下载
                         */
                        EffectsMainFestManager.aManager.downloadAndSaveGiftsManifestResource(giftManifest, false);
                        /**
                         * 比对完成这次认为直接失败
                         */
                        if (giftListCallback != null) {
                            giftListCallback.func(null);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    /**
                     * 异常，认为礼物加载失败
                     */
                    if (giftListCallback != null) {
                        giftListCallback.func(null);
                    }
                }

            } else {
                if (giftListCallback != null) {
                    giftListCallback.func(null);
                }
            }
        });
    }

    public void refreshGiftsManifest(Action<Integer> callback) {
        if (isChecking()) {
            return;//正在与服务器进行校验
        }
        setChecking(true);//标记正在与服务器进行校验
        final int version = EffectsMainFestManager.aManager.getGiftManifestVersion();
        ApiManager.GetGiftManiFest(version, value -> {
            if (value != null) {
                /**
                 * 这里如果返回了正常的数据，说明需要校验
                 */
                diffCheckGiftManifest(version, value, callback);
                setChecking(false);
            } else {
                /**
                 * 这里有可能是异常或者解析数据错误，或者是不需要进行更新，这里无论如何先做一次文件的完整性校验
                 */
                GiftManifest temp = EffectsMainFestManager.aManager.getGiftManifest();
                if (temp != null) {
                    Set<String> result = EffectsMainFestManager.aManager.verifyGiftDecorManifestData(temp);
                    if (result != null && result.size() == 0) {
                        /**
                         * 校验成功
                         */
                        EffectsMainFestManager.aManager.updateGiftManifest(temp);
                        if (callback != null) {
                            callback.func(0);
                        }
                    } else {
                        /**
                         * 下载一下
                         */
                        EffectsMainFestManager.aManager.downloadAndSaveGiftsManifestResource(temp, false);
                        /**
                         * 校验一下
                         */
                        EffectsMainFestManager.aManager.verifyGiftDecorManifestData(temp);
                    }
                } else {
                    /**
                     * local 文件不存在了这里操作比较复杂,说明之前存在过，但是因为何种原因文件不存在了
                     *
                     * 1.移除版本号
                     * 2.移除清单文件，重新进行下载
                     */
                    EffectsMainFestManager.aManager.removeGiftsManifestVersion();
                    EffectsMainFestManager.aManager.removeGiftManifest();
                    setChecking(false);
                    refreshGiftsManifest(null);
                    return;

                }
                setChecking(false);
            }
        });
    }

    public void refreshGiftsManifestForExhibition(Action<Integer> callback) {
        if (isChecking()) {
            return;
        }
        setChecking(true);
        if (EffectsMainFestManager.aManager.getGiftManifest() == null) {
            /**
             * 目前没有礼物
             */
            Log.i(TAG, "RefreshAndCheckGiftForExhibition: we don't find Manifest");
            final int version = EffectsMainFestManager.aManager.getGiftManifestVersion();
            ApiManager.GetGiftManiFest(version, value -> {
                if (value != null) {
                    diffCheckGiftManifest(version, value, callback);
                }
                setChecking(false);
            });
        } else {
            Log.i(TAG, "RefreshAndCheckGiftForExhibition: we find Manifest");
            setChecking(false);
            return;
        }
    }

    public void fillGiftImageView(String giftType, ImageView imageView) {
        /**
         * 先判断这个礼物Type是不是卡片
         */
        int apkCardBackGroundResult = ApkResourceManager.GetApkCardResource(giftType);
        if (apkCardBackGroundResult != -1) {
            /**
             * 这个是卡片
             */
            imageView.setImageResource(apkCardBackGroundResult);
            return;
        }

        /**
         * 根据giftType 拿到本地的缓存数据
         */
        try {
            GiftManifestItem item = EffectsMainFestManager.aManager.getGiftManifest().find(giftType);
            if (item != null) {
                if (!TextUtils.isEmpty(item.localImage)) {
                    /**
                     * 先进行本地校验
                     */
                    int res = ApkResourceManager.GetApkImageResource(imageView.getContext(), item.localImage);
                    if (res > 0) {
                        /**
                         * 使用Pciasso对加载本地图片优化
                         */
                        try {
                            PicassoUtils.loadImageForGift(imageView.getContext(), res, imageView);
                        } catch (Exception e) {
                            /**
                             * 如果出现异常则使用原始方式加载图片
                             */
                            e.printStackTrace();
                            imageView.setImageResource(res);
                        }
                        return;
                    }
                }

                if (!TextUtils.isEmpty(item.image)) {
                    String cachePath = EffectsMainFestManager.GetImageCacheFilePath(item.image);
                    try {
                        PicassoUtils.loadImage(imageView.getContext(), new File(cachePath), imageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getGiftName(String type) {
        String result = "";
        /**
         * 判断本地是否支持此礼物
         */
        GiftManifest giftManifestItem = EffectsMainFestManager.aManager.getGiftManifest();
        if (giftManifestItem != null) {
            GiftManifestItem item = giftManifestItem.find(type);
            if (item != null) {
                result = item.getName();
                if (!TextUtils.isEmpty(result)) {
                    return result;
                }
            }
        }
        return result;
    }

    public int getGiftPopular(String type) {
        GiftManifest manifest = EffectsMainFestManager.aManager.getGiftManifest();
        if (manifest != null) {
            GiftManifestItem item = manifest.find(type);
            if (item != null) {
                return item.popular;
            }
        }
        return 0;
    }

    public boolean isSupportType(String giftType) {
        GiftManifest giftManifestItem = EffectsMainFestManager.aManager.getGiftManifest();
        if (giftManifestItem != null) {
            GiftManifestItem item = giftManifestItem.find(giftType);
            if (item != null) {
                /**
                 * 先去本地查看
                 */
                if (ApkResourceManager.GetApkImageResource(item.localImage) > 0) {
                    /**
                     * 本地支持
                     */
                    return true;
                } else {
                    /**
                     * 本地不支持
                     */
                    if (TextUtils.isEmpty(item.image)) {
                        return false;
                    } else {
                        return true;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void findGiftForReactNative(String giftType, Callback callback) {
        ThreadToolKit.Async(new Runnable() {

            public void callBack(GiftInfoForReact react) {
                if (callback != null) {
                    Log.i(TAG, "callBack: " + GsonHelper.getGson().toJson(react));
                    callback.invoke(GsonHelper.getGson().toJson(react));
                }
            }

            @Override
            public void run() {
                GiftInfoForReact forReact = new GiftInfoForReact();
                /**
                 * 支持这个礼物
                 */
                if (isSupportType(giftType)) {
                    forReact.gift_name = getGiftName(giftType);
                    /**
                     * 判断apk中是否支持
                     */
                    GiftManifest manifest = EffectsMainFestManager.aManager.getGiftManifest();
                    if (manifest == null) {
                        return;
                    }
                    GiftManifestItem item = manifest.find(giftType);
                    if (item != null) {
                        if (ApkResourceManager.GetApkImageResource(item.localImage) > 0) {
                            forReact.gift_image = "local";
                            callBack(forReact);
                            return;
                        } else {
                            String localPath = EffectsMainFestManager.GetImageCacheFilePath(item.image);
                            if (!TextUtils.isEmpty(localPath)) {
                                forReact.gift_image = localPath;
                            }
                        }
                    }
                }
                callBack(forReact);
            }
        });
    }

    private void diffCheckGiftManifest(int localVersion, GiftManifest input, Action<Integer> callback) {
        // TODO: 2017/11/10 这些代码需要在异步线程中执行这些代码需要在异步线程中执行
        GiftManifest giftManifest = EffectsMainFestManager.aManager.getGiftManifest();

        if (giftManifest == null) {
            /**
             * 这里说明本地没有存储过任何的清单文件，那么先进行一次保存
             */
            try {
                EffectsMainFestManager.aManager.saveGiftManifest(input);
                downloadAndVerifyGiftManifest(input, false, callback);
            } catch (Exception e) {
                e.printStackTrace();
                ReportEventUtils.reportError(e);
            }
        } else {
            try {
                if (giftManifest.rev == input.rev) {
                    /**
                     * 版本号一样，按照校验成功返回
                     */
                    if (callback != null) {
                        callback.func(0);
                    }
                } else {
                    downloadAndVerifyGiftManifest(input, false, callback);
                }
            } catch (Exception e) {
                /**
                 * 出现任何错误认为比对失败
                 */
                if (callback != null) {
                    callback.func(-1);
                }
            }
        }
    }

    private void downloadAndVerifyGiftManifest(GiftManifest input, boolean force, Action<Integer> callback) {
        /**
         * 拉取数据
         */
        EffectsMainFestManager.aManager.downloadAndSaveGiftsManifestResource(input, force);
        /**
         * 下载完毕后，进行一次文件完整性校验
         */
        Set<String> verifyResult = EffectsMainFestManager.aManager.verifyGiftDecorManifestData(input);
        if (verifyResult != null) {
            if (verifyResult.size() > 0) {
                /**
                 * 检测出现失败的数据,这里再进行一此校验,这里不再关注是否成功与失败，仅是进行一次校验尝试
                 */
                Set<String> nextTry = EffectsMainFestManager.aManager.verifyGiftDecorManifestData(input);
                if (nextTry.size() > 0) {
                    /**
                     * 第二次校验还是失败了，退出,并不保存数据
                     */
                    if (callback != null) {
                        callback.func(-1);
                    }
                    return;
                } else {
                    /**
                     * 校验成功，并保存数据
                     */
                    EffectsMainFestManager.aManager.saveGiftManifest(input);
                    if (callback != null) {
                        callback.func(0);
                    }
                    return;
                }
            } else {
                /**
                 * 校验成功，并保存数据
                 */
                EffectsMainFestManager.aManager.saveGiftManifest(input);
                if (callback != null) {
                    callback.func(0);
                }
                return;
            }
        } else {
            /**
             * 输入数据居然是空的，这里终止所有检查
             */
            if (callback != null) {
                callback.func(-1);
            }
            return;
        }
    }

}
