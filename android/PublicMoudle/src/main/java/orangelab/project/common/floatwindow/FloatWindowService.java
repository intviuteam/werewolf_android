package orangelab.project.common.floatwindow;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import orangelab.project.common.floatwindow.FloatWindowInnerManager;
import orangelab.project.common.floatwindow.model.FloatUser;
import orangelab.project.common.floatwindow.receiver.HomeWatcherReceiver;
import com.networktoolkit.transport.Constant;


public class FloatWindowService extends Service {

    private static final String TAG = "FloatWindowService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate: ");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand: " + intent);
        if (intent != null) {
            initWindowData((FloatUser) intent.getSerializableExtra(Constant.DATA));
            FloatWindowInnerManager.INSTANCE.setServerHasStart(true);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initWindowData(FloatUser userHead) {
        if (userHead == null) {
            return;
        }
        FloatWindowInnerManager.LaunchFloatWindow(this, userHead);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy: ");
        super.onDestroy();
        FloatWindowInnerManager.Destroy();
        FloatWindowInnerManager.INSTANCE.setServerHasStart(false);
    }
}
