package orangelab.project.common.pay.google.v2;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.datasource.GlobalUserState;
import com.google.gson.JsonSyntaxException;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import orangelab.project.MainApplication;
import orangelab.project.common.db.DBConstant;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.PayHelper;
import orangelab.project.common.pay.PayHelperListener;
import orangelab.project.common.pay.google.GooglePayUtils;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2018/6/21 下午1:22
 * Mystery
 */
public class GooglePayV2SubscriptionsHelper extends PayHelper implements BillingManager.BillingManagerObserver {

    private static final String TAG = "GooglePayV2Subs";

    private BillingManager mBillingManager;
    private String mProductType;
    private UserPayOrderDaoV2Helper mDaoV2Helper;
    private SafeHandler mSafeHandler = new SafeHandler();

    public GooglePayV2SubscriptionsHelper(Activity mActivity, String productId, PayHelperListener listener) {
        super(productId, listener);
        mProductType = BillingClient.SkuType.SUBS;
        mBillingManager = new BillingManager(mActivity, this);
        mDaoV2Helper = MainApplication.getInstance().getUserPayOrderDaoV2Helper();
    }

    @Override
    public void repay() {
        if (mPayListener != null) {
            mPayListener.onStart();
        }
        mBillingManager.queryPurchasesSubs();
    }

    @Override
    public void pay() {
        if (mPayListener != null) {
            mPayListener.onStart();
        }
        mBillingManager.queryPurchasesSubs();
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return false;
    }

    @Override
    public void release() {
        mBillingManager.destroy();
        mSafeHandler.release();
    }

    @Override
    public void onSetupFinished() {
        Log.i(TAG, "onSetupFinished: ");
    }

    @Override
    public void onSetupError(int errorCode) {
        Log.i(TAG, "onSetupError: " + errorCode);
        googleError(errorCode);
    }

    @Override
    public void onPurchasesQuery(List<Purchase> purchases) {
        Log.i(TAG, "onPurchasesQuery: " + purchases);
        /**
         * 这里要做一个判断之类的
         */
        Runnable runnable = () -> mBillingManager.initiatePurchaseFlow(mType, mProductType);
        mBillingManager.executeServiceRequest(runnable);
    }

    @Override
    public void onPurchasesQueryError(int errorCode) {
        Log.i(TAG, "onPurchasesQueryError: " + errorCode);
        googleError(errorCode);
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        Log.i(TAG, "onPurchasesUpdated: " + purchases);
        if (purchases == null) {
            googleError(BillingClient.BillingResponse.ERROR);
            return;
        }
        for (Purchase purchase : purchases) {
            if (TextUtils.equals(purchase.getSku(), mType)) {
                /**
                 * 进行汇报服务器
                 */
                /**
                 * 报告支付成功
                 */
                ReportRobot.robot.report("GOOGLE_PAY_SUCCESS");
                /**
                 * 向数据库插入一条数据，作为订单的一个记录
                 */
                UserPayOrderEntity entity = new UserPayOrderEntity();
                entity.setUserId(GlobalUserState.getGlobalState().getUserId());
                /**
                 * 订单状态用于区分是订阅状态还是非订阅状态
                 */
                entity.setOrderState(DBConstant.PAY_SUCCESS_BUT_NOT_REPORT_SERVER_SUBS);
                entity.setProductId(purchase.getSku());
                entity.setPackageName(purchase.getPackageName());
                entity.setOrderId(purchase.getOrderId());
                entity.setPurchaseTime(purchase.getPurchaseTime());
                entity.setPurchaseToken(purchase.getPurchaseToken());
                entity.setPurchaseData(purchase.getOriginalJson());
                entity.setSignature(purchase.getSignature());
                entity.setOtherData1("0");
                entity.setOtherData2(GlobalUserState.getGlobalState().getToken());
                try {
                    if (mDaoV2Helper != null) {
                        mDaoV2Helper.insertData(entity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.runSafely(() -> {
                        ReportRobot.robot.report("INSERT_PAY_ORDER_FAILED");
                    });
                }

                /**
                 * 开始汇报服务器
                 */
                try {
                    JSONObject jsonObject = new JSONObject(purchase.getOriginalJson());
                    JSONObject requestJson = new JSONObject();
                    requestJson.put("receipt", jsonObject);
                    reportToServerWithRetry(
                            entity.getOtherData2(),
                            requestJson,
                            () -> {
                                mSafeHandler.postSafely(() -> {
                                    /**
                                     * 支付成功
                                     */
                                    if (mPayListener != null) {
                                        mPayListener.onSuccess();
                                    }
                                });
                            }
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                    serverError(-1, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_FAIL));
                }

            }
        }
    }

    @Override
    public void onPurchaseFlowError(int errorCode) {
        Log.i(TAG, "onPurchaseFlowError: " + errorCode);
        googleError(errorCode);
    }

    @Override
    public void onPurchasesUpdatedError(int errorCode) {
        Log.i(TAG, "onPurchasesUpdatedError: " + errorCode);
        googleError(errorCode);
    }

    @Override
    public void onConsumeSuccess(String token) {

    }

    @Override
    public void onConsumeError(int errorCode) {

    }

    protected void googleError(int errorCode) {
        if (mPayListener != null) {
            mPayListener.onErrorFromGoogle(GooglePayUtils.googleBillingResponseCodeTranslate(errorCode));
        }
    }

    protected void serverError(int errorCode, String reason) {
        if (mPayListener != null) {
            mPayListener.onErrorFromServer(errorCode, reason);
        }
    }

    private void reportToServerWithRetry(String token, JSONObject jsonObject, Runnable success) {
        reportToServer(token, jsonObject, success, 5);
    }

    private void reportToServer(String token, JSONObject jsonObject, Runnable success, final int retryCount) {
        Log.i(TAG, "reportToServer: " + token + "," + jsonObject);
        RequestTask.create().isGet(false).withToken(token).request(Constant.GOOGLE_SUBS_PAY).load(jsonObject)
                .action(new RequestTaskCallBack() {

                    private boolean needRetry() {
                        return retryCount > 0;
                    }

                    private void retry(int code, String msg, boolean needRetry) {
                        if (needRetry() && needRetry) {
                            final int tempRetryCount = retryCount - 1;
                            reportToServer(token, jsonObject, success, tempRetryCount);
                        } else {
                            serverError(code, msg);
                        }
                    }

                    @Override
                    public void onSuccess(String response) {
                        mSafeHandler.postSafely(() -> {
                            if (success != null) {
                                success.run();
                            }
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(code, failed, false);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(-1, MessageUtils.getString(R.string.network_is_unavailable), true);
                        });
                    }
                }).build().execute();
    }

}
