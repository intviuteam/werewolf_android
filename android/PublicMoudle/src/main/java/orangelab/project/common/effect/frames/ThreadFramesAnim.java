package orangelab.project.common.effect.frames;
import android.graphics.Bitmap;
import android.os.HandlerThread;
import android.util.Log;
import android.widget.ImageView;

import com.androidtoolkit.PLog;

import java.lang.ref.SoftReference;

import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2018/7/24 下午3:01
 * Mystery
 * 子线程动画
 */
public abstract class ThreadFramesAnim implements Runnable {

    public interface FrameCreator {

        Bitmap createFrame();

        long getDuration();
    }

    public interface Listener {

        void onStart();

        void onPause();

        void onResume();

        void onStop();

        void onFinish();
    }

    private final String TAG;

    private SoftReference<ImageView> mSoftReferenceImageView;
    private SafeHandler mUIHandler;
    private HandlerThread mHandlerThread;
    private SafeHandler mHandler;

    protected Bitmap mBitmap;

    protected boolean mShouldRun = false;
    protected boolean mPause = false;
    protected boolean mResume = false;
    protected boolean mIsInit = false;
    protected int mRepeatTime = 1;
    protected Listener mListener;

    public ThreadFramesAnim(String tag, ImageView imageView) {
        this.TAG = tag;
        mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
        mUIHandler = new SafeHandler();
    }

    final protected void reportStart() {
        mUIHandler.postSafely(() -> {
            onStart();
            if (mListener != null) {
                mListener.onStart();
            }
        });
    }

    final protected void reportPause() {
        mUIHandler.postSafely(() -> {
            onPause();
            if (mListener != null) {
                mListener.onPause();
            }
        });
    }

    final protected void reportResume() {
        mUIHandler.postSafely(() -> {
            onResume();
            if (mListener != null) {
                mListener.onResume();
            }
        });
    }

    final protected void reportStop() {
        mUIHandler.postSafely(() -> {
            onStop();
            if (mListener != null) {
                mListener.onStop();
            }
        });
    }

    final protected void reportFinish() {
        mUIHandler.postSafely(() -> {
            onFinish();
            ImageView imageView = mSoftReferenceImageView.get();
            if (imageView != null) {
                imageView.setImageDrawable(null);
            }
            if (mListener != null) {
                mListener.onFinish();
            }
        });
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public boolean isRunning() {
        return mShouldRun;
    }

    public void init() {
        mHandlerThread = new HandlerThread(TAG + hashCode());
        mHandlerThread.start();
        mHandler = new SafeHandler(mHandlerThread.getLooper());
        mIsInit = true;
    }

    final public boolean isInit() {
        return mIsInit;
    }

    public void setRepeat(int repeat) {
        post(() -> {
            mRepeatTime = repeat;
        });
    }

    final public void start() {
        post(() -> {
            if (mShouldRun) {
                return;
            }
            mShouldRun = true;
            /**
             * 开始执行
             */
            Log.i(TAG, "start: ");
            post(this);
            reportStart();
        });
    }

    final public void stop() {
        stopDelay(0);
    }

    final public void stopDelay(long delay) {
        post(() -> {
            mShouldRun = false;
            mPause = false;
            mResume = false;
            Log.i(TAG, "stop: ");
            reportStop();
            post(this);
        }, delay);
    }

    final public void pause() {
        post(() -> {
            mPause = true;
            mResume = false;
        });
    }

    final public void resume() {
        post(() -> {
            mResume = true;
            mPause = false;
            if (mShouldRun) {
                post(this);
            }
        });
    }


    final public void post(Runnable runnable) {
        post(runnable, 0);
    }

    final public void post(Runnable runnable, long delay) {
        if (!mIsInit) {
            return;
        }
        if (mHandler != null) {
            mHandler.postDelaySafely(runnable, delay);
        }
    }

    final protected void postUI(Runnable runnable) {
        postUI(runnable, 0);
    }

    final protected void postUI(Runnable runnable, long delay) {
        if (mUIHandler != null) {
            mUIHandler.postDelaySafely(runnable, delay);
        }
    }


    @Override
    public void run() {

        if (mPause) {
            PLog.i(TAG, "pause is true");
            reportPause();
            return;
        }

        if (mResume) {
            PLog.i(TAG, "resume is true");
            reportResume();
            mResume = false;
        }

        if (!mShouldRun) {
            PLog.i(TAG, "shouldRun is false");
            reportFinish();
            return;
        }

        onDraw();

    }

    public void release() {
        Log.i(TAG, "release: ");
        post(() -> {

            mShouldRun = false;

            mPause = false;
            mResume = false;

            mBitmap = null;

            onRelease();

            if (mUIHandler != null) {
                mUIHandler.release();
                mUIHandler = null;
            }
            if (mHandler != null) {
                mHandler.release();
                mHandler = null;
            }
            if (mHandlerThread != null) {
                mHandlerThread.quit();
                mHandlerThread = null;
            }

        });
        mIsInit = false;
    }

    protected void drawBitmap(final Bitmap bitmap) {
        ImageView imageView = mSoftReferenceImageView.get();
        if (imageView != null && bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    protected void clearImageView() {
        ImageView imageView = mSoftReferenceImageView.get();
        if (imageView != null) {
            imageView.setImageDrawable(null);
        }
    }

    protected abstract void onResume();

    protected abstract void onStart();

    protected abstract void onStop();

    protected abstract void onPause();

    protected abstract void onRelease();

    protected abstract void onDraw();

    protected abstract void onFinish();

}
