package orangelab.project.common.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * game_werewolf
 * 2017/7/5 下午5:08
 * Mystery
 */

public class CardListResult {
    public HashMap<String, CardListResultItem> cards;

    public static class CardListResultItem {
        public int count;
        public ArrayList<Long> expires;
    }
}
