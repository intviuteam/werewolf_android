package orangelab.project.common.db;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.db.gen.DaoMaster;
import orangelab.project.common.db.gen.DaoSession;
import orangelab.project.common.db.gen.UserPayOrderEntityDao;
import com.androidtoolkit.PLog;

import java.util.List;

/**
 * game_werewolf
 * 2017/8/11 下午1:53
 * Mystery
 */

public class UserPayOrderDaoHelper {

    private static final String TAG = "UserPayOrderDaoHelper";
    private DaoMaster.DevOpenHelper mHelper;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    private UserPayOrderEntityDao mUserPayDao;
    private boolean isDestroy = false;

    public UserPayOrderDaoHelper(Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context, DBConstant.DB_NAME, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = mDaoMaster.newSession();
        PLog.i(TAG, "UserPayOrderDaoHelper: create Session Over");
        mUserPayDao = mDaoSession.getUserPayOrderEntityDao();
        PLog.i(TAG, "UserPayOrderDaoHelper: create UserPayDao Over");
    }

    public void insertData(UserPayOrderEntity entity) {
        Log.i(TAG, "insertData: ");
        if (!isDestroy) {
            if (mUserPayDao != null) {
                mUserPayDao.insert(entity);
            }
        }
    }

    public void updateDataOtherData(String userId, String orderId, String otherData) {
        if (!isDestroy) {
            if (mUserPayDao != null) {
                UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                        UserPayOrderEntityDao.Properties.UserId.eq(userId),
                        UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
                ).build().unique();
                if (entity != null) {
                    entity.setOtherData1(otherData);
                    mUserPayDao.update(entity);
                    Log.i(TAG, "updateData: update success");
                }
            }
        }
    }

    public void updateData(String userId, String orderId, int state) {
        if (!isDestroy) {
            if (mUserPayDao != null) {
                UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                        UserPayOrderEntityDao.Properties.UserId.eq(userId),
                        UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
                ).build().unique();
                if (entity != null) {
                    entity.setOrderState(state);
                    mUserPayDao.update(entity);
                    Log.i(TAG, "updateData: update success");
                }
            }
        }
    }

    public void deleteData(String userId, String orderId) {
        if (TextUtils.isEmpty(orderId))
            return;
        if (!isDestroy) {
            Log.i(TAG, "deleteData: ");
            if (mUserPayDao != null) {
                UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                        UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
                ).build().unique();
                if (entity != null) {
                    mUserPayDao.delete(entity);
                }
            }
        }
    }

    public List<UserPayOrderEntity> getAllUserOrderEntity() {
        if (!isDestroy) {
            Log.i(TAG, "getAllUserOrderEntity: ");
            if (mUserPayDao != null) {
                List<UserPayOrderEntity> entities = mUserPayDao.queryBuilder().build().list();
                return entities;
            }
        }
        return null;
    }

    public UserPayOrderEntity getUserPayOrderByOrderId(String orderId) {
        if (TextUtils.isEmpty(orderId)) {
            return null;
        }
        if (!isDestroy) {
            if (mUserPayDao != null) {
                UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(UserPayOrderEntityDao.Properties.OrderId.eq(orderId)).build().unique();
                return entity;
            }
        }
        return null;
    }

    public List<UserPayOrderEntity> getAllUserOrderEntity(String userId) {
        if (TextUtils.isEmpty(userId))
            return null;
        if (!isDestroy) {
            Log.i(TAG, "getAllUserOrderEntity: ");
            if (mUserPayDao != null) {
                List<UserPayOrderEntity> entities = mUserPayDao.queryBuilder().where(
                        UserPayOrderEntityDao.Properties.UserId.eq(userId)
                ).build().list();
                return entities;
            }
        }
        return null;
    }


    public void release() {
        Log.i(TAG, "release: ");
        if (!isDestroy) {
            if (mUserPayDao != null) {
                mUserPayDao = null;
            }
            if (mDaoSession != null) {
                mDaoSession.clear();
                mDaoSession = null;
            }
            if (mDaoMaster != null) {
                mDaoMaster = null;
            }
            if (mHelper != null) {
                mHelper.close();
                mHelper = null;
            }
            isDestroy = true;
            PLog.i(TAG, "release: release Over");
        }
    }
}
