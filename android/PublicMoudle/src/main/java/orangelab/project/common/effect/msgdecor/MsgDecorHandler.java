package orangelab.project.common.effect.msgdecor;

import android.os.Message;

import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2018/7/13 下午2:41
 * Mystery
 */
public class MsgDecorHandler extends SafeHandler {

    @Override
    public void handleMessage(Message msg) {
        Object object = msg.obj;
        if (object == null) return;
        if (object instanceof Runnable) {
            ((Runnable) object).run();
        }
    }
}
