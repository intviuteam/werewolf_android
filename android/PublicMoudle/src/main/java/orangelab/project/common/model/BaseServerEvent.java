package orangelab.project.common.model;

import android.text.TextUtils;
import android.util.Log;

import orangelab.project.common.engine.SocketEngine;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Author:1727
 * 2017/3/2
 */

public class BaseServerEvent {

    public static final String TYPE = "type";
    public static final String CODE = "code";
    public static final String PAYLOAD = "payload";
    public static final String MSG_ID = "msg_id";
    public static final String MSG_TIME = "msg_time";
    public static final String REQUEST_ID = "request_id";

    public String type;

    public long msg_id = -1;
    public long msg_time = -1;

    public JSONObject payload;
    public String fromSocketType;


    public BaseServerEvent(String fromSocketType, JSONObject jsonObject) {

        this.fromSocketType = fromSocketType;

        try {
            type = jsonObject.getString(TYPE);

            if (jsonObject.has(PAYLOAD)) {
                payload = jsonObject.getJSONObject(PAYLOAD);
            } else {
                payload = new JSONObject();
            }
            if (jsonObject.has(MSG_TIME)) {
                msg_time = jsonObject.optLong(MSG_TIME);
            } else {
                msg_time = System.currentTimeMillis();
            }

            if (payload != null && payload.has(MSG_ID)) {
                msg_id = payload.optLong(MSG_ID);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getFromSocketType() {
        return fromSocketType;
    }

    public boolean isMiniGame() {
        return TextUtils.equals(fromSocketType, SocketEngine.MINI_GAME);
    }

    public boolean isWereWolf() {
        return TextUtils.equals(fromSocketType, SocketEngine.WEREWOLF);
    }

    public boolean isGlobal() {
        return TextUtils.equals(fromSocketType, SocketEngine.GLOBAL);
    }

    public boolean isAudio() {
        return TextUtils.equals(fromSocketType, SocketEngine.AUDIO);
    }

    public boolean isSpyRoom() {
        return TextUtils.equals(fromSocketType, SocketEngine.SPY_ROOM);
    }

    public boolean isPair() {
        return TextUtils.equals(fromSocketType, SocketEngine.PAIR);
    }

    public long getMsgTime() {
        return msg_time;
    }

    @Override
    public String toString() {
        return "type='" + type + '\n' +
                "payload=" + payload;
    }
}
