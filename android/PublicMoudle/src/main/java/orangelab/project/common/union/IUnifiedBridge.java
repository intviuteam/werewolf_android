package orangelab.project.common.union;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/9/20 下午3:02
 * Mystery
 */

public interface IUnifiedBridge {

    void SendCommendIntoRN(String action);

    void SendCommendIntoRN(String action, boolean needcallback);

    void SendCommendIntoRN(String action, JSONObject params);

    void SendCommendIntoRN(String action, JSONObject params, boolean sync, boolean needcallback, long timeout);

    void SendCommendIntoJS(String action);

    void SendCommendIntoJS(String action, boolean needcallback);

    void SendCommendIntoJS(String action, JSONObject params);

    void SendCommendIntoJS(String action, JSONObject params, boolean sync, boolean needcallback, long timeout);
}
