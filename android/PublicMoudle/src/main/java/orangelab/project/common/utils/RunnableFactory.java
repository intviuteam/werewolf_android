package orangelab.project.common.utils;

import com.androidtoolkit.ToastToolKit;
import com.R;

/**
 * game_werewolf
 * 2017/8/21 下午4:31
 * Mystery
 */

public class RunnableFactory {

    public static Runnable createNetworkErrorRunnable() {
        return () -> {
            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
        };
    }

    public static Runnable createMsgToastRunnable(String msg) {
        return () -> {
            ToastToolKit.showShort(msg);
        };
    }

    public static Runnable createMsgToastLongRunnable(String msg) {
        return () -> {
            ToastToolKit.showLong(msg);
        };
    }
}
