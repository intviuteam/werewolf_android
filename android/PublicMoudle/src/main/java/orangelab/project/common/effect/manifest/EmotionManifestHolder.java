package orangelab.project.common.effect.manifest;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.toolkit.ToolKit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.common.tool.GsonTurnToolKit;
import orangelab.project.common.utils.Utils;

/**
 * Created by hwx on 25/07/2018.
 */

public class EmotionManifestHolder extends EffectsManifestHolder<EmotionManiFest> {

    private static final String TAG = "EmotionManifestHolder";

    public EmotionManifestHolder(ManifestManagerHelper helper, String cacheManifestFile, String cacheManifestVersionFile, String diskLruType) {
        super(helper, cacheManifestFile, cacheManifestVersionFile, diskLruType);
    }

    @Override
    public void saveManifest(EmotionManiFest data) {
        try {
            String temp = GsonHelper.getGson().toJson(data);
            mHelper.putString(mCacheManifestFile, temp);
            putVersion(data.version);
            updateManifest(data);
            PLog.i(TAG, "save Emotion: success");
        } catch (Exception e) {
            e.printStackTrace();
            PLog.i(TAG, "save Emotion: failed");
        }

    }

    @Override
    public EmotionManiFest transformData(String data) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        return GsonTurnToolKit.turn(data, EmotionManiFest.class);
    }

    @Override
    public void downloadManifestResource(EmotionManiFest data, boolean forceRefresh) {
        if (data == null) {
            return;
        }
        List<EmotionManiFest.Emotion> items = data.data;
        if (ToolKit.isEmpty(items)) {
            return;
        }
        for (EmotionManiFest.Emotion item : items) {

            /**
             * 下载所有的表情图片数据
             */
            if (!TextUtils.isEmpty(item.thumbnail)) {
                downloadAndSaveIfNeed(item.type, item.thumbnail, forceRefresh);
            }
            if (!TextUtils.isEmpty(item.url)) {
                downloadAndSaveIfNeedForGif(item.type, item.url, forceRefresh);
            }
        }
    }

    public Set<String> verifyManifest(EmotionManiFest manifest) {
        Set<String> failedGiftType = new HashSet<>();
        if (manifest == null) {
            return failedGiftType;
        }
        try {
            if (Utils.targetListIsNull(manifest.data)) {
                return null;
            }
            for (EmotionManiFest.Emotion item : manifest.data) {
                //校验缩略图
                if (!TextUtils.isEmpty(item.type)) {
                    if (ApkResourceManager.GetApkImageResource(item.type) == 0) {
                        if (!verifyUrlData(item.thumbnail) || !verifyUrlData(item.url)) {
                            failedGiftType.add(item.type);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (failedGiftType.size() > 0) {
            PLog.i(TAG, "表情列表验证失败 失败的礼物为" + failedGiftType.toString());
        } else {
            PLog.i(TAG, "表情列表验证成功---------------------------------------------------------------------------------------------------------");
        }
        return failedGiftType;
    }
}
