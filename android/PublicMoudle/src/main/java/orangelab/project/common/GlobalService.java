package orangelab.project.common;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.UIActuator;

import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.engine.SocketEngineConfig;
import orangelab.project.minigame.MiniGameGlobalServiceHandler;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.project.voice.privateroom.PrivateRoomGlobalServiceHandler;
import orangelab.project.voice.privateroom.PrivateRoomMultiIntviuGlobalServiceHandler;

/**
 * game_werewolf
 * 2018/1/30 上午11:26
 * Mystery
 */

public class GlobalService extends Service implements IGlobalService {

    public static final String GLOBAL_ACTION = "global_action";

    public static final String GLOBAL_SOCKET_INIT = "global_init";
    public static final String GLOBAL_SOCKET_DESTROY = "global_destroy";

    public static final String APP_IN_BACKGROUND = "app_in_background";
    public static final String APP_ON_TOP = "app_on_top";

    public static final String NOTIFY_SERVER = "notify_server";
    public static final String CHANGE_SERVER = "change_server";

    private static final String TAG = "GlobalService";

    private HandlerThread mHandlerThread;
    private Handler mHandler;
    private List<IGlobalServiceHandler> mGlobalServiceHandlers = null;
    private boolean isGlobalSocketDestroy = true;

    private GlobalFloatWindowManager mFloatWindowManager;

    private void startThread() {
        printLog("startThread");
        mHandlerThread = new HandlerThread(TAG + "Thread");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    public void post(Runnable runnable) {
        post(runnable, 0);
    }

    public void remove(Runnable runnable) {
        if (mHandler != null) {
            mHandler.removeCallbacks(runnable);
        }
    }

    public void post(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(runnable, delay);
        }
    }

    private void releaseThread() {
        printLog("releaseThread");
        if (mHandlerThread != null) {
            mHandlerThread.quit();
            mHandlerThread = null;
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        printLog("onCreate");

        mFloatWindowManager = new GlobalFloatWindowManager(this, this);
        mGlobalServiceHandlers = new ArrayList<>();
        mGlobalServiceHandlers.add(new MiniGameGlobalServiceHandler(this));
        mGlobalServiceHandlers.add(new PrivateRoomGlobalServiceHandler(this));
        mGlobalServiceHandlers.add(new PrivateRoomMultiIntviuGlobalServiceHandler(this, mFloatWindowManager));

        GlobalSocketEngine.INSTANCE.bindGlobalService(this);

        startThread();
    }

    private void initListener() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(value -> {
            post(() -> {
                if (TextUtils.equals(value.getFromSocketType(), SocketEngine.GLOBAL)) {
                    String type = value.payload.optString("type");
                    if (TextUtils.isEmpty(type)) {

                        return;
                    } else {
                        if (TextUtils.equals(value.type, NOTIFY_SERVER)) {
                            /**
                             * 处理服务器地址突然的切换
                             */
                            if (TextUtils.equals(type, CHANGE_SERVER)) {
                                SocketEngineConfig config = GsonHelper.getGson().fromJson(value.payload.optJSONObject("data").toString(), SocketEngineConfig.class);
                                UIActuator.post(() -> {
                                    destroyGlobalSocket();
                                    setUpGlobalSocket(config);
                                });
                                return;
                            }

                        }
                        /**
                         * 将数据透传给RN
                         */
                        try {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("type", value.type);
                                jsonObject.put("payload", value.payload);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_RECEIVE_MSG_FROM_GLOBAL_SOCKET, jsonObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        /**
                         * 向Handler分发不同的数据消息
                         */
                        try {
                            for (IGlobalServiceHandler globalServiceHandler : mGlobalServiceHandlers) {
                                if (globalServiceHandler.isCareType(value.type)) {
                                    globalServiceHandler.handleMessage(type, value.payload.toString());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }).register();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand: ");
        if (intent != null) {
            String action = intent.getStringExtra(GLOBAL_ACTION);
            if (TextUtils.isEmpty(action)) {
                /**
                 * 空的Action不进行执行
                 */
            } else {
                if (TextUtils.equals(action, GLOBAL_SOCKET_INIT)) {
                    /**
                     * 初始化GlobalSocket
                     */
                    Log.i(TAG, "onStartCommand: Global init");
                    setUpGlobalSocket();
                } else if (TextUtils.equals(action, GLOBAL_SOCKET_DESTROY)) {
                    Log.i(TAG, "onStartCommand: Global destroy");
                    /**
                     * 销毁GlobalSocket
                     */
                    destroyGlobalSocket();
                } else if (TextUtils.equals(action, APP_IN_BACKGROUND)) {
                    /**
                     * 应用进入到了后台
                     */
                    if (mFloatWindowManager != null) {
                        mFloatWindowManager.appInBackGround();
                    }
                } else if (TextUtils.equals(action, APP_ON_TOP)) {
                    /**
                     * 应用进入到了前台
                     */
                    if (mFloatWindowManager != null) {
                        mFloatWindowManager.appOnTop();
                    }
                } else if (TextUtils.equals(action, PrivateRoomInviteFloatWindowHelper.DESTROY_INVITE)) {
                    /**
                     * 关闭私密房邀请View
                     */
                    if (mFloatWindowManager != null) {
                        mFloatWindowManager.destroyPrivateRoomInvite();
                    }
                }
            }
        }
        return START_STICKY;
    }

    public void sendMessage(String type, String msg) {
        post(() -> {
            GlobalSocketEngine.INSTANCE.sendMessage(type, msg);
        });
    }

    private void destroyServerHandlers() {
        printLog("destroyServerHandlers");
        if (mGlobalServiceHandlers != null) {
            for (IGlobalServiceHandler temp : mGlobalServiceHandlers) {
                temp.destroy();
            }
            mGlobalServiceHandlers.clear();
        }
    }

    private void setUpGlobalSocketInner() {
        printLog("setUpGlobalSocketInner");
        initListener();
        GlobalSocketEngine.INSTANCE.clearState();
    }

    private void setUpGlobalSocket(SocketEngineConfig config) {
        printLog("setUpGlobalSocket with Config");
        if (isGlobalSocketDestroy) {
            setUpGlobalSocketInner();
            GlobalSocketEngine.INSTANCE.initWithServer(config);
        } else {
            destroyGlobalSocket();
            setUpGlobalSocket(config);
        }
    }

    private void setUpGlobalSocket() {
        printLog("setUpGlobalSocket");
        if (isGlobalSocketDestroy) {
            setUpGlobalSocketInner();
            GlobalSocketEngine.INSTANCE.clearState();
            GlobalSocketEngine.INSTANCE.initWithOutServer();
            isGlobalSocketDestroy = false;
        } else {
            destroyGlobalSocket();
            setUpGlobalSocket();
        }
    }

    private void destroyGlobalSocket() {
        printLog("destroyGlobalSocket");
        RxToolKit.UnRegister(this);
        GlobalSocketEngine.INSTANCE.destroy();
        isGlobalSocketDestroy = true;
    }

    public HandlerThread getHandlerThread() {
        return mHandlerThread;
    }

    public Handler getHandler() {
        return mHandler;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        printLog("onDestroy");
        mFloatWindowManager.destroy();

        destroyServerHandlers();
        destroyGlobalSocket();

        releaseThread();

        GlobalSocketEngine.INSTANCE.unBindGlobalService();
    }

    private void printLog(String info) {
        PLog.i(TAG, "[" + Thread.currentThread().getName() + "]:" + info);
    }

    public static void startGlobalSocketEngine(Context context) {
        try {
            Intent intent = new Intent(context, GlobalService.class);
            intent.putExtra(GlobalService.GLOBAL_ACTION, GlobalService.GLOBAL_SOCKET_INIT);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void appOnTop(Context context) {
        try {
            Intent intent = new Intent(context, GlobalService.class);
            intent.putExtra(GlobalService.GLOBAL_ACTION, GlobalService.APP_ON_TOP);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void appInBackGround(Context context) {
        try {
            Intent intent = new Intent(context, GlobalService.class);
            intent.putExtra(GlobalService.GLOBAL_ACTION, GlobalService.APP_IN_BACKGROUND);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void destroyGlobalSocketEngine(Context context) {
        try {
            Intent intent = new Intent(context, GlobalService.class);
            intent.putExtra(GlobalService.GLOBAL_ACTION, GlobalService.GLOBAL_SOCKET_DESTROY);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void destroyPrivateRoomInvite(Context context) {
        try {
            Intent intent = new Intent(context, GlobalService.class);
            intent.putExtra(GlobalService.GLOBAL_ACTION, PrivateRoomInviteFloatWindowHelper.DESTROY_INVITE);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
