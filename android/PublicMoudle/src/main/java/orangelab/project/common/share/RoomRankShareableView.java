package orangelab.project.common.share;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;

/**
 * game_werewolf
 * 2018/2/26 下午7:43
 * Mystery
 */

public class RoomRankShareableView extends ShareableView {

    public RoomRankShareableView(@NonNull Context context) {
        this(context, null);
    }

    public RoomRankShareableView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomRankShareableView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        fillView();
    }

    public void fillView() {
        /**
         * fill View data and init View by Data
         * ......
         */
        View view = View.inflate(getContext(), R.layout.layout_share_room_picture, this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(776, 432);
        setLayoutParams(layoutParams);
    }

    public void initData(RankSharePayload payload) {
        TextView mRank = (TextView) findViewById(R.id.id_rank_list);
        TextView mPopular = (TextView) findViewById(R.id.id_popular);
        TextView mLike = (TextView) findViewById(R.id.id_like);
        TextView mName = (TextView) findViewById(R.id.id_user_name);
        ImageView mHead = (ImageView) findViewById(R.id.id_user_head);
        ImageView mTitle = (ImageView) findViewById(R.id.id_rank_title);

        PicassoUtils.loadImage(getContext(), payload.image, mHead, R.mipmap.default_head);

        mName.setText(payload.name);
        mRank.setText(payload.rankIndex);
        mLike.setText(payload.rankLove);
        mPopular.setText(payload.rankScore);

        if (payload.isOwnerAll()) {
            mTitle.setImageResource(R.mipmap.ico_room_rank_text_total);
            return;
        }
        if (payload.isOwnerLastWeek()) {
            mTitle.setImageResource(R.mipmap.ico_room_rank_text_lastweek);
            return;
        }
        if (payload.isOwnerYesterDay()) {
            mTitle.setImageResource(R.mipmap.ico_room_rank_text_yesterday);
            return;
        }
    }
}
