package orangelab.project.common.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2018/7/20 上午10:13
 * Mystery
 */
@Entity
public class GooglePaySubsOrderAssitEntity {

    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    public String orderId;

    private String extra1; //预留字段1

    private String extra2; //预留字段2

    @Generated(hash = 904240255)
    public GooglePaySubsOrderAssitEntity(Long id, String orderId, String extra1,
            String extra2) {
        this.id = id;
        this.orderId = orderId;
        this.extra1 = extra1;
        this.extra2 = extra2;
    }

    @Generated(hash = 276595905)
    public GooglePaySubsOrderAssitEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getExtra1() {
        return this.extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return this.extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }


    

}
