package orangelab.project.common.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2018/5/22 下午1:52
 * Mystery
 */
@Entity
public class MiniGameAIRecordEntity {

    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    private String user_id;//用户ID

    private String user_token;//用户token

    private String game_type;//游戏类型

    private int ai_level = -1;//单机AI的等级

    private int winning_streak = 0;//连胜记录

    private String extra1; //预留字段1

    private String extra2; //预留字段2

    @Generated(hash = 813640579)
    public MiniGameAIRecordEntity(Long id, String user_id, String user_token,
            String game_type, int ai_level, int winning_streak, String extra1,
            String extra2) {
        this.id = id;
        this.user_id = user_id;
        this.user_token = user_token;
        this.game_type = game_type;
        this.ai_level = ai_level;
        this.winning_streak = winning_streak;
        this.extra1 = extra1;
        this.extra2 = extra2;
    }

    @Generated(hash = 1359613915)
    public MiniGameAIRecordEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_token() {
        return this.user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getGame_type() {
        return this.game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public int getAi_level() {
        return this.ai_level;
    }

    public void setAi_level(int ai_level) {
        this.ai_level = ai_level;
    }

    public int getWinning_streak() {
        return this.winning_streak;
    }

    public void setWinning_streak(int winning_streak) {
        this.winning_streak = winning_streak;
    }

    public String getExtra1() {
        return this.extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return this.extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    @Override
    public String toString() {
        return "MiniGameAIRecordEntity{" +
                "user_id='" + user_id + '\'' +
                ", game_type='" + game_type + '\'' +
                ", ai_level=" + ai_level +
                ", winning_streak=" + winning_streak +
                '}';
    }
}
