package orangelab.project.common.model;

import java.io.Serializable;

/**
 * Created by lloydfinch on 29/08/2017.
 */

public class Message implements Serializable {

    public String id;
    public String message;

    public Message() {
        super();
    }

    public Message(String id, String message) {
        this.id = id;
        this.message = message;
    }
}
