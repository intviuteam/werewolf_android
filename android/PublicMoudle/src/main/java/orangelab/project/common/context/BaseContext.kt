package orangelab.project.common.context

import android.os.Handler
import orangelab.project.common.utils.SafeHandler
import com.toolkit.action.Destroyable

/**
 *game_werewolf
 *2018/3/19 上午10:51
 *Mystery
 */
open abstract class BaseContext : Destroyable, IBaseContext {
    private var mSafeHandler: SafeHandler
    private var mReferenceManager: HashMap<String, Any>

    constructor() {
        mSafeHandler = SafeHandler();
        mReferenceManager = HashMap();
    }

    override fun exec(runnable: Runnable) {
        exec(runnable, 0)
    }

    override fun exec(runnable: Runnable, delay: Long) {
        mSafeHandler.postDelaySafely(runnable, delay)
    }

    override fun getHandler(): Handler {
        return mSafeHandler
    }

    override fun addReference(key: String, reference: Any) {
        mReferenceManager.put(key, reference);
    }

    override fun getReference(key: String): Any? {
        return mReferenceManager.get(key)
    }

    override fun removeReference(key: String) {
        mReferenceManager.remove(key)
    }

    override fun destroy() {
        mSafeHandler.release()
        mReferenceManager.clear()
    }
}