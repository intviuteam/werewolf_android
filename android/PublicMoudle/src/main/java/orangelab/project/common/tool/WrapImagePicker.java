package orangelab.project.common.tool;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.exception.NetWorkException;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.UpLoadTokenResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.view.ActionSheetDialog;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCancellationSignal;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadOptions;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Action;
import com.toolkit.action.ApiResult;
import com.toolkit.action.Destroyable;

import org.json.JSONObject;

import javax.annotation.Nullable;

import orangelab.thirdparty.qiniu.QiNiuManager;
import rx.functions.Action1;

/**
 * game_werewolf
 * 2018/3/30 上午11:26
 * Mystery
 */

public class WrapImagePicker implements Promise, Destroyable {

    public static class EmptyImagePickerObserver implements ImagePickerObserver {


        @Override
        public void onPicked(String path) {

        }

        @Override
        public void onUploadStart(String path) {

        }

        @Override
        public void onUploadProgress(String path, double percent) {

        }

        @Override
        public void onUploadSuccess(String path, String key, String url) {

        }

        @Override
        public void onUploadError(String path, Exception e) {

        }
    }

    public interface ImagePickerObserver {

        void onPicked(String path);

        void onUploadStart(String path);

        void onUploadProgress(String path, double percent);

        void onUploadSuccess(String path, String key, String url);

        void onUploadError(String path, Exception e);
    }

    private static final int IMAGE_PICKER_REQUEST = 61110;
    private static final int CAMERA_PICKER_REQUEST = 61111;
    private static final int CANCEL_PICKER_REQUEST = 69;//取消选取照片

    private WritableMap mImageConfig;
    private ImagePickerObserver mObserver = null;
    private ActionSheetDialog mPickDialog = null;
    private RxPermissions mRxPermissions = null;
    private boolean mUploadCancel = false;

    public WrapImagePicker() {

    }

    public void bindCallBack(ImagePickerObserver observer) {
        mObserver = observer;
    }

    public void initImageConfig(int width, int height) {
        mImageConfig = Arguments.createMap();
        mImageConfig.putInt("width", width);
        mImageConfig.putInt("height", height);
        mImageConfig.putBoolean("cropping", true);
        mImageConfig.putString("cropperTintColor", "#9c4db7");
        mImageConfig.putBoolean("multiple", false);
    }

    public void startPick(final Activity activity) {
        if (mRxPermissions == null) {
            mRxPermissions = new RxPermissions(activity);
        }
        if (mPickDialog == null) {
            mPickDialog = new ActionSheetDialog(activity).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                    .addSheetItem(MessageUtils.getString(R.string.string_image_pick_from_camera), ActionSheetDialog.SheetItemColor.Blue, which -> {
                        if (mRxPermissions != null) {
                            mRxPermissions.request(Manifest.permission.CAMERA).subscribe(aBoolean -> {
                                if (aBoolean) {
                                    cameraPicker(activity);
                                } else {
                                    ToastToolKit.showShort(R.string.string_permission_camera_error);
                                }
                            });
                        }
                    })
                    .addSheetItem(MessageUtils.getString(R.string.string_image_pick_from_phone), ActionSheetDialog.SheetItemColor.Blue, which -> {
                        if (mRxPermissions != null) {
                            mRxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(aBoolean -> {
                                if (aBoolean) {
                                    filesPicker(activity);
                                } else {
                                    ToastToolKit.showShort(R.string.permission_read_store_error);
                                }
                            });
                        }
                    });
        }
        if (!mPickDialog.isShowing()) {
            mPickDialog.show();
        }
    }

    public void cameraPicker(Activity activity) {
        if (mImageConfig == null) {
            throw new IllegalStateException("You should call initImageConfig before camera Picker");
        }
        MainApplication.getInstance().getPickerModule().openCameraWithActivity(mImageConfig, this, activity);
    }

    public void filesPicker(Activity activity) {
        if (mImageConfig == null) {
            throw new IllegalStateException("You should call initImageConfig before camera Picker");
        }
        MainApplication.getInstance().getPickerModule().openPickerWithActivity(mImageConfig, this, activity);
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
        if (requestCode == CAMERA_PICKER_REQUEST || requestCode == IMAGE_PICKER_REQUEST || requestCode == CANCEL_PICKER_REQUEST) {
            if (MainApplication.getInstance().getPickerModule() != null) {
                MainApplication.getInstance().getPickerModule().onActivityResult(activity, requestCode, resultCode, intent);
            } else {

            }
        }
    }

    public void startUpLoad(final String roomId, final String orgPath, final Handler handler) {
        if (TextUtils.isEmpty(orgPath)) {
            return;
        }
        /**
         * 选中了一个图片
         */
        String uploadPath = "";
        if (orgPath.contains("file://")) {
            uploadPath = orgPath.replace("file://", "");
        }
        if (TextUtils.isEmpty(uploadPath)) {
            return;
        }

        final String uploadPathFinal = uploadPath;

        mUploadCancel = false;
        final Handler actionHandler;
        if (handler == null) {
            actionHandler = UIActuator.getHandler();
        } else {
            actionHandler = handler;
        }
        if (mObserver != null) {
            mObserver.onUploadStart(orgPath);
        }
        ApiManager.RefreshUpLoadToken(roomId, (upLoadTokenResult, e) -> {
            actionHandler.post(() -> {
                if (e != null) {
                    if (mObserver != null) {
                        mObserver.onUploadError(orgPath, e);
                    }
                } else {
                    QiNiuManager.UpLoadImage(uploadPathFinal, null, upLoadTokenResult.token, (key, info, response) -> {
                        actionHandler.post(() -> {
                            if (info.isOK()) {
                                String imageUrl = response.optString("url");
                                String imageKey = response.optString("key");
                                if (mObserver != null) {
                                    mObserver.onUploadSuccess(orgPath, imageKey, imageUrl);
                                }
                            } else {
                                if (mObserver != null) {
                                    mObserver.onUploadError(orgPath, new NetWorkException());
                                }
                            }
                        });
                    }, new UploadOptions(null, null, false, (key, percent) -> {
                        actionHandler.post(() -> {
                            if (mObserver != null) {
                                mObserver.onUploadProgress(orgPath, percent);
                            }
                        });
                    }, () -> mUploadCancel));
                }
            });
        });

    }

    @Override
    public void resolve(@Nullable Object value) {
        ReadableMap map = (ReadableMap) value;
        String path = map.getString("path");
        if (mObserver != null) {
            mObserver.onPicked(path);
        }
    }

    @Override
    public void reject(String code, String message) {

    }

    @Override
    public void reject(String code, Throwable e) {

    }

    @Override
    public void reject(String code, String message, Throwable e) {

    }

    @Override
    public void reject(String message) {

    }

    @Override
    public void reject(Throwable reason) {

    }

    public void cancelUpload() {
        mUploadCancel = true;
    }

    @Override
    public void destroy() {

        if (mPickDialog != null) {
            if (mPickDialog.isShowing()) {
                mPickDialog.dismiss();
            }
            mPickDialog = null;
        }
        mObserver = null;
        mRxPermissions = null;
    }
}
