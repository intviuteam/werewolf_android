package orangelab.project.common.model;

import com.toolkit.action.Keepable;

import java.util.List;
import java.util.Map;

/**
 * Author:1727
 * 2017/3/7
 */

public class AssginedRoleResult implements Keepable {

    public String role;
    public int duration;
    public List<Integer> teammates;
    public Map<Integer, String> teammate_roles;
    public int king = -1;
    public int demon = -1;

}
