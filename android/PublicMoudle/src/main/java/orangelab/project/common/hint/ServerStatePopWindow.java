package orangelab.project.common.hint;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.R;

/**
 * 系统名称:断线提示框
 * 模块名称:断线提示框
 * 功能说明:断线提示框
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 * <p>
 * 2017-04-06 温佳煜 创建
 */

public class ServerStatePopWindow extends PopupWindow {

    private TextView text;

    public ServerStatePopWindow(Context context) {
        super(context);
        View rootView = LayoutInflater.from(context).inflate(R.layout.layout_socket_state_pop, null);
        text = (TextView) rootView.findViewById(R.id.pop_socket_state_text);
        setContentView(rootView);
        setFocusable(false);
        setTouchable(false);
        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setOutsideTouchable(false);
        setBackgroundDrawable(new BitmapDrawable(context.getResources()));
    }

    public void disconnectState() {
        text.setText(R.string.socket_disconnect);
    }

    public void reconnectState() {
        text.setText(R.string.socket_reconnect);
    }

    public void showPopWindow(View parent) {
        if (!isShowing()) {
            showAtLocation(parent, Gravity.CENTER, 0, 0);
        }
    }

    public void dismissPopWindow() {
        if (isShowing()) {
            dismiss();
        }
    }
}
