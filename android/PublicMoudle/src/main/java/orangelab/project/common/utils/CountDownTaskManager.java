package orangelab.project.common.utils;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;

import orangelab.project.common.event.MediaEvent;
import orangelab.project.common.event.ViewEvent;

import com.networktoolkit.transport.Constant;
import com.toolkit.action.ActionListener;

/**
 * game_werewolf
 * 2017/5/24 下午4:01
 * Mystery
 */

public class CountDownTaskManager {

    private static final String TAG = "CountDownTaskManager";
    private static final String EMPTY_TASK_TYPE = "";
    private TextView targetTextView;
    private Handler mUIHandler;

    private String curCountDownTaskType = EMPTY_TASK_TYPE;
    private CountDownTask curCountDownTask;
    private boolean hasDanger = true;
    private int dangerTime = 0;
    private int mTextDefaultSize = 16;

    private int mCountDownTextRule = -1;

    public CountDownTaskManager() {
        mUIHandler = new Handler(Looper.getMainLooper());
    }

    public void setTextDefaultSize(int textDefaultSize) {
        mTextDefaultSize = textDefaultSize;
    }

    public void clear() {
        if (curCountDownTask != null) {
            curCountDownTask.cancelTask();
            curCountDownTask.destroyTask();
            curCountDownTask = null;
        }
    }

    public void destroy() {
        if (targetTextView != null) {
            this.targetTextView = null;
        }
        clear();
        if (mUIHandler != null) {
            mUIHandler.removeCallbacksAndMessages(null);
            mUIHandler = null;
        }
    }

    private void execShow(int value) {
        if (mUIHandler != null) {
            mUIHandler.post(() -> {
                if (targetTextView != null) {
                    if (mCountDownTextRule == -1) {
                        targetTextView.setText(Integer.toString(value));
                    } else {
                        targetTextView.setText(MessageUtils.getString(mCountDownTextRule, Integer.toString(value)));
                    }
                }
            });
        }
    }

    private void clearTargetShow() {
        if (mUIHandler != null) {
            mUIHandler.post(() -> {
                if (targetTextView != null) {
                    targetTextView.setText(EMPTY_TASK_TYPE);
                }
            });
        }
    }

    public void bindTarget(TextView targetTextView) {
        this.targetTextView = targetTextView;
    }

    private void startDangerTime() {
        if (TextUtils.equals(curCountDownTaskType, Constant.SELF_SPEECH)) {
            RxToolKit.Emit(new MediaEvent.SpeechDangerTimeBeginEvent());
            RxToolKit.Emit(new ViewEvent.SpeechDangerTimeEvent());
        }
    }

    private void endDangerTime() {
        if (TextUtils.equals(curCountDownTaskType, Constant.SELF_SPEECH)) {
            RxToolKit.Emit(new MediaEvent.SpeechDangerTimeEndEvent());
            RxToolKit.Emit(new ViewEvent.SpeechNoDangerTimeEvent());
        }
    }

    private void clearCurTaskByForce() {
        if (curCountDownTask != null) {
            Log.i(TAG, "curCountDownTask is not finish, so destroy it by force");
            curCountDownTask.destroyTask();
            Log.i(TAG, "destroyTask[" + curCountDownTaskType + "]: by force");
            curCountDownTask.cancelTask();
        }
        if (!TextUtils.isEmpty(curCountDownTaskType)) {
            Log.i(TAG, "curCountDownTaskType is not finish,so empty it");
            if (TextUtils.equals(curCountDownTaskType, Constant.SELF_SPEECH)) {
                endDangerTime();
            }
            curCountDownTaskType = EMPTY_TASK_TYPE;
        }
    }

    public void addCountDownTask(String taskType, int startSeconds) {
        addCountDownTask(taskType, startSeconds, null);
    }

    public void addCountDownTask(String taskType, int startSeconds, ActionListener actionListener) {
        Log.i(TAG, "coming a Task type[" + taskType + "],dur[" + startSeconds + "]");
        if (targetTextView == null) {
            Log.i(TAG, "targetTextView is null,so can't startTask ");
            return;
        }
        if (startSeconds == 0 || TextUtils.isEmpty(taskType)) {
            Log.i(TAG, "start Seconds is 0 or taskType is empty,so I clear the targetView");
            clearTargetShow();
            clearCurTaskByForce();
            return;
        }
        clearCurTaskByForce();
        curCountDownTaskType = taskType;

        /**
         * 计算危险事件临界值
         */
        dangerTime = startSeconds - Constant.ACTION_CHECK_TIME + Constant.DANGEROUS_TIME;
        /**
         * make sure curTaskType & curTask is Empty
         */
        curCountDownTask = new CountDownTask(startSeconds, new CountDownTaskListener() {
            @Override
            public void onTick(int seconds) {
                /**
                 * 如果是自己发言
                 */
                if (TextUtils.equals(taskType, Constant.SELF_SPEECH) && seconds == dangerTime) {
                    if (!ActionsDetector.contains(ActionsDetector.ACTION_SPEECH_CHECK)) {
                        nodanger();
                    } else {
                        danger();
                    }
                }
                execShow(seconds);
                if (TextUtils.equals(taskType, Constant.SPEECH) || TextUtils.equals(taskType, Constant.SELF_SPEECH)) {
                    if (seconds == Constant.DANGEROUS_TIME) {
                        /**
                         * reach the DANGEROUS_TIME
                         */
                        danger();
                    }
                }
            }

            @Override
            public void onStart(int startSeconds) {
                Log.i(TAG, "CountTask[" + curCountDownTaskType + "] is start now");
                nodanger();
                execShow(startSeconds);
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "CountTask[" + curCountDownTaskType + "] is finish now");
                clearTargetShow();
            }
        });
        curCountDownTask.setActionListener(actionListener);
        curCountDownTask.startTask();
    }

    private void danger() {
        if (targetTextView != null && !hasDanger) {
            ViewTools.setTextViewSize(targetTextView, mTextDefaultSize);
            startDangerTime();
            hasDanger = true;
        }
    }

    private void nodanger() {
        if (targetTextView != null && hasDanger) {
            ViewTools.setTextViewSize(targetTextView, mTextDefaultSize);
            endDangerTime();
            hasDanger = false;
        }
    }

    public static class CountDownTask extends CountDownTimer {
        private static final int SECONDS = 1000;
        private int startSeconds = 0;
        private CountDownTaskListener listener;
        private ActionListener mActionListener;
        private boolean isDestroy = false;
        private boolean needSeconds = false;

        public CountDownTask(int startSeconds, CountDownTaskListener listener) {
            this(startSeconds * SECONDS, SECONDS, startSeconds, listener);
            needSeconds = true;
        }

        public CountDownTask(long millisInFuture, long countDownInterval, int startSeconds, CountDownTaskListener listener) {
            super(millisInFuture, countDownInterval);
            this.startSeconds = startSeconds;
            this.listener = listener;
            this.needSeconds = false;
        }

        public void setActionListener(ActionListener actionListener) {
            mActionListener = actionListener;
        }

        public void startTask() {
            start();
            if (listener != null) {
                listener.onStart(this.startSeconds);
            }
            if (mActionListener != null) {
                mActionListener.onStart();
            }
        }

        public void cancelTask() {
            cancel();
            destroyTask();
        }

        public void destroyTask() {
            if (isDestroy) {
                return;
            }
            isDestroy = true;
            this.listener = null;
            this.mActionListener = null;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (!isDestroy) {
                if (needSeconds) {
                    int seconds = (int) (millisUntilFinished / SECONDS);
                    if (this.listener != null) {
                        this.listener.onTick(seconds);
                    }
                } else {
                    if (this.listener != null) {
                        this.listener.onTick((int) millisUntilFinished);
                    }
                }
            }
        }

        @Override
        public void onFinish() {
            if (isDestroy)
                return;
            if (this.listener != null) {
                this.listener.onFinish();
            }
            if (mActionListener != null) {
                mActionListener.onComplete();
            }
        }
    }

    public static interface CountDownWhenDangerTime {
        void onDangerTime(int seconds);
    }

    public static interface CountDownTaskListener {

        void onTick(int seconds);

        void onStart(int startSeconds);

        void onFinish();

    }
}
