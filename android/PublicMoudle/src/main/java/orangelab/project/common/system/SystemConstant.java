package orangelab.project.common.system;

/**
 * game_werewolf
 * 2017/8/4 上午11:58
 * Mystery
 */

public class SystemConstant {

    public static final String MSG_TYPE = "MSG_TYPE";
    public static final String START_TIME = "START_TIME";
    public static final String END_TIME = "END_TIME";

    public static final String MSG_TYPE_NOPLAY = "noplay";
    public static final String MSG_TYPE_WARNING = "warning";
    public static final String MSG_TYPE_NORMAL = "normal";
}
