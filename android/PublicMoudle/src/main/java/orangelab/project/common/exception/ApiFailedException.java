package orangelab.project.common.exception;

/**
 * game_werewolf
 * 2017/10/26 下午4:00
 * Mystery
 */

public class ApiFailedException extends Exception {

    private int code = -1;

    public ApiFailedException(int code, String message) {
        super(message);
    }

    public ApiFailedException(String message) {
        this(-1, message);
    }

    public int getCode() {
        return code;
    }
}
