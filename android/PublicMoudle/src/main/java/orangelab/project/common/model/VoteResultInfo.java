package orangelab.project.common.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author:1727
 * 2017/3/11
 */

public class VoteResultInfo {

    public String type;
    public ArrayList<Integer> positions;
    public Map<Integer, List<Integer>> vote_info;
    public List<DeathInfo.DeathInfoItem> death_info;
    public int duration = 0;
    public boolean need_pk;

    @Override
    public String toString() {
        return "VoteResultInfo{" +
                "type='" + type + '\'' +
                ", positions=" + positions +
                ", vote_info=" + vote_info +
                '}';
    }
}
