package orangelab.project.common.floatwindow;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Gravity;
import android.view.WindowManager;

import com.androidtoolkit.Environment;
import com.androidtoolkit.view.ScreenUtils;
import orangelab.project.common.floatwindow.model.FloatUser;
import orangelab.project.common.floatwindow.view.FloatWindowView;

/**
 * game_werewolf
 * 2017/12/26 上午11:43
 * Mystery
 */

enum FloatWindowInnerManager {

    INSTANCE;

    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mLayoutParams;
    private FloatWindowView mFloatWindowView;
    private boolean hasShow = false;
    private boolean serverHasStart = false;

    public boolean isServerHasStart() {
        return serverHasStart;
    }

    public void setServerHasStart(boolean serverHasStart) {
        this.serverHasStart = serverHasStart;
    }

    private FloatWindowInnerManager() {

        Context context = Environment.GetAppContext();

        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        initListener();
    }

    private void initListener() {

    }

    private void launchFloatWindowInner(Context context, FloatUser user) {
        mLayoutParams = new WindowManager.LayoutParams();

        mFloatWindowView = new FloatWindowView(context);
        mFloatWindowView.setFloatUser(user);

        if (Build.VERSION.SDK_INT >= 24) { /*android7.0不能用TYPE_TOAST*/
            mLayoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        } else { /*以下代码块使得android6.0之后的用户不必再去手动开启悬浮窗权限*/
            String packname = context.getPackageName();
            PackageManager pm = context.getPackageManager();
            boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.SYSTEM_ALERT_WINDOW", packname));
            if (permission) {
                mLayoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
            } else {
                mLayoutParams.type = WindowManager.LayoutParams.TYPE_TOAST;
            }
        }

        mLayoutParams.format = PixelFormat.RGBA_8888;

        mLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        mLayoutParams.gravity = Gravity.START | Gravity.TOP;

        int width = ScreenUtils.dip2px(85);

        mLayoutParams.x = ScreenUtils.getCurrentScreenWidth() - width;
        mLayoutParams.y = ScreenUtils.getCurrentScreenHeight() - 3 * width;

        mLayoutParams.width = width;
        mLayoutParams.height = width;

        mFloatWindowView.setParams(mLayoutParams);

        mWindowManager.addView(mFloatWindowView, mLayoutParams);

        hasShow = true;
    }

    private void showInner() {
        if (!hasShow && mFloatWindowView != null) {
            mWindowManager.addView(mFloatWindowView, mFloatWindowView.getWmParams());
            hasShow = true;
            mFloatWindowView.startWave();
        }
    }

    private void hideInner() {
        if (hasShow && mFloatWindowView != null) {
            mWindowManager.removeViewImmediate(mFloatWindowView);
            hasShow = false;
            mFloatWindowView.stopWave();
        }

    }

    private void destroyInner() {
        boolean isAttach = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && mFloatWindowView != null) {
            isAttach = mFloatWindowView.isAttachedToWindow();
        }
        if (hasShow && isAttach && mWindowManager != null) {
            mFloatWindowView.destroy();
            mWindowManager.removeView(mFloatWindowView);
        }
        if (mFloatWindowView != null) {
            mFloatWindowView.destroy();
            mFloatWindowView = null;
        }
    }


    public static void LaunchFloatWindow(Context context, FloatUser user) {
        INSTANCE.launchFloatWindowInner(context, user);
    }

    public static void Show() {
        INSTANCE.showInner();
    }

    public static void Hide() {
        INSTANCE.hideInner();
    }

    public static void Destroy() {
        INSTANCE.destroyInner();
    }

}
