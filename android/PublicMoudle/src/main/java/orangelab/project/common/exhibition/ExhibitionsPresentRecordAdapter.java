package orangelab.project.common.exhibition;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.view.UserHeadView;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/7/3 下午4:29
 * Mystery
 */

public class ExhibitionsPresentRecordAdapter extends RecyclerView.Adapter<ExhibitionsPresentRecordAdapter.ViewHolder> {

    public final static int TYPE_NORMAL = 1;
    public final static int TYPE_FOOTER = 2;

    private Context mContext;
    private List<ExhibitionsPresentRecordItem> mRecordItems;

    public ExhibitionsPresentRecordAdapter(Context context) {
        mContext = context;
    }

    public void updateRecord(List<ExhibitionsPresentRecordItem> recordItems) {
        if (recordItems == null) {
            recordItems = new ArrayList<>();
        }
        this.mRecordItems = recordItems;
        notifyDataSetChanged();
    }

    @Override
    public ExhibitionsPresentRecordAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(
                LayoutInflater.from(mContext).inflate(R.layout.layout_exhibitions_records_item,
                        parent,
                        false));
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(ExhibitionsPresentRecordAdapter.ViewHolder holder, int position) {
        ExhibitionsPresentRecordItem item = mRecordItems.get(position);
        holder.mUserHeadView.setUserHeadImageUrl(item.getFromImageUrl());
        holder.mExplain.setText(item.getExplain());

        holder.mGiftName.setText(EffectsManager.GetSupportedTypeName(item.getGiftType()));
        EffectsManager.FillGiftImageView(item.getGiftType(), holder.mGiftBg);

        holder.mTime.setText(item.getTime());
        holder.mUserHeadView.setOnClickListener(v -> {
            PersonalInfoActivity.Launch(mContext, item.getFromId());
        });
    }

    @Override
    public int getItemCount() {
        return mRecordItems.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public UserHeadView mUserHeadView;
        public TextView mExplain;
        public TextView mTime;
        public ImageView mGiftBg;
        public TextView mGiftName;

        public ViewHolder(View itemView) {
            super(itemView);
            mUserHeadView = (UserHeadView) itemView.findViewById(R.id.records_item_from_iv);
            mExplain = (TextView) itemView.findViewById(R.id.records_item_from_tx);
            mGiftBg = (ImageView) itemView.findViewById(R.id.records_item_iv);
            mTime = (TextView) itemView.findViewById(R.id.records_item_time);
            mGiftName = (TextView) itemView.findViewById(R.id.records_item_name);
        }
    }
}
