package orangelab.project.common.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/12/3 下午9:43
 * Mystery
 */

public class CheckResult implements Keepable {

    public String role;
    public int position;
    public int duration;

}
