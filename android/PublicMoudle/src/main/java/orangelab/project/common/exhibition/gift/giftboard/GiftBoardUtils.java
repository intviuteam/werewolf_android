package orangelab.project.common.exhibition.gift.giftboard;

import com.R;

/**
 * game_werewolf
 * 2017/11/9 下午5:22
 * Mystery
 */

public class GiftBoardUtils {

    public static int GetToPositionImage(int position) {
        switch (position) {
            case 1:
                return R.mipmap.gift_count_to_1;
            case 2:
                return R.mipmap.gift_count_to_2;
            case 3:
                return R.mipmap.gift_count_to_3;
            case 4:
                return R.mipmap.gift_count_to_4;
            case 5:
                return R.mipmap.gift_count_to_5;
            case 6:
                return R.mipmap.gift_count_to_6;
            case 7:
                return R.mipmap.gift_count_to_7;
            case 8:
                return R.mipmap.gift_count_to_8;
            case 9:
                return R.mipmap.gift_count_to_9;
            case 10:
                return R.mipmap.gift_count_to_10;
            case 11:
                return R.mipmap.gift_count_to_11;
            case 12:
                return R.mipmap.gift_count_to_12;
            default:
                return R.mipmap.gift_count_to_1;
        }
    }

    public static int GetFromPositionImage(int position) {
        switch (position) {
            case 1:
                return R.mipmap.gift_count_from_1;
            case 2:
                return R.mipmap.gift_count_from_2;
            case 3:
                return R.mipmap.gift_count_from_3;
            case 4:
                return R.mipmap.gift_count_from_4;
            case 5:
                return R.mipmap.gift_count_from_5;
            case 6:
                return R.mipmap.gift_count_from_6;
            case 7:
                return R.mipmap.gift_count_from_7;
            case 8:
                return R.mipmap.gift_count_from_8;
            case 9:
                return R.mipmap.gift_count_from_9;
            case 10:
                return R.mipmap.gift_count_from_10;
            case 11:
                return R.mipmap.gift_count_from_11;
            case 12:
                return R.mipmap.gift_count_from_12;
            default:
                return R.mipmap.gift_count_from_1;
        }
    }
}
