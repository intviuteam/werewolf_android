package orangelab.project.common.exhibition.gift;

import com.R;
import orangelab.project.common.exhibition.ExhibitionsUtils;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/7/3 上午11:37
 * Mystery
 */

public class ExhibitionsRecordItem {

    private static final int MAX_NUMBER = 999;

    private String mType;// 类型
    private int mNumber;//数量

    public ExhibitionsRecordItem(String type) {
        mType = type;
        init();
    }

    public ExhibitionsRecordItem(String type, int number) {
        mType = type;
        mNumber = number;
        init();
    }

    private void init() {
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getNumber() {
        if (mNumber < MAX_NUMBER) {
            return MessageUtils.getString(R.string.gifts_record_item_number_string, Integer.toString(mNumber));
        } else {
            return MessageUtils.getString(R.string.gifts_record_item_number_max_string);
        }
    }

    public void setNumber(int number) {
        mNumber = number;
    }
}
