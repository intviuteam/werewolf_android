package orangelab.project.common.model;


import com.toolkit.action.Keepable;

import org.json.JSONObject;

/**
 * Created by lloydfinch on 02/01/2018.
 * <p>
 * 二手消息，用于语音房先进行过数据处理再发送的对应消息类型
 */

public class ServerMessageEventOver implements Keepable {

    public static final String TYPE = "type";
    public static final String CODE = "code";
    public static final String PAYLOAD = "payload";
    public static final String MSG_ID = "msg_id";
    public static final String MSG_TIME = "msg_time";
    public static final String RESEND = "resend";
    public static final String REQUEST_ID = "request_id";

    public int code;
    public String type;
    public long msg_id = -1;
    public long msg_time = -1;
    public JSONObject payload;
    public boolean resend = false;
    public String message = "";//20180122添加了message，用于提示用户


    public ServerMessageEventOver(ServerMessageEvent serverMessageEvent) {
        code = serverMessageEvent.code;
        type = serverMessageEvent.type;
        payload = serverMessageEvent.payload;
        message = serverMessageEvent.message;
        if (payload != null) {
            msg_id = payload.optLong(MSG_ID);
        }
    }

    @Override
    public String toString() {
        return "ServerMessageEventOver{" +
                "type='" + type + '\'' +
                ", msg_id=" + msg_id +
                ", msg_time=" + msg_time +
                ", payload=" + payload +
                ", resend=" + resend +
                '}';
    }
}
