package orangelab.project.common.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Toast;

import com.androidtoolkit.PLog;
import com.androidtoolkit.ToastToolKit;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.RNActivityManager;
import orangelab.project.common.UserBehaviorObserver;
import orangelab.project.common.db.MiniGameLaunchRecordDaoHelper;
import orangelab.project.common.floatwindow.FloatWindowCommander;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.exception.ApiFailedException;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.minigame.MiniGameActivity;
import orangelab.project.common.dialog.EnterLoadingDialog;
import orangelab.project.game.dialog.SystemMessageDialog;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.ServerGet;
import orangelab.project.common.system.SystemController;
import orangelab.project.common.system.SystemMessage;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.dialog.RoomPasswordDialog;
import orangelab.project.voice.dialog.VoiceTurnRoomDialog;
import orangelab.project.voice.manager.VoiceTopicManager;
import orangelab.project.voice.privateroom.PrivateRoomContext;
import orangelab.project.voice.manager.VoiceRoomCommander;

import com.networktoolkit.transport.Constant;

import cn.intviu.support.NetworkHelpers;

import com.datasource.GlobalUserState;

import java.util.ArrayList;

/**
 * 启动页面，终究目的是启动游戏页面
 */

public class LaunchActivity extends SafeActivity {

    private static boolean LAUNCH_HAS_EXIST = false;

    private static final String TAG = "LaunchActivity";

    private EnterLoadingDialog mEnterLoadingDialog;
    private RoomPasswordDialog mRoomPasswordDialog;

    private boolean isEntering = false;
    private boolean mDestroy = false;

    private static boolean IsLaunchExist() {
        return LAUNCH_HAS_EXIST;
    }

    private static boolean IsCanLaunch(boolean needGamingJudge) {
        if (IsLaunchExist()) {
            return false;
        }
        if (needGamingJudge) {
            if (GlobalUserState.getGlobalState().isGaming()) {
                ToastToolKit.showShort(MessageUtils.getString(R.string.mini_game_enter_error));
                return false;
            }
        }
        return true;
    }

    public static void LaunchForRoom(Context context, String type, EnterRoomPayload enterRoomPayload) {
        if (!IsCanLaunch(false)) {
            return;
        }
        Intent intent = new Intent(context, LaunchActivity.class);
        IntentDataHelper.setIntentType(intent, type);
        IntentDataHelper.setEnterRoomPayload(intent, enterRoomPayload);
        context.startActivity(intent);
    }

    public static void LaunchForRoom(Context context, String fromType, String roomId, String roomType) {
        LaunchForRoom(context, fromType, roomId, roomType, "");
    }

    public static void LaunchForRoom(Context context, String fromType, String roomId, String roomType, String roomPassword) {
        if (!IsCanLaunch(true)) {
            return;
        }
        EnterRoomPayload enterRoomPayload = EnterRoomPayload.CreateSimplePayload(roomId, roomType, roomPassword);
        Intent intent = new Intent(context, LaunchActivity.class);
        IntentDataHelper.setIntentType(intent, fromType);
        IntentDataHelper.setEnterRoomPayload(intent, enterRoomPayload);
        context.startActivity(intent);
    }

    public static void LaunchForAmusement(Context context, String roomId, String type) {
        if (!IsCanLaunch(true)) {
            return;
        }
        Intent intent = new Intent(context, LaunchActivity.class);
        IntentDataHelper.setIntentType(intent, IntentDataHelper.TYPE_EVENT_AMUSEMENT_ACTIVITY);
        IntentDataHelper.setRoomId(intent, roomId);
        IntentDataHelper.setGameType(intent, type);
        context.startActivity(intent);
    }

    public static void LaunchForAmusementRandom(Context context, String type) {
        LaunchForAmusement(context, "", type);
    }

    public static void LaunchForConsoleMiniGame(Context context, String gameType) {
        if (!IsCanLaunch(true)) {
            return;
        }
        Intent intent = new Intent(context, LaunchActivity.class);
        IntentDataHelper.setIntentType(intent, IntentDataHelper.TYPE_EVENT_AMUSEMENT_ACTIVITY);
        IntentDataHelper.setGameType(intent, gameType);
        IntentDataHelper.setType(intent, MiniGameLaunch.CONSOLE_GAME);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    private LaunchObserver mLaunchObserver = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LAUNCH_HAS_EXIST) {
            return;
        } else {
            LAUNCH_HAS_EXIST = true;
        }


        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        mEnterLoadingDialog = new EnterLoadingDialog(this);

        final String oldType = GlobalUserState.getGlobalState().getCurGameType();

        mLaunchObserver = new LaunchObserver() {

            @Override
            public void onStart() {
                GlobalUserState.getGlobalState().setActionBlock(true);
            }

            @Override
            public void onServerGetStart() {

            }

            @Override
            public void onServerGetDone(String roomId, String roomType) {
                PLog.i(TAG, "old type is " + oldType);
                PLog.i(TAG, "new type is " + roomType);
                /**
                 * 临时设定为新的Type
                 */
                GlobalUserState.getGlobalState().setCurGameType(roomType);

            }

            @Override
            public void onSocketEnter() {

            }

            @Override
            public void onInterrupt() {
                dismissLoading();
                finish();
                GlobalUserState.getGlobalState().setGaming(false);
                GlobalUserState.getGlobalState().setCurGameType(oldType);
                GlobalUserState.getGlobalState().setActionBlock(false);
            }

            @Override
            public void onComplete() {
                /**
                 * 进入成功
                 */
                dismissLoading();
                finish();
                GlobalUserState.getGlobalState().setActionBlock(false);
            }

            @Override
            public void onError(String msg) {
                dismissLoading();
                showError(msg);
                GlobalUserState.getGlobalState().setGaming(false);
                GlobalUserState.getGlobalState().setCurGameType(oldType);
                GlobalUserState.getGlobalState().setActionBlock(false);
            }
        };

        launchStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (LAUNCH_HAS_EXIST) {
            return;
        } else {
            LAUNCH_HAS_EXIST = true;
        }
        launchStart();
    }

    private void launchStart() {

        if (mLaunchObserver != null) {
            mLaunchObserver.onStart();
        }

        FloatWindowCommander.HideFloatWindow();
        /**
         * 这里先查看是不是在禁玩时间范围内
         */
        SystemMessage noPlayMessage = SystemController.isInNoPlayTime(GlobalUserState.getGlobalState().getUserId(), System.currentTimeMillis());
        if (noPlayMessage != null) {
            /**
             * 被禁玩了
             */
            showNoPlay(noPlayMessage);
        } else {
            performTurnToOtherActivity();
        }
    }

    private void showNoPlay(SystemMessage noPlayMessage) {
        SystemMessageDialog dialog = new SystemMessageDialog(this, () -> {
            if (mLaunchObserver != null) {
                mLaunchObserver.onInterrupt();
            }
        });
        dialog.setTitle(noPlayMessage.getTitle());
        dialog.setContent(noPlayMessage.getContent());
        dialog.show();
    }

    private void performTurnToOtherActivity() {
        turnToOtherActivity();
//        if (NetworkHelpers.isNetworkAvailable(this, true, true)) {
//
//        } else {
//            if (mLaunchObserver != null) {
//                mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
//            }
//        }
    }

    private void turnToRnActivity() {
        try {
            getIntent().setComponent(new ComponentName(this, RNActivityManager.INSTANCE.GetRNActivity().getClass()));
            getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(getIntent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mLaunchObserver != null) {
            mLaunchObserver.onComplete();
        }
    }

    private void showLoading() {
        if (mEnterLoadingDialog != null && !mEnterLoadingDialog.isShowing() && !mDestroy) {
            mEnterLoadingDialog.show();
        }
    }

    private void showPasswordDialog() {
        if (mRoomPasswordDialog != null && !mRoomPasswordDialog.isShowing() && !mDestroy) {
            mRoomPasswordDialog.show();
        }
    }

    private void dismissPasswordDialog() {
        if (mRoomPasswordDialog != null && mRoomPasswordDialog.isShowing() && !mDestroy) {
            mRoomPasswordDialog.dismiss();
        }
    }

    private void dismissLoading() {
        if (mEnterLoadingDialog != null && mEnterLoadingDialog.isShowing() && !mDestroy) {
            mEnterLoadingDialog.dismiss();
        }
    }

    private void showError(String msg) {
        showError(msg, true);
    }

    private void showError(String msg, boolean needshow) {
        if (needshow && !mDestroy) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    private void turnToOtherActivity() {
        if (FloatWindowCommander.IsFloatWindowOpen()) {
            /**
             * 如果意图进入相同的房间，则直接进入房间
             */
            try {
                EnterRoomPayload payload = IntentDataHelper.getEnterRoomPayload(getIntent());

                try {
                    PrivateRoomContext context = VoiceRoomCommander.ContextManager.INSTANCE.GetPrivateRoomContext();
                    if (context != null) {
                        /**
                         * 当前是私密房小窗模式
                         */
                        if (!TextUtils.isEmpty(context.getRoomId()) && TextUtils.equals(payload.roomId, context.getRoomId())) {
                            FloatWindowCommander.ReEnterRoom();

                            if (mLaunchObserver != null) {
                                mLaunchObserver.onComplete();
                            }
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /**
                 * 处理语音房的进入问题
                 */
                if (!TextUtils.isEmpty(VoiceRoomConfig.getRoomId()) && TextUtils.equals(payload.roomId, VoiceRoomConfig.getRoomId())) {
                    FloatWindowCommander.ReEnterRoom();
                    if (mLaunchObserver != null) {
                        mLaunchObserver.onComplete();
                    }
                    return;
                }

                VoiceTurnRoomDialog roomDialog = new VoiceTurnRoomDialog(this);
                roomDialog.setWhenTurn(() -> {
                    turnToOtherActivityInner();
                });
                roomDialog.setParentDestroy(() -> {
                    if (mLaunchObserver != null) {
                        mLaunchObserver.onInterrupt();
                    }
                });
                roomDialog.show();

            } catch (Exception e) {
                if (mLaunchObserver != null) {
                    mLaunchObserver.onInterrupt();
                }
                return;
            }
        } else {
            FloatWindowCommander.StopService(this, null);
            turnToOtherActivityInner();
        }
    }

    private void turnToOtherActivityInner() {
        String type = IntentDataHelper.getIntentType(getIntent());
        if (TextUtils.equals(type, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN)) {
            enterRoomFromIntent();
        } else if (TextUtils.equals(type, IntentDataHelper.TYPE_EVENT_ROOM_FROM_CHAT)) {
            enterRoomFromIntent();
        } else if (TextUtils.equals(type, IntentDataHelper.TYPE_NOTIFACTION_FROM_INTVIU)) {
            fromNotificationIntviu();
        } else if (TextUtils.equals(type, IntentDataHelper.TYPE_EVENT_AMUSEMENT_ACTIVITY)) {
            forMiniGame();
        } else {
            if (mLaunchObserver != null) {
                mLaunchObserver.onInterrupt();
            }
        }
    }

    private void enterRoom(EnterRoomPayload payload) {
        //获取语音房类型列表
        if (payload != null && TextUtils.equals(payload.gameType, Constant.GAME_TYPE_AUDIO)) {
            VoiceTopicManager.getInstance().getTopicList(payload.roomId);
        }
        enterRoomInner(payload);
    }

    private void enterRoomInner(EnterRoomPayload payload) {
        if (mLaunchObserver != null) {
            mLaunchObserver.onSocketEnter();
        }
        MainApplication.getInstance().getSocketEngineHelper()
                .enterRoom(
                        LaunchActivity.this,
                        payload,
                        (args) -> {
                            runOnUiThreadSafely(() -> {
                                isEntering = false;
                                if (mLaunchObserver != null) {
                                    mLaunchObserver.onComplete();
                                }
                            });
                        },
                        (args) -> {
                            runOnUiThreadSafely(() -> {
                                isEntering = false;
                                if (args != null && args.length > 0) {
                                    if (mLaunchObserver != null) {
                                        mLaunchObserver.onError(args[0].toString());
                                    }
                                    return;
                                } else {
                                    if (mLaunchObserver != null) {
                                        mLaunchObserver.onInterrupt();
                                    }
                                }
                            });
                        }
                );
    }

    private void destroy() {
        if (!mDestroy) {
            dismissLoading();
            FloatWindowCommander.ShowFloatWindow();
            mDestroy = true;
            LAUNCH_HAS_EXIST = false;
        }
    }

    @Override
    public void finish() {
        super.finish();
        destroy();
        if (mLaunchObserver != null) {
            mLaunchObserver = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroy();
    }

    private void forMiniGame() {
        if (GlobalUserState.getGlobalState().isGaming()) {
            if (mLaunchObserver != null) {
                mLaunchObserver.onError(MessageUtils.getString(R.string.str_you_has_in_room));
            }
            return;
        }
        if (!isEntering) {
            isEntering = true;
            showLoading();
            String roomId = IntentDataHelper.getRoomID(getIntent());
            String gameType = IntentDataHelper.getGameType(getIntent());

            String type = IntentDataHelper.getType(getIntent());

            if (TextUtils.isEmpty(type)) {
                /**
                 * 联机进入
                 */
                if (TextUtils.isEmpty(roomId)) {
                    /**
                     * 说明是随机进入
                     */
                    serverGetForOnlineMiniGame("", gameType);
                } else {
                    /**
                     * 说明是指定房间号进入
                     */
                    serverGetForOnlineMiniGame(roomId, gameType);
                }
            } else {
                /**
                 * 单机进入
                 */
                serverGetConsoleMiniGame(gameType);
            }
        } else {
            if (mLaunchObserver != null) {
                mLaunchObserver.onInterrupt();
            }
        }
    }

    private void fromNotificationIntviu() {
        /**
         * 如果应用未开启
         */
        if (GlobalUserState.getGlobalState().isGaming()) {
            if (mLaunchObserver != null) {
                mLaunchObserver.onError(MessageUtils.getString(R.string.str_you_has_in_room));
            }
            return;
        } else {
            if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken())) {
                /**
                 * 应用没有开启，进入RnActivity
                 */
                turnToRnActivity();

            } else {
                /**
                 * 应用已经开启
                 */
                String roomId = IntentDataHelper.getRoomID(getIntent());
                String roomPassword = IntentDataHelper.getRoomPassword(getIntent());
                getSocketUrl(EnterRoomPayload.CreateSimplePayload(roomId, "", roomPassword));
            }
        }
    }

    private void enterRoomFromIntent() {
        EnterRoomPayload payload = IntentDataHelper.getEnterRoomPayload(getIntent());
        if (GlobalUserState.getGlobalState().isGaming()) {
            if (mLaunchObserver != null) {
                mLaunchObserver.onError(MessageUtils.getString(R.string.str_you_has_in_room));
            }
            return;
        }
        if (payload != null && !isEntering) {
            isEntering = true;

            showLoading();

            getSocketUrl(payload);
        } else {
            if (mLaunchObserver != null) {
                mLaunchObserver.onInterrupt();
            }
        }
    }

    private void serverGetConsoleMiniGame(String gameType) {
        /**
         * 这里执行单机的 MiniGameLaunch的获取
         *
         * Step1.判断是否网络正常 ，正常则使用server/get进行获取数据，不正常向数据库进行查询
         *
         * Step2.使用Launch数据进行进入小游戏
         */
        if (NetworkHelpers.isNetworkAvailable(this, true, true)) {
            ApiManager.ServerGet(gameType, "", GlobalUserState.getGlobalState().getUserId(), "", ((serverGet, e) -> {
                runOnUiThreadSafely(() -> {
                    if (e == null) {
                        ServerGet.ServerGetMiniGame miniGame = serverGet.mini_game;

                        if (miniGame == null) {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
                            }
                            return;
                        }

                        /**
                         * 存入数据库
                         */
                        try {
                            MiniGameLaunchRecordDaoHelper helper = MainApplication.getInstance().getMiniGameLaunchRecordDaoHelper();
                            if (helper != null) {
                                helper.insertOrUpdateLaunch(GlobalUserState.getGlobalState().getUserId(), gameType, GsonHelper.getGson().toJson(miniGame));
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }

                        /**
                         * 使用得到的ServerGet进入数据
                         */
                        MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder()
                                .type(serverGet.type)
                                .server("")
                                .roomId("")
                                .launchMode(MiniGameLaunch.CONSOLE_GAME)
                                .gameName(miniGame.name)
                                .gameIcon(miniGame.icon)
                                .gameUrl(miniGame.url)
                                .gameConsoleUrl(miniGame.single_url)
                                .gameDownloadUrl(miniGame.download)
                                .landscape(miniGame.landscape)
                                .extraData("")
                                .password("")
                                .user_images(new ArrayList<>())
                                .tips(miniGame.tips);
                        MiniGameLaunch launch = builder.build();

                        if (!UserBehaviorObserver.enterMiniGameIntentInterrupt(this, launch)) {
                            MiniGameActivity.Launch(LaunchActivity.this, launch);
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onComplete();
                            }
                        } else {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onInterrupt();
                            }
                        }
                    } else {

                        if (e instanceof ApiFailedException) {
                            if (((ApiFailedException) e).getCode() == 1009) {
                                SystemMessage systemMessage = new SystemMessage(MessageUtils.getString(R.string.system_hint_dialog_title_noplay), e.getMessage());
                                showNoPlay(systemMessage);
                            } else {
                                /**
                                 * 因为网络问题，或者其他问题导致进入单机游戏失败的情况，直接去数据查询上次server／get数据
                                 */
                                serverGetConsoleMiniGameFromDB(gameType);
                            }
                        } else {
                            /**
                             * 因为网络问题，或者其他问题导致进入单机游戏失败的情况，直接去数据查询上次server／get数据
                             */
                            serverGetConsoleMiniGameFromDB(gameType);
                        }

                    }
                });
            }));
        } else {
            /**
             * 因为网络问题，或者其他问题导致进入单机游戏失败的情况，直接去数据查询上次server／get数据
             */
            serverGetConsoleMiniGameFromDB(gameType);
        }
    }

    private void serverGetConsoleMiniGameFromDB(String gameType) {
        try {
            MiniGameLaunchRecordDaoHelper helper = MainApplication.getInstance().getMiniGameLaunchRecordDaoHelper();
            if (helper != null) {
                String json = helper.getLaunch(GlobalUserState.getGlobalState().getUserId(), gameType);
                if (!TextUtils.isEmpty(json)) {
                    ServerGet.ServerGetMiniGame miniGame = GsonHelper.getGson().fromJson(json, ServerGet.ServerGetMiniGame.class);
                    MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder()
                            .type(gameType)
                            .server("")
                            .roomId("")
                            .launchMode(MiniGameLaunch.CONSOLE_GAME)
                            .gameName(miniGame.name)
                            .gameIcon(miniGame.icon)
                            .gameUrl(miniGame.url)
                            .gameConsoleUrl(miniGame.single_url)
                            .gameDownloadUrl(miniGame.download)
                            .landscape(miniGame.landscape)
                            .extraData("")
                            .password("")
                            .user_images(new ArrayList<>())
                            .tips(miniGame.tips);
                    MiniGameLaunch launch = builder.build();
                    if (!UserBehaviorObserver.enterMiniGameIntentInterrupt(this, launch)) {
                        MiniGameActivity.Launch(LaunchActivity.this, launch);
                        if (mLaunchObserver != null) {
                            mLaunchObserver.onComplete();
                        }
                    } else {
                        if (mLaunchObserver != null) {
                            mLaunchObserver.onInterrupt();
                        }
                    }
                } else {
                    if (mLaunchObserver != null) {
                        mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
                    }
                }
            } else {
                if (mLaunchObserver != null) {
                    mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            if (mLaunchObserver != null) {
                mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
            }
        }
    }

    private void serverGetForOnlineMiniGame(String roomId, String gameType) {
        ApiManager.ServerGet(gameType, roomId, GlobalUserState.getGlobalState().getUserId(), GlobalUserState.getGlobalState().getUserLocation(), (serverGet, e) ->
                runOnUiThreadSafely(() -> {
                    if (e == null) {
                        ServerGet.ServerGetMiniGame miniGame = serverGet.mini_game;
                        MiniGameLaunch.Builder builder = new MiniGameLaunch.Builder()
                                .type(serverGet.type)
                                .server(serverGet.server)
                                .roomId(serverGet.room_id)
                                .gameName(miniGame.name)
                                .gameIcon(miniGame.icon)
                                .gameUrl(miniGame.url)
                                .gameConsoleUrl(miniGame.single_url)
                                .gameDownloadUrl(miniGame.download)
                                .landscape(miniGame.landscape)
                                .extraData(serverGet.extraData)
                                .password(serverGet.password)
                                .user_images(serverGet.user_images)
                                .tips(miniGame.tips);
                        MiniGameLaunch launch = builder.build();
                        if (!UserBehaviorObserver.enterMiniGameIntentInterrupt(this, launch)) {
                            MiniGameActivity.Launch(LaunchActivity.this, launch);
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onComplete();
                            }
                        } else {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onInterrupt();
                            }
                        }
                    } else {
                        if (e instanceof ApiFailedException) {
                            if (((ApiFailedException) e).getCode() == 1009) {
                                SystemMessage systemMessage = new SystemMessage(MessageUtils.getString(R.string.system_hint_dialog_title_noplay), e.getMessage());
                                showNoPlay(systemMessage);
                            } else {
                                if (mLaunchObserver != null) {
                                    mLaunchObserver.onError(e.getMessage());
                                }
                            }
                        } else {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
                            }
                        }
                    }
                }));
    }

    private void getSocketUrl(final EnterRoomPayload payload) {
        if (mLaunchObserver != null) {
            mLaunchObserver.onServerGetStart();
        }
        ApiManager.ServerGet(payload.gameType, payload.roomId, GlobalUserState.getGlobalState().getUserId(), GlobalUserState.getGlobalState().getUserLocation(), (serverGet, e) ->
                runOnUiThreadSafely(() -> {
                    if (e == null) {
                        try {
                            payload.gameType = serverGet.type;
                            payload.socketUrl = serverGet.server;

                            if (mLaunchObserver != null) {
                                mLaunchObserver.onServerGetDone(serverGet.room_id, serverGet.type);
                            }

                            if (serverGet.password_needed && !TextUtils.isEmpty(payload.password)) {
                                /**
                                 * 如果密码不为空并且需要密码则直接进入
                                 */
                                enterRoom(payload);
                                return;
                            }

                            if (serverGet.password_needed && TextUtils.isEmpty(payload.password)) {
                                /**
                                 * 如果密码为空并且需要密码则弹出密码输入框
                                 */
                                mRoomPasswordDialog = new RoomPasswordDialog(MessageUtils.getString(R.string.werewolf_enter_password, payload.roomId), this, "", value -> {
                                    /**
                                     * 密码输入完了
                                     */
                                    dismissPasswordDialog();
                                    payload.password = value;
                                    showLoading();
                                    enterRoom(payload);

                                }, value -> {
                                    if (mLaunchObserver != null) {
                                        mLaunchObserver.onInterrupt();
                                    }
                                });
                                dismissLoading();
                                showPasswordDialog();
                                return;
                            }

                            /**
                             * 正常进入
                             */
                            enterRoom(payload);

                        } catch (Exception e1) {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onInterrupt();
                            }
                        }
                    } else {
                        dismissLoading();
                        if (e instanceof ApiFailedException) {
                            if (((ApiFailedException) e).getCode() == 1009) {
                                SystemMessage systemMessage = new SystemMessage(MessageUtils.getString(R.string.system_hint_dialog_title_noplay), e.getMessage());
                                showNoPlay(systemMessage);
                            } else {
                                if (mLaunchObserver != null) {
                                    mLaunchObserver.onError(e.getMessage());
                                }
                            }
                        } else {
                            if (mLaunchObserver != null) {
                                mLaunchObserver.onError(MessageUtils.getString(R.string.network_is_unavailable));
                            }
                        }
                    }
                }));
    }


    public interface LaunchObserver {

        void onStart();   //流程开始

        void onServerGetStart();

        void onServerGetDone(String roomId, String roomType);

        void onSocketEnter();

        void onInterrupt();  //流程结束 未成功

        void onComplete();//流程结束 成功

        void onError(String msg);//错误处理
    }
}
