package orangelab.project.common.model;

import com.toolkit.action.Keepable;

import org.json.JSONObject;

/**
 * Created by lloydfinch on 02/01/2018.
 * <p>
 * //二手消息，用于语音房先进行过数据处理再发送的对应消息类型
 */

public class ServerResponseEventOver implements Keepable {
    public static final String TYPE = "type";
    public static final String CODE = "code";
    public static final String PAYLOAD = "payload";
    public static final String MSG_ID = "msg_id";
    public static final String MSG_TIME = "msg_time";
    public static final String RESEND = "resend";
    public static final String REQUEST_ID = "request_id";

    public int code;
    public String request_id;
    public String type;
    public long msg_id = -1;
    public long msg_time = -1;
    public JSONObject payload;
    public boolean resend = false;
    public String message;

    public ServerResponseEventOver(ServerResponseEvent serverResponseEvent) {
        this.code = serverResponseEvent.code;
        this.request_id = serverResponseEvent.request_id;
        this.message = serverResponseEvent.message;
        this.type = serverResponseEvent.type;
        this.payload = serverResponseEvent.payload;
        this.message = serverResponseEvent.message;

        if (payload != null) {
            msg_id = payload.optLong(MSG_ID);
        }
    }
}
