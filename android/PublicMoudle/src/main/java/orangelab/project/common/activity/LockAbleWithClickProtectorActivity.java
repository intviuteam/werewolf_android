package orangelab.project.common.activity;

import android.view.View;

import com.toolkit.ClickProtector;

/**
 * game_werewolf
 * 2017/11/6 上午10:55
 * Mystery
 */

public abstract class LockAbleWithClickProtectorActivity extends LockAbleActivity {


    private ClickProtector mClickProtector = new ClickProtector() {
        @Override
        public void onClickWithProtector(View v) {
            LockAbleWithClickProtectorActivity.this.onClickWithProtector(v);
        }
    };

    {
        mClickProtector.addIntercept(new ClickProtector.ClickTimeIntercept(1500));
    }

    public abstract void onClickWithProtector(View v);

}
