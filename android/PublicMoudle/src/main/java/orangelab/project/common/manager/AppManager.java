package orangelab.project.common.manager;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.ApkToolKit;
import com.androidtoolkit.DeviceToolKit;
import com.androidtoolkit.Environment;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.TimeZoneToolKit;

import orangelab.project.ProjectConfig;
import orangelab.project.common.GlobalService;
import orangelab.project.common.floatwindow.FloatWindowCommander;

import com.BuildConfig;
import com.R;

import com.datasource.GlobalUserState;
import com.networktoolkit.transport.TransportConfig;

import java.util.Map;

import cn.intviu.service.IntviuApplication;
import cn.intviu.support.ConstInfo;

/**
 * game_werewolf
 * 2017/9/15 上午11:48
 * Mystery
 */

public class AppManager {

    private static int sessionDepth = 0;
    private static long lastCallTime = 0;
    private static final int CALL_TIME = 1000 * 60 * 3;

    public static String GetApp() {
        return ProjectConfig.APP_TYPE;
    }

    public static boolean IsWereWolf() {
        return TextUtils.equals(ProjectConfig.APP_TYPE, "werewolf");
    }

    public static boolean GreaterLollipop() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        }
        return false;
    }

    public static int GetAppIcon() {
        return ProjectConfig.APP_ICON;
    }

    public static void RoomCall() {
        lastCallTime = System.currentTimeMillis();
    }

    public static void ClearRoomCall() {
        lastCallTime = lastCallTime - CALL_TIME - 1;
    }

    public static boolean CanCall(Runnable runnable) {
        if (System.currentTimeMillis() - lastCallTime > CALL_TIME) {
            return true;
        } else {
            if (runnable != null) {
                runnable.run();
            }
            return false;
        }

    }

    public static void ActivityOnStart() {
        if (sessionDepth == 0) {
            Log.i("BasicActivity", "ActivityOnStart: resume from background");
            reportResumeApp();
        }
        sessionDepth++;
    }

    public static void ActivityOnStop() {
        if (sessionDepth > 0)
            sessionDepth--;
        if (sessionDepth == 0) {
            Log.i("BasicActivity", "ActivityOnStop onStart: enter background");
            reportLeaveApp();
        }
    }


    public static void reportResumeApp() {
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken()))
            return;
        ThreadToolKit.Async(() -> {
            ApiManager.ResumeAppInCurrThread();
        });
        FloatWindowCommander.ShowFloatWindow();
        GlobalService.appOnTop(Environment.GetAppContext());
    }

    public static void reportLeaveApp() {
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken()))
            return;
        ThreadToolKit.Async(() -> {
            ApiManager.LeaveAppInCurrThread();
        });
        FloatWindowCommander.HideFloatWindow();
        GlobalService.appInBackGround(Environment.GetAppContext());
    }
}
