package orangelab.project.common.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;


import com.R;
import com.androidtoolkit.ToastToolKit;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.upgrade.IUpgradeServiceDefines;
import orangelab.project.common.upgrade.UpgradeService;
import orangelab.project.common.utils.IntentDataHelper;

import cn.intviu.sdk.model.AppUpgradeInfo;
import cn.intviu.support.AppUtil;

import com.androidtoolkit.view.MaterialDialog;

/**
 * Created by Mystery on 2016/11/1.
 */

public class UpgradeActivity extends BasicActivity implements IUpgradeServiceDefines {

    private MaterialDialog mUpdateDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        Intent intent = getIntent();
        AppUpgradeInfo.AppUpgradeDetail detail = (AppUpgradeInfo.AppUpgradeDetail) intent.getSerializableExtra
                (UPDATE_DETAIL);
        if (detail != null) {
            showUpdateDialog(detail);
        }
    }

    private void showUpdateDialog(final AppUpgradeInfo.AppUpgradeDetail detail) {
        if (mUpdateDialog == null) {
            mUpdateDialog = new MaterialDialog(this);
            View view = View.inflate(this, R.layout.dialog_app_update_layout, null);
            TextView versionCode = (TextView) view.findViewById(R.id.version_code);
            TextView versionSize = (TextView) view.findViewById(R.id.app_size);
            TextView versionContent = (TextView) view.findViewById(R.id.version_info);
            versionCode.setText(getString(R.string.new_version, detail.version_name));
            versionContent.setText(detail.description);
            mUpdateDialog.setContentView(view);
            mUpdateDialog.setCanceledOnTouchOutside(false);
            mUpdateDialog.setCancelable(false);
            mUpdateDialog.setPositiveButton(R.string.action_update, v -> {

                String packageName = getPackageName();
                if (TextUtils.isEmpty(packageName)) {
                    mUpdateDialog.dismiss();
                    finish();
                    return;
                }

                if (AppUtil.hasInstallApp(this, "com.android.vending")) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + packageName));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setPackage("com.android.vending");
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ToastToolKit.showShort(R.string.string_market_failed);
                    }
                } else {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + packageName));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } catch (Exception e) {
                        /**
                         * 跳转应用市场失败
                         */
                        ToastToolKit.showShort(R.string.string_market_failed);
                        e.printStackTrace();
                    }
                }
//                    updateApk(detail.url);
//                    finish();
                if (detail.update_level == 1) {
                    mUpdateDialog.dismiss();
                    finish();
                    return;
                }

                if (detail.update_level == 2) {
                    /**
                     * 强制更新不管它
                     */
                    return;
                }

            });
            mUpdateDialog.setNegativeButton(R.string.action_cancel, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mUpdateDialog.dismiss();
                    /**
                     * 此处需要添加根据更新等级来处理用户取消更新问题
                     */
                    if (detail.update_level == 1) {
                        /* 1 等级可以忽略*/
                        finish();
                    } else if (detail.update_level == 2) {
                        /* 2等级为强制更新，一旦用户点击取消则退出程序*/
                        //AppUtil.exitApp();
                        final MaterialDialog noticeDialog = new MaterialDialog(UpgradeActivity.this);
                        noticeDialog.setTitle(R.string.dialog_title_exceed_error);
                        noticeDialog.setMessage(R.string.update_force_exit_text);
                        noticeDialog.setPositiveButton(R.string.title_confirm, v1 -> {
                            noticeDialog.dismiss();
                            AppUtil.exitApp();
                        });
                        noticeDialog.show();
                    }
                }
            });
        }
        mUpdateDialog.show();
    }

    /**
     * 更新apk
     *
     * @param url
     */
    private void updateApk(String url) {
        Intent intent = new Intent(this, UpgradeService.class);
        IntentDataHelper.setUpgradeType(intent, Constant.DOWNLOAD_APK);
        IntentDataHelper.setDownloadApkUrl(intent, url);
        startService(intent);
    }

    public static void checkUpgrade(Context context, AppUpgradeInfo.AppUpgradeDetail detail) {
        Intent intent = new Intent(context, UpgradeActivity.class);
        intent.putExtra(UPDATE_DETAIL, detail);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
