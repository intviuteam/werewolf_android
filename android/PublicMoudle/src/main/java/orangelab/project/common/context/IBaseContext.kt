package orangelab.project.common.context

import android.os.Handler

/**
 * game_werewolf
 * 2018/3/19 下午3:51
 * Mystery
 */

interface IBaseContext {

    fun exec(runnable: Runnable)

    fun exec(runnable: Runnable, delay: Long)

    fun getHandler(): Handler

    fun addReference(key: String, reference: Any)

    fun getReference(key: String): Any?

    fun removeReference(key: String)

    fun registerProcess(process: ServerMessageProcess)

    fun unRegisterProcess(process: ServerMessageProcess)

}
