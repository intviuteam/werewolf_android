package orangelab.project.common.exhibition.card;

import orangelab.project.game.IWereWolfService;

/**
 * game_werewolf
 * 2018/3/15 上午10:03
 * Mystery
 */

public interface IWereWolfCardService extends IWereWolfService {

    String WEREWOLF_CARD_SERVICE = "WEREWOLF_CARD_SERVICE";

    void showCard(CardEvent.WereWolfRequestShowCardEvent event);

    void dismissCard(CardEvent.WereWolfDismissCardEvent event);

    void useCard(CardEvent.WereWolfRequestUseCardEvent event);
}
