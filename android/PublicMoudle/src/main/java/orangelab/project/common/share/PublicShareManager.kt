package orangelab.project.common.share

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import cn.intviu.support.GsonHelper
import com.androidtoolkit.FileToolKit
import com.androidtoolkit.PLog
import com.androidtoolkit.PicassoUtils
import com.androidtoolkit.StorageToolKit
import com.networktoolkit.transport.RequestTask
import orangelab.project.MainApplication
import orangelab.project.common.utils.ImageUtils
import java.io.File
import java.io.Serializable
import java.lang.Exception
import java.util.*

/**
 * 公共的分享管理类(狼人杀和快玩)
 * 使用object单例，生命周期和app一样，不存在内存消耗问题
 * 增加房间类型就添加新的方法
 */
object PublicShareManager : Serializable {

    val TAG: String = "PublicShareManager"

    val shareFileDir = StorageToolKit.GetStoragePath(MainApplication.getInstance(), "werewolf_share")
    val shareFileName = "share_json"
    val shareVersionSp = "share_version_sp"
    val shareVersionName = "share_version"

    var publicShareCase: PublicShareCase? = null

    /**
     * 从server获取分享内容
     */
    fun initShareContent(url: String) {

        //本地初始化
        initLocal()

        //请求网络数据
        val shareSp = MainApplication.getInstance().getSharedPreferences(shareVersionSp, Context.MODE_PRIVATE)
        val version = shareSp.getString(shareVersionName, "0")
        PLog.e(TAG, "initShareContent: url = ${url + version}")
        RequestTask.create()
                .isGet(true)
                .request(url + version)
                .interceptor {
                    PLog.e(TAG, "response = $it")
                    if (!TextUtils.isEmpty(it)) {
                        try {
                            publicShareCase = GsonHelper.parseJsonToBean(it, PublicShareCase::class.java)

                            //加载分享的icon
                            loadAllIcon()

                            //保存在本地
                            if (publicShareCase != null) {
                                shareSp.edit().putString(shareVersionName, publicShareCase!!.data.version).apply()
                            }
                            //保存数据
                            FileToolKit.writeStringIntoFile(shareFileDir, shareFileName, it)
                            PLog.e(TAG, "publicShareCase = $publicShareCase")
                        } catch (e: Exception) {
                            PLog.e(TAG, "error occur when parse share response, error is ${e.message}")
                        }
                    }
                    true
                }
                .build().execute()

    }

    /**
     * 本地初始化
     */
    fun initLocal() {
        val shareJson = FileToolKit.getStringFromFile(shareFileDir, shareFileName)
        PLog.e(TAG, "shareJson = $shareJson")
        if (!TextUtils.isEmpty(shareJson)) {
            publicShareCase = GsonHelper.parseJsonToBean(shareJson, PublicShareCase::class.java)
        }
        PLog.e(TAG, "publicShareCase = $publicShareCase")

    }


    /**
     * 获取app房分享实体，随机生成
     */
    fun getAppShareContent(): PublicShareCase.ShareBean? {

        try {
            if (publicShareCase!!.data.data.app_share != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.app_share.size)
                return publicShareCase!!.data.data.app_share[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取游戏房分享实体，随机生成
     */
    fun getGameRoomShareContent(): PublicShareCase.ShareBean? {

        try {
            if (publicShareCase!!.data.data.game_room != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.game_room.size)
                return publicShareCase!!.data.data.game_room[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取语音房分享实体，随机生成
     */
    fun getVoiceRoomShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.audio_room != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.audio_room.size)
                return publicShareCase!!.data.data.audio_room[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取小游戏分享实体，随机生成
     */
    fun getMinigameShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.mini_game != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.mini_game.size)
                return publicShareCase!!.data.data.mini_game[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取私密房分享实体，随机生成
     */
    fun getPrivategameShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.private_room != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.private_room.size)
                return publicShareCase!!.data.data.private_room[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取语音房谁是卧底分享实体，随机生成
     */
    fun getSpyInVoiceShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.undercover_room != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.undercover_room.size)
                return publicShareCase!!.data.data.undercover_room[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取谁是卧底房分享实体，随机生成
     */
    fun getSpyRoomShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.undercover_room_new != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.undercover_room_new.size)
                return publicShareCase!!.data.data.undercover_room_new[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }

    /**
     * 获取大厅分享实体，随机生成
     */
    fun getLobbyShareContent(): PublicShareCase.ShareBean? {
        try {
            if (publicShareCase!!.data.data.mini_game_lobby != null) {
                val index = Random().nextInt(publicShareCase!!.data.data.mini_game_lobby.size)
                return publicShareCase!!.data.data.mini_game_lobby[index]
            }
        } catch (e: Exception) {
            //说明有空指针
            return null
        }

        return null
    }


    /**
     * 保存icon
     */
    fun loadAllIcon() {

        //app icon
        val appShareContent = getAppShareContent()
        if (appShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), appShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(appShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //game icon
        val gameRoomShareContent = getGameRoomShareContent()
        if (gameRoomShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), gameRoomShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(gameRoomShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //voice icon
        val voiceShareContent = getVoiceRoomShareContent()
        if (voiceShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), voiceShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(voiceShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //spy icon
        val spyShareContent = getSpyRoomShareContent()
        if (spyShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), spyShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(spyShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //spy in voice icon
        val spyInVoiceShareContent = getSpyInVoiceShareContent()
        if (spyInVoiceShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), spyInVoiceShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(spyInVoiceShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //private room icon
        val privateShareContent = getPrivategameShareContent()
        if (privateShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), privateShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(privateShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }

        //lobby room icon
        val lobbyShareContent = getLobbyShareContent()
        if (lobbyShareContent != null) {
            PicassoUtils.getImage(MainApplication.getInstance(), lobbyShareContent.share_icon, {
                //icon加载过来保存在本地
                FileToolKit.saveFile(lobbyShareContent.share_icon, ImageUtils.bitmapToByteArray(it))
            })
        }
    }

}