package orangelab.project.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.R;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/8/23 下午6:50
 * Mystery
 */

public class WereWolfWebView extends WebView {

    public interface WereWolfWebViewObserver {

        boolean onActionDown();

        boolean onActionUp();
    }

    public ProgressBar progressbar;
    private List<WebChromeClient> mCustomWebChromeClient;
    private List<WebViewClient> mCustomWebViewClient;
    private WereWolfWebViewObserver mObserver;

    public WereWolfWebView(Context context) {
        this(context, null);
    }

    public WereWolfWebView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.webViewStyle);
    }

    public WereWolfWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        progressbar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        progressbar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 10, 0, 0));
        Drawable drawable = context.getResources().getDrawable(R.drawable.werewolf_webview_progressbar);
        progressbar.setProgressDrawable(drawable);
        addView(progressbar, 0);
        getSettings().setSupportZoom(true);
        getSettings().setBuiltInZoomControls(true);
        mCustomWebChromeClient = new ArrayList<>();
        mCustomWebViewClient = new ArrayList<>();
    }

    public void setObserver(WereWolfWebViewObserver observer) {
        mObserver = observer;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mObserver != null) {
                    if (mObserver.onActionDown()) {
                        return true;
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mObserver != null) {
                    if (mObserver.onActionUp()) {
                        return true;
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        LayoutParams lp = (LayoutParams) progressbar.getLayoutParams();
        lp.x = l;
        progressbar.setLayoutParams(lp);
        super.onScrollChanged(l, t, oldl, oldt);
    }
}
