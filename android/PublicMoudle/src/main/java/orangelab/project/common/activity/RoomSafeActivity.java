package orangelab.project.common.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import orangelab.project.common.effect.EffectsMainFestManager;

/**
 * game_werewolf
 * 2018/7/18 下午3:51
 * Mystery
 */
public abstract class RoomSafeActivity extends SafeActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**
         * 每次销毁房间要清理一下缓存
         */
        EffectsMainFestManager.ClearCache();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
