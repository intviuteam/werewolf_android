package orangelab.project.common.effect.event;

import android.graphics.PointF;

/**
 * game_werewolf
 * 2018/7/30 下午4:47
 * Mystery
 */
public class RoomGameEffectsTestEvent {

    public PointF point;
    public int height;
    public int width;
    public int position;

}
