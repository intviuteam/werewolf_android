package orangelab.project.common.engine;

import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;

import com.BuildConfig;
import com.androidtoolkit.PLog;
import com.androidtoolkit.TimeZoneToolKit;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.networktoolkit.transport.TransportConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.GlobalService;
import orangelab.project.common.model.GameEnterRoom;
import orangelab.project.common.utils.Utils;
import orangelab.project.minigame.MiniGameConstant;
import orangelab.project.voice.lobby.api.LobbyRoomConstants;
import orangelab.project.voice.privateroom.model.PrivateRoomMultiIntviu;

/**
 * game_werewolf
 * 2018/1/30 上午11:13
 * Mystery
 */

public enum GlobalSocketEngine {


    INSTANCE;

    private static final int PROCESS_NONE = 0b00000000000000000000000000000000;
    private static final int PROCESS_SERVER_GET = 0b00000000000000000000000000000001;
    private static final int PROCESS_SERVER_GET_SUCCESS = 0b00000000000000000000000000000010;
    private static final int PROCESS_SERVER_GET_ERROR = 0b00000000000000000000000000000100;
    private static final int PROCESS_SOCKET_ENTER = 0b00000000000000000000000000001000;
    private static final int PROCESS_SOCKET_ENTER_SUCCESS = 0b00000000000000000000000000010000;
    private static final int PROCESS_SOCKET_ENTER_ERROR = 0b00000000000000000000000000100000;

    private static final String TAG = "GlobalSocketEngine";
    private SocketEngine mSocketEngine;
    private RequestTask mServerGetRequest = null;
    private JSONObject mServerGetPayload = null;

    private boolean isInit = false;
    private boolean isDestroy = false;

    private int mCurProcessState = PROCESS_NONE;

    private SocketEngineConfig mSocketEngineConfig = null;

    private final long mRetryDelayTime = 3000;

    private Runnable mRetryRunnable = () -> {
        if (isDestroy) {
            return;
        }
        if ((PROCESS_NONE == mCurProcessState)) {
            printLog("socket enter retry because socket config is null at begin");
            initWithOutServer();
            return;
        }
        if ((PROCESS_SOCKET_ENTER_ERROR & mCurProcessState) != 0) {
            printLog("socket enter retry");
            if (mSocketEngineConfig != null) {
                initWithServer(mSocketEngineConfig);
            } else {
                initWithOutServer();
            }
            return;
        }
        if ((PROCESS_SERVER_GET_ERROR & mCurProcessState) != 0) {
            printLog("server get retry");
            initWithOutServer();
            return;
        }
    };

    private GlobalService mGlobalService;

    private GlobalSocketEngine() {
        mServerGetPayload = new JSONObject();
        try {
            mServerGetPayload.put(Constant.USER_ID_LOWER, GlobalUserState.getGlobalState().getUserId());
            mServerGetPayload.put(Constant.GAME_LEVEL, "lobby");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void bindGlobalService(GlobalService globalService) {
        this.mGlobalService = globalService;
    }

    public void unBindGlobalService() {
        this.mGlobalService = null;
    }

    public void post(Runnable runnable) {
        postDelay(runnable, 0);
    }

    public void postDelay(Runnable runnable, long delay) {
        if (mGlobalService != null) {
            mGlobalService.post(runnable, delay);
        }
    }

    public void remove(Runnable runnable) {
        if (mGlobalService != null) {
            mGlobalService.remove(runnable);
        }
    }

    private void retry() {
        if (!isDestroy) {
            remove(mRetryRunnable);
            postDelay(mRetryRunnable, mRetryDelayTime);
        }
    }

    public boolean isSocketError() {
        return mSocketEngine != null ? mSocketEngine.isSocketError() : true;
    }

    public long getServerCurTime() {
        if (mSocketEngine != null) {
            return mSocketEngine.getServerTime();
        } else {
            return System.currentTimeMillis();
        }
    }

    public boolean isDestroy() {
        return isDestroy;
    }

    public void destroy() {
        post(() -> {
            printLog("begin socket engine destroy");
            isDestroy = true;
//            if (!isInit) {
//                printLog("destroy: not init");
//                return;
//            }
            isInit = false;
            mSocketEngineConfig = null;
            releaseSocket();
            printLog("end socket engine destroy");
        });
    }


    public void clearState() {
        post(() -> {
            isDestroy = false;
            isInit = false;
            mCurProcessState = PROCESS_NONE;
        });
    }

    public void initWithOutServer() {
        post(() -> {
            if (isInit) {
                printLog("initWithOutServer: has init");
                return;
            }
            printLog("initWithOutServer: ");
            if (mServerGetRequest != null) {
                mServerGetRequest.cancel();
                mServerGetRequest = null;
            }
            mCurProcessState = PROCESS_SERVER_GET;
            mServerGetRequest = RequestTask.create().request("/server/get").isGet(false).withToken(GlobalUserState.getGlobalState().getToken()).load(mServerGetPayload)
                    .action(new RequestTaskCallBack() {
                        @Override
                        public void onSuccess(String response) {
                            post(() -> {
                                mCurProcessState = PROCESS_SERVER_GET | PROCESS_SERVER_GET_SUCCESS;

                                isInit = true;
                                printLog("server/get success ");

                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String type = jsonObject.optString("type");
                                    String level = jsonObject.optString("level");
                                    String server = jsonObject.optString("server");
                                    String extraData = jsonObject.optString("extraData");

                                    SocketEngineConfig mSocketEngineConfig = new SocketEngineConfig();
                                    mSocketEngineConfig.type = type;
                                    mSocketEngineConfig.level = level;
                                    mSocketEngineConfig.server = server;
                                    mSocketEngineConfig.extraData = extraData;

                                    initWithServer(mSocketEngineConfig);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }

                        @Override
                        public void onFailed(int code, String failed) {
                            printLog(failed);
                            post(() -> {
                                isInit = false;
                                mCurProcessState = PROCESS_SERVER_GET | PROCESS_SERVER_GET_ERROR;
                                retry();
                            });
                        }

                        @Override
                        public void onError(Exception e) {
                            PLog.e(TAG, "", e);
                            post(() -> {
                                isInit = false;
                                mCurProcessState = PROCESS_SERVER_GET | PROCESS_SERVER_GET_ERROR;
                                retry();
                            });
                        }
                    }).build();
            mServerGetRequest.execute();
        });
    }

    public void initWithServer(SocketEngineConfig config) {
        post(() -> {
            printLog("initWithServer: ");
            if (config == null || TextUtils.isEmpty(config.server)) {
                retry();
            } else {
                mSocketEngineConfig = config;
                initSocket(config.type, config.level, config.server, config.extraData);
            }
        });
    }

    public void sendMessage(String type, String payload) {
        if (TextUtils.isEmpty(payload)) {
            sendMessage(type, new JSONObject());
        } else {
            try {
                sendMessage(type, new JSONObject(payload));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(String type, JSONObject payload) {
        post(() -> {
            if (mSocketEngine != null) {
                try {
                    mSocketEngine.nativeRequestMessage(type, payload, null, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void enterSocket(JSONObject sendJson) {
        post(() -> {
            if (mSocketEngine != null) {
                mSocketEngine.nativeRequestMessage(Constant.TYPE_ENTER, sendJson,
                        result -> {
                            post(() -> {
                                isInit = true;
                                mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_SUCCESS;
                            });
                        },
                        result -> {
                            post(() -> {
                                printLog("onOpen: " + result);
                                mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_ERROR;
                                retry();
                            });
                        });
            }
        });
    }

    private void initSocket(String type, String level, String server, String extraData) {
        mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER;
        releaseSocket();
        try {
            final GameEnterRoom.GameEnterRoomItem item = new GameEnterRoom.GameEnterRoomItem();
            item.id = GlobalUserState.getGlobalState().getUserId();
            item.name = GlobalUserState.getGlobalState().getUserName();
            item.sex = GlobalUserState.getGlobalState().getUserSex();
            item.avatar = GlobalUserState.getGlobalState().getUserIcon();
            final GameEnterRoom.GameExtra extra = new GameEnterRoom.GameExtra();
            extra.pt = "android";
            extra.sv = Integer.parseInt(TransportConfig.SV_VALUE);
            extra.tz = TimeZoneToolKit.GetCurrentTimeZone();
            extra.v = BuildConfig.VERSION_CODE;
            extra.lg = Locale.getDefault().getLanguage();
            GameEnterRoom room = new GameEnterRoom();
            room.room_id = "";
            room.password = "";
            room.token = GlobalUserState.getGlobalState().getToken();
            room.type = type;
            room.user = item;
            room.extra = extra;

            if (mGlobalService != null) {
                Handler handle = mGlobalService.getHandler();
                HandlerThread handlerThread = mGlobalService.getHandlerThread();
                if (handle != null && handlerThread != null) {
                    /**
                     * 使用外部提供的线程，节省一个线程的开销
                     */
                    mSocketEngine = new SocketEngine(TAG, SocketEngine.GLOBAL, handle, handlerThread);
                } else {
                    mSocketEngine = new SocketEngine(TAG, SocketEngine.GLOBAL);
                }
            } else {
                mSocketEngine = new SocketEngine(TAG, SocketEngine.GLOBAL);
            }


            mSocketEngine.setSocketMaxReconnectCount(Integer.MAX_VALUE);
            mSocketEngine.setSocketMsgToken(extraData);
            mSocketEngine.setSocketUrl(server, type, "", GameSocketIO.GAME_SOCKET_GLOBAL_PATH);
            mSocketEngine.setBootFinish(true);

            final JSONObject sendJson = new JSONObject(GsonHelper.getGson().toJson(room));
            mSocketEngine.setSocketEngineLifeCycleListener(new SocketEngine.SocketEngineLifeCycleListener() {
                @Override
                public boolean onSocketOpen() {
                    post(() -> {
                        printLog("onSocketOpen");
                    });
                    return true;
                }

                @Override
                public boolean onSocketOpenError() {
                    post(() -> {
                        printLog("onSocketOpenError");
                        isInit = false;
                        mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_ERROR;
                        retry();
                    });
                    return true;
                }

                @Override
                public boolean onSocketConnectInfo() {
                    post(() -> {
                        printLog("onSocketConnectInfo");
                        enterSocket(sendJson);
                    });
                    return true;
                }

                @Override
                public boolean onSocketError() {
                    post(() -> {
                        printLog("onSocketError");
                        isInit = false;
                        mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_ERROR;
                        retry();
                    });
                    return true;
                }

                @Override
                public boolean onSocketClose() {
                    post(() -> {
                        printLog("onSocketClose");
                        isInit = false;
                        mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_ERROR;
                        retry();
                    });
                    return true;
                }
            });
            mSocketEngine.openSocketEngine();
        } catch (Exception e) {
            printLog("Exception :" + e.getMessage());
            isInit = false;
            mCurProcessState = mCurProcessState | PROCESS_SOCKET_ENTER_ERROR;
            retry();
        }
    }

    private void releaseSocket() {
        if (mSocketEngine != null) {
            mSocketEngine.release();
            mSocketEngine = null;
        }
    }

    private void printLog(String info) {
        PLog.i(TAG, "[" + Thread.currentThread().getName() + "]:" + info);
    }

    //------------------------------------------下为对外方便调用封装功能接口

    public void sendInviteCancel() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.TYPE, MiniGameConstant.CANCEL_INVITE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(MiniGameConstant.PLAY_MINI_GAME, jsonObject);
    }

    public void sendPrivateRoomIntviu(String targetUserId, long waitTime) {
        if (TextUtils.isEmpty(targetUserId) || Utils.checkUserIdIsTourist(targetUserId)) {
            return;
        }

        JSONObject payload = new JSONObject();
        try {
            payload.put(Constant.TYPE, GlobalSocketConstant.INVITE);
            payload.put(GlobalSocketConstant.ROOM_TYPE, Constant.GAME_TYPE_PRIVACY);
            payload.put(GlobalSocketConstant.TARGET_USER_ID, targetUserId);
            payload.put(GlobalSocketConstant.WAIT_TIMER, waitTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendMessage(GlobalSocketConstant.PRIVATE_ROOM_TYPE, payload);

    }

    public void sendAcceptPrivateRoomIntviu(String inviteUserId) {
        if (TextUtils.isEmpty(inviteUserId) || Utils.checkUserIdIsTourist(inviteUserId)) {
            return;
        }

        JSONObject payload = new JSONObject();
        try {
            payload.put(Constant.TYPE, GlobalSocketConstant.INVITE_ACCEPT);
            payload.put(GlobalSocketConstant.ROOM_TYPE, Constant.GAME_TYPE_PRIVACY);
            payload.put(GlobalSocketConstant.INVITE_USER_ID, inviteUserId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sendMessage(GlobalSocketConstant.PRIVATE_ROOM_TYPE, payload);
    }

    public void sendCancelPrivateRoomInvite() {
        JSONObject payload = new JSONObject();
        try {
            payload.put(Constant.TYPE, GlobalSocketConstant.INVITE_CANCEL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendMessage(GlobalSocketConstant.PRIVATE_ROOM_TYPE, payload);
    }

    public void sendPrivateRoomRefuseInvite(String inviteUserId) {
        if (TextUtils.isEmpty(inviteUserId) || Utils.checkUserIdIsTourist(inviteUserId)) {
            return;
        }
        JSONObject payload = new JSONObject();
        try {
            payload.put(Constant.TYPE, GlobalSocketConstant.INVITE_REFUSE);
            payload.put(GlobalSocketConstant.ROOM_TYPE, Constant.GAME_TYPE_PRIVACY);
            payload.put(GlobalSocketConstant.INVITE_USER_ID, inviteUserId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(GlobalSocketConstant.PRIVATE_ROOM_TYPE, payload);
    }

    public void sendPrivateRoomMultiInvite(String roomId, String roomType, String roomPassword, List<String> userList) {

        try {
            PrivateRoomMultiIntviu intviu = new PrivateRoomMultiIntviu();
            intviu.room_id = roomId;
            intviu.room_pw = roomPassword;
            intviu.room_type = roomType;
            intviu.target_user_id = userList;
            sendMessage(GlobalSocketConstant.MULTI_PRI_ROOM, new JSONObject(GsonHelper.getGson().toJson(intviu)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 发送游戏邀请信息/小游戏逻辑
     */
    public void sendInvite(String gameType, Long duration) {
        JSONObject json = new JSONObject();
        try {
            json.put(LobbyRoomConstants.TYPE, LobbyRoomConstants.INVITE);
            json.put(LobbyRoomConstants.GAME_TYPE, gameType);
            json.put(LobbyRoomConstants.WAIT_TIMER, duration);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(LobbyRoomConstants.PLAY_MINI_GAME, json);
    }

    /**
     * 发送游戏接受信息/小游戏逻辑
     */
    public void sendAccept(String gameType, String id, String uuid) {
        JSONObject json = new JSONObject();
        try {
            json.put(LobbyRoomConstants.TYPE, LobbyRoomConstants.ACCEPT_INVITE);
            json.put(LobbyRoomConstants.GAME_TYPE, gameType);
            json.put(LobbyRoomConstants.INVITE_USER_ID, id);
            json.put(LobbyRoomConstants.UUID, uuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(LobbyRoomConstants.PLAY_MINI_GAME, json);
    }

}
