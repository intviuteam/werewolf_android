package orangelab.project.common.effect.view

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import com.R
import com.androidtoolkit.PLog
import com.androidtoolkit.StreamToolKit
import com.androidtoolkit.ThreadToolKit
import com.networktoolkit.transport.Constant
import orangelab.project.common.effect.ApkResourceManager
import orangelab.project.common.effect.EffectsManager
import orangelab.project.common.effect.EffectsUtils
import orangelab.project.common.effect.event.RoomEffectEvent
import orangelab.project.voice.constants.VoiceConstants
import orangelab.project.voice.manager.GameScoreReflection
import orangelab.project.voice.manager.VoiceRoomManager
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import java.util.ArrayList

/**
 *game_werewolf
 *2018/8/2 下午1:33
 *Mystery
 * 用于展示房间小游戏的View
 */
class RoomMemberEffectsView : FrameLayout, Runnable {
    companion object {
        val TAG = "RoomMemberEffectsView"
    }

    /**
     * 骰子小游戏View
     */
    private var mDiceView: RoomDiceView? = null

    /**
     * 扑克牌小游戏
     */
    private var mPokerCardView: RoomPokerCardView? = null

    /**
     * 打分小游戏
     */
    private var mPointsGameView: ImageView? = null

    private lateinit var mEffectsContext: RoomEffectsContext

    private var mGifView: RoomGifImageView? = null

    private var mHeight: Int = 0
    private var mWidth: Int = 0
    private var mEventCache: MutableList<RoomEffectEvent.RoomMemberEffect> = ArrayList()
    private var mIsRunning = false
    /**
     * 特效View的位置
     */
    var position = -1

    /**
     * 当前执行的特效类型
     */
    private var mCurType: String = Constant.EMPTY

    constructor(context: Context?) : this(context, null) {

    }

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0) {

    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

    }


    fun bindEffectsContext(context: RoomEffectsContext) {
        mEffectsContext = context
    }

    private fun createRoomGifImageView(): RoomGifImageView {
        val result = RoomGifImageView(context)
        val params = LayoutParams(mHeight, mWidth)
        result.setFinishListener(this)
        params.gravity = Gravity.CENTER
        result.layoutParams = params
        return result
    }


    private fun handlePokerCardGame(event: RoomEffectEvent.RoomMemberEffect) {
        mPokerCardView = RoomPokerCardView(context)

        var cardHeight = event.height * 2 / 5
        var cardWidth = event.width

        var params = LayoutParams(cardWidth, cardHeight)
        mPokerCardView!!.layoutParams = params
        mPokerCardView!!.x = 0f
        mPokerCardView!!.y = (event.height - cardHeight).toFloat()
        addView(mPokerCardView)
        mPokerCardView!!.initCardTest(cardWidth, cardHeight)
        mEffectsContext.isPokerIsShown = true
        handleNextPost()
    }

    private fun handlePointsGame(event: RoomEffectEvent.RoomMemberEffect) {
        EffectsUtils.removeViewIfNeed(this, mPointsGameView)
        mPointsGameView = ImageView(context)
        val params = LayoutParams(mWidth, mHeight)
        mPointsGameView!!.layoutParams = params
        addView(mPointsGameView!!)
        mEffectsContext.post {
            mPointsGameView!!.setImageResource(GameScoreReflection.scoreToImageId(event.lastFrame))
            val animation = AnimationUtils.loadAnimation(context, R.anim.anim_game_score)
            animation.fillAfter = true
            mPointsGameView!!.startAnimation(animation)
        }
        val delay = event.lastFrameDuration
        mEffectsContext.post({
            EffectsUtils.removeViewIfNeed(this, mPointsGameView)
            handleNext()
        }, delay)
    }

    private fun handleEmotion(event: RoomEffectEvent.RoomMemberEffect) {
        ThreadToolKit.Async {
            try {
                var inputStream = ApkResourceManager.GetApkAssetInputStream(event.type)
                if (inputStream == null) {
                    inputStream = EffectsManager.GetEmotionInputStream(event.url)
                }
                val data = StreamToolKit.readStream(inputStream)
                try {
                    inputStream!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                if (data == null) {
                    handleNextPost()
                    return@Async
                }
                mEffectsContext.post {
                    startGifTask(RoomGifImageView.Task(data, event.repeatTime, event.duration))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                handleNextPost()
            }
        }
    }

    private fun handleSmallGame(event: RoomEffectEvent.RoomMemberEffect) {

        /**
         * 处理小游戏
         */
        val smallGame = mEffectsContext.getSmallGame(event.type)
        smallGame ?: return

        ThreadToolKit.Async {
            try {
                var bytes: ByteArray? = null
                try {
                    bytes = StreamToolKit.readStream(context.assets.open(smallGame.url))
                } catch (e: Exception) {

                }

                mEffectsContext.post {
                    if (bytes != null) {
                        /**
                         * 解析小游戏
                         */
                        val task = RoomGifImageView.Task(bytes, event.repeatTime, event.duration)
                        if (event.isSmallDiceGame) {
                            task.mFrameCreator = EffectsUtils.DiceGameFrameCreator(context, event.lastFrame, event.lastFrameDuration)
                        } else if (event.isGuessGame) {
                            task.mFrameCreator = EffectsUtils.GuessGameFrameCreator(context, event.lastFrame, event.lastFrameDuration)
                        } else if (event.isOptMachineGame) {
                            task.mFrameCreator = EffectsUtils.OctopusMachineGameFrameCreator(context, event.lastFrame, event.lastFrameDuration, event.width, event.height)
                        } else if (event.isLotteryGame) {
                            task.mFrameCreator = EffectsUtils.LotteryGameFrameCreator(context, event.lastFrame, event.lastFrameDuration, event.width, event.height)
                        } else if (event.isCoinGame) {
                            task.mFrameCreator = EffectsUtils.CoinGameFrameCreator(context, event.lastFrame, event.lastFrameDuration)
                        } else if (event.isScoreGame) {
                            /**
                             * 打分游戏单独处理
                             */
                            handlePointsGame(event)
                            return@post
                        }
                        startGifTask(task)
                        /**
                         * 如果声音不为空，也需要展示声音
                         */
                        if (!TextUtils.isEmpty(event.backgroundMusic)) {
                            VoiceRoomManager.playSound(context, event.backgroundMusic, event.lastFrameDuration.toInt())
                        }
                        return@post
                    }

                    if (event.isScoreGame) {
                        handlePointsGame(event)
                    } else if (event.isPokerCardGame) {
                        handlePokerCardGame(event)
                    }


                }
            } catch (e: IOException) {
                e.printStackTrace()
                handleNextPost()
            }
        }


    }

    fun updateWidthAndHeight(width: Int, height: Int) {
        mHeight = height
        mWidth = width
    }

    fun handleRoomEffectsEvent(event: RoomEffectEvent.RoomMemberEffect) {
        /**
         * 如果当前缓存不是空的，说明当前正在有特效播放
         */
        if (mIsRunning) {
            mEventCache.add(event)
            PLog.i(TAG, "add event into cache")
        } else {
            mIsRunning = true

            removePokerCardView()

            mCurType = event.type

            if (event.isEmotion) {
                handleEmotion(event)
                return
            }

            if (event.isSmallGame) {
                handleSmallGame(event)
                return
            }
        }
    }

    fun startGifTask(task: RoomGifImageView.Task) {
        try {
            if (mGifView == null) {
                mGifView = createRoomGifImageView()
                addView(mGifView)
                mEffectsContext.post {
                    mGifView?.start(task)
                }
            } else {
                mGifView?.start(task)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            handleNextPost()
        }
    }

    private fun handleNext() {
        if (mEventCache.isEmpty()) {
            mIsRunning = false
            PLog.i(TAG, "cache is empty")
            /**
             * cache为空了，开启一个检测
             */
            mEffectsContext.post({
                if (canClear()) {
                    /**
                     * 通知parent要移除自己
                     */
                    EventBus.getDefault().post(RoomEffectEvent.RoomEffectClear(position))
                }
            }, 10000)
        } else {
            val event = mEventCache.removeAt(0)
            PLog.i(TAG, "use event from cache")
            mIsRunning = false
            handleRoomEffectsEvent(event)
        }
    }

    private fun handleNextPost() {
        mEffectsContext.post {
            handleNext()
        }
    }

    /**
     * 特效结束时回调
     */
    override fun run() {
        PLog.i("GifFramesAnim", "onGifFinish")
        handleNext()
    }

    private fun onEffectsFinish() {
        run()
    }

    private fun canClear(): Boolean {
        /**
         * 如果当前为poker类型，由于poker会一直存在，则会认为一直运行，但是实际上已经有可能不再运行了
         */
        if (TextUtils.equals(mCurType, VoiceConstants.POKER)) {
            return false
        }
        return !mIsRunning
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeDiceGameView()
        removePokerCardView()
        EffectsUtils.removeViewIfNeed(this, mGifView)
        mEventCache.clear()
    }

    private fun removePokerCardView() {
        EffectsUtils.removeViewIfNeed(this, mPokerCardView)
        mEffectsContext.isPokerIsShown = false
    }

    private fun removeDiceGameView() {
        EffectsUtils.removeViewIfNeed(this, mDiceView)
    }

}