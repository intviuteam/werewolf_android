package orangelab.project.common.bridge.minigame;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * game_werewolf
 * 2018/8/1 下午6:58
 * Mystery
 */
public class MiniGameDownloadViewManager extends SimpleViewManager<MiniGameDownloadView> {

    private static final String TAG = "MiniGameDownloadView";

    @Override
    public String getName() {
        return "MiniGameDownloadMaskView";
    }

    @Override
    protected MiniGameDownloadView createViewInstance(ThemedReactContext reactContext) {
        MiniGameDownloadView miniGameDownloadView = new MiniGameDownloadView(reactContext);
        return miniGameDownloadView;
    }

    @ReactProp(name = "checkGame")
    public void checkGame(MiniGameDownloadView view, String json) {

        Log.i(TAG, "checkGame: " + json);

        /**
         * 根据不同type来设定
         */
        try {
            JSONObject jsonObject = new JSONObject(json);
            view.setJSONObject(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomBubblingEventTypeConstants() {
        return MapBuilder.<String, Object>builder()
                .put(
                        "checkGameEvent",
                        MapBuilder.of("phasedRegistrationNames", MapBuilder.of("bubbled", "onCheckGameResult"))
                )
                .build();
    }

}
