package orangelab.project.common.share

import android.text.TextUtils

import com.R
import orangelab.project.common.utils.MessageUtils

/**
 * game_werewolf
 * 2018/2/27 下午2:22
 * Mystery
 */
class ShareHelper {
    companion object {
        fun GetFamilyRankList(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_family, value)
        }

        fun GetFamilyRankPopular(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_family_value, value)
        }

        fun GetPopularRankList(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_popular, value)
        }

        fun GetPopularRankPopular(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_popular_value, value)
        }

        fun GetRoomRankListYesterDay(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_room_master_yesterday, value)
        }

        fun GetRoomRankListLastWeek(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_room_master_lastweek, value)
        }

        fun GetRoomRankListTotal(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_room_master_total, value)
        }

        fun GetRoomRankLike(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_room_like, value)
        }

        fun GetRoomRankPopular(value: String?): String {
            return GetFamilyRankPopular(value)
        }

        fun GetTotalPopularRankList(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_total_popular, value)
        }

        fun GetTotalPopularRankPopular(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_total_popular_value, value)
        }

        fun GetTodayPopularRankList(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_total_popular, value)
        }

        fun GetTodayPopularRankPopular(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_total_popular_value, value)
        }

        fun GetLastWeekPopularRankList(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "-"
            }
            return MessageUtils.getString(R.string.string_rank_popular_lastweek, value)
        }

        fun GetLastWeekPopularRankPopular(value: String?): String {
            var value = value
            if (TextUtils.isEmpty(value)) {
                value = "0"
            }
            return MessageUtils.getString(R.string.string_rank_popular_lastweek_value, value)
        }
    }
}
