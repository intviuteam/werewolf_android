package orangelab.project.common.system;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/8/4 下午1:19
 * Mystery
 */

public class SystemMessage implements Keepable {

    private String mTitle;
    private String mContent;

    public SystemMessage(String title, String content) {
        mTitle = title;
        mContent = content;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }
}
