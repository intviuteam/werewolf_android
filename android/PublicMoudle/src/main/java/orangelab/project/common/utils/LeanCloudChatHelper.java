package orangelab.project.common.utils;

import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessageOption;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.R;

import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.manager.AppManager;
import orangelab.project.voice.config.VoiceRoomConfig;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import orangelab.thirdparty.leancloud.chatkit.LCChatKit;
import orangelab.thirdparty.leancloud.chatkit.event.LCIMIMTypeMessageEvent;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMGiftMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMIntviuMessage;
import orangelab.thirdparty.leancloud.chatkit.model.AVIMTempTextMessage;
import orangelab.thirdparty.leancloud.chatkit.utils.Constant;

import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.utils.LeanCloudToolKit;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2017/5/17 下午4:23
 * Mystery
 */

public class LeanCloudChatHelper {

    private static final String TAG = "LeanCloudChatHelper";

    public static void sendGiftMessageIntoConversationAsync(
            String giftType,
            String targetId,
            String targetName,
            String targetIv,
            int targetSex,
            String rebate
    ) {
        ThreadToolKit.Async(() -> {
            if (LCChatKit.getInstance().getClient() == null) {
                return;
            }
            Map<String, Object> mapData = new HashMap<>();
            mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
            mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
            mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
            mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
            mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
            mapData.put(Constant.GIFT_TYPE, giftType);
            mapData.put(Constant.GIFT_REBATE, rebate);
            mapData.put(Constant.APP, AppManager.GetApp());
            Map<String, Object> target = new HashMap<>();
            target.put(Constant.USER_ID, targetId);
            target.put(Constant.USER_ICON, targetIv);
            target.put(Constant.USER_SEX, Integer.toString(targetSex));
            target.put(Constant.USER_NAME, targetName);
            target.put(Constant.GIFT_TYPE, giftType);
            target.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
            target.put(Constant.READ, true);
            target.put(Constant.APP, AppManager.GetApp());
            AVIMGiftMessage message = new AVIMGiftMessage();
            String name = EffectsManager.GetSupportedTypeName(giftType);
            if (TextUtils.isEmpty(name)) {
                message.setText(MessageUtils.getString(R.string.present_gift,
                        MessageUtils.getString(R.string.gifts_chat_type_unsupport)));
            } else {
                message.setText(MessageUtils.getString(R.string.present_gift, name));
            }
            message.setAttrs(mapData);
            LCChatKit.getInstance().getClient().createConversation(
                    Arrays.asList(targetId), "", null, false, true, new AVIMConversationCreatedCallback() {
                        @Override
                        public void done(AVIMConversation avimConversation, AVIMException e) {
                            if (e == null) {
                                if (avimConversation != null) {
                                    AVIMMessageOption option = new AVIMMessageOption();
                                    option.setReceipt(true);
                                    avimConversation.sendMessage(message, option, new AVIMConversationCallback() {
                                        @Override
                                        public void done(AVIMException e) {
                                            UIActuator.post(() -> {
                                                if (e == null) {
                                                    Log.i(TAG, "done: 发送礼物聊天数据成功 " + targetName);
                                                    message.setAttrs(target);
                                                    LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                                                    event.conversation = avimConversation;
                                                    event.message = message;
                                                    EventBus.getDefault().post(event);
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
        });
    }

    public static void sendGiftMessageIntoConversation(
            String giftType,
            String targetId,
            String targetName,
            String targetIv,
            int targetSex
    ) {
        if (LCChatKit.getInstance().getClient() == null) {
            return;
        }
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
        mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
        mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
        mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
        mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
        mapData.put(Constant.APP, AppManager.GetApp());
        mapData.put(Constant.GIFT_TYPE, giftType);
        Map<String, Object> target = new HashMap<>();
        target.put(Constant.USER_ID, targetId);
        target.put(Constant.USER_ICON, targetIv);
        target.put(Constant.USER_SEX, Integer.toString(targetSex));
        target.put(Constant.USER_NAME, targetName);
        target.put(Constant.GIFT_TYPE, giftType);
        target.put(Constant.APP, AppManager.GetApp());
        target.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
        target.put(Constant.READ, true);
        AVIMGiftMessage message = new AVIMGiftMessage();
        String name = EffectsManager.GetSupportedTypeName(giftType);
        if (TextUtils.isEmpty(name)) {
            message.setText(MessageUtils.getString(R.string.present_gift,
                    MessageUtils.getString(R.string.gifts_chat_type_unsupport)));
        } else {
            message.setText(MessageUtils.getString(R.string.present_gift, name));
        }
        message.setAttrs(mapData);
        LCChatKit.getInstance().getClient().createConversation(
                Arrays.asList(targetId), "", null, false, true, new AVIMConversationCreatedCallback() {

                    @Override
                    public void done(AVIMConversation avimConversation, AVIMException e) {
                        if (e == null) {
                            if (avimConversation != null) {
                                AVIMMessageOption option = new AVIMMessageOption();
                                option.setReceipt(true);
                                avimConversation.sendMessage(message, option, new AVIMConversationCallback() {
                                    @Override
                                    public void done(AVIMException e) {
                                        if (e == null) {
                                            Log.i(TAG, "done: 发送礼物聊天数据成功 " + targetName);
                                            message.setAttrs(target);
                                            LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                                            event.conversation = avimConversation;
                                            event.message = message;
                                            EventBus.getDefault().post(event);
                                        }
                                    }
                                });
                            }
                        } else {
                            /**
                             * ignore
                             */
                        }
                    }
                });
    }

    /**
     * 向对话里面发送一条Text数据
     *
     * @param msg
     * @param targetId
     * @param targetName
     * @param targetIv
     * @param targetSex
     */
    public static void sendTextMessageIntoConversation(
            String msg,
            String targetId,
            String targetName,
            String targetIv,
            int targetSex
    ) {
        sendTextMessageIntoConversation(msg, targetId, targetName, targetIv, targetSex, "");
    }

    public static void sendTempMessageIntoConversation(String msg, String targetId) {
        if (TextUtils.isEmpty(targetId)) {
            return;
        }
        final Map<String, Object> mapData = new HashMap<>();
        mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
        mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
        mapData.put(Constant.USER_SEX, GlobalUserState.getGlobalState().getUserSex());
        mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
        mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_SINGLE);
        mapData.put(Constant.APP, AppManager.GetApp());
        AVIMTempTextMessage message = new AVIMTempTextMessage();
        message.setText(msg);
        try {
            LCChatKit.getInstance().getClient().createConversation(
                    Arrays.asList(targetId), "", null, false, true, new AVIMConversationCreatedCallback() {
                        @Override
                        public void done(AVIMConversation avimConversation, AVIMException e) {
                            if (e == null) {
                                if (avimConversation != null) {
                                    mapData.put(Constant.CONVERSATION_ID, avimConversation.getConversationId());

                                    AVIMMessageOption option = new AVIMMessageOption();
                                    option.setTransient(true);

                                    avimConversation.sendMessage(message, option, new AVIMConversationCallback() {
                                        @Override
                                        public void done(AVIMException e) {
                                            if (e == null) {
                                                /**
                                                 * Send Success
                                                 */
                                            }
                                        }
                                    });
                                }
                            } else {
                                /**
                                 * ignore
                                 */
                            }
                        }
                    }
            );
        } catch (Exception e) {
            /**
             * 这里有一个情况 getClient 可能为空，在用户链接leanCloud失败的时候，只有这一种情况，所以此异样吞噬
             */
        }

    }

    /**
     * 向对话里面发送一条Text数据
     *
     * @param msg
     * @param targetId
     * @param targetName
     * @param targetIv
     * @param targetSex
     */
    public static void sendTextMessageIntoConversation(
            String msg,
            String targetId,
            String targetName,
            String targetIv,
            int targetSex,
            String intent
    ) {
        if (LCChatKit.getInstance().getClient() == null) {
            return;
        }
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
        mapData.put(Constant.APP, AppManager.GetApp());
        mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
        mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
        mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
        if (!TextUtils.isEmpty(intent)) {
            mapData.put(Constant.INTENT, intent);
        }
        Map<String, Object> target = new HashMap<>();
        target.put(Constant.USER_ID, targetId);
        target.put(Constant.USER_ICON, targetIv);
        target.put(Constant.USER_SEX, Integer.toString(targetSex));
        target.put(Constant.USER_NAME, targetName);
        target.put(Constant.READ, true);
        target.put(Constant.APP, AppManager.GetApp());
        if (!TextUtils.isEmpty(intent)) {
            target.put(Constant.INTENT, intent);
        }
        AVIMTextMessage message = new AVIMTextMessage();
        message.setText(msg);
        message.setAttrs(mapData);
        LCChatKit.getInstance().getClient().createConversation(
                Arrays.asList(targetId), "", null, false, true, new AVIMConversationCreatedCallback() {
                    @Override
                    public void done(AVIMConversation avimConversation, AVIMException e) {
                        if (e == null) {
                            if (avimConversation != null) {
                                AVIMMessageOption option = new AVIMMessageOption();
                                option.setReceipt(true);
                                avimConversation.sendMessage(message, option, new AVIMConversationCallback() {
                                    @Override
                                    public void done(AVIMException e) {
                                        if (e == null) {
                                            message.setAttrs(target);
                                            LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                                            event.conversation = avimConversation;
                                            event.message = message;
                                            EventBus.getDefault().post(event);
                                        }
                                    }
                                });
                            }
                        } else {
                            /**
                             * ignore
                             */
                        }
                    }
                }
        );

    }

    private static int successCount = 0;
    private static int count = 0;

    public static void SendIntviuToConversationForFamily(String roomId, String password, String conversationId) {
        if (TextUtils.isEmpty(conversationId)) {
            return;
        }
        final Map<String, Object> mapData = new HashMap<>();
        mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
        mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
        mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
        mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
        mapData.put(Constant.ROOM_ID, roomId);
        mapData.put(Constant.GAME_TYPE, GlobalUserState.getGlobalState().getCurGameType());
        mapData.put(Constant.ROOM_PASSWORD, password);
        mapData.put(Constant.APP, AppManager.GetApp());
        mapData.put(Constant.MSG_TYPE, Constant.MSG_TYPE_FAMILY);
        mapData.put(Constant.CONVERSATION_ID, conversationId);
        mapData.put(com.networktoolkit.transport.Constant.CHILD_TYPE, VoiceRoomConfig.transferTypeToShareTitle());

        AVIMIntviuMessage textMessage = new AVIMIntviuMessage();
        textMessage.setAttrs(mapData);
        textMessage.setText(MessageUtils.getString(R.string.intviu_room_msg, roomId));

        LeanCloudToolKit.SendMessageIntoConversation(conversationId, textMessage, new AVIMConversationQueryCallback() {
            @Override
            public void done(List<AVIMConversation> list, AVIMException e) {
                if (e != null) {
                    UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_intviu_into_family_failed)));
                } else {
                    if (list != null && list.size() > 0) {
                        Log.i(TAG, "done: 邀请至家族成功");
                        UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.intviu_friend_send)));
                        LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                        event.conversation = list.get(0);
                        event.message = textMessage;
                        EventBus.getDefault().post(event);
                        FamilyApiManager.FamilyTaskDoIntviuApi();
                    } else {
                        UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_intviu_into_family_failed)));
                    }
                }
            }
        });
    }

    public static void sendIntviuToConversation(String roomId, String password,
                                                List<IntviuFriendDialog.FriendItem> datas) {
        final Map<String, Object> mapData = new HashMap<>();
        mapData.put(Constant.USER_ID, GlobalUserState.getGlobalState().getUserId());
        mapData.put(Constant.USER_ICON, GlobalUserState.getGlobalState().getUserIcon());
        mapData.put(Constant.USER_SEX, Integer.toString(GlobalUserState.getGlobalState().getUserSex()));
        mapData.put(Constant.USER_NAME, GlobalUserState.getGlobalState().getUserName());
        mapData.put(Constant.ROOM_ID, roomId);
        mapData.put(Constant.APP, AppManager.GetApp());
        mapData.put(Constant.GAME_TYPE, GlobalUserState.getGlobalState().getCurGameType());
        mapData.put(Constant.ROOM_PASSWORD, password);

        //语音房子类型
        mapData.put(com.networktoolkit.transport.Constant.CHILD_TYPE, VoiceRoomConfig.transferTypeToShareTitle());

        Observable.from(datas)
                .onBackpressureBuffer()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<IntviuFriendDialog.FriendItem>() {

                    @Override
                    public void onStart() {
                        request(1);
                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(IntviuFriendDialog.FriendItem intviuFriendItem) {
                        LCChatKit.getInstance().getClient().createConversation(Arrays.asList(intviuFriendItem.id), "", null, false, true, new
                                AVIMConversationCreatedCallback() {
                                    @Override
                                    public void done(AVIMConversation avimConversation, AVIMException e) {
                                        count++;
                                        if (null != e) {
                                            ToastToolKit.showShort(MessageUtils.getString(R.string.intviu_failed, intviuFriendItem.name));
                                            request(1);
                                        } else {
                                            if (avimConversation != null) {
                                                AVIMIntviuMessage textMessage = new AVIMIntviuMessage();
                                                textMessage.setAttrs(mapData);
                                                textMessage.setText(MessageUtils.getString(R.string.intviu_room_msg, roomId));
                                                AVIMMessageOption option = new AVIMMessageOption();
                                                option.setReceipt(true);
                                                avimConversation.sendMessage(textMessage, option, new AVIMConversationCallback() {
                                                    @Override
                                                    public void done(AVIMException e) {
                                                        if (e == null) {
                                                            Log.i(TAG, "done: 邀请" + intviuFriendItem.name + "成功");
                                                            successCount++;
                                                            textMessage.setText(MessageUtils.getString(R.string.intviu_msg_from_myself));
                                                            Map<String, Object> otherData = new HashMap<>();
                                                            otherData.put(Constant.USER_ID, intviuFriendItem.id);
                                                            otherData.put(Constant.USER_ICON, intviuFriendItem.image);
                                                            otherData.put(Constant.USER_SEX, Integer.toString(intviuFriendItem.sex));
                                                            otherData.put(Constant.USER_NAME, intviuFriendItem.name);
                                                            otherData.put(Constant.READ, true);
                                                            textMessage.setAttrs(otherData);
                                                            LCIMIMTypeMessageEvent event = new LCIMIMTypeMessageEvent();
                                                            event.conversation = avimConversation;
                                                            event.message = textMessage;
                                                            EventBus.getDefault().post(event);
                                                        } else {
                                                            Log.i(TAG, "done: 邀请" + intviuFriendItem.name + "失败");
                                                            ToastToolKit.showShort(MessageUtils.getString(R.string.intviu_failed, intviuFriendItem.name));
                                                        }
                                                        request(1);
                                                        //邀请好友成功后上报服务器
                                                        if (count == datas.size() && successCount > 0) {
                                                            shareSuccessReported();
                                                            Utils.runSafely(() -> {
                                                                if (datas != null) {
                                                                    datas.clear();
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                request(1);
                                            }
                                        }
                                    }
                                });
                    }
                });
    }

    private static void shareSuccessReported() {
        ApiManager.DoShareApi(com.networktoolkit.transport.Constant.INVITE_TO_FRIEND, "", 1, (s, e) -> {
            count = 0;
            successCount = 0;
        });
    }

}
