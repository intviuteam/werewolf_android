package orangelab.project.common.floatwindow;

import android.content.Context;
import android.content.Intent;

import com.androidtoolkit.RxToolKit;
import orangelab.project.common.floatwindow.event.RestoreEvent;
import orangelab.project.common.floatwindow.model.FloatUser;
import orangelab.project.common.floatwindow.permission.FloatPermissionManager;
import orangelab.project.common.event.SmallWindowEvent;
import com.networktoolkit.transport.Constant;
import com.toolkit.action.Action;

import java.util.UUID;


import static orangelab.project.common.floatwindow.FloatWindowInnerManager.INSTANCE;

/**
 * game_werewolf
 * 2017/12/26 下午2:46
 * Mystery
 * 通过此方法来对外操纵悬浮Window操作
 */

public class FloatWindowCommander {

    public static void StartService(Context context, Action<Integer> callback) {
        StartService(context, null, callback);
    }

    public static void StartService(Context context, FloatUser user, Action<Integer> callback) {
        if (!INSTANCE.isServerHasStart()) {
            if (FloatPermissionManager.getInstance().applyFloatWindow(context)) {
                if (callback != null) {
                    callback.func(0);
                }
                Intent intent = new Intent(context, FloatWindowService.class);
                if (user != null) {
                    intent.putExtra(Constant.DATA, user);
                }
                context.startService(intent);
            } else {
                if (callback != null) {
                    callback.func(-1);
                }
            }
        }
    }

    public static boolean IsFloatWindowOpen() {
        return INSTANCE.isServerHasStart();
    }

    public static void StopService(Context context, Action<Integer> callback) {
        if (INSTANCE.isServerHasStart()) {
            Intent intent = new Intent(context, FloatWindowService.class);
            context.stopService(intent);
            if (callback != null) {
                callback.func(0);
            }
        }
    }

    public static void ReEnterRoom() {
        ReEnterRoom(false);
    }

    public static void ReEnterRoom(boolean flag) {
        RxToolKit.Emit(new RestoreEvent(flag));
    }

    public static void ShowFloatWindow() {
        FloatWindowInnerManager.Show();
    }

    public static void HideFloatWindow() {
        FloatWindowInnerManager.Hide();
    }

    public static void DestroyFloatWindow(Context context, Action<Exception> after) {
        StopService(context, null);
        RxToolKit.Build(UUID.randomUUID().toString(), SmallWindowEvent.VoiceEngineReleasedEvent.class).destroyWhenFinish(true).destroyWhenActionDone(true).action(value -> {
            if (after != null) {
                after.func(null);
            }
        }).register();
        RxToolKit.Emit(new SmallWindowEvent.DestroyRoomFromSwEvent());
    }

    public static void DestroyFloatWindowImmediately(Context context, Action<Integer> callback) {
        StopService(context, callback);
    }

}
