package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/20 上午10:33
 * Mystery
 */

public class ServerActionShowEmoticon extends ServerActionContainsPosition implements Keepable {
    public String mark;
    public String url;
    public String thumbnail;
}
