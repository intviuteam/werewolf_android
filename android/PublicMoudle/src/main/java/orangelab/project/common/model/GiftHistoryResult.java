package orangelab.project.common.model;

import java.util.HashMap;
import java.util.List;

/**
 * game_werewolf
 * 2017/7/4 下午6:19
 * Mystery
 */

public class GiftHistoryResult {

    public List<GiftHistoryItem> histories;
    public HashMap<String, GiftHistoryUserDataItem> users;

    public static class GiftHistoryItem {
        public long time;
        public String from;
        public String type;
    }

    public static class GiftHistoryUserDataItem {
        public String name;
        public String id;
        public String image;
    }
}
