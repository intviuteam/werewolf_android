package orangelab.project.common.manager;

import android.content.Context;
import android.text.TextUtils;

import orangelab.project.common.db.BlackListLightDaoHelper;
import orangelab.project.common.db.entity.BlackListLightEntity;
import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.model.BlockListResult;
import com.androidtoolkit.PLog;

import com.datasource.GlobalUserState;
import orangelab.project.common.utils.Utils;
import com.toolkit.ToolKit;
import com.toolkit.action.ApiResult;

import static com.avos.avoscloud.Messages.OpType.result;

/**
 * game_werewolf
 * 2017/9/18 下午4:07
 * Mystery
 */

public class BlackListManager {

    private static final String TAG = "BlackListManager";

    private static volatile BlackListManager mInstance;
    private BlackListLightDaoHelper mDaoHelper;

    private BlackListManager() {

    }

    private static BlackListManager getInstance() {
        if (mInstance == null) {
            synchronized (BlackListManager.class) {
                if (mInstance == null) {
                    mInstance = new BlackListManager();
                }
            }
        }
        return mInstance;
    }

    private void init(Context context) {
        mDaoHelper = new BlackListLightDaoHelper(context);
    }

    private void refresh() {
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken())) {
            PLog.i(TAG, "Token is null so skip this refresh");
            return;
        }
        ApiManager.BlockListAll((result, e) -> {
            if (e == null) {
                Utils.runSafely(() -> {
                    final long updateTime = System.currentTimeMillis();
                    if (Utils.targetIsNull(result)) {
                        return;
                    }
                    if (ToolKit.isEmpty(result.users)) {
                        if (mDaoHelper != null) {
                            mDaoHelper.deleteAll(GlobalUserState.getGlobalState().getUserId());
                        }
                    }
                    for (IntviuFriendDialog.FriendItem item : result.users) {
                        if (mDaoHelper != null) {
                            BlackListLightEntity entity = new BlackListLightEntity();
                            entity.setUserId(item.id);
                            entity.setCreateTime(item.created_at);
                            mDaoHelper.refresh(GlobalUserState.getGlobalState().getUserId(), entity, updateTime);
                        }
                    }
                    if (mDaoHelper != null) {
                        mDaoHelper.refreshAll(GlobalUserState.getGlobalState().getUserId(), updateTime);
                    }
                });
            } else {

            }
        });
    }

    private boolean has(String userId) {
        if (TextUtils.isEmpty(userId))
            return false;
        boolean result = mDaoHelper.hasEntity(GlobalUserState.getGlobalState().getUserId(), userId);
        if (result) {
            PLog.i(TAG, "this message has been filter");
        }
        return result;
    }

    private void remove(String userId) {
        if (TextUtils.isEmpty(userId))
            return;
        mDaoHelper.delete(GlobalUserState.getGlobalState().getUserId(), userId);
    }

    private void add(String userId) {
        if (TextUtils.isEmpty(userId))
            return;
        BlackListLightEntity entity = new BlackListLightEntity();
        entity.setUserId(userId);
        entity.setCreateTime(System.currentTimeMillis());
        mDaoHelper.insert(GlobalUserState.getGlobalState().getUserId(), entity);
    }

    public static void Init(Context context) {
        getInstance().init(context);
    }

    public static void Add(String userId) {
        getInstance().add(userId);
    }

    public static void Remove(String userId) {
        getInstance().remove(userId);
    }

    public static void Refresh() {
        getInstance().refresh();
    }

    public static boolean Has(String userId) {
        return getInstance().has(userId);
    }
}
