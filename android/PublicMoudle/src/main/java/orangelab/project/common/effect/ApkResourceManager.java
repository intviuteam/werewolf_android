package orangelab.project.common.effect;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

import com.R;
import com.androidtoolkit.PLog;

import orangelab.project.MainApplication;
import orangelab.project.common.exhibition.card.CardUtils;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * game_werewolf
 * 2017/11/10 下午6:34
 * Mystery
 * <p>
 * 包里面的礼物数据管理器
 */

public class ApkResourceManager {

    public static final String TAG = "ApkResourceManager";

    public static int GetApkCardResource(String giftType) {
        return CardUtils.getCardBgResource(giftType);
    }

    public static int GetApkImageResource(String image) {
        if (TextUtils.isEmpty(image)) {
            return 0;
        }
        Class mipmap = R.mipmap.class;
        try {
            Field field = mipmap.getField(image);
            int resId = field.getInt(image);
            return resId;
        } catch (Exception e) {
            PLog.e(TAG,"Resource get failed: "+image);
            return 0;
        }
    }

    public static int GetApkDrawableResource(Context context, String image) {
        try {
            if (TextUtils.isEmpty(image)) {
                return -1;
            }
            int resId = context.getResources().getIdentifier(image, "drawable", context.getPackageName());
            return resId;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int GetApkImageResource(Context context, String image) {
        try {
            if (TextUtils.isEmpty(image)) {
                return -1;
            }
            int resId = context.getResources().getIdentifier(image, "mipmap", context.getPackageName());
            return resId;
        } catch (Exception e) {
            PLog.e(TAG,"Resource get failed: "+image);
            return -1;
        }
    }

    public static InputStream GetApkAssetInputStream(String name) {
        try {
            if (TextUtils.isEmpty(name)) {
                return null;
            }
            AssetManager asset = MainApplication.getInstance().getBaseContext().getAssets();
            StringBuilder sb = new StringBuilder("gif/emotion/");
            return asset.open(sb.append(name).append(".gif").toString());
        } catch (Exception e) {
            PLog.e(TAG,"Resource get failed: "+name);
            return null;
        }
    }

}
