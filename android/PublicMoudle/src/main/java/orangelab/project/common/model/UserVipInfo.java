package orangelab.project.common.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * game_werewolf
 * 2018/7/17 下午4:41
 * Mystery
 */
public class UserVipInfo implements Keepable, Serializable {

    public boolean active = false;
    public String appType;
    public int lvl;
    public long nextExp;
    public String nameColor = "ff2e4d";
    public int left_days;
    public int exp;

}
