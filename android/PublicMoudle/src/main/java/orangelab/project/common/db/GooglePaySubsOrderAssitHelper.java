package orangelab.project.common.db;

import android.text.TextUtils;

import com.toolkit.action.Destroyable;

import orangelab.project.common.db.entity.GooglePaySubsOrderAssitEntity;
import orangelab.project.common.db.gen.GooglePaySubsOrderAssitEntityDao;

/**
 * game_werewolf
 * 2018/7/20 上午10:17
 * Mystery
 */
public class GooglePaySubsOrderAssitHelper implements Destroyable {

    private GooglePaySubsOrderAssitEntityDao mDao;

    public GooglePaySubsOrderAssitHelper(GooglePaySubsOrderAssitEntityDao dao) {
        mDao = dao;
    }

    public void insert(String orderId) {
        if (mDao == null) return;

        if (TextUtils.isEmpty(orderId)) return;

        GooglePaySubsOrderAssitEntity result = mDao.queryBuilder().where(GooglePaySubsOrderAssitEntityDao.Properties.OrderId.eq(orderId)).build().unique();
        if (result == null) {
            GooglePaySubsOrderAssitEntity entity = new GooglePaySubsOrderAssitEntity();
            entity.setOrderId(orderId);
            mDao.insert(entity);
        }

    }

    public void delete(String orderId) {
        if (mDao == null) return;

        if (TextUtils.isEmpty(orderId)) return;

        GooglePaySubsOrderAssitEntity result = mDao.queryBuilder().where(GooglePaySubsOrderAssitEntityDao.Properties.OrderId.eq(orderId)).build().unique();
        if (result != null) {
            mDao.delete(result);
        }
    }

    public boolean contains(String orderId) {
        if (mDao == null) return false;

        if (TextUtils.isEmpty(orderId)) return false;

        try {
            GooglePaySubsOrderAssitEntity entity = mDao.queryBuilder().where(GooglePaySubsOrderAssitEntityDao.Properties.OrderId.eq(orderId)).build().unique();
            if (entity != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void destroy() {
        mDao = null;
    }
}
