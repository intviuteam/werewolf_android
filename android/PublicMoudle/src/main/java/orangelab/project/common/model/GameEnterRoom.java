package orangelab.project.common.model;

/**
 * Author:1727
 * 2017/3/24
 */

public class GameEnterRoom {
    public String from = "";
    public String token;
    public String type;
    public String room_id;
    public String password;
    public GameEnterRoomItem user;
    public GameExtra extra;


    public static class GameEnterRoomItem {
        public String id;
        public String name;
        public String avatar;
        public int sex;
        public int level;
        public int experience;
    }

    public static class GameExtra {
        public String pt;
        public int v;
        public String lg;
        public int sv;
        public String tz;
        public String inviter;
        public String invitee;
    }
}
