package orangelab.project.common.utils;

import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * 记录分享的平台以及分享的方式
 * shareMedia：weixin/weixin_circle/qq/facebook/whatsapp/line
 * type:invite/share
 */

public enum ShareMedia {

    INSTANCE;

    private SHARE_MEDIA shareMedia;
    private String type;

    public SHARE_MEDIA getShareMediaType() {
        return shareMedia;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setShareMediaType(SHARE_MEDIA shareMedia) {
        this.shareMedia = shareMedia;
    }

    private ShareMedia() {

    }

    public static ShareMedia getShareMedia() {
        return INSTANCE;
    }
}
