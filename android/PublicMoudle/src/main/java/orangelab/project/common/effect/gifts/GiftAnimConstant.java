package orangelab.project.common.effect.gifts;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/11/6 下午5:57
 * Mystery
 */

public class GiftAnimConstant {

    public static final int MEMBER_ORIENTATION_LEFT = 0;//座位位于左边
    public static final int MEMBER_ORIENTATION_RIGHT = 1;//座位位于右边

    public static final int DEFAULT_GAUGE_LARGE = ScreenUtils.dip2px(16);
    public static final int DEFAULT_GAUGE_SMALL = ScreenUtils.dip2px(2);
    public static final int DEFAULT_CHAIR_WIDTH = ScreenUtils.dip2px(65);
    public static final int DEFAULT_CHAIR_HEIGHT = ScreenUtils.dip2px(75);


    public static final String LOCATION_FROMABOVE = "fromAbove";    //发起者 正上方
    public static final String LOCATION_FROMFRONT = "fromFront";    //发起者 正前方
    public static final String LOCATION_TOABOVE = "toAbove";        //接受者 正上方
    public static final String LOCATION_TOFRONT = "toFront";        //接受者 正前方

    public static final String LOCATION_SCREENTOPLEFT = "screenTopLeft";  //屏幕上边 左部
    public static final String LOCATION_SCREENTOPRIGHT = "screenTopRight"; //屏幕上边 右部
    public static final String LOCATION_SCREENTOPMID = "screenTopMid";//屏幕上边 中部

    public static final String LOCATION_SCREENLEFTTOP = "screenLeftTop";  //屏幕左边 上部
    public static final String LOCATION_SCREENLEFTMID = "screenLeftMid";//屏幕左边 中部
    public static final String LOCATION_SCREENLEFTBOTTOM = "screenLeftBottom";//屏幕左边 底部

    public static final String LOCATION_SCREENRIGHTTOP = "screenRightTop";  //屏幕右边 上部
    public static final String LOCATION_SCREENRIGHTMID = "screenRightMid";//屏幕右边 中部
    public static final String LOCATION_SCREENRIGHTBOTTOM = "screenRightBottom";//屏幕右边 下部

    public static final String LOCATION_SCREENBOTTOMLEFT = "screenBottomLeft";//屏幕低边 左部
    public static final String LOCATION_SCREENBOTTOMMID = "screenBottomMid";//屏幕低边 中部
    public static final String LOCATION_SCREENBOTTOMRIGHT = "screenBottomRight";//屏幕低边 右部

    public static final String LOCATION_SCREENLEFTTOPCORNER = "screenLeftTopCorner";  //屏幕左上角
    public static final String LOCATION_SCREENLEFTBOTTOMCORNER = "screenLeftBottomCorner";//屏幕左下角
    public static final String LOCATION_SCREENRIGHTTOPCORNER = "screenRightTopCorner";//屏幕右上角
    public static final String LOCATION_SCREENRIGHTBOTTOMCORNER = "screenRightBottomCorner";//屏幕右上角

    public static final String LOCATION_SCREENMID = "screenMid";//屏幕正中央

    public static final String INTERPOLATOR_LINEAR = "linear";   //线性
    public static final String INTERPOLATOR_ACCELERATE = "accelerate";  //加速
    public static final String INTERPOLATOR_DECELERATE = "decelerate";  //减速
    public static final String INTERPOLATOR_DECLERATEACCELERATE = "declerateAccelerate"; //先减速后加速
    public static final String INTERPOLATOR_ACCELERATEDECLERATE = "accelerateDeclerate";//先加速后减速

    public static final String TYPE_THREEPOINTANIM = "ThreePointAnim";
    public static final String TYPE_TWOPOINTANIM = "TwoPointAnim";
    public static final String TYPE_ONEPOINTANIM = "OnePointAnim";
    public static final String TYPE_TWOPOINT_LINE_ANIM = "TwoPointLineRotateAnim";  //两个点的新动画 路径为直线并且支持旋转

    public static final String GIFT_FROM_TYPE = "GIFT_FROM_TYPE";
    public static final String GIFT_FROM_TYPE_GAME = "GIFT_FROM_TYPE_GAME";
    public static final String GIFT_FROM_TYPE_VOICE = "GIFT_FROM_TYPE_VOICE";

    public static final float GIFT_REBATE_SCALE = 1.5f;
    public static final long GIFT_REBATE_DURATION = 1200;


}
