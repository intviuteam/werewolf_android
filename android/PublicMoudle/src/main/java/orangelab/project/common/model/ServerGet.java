package orangelab.project.common.model;

import orangelab.project.minigame.model.MiniGameTip;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2018/1/30 下午2:06
 * Mystery
 */

public class ServerGet implements Keepable {

    public String server;
    public String room_id = "";
    public String type;
    public String level;
    public boolean password_needed = false;
    public String password = "";
    public String extraData;
    public ServerGetMiniGame mini_game;
    public ArrayList<String> user_images;
    public MiniGameOpponent oppo;
    public ServerGetExtra extra;

    public static class ServerGetExtra implements Keepable {
        public String inviter;
        public String invitee;

    }

    public static class ServerGetMiniGame implements Keepable {
        public String url;
        public String download = "";
        public boolean landscape = false;
        public String icon;
        public String name;
        public ArrayList<MiniGameTip> tips;
        public String single_url = "";
    }

    public static class MiniGameOpponent implements Keepable {
        public String id;
        public String name;
        public String avatar;
        public int sex;
    }
}
