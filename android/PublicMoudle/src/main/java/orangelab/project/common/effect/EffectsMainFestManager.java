package orangelab.project.common.effect;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.datasource.cache.ACache;
import com.datasource.cache.DiskLruCacheManager;

import orangelab.project.common.effect.emotion.EmotionManiFest;
import orangelab.project.common.effect.headdecor.HeadDecorManiFest;
import orangelab.project.common.effect.manifest.EmotionManifestHolder;
import orangelab.project.common.effect.manifest.GiftsManifestHolder;
import orangelab.project.common.effect.manifest.HeadDecorManifestHolder;
import orangelab.project.common.effect.manifest.ManifestManagerHelper;
import orangelab.project.common.effect.manifest.MsgDecorManifestHolder;
import orangelab.project.common.effect.gifts.CustomGiftAnim;
import orangelab.project.common.exhibition.gift.model.GiftManifest;

import com.toolkit.ToolKit;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * game_werewolf
 * 2017/11/8 下午4:53
 * Mystery
 * <p>
 * <p>
 * 礼物清单管理器,这里为了做复用将
 */

public enum EffectsMainFestManager {

    aManager;

    public static final String TAG = "AnimationMainFest";

    private static final String ACACHE_NAME = ToolKit.GetHashKey("giftManifest");

    private static final String HEAD_DECOR_MANIFEST = ToolKit.GetHashKey("HeadDecorManiFest");
    private static final String HEAD_DECOR_MANIFEST_VERSION = ToolKit.GetHashKey("HeadDecorManiFestVersion");

    private static final String GIFT_MANIFEST_ALL = ToolKit.GetHashKey("giftAllManifest");
    private static final String GIFT_MANIFEST_VERSION = ToolKit.GetHashKey("giftAllManifestVersion");

    private static final String MSG_DECOR_MANIFEST = ToolKit.GetHashKey("MsgDecorManiFest");
    private static final String MSG_DECOR_MANIFEST_VERSION = ToolKit.GetHashKey("MsgDecorManiFestVersion");

    private static final String EMOTION_MANIFEST = ToolKit.GetHashKey("EmotionManiFest");
    private static final String EMOTION_MANIFEST_VERSION = ToolKit.GetHashKey("EmotionManiFestVersion");

    private ACache mManifest;

    private ManifestManagerHelper mHelper;

    public GiftsManifestHolder mGiftsManifestHolder;

    public HeadDecorManifestHolder mHeadDecorManifestHolder;

    public MsgDecorManifestHolder mMsgDecorManifestHolder;

    public EmotionManifestHolder mEmotionManifestHolder;

    /**
     * 设定一个MsgDecorCache
     */
    public Map<String, Bitmap> mMsgDecorCache;

    private EffectsMainFestManager() {
        mMsgDecorCache = new LinkedHashMap<>();
    }

    public void init(Context context) {

        mManifest = ACache.get(context, ACACHE_NAME);
        mHelper = new ManifestManagerHelper(mManifest);
        /**
         * 如下的初始化顺序不可以更改
         */

        /**
         * 礼物文件相关
         */
        mHelper.createDiskLruCache(context, DiskLruCacheManager.GIFT_IMAGE_CACHE);
        mGiftsManifestHolder = new GiftsManifestHolder(mHelper, GIFT_MANIFEST_ALL, GIFT_MANIFEST_VERSION, DiskLruCacheManager.GIFT_IMAGE_CACHE);

        /**
         * 头像框相关
         */
        mHelper.createDiskLruCache(context, DiskLruCacheManager.HEAD_DECOR_CACHE);
        mHeadDecorManifestHolder = new HeadDecorManifestHolder(mHelper, HEAD_DECOR_MANIFEST, HEAD_DECOR_MANIFEST_VERSION, DiskLruCacheManager.HEAD_DECOR_CACHE);

        /**
         * 聊天框相关
         */
        mHelper.createDiskLruCache(context, DiskLruCacheManager.MSG_DECOR_CACHE);
        mMsgDecorManifestHolder = new MsgDecorManifestHolder(mHelper, MSG_DECOR_MANIFEST, MSG_DECOR_MANIFEST_VERSION, DiskLruCacheManager.MSG_DECOR_CACHE);

        /**
         * 表情相关
         */
        mHelper.createDiskLruCache(context, DiskLruCacheManager.EMOTION_CACHE);
        mEmotionManifestHolder = new EmotionManifestHolder(mHelper, EMOTION_MANIFEST, EMOTION_MANIFEST_VERSION, DiskLruCacheManager.EMOTION_CACHE);
    }

    public int getEmotionVersion() {
        return mEmotionManifestHolder.getVersion();
    }

    public int getHeadDecorVersion() {
        return mHeadDecorManifestHolder.getVersion();
    }

    public int getGiftManifestVersion() {
        return mGiftsManifestHolder.getVersion();
    }

    public void removeHeadDecorVersion() {
        mHeadDecorManifestHolder.removeVersion();
    }

    public void removeHeadDecorManifest() {
        mHeadDecorManifestHolder.removeManifest();
    }

    public void removeGiftsManifestVersion() {
        mGiftsManifestHolder.removeVersion();
    }

    public void removeGiftManifest() {
        mGiftsManifestHolder.removeManifest();
    }

    public void removeEmotionManifestVersion() {
        mEmotionManifestHolder.removeVersion();
    }

    public void removeEmotionManifest() {
        mEmotionManifestHolder.removeManifest();
    }


    public void updateGiftManifest(GiftManifest giftManifest) {
        mGiftsManifestHolder.updateManifest(giftManifest);
    }

    public void updateHeadDecorManifest(HeadDecorManiFest maniFest) {
        mHeadDecorManifestHolder.updateManifest(maniFest);
    }

    public void updateEmotionManifest(EmotionManiFest maniFest) {
        mEmotionManifestHolder.updateManifest(maniFest);
    }

    public HeadDecorManiFest getHeadDecorManiFest() {
        return mHeadDecorManifestHolder.getManifestData();
    }

    public GiftManifest getGiftManifest() {
        return mGiftsManifestHolder.getManifestData();
    }

    public EmotionManiFest getEmotionManiFest() {
        return mEmotionManifestHolder.getManifestData();
    }

    public void downloadAndSaveHeadDecorManifestResource(HeadDecorManiFest maniFest, boolean forceRefresh) {
        mHeadDecorManifestHolder.downloadManifestResource(maniFest, forceRefresh);
    }

    public void downloadAndSaveGiftsManifestResource(GiftManifest manifest, boolean forceRefresh) {
        mGiftsManifestHolder.downloadManifestResource(manifest, forceRefresh);
    }

    public void downloadAndSaveEmotionManifestResource(EmotionManiFest maniFest, boolean forceRefresh) {
        mEmotionManifestHolder.downloadManifestResource(maniFest, forceRefresh);
    }

    public Set<String> verifyHeadDecorManifestData(HeadDecorManiFest data) {
        return mHeadDecorManifestHolder.verifyManifest(data);
    }

    public Set<String> verifyGiftDecorManifestData(GiftManifest data) {
        return mGiftsManifestHolder.verifyManifest(data);
    }

    public Set<String> verifyEmotionManifestData(EmotionManiFest data) {
        return mEmotionManifestHolder.verifyManifest(data);
    }

    public void saveGiftManifest(GiftManifest giftManifest) {
        mGiftsManifestHolder.saveManifest(giftManifest);
    }

    public void saveHeadDecorManifest(HeadDecorManiFest headDecorManiFest) {
        mHeadDecorManifestHolder.saveManifest(headDecorManiFest);
    }

    public void saveEmotionManifest(EmotionManiFest emotionManiFest) {
        mEmotionManifestHolder.saveManifest(emotionManiFest);
    }

    public static InputStream GetGiftImageInputStream(String url) {
        return aManager.mGiftsManifestHolder.getInputStream(url);
    }

    public static InputStream GetHeadDecorImageInputStream(String url) {
        return aManager.mHeadDecorManifestHolder.getInputStream(url);
    }

    public static InputStream GetEmotionInputStream(String url) {
        return aManager.mEmotionManifestHolder.getInputStream(url);
    }

    public static void FillImageViewByGiftImageCacheUrl(ImageView imageView, String local, String remote) {
        aManager.mGiftsManifestHolder.fillImageView(imageView, local, remote);
    }

    public static void Init(Context context) {
        aManager.init(context);
    }

    public static HeadDecorManiFest.HeadDecorManiFestItem FindHeadDecorManiFestItem(String decorType) {
        return aManager.mHeadDecorManifestHolder.getManiFestItem(decorType);
    }

    public static CustomGiftAnim FindGiftAnimFromGiftManifest(String giftType) {
        return aManager.mGiftsManifestHolder.getManifestItemAnim(giftType);
    }

    public static String GetImageCacheFilePath(String url) {
        return aManager.mGiftsManifestHolder.getCacheFileDir(url);
    }

    public static void PutIntoCache(String key, Bitmap bitmap) {
        aManager.mMsgDecorCache.put(key, bitmap);
    }

    public static Bitmap GetDrawableFromCache(String key) {
        return aManager.mMsgDecorCache.get(key);
    }

    /**
     * 每次退出房间要清理 聊天框的缓存
     */
    public static void ClearCache() {
        aManager.mMsgDecorCache.clear();
    }

}
