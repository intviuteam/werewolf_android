package orangelab.project.common.db.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/5/22 下午2:05
 * Mystery
 */
public class MiniGameAiRecord implements Keepable {

    public int ai_level;
    public int winning_streak;

    @Override
    public String toString() {
        return "MiniGameAiRecord{" +
                "api_level=" + ai_level +
                ", winning_streak=" + winning_streak +
                '}';
    }
}
