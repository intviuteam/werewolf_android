package orangelab.project.common.db;

import android.text.TextUtils;

import com.toolkit.action.Destroyable;

import java.util.List;
import java.util.UUID;

import orangelab.project.common.db.entity.MiniGameRecordEntity;
import orangelab.project.common.db.gen.MiniGameRecordEntityDao;


/**
 * game_werewolf
 * 2018/5/17 下午4:45
 * Mystery
 */
public class MiniGameRecordDaoHelper implements Destroyable {

    private MiniGameRecordEntityDao mMiniGameRecordEntityDao;

    public MiniGameRecordDaoHelper(MiniGameRecordEntityDao miniGameRecordEntityDao) {
        mMiniGameRecordEntityDao = miniGameRecordEntityDao;
    }

    /**
     * 添加一条单机小游戏的记录
     *
     * @param userToken
     * @param userId
     * @param gameType
     * @param gameScoreValue
     * @param gameCostTime
     * @return
     */
    public String addMiniGameRecord(String userToken, String userId, String gameType, long gameScoreValue, long gameCostTime) {
        if (TextUtils.isEmpty(userId)) {
            return "";
        }
        if (TextUtils.isEmpty(userToken)) {
            return "";
        }
        String recordId = UUID.randomUUID().toString();
        MiniGameRecordEntity entity = new MiniGameRecordEntity();

        entity.setUserId(userId);
        entity.setUserToken(userToken);

        entity.setGameType(gameType);

        entity.setGameScoreRecordValue(gameScoreValue);
        entity.setGameCostTime(gameCostTime);

        entity.setGameRecordId(recordId);

        if (mMiniGameRecordEntityDao != null) {
            mMiniGameRecordEntityDao.insert(entity);
        }
        return recordId;
    }

    public void removeRecord(MiniGameRecordEntity entity) {
        if (mMiniGameRecordEntityDao != null) {
            mMiniGameRecordEntityDao.delete(entity);
        }
    }

    public void removeMiniGameRecord(String userId, String gameType, String gameRecordId) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (mMiniGameRecordEntityDao != null) {
            MiniGameRecordEntity entity =
                    mMiniGameRecordEntityDao.queryBuilder().where(
                            MiniGameRecordEntityDao.Properties.UserId.eq(userId),
                            MiniGameRecordEntityDao.Properties.GameRecordId.eq(gameRecordId),
                            MiniGameRecordEntityDao.Properties.GameType.eq(gameType)).build().unique();
            if (entity != null) {
                removeRecord(entity);
            }
        }
    }

    /**
     * 获取用户的所有成绩
     *
     * @param userId
     * @return
     */
    public List<MiniGameRecordEntity> getMiniGameReocrd(String userId) {
        if (TextUtils.isEmpty(userId)) {
            return null;
        }
        if (mMiniGameRecordEntityDao != null) {
            List<MiniGameRecordEntity> miniGameRecordEntities =
                    mMiniGameRecordEntityDao.queryBuilder().where(
                            MiniGameRecordEntityDao.Properties.UserId.eq(userId)
                    ).build().list();
            return miniGameRecordEntities;
        }
        return null;
    }

    /**
     * 获取用户的某个游戏的成绩
     *
     * @param userId
     * @param gameType
     * @return
     */
    public List<MiniGameRecordEntity> getMiniGameRecord(String userId, String gameType) {
        if (TextUtils.isEmpty(userId)) {
            return null;
        }
        if (mMiniGameRecordEntityDao != null) {
            List<MiniGameRecordEntity> miniGameRecordEntities =
                    mMiniGameRecordEntityDao.queryBuilder().where(
                            MiniGameRecordEntityDao.Properties.UserId.eq(userId),
                            MiniGameRecordEntityDao.Properties.GameType.eq(gameType)
                    ).build().list();
            return miniGameRecordEntities;
        }
        return null;
    }

    @Override
    public void destroy() {
        if (mMiniGameRecordEntityDao != null) {
            mMiniGameRecordEntityDao = null;
        }
    }
}
