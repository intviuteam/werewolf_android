package orangelab.project.common.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.UpgradeActivity;
import orangelab.project.common.upgrade.IUpgradeServiceDefines;

import com.networktoolkit.transport.Constant;

import cn.intviu.sdk.model.AppUpgradeInfo;
import cn.intviu.support.AppUtil;

/**
 * Created by wangxu on 2017/4/7.
 * 作用：用与更新应用的notification
 */

public class UpgradeAppNotificationReceiver extends BroadcastReceiver implements IUpgradeServiceDefines {
    /**
     * 用于下载更新时的通知ID
     */
    private int mUpDateApkNotificationId = "ACTION_UPDATE_APK".hashCode();

    @Override
    public void onReceive(Context context, Intent intent) {
        int type = intent.getIntExtra(EXTRA_NOTIFICATION_TYPE, 0);
        switch (type) {
            case ACTION_BEGIN_DOWNLOAD:
                /**
                 * 通知下载开始
                 */
                beginDownLoadApk(context, intent);
                break;
            case ACTION_DOWNLOAD_PROCESS:
                /**
                 * 更新进度通知
                 */
                updateLoadingProcess(context, intent);
                break;
            case ACTION_FINISH_DOWNLOAD:
                /**
                 * 下载完成通知，进行安装Apk操作
                 */
                finishDownload(context, intent);
                break;
            case ACTION_DOWNLOAD_ERROR:
                /**
                 * 下载失败通知，进行错误处理操作
                 */
                downloadError(context, intent);
                break;
            case ACTION_CHECK_UPDATE:
                /**
                 * 检查更新操作，如果需要更新则进入更新页面，如果不需要更新则什么也不做
                 */
                checkUpdate(context, intent);
                break;
        }
    }

    private synchronized void finishDownload(Context context, Intent intent) {
        String path = intent.getStringExtra(DOWNLOAD_PATH);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(mUpDateApkNotificationId);
        if (!TextUtils.isEmpty(path))
            AppUtil.installApk(context, path);
    }

    private synchronized void updateLoadingProcess(Context context, Intent intent) {
        int process = intent.getIntExtra(PROCESS, 0);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(ProjectConfig.APP_ICON);
        Notification notification = mBuilder.build();
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.item_downloadapk_notificaion);
        contentView.setImageViewResource(R.id.iv_logo_icon, ProjectConfig.APP_ICON);
        contentView.setTextViewText(R.id.tv_title, context.getString(R.string.title_downloading));
        contentView.setProgressBar(R.id.progressbar, 100, process, false);
        notification.contentView = contentView;
        notification.flags = Notification.FLAG_NO_CLEAR;
        notification.when = System.currentTimeMillis();
        notificationManager.notify(mUpDateApkNotificationId, notification);
    }

    private synchronized void beginDownLoadApk(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(ProjectConfig.APP_ICON);
        Notification notification = mBuilder.build();
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.item_downloadapk_notificaion);
        contentView.setImageViewResource(R.id.iv_logo_icon, ProjectConfig.APP_ICON);
        contentView.setTextViewText(R.id.tv_title, context.getString(R.string.title_downloading));
        contentView.setProgressBar(R.id.progressbar, 100, 0, false);
        notification.contentView = contentView;
        notification.flags = Notification.FLAG_NO_CLEAR;
        notification.when = System.currentTimeMillis();
        notificationManager.notify(mUpDateApkNotificationId, notification);
    }

    private synchronized void checkUpdate(Context context, Intent intent) {
        AppUpgradeInfo.AppUpgradeDetail detail = (AppUpgradeInfo.AppUpgradeDetail) intent.getSerializableExtra(UPDATE_DETAIL);
        boolean initiative_check = intent.getBooleanExtra(Constant.INITIATIVE_CHECK, false);
        if (detail.update_level == 0) {
            /*为0代表不需要更新*/
            if (initiative_check) {
                Toast.makeText(context, R.string.upgrade_already_is_newest, Toast.LENGTH_SHORT).show();
            }
        } else {
            UpgradeActivity.checkUpgrade(context, detail);
        }
    }

    private synchronized void downloadError(Context context, Intent intent) {
        int errorCode = intent.getIntExtra(ERROR_CODE, ERROR_CDOE_NETWORK);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(mUpDateApkNotificationId);
        switch (errorCode) {
            case ERROR_CODE_CHECK_FILE:
                Toast.makeText(context, R.string.action_download_error_md5, Toast.LENGTH_SHORT).show();
                break;
            case ERROR_CDOE_NETWORK:
                Toast.makeText(context, R.string.action_download_error, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
