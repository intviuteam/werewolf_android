package orangelab.project.common.effect.view;

import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.FrameLayout;

import com.androidtoolkit.PLog;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.effect.event.RoomEffectEvent;

/**
 * game_werewolf
 * 2018/7/24 下午4:42
 * Mystery
 * <p>
 * 用于房间内小游戏
 */
class RoomEffectsGameViewGroup extends FrameLayout implements Destroyable {

    private static final String TAG = "RoomEffectsGameView";

    private SparseArray<RoomMemberEffectsView> mGameViewSparseArray;
    private RoomEffectsContext mRoomEffectsContext;

    public RoomEffectsGameViewGroup(@NonNull Context context) {
        this(context, null);
    }

    public RoomEffectsGameViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomEffectsGameViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        EventBus.getDefault().register(this);
        setClipChildren(false);
        mGameViewSparseArray = new SparseArray<>();
    }

    public void bindEffectsContext(RoomEffectsContext context) {
        mRoomEffectsContext = context;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomEffectClear(RoomEffectEvent.RoomEffectClear event) {
        int position = event.position;
        RoomMemberEffectsView view = mGameViewSparseArray.get(position);
        if (view != null) {
            removeView(view);
        }
        mGameViewSparseArray.remove(position);
        PLog.i(TAG, "Clear Effect position = " + position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomEffectEvent(RoomEffectEvent.RoomMemberEffect event) {
        RoomMemberEffectsView memberEffectsView = mGameViewSparseArray.get(event.position);

        if (memberEffectsView != null) {
            /**
             * 已经添加过这个View
             */

        } else {

            /**
             * 没有添加过这个View
             */
            memberEffectsView = new RoomMemberEffectsView(getContext());
            memberEffectsView.bindEffectsContext(mRoomEffectsContext);
            memberEffectsView.setPosition(event.position);
            PointF pointF = event.point;
            memberEffectsView.setX(pointF.x);
            memberEffectsView.setY(pointF.y);
            LayoutParams layoutParams = new LayoutParams(event.width, event.height);
            memberEffectsView.setLayoutParams(layoutParams);
            memberEffectsView.updateWidthAndHeight(event.width, event.height);
            addView(memberEffectsView);
            mGameViewSparseArray.put(event.position, memberEffectsView);
        }

        memberEffectsView.handleRoomEffectsEvent(event);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    @Override
    public void destroy() {
        EventBus.getDefault().unregister(this);
    }
}
