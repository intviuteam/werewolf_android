package orangelab.project.common.dialog;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.ToastToolKit;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.common.family.FamilyInfoActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.FamilyTools;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.game.config.GameRoomConfig;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.FlowLayout;
import orangelab.project.common.view.UserHeadView;

import com.networktoolkit.transport.Constant;

import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * Author:1727
 * 2017/3/22
 */

public class UserDetailDialog extends SafeDialog implements View.OnClickListener {

    private static final String TAG = "UserDetailDialog";
    private final int dialogMaxWidth = ScreenUtils.dip2px(345);

    private Context context;
    private View outSideContainer;
    /**
     * 举报
     */
    private View reportContainer;
    private View reportView;
    /**
     * 个人详情信息部分
     */
    private View userDetailContainer;
    private UserHeadView mHeadView;
    private TextView userName;
    private TextView userLevel;
    private TextView userPopular;
    private FlowLayout userRoles;
    private TextView userUid;
    /**
     * 家族信息部分
     */
    private View mFamilyContainer;
    private ImageView mFamilyBadge;
    private View mFamilyPlaceHolder;
    private View mFamilyArrow;
    private TextView mFamilyName;

    /**
     * 战绩部分
     */
    private TextView userRate;
    private TextView userWins;
    private TextView userLoses;
    private TextView userEscapeRate;

    /**
     * 按钮部分
     */
    private Button userAddPage;
    private Button userKickPage;
    private Button userClosePage;
    private Button userGiveGift;
    private View userButtonPlaceHolder;


    private int selectPosition = -1;
    private String mUserId;
    private String mUserName;
    private String mUserIv;
    private boolean mIsRoomOwner = false;
    private boolean mIsGaming = false;
    private int mUserSex;
    private boolean canSendGift = true;


    public UserDetailDialog(Context context,
                            boolean isRoomOwner,
                            String userIdString,
                            int position,
                            boolean isGaming
    ) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.layout_user_detail_dialog);
        this.context = context;
        mUserId = userIdString;
        selectPosition = position;
        mIsRoomOwner = isRoomOwner;
        mIsGaming = isGaming;
        initView();
        handleUserInfo();
        initDialogParams();
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    private void initView() {
        initTitle();
        initUserDetailView();
        initFamilyView();
        initCombatView();
        initButtonView();
    }

    private void initTitle() {
        outSideContainer = findViewById(R.id.touch_outside);
        outSideContainer.setOnClickListener(this);
        reportView = findViewById(R.id.user_report_btn);
        reportContainer = findViewById(R.id.user_report);
        reportView.setOnClickListener(this);
        reportView.setVisibility(View.GONE);
        reportContainer.setVisibility(View.VISIBLE);
    }

    private void initUserDetailView() {
        userDetailContainer = findViewById(R.id.user_detail_container);
        mHeadView = (UserHeadView) findViewById(R.id.user_head_view);
        userName = (TextView) findViewById(R.id.user_name);
        userName.setText("");
        userLevel = (TextView) findViewById(R.id.user_level);
        userLevel.setText("");
        userPopular = (TextView) findViewById(R.id.user_popular);
        userPopular.setVisibility(View.GONE);
        userRoles = (FlowLayout) findViewById(R.id.id_user_detail_flowlayout);
        userRoles.setVisibility(View.GONE);
        userUid = (TextView) findViewById(R.id.user_info_uid);
    }

    private void initFamilyView() {
        mFamilyContainer = findViewById(R.id.id_user_detail_dialog_container);
        mFamilyName = (TextView) findViewById(R.id.id_user_detail_dialog_family_name);
        mFamilyArrow = findViewById(R.id.id_user_detail_dialog_arrow);
        mFamilyBadge = (ImageView) findViewById(R.id.id_user_detail_dialog_badge);
        mFamilyPlaceHolder = findViewById(R.id.id_user_detail_dialog_family_placeholder);
        setHasFamily(false);
    }

    private void initCombatView() {
        userRate = (TextView) findViewById(R.id.user_win_rate);
        userRate.setText("0%");
        userWins = (TextView) findViewById(R.id.wins);
        userWins.setText("0");
        userLoses = (TextView) findViewById(R.id.lose);
        userLoses.setText("0");
        userEscapeRate = (TextView) findViewById(R.id.user_escape_rate);
        userEscapeRate.setText("0%");
    }

    private void initButtonView() {
        userButtonPlaceHolder = findViewById(R.id.user_button_placeholder);
        userAddPage = (Button) findViewById(R.id.user_add_page);
        userKickPage = (Button) findViewById(R.id.user_kick_page);
        userClosePage = (Button) findViewById(R.id.user_close_page);
        userGiveGift = (Button) findViewById(R.id.user_give_gift);
        userAddPage.setOnClickListener(this);
        userKickPage.setOnClickListener(this);
        userClosePage.setOnClickListener(this);
        userGiveGift.setOnClickListener(this);
        userGiveGift.setVisibility(View.GONE);
        userAddPage.setVisibility(View.GONE);
        userKickPage.setVisibility(View.GONE);
        userClosePage.setVisibility(View.GONE);
        userButtonPlaceHolder.setVisibility(View.GONE);
    }

    private boolean canCloseSit() {
        return GameRoomConfig.CloseSitEnable(null);
    }

    public void setCanSendGift(boolean canSendGift) {
        this.canSendGift = canSendGift;
    }

    private void initGiftButton() {
        if (Utils.checkUserIdIsTourist(mUserId)) {
            userGiveGift.setOnClickListener(v -> {
                if (GlobalUserState.getGlobalState().isTourist()) {
                    Toast.makeText(context, R.string.gifts_preset_from_tourist, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, R.string.gifts_preset_to_tourist, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userDetailContainer.setOnClickListener(this);
            userGiveGift.setVisibility(View.VISIBLE);
            if (GlobalUserState.getGlobalState().isTourist()) {
                userGiveGift.setVisibility(View.GONE);
                return;
            }
        }
        if (canSendGift) {
            if (!TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
                userGiveGift.setVisibility(View.VISIBLE);
            } else {
                userGiveGift.setVisibility(View.VISIBLE);
            }
        } else {
            userGiveGift.setVisibility(View.GONE);
        }
        try {
            if (PersonalDataHelper.getPersonalData().isDead()) {
                userGiveGift.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            /**
             * ignore
             */
        }
    }

    private void initDialogParams() {
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    public void setHasFamily(boolean isHas) {
        if (isHas) {
            mFamilyArrow.setVisibility(View.VISIBLE);
            mFamilyBadge.setVisibility(View.VISIBLE);
            mFamilyPlaceHolder.setVisibility(View.GONE);
            mFamilyName.setVisibility(View.VISIBLE);
            mFamilyContainer.setOnClickListener(this);
        } else {
            mFamilyArrow.setVisibility(View.GONE);
            mFamilyBadge.setVisibility(View.GONE);
            mFamilyPlaceHolder.setVisibility(View.VISIBLE);
            mFamilyName.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.user_add_page) {
            ApiManager.AddFriendApi(mUserId, null, null, null);
        } else if (id == R.id.user_kick_page) {
            RoomSocketEngineHelper.sendPositionMessage(Constant.KICK_OUT, selectPosition);
            dismissUserDetailDialog();
        } else if (id == R.id.user_close_page) {
            RoomSocketEngineHelper.sendPositionMessage(Constant.LOCK, selectPosition);
            dismissUserDetailDialog();
        } else if (id == R.id.user_give_gift) {
            if (selectPosition == -1) {
                ExhibitionsActivity.startActivityFromOutSide(context, mUserId, mUserName, mUserSex, mUserIv);
            } else {
                ExhibitionsActivity.startActivityFromGame(context, mUserId, mUserName, mUserSex, mUserIv, selectPosition);
            }
            dismissUserDetailDialog();
        } else if (id == R.id.user_detail_container) {
            if (selectPosition == -1) {
                handUserMainPageFromOutSide();
            } else {
                handUserMainPage();
            }
            dismissUserDetailDialog();
        } else if (id == R.id.touch_outside) {

        } else if (id == R.id.user_report_btn) {
            ReportDialog reportDialog = new ReportDialog(getContext(), mUserId, mUserName);
            reportDialog.show();
        }
//        switch (v.getId()) {
//            case R.id.user_add_page:
//                ApiManager.AddFriendApi(mUserId, null, null, null);
//                break;
//            case R.id.user_kick_page:
//                GameHelper.sendPositionMessage(Constant.KICK_OUT, selectPosition);
//                dismissUserDetailDialog();
//                break;
//            case R.id.user_close_page:
//                GameHelper.sendPositionMessage(Constant.LOCK, selectPosition);
//                dismissUserDetailDialog();
//                break;
//            case R.id.user_give_gift:
//                if (selectPosition == -1) {
//                    ExhibitionsActivity.startActivityFromOutSide(context, mUserId, mUserName, mUserSex, mUserIv);
//                } else {
//                    ExhibitionsActivity.startActivityFromGame(context, mUserId, mUserName, mUserSex, mUserIv, selectPosition);
//                }
//                dismissUserDetailDialog();
//                break;
//            case R.id.user_detail_container:
//                if (selectPosition == -1) {
//                    handUserMainPageFromOutSide();
//                } else {
//                    handUserMainPage();
//                }
//                dismissUserDetailDialog();
//                break;
//            case R.id.touch_outside:
//                break;
//            case R.id.user_report_btn: {
//                ReportDialog reportDialog = new ReportDialog(getContext(), mUserId, mUserName);
//                reportDialog.show();
//            }
//            break;
//        }
    }

    private void handUserMainPage() {
        PersonalInfoActivity.Launch(context, mUserId);
    }

    private void handUserMainPageFromOutSide() {
        PersonalInfoActivity.Launch(context, mUserId, true);
    }

    private void handleUserInfo() {
        ApiManager.GetUserInfoApi(mUserId, (userInfoResult, e) -> {
            runSafely(() -> {
                if (e == null) {
                    handleUserInfoResult(userInfoResult);
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void handleUserInfoResult(UserInfoResult result) {
        try {
            initReportButton();
            handleUserDetail(result);
            handleFamilyDetail(result);
            handleCombatDetail(result);
            handleButtonDetail(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleButtonDetail(UserInfoResult result) {
        try {
            /**
             * 判断加好友按钮是否显示
             */
            if (!result.is_friend) {
                if (selectPosition != -1) {
                    if (PersonalDataHelper.getPersonalData().getSelfPosition() == selectPosition) {
                        userAddPage.setVisibility(View.GONE);
                    } else {
                        userAddPage.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
                        userAddPage.setVisibility(View.GONE);
                    } else {
                        userAddPage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                userAddPage.setVisibility(View.GONE);
            }
            /**
             * 判断座位操作按钮是否可以显示
             */
            if (mIsRoomOwner) {
                if (mIsGaming) {
                    userKickPage.setVisibility(View.GONE);
                    userClosePage.setVisibility(View.GONE);
                } else {
                    userKickPage.setVisibility(View.VISIBLE);
                    if (!canCloseSit()) {
                        userClosePage.setVisibility(View.GONE);
                    } else {
                        userClosePage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                userKickPage.setVisibility(View.GONE);
                userClosePage.setVisibility(View.GONE);
            }

            /**
             * 送礼物页面
             */
            initGiftButton();

            /**
             * 按钮UI美化设置
             */
            showGiftPlaceHolderIfNeed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleFamilyDetail(UserInfoResult result) {
        if (result.group != null) {
            setHasFamily(true);

            FamilyTools.FillFamilyBadge(getContext(), result.group, mFamilyBadge);

            mFamilyName.setText(MessageUtils.getString(R.string.string_family_detail_dialog_shortname, result.group.short_name));
            mFamilyContainer.setOnClickListener(v -> {
                FamilyInfoActivity.Launch(getContext(), result.group.group_id);
                dismiss();
            });
            if (TextUtils.equals(result.id, GlobalUserState.getGlobalState().getUserId())) {
                FamilyMemo.UpdateMemo(result.group);
            }
        } else {
            setHasFamily(false);
        }
    }

    private void handleCombatDetail(UserInfoResult result) {
        float total = (result.game.lose + result.game.win + result.game.escape);
        float rate = total > 0 ? (result.game.win / (total)) * 100f : 0;
        float escapeRate = total > 0 ? (result.game.escape / (total)) * 100f : 0;
        String rateString = new BigDecimal(rate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        String escapeRateString = new BigDecimal(escapeRate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        userRate.setText(rateString + "%");
        userEscapeRate.setText(escapeRateString + "%");
        userWins.setText(Integer.toString(result.game.win));
        userLoses.setText(Integer.toString(result.game.lose));
    }

    private void handleUserDetail(UserInfoResult result) {
        mUserSex = result.sex;
        mUserName = result.name;
        mUserIv = result.image;
        mHeadView.setUserSex(result.sex);
        mHeadView.setUserHeadImageUrl(result.image);
        userName.setText(result.name);
        userLevel.setText(MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(result.game.level)));
        try {
            userPopular.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(result.popular)));
            userPopular.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (result.uid == null) {
                userUid.setVisibility(View.GONE);
            } else {
                userUid.setVisibility(View.VISIBLE);
                userUid.setText(MessageUtils.getString(R.string.string_user_info_uid, Integer.toString(result.uid)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<String> roleData = new ArrayList<>();
            if (result.role != null && result.role.customs != null && result.role.customs.size() > 0) {
//                if (result.role.type == 1) {
//                    /**
//                     * 管理员
//                     */
//                    roleData.add(MessageUtils.getString(R.string.user_role_manager));
//                }
//                if (result.role.type == 2) {
//                    /**
//                     * 老师
//                     */
//                    roleData.add(MessageUtils.getString(R.string.user_role_teacher));
//                }
//                if (result.role.type == 3) {
//                    /**
//                     * 管理员&老师，但是只展示一个
//                     */
//                    roleData.add(MessageUtils.getString(R.string.user_role_manager));
//                    roleData.add(MessageUtils.getString(R.string.user_role_teacher));
//                }
//                if (!TextUtils.isEmpty(result.role.custom)) {
//                    roleData.add(result.role.custom);
//                }
                roleData.addAll(result.role.customs);
            }
            if (roleData.size() > 0) {
                userRoles.setVisibility(View.VISIBLE);
                userRoles.addTextViewsWithVipInfo(roleData, Color.parseColor("#2d1c4c"), 10, true,result.vipInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initReportButton() {
        if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
            reportView.setVisibility(View.GONE);
            reportContainer.setVisibility(View.VISIBLE);
        } else {
            reportView.setVisibility(View.VISIBLE);
            reportContainer.setVisibility(View.VISIBLE);
        }
    }

    private void showGiftPlaceHolderIfNeed() {
        if (userAddPage.getVisibility() == View.VISIBLE && userGiveGift.getVisibility() == View.VISIBLE) {
            userButtonPlaceHolder.setVisibility(View.VISIBLE);
        } else {
            userButtonPlaceHolder.setVisibility(View.GONE);
        }
    }

    public void showUserDetailDialog() {
        if (!isShowing())
            show();
    }

    public void dismissUserDetailDialog() {
        if (isShowing())
            dismiss();
    }

    @Override
    protected void release() {
    }
}
