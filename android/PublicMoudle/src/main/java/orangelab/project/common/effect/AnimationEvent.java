package orangelab.project.common.effect;

/**
 * game_werewolf
 * 2017/6/27 下午1:55
 * Mystery
 */

public class AnimationEvent {

    public static class AnimationTaskEvent {

        private EffectsTaskManager.Task mTask;

        public AnimationTaskEvent(EffectsTaskManager.Task mTask) {
            this.mTask = mTask;
        }

        public EffectsTaskManager.Task getTask() {
            return mTask;
        }
    }

    public static class RemainAnimationTaskAddEvent {

        private EffectsTaskManager.RemainTask mRemainTask;

        public RemainAnimationTaskAddEvent(EffectsTaskManager.RemainTask remainTask) {
            mRemainTask = remainTask;
        }

        public EffectsTaskManager.RemainTask getRemainTask() {
            return mRemainTask;
        }
    }

    public static class RemainAnimationTaskRemoveEvent {

        private int position = -1;

        public RemainAnimationTaskRemoveEvent(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }
}
