package orangelab.project.common.exception;

/**
 * game_werewolf
 * 2017/10/25 下午2:35
 * Mystery
 */

public class GoToException extends Exception {

    public GoToException() {
        super("This is a GoTo action");
    }
}
