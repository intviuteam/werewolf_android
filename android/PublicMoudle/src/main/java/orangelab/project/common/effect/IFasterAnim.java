package orangelab.project.common.effect;

/**
 * game_werewolf
 * 2017/11/7 下午5:52
 * Mystery
 */

public interface IFasterAnim {

    void start();

    void stop();

    void release();

}
