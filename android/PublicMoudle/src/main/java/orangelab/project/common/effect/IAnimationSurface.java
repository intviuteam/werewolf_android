package orangelab.project.common.effect;

/**
 * game_werewolf
 * 2017/6/23 下午2:37
 * Mystery
 */

public interface IAnimationSurface extends IBaseAnimationSurface {

    public void destroy();

    public boolean hasCard(String type);

    public void userCard(String type, boolean isDisposable);

    public void refreshCard();
}
