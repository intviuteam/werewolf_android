package orangelab.project.common.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

/**
 * Created by wangxu on 2017/6/26.
 */

public class CheckApkExist {
    private static String facebookPkgName = "com.facebook.katana";

    public static boolean checkApkExist(Context context, String packageName) {
        if (TextUtils.isEmpty(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkFacebookExist(Context context) {
        return checkApkExist(context, facebookPkgName);
    }
}

