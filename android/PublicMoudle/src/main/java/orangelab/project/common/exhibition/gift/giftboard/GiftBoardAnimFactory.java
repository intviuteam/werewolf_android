package orangelab.project.common.exhibition.gift.giftboard;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.PointF;
import android.view.View;

/**
 * game_werewolf
 * 2017/11/9 下午6:09
 * Mystery
 */

public class GiftBoardAnimFactory {

    public static Animator CreateAlphaOut(View targetView, float scale) {
        ObjectAnimator animatorAlphaOut = ObjectAnimator.ofFloat(targetView, "alpha", 0, 1);
        animatorAlphaOut.setDuration(200);
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 0f, scale);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 0f, scale);
        Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animatorAlphaOut).with(animatorScale);
        return animatorSet;
    }

    public static Animator CreateAlphaIn(View targetView, float scale) {
        ObjectAnimator animatorAlphaIn = ObjectAnimator.ofFloat(targetView, "alpha", 1, 0);
        animatorAlphaIn.setDuration(200);
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", scale, 0f);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", scale, 0f);
        Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animatorAlphaIn).with(animatorScale);
        return animatorSet;
    }

    public static Animator CreatePathAnimator(View targetView, PointF start, PointF end) {
        PropertyValuesHolder pvhXl = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, start.x, end.x);
        PropertyValuesHolder pvhYl = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, start.y, end.y);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhXl, pvhYl).setDuration(300);
        return animator;
    }
}
