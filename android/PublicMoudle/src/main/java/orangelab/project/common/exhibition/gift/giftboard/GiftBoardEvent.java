package orangelab.project.common.exhibition.gift.giftboard;

import orangelab.project.game.component.GameMember;

/**
 * game_werewolf
 * 2017/11/9 下午5:42
 * Mystery
 */

public class GiftBoardEvent {

    private GameMember.GameMemberData mFromData;
    private GameMember.GameMemberData mToData;
    private String giftType;

    public GiftBoardEvent(GameMember.GameMemberData fromData, GameMember.GameMemberData toData, String giftType) {
        mFromData = fromData;
        mToData = toData;
        this.giftType = giftType;
    }

    public GameMember.GameMemberData getFromData() {
        return mFromData;
    }

    public GameMember.GameMemberData getToData() {
        return mToData;
    }

    public String getGiftType() {
        return giftType;
    }
}
