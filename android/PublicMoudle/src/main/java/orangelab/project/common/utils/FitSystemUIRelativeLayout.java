package orangelab.project.common.utils;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import orangelab.project.common.manager.AppManager;

import org.greenrobot.greendao.annotation.NotNull;

/**
 * game_werewolf
 * 2018/1/26 下午7:00
 * Mystery
 */

public class FitSystemUIRelativeLayout extends RelativeLayout {
    private int[] mInsets = new int[4];

    public FitSystemUIRelativeLayout(Context context) {
        super(context);
    }

    public FitSystemUIRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FitSystemUIRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected final boolean fitSystemWindows(@NotNull Rect insets) {
        if (AppManager.GreaterLollipop()) {
            // Intentionally do not modify the bottom inset. For some reason,
            // if the bottom inset is modified, window resizing stops working.
            // TODO: Figure out why.

            mInsets[0] = insets.left;
            mInsets[1] = insets.top;
            mInsets[2] = insets.right;

            insets.left = 0;
            insets.top = 0;
            insets.right = 0;
        }

        return super.fitSystemWindows(insets);
    }
}
