package orangelab.project.common.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

/**
 * game_werewolf
 * 2017/10/19 上午11:47
 * Mystery
 */

public class RecycleViewClickListener implements View.OnTouchListener {

    private int mLastDownX, mLastDownY;
    private int touchSlop;
    //是否是单击事件
    private boolean isSingleTapUp = false;
    //是否是长按事件
    private boolean isLongPressUp = false;
    private boolean isMove = false;
    private boolean dispatchChild = true;
    private long mDownTime;

    private View.OnClickListener mOnClickListener;
    private View.OnLongClickListener mOnLongClickListener;

    public RecycleViewClickListener(Context context) {
        touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public void setDispatchChild(boolean dispatchChild) {
        this.dispatchChild = dispatchChild;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        mOnLongClickListener = onLongClickListener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent e) {
        int x = (int) e.getX();
        int y = (int) e.getY();
        switch (e.getAction()) {
            /**
             *  如果是ACTION_DOWN事件，那么记录当前按下的位置，
             *  记录当前的系统时间。
             */
            case MotionEvent.ACTION_DOWN:
                mLastDownX = x;
                mLastDownY = y;
                mDownTime = System.currentTimeMillis();
                isMove = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if (Math.abs(x - mLastDownX) > touchSlop || Math.abs(y - mLastDownY) > touchSlop) {
                    isMove = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isMove) {
                    break;
                }
                if (System.currentTimeMillis() - mDownTime > 1000) {
                    isLongPressUp = true;
                } else {
                    isSingleTapUp = true;
                }
                break;
        }
        if (dispatchChild) {
            if (isSingleTapUp) {
                isSingleTapUp = false;
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                    return true;
                }
                return false;
            }
            if (isLongPressUp) {
                isLongPressUp = false;
                if (mOnLongClickListener != null) {
                    mOnLongClickListener.onLongClick(v);
                    return true;
                }
                return false;
            }
        }
        return true;
    }

}
