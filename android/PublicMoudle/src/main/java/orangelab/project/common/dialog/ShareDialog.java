package orangelab.project.common.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.R;

/**
 * game_werewolf
 * 2018/2/2 上午11:33
 * Mystery
 */

public class ShareDialog extends SafeDialog {

    private View mClose;
    private ViewGroup mDialogContent;
    private ViewGroup mDialogTitle;
    private Runnable mDismissRunnable;

    public ShareDialog(@NonNull Context context) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.layout_share_outof_game_dialog);

        mClose = findViewById(R.id.id_dialog_close);
        mClose.setOnClickListener(v -> {
            if (mDismissRunnable != null) {
                mDismissRunnable.run();
            }
        });

        mDialogContent = (ViewGroup) findViewById(R.id.id_dialog_content);
        mDialogTitle = (ViewGroup) findViewById(R.id.id_dialog_title);
        setCancelable(false);
        resizeDialog(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public void setDismissRunnable(Runnable dismissRunnable) {
        mDismissRunnable = dismissRunnable;
    }

    public void addTitleView(View view) {
        if (view != null) {
            mDialogTitle.addView(view);
        }
    }

    public void addContentView(View view) {
        if (view != null) {
            mDialogContent.addView(view);
        }
    }

    @Override
    protected void release() {

    }
}
