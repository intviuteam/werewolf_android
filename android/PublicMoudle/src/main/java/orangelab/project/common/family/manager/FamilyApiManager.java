package orangelab.project.common.family.manager;

import android.text.TextUtils;

import com.toolkit.action.ApiResult;
import orangelab.project.common.exception.ApiFailedException;
import orangelab.project.common.exception.NetWorkException;
import orangelab.project.common.family.model.FamilyInfoApiResult;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;

import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/10/24 上午10:48
 * Mystery
 * <p>
 * 家族相关Api管理器
 */

public class FamilyApiManager {


    public static void CreateFamilyForTest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", "这是一个很深很深的狼穴");
            jsonObject.put("short_name", "狼穴");
            jsonObject.put("desc", "这个是终极狼人杀的第一个家族,族长是一个很美很美很美的囧");
            jsonObject.put("image", "http://ac-BrjpIjqW.clouddn.com/4323acd70c406bfe74b1?imageslim");
            jsonObject.put("lc_id", "59f185da8159ccabfcd51c4e");
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestTask.create()

                .isGet(false)
                .request("/group/create")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .load(jsonObject)
                .build().execute();
    }

    public static void GetFamilyGroupInfo(String groupId, ApiResult<FamilyInfoApiResult> apiResult) {
        if (TextUtils.isEmpty(groupId)) {
            return;
        }
        RequestTask.create()
                .isGet(true)
                .request("/group/info/" + groupId + "?maxUser=30")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        FamilyInfoApiResult result = GsonHelper.getGson().fromJson(response, FamilyInfoApiResult.class);
                        if (apiResult != null) {
                            apiResult.onResult(result, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void KickOutMembersFromFamily(String userId, ApiResult<String> apiResult) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        RequestTask.create()
                .isGet(true)
                .request("/group/member/remove/" + userId)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void TransferFamilyBoss(String userId, ApiResult<String> apiResult) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        RequestTask.create()
                .isGet(true)
                .request("/group/member/change_own/" + userId)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void ApproveFamilyApply(String userId, ApiResult<String> apiResult) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        RequestTask.create()
                .isGet(true)
                .request("/group/member/accept/" + userId)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void ApproveAllFamilyApply(ApiResult<String> apiResult) {
        RequestTask.create()
                .isGet(true)
                .request("/group/member/accept_all")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void RejectAllFamilyApply(ApiResult<String> apiResult) {
        RequestTask.create()
                .isGet(true)
                .request("/group/member/reject_all")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult("", null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void QuitFamily(ApiResult<Integer> apiResult) {
        RequestTask.create()
                .isGet(true)
                .request("/group/member/quit")
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        if (apiResult != null) {
                            apiResult.onResult(0, null);
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(code, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(-1, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void EditFamilyDetail(String name, String shortName, String desc, String image, String imageId, ApiResult<FamilyInfoApiResult> apiResult) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("short_name", shortName);
            jsonObject.put("desc", desc);
            jsonObject.put("image", image);
            jsonObject.put("image_id", imageId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestTask.create().isGet(false).request("/group/update").load(jsonObject).withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            FamilyInfoApiResult result = GsonHelper.getGson().fromJson(response, FamilyInfoApiResult.class);
                            if (apiResult != null) {
                                apiResult.onResult(result, null);
                            }
                        } catch (Exception e) {
                            if (apiResult != null) {
                                apiResult.onResult(null, new NetWorkException());
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void GetFamilyInfoByConversationId(String conversationId, ApiResult<FamilyInfoApiResult> apiResult) {
        if (TextUtils.isEmpty(conversationId))
            return;
        RequestTask.create().isGet(true).request("/group/find/lc_id/" + conversationId).withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            FamilyInfoApiResult result = GsonHelper.getGson().fromJson(response, FamilyInfoApiResult.class);
                            if (apiResult != null) {
                                apiResult.onResult(result, null);
                            }
                        } catch (Exception e) {
                            if (apiResult != null) {
                                apiResult.onResult(null, new NetWorkException());
                            }
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new ApiFailedException(failed));
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        if (apiResult != null) {
                            apiResult.onResult(null, new NetWorkException());
                        }
                    }
                }).build().execute();
    }

    public static void FamilyTaskDoApi() {
        RequestTask.create().isGet(true).request("/task/do?type=family_chat").withToken(GlobalUserState.getGlobalState().getToken()).build().execute();
    }

    public static void FamilyTaskDoIntviuApi() {
        RequestTask.create().isGet(true).request("/task/do?type=family_invite").withToken(GlobalUserState.getGlobalState().getToken()).build().execute();
    }

    public static void FamilyChangeTitleApi(String userId, String type, ApiResult<Integer> result) {
        RequestTask.create().isGet(true).request("/group/member/change_title/" + userId + "/" + type).withToken(GlobalUserState.getGlobalState().getToken()).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                if (result != null) {
                    result.onResult(0, null);
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.onResult(code, new Exception(failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.onResult(-1, new NetWorkException());
                }
            }
        }).build().execute();
    }

    public static void FamilyApplyIn(String groupId, ApiResult<Integer> result) {
        RequestTask.create().isGet(true).request("/group/member/join/" + groupId).withToken(GlobalUserState.getGlobalState().getToken()).action(new RequestTaskCallBack() {
            @Override
            public void onSuccess(String response) {
                if (result != null) {
                    result.onResult(0, null);
                }
            }

            @Override
            public void onFailed(int code, String failed) {
                if (result != null) {
                    result.onResult(code, new Exception(failed));
                }
            }

            @Override
            public void onError(Exception e) {
                if (result != null) {
                    result.onResult(-1, new NetWorkException());
                }
            }
        }).build().execute();
    }


}
