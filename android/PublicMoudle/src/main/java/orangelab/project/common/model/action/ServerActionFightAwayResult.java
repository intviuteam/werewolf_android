package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

import orangelab.project.common.model.DeathInfo;

/**
 * game_werewolf
 * 2018/4/24 上午11:28
 * Mystery
 */

public class ServerActionFightAwayResult implements Keepable {

    public ArrayList<DeathInfo.DeathInfoItem> death_info;
    public int knight = -1;
    public long duration = 2000;
}
