package orangelab.project.common.pay.wechat;

import android.app.Activity;
import android.content.Intent;

import orangelab.project.common.pay.PayHelper;
import orangelab.project.common.pay.PayHelperListener;

/**
 * game_werewolf
 * 2017/7/22 上午11:18
 * Mystery
 */

public class WeChatPayHelper extends PayHelper {

    private Activity mActivity;
    private String mType;
    private PayHelperListener mPayHelperListener;

    public WeChatPayHelper(Activity activity, String mType, PayHelperListener payHelperListener) {
        super(mType, payHelperListener);
        this.mActivity = activity;
        this.mType = mType;
        this.mPayHelperListener = payHelperListener;
    }

    @Override
    public void repay() {

    }

    @Override
    public void pay() {
        /**
         * 1.向服务器调用下单接口
         * 2.根据步骤1的结果
         */
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return false;
    }

    @Override
    public void release() {

    }
}
