package orangelab.project.common.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.R;
import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.utils.MessageUtils;

import com.androidtoolkit.view.ScreenUtils;

/**
 * 系统名称:
 * 模块名称:
 * 功能说明:
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 */

public class WarningDialog extends BaseGameSelectDialog {

    private View.OnClickListener cancelListener = null;
    private boolean hasConfirmed = false;
    private boolean hasDismiss = false;
    private boolean hasSendLeaveEvent = false;

    public WarningDialog(Context context, final View.OnClickListener confirm, final View.OnClickListener
            cancel) {
        super(context);
        setTitle(MessageUtils.getString(R.string.dialog_hint));
        View view = LayoutInflater.from(context).inflate(R.layout.layout_msg_dialog, null);
        TextView textView = (TextView) view.findViewById(R.id.msg);
        addContentView(view);
        cancelListener = cancel;
        textView.setText(MessageUtils.getString(R.string.active_warning_string));
        setButtonType(TWO_BUTTON);
        setCancelString("离开");
        setConfirmString("继续玩");
        setCancelListener(v -> {
            cancel.onClick(v);
            hasConfirmed = true;
        });
        setConfirmListener(v -> {
            confirm.onClick(v);
            hasConfirmed = true;
        });
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        params.width = ScreenUtils.dip2px(345);
        getWindow().setAttributes(params);
    }

    @Override
    public void dismiss() {
        hasDismiss = true;
        super.dismiss();
    }

    @Override
    public void onFinish() {
        if (hasDismiss) {
            return;
        }
        if (!hasConfirmed) {
            if (cancelListener != null) {
                cancelListener.onClick(null);
            }
            if (!hasSendLeaveEvent) {
                RxToolKit.Emit(new ViewEvent.LeaveMessageEvent());
                hasSendLeaveEvent = true;
            }
        }
        super.onFinish();
    }
}
