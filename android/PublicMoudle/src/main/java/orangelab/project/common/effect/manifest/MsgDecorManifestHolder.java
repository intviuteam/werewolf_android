package orangelab.project.common.effect.manifest;

import android.text.TextUtils;

import com.androidtoolkit.PLog;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.tool.GsonTurnToolKit;

/**
 * game_werewolf
 * 2018/7/12 下午8:22
 * Mystery
 */
public class MsgDecorManifestHolder extends EffectsManifestHolder<MsgDecorManiFest> {

    private static final String TAG = "MsgDecorManifestHolder";

    public MsgDecorManifestHolder(ManifestManagerHelper helper, String cacheManifestFile, String cacheManifestVersionFile, String diskLruType) {
        super(helper, cacheManifestFile, cacheManifestVersionFile, diskLruType);
    }

    @Override
    public MsgDecorManiFest transformData(String data) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        return GsonTurnToolKit.turn(data, MsgDecorManiFest.class);
    }

    @Override
    public void saveManifest(MsgDecorManiFest data) {
        if (data == null) {
            return;
        }
        try {
            String temp = GsonHelper.getGson().toJson(data);
            putVersion(data.rev);
            mHelper.putString(mCacheManifestFile, temp);
            updateManifest(data);
            PLog.i(TAG, "Msg Decor Save Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<String> verifyManifest(MsgDecorManiFest data) {
        Set<String> result = new HashSet<>();
        if (data == null) {
            return result;
        }
        List<MsgDecorManiFest.MsgDecorManiFestItem> items = data.message_boxs;
        if (items == null) {
            return result;
        }
        for (MsgDecorManiFest.MsgDecorManiFestItem item : items) {
            MsgDecorManiFest.MsgDecorManiFestItemResource resource = item.animate;
            if (resource != null) {
                if (ApkResourceManager.GetApkImageResource(resource.local) > 0) {

                } else {
                    if (!verifyUrlData(resource.remote)) {
                        result.add(item.message_box_type);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void downloadManifestResource(MsgDecorManiFest data, boolean forceRefresh) {
        if (data == null) {
            return;
        }
        List<MsgDecorManiFest.MsgDecorManiFestItem> items = data.message_boxs;
        if (items == null) {
            return;
        }
        for (MsgDecorManiFest.MsgDecorManiFestItem item : items) {
            MsgDecorManiFest.MsgDecorManiFestItemResource resource = item.animate;
            if (resource != null) {
                if (!TextUtils.isEmpty(resource.remote)) {
                    downloadAndSaveIfNeed(resource.local, resource.remote, forceRefresh);
                }
            }
        }
    }

    public MsgDecorManiFest.MsgDecorManiFestItem getManifestItem(String type) {
        MsgDecorManiFest msgDecorManiFest = getManifestData();
        if (msgDecorManiFest != null) {
            return msgDecorManiFest.find(type);
        }
        return null;
    }
}
