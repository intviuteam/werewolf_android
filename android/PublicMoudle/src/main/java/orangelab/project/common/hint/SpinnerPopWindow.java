package orangelab.project.common.hint;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.R;

import orangelab.project.common.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/9/12 上午10:58
 * Mystery
 */

public class SpinnerPopWindow<T> extends PopupWindow {
    private LayoutInflater inflater;
    private ListView mListView;
    private List<T> list;
    private MyAdapter<T> mAdapter;
    private String textColor = "#eaae10";
    private ISpinnerPopWindowShow<T> mTISpinnerPopWindowShow;

    public SpinnerPopWindow(Context context, List<T> list, AdapterView.OnItemClickListener clickListener) {
        this(context, list, clickListener, null);
    }

    public SpinnerPopWindow(Context context, List<T> list, AdapterView.OnItemClickListener clickListener, ISpinnerPopWindowShow<T> spinnerPopWindowShow) {
        super(context);
        inflater = LayoutInflater.from(context);
        if (list == null) {
            list = new ArrayList<T>();
        }
        this.list = list;
        this.mTISpinnerPopWindowShow = spinnerPopWindowShow;
        init(clickListener);
    }

    private void init(AdapterView.OnItemClickListener clickListener) {
        View view = inflater.inflate(R.layout.dialog_call_list, null);
        setContentView(view);
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        ColorDrawable dw = new ColorDrawable(0x00);
        setBackgroundDrawable(dw);
        mListView = (ListView) view.findViewById(R.id.listview);
        mListView.setAdapter(mAdapter = new MyAdapter<T>());
        mListView.setOnItemClickListener(clickListener);
    }

    public void setListViewDivider(int height, int colorRes) {
        mListView.setDivider(new ColorDrawable(mListView.getContext().getResources().getColor(colorRes)));
        mListView.setDividerHeight(height);
    }

    public void setListViewBackGround(int res) {
        mListView.setBackgroundResource(res);
    }

    public void setTextColor(String color) {
        textColor = color;
    }

    private class MyAdapter<T> extends BaseAdapter {
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.layout_werewolf_setting_drop_item, null);
                holder.tvName = (TextView) convertView.findViewById(R.id.text1);
                holder.tvName.setTextColor(Color.parseColor(textColor));
                holder.tvName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                holder.tvName.setGravity(Gravity.CENTER);
                holder.tvName.setPadding(ScreenUtils.dip2px(5), ScreenUtils.dip2px(5), ScreenUtils.dip2px(5), ScreenUtils.dip2px(5));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (mTISpinnerPopWindowShow != null) {
                mTISpinnerPopWindowShow.onItemShow(list.get(position), holder.tvName);
            } else {
                holder.tvName.setText(MessageUtils.getLevelLimit(getItem(position).toString()));
            }
            return convertView;
        }
    }

    public interface ISpinnerPopWindowShow<R> {
        void onItemShow(R item, TextView textView);
    }

    private class ViewHolder {
        private TextView tvName;
    }
}