package orangelab.project.common.exhibition.gift;

import android.text.TextUtils;

/**
 * game_werewolf
 * 2017/6/28 上午11:56
 * Mystery
 */

public class GiftUtils {

//    public static String getGiftName(String type) {
//        String mGiftName = "";
//        switch (type) {
//            case ExhibitionsConstant.GIFT_ROSE:
//                mGiftName = MessageUtils.getString(R.string.gift_rose_string);
//                break;
//            case ExhibitionsConstant.GIFT_ROSE3:
//                mGiftName = MessageUtils.getString(R.string.gift_rose3_string);
//                break;
//            case ExhibitionsConstant.GIFT_ROSE99:
//                mGiftName = MessageUtils.getString(R.string.gift_rose99_string);
//                break;
//            case ExhibitionsConstant.GIFT_KISS:
//                mGiftName = MessageUtils.getString(R.string.gift_kiss_string);
//                break;
//            case ExhibitionsConstant.GIFT_VEGETABLE:
//                mGiftName = MessageUtils.getString(R.string.gift_vegetable_string);
//                break;
//            case ExhibitionsConstant.GIFT_EGG:
//                mGiftName = MessageUtils.getString(R.string.gift_egg_string);
//                break;
//            case ExhibitionsConstant.GIFT_CLAP:
//                mGiftName = MessageUtils.getString(R.string.gift_clap_string);
//                break;
//            case ExhibitionsConstant.GIFT_CHOCOLATE:
//                mGiftName = MessageUtils.getString(R.string.gift_chocolate_string);
//                break;
//            case ExhibitionsConstant.GIFT_CAKE:
//                mGiftName = MessageUtils.getString(R.string.gift_cake_string);
//                break;
//            case ExhibitionsConstant.GIFT_WEDDINGDRESS:
//                mGiftName = MessageUtils.getString(R.string.gift_weddingdress_string);
//                break;
//            case ExhibitionsConstant.GIFT_JAWBREAKER:
//                mGiftName = MessageUtils.getString(R.string.gift_jawbreaker_string);
//                break;
//            case ExhibitionsConstant.GIFT_BOAT:
//                mGiftName = MessageUtils.getString(R.string.gift_boat_string);
//                break;
//            case ExhibitionsConstant.GIFT_TEDDYBEAR:
//                mGiftName = MessageUtils.getString(R.string.gift_teddybear_string);
//                break;
//            case ExhibitionsConstant.GIFT_HEART:
//                mGiftName = MessageUtils.getString(R.string.gift_heart_string);
//                break;
//            case ExhibitionsConstant.GIFT_RING:
//                mGiftName = MessageUtils.getString(R.string.gift_ring_string);
//                break;
//            case ExhibitionsConstant.GIFT_CROWN:
//                mGiftName = MessageUtils.getString(R.string.gift_crown_string);
//                break;
//            case ExhibitionsConstant.GIFT_CAR:
//                mGiftName = MessageUtils.getString(R.string.gift_car_string);
//                break;
//            case ExhibitionsConstant.GIFT_AIRPLANE:
//                mGiftName = MessageUtils.getString(R.string.gift_airplane_string);
//                break;
//            case ExhibitionsConstant.GIFT_ROCK:
//                mGiftName = MessageUtils.getString(R.string.gift_rock_string);
//                break;
//        }
//        return mGiftName;
//    }

//    public static int getGiftBgResource(String type) {
//        int mGiftExhibitionBackGroundResource = -1;
//        switch (type) {
//            case ExhibitionsConstant.GIFT_ROSE:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_rose;
//                break;
//            case ExhibitionsConstant.GIFT_ROSE3:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_rose3;
//                break;
//            case ExhibitionsConstant.GIFT_ROSE99:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_rose99;
//                break;
//            case ExhibitionsConstant.GIFT_KISS:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_kiss;
//                break;
//            case ExhibitionsConstant.GIFT_VEGETABLE:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_vegetable;
//                break;
//            case ExhibitionsConstant.GIFT_EGG:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_egg;
//                break;
//            case ExhibitionsConstant.GIFT_CLAP:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_clap;
//                break;
//            case ExhibitionsConstant.GIFT_CHOCOLATE:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_chocolate;
//                break;
//            case ExhibitionsConstant.GIFT_CAKE:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_cake;
//                break;
//            case ExhibitionsConstant.GIFT_WEDDINGDRESS:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_weddingdress;
//                break;
//            case ExhibitionsConstant.GIFT_JAWBREAKER:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_jawbreaker;
//                break;
//            case ExhibitionsConstant.GIFT_BOAT:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_boat;
//                break;
//            case ExhibitionsConstant.GIFT_TEDDYBEAR:
//                mGiftExhibitionBackGroundResource = R.mipmap.gift_bear_0;
//                break;
//            case ExhibitionsConstant.GIFT_HEART:
//                mGiftExhibitionBackGroundResource = R.mipmap.heart_v2_local;
//                break;
//            case ExhibitionsConstant.GIFT_RING:
//                mGiftExhibitionBackGroundResource = R.mipmap.dim_ring_v2_local;
//                break;
//            case ExhibitionsConstant.GIFT_CROWN:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_crown;
//                break;
//            case ExhibitionsConstant.GIFT_CAR:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_car;
//                break;
//            case ExhibitionsConstant.GIFT_AIRPLANE:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_airplane;
//                break;
//            case ExhibitionsConstant.GIFT_ROCK:
//                mGiftExhibitionBackGroundResource = R.mipmap.room_gift_rocket;
//                break;
//            case ExhibitionsConstant.GIFT_GOLDEN_MICROPHONE:
//                mGiftExhibitionBackGroundResource = R.mipmap.golden_microphone_v1_local;
//                break;
//            case ExhibitionsConstant.GIFT_PERFUME:
//                mGiftExhibitionBackGroundResource = R.mipmap.perfume_v1_local;
//                break;
//            case ExhibitionsConstant.GIFT_WEAK_SAUCE:
//                mGiftExhibitionBackGroundResource = R.mipmap.weak_sauce_v2_local;
//                break;
//        }
//        return mGiftExhibitionBackGroundResource;
//    }

    public static int NeedCurvePathAnimator(String fromLocation, String toLocation) {
        if (TextUtils.isEmpty(fromLocation) || TextUtils.isEmpty(toLocation)) {
            return -1;
        }
        if (!fromLocation.contains("screen") && !toLocation.contains("screen")) {
            return 0;
        }
        if (fromLocation.contains("screen") && !toLocation.contains("screen")) {
            return 0;
        }
        if (!fromLocation.contains("screen") && toLocation.contains("screen")) {
            return 0;
        }
        return 1;
    }
}
