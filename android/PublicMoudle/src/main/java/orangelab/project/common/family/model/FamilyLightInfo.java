package orangelab.project.common.family.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/10/28 下午12:43
 * Mystery
 */

public class FamilyLightInfo implements Parcelable,Keepable {

    public String familyName;
    public String familyShortName;
    public String familyDesc;
    public String familyImage;

    public FamilyLightInfo() {
    }

    protected FamilyLightInfo(Parcel in) {
        familyName = in.readString();
        familyShortName = in.readString();
        familyDesc = in.readString();
        familyImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(familyName);
        dest.writeString(familyShortName);
        dest.writeString(familyDesc);
        dest.writeString(familyImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FamilyLightInfo> CREATOR = new Creator<FamilyLightInfo>() {
        @Override
        public FamilyLightInfo createFromParcel(Parcel in) {
            return new FamilyLightInfo(in);
        }

        @Override
        public FamilyLightInfo[] newArray(int size) {
            return new FamilyLightInfo[size];
        }
    };
}
