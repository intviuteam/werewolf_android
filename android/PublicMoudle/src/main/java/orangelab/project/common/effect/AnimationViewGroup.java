package orangelab.project.common.effect;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import com.androidtoolkit.RxToolKit;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Set;

import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.exhibition.card.WereWolfCardManager;
import orangelab.project.game.manager.FriendRequestViewManager;

/**
 * game_werewolf
 * 2017/6/21 下午1:46
 * Mystery
 */

public class AnimationViewGroup extends FrameLayout implements IAnimationSurface {

    private static final String TAG = "AnimationViewGroup";

    private EffectsTaskManager mAnimManager;
    private WereWolfCardManager mCardManager;
    private FriendRequestViewManager mRequestFriendManager;

    private boolean activityHolderIsAvailable = true;


    public AnimationViewGroup(Context context) {
        this(context, null);
    }

    public AnimationViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimationViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mAnimManager = new EffectsTaskManager(context, this);
        mCardManager = new WereWolfCardManager(context, this);
        mRequestFriendManager = new FriendRequestViewManager(context, this);
        initListener();
    }

    public void restoreRequestFriendManagerFilter(Set<String> filter) {
        mRequestFriendManager.restoreUserFilter(filter);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AnimationEvent.RemainAnimationTaskAddEvent event) {
        try {
            mAnimManager.addRemainAnim(event.getRemainTask());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AnimationEvent.RemainAnimationTaskRemoveEvent event) {
        try {
            mAnimManager.removeRemainAnim(event.getPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListener() {
        EventBus.getDefault().register(this);
        RxToolKit.Build(this, AnimationEvent.AnimationTaskEvent.class).action(giftAnimEvent ->
                addAnimation(giftAnimEvent.getTask())
        ).register();

        RxToolKit.Build(this, ActivityEvent.GameActivityEvent.class).action(gameActivityEvent -> {
            Log.i(TAG, "initListener: " + gameActivityEvent);
            if (gameActivityEvent.isResume()) {
                activityHolderIsAvailable = true;
            }
            if (gameActivityEvent.isStop()) {
                activityHolderIsAvailable = false;
            }
            Log.i(TAG, "activityHolderIsAvailable =" + activityHolderIsAvailable);
        }).register();
    }

    public void addAnimation(EffectsTaskManager.Task task) {
        if (activityHolderIsAvailable) {
            /**
             * 只有在Activity可见的时候才展示动画
             */
            mAnimManager.addAnimation(task);
        } else {
            Log.i(TAG, "addAnimation: activity container is unavailable so ignore this task");
        }
    }

    public void destroy() {
        RxToolKit.UnRegister(this);
        EventBus.getDefault().unregister(this);
        if (mAnimManager != null) {
            mAnimManager.onDestroy();
        }
        if (mCardManager != null) {
            mCardManager.destroy();
        }
        if (mRequestFriendManager != null) {
            mRequestFriendManager.destroy();
        }
    }

    public EffectsTaskManager getAnimManager() {
        return mAnimManager;
    }

    public WereWolfCardManager getCardManager() {
        return mCardManager;
    }

    public FriendRequestViewManager getRequestFriendManager() {
        return mRequestFriendManager;
    }

    @Override
    public boolean hasCard(String type) {
        return mCardManager.hasCard(type);
    }

    @Override
    public void userCard(String type, boolean isDisposable) {
        mCardManager.userCard(type, isDisposable);
    }

    @Override
    public void refreshCard() {
        mCardManager.refreshCardList();
    }
}
