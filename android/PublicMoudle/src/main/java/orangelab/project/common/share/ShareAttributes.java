package orangelab.project.common.share;

/**
 * 分享后缀属性
 * 前缀从server获取
 */
public interface ShareAttributes {

    //app 分享
    String SHARE_APP_ATTRIBUTES = "channel=%s&caseid=%s&uid=%s";

    //游戏房分享
    String SHARE_GAME_ATTRIBUTES = "channel=%s&id=%s&password=%s&inviter=%s&caseid=%s&uid=%s";

    //语音房分享
    String SHARE_VOICE_ATTRIBUTES = "type=audio&roomid=%s&password=%s&caseid=%s&uid=%s";

    //私密房分享
    String SHARE_PRIVATE_ATTRIBUTES = "type=privacy&roomid=%s&password=%s&caseid=%s&uid=%s";

    //卧底房分享
    String SHARE_SPY_ATTRIBUTES = "room_id=%s&pass=%s&name=%s&avatar=%s&caseid=%s&uid=%s";

    //小游戏房分享(rn调用)
    String SHARE_MINIGAME_ATTRIBUTES = "game_type=%s&caseid=%s&uid=%s";

    //游戏大厅分享(暂时没有)
    String SHARE_LOBBY_ATTRIBUTES = "caseid=%s&uid=%s";

}
