package orangelab.project.common.event;

/**
 * game_werewolf
 * 2017/8/8 下午7:20
 * Mystery
 */

public class WebViewEvent {

    public static class WebViewPayResultEvent {
        private int code;
        private String message;

        public WebViewPayResultEvent(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return "WebViewPayResultEvent{" +
                    "code=" + code +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
