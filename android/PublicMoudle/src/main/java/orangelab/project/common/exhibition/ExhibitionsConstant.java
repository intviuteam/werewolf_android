package orangelab.project.common.exhibition;

/**
 * game_werewolf
 * 2017/6/28 下午12:00
 * Mystery
 */

public class ExhibitionsConstant {

    public static final String ACTION_SHOW_GIFT = "ACTION_SHOW_GIFT";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_POSITION = "USER_POSITION";
    public static final String TO_USER_ID = "TO_USER_ID";
    public static final String FROM_CHAT = "FROM_CHAT";    //从聊天送礼物
    public static final String FROM_GAME = "FROM_GAME";    //从游戏内送礼物
    public static final String FROM_TYPE = "FROM_TYPE";
    public static final String FROM_MAIN_PAGE = "FROM_MAIN_PAGE"; //从个人页面送礼物
    public static final String FROM_OUT_SIDER = "FROM_OUT_SIDER";
    public static final String TO_USER_DATA = "TO_USER_DATA";

    public static final String EXHIBITIONS_TYPE_CARD = "CARD";
    public static final String EXHIBITIONS_TYPE_GIFT = "GIFT";

    public static final String GIFT_ROSE = "flower_1"; // 1花
    public static final String GIFT_ROSE3 = "flower_3"; //3 花
    public static final String GIFT_ROSE99 = "flower_99"; //99 花
    public static final String GIFT_VEGETABLE = "cabbage"; // 烂菜叶
    public static final String GIFT_EGG = "egg"; // 鸡蛋
    public static final String GIFT_CLAP = "clap"; // 鼓掌
    public static final String GIFT_CHOCOLATE = "chocolate"; // 巧克力
    public static final String GIFT_HEART = "heart"; // 心
    public static final String GIFT_JAWBREAKER = "lollipop"; //棒棒糖
    public static final String GIFT_KISS = "kiss"; // 么么哒
    public static final String GIFT_RING = "dim_ring"; // 钻戒
    public static final String GIFT_TEDDYBEAR = "tidy"; // 泰迪
    public static final String GIFT_WEDDINGDRESS = "wedding_dress"; // 婚纱
    public static final String GIFT_CROWN = "crown"; // 皇冠
    public static final String GIFT_CAR = "ferrari"; // 法拉利
    public static final String GIFT_CAKE = "cake"; // 蛋糕
    public static final String GIFT_BOAT = "ship"; // 船
    public static final String GIFT_AIRPLANE = "airplane"; // 飞机
    public static final String GIFT_ROCK = "rocket"; // 火箭
    public static final String GIFT_GOLDEN_MICROPHONE = "golden_microphone";
    public static final String GIFT_PERFUME = "perfume";
    public static final String GIFT_WEAK_SAUCE = "weak_sauce";

    public static final String PRICE_TYPE_GOLD = "gold"; //金币
    public static final String PRICE_TYPE_DIAMOND = "dim"; //钻石
    public static final String PRICE_TYPE_NONE = "PRICE_TYPE_NONE";
    public static final int PRICE_NONE = -1;
    public static final int GIFT_COUNT_NONE = 0;

    public static final String CARD_DELAY_TIME = "append_time"; //延时卡
    public static final String CARD_CHECK = "check";    //查验卡
    public static final String CARD_CHECKER = "seer";//预言家卡
    public static final String CARD_HUNTER = "hunter";//猎人
    public static final String CARD_PEOPLE = "people";//平民
    public static final String CARD_WITCH = "witch";//女巫
    public static final String CARD_WOLF = "werewolf";//狼人
    public static final String CARD_CUPID = "cupid";//爱神
    public static final String CARD_GUARD = "guard";//守卫
    public static final String CARD_WEREWOLF_KING = "werewolf_king";//白狼王

    public static final String ALL_CARD = "ALL_CARD";//所有卡片
    public static final String MOVE_CARD = "MOVE_CARD";//移动卡片
    public static final String REDUCTION_CARD = "REDUCTION_CARD";//还原卡片位置
    public static final String LOAD_FB_AD = "LOAD_FB_AD";//加载广告


    public static final String DISPLAY_TYPE = "display_type";  //礼物展现类型
    public static final String DISPLAY_TYPE_SEAT = "seat";  //一般送礼：终点在玩家头像上
    public static final String DISPLAY_TYPE_SING = "sing";  //唱歌送礼：终点在播放器头像上


}
