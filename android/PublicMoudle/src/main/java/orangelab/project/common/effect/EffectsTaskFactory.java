package orangelab.project.common.effect;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.exhibition.gift.GiftUtils;
import orangelab.project.common.effect.gifts.CustomGiftAnim;
import orangelab.project.common.effect.gifts.CustomGiftAnimPayload;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.ToolKit;

import org.greenrobot.eventbus.EventBus;

/**
 * game_werewolf
 * 2017/11/6 下午6:30
 * Mystery
 */

public enum EffectsTaskFactory {

    INSTANCE;
    private final int screenWidth;
    private final int screenHeight;
    private int statsBarHeight = ScreenUtils.dip2px(25);
    private long rotateTime = 2;
    private PointF mCenterPoint;

    private Interpolator[] interpolators;
    private Random rand = new Random();

    private EffectsTaskFactory() {
        screenWidth = ScreenUtils.getScreenWidth();
        screenHeight = ScreenUtils.getScreenHeight();
        mCenterPoint = new PointF(screenWidth / 2, (screenHeight - statsBarHeight) / 2); //屏幕中心点
        interpolators = new Interpolator[5];
        interpolators[0] = new LinearInterpolator();//线性
        interpolators[1] = new AccelerateInterpolator();//加速
        interpolators[2] = new DecelerateInterpolator();//减速
        interpolators[3] = new AccelerateDecelerateInterpolator();//先加速后减速
    }

    private void wrapPoint(PointF pointF) {
//        pointF.set(pointF.x, pointF.y - statsBarHeight);
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public PointF getCenterPoint() {
        return mCenterPoint;
    }

    private PointF getUpReferencePoint() {
        return getUpReferencePoint(new PointF(screenWidth, screenHeight));
    }

    private PointF getDownReferencePoint() {
        return getDownReferencePoint(new PointF(screenWidth, screenHeight));
    }

    private PointF getUpReferencePoint(PointF scopeP) {
        PointF p = new PointF();
        //随即范围[0,scopeP.x]
        p.x = rand.nextFloat() * scopeP.x;
        float height = scopeP.y / 2;
        //随即范围[0,height]
        float y = rand.nextFloat() * height;
        p.y = y;
        return p;
    }

    private PointF getDownReferencePoint(PointF scopeP) {
        PointF p = new PointF();
        //随即范围[0,scopeP.x]
        p.x = rand.nextFloat() * scopeP.x;
        float height = scopeP.y / 2;
        //随即范围[0,height]
        float y = rand.nextFloat() * height;
        //随即范围[height,scopeP.y]
        y = y + height;
        p.y = y;
        return p;
    }

    /**
     * 创建礼物统一出现动画
     *
     * @param targetView
     * @param scale
     * @return
     */
    private Animator createAlphaOut(View targetView, float scale) {
        ObjectAnimator animatorAlphaOut = ObjectAnimator.ofFloat(targetView, "alpha", 0, 1);
        animatorAlphaOut.setDuration(200);
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", 0f, scale);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", 0f, scale);
        Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animatorAlphaOut).with(animatorScale);
        return animatorSet;
    }

    /**
     * 创建礼物统一消失动画
     *
     * @param targetView
     * @param scale
     * @return
     */
    private Animator createAlphaIn(View targetView, float scale) {
        ObjectAnimator animatorAlphaIn = ObjectAnimator.ofFloat(targetView, "alpha", 1, 0);
        animatorAlphaIn.setDuration(200);
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", scale, 0f);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", scale, 0f);
        Animator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animatorAlphaIn).with(animatorScale);
        return animatorSet;
    }

    public AnimationEvent.AnimationTaskEvent createAnimByGiftAnim(String fromType, Context context, CustomGiftAnim anim, CustomGiftAnimPayload payload) {
        if (anim == null) {
            return null;
        }
        if (TextUtils.isEmpty(anim.type)) {
            return null;
        }
        switch (anim.type) {
            case GiftAnimConstant.TYPE_ONEPOINTANIM:
                return createOnePointAnim(fromType, context, anim, payload);
            case GiftAnimConstant.TYPE_TWOPOINTANIM:
                return createTwoPointAnim(fromType, context, anim, payload);
            case GiftAnimConstant.TYPE_THREEPOINTANIM:
                return createThreePointAnim(fromType, context, anim, payload);
            case GiftAnimConstant.TYPE_TWOPOINT_LINE_ANIM:
                return createTwoPointLineWithRotateAnim(fromType, context, anim, payload);
        }
        return null;
    }

    /**
     * 创建残留动画
     *
     * @param context
     * @param startPointF
     * @param point
     * @param payload
     */
    private void createAndPostPointRemainAnim(Context context, PointF startPointF, int position, CustomGiftAnim.CustomGiftAnimPoint point, CustomGiftAnimPayload payload) {
        ImageView targetView = null;
        try {
            if (startPointF == null) {
                return;
            }
            if (point.remainAnim == null || point.remainAnim.imageAndroid.size() == 0) {
                return;
            }
            if (payload.viewWidth == -1 && payload.viewHeight == -1) {
                targetView = createAnimImageView(context, startPointF, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
            } else {
                targetView = createAnimImageView(context, startPointF, payload.viewWidth, payload.viewHeight);
            }

            /**
             * 创建静止动画
             */
            EffectsTaskManager.RemainTask task = new EffectsTaskManager.RemainTask(position);

            Animator animator = createStaticAnim(targetView, point.remainAnim.duration, point.remainAnim.scale);
            if (point.remainAnim.imageAndroid.size() >= 2) {
                /**
                 * 需要特效
                 */
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, point.remainAnim.needRepeat);
                faster.addAllFrames(point.remainAnim.imageAndroid, point.remainAnim.frameTime);
                animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));
                task.setIFasterAnim(faster);
            } else {
                /**
                 * 不需要特效
                 */
                CustomGiftAnim.CustomAnimFrame frame = point.remainAnim.imageAndroid.get(0);
                EffectsMainFestManager.FillImageViewByGiftImageCacheUrl(targetView, frame.local, frame.remote);
            }
            task.setAnimator(animator);
            task.setView(targetView);
            EventBus.getDefault().post(new AnimationEvent.RemainAnimationTaskAddEvent(task));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建双点直线并带旋转动画
     *
     * @param fromType
     * @param context
     * @param anim
     * @param payload
     * @return
     */
    private AnimationEvent.AnimationTaskEvent createTwoPointLineWithRotateAnim(String fromType, Context context, CustomGiftAnim anim, CustomGiftAnimPayload payload) {
        if (anim.point.size() != 2 && anim.path.size() != 1) {
            return null;
        }
        wrapPoint(payload.start);
        wrapPoint(payload.end);

        CustomGiftAnim.CustomGiftAnimPoint startGiftPoint = anim.point.get(0);
        CustomGiftAnim.CustomGiftAnimPoint endGiftPoint = anim.point.get(1);

        CustomGiftAnim.CustomGiftAnimPath giftPath = anim.path.get(0);

        AnimatorSet twoPointAnimor = new AnimatorSet();
        List<IFasterAnim> allFasterAnimByUrl = new ArrayList<>();

        float degree = 0f;

        try {

            PointF startPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, startGiftPoint);

            PointF endPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, endGiftPoint);

            final ImageView targetView;

            if (payload.viewWidth == -1 && payload.viewHeight == -1) {
                targetView = createAnimImageView(context, startPoint, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
            } else {
                targetView = createAnimImageView(context, startPoint, payload.viewWidth, payload.viewHeight);
            }

            if (anim.defaultAndroidFrame != null) {
                /**
                 * 需要展示默认的图片
                 */
                EffectsMainFestManager.FillImageViewByGiftImageCacheUrl(targetView, anim.defaultAndroidFrame.local, anim.defaultAndroidFrame.remote);
            }

            AnimatorSet leftAnimator = new AnimatorSet();
            AnimatorSet pathAnimator = new AnimatorSet();
            AnimatorSet rightAnimator = new AnimatorSet();

            Animator leftOutAnimator = createAlphaOut(targetView, startGiftPoint.scale);

            float orginRotate = targetView.getRotation();
            final float newRotate;

            /**
             * 第一个点的动画
             */
            if (startGiftPoint.anim != null && startGiftPoint.anim.duration > 0) {
                /**
                 * 这里说明第一个点要展示动画,初始化一些基本的数据
                 */
                Animator animator = createStaticAnim(targetView, startGiftPoint.anim.duration, startGiftPoint.scale);
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, startGiftPoint.anim.needRepeat);
                allFasterAnimByUrl.add(faster);
                faster.addAllFrames(startGiftPoint.anim.imageAndroid, startGiftPoint.anim.frameTime);
                animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));

                /**
                 * 播放旋转动画，获取旋转的角度
                 */
                degree = ToolKit.getRotationBetweenPoint(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
                degree = degree - 90; //因为图片都是水平的
                newRotate = orginRotate + degree;

                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        createAndPostPointRemainAnim(context, startPoint, payload.startPosition, startGiftPoint, payload);
                        /**
                         * 第一个点动画结束之后将图片进行旋转
                         */
                        targetView.setImageResource(R.mipmap.gift_placeholder);
                        targetView.setRotation(newRotate);
                    }
                });
                leftAnimator.play(animator).after(leftOutAnimator);
            }

            /**
             * 路径动画
             */
            if (giftPath.anim != null) {
                /**
                 * 需要路径动画,初始化路径动画
                 */
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, giftPath.anim.needRepeat);
                faster.addAllFrames(giftPath.anim.imageAndroid, giftPath.anim.frameTime);
                allFasterAnimByUrl.add(faster);
                /**
                 * 这里需要计算路径的时间
                 */
                long duration;
                if (giftPath.speed <= 0) {
                    duration = giftPath.duration;
                } else {
                    duration = (long) (ToolKit.getDistanceBetweenPoint(startPoint.x, startPoint.y, endPoint.x, endPoint.y) / giftPath.speed);
                }
                Animator animator1 = createStraightLineAnim(targetView, startPoint, endPoint, duration);
                animator1.setInterpolator(EffectsFactory.CreateInterpolator(giftPath.interpolator));
                Animator animator2 = createScaleAnim(targetView, giftPath.fromScale, giftPath.toScale, duration);
                animator1.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));
                AnimatorSet tempAnimator = new AnimatorSet();
                tempAnimator.play(animator1).with(animator2);
                pathAnimator.play(tempAnimator);
            } else {
                /**
                 * 不需要路径动画
                 */
                Animator animator1 = createStraightLineAnim(targetView, startPoint, endPoint, giftPath.duration);
                animator1.setInterpolator(EffectsFactory.CreateInterpolator(giftPath.interpolator));
                Animator animator2 = createScaleAnim(targetView, giftPath.fromScale, giftPath.toScale, giftPath.duration);
                AnimatorSet tempAnimator = new AnimatorSet();
                tempAnimator.play(animator1).with(animator2);
                pathAnimator.play(tempAnimator);
            }
            pathAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    /**
                     * 路径动画结束之后直接旋转回去
                     */
                    targetView.setImageResource(R.mipmap.gift_placeholder);
                    targetView.setRotation(orginRotate);
                }
            });

            /**
             * 第二个点的动画
             */
            Animator rightInAnimator = createAlphaIn(targetView, endGiftPoint.scale);

            if (endGiftPoint.anim != null && endGiftPoint.anim.duration > 0) {
                Animator animator = createStaticAnim(targetView, endGiftPoint.anim.duration, endGiftPoint.scale);
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, endGiftPoint.anim.needRepeat);
                allFasterAnimByUrl.add(faster);
                faster.addAllFrames(endGiftPoint.anim.imageAndroid, endGiftPoint.anim.frameTime);
                animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        createAndPostPointRemainAnim(context, endPoint, payload.endPosition, endGiftPoint, payload);
                    }
                });
                rightAnimator.play(animator).before(rightInAnimator);
            }

            if (startPoint.equals(endPoint)) {
                /**
                 * 赠送的开始坐标和结束坐标相同，则不展示路径动画
                 */
                twoPointAnimor.play(leftAnimator).before(rightAnimator);
            } else {
                twoPointAnimor.play(pathAnimator).after(leftAnimator).before(rightAnimator);
            }
            return createAnimationTask(targetView, twoPointAnimor, allFasterAnimByUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 创建私密房点赞动画
     *
     * @param context
     * @param payload
     * @return
     */
    public AnimationEvent.AnimationTaskEvent createPraiseAnim(Context context, CustomGiftAnimPayload payload) {
        AnimatorSet animatorSet = new AnimatorSet();
        wrapPoint(payload.start);
        wrapPoint(payload.end);
        View view = null;
        if (payload.viewWidth == -1 && payload.viewHeight == -1) {
            view = createAnimImageView(context, R.mipmap.ico_private_room_little_heart, payload.start, CustomGiftAnim.DEFAULT_PRIASE_WIDTH, CustomGiftAnim.DEFAULT_PRIASE_HEIGHT);
        } else {
            view = createAnimImageView(context, R.mipmap.ico_private_room_little_heart, payload.start, payload.viewWidth, payload.viewHeight);
        }
        Animator outAnimator = createAlphaOut(view, 1f);
        Animator inAnimator = createAlphaIn(view, 1f);
        Animator pathAnim = createCurvePathAnimator(
                view, getUpReferencePoint(), getDownReferencePoint(), payload.start, payload.end, 2000, false);
        animatorSet.play(pathAnim).after(outAnimator).before(inAnimator);
        EffectsTaskManager.Task task = new EffectsTaskManager.Task();
        task.mAnimator = animatorSet;
        task.mView = view;
        task.setType(EffectsTaskManager.TASK_PRAISE);
        return new AnimationEvent.AnimationTaskEvent(task);
    }

    /**
     * 创建礼物反钻动画
     *
     * @param context
     * @param rebate
     * @param payload
     * @return
     */
    public AnimationEvent.AnimationTaskEvent createRebateAnim(Context context, String rebate, CustomGiftAnimPayload payload) {
        AnimatorSet animatorSet = new AnimatorSet();
        wrapPoint(payload.start);
        wrapPoint(payload.end);
        PointF resultPoint = payload.end;
        PointF resultPoint2 = new PointF(resultPoint.x, resultPoint.y - ScreenUtils.dip2px(20));
        View view = null;
        if (payload.viewWidth == -1 && payload.viewHeight == -1) {
            view = createCustomAnimView(context, R.layout.layout_werewolf_rebate, resultPoint, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
        } else {
            view = createCustomAnimView(context, R.layout.layout_werewolf_rebate, resultPoint, payload.viewWidth, payload.viewHeight);
        }
        TextView textView = (TextView) view.findViewById(R.id.rebate);
        textView.setText("+" + rebate);
        Animator outAnimator = createAlphaOut(view, GiftAnimConstant.GIFT_REBATE_SCALE);
        Animator inAnimator = createAlphaIn(view, GiftAnimConstant.GIFT_REBATE_SCALE);
        Animator animator = createStraightLineAnim(view, resultPoint, resultPoint2, GiftAnimConstant.GIFT_REBATE_DURATION);
        animator.setInterpolator(new AccelerateInterpolator());
        animatorSet.play(animator).after(outAnimator).before(inAnimator);
        EffectsTaskManager.Task task = new EffectsTaskManager.Task();
        task.mAnimator = animatorSet;
        task.mView = view;
        task.setType(EffectsTaskManager.TASK_REBATE);
        return new AnimationEvent.AnimationTaskEvent(task);
    }

    /**
     * 创建三点动画
     *
     * @param fromType
     * @param context
     * @param anim
     * @param payload
     * @return
     */
    private AnimationEvent.AnimationTaskEvent createThreePointAnim(String fromType, Context context, CustomGiftAnim anim, CustomGiftAnimPayload payload) {
        if (anim.point.size() != 3 && anim.path.size() != 2) {
            return null;
        }
        wrapPoint(payload.start);
        wrapPoint(payload.end);


        CustomGiftAnim.CustomGiftAnimPoint giftStartPoint = anim.point.get(0);
        CustomGiftAnim.CustomGiftAnimPoint giftMidPoint = anim.point.get(1);
        CustomGiftAnim.CustomGiftAnimPoint giftEndPoint = anim.point.get(2);

        CustomGiftAnim.CustomGiftAnimPath path1 = anim.path.get(0);
        CustomGiftAnim.CustomGiftAnimPath path2 = anim.path.get(1);

        AnimatorSet threePointAnimator = new AnimatorSet();
        List<IFasterAnim> allFasterAnimByUrl = new ArrayList<>();

        try {
            /**
             * 得到第一个点的实体坐标
             */
            PointF startPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, giftStartPoint);
            /**
             * 得到第二个点的实体坐标
             */
            PointF midPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, giftMidPoint);
            /**
             * 得到第三个点的实体坐标
             */
            PointF endPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, giftEndPoint);

            /**
             * 初始化View
             */
            ImageView targetView = null;

            if (payload.viewWidth == -1 && payload.viewHeight == -1) {
                targetView = createAnimImageView(context, startPoint, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
            } else {
                targetView = createAnimImageView(context, startPoint, payload.viewWidth, payload.viewHeight);
            }

            if (anim.defaultAndroidFrame != null) {
                /**
                 * 需要展示默认的图片
                 */
                EffectsMainFestManager.FillImageViewByGiftImageCacheUrl(targetView, anim.defaultAndroidFrame.local, anim.defaultAndroidFrame.remote);
            }

            AnimatorSet leftPartAnimator = new AnimatorSet();
            AnimatorSet.Builder leftPartAnimatorBuilder = null;

            AnimatorSet midPartAnimator = new AnimatorSet();

            AnimatorSet rightPartAnimator = new AnimatorSet();
            AnimatorSet.Builder rightPartAnimatorBuilder = null;

            Animator targetOutAnimator = createAlphaOut(targetView, giftStartPoint.scale);
            Animator targetInAnimator = createAlphaIn(targetView, giftEndPoint.scale);
            /**
             * 解析左半部，包括 1。起点的动画 2。路径的动画
             */
            {
                CustomGiftAnim.CustomGiftAnimPath tempPath = path1;
                CustomGiftAnim.CustomGiftAnimPoint tempStartPoint = giftStartPoint;
                /**
                 * 分析起点动画
                 */
                if (tempStartPoint.anim != null && tempStartPoint.anim.duration > 0) {
                    CustomGiftAnim.CustomAnim tempAnim = tempStartPoint.anim;
                    /**
                     * 起点需要展示动画
                     */
                    Animator pointAnimator = createStaticAnim(targetView, tempAnim.duration, tempStartPoint.scale);
                    FasterAnimByCustomAnimFrame fasterAnim = FasterAnimByCustomAnimFrame.create(targetView, tempAnim.needRepeat);
                    fasterAnim.addAllFrames(tempAnim.imageAndroid, tempAnim.frameTime);
                    allFasterAnimByUrl.add(fasterAnim);
                    pointAnimator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(fasterAnim) {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            createAndPostPointRemainAnim(context, startPoint, payload.startPosition, tempStartPoint, payload);
                        }
                    });
                    leftPartAnimatorBuilder = leftPartAnimator.play(pointAnimator).after(targetOutAnimator);
                } else {
                    /**
                     * 起点不需要动画
                     */
                    leftPartAnimatorBuilder = leftPartAnimator.play(targetOutAnimator);
                }
                /**
                 * 分析第一个路径动画也叫左半部分路径动画,注意所有的三点动画，路径都是曲线
                 */
                Animator pathAnimator = createCurvePathAnimator(
                        targetView,
                        getUpReferencePoint(),
                        getDownReferencePoint(),
                        startPoint, midPoint,
                        tempPath.duration,
                        tempPath.supportRotate);
                Animator scaleAnimator = createScaleAnim(targetView, tempPath.fromScale, tempPath.toScale, tempPath.duration);
                pathAnimator.setInterpolator(EffectsFactory.CreateInterpolator(tempPath.interpolator));
                AnimatorSet tempAnimator = new AnimatorSet();
                tempAnimator.play(pathAnimator).with(scaleAnimator);
                if (tempPath.anim != null) {
                    /**
                     * 需要展示路径动画
                     */
                    FasterAnimByCustomAnimFrame fasterAnim = FasterAnimByCustomAnimFrame.create(targetView, tempPath.anim.needRepeat);
                    fasterAnim.addAllFrames(tempPath.anim.imageAndroid, tempPath.anim.frameTime);
                    pathAnimator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(fasterAnim));
                    allFasterAnimByUrl.add(fasterAnim);
                }
                leftPartAnimatorBuilder.before(tempAnimator);
                //左半部分分析结束
            }

            /**
             * 解析中部动画
             */
            {
                /**
                 * 这里只需要解析中点是否有动画即可
                 */
                CustomGiftAnim.CustomGiftAnimPoint tempMidPoint = giftMidPoint;
                if (tempMidPoint.anim != null && tempMidPoint.anim.duration > 0) {
                    /**
                     * 拼装动画
                     */
                    CustomGiftAnim.CustomAnim tempAnim = tempMidPoint.anim;
                    Animator midAnimator = createStaticAnim(targetView, tempAnim.duration, tempMidPoint.scale);
                    FasterAnimByCustomAnimFrame fasterAnim = FasterAnimByCustomAnimFrame.create(targetView, tempAnim.needRepeat);
                    fasterAnim.addAllFrames(tempAnim.imageAndroid, tempAnim.frameTime);
                    midAnimator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(fasterAnim));
                    midPartAnimator.play(midAnimator);
                }
            }

            /**
             * 解析右半部动画,包括 1。终点动画 2。路径动画
             */
            {
                CustomGiftAnim.CustomGiftAnimPoint tempEndPoint = giftEndPoint;
                CustomGiftAnim.CustomGiftAnimPath tempRightPath = path2;
                /**
                 * 解析一个点动画
                 */
                if (tempEndPoint.anim != null && tempEndPoint.anim.duration > 0) {
                    /**
                     * 终点有动画显示
                     */
                    CustomGiftAnim.CustomAnim tempAnim = tempEndPoint.anim;
                    Animator animator = createStaticAnim(targetView, tempAnim.duration, tempEndPoint.scale);
                    FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, tempAnim.needRepeat);
                    faster.addAllFrames(tempAnim.imageAndroid, tempAnim.frameTime);
                    animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster) {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            createAndPostPointRemainAnim(context, endPoint, payload.endPosition, tempEndPoint, payload);
                        }
                    });
                    allFasterAnimByUrl.add(faster);

                    rightPartAnimatorBuilder = rightPartAnimator.play(animator).before(targetInAnimator);
                } else {
                    /**
                     * 终点不需要动画显示
                     */
                    rightPartAnimatorBuilder = rightPartAnimator.play(targetInAnimator);
                }
                /**
                 * 解析路径动画
                 */
                Animator pathAnimator = createCurvePathAnimator(
                        targetView,
                        getUpReferencePoint(),
                        getDownReferencePoint(),
                        midPoint, endPoint,
                        tempRightPath.duration,
                        tempRightPath.supportRotate);
                Animator pathScaleAnimator = createScaleAnim(targetView, tempRightPath.fromScale, tempRightPath.toScale, tempRightPath.duration);
                pathAnimator.setInterpolator(EffectsFactory.CreateInterpolator(tempRightPath.interpolator));
                if (tempRightPath.anim != null) {
                    CustomGiftAnim.CustomAnim tempAnim = tempRightPath.anim;
                    FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(targetView, tempAnim.needRepeat);
                    faster.addAllFrames(tempAnim.imageAndroid, tempAnim.frameTime);
                    pathAnimator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));
                }
                AnimatorSet tempAnimatorSet = new AnimatorSet();
                tempAnimatorSet.play(pathAnimator).with(pathScaleAnimator);
                rightPartAnimatorBuilder.after(tempAnimatorSet);
            }

            threePointAnimator.play(midPartAnimator).after(leftPartAnimator).before(rightPartAnimator);
            return createAnimationTask(targetView, threePointAnimator, allFasterAnimByUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建双点动画
     *
     * @param fromType
     * @param context
     * @param anim
     * @param payload
     * @return
     */
    private AnimationEvent.AnimationTaskEvent createTwoPointAnim(String fromType, Context context, CustomGiftAnim anim, CustomGiftAnimPayload payload) {
        if (anim.point.size() != 2 && anim.path.size() != 1) {
            return null;
        }
        wrapPoint(payload.start);
        wrapPoint(payload.end);

        CustomGiftAnim.CustomGiftAnimPoint point1 = anim.point.get(0);
        CustomGiftAnim.CustomGiftAnimPoint point2 = anim.point.get(1);
        CustomGiftAnim.CustomGiftAnimPath path = anim.path.get(0);

        AnimatorSet animatorSet = new AnimatorSet();
        List<IFasterAnim> allFasterAnimByUrl = new ArrayList<>();
        try {
            PointF startPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, point1);
            PointF endPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, point2);
            /**
             * 创建一个展示图片的View
             */
            ImageView image = null;

            if (payload.viewWidth == -1 && payload.viewHeight == -1) {
                image = createAnimImageView(context, startPoint, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
            } else {
                image = createAnimImageView(context, startPoint, payload.viewWidth, payload.viewHeight);
            }

            if (anim.defaultAndroidFrame != null) {
                /**
                 * 需要展示默认的图片
                 */
                EffectsMainFestManager.FillImageViewByGiftImageCacheUrl(image, anim.defaultAndroidFrame.local, anim.defaultAndroidFrame.remote);
            }

            /**
             * 创建左部与右部的动画集合
             */
            AnimatorSet leftAnimator = new AnimatorSet();
            AnimatorSet rightAnimator = new AnimatorSet();
            AnimatorSet pathAnimator = new AnimatorSet();

            Animator outAnimator = createAlphaOut(image, point1.scale);
            Animator inAnimator = createAlphaIn(image, point2.scale);

            AnimatorSet.Builder animatorLeftBuilder = leftAnimator.play(outAnimator);
            AnimatorSet.Builder animatorRightBuilder = rightAnimator.play(inAnimator);
            /**
             * 第一个点的动画
             */
            if (point1.anim != null && point1.anim.duration > 0) {
                /**
                 * 这里说明第一个点要展示动画,初始化一些基本的数据
                 */
                Animator animator = createStaticAnim(image, point1.anim.duration, point1.scale);
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(image, point1.anim.needRepeat);
                allFasterAnimByUrl.add(faster);
                faster.addAllFrames(point1.anim.imageAndroid, point1.anim.frameTime);
                animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        createAndPostPointRemainAnim(context, startPoint, payload.startPosition, point1, payload);
                    }
                });
                animatorLeftBuilder.before(animator);
            }

            /**
             * 处理中间的路径动画
             */
            switch (GiftUtils.NeedCurvePathAnimator(path.from, path.to)) {
                case -1:
                    /**
                     * 异常，返回
                     */
                    return null;
                case 0:
                    /**
                     * 需要曲线
                     */
                {
                    if (path.anim != null) {
                        /**
                         * 需要路径动画
                         * 1.初始化路径动画
                         */
                        FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(image, path.anim.needRepeat);
                        faster.addAllFrames(path.anim.imageAndroid, path.anim.frameTime);
                        allFasterAnimByUrl.add(faster);

                        /**
                         * 2.初始化路径
                         */
                        Animator animator1 = createCurvePathAnimator(
                                image,
                                getUpReferencePoint(),
                                getDownReferencePoint(),
                                startPoint, endPoint,
                                path.duration,
                                path.supportRotate
                        );
                        animator1.setInterpolator(EffectsFactory.CreateInterpolator(path.interpolator));
                        Animator animator2 = createScaleAnim(image, path.fromScale, path.toScale, path.duration);
                        animator1.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));
                        AnimatorSet tempAnimator = new AnimatorSet();
                        tempAnimator.play(animator1).with(animator2);
                        pathAnimator.play(tempAnimator);
                    } else {
                        /**
                         * 不需要路径动画
                         */
                        Animator animator1 = createCurvePathAnimator(
                                image,
                                getUpReferencePoint(),
                                getDownReferencePoint(),
                                startPoint, endPoint,
                                path.duration,
                                path.supportRotate);
                        animator1.setInterpolator(EffectsFactory.CreateInterpolator(path.interpolator));
                        Animator animator2 = createScaleAnim(image, path.fromScale, path.toScale, path.duration);
                        AnimatorSet tempAnimator = new AnimatorSet();
                        tempAnimator.play(animator1).with(animator2);
                        pathAnimator.play(tempAnimator);
                    }
                }
                break;
                case 1:
                    /**
                     * 需要直线
                     */
                    if (path.anim != null) {
                        /**
                         * 需要路径动画,初始化路径动画
                         */
                        FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(image, path.anim.needRepeat);
                        faster.addAllFrames(path.anim.imageAndroid, path.anim.frameTime);
                        allFasterAnimByUrl.add(faster);
                        Animator animator1 = createStraightLineAnim(image, startPoint, endPoint, path.duration);
                        animator1.setInterpolator(EffectsFactory.CreateInterpolator(path.interpolator));
                        Animator animator2 = createScaleAnim(image, path.fromScale, path.toScale, path.duration);
                        animator1.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster));
                        AnimatorSet tempAnimator = new AnimatorSet();
                        tempAnimator.play(animator1).with(animator2);
                        pathAnimator.play(tempAnimator);
                    } else {
                        /**
                         * 不需要路径动画
                         */
                        Animator animator1 = createStraightLineAnim(image, startPoint, endPoint, path.duration);
                        animator1.setInterpolator(EffectsFactory.CreateInterpolator(path.interpolator));
                        Animator animator2 = createScaleAnim(image, path.fromScale, path.toScale, path.duration);
                        AnimatorSet tempAnimator = new AnimatorSet();
                        tempAnimator.play(animator1).with(animator2);
                        pathAnimator.play(tempAnimator);
                    }
                    break;
            }

            /**
             * 处理第二个点的动画
             */
            if (point2.anim != null && point2.anim.duration > 0) {
                Animator animator = createStaticAnim(image, point2.anim.duration, point2.scale);
                FasterAnimByCustomAnimFrame faster = FasterAnimByCustomAnimFrame.create(image, point2.anim.needRepeat);
                allFasterAnimByUrl.add(faster);
                faster.addAllFrames(point2.anim.imageAndroid, point2.anim.frameTime);
                animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(faster) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        createAndPostPointRemainAnim(context, endPoint, payload.endPosition, point2, payload);
                    }
                });
                animatorRightBuilder.after(animator);
            }

            animatorSet.play(pathAnimator).after(leftAnimator).before(rightAnimator);
            return createAnimationTask(image, animatorSet, allFasterAnimByUrl);
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    /**
     * 创建单点动画
     *
     * @param fromType
     * @param context
     * @param anim
     * @param payload
     * @return
     */
    public AnimationEvent.AnimationTaskEvent createOnePointAnim(String fromType, Context context, CustomGiftAnim anim, CustomGiftAnimPayload payload) {
        /**
         * 单点动画说明只会有一个点
         */
        if (anim.point.size() != 1) {
            return null;
        }
        CustomGiftAnim.CustomGiftAnimPoint point = anim.point.get(0);
        try {
            AnimatorSet animatorSet = new AnimatorSet();
            wrapPoint(payload.start);
            wrapPoint(payload.end);
            PointF resultPoint = EffectsFactory.GetPointByGiftAnimPoint(fromType, payload.start, payload.end, anim, payload, point); //等到计算后的点的坐标
            CustomGiftAnim.CustomAnim pointAnim = point.anim;
            ImageView image = null;

            if (payload.viewWidth == -1 && payload.viewHeight == -1) {
                image = createAnimImageView(context, resultPoint, CustomGiftAnim.DEFAULT_WIDTH, CustomGiftAnim.DEFAULT_HEIGHT);
            } else {
                image = createAnimImageView(context, resultPoint, payload.viewWidth, payload.viewHeight);
            }

            if (anim.defaultAndroidFrame != null) {
                /**
                 * 需要展示默认的图片
                 */
                EffectsMainFestManager.FillImageViewByGiftImageCacheUrl(image, anim.defaultAndroidFrame.local, anim.defaultAndroidFrame.remote);
            }

            Animator outAnimator = createAlphaOut(image, point.scale);
            Animator inAnimator = createAlphaIn(image, point.scale);


            if (pointAnim == null) {
                /**
                 * 说明单点不需要展示动画，这种情况下就展示一个图片显示
                 */
                animatorSet.play(inAnimator).after(outAnimator);
                return createAnimationTask(image, animatorSet);
            } else {
                FasterAnimByCustomAnimFrame animContainer = FasterAnimByCustomAnimFrame.create(image, pointAnim.needRepeat);
                animContainer.addAllFrames(pointAnim.imageAndroid, pointAnim.frameTime);
                long duration = pointAnim.duration;
                if (duration > 0) {
                    Animator animator = createStaticAnim(image, duration, point.scale);
                    animator.addListener(new EffectsFactory.AnimatorAdapterWithFasterAnim(animContainer) {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            createAndPostPointRemainAnim(context, resultPoint, payload.endPosition, point, payload);
                        }
                    });
                    animatorSet.play(animator).after(outAnimator).before(inAnimator);
                    return createAnimationTask(image, animatorSet, animContainer);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 创建自定义的动画View
     *
     * @param context
     * @param layout
     * @param pointF
     * @param width
     * @param height
     * @return
     */
    public View createCustomAnimView(Context context, int layout, PointF pointF, int width, int height) {
        View view = View.inflate(context, layout, null);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                width,
                height
        );
        view.setLayoutParams(layoutParams);
        view.setX(pointF.x);
        view.setY(pointF.y);
        return view;
    }

    /**
     * 创建一个含有默认图片的ImageView
     *
     * @param context
     * @param background
     * @param pointF
     * @param width
     * @param height
     * @return
     */
    public View createAnimImageView(Context context, int background, PointF pointF, int width, int height) {
        ImageView view = createAnimImageView(context, pointF, width, height);
        view.setBackgroundResource(background);
        return view;
    }

    /**
     * 创建一个动画ImageView
     *
     * @param context
     * @param start
     * @param width
     * @param height
     * @return
     */
    public ImageView createAnimImageView(Context context, PointF start, int width, int height) {
        ImageView targetView = new ImageView(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                width,
                height
        );
        targetView.setLayoutParams(layoutParams);
        targetView.setX(start.x);
        targetView.setY(start.y);
        targetView.setAdjustViewBounds(true);
        targetView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return targetView;
    }

    private AnimationEvent.AnimationTaskEvent createAnimationTask(View targetView, AnimatorSet animatorSet, List<IFasterAnim> all) {
        EffectsTaskManager.Task task = new EffectsTaskManager.Task();
        task.mAnimator = animatorSet;
        task.addIFasterAnim(all);
        task.mView = targetView;
        return new AnimationEvent.AnimationTaskEvent(task);
    }

    private AnimationEvent.AnimationTaskEvent createAnimationTask(View targetView, AnimatorSet animatorSet, IFasterAnim...
            containers) {
        EffectsTaskManager.Task task = new EffectsTaskManager.Task();
        task.mAnimator = animatorSet;
        task.addIFasterAnim(containers);
        task.mView = targetView;
        return new AnimationEvent.AnimationTaskEvent(task);
    }


    /**
     * 创建一个贝塞尔曲线动画
     *
     * @param view
     * @param startP1
     * @param startP2
     * @param startP
     * @param stopP
     * @param dur
     * @param supportRadio View是否需要随着路径进行旋转
     * @return
     */
    public ValueAnimator createCurvePathAnimator(
            @NonNull final View view,
            @Nullable PointF startP1,
            @Nullable PointF startP2,
            @Nullable PointF startP,
            @NonNull PointF stopP,
            long dur,
            boolean supportRadio
    ) {
        ValueAnimator valueAnimator = ValueAnimator.ofObject(new CurvePathEvaluator(startP1, startP2), startP, stopP);
        valueAnimator.setDuration(dur);
        valueAnimator.addUpdateListener(animation -> {

            float oldX = view.getX();
            float oldY = view.getY();

            PointF pointF = (PointF) animation.getAnimatedValue();

            float newX = pointF.x;
            float newY = pointF.y;

            view.setX(pointF.x);
            view.setY(pointF.y);

            if (supportRadio) {
                /**
                 * 以View作为原点
                 */
                float originX = oldX;
                float originY = oldY;

                float relativeX = newX - originX;
                float relativeY = newY - originY; //得到相对的坐标

                if (relativeX == 0 || relativeY == 0 /**|| Math.abs(relativeX) == Math.abs(relativeY)**/) {
                    /**
                     * 不需要旋转
                     */
                    return;
                }

                /**
                 * 需要旋转
                 */
                view.setRotation(ToolKit.getRotationBetweenPoint(oldX, oldY, newX, newY));
            }

        });
        return valueAnimator;
    }


    /**
     * 创建静止动画
     *
     * @param targetView
     * @param dur
     * @param scale
     * @return
     */
    public Animator createStaticAnim(View targetView, long dur, float scale) {
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("scaleX", scale, scale);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("scaleY", scale, scale);
        ObjectAnimator animatorScale = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhX, pvhY).setDuration(dur);
        return animatorScale;
    }

    /**
     * 创建旋转动画
     *
     * @param targetView
     * @param dur
     * @param fromDegree
     * @param toDegree
     * @return
     */
    public Animator createRotateAnim(View targetView, long dur, float fromDegree, float toDegree) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(targetView, View.ROTATION, fromDegree, toDegree);
        animator.setDuration(dur);
        return animator;
    }

    /**
     * 创建放大缩小动画
     *
     * @param targetView
     * @param fromScale
     * @param toScale
     * @param duration
     * @return
     */
    public Animator createScaleAnim(View targetView, float fromScale, float toScale, long duration) {
        PropertyValuesHolder pvhXl = PropertyValuesHolder.ofFloat(View.SCALE_X, fromScale, toScale);
        PropertyValuesHolder pvhYl = PropertyValuesHolder.ofFloat(View.SCALE_Y, fromScale, toScale);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhXl, pvhYl).setDuration(duration);
        return animator;
    }

    /**
     * 创建直线动画
     *
     * @param targetView
     * @param startPoint
     * @param endPoint
     * @param duration
     * @return
     */
    public Animator createStraightLineAnim(View targetView, PointF startPoint, PointF endPoint, long duration) {
        PropertyValuesHolder pvhXl = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, startPoint.x, endPoint.x);
        PropertyValuesHolder pvhYl = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, startPoint.y, endPoint.y);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(targetView, pvhXl, pvhYl).setDuration(duration);
        return animator;
    }

    /**
     * 贝塞尔曲线计算器
     */
    public static class CurvePathEvaluator implements TypeEvaluator<PointF> {

        private PointF pointF1, pointF2;

        public CurvePathEvaluator(PointF pointF1, PointF pointF2) {
            this.pointF1 = pointF1;
            this.pointF2 = pointF2;
        }

        @Override
        public PointF evaluate(float fraction, PointF startValue, PointF endValue) {
            float timeLeft = 1.0f - fraction;
            PointF pointF = new PointF();//结果
            pointF.x = timeLeft * timeLeft * timeLeft * (startValue.x)
                    + 3 * timeLeft * timeLeft * fraction * (pointF1.x)
                    + 3 * timeLeft * fraction * fraction * (pointF2.x)
                    + fraction * fraction * fraction * (endValue.x);

            pointF.y = timeLeft * timeLeft * timeLeft * (startValue.y)
                    + 3 * timeLeft * timeLeft * fraction * (pointF1.y)
                    + 3 * timeLeft * fraction * fraction * (pointF2.y)
                    + fraction * fraction * fraction * (endValue.y);
            return pointF;
        }
    }

}
