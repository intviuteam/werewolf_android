package orangelab.project.common.share;

import com.toolkit.action.Destroyable;

/**
 * game_werewolf
 * 2018/2/26 下午7:35
 * Mystery
 */

public interface Shareable<T> extends Destroyable {

    void make(CallBack<T> callBack);

}
