package orangelab.project.common.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.R;
import com.toolkit.ToolKit;
import com.toolkit.action.TravelCollection;
import com.toolkit.action.TravelCollectionByIterator;

import orangelab.project.common.model.UserVipInfo;
import orangelab.project.common.tool.UserRolesTools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * game_werewolf
 * 2017/10/24 上午11:54
 * Mystery
 */

public class FlowLayout extends ViewGroup {

    public interface FlowLayoutItemBackGround {
        int getBackGround(int index, int size);
    }

    //存储所有子View
    private List<List<View>> mAllChildViews = new ArrayList<>();

    //每一行的高度
    private List<Integer> mLineHeight = new ArrayList<>();

    private FlowLayoutItemBackGround mItemBackGround;

    public FlowLayout(Context context) {
        this(context, null);
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlowLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        // 获得它的父容器为它设置的测量模式和大小
        int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
        int sizeHeight = MeasureSpec.getSize(heightMeasureSpec);
        int modeWidth = MeasureSpec.getMode(widthMeasureSpec);
        int modeHeight = MeasureSpec.getMode(heightMeasureSpec);

        // 如果是warp_content情况下，记录宽和高
        int width = 0;
        int height = 0;
        /**
         * 记录每一行的宽度，width不断取最大宽度
         */
        int lineWidth = 0;
        /**
         * 每一行的高度，累加至height
         */
        int lineHeight = 0;

        int cCount = getChildCount();

        // 遍历每个子元素
        for (int i = 0; i < cCount; i++) {
            View child = getChildAt(i);
            // 测量每一个child的宽和高
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            // 得到child的lp
            MarginLayoutParams lp = (MarginLayoutParams) child
                    .getLayoutParams();
            // 当前子空间实际占据的宽度
            int childWidth = child.getMeasuredWidth() + lp.leftMargin
                    + lp.rightMargin;
            // 当前子空间实际占据的高度
            int childHeight = child.getMeasuredHeight() + lp.topMargin
                    + lp.bottomMargin;
            /**
             * 如果加入当前child，则超出最大宽度，则的到目前最大宽度给width，类加height 然后开启新行
             */
            if (lineWidth + childWidth > sizeWidth) {
                width = Math.max(lineWidth, childWidth);// 取最大的
                lineWidth = childWidth; // 重新开启新行，开始记录
                // 叠加当前高度，
                height += lineHeight;
                // 开启记录下一行的高度
                lineHeight = childHeight;
            } else
            // 否则累加值lineWidth,lineHeight取最大高度
            {
                lineWidth += childWidth;
                lineHeight = Math.max(lineHeight, childHeight);
            }
            // 如果是最后一个，则将当前记录的最大宽度和当前lineWidth做比较
            if (i == cCount - 1) {
                width = Math.max(width, lineWidth);
                height += lineHeight;
            }

        }
        setMeasuredDimension((modeWidth == MeasureSpec.EXACTLY) ? sizeWidth
                : width, (modeHeight == MeasureSpec.EXACTLY) ? sizeHeight
                : height);
//
//        //父控件传进来的宽度和高度以及对应的测量模式
//        int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
//        int modeWidth = MeasureSpec.getMode(widthMeasureSpec);
//        int sizeHeight = MeasureSpec.getSize(heightMeasureSpec);
//        int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
//
//        //如果当前ViewGroup的宽高为wrap_content的情况
//        int width = 0;//自己测量的 宽度
//        int height = 0;//自己测量的高度
//        //记录每一行的宽度和高度
//        int lineWidth = 0;
//        int lineHeight = 0;
//
//        //获取子view的个数
//        int childCount = getChildCount();
//        for (int i = 0; i < childCount; i++) {
//            View child = getChildAt(i);
//            //测量子View的宽和高
//            measureChild(child, widthMeasureSpec, heightMeasureSpec);
//            //得到LayoutParams
//            MarginLayoutParams lp = (MarginLayoutParams) getLayoutParams();
//            //子View占据的宽度
//            int childWidth = child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin;
//            //子View占据的高度
//            int childHeight = child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin;
//            //换行时候
//            if (lineWidth + childWidth > sizeWidth) {
//                //对比得到最大的宽度
//                width = Math.max(width, lineWidth);
//                //重置lineWidth
//                lineWidth = childWidth;
//                //记录行高
//                height += lineHeight;
//                lineHeight = childHeight;
//            } else {//不换行情况
//                //叠加行宽
//                lineWidth += childWidth;
//                //得到最大行高
//                lineHeight = Math.max(lineHeight, childHeight);
//            }
//            //处理最后一个子View的情况
//            if (i == childCount - 1) {
//                width = Math.max(width, lineWidth);
//                height += lineHeight + 2 * lp.bottomMargin;
//            }
//        }
////        //wrap_content
//        setMeasuredDimension(modeWidth == MeasureSpec.EXACTLY ? sizeWidth : width,
//                modeHeight == MeasureSpec.EXACTLY ? sizeHeight : height);
    }

    public void setItemBackGround(FlowLayoutItemBackGround itemBackGround) {
        mItemBackGround = itemBackGround;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mAllChildViews.clear();
        mLineHeight.clear();
        //获取当前ViewGroup的宽度
        int width = getWidth();

        int lineWidth = 0;
        int lineHeight = 0;
        //记录当前行的view
        List<View> lineViews = new ArrayList<View>();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
            int childWidth = child.getMeasuredWidth();
            int childHeight = child.getMeasuredHeight();

            //如果需要换行
            if (childWidth + lineWidth + lp.leftMargin + lp.rightMargin > width) {
                //记录LineHeight
                mLineHeight.add(lineHeight);
                //记录当前行的Views
                mAllChildViews.add(lineViews);
                //重置行的宽高
                lineWidth = 0;
                lineHeight = childHeight + lp.topMargin + lp.bottomMargin;
                //重置view的集合
                lineViews = new ArrayList();
            }
            lineWidth += childWidth + lp.leftMargin + lp.rightMargin;
            lineHeight = Math.max(lineHeight, childHeight + lp.topMargin + lp.bottomMargin);
            lineViews.add(child);
        }
        //处理最后一行
        mLineHeight.add(lineHeight);
        mAllChildViews.add(lineViews);

        //设置子View的位置
        int left = 0;
        int top = 0;
        //获取行数
        int lineCount = mAllChildViews.size();
        for (int i = 0; i < lineCount; i++) {
            //当前行的views和高度
            lineViews = mAllChildViews.get(i);
            lineHeight = mLineHeight.get(i);
            for (int j = 0; j < lineViews.size(); j++) {
                View child = lineViews.get(j);
                //判断是否显示
                if (child.getVisibility() == View.GONE) {
                    continue;
                }
                MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
                int cLeft = left + lp.leftMargin;
                int cTop = top + lp.topMargin;
                int cRight = cLeft + child.getMeasuredWidth();
                int cBottom = cTop + child.getMeasuredHeight();
                //进行子View进行布局
                child.layout(cLeft, cTop, cRight, cBottom);
                left += child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin;
            }
            left = 0;
            top += lineHeight;
        }

    }

    public void addTextViewsWithVipInfo(List<String> strings, int textColor, int textSize, boolean clear, UserVipInfo vipInfo) {
        /**
         * 2018。04。27 由于服务器此处返回的Strings 里面有可能里面有空字符串，所以这里做一个过滤,清除空字符串
         */
        ToolKit.travelCollectionByIterator(strings, (iterator, data) -> {
            if (TextUtils.isEmpty(data)) {
                iterator.remove();
            }
        });
        if (ToolKit.isEmpty(strings)) {
            removeAllViews();
            return;
        }

        if (clear) {
            removeAllViews();
        }
        boolean hasVip = false;
        if (vipInfo != null && vipInfo.active && vipInfo.lvl > 0) {
            String vipInfoString = "VIP " + vipInfo.lvl;
            hasVip = true;
            strings.add(0, vipInfoString);
        }

        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.leftMargin = 5;
        lp.rightMargin = 5;
        lp.topMargin = 5;
        lp.bottomMargin = 5;
        int size = strings.size();

        for (int i = 0; i < size; i++) {
            TextView view = new TextView(getContext());
            if (hasVip && i == 0) {
                view.setText(strings.get(i));
                view.setTextColor(Color.parseColor("#ffd800"));
                view.setBackgroundResource(UserRolesTools.getVipUserRolesBackGround());
            } else {
                view.setText(strings.get(i));
                view.setTextColor(textColor);
                if (mItemBackGround != null) {
                    view.setBackgroundResource(mItemBackGround.getBackGround(i, size));
                } else {
                    view.setBackgroundResource(UserRolesTools.getUserRolesBackGround(i, size));
                }
            }
            view.setLayoutParams(lp);
            view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
            addView(view);
        }
        requestLayout();
    }

    public void addTextViews(List<String> strings, int textColor, int textSize, boolean clear) {

        /**
         * 2018。04。27 由于服务器此处返回的Strings 里面有可能里面有空字符串，所以这里做一个过滤,清除空字符串
         */
        ToolKit.travelCollectionByIterator(strings, (iterator, data) -> {
            if (TextUtils.isEmpty(data)) {
                iterator.remove();
            }
        });
        if (ToolKit.isEmpty(strings)) {
            removeAllViews();
            return;
        }

        if (clear) {
            removeAllViews();
        }
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.leftMargin = 5;
        lp.rightMargin = 5;
        lp.topMargin = 5;
        lp.bottomMargin = 5;
        int size = strings.size();
        for (int i = 0; i < size; i++) {
            TextView view = new TextView(getContext());
            view.setText(strings.get(i));
            view.setTextColor(textColor);
            if (mItemBackGround != null) {
                view.setBackgroundResource(mItemBackGround.getBackGround(i, size));
            } else {
                view.setBackgroundResource(UserRolesTools.getUserRolesBackGround(i, size));
            }
            view.setLayoutParams(lp);
            view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
            addView(view);
        }
        requestLayout();
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }
}
