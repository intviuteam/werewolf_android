package orangelab.project.common.model;

import java.util.List;

/**
 * game_werewolf
 * 2017/8/11 下午7:36
 * Mystery
 */

public class PurchaseResult {

    public List<PurchaseResultItem> purchase;

    @Override
    public String toString() {
        return "PurchaseResult{" +
                "purchase=" + purchase +
                '}';
    }

    public static class PurchaseResultItem {
        private int op_result;
        private String orderId;


        public int getOp_result() {
            return op_result;
        }

        public void setOp_result(int op_result) {
            this.op_result = op_result;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        @Override
        public String toString() {
            return "PurchaseResultItem{" +
                    "op_result=" + op_result +
                    ", orderId='" + orderId + '\'' +
                    '}';
        }
    }
}
