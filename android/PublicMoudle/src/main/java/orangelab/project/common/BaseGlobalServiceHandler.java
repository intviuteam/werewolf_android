package orangelab.project.common;


/**
 * game_werewolf
 * 2018/1/30 下午2:41
 * Mystery
 */

public abstract class BaseGlobalServiceHandler implements IGlobalServiceHandler {

    protected IGlobalService mGlobalService;

    public BaseGlobalServiceHandler(IGlobalService globalService) {
        mGlobalService = globalService;
    }

    @Override
    public boolean isCareType(String type) {
        return false;
    }

    @Override
    public void handleMessage(String type, String payload) {

    }

    @Override
    public void destroy() {
        mGlobalService = null;
    }
}
