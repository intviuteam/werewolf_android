package orangelab.project.common.floatwindow;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.androidtoolkit.view.ScreenUtils;
import com.R;

/**
 * Created by lloydfinch on 13/10/2017.
 */

public class FloatWindowPermissionDialog extends Dialog {

    private View btnSure;
    private View btnCancel;

    private Context mContext;
    private View.OnClickListener confirmClickListener;

    public FloatWindowPermissionDialog(@NonNull Context context, View.OnClickListener confirm) {
        super(context, R.style.DarkDialog);
        this.mContext = context;
        this.confirmClickListener = confirm;

        setCancelable(false);
        setCanceledOnTouchOutside(false);
        init();
    }

    private void init() {
        initView();
        initWindow();
        initListener();
    }

    private void initView() {
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_dialog_float_window_permission, null);
        contentView.setFocusable(true);
        contentView.setFocusableInTouchMode(true);
        setContentView(contentView);

        btnSure = contentView.findViewById(R.id.btn_sure);
        btnCancel = contentView.findViewById(R.id.btn_cancel);
    }

    private void initWindow() {
        int dialogMaxWidth = ScreenUtils.dip2px(340);
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        int dur = (int) (ScreenUtils.getScreenWidth() * 0.25f);
        if (ScreenUtils.getScreenWidth() - dialogMaxWidth > dur) {
            params.width = dialogMaxWidth;
        } else {
            params.width = (int) (ScreenUtils.getScreenWidth() * 0.95f);
        }
        getWindow().setAttributes(params);
    }

    private void initListener() {
        btnSure.setOnClickListener(confirmClickListener == null ? v -> dismiss() : confirmClickListener);
        btnCancel.setOnClickListener(v -> dismiss());
    }
}
