package orangelab.project.common.exhibition.gift;

import android.text.TextUtils;

import com.androidtoolkit.RxToolKit;
import orangelab.project.common.exhibition.ExhibitionPackage;
import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.exhibition.IExhibition;

/**
 * game_werewolf
 * 2017/6/21 下午1:04
 * Mystery
 */

public class Gift implements IExhibition {


    private String mGiftType;
    private String mGiftName;
    private String mGiftPriceType;
    private int mGiftPrice = 0;
    private int mPopular = 0;
    private String mGiftTicket = "";
    private String mGiftDisCount = "";


    public Gift(String giftType, String giftPriceType, int giftPrice) {
        mGiftType = giftType;
        mGiftPriceType = giftPriceType;
        mGiftPrice = giftPrice;
    }

    public void setGiftName(String giftName) {
        mGiftName = giftName;
    }

    @Override
    public String getExhibitionType() {
        return ExhibitionsConstant.EXHIBITIONS_TYPE_GIFT;
    }

    @Override
    public String getRealType() {
        return mGiftType;
    }

    @Override
    public int getExhibitionBackGroundResource() {
        return -1;
    }

    @Override
    public String getExhibitionName() {
        return mGiftName;
    }

    @Override
    public String getExhibitionPriceType() {
        return mGiftPriceType;
    }

    @Override
    public int getExhibitionPrice() {
        return mGiftPrice;
    }

    @Override
    public void present(ExhibitionPackage exhibitionPackage) {
        if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_GAME)) {
            /**
             * 游戏中送礼物
             */
            RxToolKit.Emit(new GiftEvent.GiftForGameWrapperEvent(Gift.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_CHAT)) {
            /**
             * 聊天中送礼物
             */
            RxToolKit.Emit(new GiftEvent.GiftForChatWrapperEvent(Gift.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_MAIN_PAGE)) {
            /**
             * 个人详情页面送礼物
             */
            RxToolKit.Emit(new GiftEvent.GiftForMainPageWrapperEvent(Gift.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_OUT_SIDER)) {
            /**
             * 外部送礼物
             */
            RxToolKit.Emit(new GiftEvent.GiftForOutSideWrapperEvent(Gift.this, exhibitionPackage));
        }
    }

    @Override
    public int getExhibitionCount() {
        return ExhibitionsConstant.PRICE_NONE;
    }

    @Override
    public void setExhibitionTicket(String ticket) {
        this.mGiftTicket = ticket;
    }

    @Override
    public void setPopularValue(int value) {
        this.mPopular = value;
    }

    @Override
    public int getPopularValue() {
        return mPopular;
    }

    @Override
    public String getDisCount() {
        return mGiftDisCount;
    }

    @Override
    public void setDisCount(String disCount) {
        mGiftDisCount = disCount;
    }

    @Override
    public void decCount() {

    }

    public String getGiftType() {
        return mGiftType;
    }

    public String getGiftTicket() {
        return mGiftTicket;
    }
}
