package orangelab.project.common.effect.manifest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import com.androidtoolkit.CodeTimeRecord;
import com.androidtoolkit.PLog;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.gifts.CustomGiftAnim;
import orangelab.project.common.exhibition.gift.model.GiftManifest;
import orangelab.project.common.exhibition.gift.model.GiftManifestItem;
import orangelab.project.common.tool.GsonTurnToolKit;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2018/7/13 上午10:20
 * Mystery
 */
public class GiftsManifestHolder extends EffectsManifestHolder<GiftManifest> {

    private static final String TAG = "GiftsManifestHolder";

    public GiftsManifestHolder(ManifestManagerHelper helper, String cacheManifestFile, String cacheManifestVersionFile, String diskLruType) {
        super(helper, cacheManifestFile, cacheManifestVersionFile, diskLruType);
    }

    @Override
    public GiftManifest transformData(String data) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        return GsonTurnToolKit.turn(data, GiftManifest.class);
    }

    @Override
    public void saveManifest(GiftManifest data) {
        try {
            String temp = GsonHelper.getGson().toJson(data);
            mHelper.putString(mCacheManifestFile, temp);
            putVersion(data.rev);
            updateManifest(data);
            PLog.i(TAG, "save giftsManifest: success");
        } catch (Exception e) {
            e.printStackTrace();
            PLog.i(TAG, "save giftsManifest: failed");
        }
    }

    @Override
    public Set<String> verifyManifest(GiftManifest manifest) {
        CodeTimeRecord.iRecord.recordCodeStart("verifyGiftManifestData");
        PLog.i(TAG, "start verify giftsManifest ---");
        Set<String> failedGiftType = new HashSet<>();
        if (manifest == null) {
            return failedGiftType;
        }
        List<GiftManifestItem> data = manifest.gifts;
        try {
            if (Utils.targetListIsNull(data)) {
                return null;
            }
            for (GiftManifestItem item : data) {
                if (!TextUtils.isEmpty(item.localImage)) {
                    if (ApkResourceManager.GetApkImageResource(item.localImage) > 0) {
                        /**
                         * 本地有就不进行远程校验
                         */
                    } else {
                        if (!verifyUrlData(item.image)) {
                            failedGiftType.add(item.gift_type);
                        }
                    }
                }

                if (item.status != 1) {
                    try {
                        PLog.i(TAG, "This gift [" + item.gift_type + "] has been sold out，so we don't check it anim");
                    } catch (Exception e) {
                        /**
                         * we don't need report this error
                         */
                    }
                    continue;
                }
                /**
                 * item的遍历
                 */
                if (item.animate != null) {
                    /**
                     * 验证默认图片
                     */
                    if (!TextUtils.isEmpty(item.animate.defaultAndroidFrame.local)) {
                        if (ApkResourceManager.GetApkImageResource(item.animate.defaultAndroidFrame.local) > 0) {

                        } else {
                            if (!verifyUrlData(item.animate.defaultAndroidFrame.remote)) {
                                failedGiftType.add(item.gift_type);
                            }
                        }
                    }

                    /**
                     * 验证点
                     */
                    if (!Utils.targetListIsNull(item.animate.point)) {
                        List<CustomGiftAnim.CustomGiftAnimPoint> point = item.animate.point;
                        for (CustomGiftAnim.CustomGiftAnimPoint temp : point) {
                            if (temp.anim != null) {
                                if (!verifyAnimFrameData(temp.anim.imageAndroid)) {
                                    /**
                                     * 验证失败,记录失败的礼物类型
                                     */
                                    failedGiftType.add(item.gift_type);
                                }
                            }
                            /**
                             * 验证残留动画资源是否完整
                             */
                            if (temp.remainAnim != null) {
                                if (!verifyAnimFrameData(temp.remainAnim.imageAndroid)) {
                                    /**
                                     * 验证失败，记录失败的礼物类型
                                     */
                                    failedGiftType.add(item.gift_type);
                                }
                            }
                        }
                    }


                    /**
                     * 验证路径
                     */
                    if (!Utils.targetListIsNull(item.animate.path)) {
                        List<CustomGiftAnim.CustomGiftAnimPath> path = item.animate.path;
                        for (CustomGiftAnim.CustomGiftAnimPath temp : path) {
                            if (temp.anim != null) {
                                if (!verifyAnimFrameData(temp.anim.imageAndroid)) {
                                    /**
                                     * 验证失败,记录失败的礼物类型
                                     */
                                    failedGiftType.add(item.gift_type);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (failedGiftType.size() > 0) {
            PLog.i(TAG, "礼物列表验证失败 失败的礼物为" + failedGiftType.toString());
        } else {
            PLog.i(TAG, "礼物列表验证成功---------------------------------------------------------------------------------------------------------");
        }
        CodeTimeRecord.iRecord.recordCodeEnd("verifyGiftManifestData");
        return failedGiftType;
    }

    private boolean verifyAnimFrameData(List<CustomGiftAnim.CustomAnimFrame> frames) {
        if (Utils.targetListIsNull(frames)) {
            return false;
        }
        for (CustomGiftAnim.CustomAnimFrame temp : frames) {
            if (ApkResourceManager.GetApkImageResource(temp.local) > 0) {
                continue;
            }
            if (verifyUrlData(temp.remote)) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    public void downloadManifestResource(GiftManifest data, boolean forceRefresh) {
        if (data == null) return;
        List<GiftManifestItem> giftManifestData = data.gifts;
        /**
         * 下载并保存所有清单文件用需要的数据
         */
        for (GiftManifestItem item : giftManifestData) {

            downloadAndSaveIfNeed(item.localImage, item.image, forceRefresh);

            if (item.status != 1) {
                try {
                    PLog.i(TAG, "This gift [" + item.gift_type + "] has been sold out,so we don't download it");
                } catch (Exception e) {

                }
                continue;
            }

            if (item.animate != null) {
                CustomGiftAnim tempGiftAnim = item.animate;

                downloadAndSaveIfNeed(tempGiftAnim.defaultAndroidFrame.local, tempGiftAnim.defaultAndroidFrame.remote, forceRefresh);

                if (!Utils.targetListIsNull(tempGiftAnim.point)) {
                    List<CustomGiftAnim.CustomGiftAnimPoint> pointList = tempGiftAnim.point;
                    for (CustomGiftAnim.CustomGiftAnimPoint tempPoint : pointList) {
                        if (tempPoint.anim != null) {
                            CustomGiftAnim.CustomAnim tempAnim = tempPoint.anim;
                            if (!Utils.targetListIsNull(tempAnim.imageAndroid)) {
                                for (CustomGiftAnim.CustomAnimFrame frame : tempAnim.imageAndroid) {
                                    downloadAndSaveIfNeed(frame.local, frame.remote, forceRefresh);
                                }
                            }
                        }

                        if (tempPoint.remainAnim != null) {
                            CustomGiftAnim.CustomRemainAnim tempAnim = tempPoint.remainAnim;
                            if (!Utils.targetListIsNull(tempAnim.imageAndroid)) {
                                for (CustomGiftAnim.CustomAnimFrame frame : tempAnim.imageAndroid) {
                                    downloadAndSaveIfNeed(frame.local, frame.remote, forceRefresh);
                                }
                            }
                        }
                    }
                }

                if (!Utils.targetListIsNull(tempGiftAnim.path)) {
                    List<CustomGiftAnim.CustomGiftAnimPath> pathList = tempGiftAnim.path;
                    for (CustomGiftAnim.CustomGiftAnimPath tempPoint : pathList) {
                        if (tempPoint.anim != null) {
                            CustomGiftAnim.CustomAnim tempAnim = tempPoint.anim;
                            if (!Utils.targetListIsNull(tempAnim.imageAndroid)) {
                                for (CustomGiftAnim.CustomAnimFrame frame : tempAnim.imageAndroid) {
                                    downloadAndSaveIfNeed(frame.local, frame.remote, forceRefresh);
                                }
                            }
                        }
                    }
                }
            }
        }
        PLog.i(TAG, "we finish all need image download");
    }

    public void fillImageView(ImageView imageView, String local, String remote) {
        /**
         * 先去本地判断
         */
        int localResource = ApkResourceManager.GetApkImageResource(imageView.getContext(), local);
        if (localResource > 0) {
            imageView.setImageResource(localResource);
            return;
        }

        /**
         * 再去网络查看
         */
        InputStream inputStream = getInputStream(remote);
        if (inputStream != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = false;
            options.inSampleSize = 1;
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
            imageView.setImageBitmap(bitmap);
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public CustomGiftAnim getManifestItemAnim(String giftType) {
        if (TextUtils.isEmpty(giftType)) {
            return null;
        }
        /**
         * 这里要使用缓存的一个列表清单
         */
        GiftManifest giftManifest = getManifestData();
        if (giftManifest == null) {
            /**
             * 缓存为空，暂时返回为空
             */
            return null;
        } else {
            return giftManifest.find(giftType).animate;
        }
    }


}
