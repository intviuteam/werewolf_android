package orangelab.project.common.share;

import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import orangelab.project.MainApplication;

/**
 * 分享文案的json实体，适用于狼人杀和快玩
 * app启动的时候，就去获取并解析
 * 此实体需要根据后台分享定期更新(添加房间类型)
 */
public class PublicShareCase implements Serializable, Keepable {

    public int code;
    public OuterData data;

    @Override
    public String toString() {
        return "PublicShareCase{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }

    public static class OuterData implements Serializable, Keepable {
        public String version;
        public Data data;


        @Override
        public String toString() {
            return "OuterData{" +
                    "version='" + version + '\'' +
                    ", data=" + data +
                    '}';
        }
    }

    public static class Data implements Serializable, Keepable {
        public List<ShareBean> app_share;                               //房间外部分享
        public List<ShareBean> game_room;                               //游戏房分享
        public List<ShareBean> audio_room;                              //语音房分享
        public List<ShareBean> mini_game;                               //小游戏房分享
        public List<ShareBean> private_room;                            //私密房分享
        public List<ShareBean> undercover_room;                         //语音房谁是卧底分享
        public List<ShareBean> undercover_room_new;                     //谁是卧底房分享
        public List<ShareBean> mini_game_lobby;                         //小游戏大厅分享

        @Override
        public String toString() {
            return "Data{" +
                    "app_share=" + app_share +
                    ", game_room=" + game_room +
                    ", audio_room=" + audio_room +
                    ", mini_game=" + mini_game +
                    ", private_room=" + private_room +
                    ", undercover_room=" + undercover_room +
                    ", undercover_room_new=" + undercover_room_new +
                    ", mini_game_lobby=" + mini_game_lobby +
                    '}';
        }
    }

    /**
     * 不同房间分享的公共实体
     */
    public static class ShareBean implements Serializable, Keepable {
        public String title = "";
        public String content = "";
        public String title_zh_TW = "";
        public String content_zh_TW = "";
        public String link = "";
        public String share_icon = "";
        public String share_case_id = "";
        public long total_clicked = 0;

        public String getLocaleTitle() {
            Locale locale = Locale.getDefault();
            if (locale.equals(Locale.CHINA)) {
                return title;
            } else {
                return title_zh_TW;
            }
        }

        public String getLocaleContent() {
            Locale locale = Locale.getDefault();
            if (locale.equals(Locale.CHINA)) {
                return content;
            } else {
                return content_zh_TW;
            }
        }

        @Override
        public String toString() {
            return "ShareBean{" +
                    "title='" + title + '\'' +
                    ", content='" + content + '\'' +
                    ", title_zh_TW='" + title_zh_TW + '\'' +
                    ", content_zh_TW='" + content_zh_TW + '\'' +
                    ", link='" + link + '\'' +
                    ", share_icon='" + share_icon + '\'' +
                    ", share_case_id='" + share_case_id + '\'' +
                    ", total_clicked=" + total_clicked +
                    '}';
        }
    }
}
