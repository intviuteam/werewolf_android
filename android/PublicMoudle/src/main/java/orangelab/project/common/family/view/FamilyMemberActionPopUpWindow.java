package orangelab.project.common.family.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.hint.SafePopWindow;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.view.ArrowRectangleViewGroup;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;

/**
 * game_werewolf
 * 2017/10/19 下午4:12
 * Mystery
 */

public class FamilyMemberActionPopUpWindow extends SafePopWindow implements View.OnClickListener {

    private Button mMemberDetail;
    private Button mMemberTransfer;
    private Button mMemberKickOut;
    private Button mMemberRename;

    private ArrowRectangleViewGroup mContainer;

    private int mWidth;
    private int mHeight;
    private Config mConfig;

    private Context mContext;
    private View mTargetView;
    private View mReferenceView;


    private FamilyMemberActionPopUpWindow(Context context, Config config) {
        super(context);
        mConfig = config;
        mContext = context;
        mContainer = (ArrowRectangleViewGroup) View.inflate(context, R.layout.layout_popwindow_family_member_action, null);
        mMemberDetail = (Button) mContainer.findViewById(R.id.id_family_members_action_check);
        mMemberKickOut = (Button) mContainer.findViewById(R.id.id_family_members_action_kickout);
        mMemberTransfer = (Button) mContainer.findViewById(R.id.id_family_members_action_transfer);
        mMemberRename = (Button) mContainer.findViewById(R.id.id_family_members_action_rename);
        initView(mConfig);
        mContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mWidth = ScreenUtils.dip2px(135);
        mHeight = mContainer.getMeasuredHeight();
        setContentView(mContainer);
        setWidth(ScreenUtils.dip2px(135));
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initView(Config config) {
        if (config.type == Config.FOR_MEMBERS_ACTION) {
            mMemberDetail.setVisibility(View.VISIBLE);

            if (FamilyMemo.IsMyFamily(config.targetGroupId)) {
                if (FamilyMemo.iMemo.isOwner()) {
                    mMemberKickOut.setVisibility(View.VISIBLE);
                    mMemberRename.setVisibility(View.VISIBLE);
                } else {
                    mMemberKickOut.setVisibility(View.GONE);
                    mMemberRename.setVisibility(View.GONE);
                }
                mMemberTransfer.setVisibility(View.GONE);
            } else {
                mMemberTransfer.setVisibility(View.GONE);
                mMemberKickOut.setVisibility(View.GONE);
                mMemberRename.setVisibility(View.GONE);
            }
        }

        if (config.type == Config.FOR_TRANSFER_BOSS) {
            mMemberDetail.setVisibility(View.GONE);
            mMemberKickOut.setVisibility(View.GONE);
            mMemberRename.setVisibility(View.GONE);
            mMemberTransfer.setVisibility(View.VISIBLE);
        }

        if (TextUtils.equals(config.targetUserId, GlobalUserState.getGlobalState().getUserId())) {
            /**
             * 如果是自己，那么只能查看信息
             */
            mMemberDetail.setVisibility(View.VISIBLE);
            mMemberRename.setVisibility(View.GONE);
            mMemberKickOut.setVisibility(View.GONE);
            mMemberTransfer.setVisibility(View.GONE);
        }

        mMemberDetail.setOnClickListener(this);
        mMemberKickOut.setOnClickListener(this);
        mMemberTransfer.setOnClickListener(this);
        mMemberRename.setOnClickListener(this);
    }

    public void setTargetView(View targetView) {
        mTargetView = targetView;
    }

    public void setReferenceView(View referenceView) {
        mReferenceView = referenceView;
    }

    public int getRealHeight() {
        return mHeight;
    }

    public int getRealWidth() {
        return mWidth;
    }

    public void setArrowDirection(int direction) {
        mContainer.setArrowDirection(direction);
    }

    public static void ShowAtView(View targetView, View referenceView, String userId, int intentType, String targetGroupId) {
        int[] location = new int[2];
        referenceView.getLocationOnScreen(location);


        int viewX = location[0];
        int viewY = location[1];

        int curScreenWidth = ScreenUtils.getCurrentScreenWidth();
        int curScreenHeight = ScreenUtils.getCurrentScreenHeight();

        Config builder = new Config.Builder().targetUserId(userId).targetGroupId(targetGroupId).type(intentType).build();
        FamilyMemberActionPopUpWindow popUpWindow = new FamilyMemberActionPopUpWindow(targetView.getContext(), builder);
        popUpWindow.setTargetView(targetView);
        popUpWindow.setReferenceView(referenceView);
        int popWindowWidth = popUpWindow.getRealWidth();
        int popWindowHeight = popUpWindow.getRealHeight();

        int dif = viewY + popWindowHeight - curScreenHeight;
        int minDif = ScreenUtils.dip2px(55); //最低差值
        int margin = ScreenUtils.dip2px(6);
        int showX = viewX + referenceView.getWidth() + margin;

        if (dif > 0) {
            /**
             * 此时PopWindow已经显示不开了
             */
            if (dif >= minDif + margin) {
                /**
                 * 暴露出来的空间太小，直接忽略
                 */
                popUpWindow = null;
                return;
            } else {
                popUpWindow.setArrowDirection(ArrowRectangleViewGroup.ARROW_LEFT_BOTTOM);
                popUpWindow.showAtLocation(targetView, Gravity.NO_GRAVITY, showX, viewY - dif - margin);
            }
        } else {
            /**
             * 此时PopWindow是可以正常显示的
             */
            popUpWindow.setArrowDirection(ArrowRectangleViewGroup.ARROW_LEFT_TOP);
            popUpWindow.showAtLocation(targetView, Gravity.NO_GRAVITY, showX, viewY + margin);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_family_members_action_check) {
            if (mContext != null) {
                PersonalInfoActivity.Launch(mContext, mConfig.targetUserId, false, false);
                dismiss();
            }
        } else if (id == R.id.id_family_members_action_kickout) {
            /**
             * 踢出家族
             */
            FamilyApiManager.KickOutMembersFromFamily(mConfig.targetUserId, (s, e) -> {
                if (e == null) {
                    /**
                     * 踢出成功
                     */
                    runSafely(() -> {
                        NotificationUtils.INSTANCE.clearNotification(mContext, mConfig.targetUserId);
                        dismiss();
                        RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_kick_out_family_member_success)).run();
                        RxToolKit.Emit(new FamilyEvent.KickOutEvent(mConfig.targetUserId));
                        RxToolKit.Emit(new FamilyEvent.RefreshFamilyInfoEvent());

                    });
                } else {
                    runSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
                }
            });
        } else if (id == R.id.id_family_members_action_transfer) {
            /**
             * 转让家族
             */
            FamilyApiManager.TransferFamilyBoss(mConfig.targetUserId, (s, e) -> {
                if (e == null) {
                    runSafely(() -> {
                        dismiss();
                        RxToolKit.Emit(new FamilyEvent.TransferEvent(mConfig.targetUserId));
                        FamilyMemo.iMemo.updateOwner(false);
                        RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_transfer_family_success)).run();
                    });
                } else {
                    runSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
                }
            });
        } else if (id == R.id.id_family_members_action_rename) {
            /**
             * 设置家族称谓
             */
            FamilyMemberReNamePopUpWindow.ShowAtView(mTargetView, mReferenceView, mConfig.targetUserId);
            dismiss();
        }
//        switch (v.getId()) {
//            case R.id.id_family_members_action_check: {
//                if (mContext != null) {
//                    PersonalInfoActivity.Launch(mContext, mConfig.targetUserId, false, false);
//                    dismiss();
//                }
//            }
//            break;
//            case R.id.id_family_members_action_kickout: {
//                /**
//                 * 踢出家族
//                 */
//                FamilyApiManager.KickOutMembersFromFamily(mConfig.targetUserId, (s, e) -> {
//                    if (e == null) {
//                        /**
//                         * 踢出成功
//                         */
//                        runSafely(() -> {
//                            NotificationUtils.INSTANCE.clearNotification(mContext, mConfig.targetUserId);
//                            dismiss();
//                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_kick_out_family_member_success)).run();
//                            RxToolKit.Emit(new FamilyEvent.KickOutEvent(mConfig.targetUserId));
//                            RxToolKit.Emit(new FamilyEvent.RefreshFamilyInfoEvent());
//
//                        });
//                    } else {
//                        runSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
//                    }
//                });
//            }
//            break;
//            case R.id.id_family_members_action_transfer: {
//
//            }
//            break;
//            case R.id.id_family_members_action_rename: {
//                /**
//                 * 设置家族称谓
//                 */
//                FamilyMemberReNamePopUpWindow.ShowAtView(mTargetView, mReferenceView, mConfig.targetUserId);
//                dismiss();
//            }
//            break;
//        }
    }

    @Override
    protected void release() {
        mContext = null;
        mTargetView = null;
        mReferenceView = null;
    }


    public static class Config {
        public static final int FOR_MEMBERS_ACTION = 0;
        public static final int FOR_TRANSFER_BOSS = 1;
        public int type;
        public String targetUserId;
        public String targetGroupId;

        private Config(Builder builder) {
            type = builder.type;
            targetUserId = builder.targetUserId;
            targetGroupId = builder.targetGroupId;
        }


        public static final class Builder {
            private int type;
            private String targetUserId;
            private String targetGroupId;

            public Builder() {
            }

            public Builder type(int val) {
                type = val;
                return this;
            }

            public Builder targetUserId(String val) {
                targetUserId = val;
                return this;
            }

            public Builder targetGroupId(String val) {
                targetGroupId = val;
                return this;
            }

            public Config build() {
                return new Config(this);
            }
        }
    }


}
