package orangelab.project.common.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/10/31 下午3:50
 * Mystery
 */

public class UpLoadFinishItem implements Keepable {

    public String url;
    public String key;
}
