package orangelab.project.common.model;

import com.networktoolkit.transport.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Author:1727
 * 2017/3/2
 */

public class ServerResponseEvent extends BaseServerEvent {

    public int code;
    public String request_id;
    public String message;

    public ServerResponseEvent(String fromSocketType, JSONObject jsonObject) {
        super(fromSocketType, jsonObject);
        try {
            code = jsonObject.getInt(CODE);
            message = jsonObject.optString(Constant.MESSAGE);
            request_id = jsonObject.optString(REQUEST_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getRequestId() {
        return request_id;
    }

    @Override
    public String toString() {
        return "ServerResponseEvent{" +
                "code=" + code +
                " " + super.toString() + "}";
    }
}
