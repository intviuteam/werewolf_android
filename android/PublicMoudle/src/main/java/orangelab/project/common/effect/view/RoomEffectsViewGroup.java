package orangelab.project.common.effect.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import orangelab.project.common.effect.event.RoomEffectEvent;
import orangelab.project.common.utils.SafeHandler;


/**
 * game_werewolf
 * 2018/7/20 下午6:05
 * Mystery
 * <p>
 * 房间内特效集合
 */
public class RoomEffectsViewGroup extends FrameLayout {

    private RoomEffectsContext mRoomEffectsContext;

    /**
     * 房间内游戏ViewGroup,负责实现骰子游戏和纸牌游戏
     */
    private RoomEffectsGameViewGroup mRoomGameEffectsViewGroup;

    /**
     * 房间内操作控制台
     *
     * @param context
     */
    private RoomEffectsConsoleViewGroup mRoomEffectsConsoleViewGroup;

    public RoomEffectsViewGroup(@NonNull Context context) {
        this(context, null);
    }

    public RoomEffectsViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomEffectsViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        EventBus.getDefault().register(this);

        mRoomEffectsContext = new RoomEffectsContext(getContext());

        /**
         * 用于展示成员的统一特效
         */
        mRoomGameEffectsViewGroup = new RoomEffectsGameViewGroup(context);
        mRoomGameEffectsViewGroup.bindEffectsContext(mRoomEffectsContext);
        addView(mRoomGameEffectsViewGroup, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        /**
         * 房间内的操作控制台
         */
        mRoomEffectsConsoleViewGroup = new RoomEffectsConsoleViewGroup(context);
        mRoomEffectsConsoleViewGroup.bindEffectsContext(mRoomEffectsContext);
        addView(mRoomEffectsConsoleViewGroup, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateContextData(RoomEffectEvent.UpdateEffectContextData updateEffectContextData) {
        mRoomEffectsContext.updatePositionData(updateEffectContextData.mSelfPosition, updateEffectContextData.mMasterPosition, updateEffectContextData.mSelfIsAvailablePosition);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        release();
    }

    private void release() {
        EventBus.getDefault().unregister(this);
        mRoomEffectsContext.destroy();
        mRoomGameEffectsViewGroup.destroy();
        mRoomEffectsConsoleViewGroup.destroy();
    }
}
