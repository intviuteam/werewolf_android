package orangelab.project.common.share;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/2/27 上午11:58
 * Mystery
 */

public class RankSharePayload implements Keepable, Parcelable {

    private static final String TYPE_RIR = "rir";
    private static final String TYPE_GROUP = "group";
    private static final String TYPE_RIRT = "rirt";
    private static final String TYPE_RIR_TODAY = "rir_rtoday";
    private static final String TYPE_RIR_LASWEEK = "rir_rlastweek";
    private static final String TYPE_RIR_ALL = "rir_rall";
    private static final String TYPE_OWNER_YESTERDAY = "owner_yesterday";
    private static final String TYPE_OWNER_LASTWEEK = "owner_lastweek";
    private static final String TYPE_OWNER_ALL = "owner_all";

    public String type = "";
    public String name = "";
    public String image = "";
    public String rankIndex = "";
    public String rankScore = "";
    public String rankLove = "";

    public boolean isPopular() {
        return TextUtils.equals(type, TYPE_RIR);
    }

    public boolean isTodayPopular() {
        return TextUtils.equals(type, TYPE_RIR_TODAY);
    }

    public boolean isLastWeekPopular() {
        return TextUtils.equals(type, TYPE_RIR_LASWEEK);
    }

    public boolean isFamily() {
        return TextUtils.equals(type, TYPE_GROUP);
    }

    public boolean isTotalPopular() {
        return TextUtils.equals(type, TYPE_RIRT) || TextUtils.equals(type, TYPE_RIR_ALL);
    }

    public boolean isOwnerYesterDay() {
        return TextUtils.equals(type, TYPE_OWNER_YESTERDAY);
    }

    public boolean isOwnerLastWeek() {
        return TextUtils.equals(type, TYPE_OWNER_LASTWEEK);
    }

    public boolean isOwnerAll() {
        return TextUtils.equals(type, TYPE_OWNER_ALL);
    }

    protected RankSharePayload(Parcel in) {
        type = in.readString();
        name = in.readString();
        image = in.readString();
        rankIndex = in.readString();
        rankScore = in.readString();
        rankLove = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(rankIndex);
        dest.writeString(rankScore);
        dest.writeString(rankLove);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RankSharePayload> CREATOR = new Creator<RankSharePayload>() {
        @Override
        public RankSharePayload createFromParcel(Parcel in) {
            return new RankSharePayload(in);
        }

        @Override
        public RankSharePayload[] newArray(int size) {
            return new RankSharePayload[size];
        }
    };
}
