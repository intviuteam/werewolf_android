package orangelab.project.common.view;

import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.view.viewpagergroup.ViewPagerGroupHolder;

public class MiniGameMapViewHolder extends ViewPagerGroupHolder {
    public ImageView mImageView;
    public TextView mTextView;
}
