package orangelab.project.common.engine;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;

import com.androidtoolkit.RxToolKit;

import orangelab.project.common.engine.event.BaseOrbitEvent;
import orangelab.project.common.event.ServerEvent;
import orangelab.project.common.model.ServerMessageEvent;

import com.google.gson.JsonSyntaxException;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.event.ViewEvent;
import orangelab.project.common.model.EnterRoomResult;

import org.json.JSONObject;
import org.webrtc.voiceengine.WebRtcAudioTrack;

import java.util.List;

import cn.intviu.orbit.manager.OrbitManager;
import cn.intviu.orbit.manager.OrbitRoomConfig;
import cn.intviu.orbit.manager.SequenceExecutor;
import cn.intviu.sdk.model.User;
import cn.intviu.support.GsonHelper;

/**
 * Author:1727
 * 2017/3/3
 */

public class OrbitEngine extends BaseOrbitEvent implements IOrbitEngine {

    private static final String TAG = "PCRTCClient";

    private String roomId;
    private User mUser;
    private Context context;
    private boolean hasStart = false;

    private OrbitManager orbitManager;
    private OrbitRoomConfig roomConfig;
    private Handler mUIHandler = null;
    private boolean mDefaultOpenRecord = false;

    private Runnable reConnectRunnable = new Runnable() {
        @Override
        public void run() {
            if (mUIHandler != null) {
                if (roomConfig != null) {
                    postMessageIntoDebug("Begin Reconnect to the Orbit!");
                    Log.i(TAG, "run: Begin Reconnect to the Orbit");
                    initOrbitManager();
                    startInner(roomConfig);
                }
            }
        }
    };

    public OrbitEngine(Context context) {
        this.context = context;
        mUIHandler = new Handler(Looper.getMainLooper());
        init();
        openSpeaker();
    }

    public void setDefaultOpenSpeaker(boolean defaultOpenRecord) {
        mDefaultOpenRecord = defaultOpenRecord;
    }

    @Override
    public void onError(int code, String reason) {
        super.onError(code, reason);
        postMessageIntoDebug("code=" + code + " reason = " + reason);
    }

    @Override
    public void onRoomUserList(List<User> userList) {
        closeRecord();
    }

    private void init() {
        orbitManager = OrbitManager.getInstance();
        lockOrbitManager();
        initOrbitManager();
        initOrbitAudioSource();
        initOrbitVideoSource();
        initOrbitBaseResource();
        initListener();
    }

    private void initOrbitBaseResource() {
        orbitManager.initBaseResource();
    }

    private void initOrbitAudioSource() {
        orbitManager.initAudioSource();
    }

    private void initOrbitVideoSource() {
        /**
         * don't need it
         */
    }

    private void releaseOrbitAudioSource() {
        orbitManager.releaseAudioSource();
    }

    private void releaseOrbitVideoSource() {
        orbitManager.releaseVideoSource();
    }

    private void lockOrbitManager() {
        if (orbitManager != null) {
            orbitManager.setBlockOtherRequest(true);
        }
    }

    private void releaseLockOrbitManager() {
        if (orbitManager != null) {
            orbitManager.setBlockOtherRequest(false);
        }
    }

    private void initOrbitManager() {
        orbitManager.initAVManager(context.getApplicationContext(), true);
    }

    public void start(String roomId, EnterRoomResult.RoomServerMessage message, User user) {
        OrbitRoomConfig roomConfig = new OrbitRoomConfig();
        roomConfig.setRoomId(roomId);
        roomConfig.setMeetingServerType(message.type);
        roomConfig.setRoomServer(message.media_server);
        roomConfig.setTurnServers(message.turn_servers);
        roomConfig.setUser(user);
        roomConfig.setStandbyRelay(message.standby_relay);
        this.roomConfig = roomConfig;
        this.mUser = user;
        PLog.i(TAG, roomConfig.toString());
        startInner(roomConfig);
    }

    private void startInner(OrbitRoomConfig roomConfig) {
        if (!engineIsStart()) {
            this.roomId = roomConfig.getRoomId();
            orbitManager.enterRoom(roomConfig, this);
            switchStart();
        } else {
            Log.i(TAG, "startInner: sorry engineIsStart");
        }
    }

    private void postMessageIntoDebug(String msg) {
        RoomSocketEngineHelper.postMessageIntoDebug(msg);
    }

    @Override
    public void onDisconnectFromRoom(String roomId) {
        super.onDisconnectFromRoom(roomId);
        Log.i(TAG, "onDisconnectFromRoom: ");
        shutdownInner(true, false, null);
    }

    @Override
    public void onRemoteUserConnected(User user) {
        Log.i(TAG, "onRemoteUserConnected: ");
        RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.RECONNECT_FROM_AUDIO_SERVER));
        if (mDefaultOpenRecord) {
            openRecord();
        }
    }

    @Override
    public void onRemoteUserFailed(User user) {
        Log.i(TAG, "onRemoteUserFailed: ");
        shutdownInner(true, false, null);
    }

    @Override
    public void onRemoteUserDisconnected(User user) {
        Log.i(TAG, "onRemoteUserDisconnected: ");
        RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.DISCONNECT_FROM_AUDIO_SERVER));
    }

    private void reconnect() {
        if (mUIHandler != null) {
            mUIHandler.removeCallbacks(reConnectRunnable);
            mUIHandler.postDelayed(reConnectRunnable, 5000);
        }
    }

    private void shutdownInner(final boolean needReconnect, boolean isShutDown, boolean needNotify, Runnable completeListener) {
        Log.i(TAG, "shutdownInner: prepare shut down");
        if (orbitManager != null) {
            Log.i(TAG, "shutdownInner: begin shut down");
            orbitManager.exitAllRoom(new SequenceExecutor.RunnableListener() {
                @Override
                public void onStart() {
                    Log.i(TAG, "onStart[exitAll]: onStart release all resource");
                }

                @Override
                public void onComplete() {
                    Log.i(TAG, "onComplete[exitAll]: onComplete release all resource");
                    switchStart();
                    /**
                     * 需要重新连接
                     */
                    if (needReconnect) {
                        if (mUIHandler != null) {
                            mUIHandler.post(() -> reconnect());
                        }
                        return;
                    }
                    /**
                     * 如果是直接关闭
                     */
                    if (isShutDown) {
                        if (mUIHandler != null) {
                            mUIHandler.post(() -> {
                                releaseOrbitAudioSource();
                                releaseOrbitVideoSource();
                                releaseLockOrbitManager();
                                if (needNotify) {
                                    RxToolKit.Emit(new ViewEvent.DestroyEvent(ViewEvent.DestroyEvent.ACTION_NORMAL));
                                }
                                if (mUIHandler != null) {
                                    mUIHandler.removeCallbacks(reConnectRunnable);
                                    Log.i(TAG, "onComplete: remove ConnectRunnable");
                                }
                                /**
                                 * 执行监听回调
                                 */
                                if (completeListener != null) {
                                    completeListener.run();
                                }
                            });
                        }
                        return;
                    } else {
                        //2018-06-05 修复问题
                        if (mUIHandler != null) {
                            mUIHandler.post(() -> {
                                if (completeListener != null) {
                                    completeListener.run();
                                }
                            });
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    Log.i(TAG, "onError: onError release all resource", t);
                }
            });
        } else {
            if (completeListener != null) {
                completeListener.run();
            }
        }
    }

    private void shutdownInner(final boolean needReconnect, boolean isShutDown, Runnable completeListener) {
        shutdownInner(needReconnect, isShutDown, true, completeListener);
    }

    public void shutdown(boolean needNotify, Runnable completeListener) {
        RxToolKit.UnRegister(this);
        releaseOrbitBaseResource();
        shutdownInner(false, true, needNotify, completeListener);
    }

    public void shutdown() {
        shutdown(true, null);
    }

    @Override
    public void closeRecord() {
        if (orbitManager != null) {
            orbitManager.setAudioEnable(roomId, true);
        }
    }

    @Override
    public void openRecord() {
        if (orbitManager != null) {
            orbitManager.setAudioEnable(roomId, false);
        }
    }

    @Override
    public void closeSpeaker() {
        WebRtcAudioTrack.setSpeakerMute(true);
    }

    @Override
    public void openSpeaker() {
        WebRtcAudioTrack.setSpeakerMute(false);
    }

    private void releaseOrbitBaseResource() {
        orbitManager.releaseBaseResource();
    }

    private boolean engineIsStart() {
        return hasStart;
    }

    private void switchStart() {
        hasStart = !hasStart;
    }

    private void initListener() {
        initSpeakViewEvent();
        initServerMessageEvent();
        orbitManager.addOrbitErrorReporter(error -> {
            RxToolKit.Emit(error);
        });
    }

    private void initServerMessageEvent() {
        RxToolKit.Build(this, ServerMessageEvent.class).action(v -> {
            if (TextUtils.equals(v.type, Constant.OLIVE_CHANGE)) {
                handleOliveChange(v.payload);
            }
        }).register();
    }

    private void handleOliveChange(JSONObject payload) {
        try {
            EnterRoomResult msg = GsonHelper.getGson().fromJson(payload.toString(), EnterRoomResult.class);
            if (msg != null && msg.media_server != null && mUser != null && !TextUtils.isEmpty
                    (roomId)) {
                /**
                 * 先shutdown上一次引擎
                 */
                shutdownInner(false, false, () -> {
                    /**
                     * 释放完毕后更换配置进行启动
                     */
                    RxToolKit.Emit(new ServerEvent.AudioServerEvent(ServerEvent.DISCONNECT_FROM_AUDIO_SERVER));
                    start(roomId, msg.media_server, mUser);
                });
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void initSpeakViewEvent() {
        RxToolKit.Build(this, ViewEvent.SpeakViewEvent.class).action(v -> {
            if (engineIsStart()) {
                switch (v.getMotionEvent()) {
                    case MotionEvent.ACTION_DOWN:
                        openRecord();
                        break;
                    case MotionEvent.ACTION_UP:
                        closeRecord();
                        break;
                }
            }
        }).register();
    }

}
