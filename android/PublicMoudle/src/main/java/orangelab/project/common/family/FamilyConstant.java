package orangelab.project.common.family;

/**
 * game_werewolf
 * 2017/10/26 下午5:53
 * Mystery
 */

public class FamilyConstant {
    public static final String FAMILY_ID = "FAMILY_ID";
    public static final String FAMILY_LIGHT_INFO = "FAMILY_LIGHT_INFO";
    public static final String FAMILY_MEMBERS_INTENT = "FAMILY_MEMBERS_INTENT";


    public static final String FAMILY_DEPUTY = "deputy";
    public static final String FAMILY_ELDER = "elder";
    public static final String FAMILY_ELITE = "elite";
    public static final String FAMILY_MEMBER = "memeber";


}
