package orangelab.project.common.db;

import android.text.TextUtils;

import com.toolkit.action.Destroyable;

import orangelab.project.common.db.entity.MiniGameLaunchRecordEntity;
import orangelab.project.common.db.gen.MiniGameLaunchRecordEntityDao;

/**
 * game_werewolf
 * 2018/5/31 下午2:37
 * Mystery
 */
public class MiniGameLaunchRecordDaoHelper implements Destroyable {


    private MiniGameLaunchRecordEntityDao mMiniGameLaunchRecordEntityDao;

    public MiniGameLaunchRecordDaoHelper(MiniGameLaunchRecordEntityDao miniGameLaunchRecordEntityDao) {
        mMiniGameLaunchRecordEntityDao = miniGameLaunchRecordEntityDao;
    }

    public void insertOrUpdateLaunch(String userId, String gameType, String json) {
        if (TextUtils.isEmpty(userId)) return;

        if (TextUtils.isEmpty(gameType)) return;

        if (TextUtils.isEmpty(json)) return;

        if (mMiniGameLaunchRecordEntityDao == null) return;

        String oldJson = getLaunch(userId, gameType);


        if (TextUtils.isEmpty(oldJson)) {

            /**
             * 插入操作
             */
            MiniGameLaunchRecordEntity entity = new MiniGameLaunchRecordEntity();
            entity.setGame_launch_json(json);
            entity.setUserId(userId);
            entity.setGame_type(gameType);
            mMiniGameLaunchRecordEntityDao.insert(entity);
        } else {

            /**
             * 更新操作
             */
            MiniGameLaunchRecordEntity entity = mMiniGameLaunchRecordEntityDao.queryBuilder().where(
                    MiniGameLaunchRecordEntityDao.Properties.UserId.eq(userId),
                    MiniGameLaunchRecordEntityDao.Properties.Game_type.eq(gameType)
            ).build().unique();
            if (entity == null) {
                /**
                 * 这里有可能是个意外
                 */

            } else {
                entity.setGame_launch_json(json);
                mMiniGameLaunchRecordEntityDao.update(entity);
            }
        }

    }

    public String getLaunch(String userId, String gameType) {

        if (TextUtils.isEmpty(userId)) return null;

        if (TextUtils.isEmpty(gameType)) return null;

        if (mMiniGameLaunchRecordEntityDao != null) {
            MiniGameLaunchRecordEntity entity = mMiniGameLaunchRecordEntityDao.queryBuilder().where(
                    MiniGameLaunchRecordEntityDao.Properties.UserId.eq(userId),
                    MiniGameLaunchRecordEntityDao.Properties.Game_type.eq(gameType)
            ).build().unique();
            if (entity == null) {
                return null;
            } else {
                return entity.getGame_launch_json();
            }
        }
        return null;
    }

    @Override
    public void destroy() {
        if (mMiniGameLaunchRecordEntityDao != null) {
            mMiniGameLaunchRecordEntityDao = null;
        }
    }
}
