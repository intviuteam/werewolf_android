package orangelab.project.common.share;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidtoolkit.PicassoUtils;
import com.R;

import orangelab.project.common.UIAdapterRemark;
import orangelab.project.common.dialog.SafeDialog;

/**
 * game_werewolf
 * 2018/2/27 上午10:13
 * Mystery
 */

public class RankShareDialog extends SafeDialog implements UIAdapterRemark, View.OnClickListener {

    private ShareListener mShareListener;
    private Runnable mDismissListener;

    private ImageView mUserHead;
    private TextView mUserName;

    private View mLikeIcon;

    private TextView mRankList;
    private TextView mPopular;
    private TextView mLike;

    private View shareWeChat;
    private View shareWeChatFriend;
    private View shareQQ;
    private View shareLine;
    private View shareFaceBook;
    private View shareWhatsApp;
    private View mDismiss;

    public RankShareDialog(@NonNull Context context, RankSharePayload payload) {
        super(context, R.style.DarkDialogAllowDismiss);
        View view = View.inflate(context, R.layout.layout_share_rank_dialog, null);
        setContentView(view);
        mDismiss = findViewById(R.id.id_dialog_close);
        mDismiss.setOnClickListener(this);

        mUserHead = (ImageView) findViewById(R.id.id_rank_user_head);

        mUserName = (TextView) findViewById(R.id.id_rank_user_name);

        mRankList = (TextView) findViewById(R.id.id_rank_list_value);
        mPopular = (TextView) findViewById(R.id.id_rank_popular_value);
        mLike = (TextView) findViewById(R.id.id_rank_like_value);

        mLikeIcon = findViewById(R.id.id_rank_like_value_ico);

        shareWeChat = findViewById(R.id.share_wechat);
        shareWeChatFriend = findViewById(R.id.share_wechat_friend);
        shareQQ = findViewById(R.id.share_qq);
        shareLine = findViewById(R.id.share_line);
        shareFaceBook = findViewById(R.id.share_facebook);
        shareWhatsApp = findViewById(R.id.share_whatsapp);
        shareWhatsApp.setVisibility(View.GONE);

        shareWeChat.setOnClickListener(this);
        shareWeChatFriend.setOnClickListener(this);
        shareQQ.setOnClickListener(this);
        shareLine.setOnClickListener(this);
        shareFaceBook.setOnClickListener(this);
        shareWhatsApp.setOnClickListener(this);

        PicassoUtils.loadImage(context, payload.image, mUserHead, R.mipmap.default_head);
        mUserName.setText(payload.name);

        initRank(payload);

        resizeDialog(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        setCancelable(false);
    }

    public void initRank(RankSharePayload payload) {
        if (payload.isTodayPopular()) {
            /**
             * 今日人气榜
             */
            likeAble(false);

            mRankList.setText(ShareHelper.Companion.GetTodayPopularRankList(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetTodayPopularRankPopular(payload.rankScore));
            return;
        }

        if (payload.isLastWeekPopular()) {
            /**
             * 上周人气榜
             */
            likeAble(false);

            mRankList.setText(ShareHelper.Companion.GetLastWeekPopularRankList(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetLastWeekPopularRankPopular(payload.rankScore));
            return;
        }

        if (payload.isFamily()) {
            /**
             * 家族榜
             */
            likeAble(false);

            mRankList.setText(ShareHelper.Companion.GetFamilyRankList(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetFamilyRankPopular(payload.rankScore));
            return;
        }

        if (payload.isTotalPopular()) {
            /**
             * 总人气榜
             */
            likeAble(false);

            mRankList.setText(ShareHelper.Companion.GetTotalPopularRankList(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetTotalPopularRankPopular(payload.rankScore));
            return;
        }

        /**
         * 昨日房主榜 初始化
         */
        if (payload.isOwnerYesterDay()) {
            likeAble(true);
            mRankList.setText(ShareHelper.Companion.GetRoomRankListYesterDay(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetRoomRankPopular(payload.rankScore));
            mLike.setText(ShareHelper.Companion.GetRoomRankLike(payload.rankLove));
            return;
        }

        /**
         * 上周房主榜 初始化
         */
        if (payload.isOwnerLastWeek()) {
            likeAble(true);
            mRankList.setText(ShareHelper.Companion.GetRoomRankListLastWeek(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetRoomRankPopular(payload.rankScore));
            mLike.setText(ShareHelper.Companion.GetRoomRankLike(payload.rankLove));
            return;
        }


        /**
         * 总房主榜 初始化
         */
        if (payload.isOwnerAll()) {
            likeAble(true);
            mRankList.setText(ShareHelper.Companion.GetRoomRankListTotal(payload.rankIndex));
            mPopular.setText(ShareHelper.Companion.GetRoomRankPopular(payload.rankScore));
            mLike.setText(ShareHelper.Companion.GetRoomRankLike(payload.rankLove));
            return;
        }
    }

    public void likeAble(boolean able) {
        if (able) {
            mLikeIcon.setVisibility(View.VISIBLE);
            mLike.setVisibility(View.VISIBLE);
        } else {
            mLikeIcon.setVisibility(View.GONE);
            mLike.setVisibility(View.GONE);
        }
    }

    public void setDismissListener(Runnable dismissListener) {
        mDismissListener = dismissListener;
    }

    public void setShareListener(ShareListener shareListener) {
        mShareListener = shareListener;
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    protected void release() {
        mShareListener = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_dialog_close) {
            dismiss();
            if (mDismissListener != null) {
                mDismissListener.run();
                mDismissListener = null;
            }
        } else if (id == R.id.share_wechat) {
            if (mShareListener != null) {
                mShareListener.onShareWeChat();
            }
        } else if (id == R.id.share_wechat_friend) {
            if (mShareListener != null) {
                mShareListener.onShareWeChatFriend();
            }
        } else if (id == R.id.share_qq) {
            if (mShareListener != null) {
                mShareListener.onShareQQ();
            }
        } else if (id == R.id.share_line) {
            if (mShareListener != null) {
                mShareListener.onShareLine();
            }
        } else if (id == R.id.share_facebook) {
            if (mShareListener != null) {
                mShareListener.onShareFaceBook();
            }
        } else if (id == R.id.share_whatsapp) {

        }
//        switch (v.getId()) {
//            case R.id.id_dialog_close: {
//                dismiss();
//                if (mDismissListener != null) {
//                    mDismissListener.run();
//                    mDismissListener = null;
//                }
//            }
//            break;
//            case R.id.share_wechat: {
//                if (mShareListener != null) {
//                    mShareListener.onShareWeChat();
//                }
//            }
//            break;
//            case R.id.share_wechat_friend: {
//                if (mShareListener != null) {
//                    mShareListener.onShareWeChatFriend();
//                }
//            }
//            break;
//            case R.id.share_qq: {
//                if (mShareListener != null) {
//                    mShareListener.onShareQQ();
//                }
//            }
//            break;
//            case R.id.share_line: {
//                if (mShareListener != null) {
//                    mShareListener.onShareLine();
//                }
//            }
//            break;
//            case R.id.share_facebook: {
//                if (mShareListener != null) {
//                    mShareListener.onShareFaceBook();
//                }
//            }
//            break;
//            case R.id.share_whatsapp: {
//
//            }
//            break;
//        }
    }
}
