package orangelab.project.common;

import android.content.Context;
import android.view.WindowManager;

import com.toolkit.action.Destroyable;

/**
 * game_werewolf
 * 2018/5/23 下午3:48
 * Mystery
 */
public abstract class BaseGlobalFloatWindowHelper implements Destroyable {

    protected IGlobalService mIGlobalService;
    protected WindowManager mWindowManager;
    protected Context mContext;


    public BaseGlobalFloatWindowHelper(Context context, IGlobalService IGlobalService, WindowManager windowManager) {
        mIGlobalService = IGlobalService;
        mWindowManager = windowManager;
        mContext = context;
    }

    @Override
    public void destroy() {

    }
}
