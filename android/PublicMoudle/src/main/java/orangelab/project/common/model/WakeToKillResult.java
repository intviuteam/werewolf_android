package orangelab.project.common.model;

import java.util.List;

/**
 * Created by wangxu on 2017/3/9.
 */

public class WakeToKillResult {
    public int duration;
    public List<Integer> alives;
}
