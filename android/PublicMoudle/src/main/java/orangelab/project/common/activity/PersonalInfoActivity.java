package orangelab.project.common.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.exhibition.ExhibitionsRecordActivity;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.EffectsTaskFactory;
import orangelab.project.common.family.FamilyInfoActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.FamilyTools;
import orangelab.project.common.family.model.FamilyInfo;
import orangelab.project.common.view.HeadDecorView;
import orangelab.project.common.view.PersonalInfoMiniGameHonourView;
import orangelab.project.common.view.PersonalInfoMiniGameRecordView;
import orangelab.project.common.view.VipNameView;
import orangelab.project.game.manager.WereWolfUserActiveManager;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.dialog.ReportDialog;
import orangelab.project.common.exception.GoToException;
import orangelab.project.common.exhibition.ExhibitionsActivity;
import orangelab.project.common.exhibition.ExhibitionsRecordAdapter;
import orangelab.project.common.exhibition.card.CardEvent;
import orangelab.project.common.exhibition.gift.ExhibitionsRecordItem;
import orangelab.project.common.exhibition.gift.GiftEvent;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.view.ActionSheetDialog;
import orangelab.project.common.view.FlowLayout;
import orangelab.project.common.view.TitleView;
import orangelab.project.common.web.WebViewActivity;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;

import com.androidtoolkit.PicassoUtils;

import orangelab.project.common.utils.Utils;

import com.androidtoolkit.transport.TransportHelper;
import com.networktoolkit.transport.URLManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.androidtoolkit.view.ScreenUtils;

import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.utils.LCIMIntentDataHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by wangxu on 2017/4/14.
 * 用户信息展示
 */

public class PersonalInfoActivity extends LockAbleActivity implements View.OnClickListener {

    private static final String TAG = "PersonalInfoActivity";

    private static final String FAMILY_INFO_ENABLE = "FAMILY_INFO_ENABLE";
    private static final String PERSONAL_USER_INFO = "PERSONAL_USER_INFO";
    private static final String MINI_GAME_RECORD = "MINI_GAME_RECORD";

    /**
     * TitleView
     */
    private TitleView mTitleView;
    private View mReport = null;

    /**
     * 主要滚轮
     */
    private ScrollView scrollView;

    /**
     * 个人详情信息相关
     */
    private ViewGroup mUserDataContainer;
    private HeadDecorView mUserHeadView;
    private TextView personalMoto;
    private TextView personalPopular;
    private Button mAddFriend;
    private Button mEnterRoom;
    private Button mChat;
    private TextView mGameLevel;
    private VipNameView mUserName;
    private String mUserId;
    private UserInfoResult mUserInfoResult = null;
    private boolean listenerIsAdded = false;
    private FlowLayout mRoleLayout;
    private TextView mUid;

    private TextView mLikeValue;
    private View mLikeContainer;


    /**
     * 战绩相关
     */
    private View mCombatDetailContainer;
    private TextView mGameInnings;
    private TextView mGameWin;
    private TextView mGameLose;
    private TextView mPersonalRunRate;
    private TextView mPersonalOdds;
    private ProgressBar mPersonalRunProgressBar;
    private ProgressBar mPersonalOddsProgressBar;
    private View mCombatArrow;

    private ImageView mUserActiveIv;
    private TextView mUserActionTv;
    private TextView mUserActionValue;

    /**
     * 送礼物按钮
     */
    private View presentGift;

    /**
     * 家族相关
     */
    private View mFamilyContainer;
    private TextView mFamilyName;
    private TextView mFamilyId;
    private ImageView mFamilyImage;
    private TextView mFamilyMembersCount;
    private TextView mFamilyPlaceHolder;
    private ImageView mFamilyBadge;
    private View mFamilyArrow;
    private boolean mFamilyEnable = true;

    /**
     * 照片墙相关
     */
    private View mPictureContainer;
    private GridView photosGridView;
    private View photosPageContainer;
    private View photosPlaceHolder;

    /**
     * 送礼物记录相关
     */
    private View mGiftContainer;
    private GridView giftsRecordGridView;
    private TextView presentPlaceHolder;
    private View presentArrow;
    private int recordItemWidth = ScreenUtils.dip2px(65);
    private int recordItemHeight = ScreenUtils.dip2px(75);
    private String personalInfoKey = UUID.randomUUID().toString();

    /**
     * 小游戏战绩
     */
    private PersonalInfoMiniGameRecordView mRecordView;
    private PersonalInfoMiniGameHonourView mHonourView;

    /**
     * 黑名单相关
     */
    private View mBlockContainer;

    //从排行榜进入好友详情，用于后期控制某些功能的开放
    private boolean mFromLeaderBoard;
    private boolean isBlocked = false;

    private boolean isFirstResume = true;
    private boolean isEnterWithUserInfo = false;

    public static void LaunchForMiniGameRecord(Context context, String userId) {
        try {
            if (ProjectConfig.EXCLUDE_USERID.contains(userId)) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, PersonalInfoActivity.class);
        IntentDataHelper.setType(intent, MINI_GAME_RECORD);
        IntentDataHelper.setUserId(intent, userId);
        IntentDataHelper.setIsFromLeaderBoard(intent, false);
        intent.putExtra(FAMILY_INFO_ENABLE, true);
        context.startActivity(intent);
    }

    public static void Launch(Context context, String userId, boolean isFromLeaderBoard, boolean needShowFamilyInfo) {
        try {
            if (ProjectConfig.EXCLUDE_USERID.contains(userId)) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, PersonalInfoActivity.class);
        IntentDataHelper.setUserId(intent, userId);
        IntentDataHelper.setIsFromLeaderBoard(intent, isFromLeaderBoard);
        intent.putExtra(FAMILY_INFO_ENABLE, needShowFamilyInfo);
        context.startActivity(intent);
    }


    public static void start(Context context, String userID) {
        Intent intent = new Intent(context, PersonalInfoActivity.class);
        IntentDataHelper.setUserId(intent, userID);
        context.startActivity(intent);
    }

    public static void Launch(Context context, String userId, boolean isFromLeaderBoard) {
        Launch(context, userId, isFromLeaderBoard, true);
    }

    public static void Launch(Context context, String userId) {
        Launch(context, userId, false);
    }

    public static void LaunchWithUserInfo(Context context, UserInfoResult userInfoResult) {
        if (userInfoResult == null) {
            return;
        }
        if (ProjectConfig.EXCLUDE_USERID.contains(userInfoResult.id)) {
            return;
        }
        Intent intent = new Intent(context, PersonalInfoActivity.class);
        IntentDataHelper.setUserId(intent, userInfoResult.id);
        IntentDataHelper.setIsFromLeaderBoard(intent, false);
        intent.putExtra(FAMILY_INFO_ENABLE, true);
        TransportHelper.putTransportable(PERSONAL_USER_INFO, userInfoResult);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_personal_info_layout);
        EventBus.getDefault().register(this);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        Intent intent = getIntent();
        mUserId = IntentDataHelper.getUserID(intent);
        mFamilyEnable = getIntent().getBooleanExtra(FAMILY_INFO_ENABLE, true);
        mFromLeaderBoard = IntentDataHelper.getIsFromLeaderBoard(intent);
        mUserInfoResult = TransportHelper.getTransport(PERSONAL_USER_INFO);
        if (mUserInfoResult != null) {
            isEnterWithUserInfo = true;
        } else {
            isEnterWithUserInfo = false;
        }

        initView();

        if (isEnterWithUserInfo) {
            handleUserInfoResult(mUserInfoResult);
        }
        smoothScrollToTop();
    }

    private void smoothScrollToTop() {
        scrollView.post(() -> {
            scrollView.smoothScrollTo(0, 0);
        });
    }

    private void initView() {
        initContainer();
        initMiniGameRecord();
        initTitle();
        initPersonalDataView();
        initFamilyView();
        initCombatDetailView();
        initPhotosPageView();
        initGiftPageView();
        initBlockView();
        initPresentGiftView();
    }

    /**
     * 初始化小游戏战绩部分功能
     */
    private void initMiniGameRecord() {

        String type = IntentDataHelper.getType(getIntent());
        if (TextUtils.equals(type, MINI_GAME_RECORD)) {
            View view = findViewById(R.id.mini_game_record_container1);
            view.setVisibility(View.VISIBLE);
            View view2 = findViewById(R.id.mini_game_honour_container);
            mRecordView = new PersonalInfoMiniGameRecordView(view);
            mRecordView.switchView();
            mHonourView = new PersonalInfoMiniGameHonourView(view2);
        } else {
            View view = findViewById(R.id.mini_game_record_container2);
            view.setVisibility(View.VISIBLE);
            mRecordView = new PersonalInfoMiniGameRecordView(view);
        }

    }

    private void initContainer() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
    }

    private void initTitle() {
        mReport = findViewById(R.id.report);
        mReport.setVisibility(View.GONE);
        mTitleView = (TitleView) findViewById(R.id.title);
        mTitleView.setTitle(R.string.personal_title);
        mTitleView.setActionBack(v -> {
            finish();
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initPersonalDataView() {
        mUserDataContainer = (ViewGroup) findViewById(R.id.id_personal_data_container);
        mAddFriend = (Button) findViewById(R.id.action_add_friend);
        mEnterRoom = (Button) findViewById(R.id.action_enter_room);
        mChat = (Button) findViewById(R.id.action_chat);
        mUserName = (VipNameView) findViewById(R.id.user_name);
        mUid = (TextView) findViewById(R.id.user_info_uid);
        mUserHeadView = (HeadDecorView) findViewById(R.id.user_head_view);
        personalMoto = (TextView) findViewById(R.id.personal_motto);
        personalPopular = (TextView) findViewById(R.id.user_popular);
        mRoleLayout = (FlowLayout) findViewById(R.id.id_personal_flowLayout);
        mLikeValue = (TextView) findViewById(R.id.like_value);
        mLikeContainer = findViewById(R.id.id_like_container);
        mLikeContainer.setOnClickListener(v -> {
            if (mUserInfoResult == null) {
                /**
                 * 此时用户的数据还没有被请求出来
                 */
                return;
            }
            /**
             * 点击点赞
             */
            ApiManager.userLike(mUserId, (s, e) -> {
                runOnUiThreadSafely(() -> {
                    if (e == null) {
                        /**
                         * 点赞成功
                         * 1.重新刷新点赞数量
                         * 2.展示一个点赞特效
                         */
                        updateLikeValue(s);

                        /**
                         * 展示一个特效
                         */
                        showLikeAnim();
                    } else {
                        ToastToolKit.showShort(e.getMessage());
                    }
                });
            });
        });
        personalPopular.setVisibility(View.GONE);
        mChat.setVisibility(View.GONE);
        mAddFriend.setVisibility(View.GONE);
        mUserHeadView.setOnClickListener(v -> {
            /**
             * 点击进入头像
             */
            Intent intent = new Intent(this, PictureActivity.class);
            IntentDataHelper.setUserAvater(intent, mUserHeadView.getHeadUrl());
            startActivity(intent);
        });
        if (mFromLeaderBoard) {
            mChat.setVisibility(View.GONE);
        }
    }

    private void hasFamily(boolean has) {
        if (has) {
            mFamilyId.setVisibility(View.VISIBLE);
            mFamilyName.setVisibility(View.VISIBLE);
            mFamilyImage.setVisibility(View.VISIBLE);
            mFamilyArrow.setVisibility(View.VISIBLE);
            mFamilyBadge.setVisibility(View.VISIBLE);
            mFamilyMembersCount.setVisibility(View.VISIBLE);
            mFamilyPlaceHolder.setVisibility(View.GONE);
        } else {
            mFamilyId.setVisibility(View.GONE);
            mFamilyName.setVisibility(View.GONE);
            mFamilyImage.setVisibility(View.GONE);
            mFamilyArrow.setVisibility(View.GONE);
            mFamilyBadge.setVisibility(View.GONE);
            mFamilyMembersCount.setVisibility(View.GONE);
            mFamilyPlaceHolder.setVisibility(View.VISIBLE);
        }
    }

    private void initFamilyView() {
        mFamilyContainer = findViewById(R.id.id_personal_family_container);
        mFamilyId = (TextView) findViewById(R.id.id_personal_family_id);
        mFamilyName = (TextView) findViewById(R.id.id_personal_info_family_name);
        mFamilyImage = (ImageView) findViewById(R.id.id_personal_info_family_iv);
        mFamilyMembersCount = (TextView) findViewById(R.id.id_personal_info_family_members);
        mFamilyPlaceHolder = (TextView) findViewById(R.id.id_family_placeholder);
        mFamilyBadge = (ImageView) findViewById(R.id.id_personal_family_badge);
        mFamilyArrow = findViewById(R.id.imageView7);
        hasFamily(false);
        if (mFamilyEnable) {
            mFamilyContainer.setVisibility(View.VISIBLE);
        } else {
            mFamilyContainer.setVisibility(View.GONE);
        }
    }

    private void initCombatDetailView() {
        mCombatDetailContainer = findViewById(R.id.id_personal_info_combat_detail_container);
        mGameInnings = (TextView) findViewById(R.id.game_innings);
        mGameWin = (TextView) findViewById(R.id.game_win);
        mGameLose = (TextView) findViewById(R.id.game_lose);
        mGameLevel = (TextView) findViewById(R.id.game_level);
        mPersonalOdds = (TextView) findViewById(R.id.game_odds);
        mPersonalRunRate = (TextView) findViewById(R.id.game_runs);
        mPersonalOddsProgressBar = (ProgressBar) findViewById(R.id.game_odd_view);
        mPersonalRunProgressBar = (ProgressBar) findViewById(R.id.game_runs_view);
        mCombatArrow = findViewById(R.id.id_personal_info_combat_arrow);
        mUserActiveIv = (ImageView) findViewById(R.id.imageView4);
        mUserActionTv = (TextView) findViewById(R.id.textView5);
        mUserActionValue = (TextView) findViewById(R.id.user_active_value);
        mCombatArrow.setVisibility(View.GONE);
        mCombatDetailContainer.setOnClickListener(this);
    }

    private void initPhotosPageView() {
        mPictureContainer = findViewById(R.id.photos_page);
        photosGridView = (GridView) findViewById(R.id.photos_page_gridview);
        photosPageContainer = findViewById(R.id.photos_page);
        photosPlaceHolder = findViewById(R.id.photos_placeHolder);
        photosPageContainer.setVisibility(View.GONE);
    }

    private void initGiftPageView() {
        mGiftContainer = findViewById(R.id.gifts_page);
        mGiftContainer.setVisibility(View.GONE);
        giftsRecordGridView = (GridView) findViewById(R.id.gifts_gridView);
        presentPlaceHolder = (TextView) findViewById(R.id.gifts_placeHolder);
        presentArrow = findViewById(R.id.gift_arrow);
        mAddFriend.setOnClickListener(this);
        mChat.setOnClickListener(this);
        mReport.setOnClickListener(this);
    }

    private void initBlockView() {
        mBlockContainer = findViewById(R.id.block_view);
        mBlockContainer.setVisibility(View.GONE);
    }

    private void initPresentGiftView() {
        presentGift = findViewById(R.id.fragment_chat_present_gift);
        presentGift.setOnClickListener(this);
        presentGift.setVisibility(View.GONE);
    }

    private void initListener() {
        /**
         * 防止Listener 添加多次
         */
        if (!listenerIsAdded) {
            RxToolKit.Build(personalInfoKey, GiftEvent.GiftForMainPageWrapperEvent.class).action(v -> sendPresentMessage(v.getGift().getGiftType(), v.getGift().getDisCount())).register();
            RxToolKit.Build(personalInfoKey, CardEvent.CardForMainPageWrapperEvent.class).action(v -> sendPresentMessage(v.getCard().getRealType())).register();
            listenerIsAdded = true;
        }
    }

    private void sendPresentMessage(String type) {
        sendPresentMessage(type, "");
    }

    private void sendPresentMessage(String type, String rebate) {
        LeanCloudChatHelper.sendGiftMessageIntoConversationAsync(
                type,
                mUserInfoResult.id,
                mUserInfoResult.name,
                mUserInfoResult.image,
                mUserInfoResult.sex,
                rebate
        );
    }

    private void initPhotos(ArrayList<String> photos, ArrayList<String> thumbs) {
        if (photos == null || photos.size() == 0) {
            photosPageContainer.setVisibility(View.VISIBLE);
            photosGridView.setVisibility(View.GONE);
            photosPlaceHolder.setVisibility(View.VISIBLE);
        } else {
            photosPageContainer.setVisibility(View.VISIBLE);
            photosGridView.setVisibility(View.VISIBLE);
            photosPlaceHolder.setVisibility(View.GONE);
        }
        PhotosGridViewAdapter adapter = new PhotosGridViewAdapter(this, thumbs);
        photosGridView.setAdapter(adapter);
        photosGridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(PersonalInfoActivity.this, PictureBrowseActivity.class);
            IntentDataHelper.setImageList(intent, photos);
            IntentDataHelper.setImageCurPosition(intent, position);
            startActivity(intent);
        });
        Utils.customGridViewAndChildHeightAndWidth(photosGridView, 3, ScreenUtils.dip2px(90), ScreenUtils.dip2px(90));
        smoothScrollToTop();
    }

    private void initGiftsRecord(List<ExhibitionsRecordItem> recordItems) {
        if (TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId())) {
            presentPlaceHolder.setText(MessageUtils.getString(R.string.photos_page_placeholder_myself));
        } else {
            presentPlaceHolder.setText(MessageUtils.getString(R.string.personal_present_placeholder));
        }
        if (recordItems == null || recordItems.size() == 0) {
            mGiftContainer.setVisibility(View.VISIBLE);
            presentArrow.setVisibility(View.GONE);
            presentPlaceHolder.setVisibility(View.VISIBLE);
            giftsRecordGridView.setVisibility(View.GONE);
        } else {
            mGiftContainer.setVisibility(View.VISIBLE);
            presentArrow.setVisibility(View.VISIBLE);
            presentPlaceHolder.setVisibility(View.GONE);
            giftsRecordGridView.setVisibility(View.VISIBLE);
            mGiftContainer.setOnClickListener(this);
        }
        ExhibitionsRecordAdapter recordAdapter = new ExhibitionsRecordAdapter(this);
        recordAdapter.updateRecord(recordItems);
        giftsRecordGridView.setAdapter(recordAdapter);
        giftsRecordGridView.setOnItemClickListener((parent, view, position, id) -> {
            handleGiftsPage();
        });
        giftsRecordGridView.setSelector(new ColorDrawable(Color.TRANSPARENT));//屏蔽点击特效
        Utils.customGridViewHeightAndWidth(giftsRecordGridView, 5, recordItemWidth, recordItemHeight);
        smoothScrollToTop();
    }

    private void setBlocked() {
        isBlocked = true;
        mBlockContainer.setVisibility(View.VISIBLE);
        mCombatDetailContainer.setVisibility(View.GONE);
        mPictureContainer.setVisibility(View.GONE);
        mFamilyContainer.setVisibility(View.GONE);
        mGiftContainer.setVisibility(View.GONE);
        presentGift.setVisibility(View.GONE);
        mAddFriend.setVisibility(View.GONE);
        mChat.setVisibility(View.GONE);
    }

    private void getUserInfo() {
        String type = IntentDataHelper.getType(getIntent());
        boolean forMiniGame = false;
        if (TextUtils.equals(type, MINI_GAME_RECORD)) {
            forMiniGame = true;
        } else {
            forMiniGame = false;
        }
        ApiManager.GetUserInfoApi(mUserId, forMiniGame, (userInfoResult, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    handleUserInfoResult(userInfoResult);
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void handleUserInfoResult(UserInfoResult userInfoResult) {
        try {
            mUserInfoResult = userInfoResult;
            handleTitle();
            handlePersonalData();
            handleBlock();
            handleFamily();
            handleCombatDetail();
            handleInitPhotos();
            handleInitGiftRecords();
            handlePresentGift();
            if (TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId())) {
                FamilyMemo.UpdateMemo(mUserInfoResult.group);
            }
        } catch (GoToException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleCombatDetail() {
        float total = (mUserInfoResult.game.lose + mUserInfoResult.game.win + mUserInfoResult.game.escape);
        float rate = total > 0 ? (mUserInfoResult.game.win / (total)) * 100f : 0;
        float runRate = total > 0 ? (mUserInfoResult.game.escape / total) * 100f : 0;
        String rateString = new BigDecimal(rate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        String runRateString = new BigDecimal(runRate).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
        mPersonalOdds.setText(MessageUtils.getString(R.string.user_detail_win_rate, rateString + "%"));
        mPersonalRunRate.setText(MessageUtils.getString(R.string.run_rate, runRateString + "%"));
        mGameInnings.setText(MessageUtils.getString(R.string.user_detail_total, Integer.toString(mUserInfoResult.game.win + mUserInfoResult.game.lose)));
        mGameWin.setText(MessageUtils.getString(R.string.user_detail_win, Integer.toString(mUserInfoResult.game.win)));
        mGameLose.setText(MessageUtils.getString(R.string.user_detail_lose, Integer.toString(mUserInfoResult.game.lose)));
        mPersonalOddsProgressBar.setProgress((int) (rate));
        mPersonalRunProgressBar.setProgress((int) runRate);
        if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
            mCombatArrow.setVisibility(View.VISIBLE);
        } else {
            mCombatArrow.setVisibility(View.GONE);
        }
        mUserActionValue.setText(WereWolfString.getString(R.string.werewolf_user_active_value, Integer.toString(mUserInfoResult.active.experience)));
        WereWolfUserActiveManager.FillActiveImageView(mUserInfoResult.active, mUserActiveIv);
        WereWolfUserActiveManager.FillActiveTextViewWithOutBg(mUserInfoResult.active, mUserActionTv);

        mRecordView.updateData(mUserInfoResult.mini_game_datas);

        if (mHonourView != null) {
            mHonourView.updateData(mUserInfoResult);
        }
    }

    private void handleFamily() {
        FamilyInfo familyInfo = mUserInfoResult.group;
        if (familyInfo != null) {
            try {
                mFamilyName.setText(familyInfo.name);
                mFamilyId.setText(familyInfo.gid);
                PicassoUtils.loadImage(this, familyInfo.image, mFamilyImage, R.mipmap.ico_family_default);
                mFamilyMembersCount.setText(MessageUtils.getString(R.string.string_family_members_count, Integer.toString(familyInfo.member_count)));
                mFamilyContainer.setOnClickListener(v -> {
                    addLockViewRect(mFamilyContainer);
                    FamilyInfoActivity.Launch(this, familyInfo.group_id);
                    removeLockViewRect(mFamilyContainer);
                });
                mFamilyContainer.setVisibility(View.VISIBLE);
                /**
                 * 这里要处理家族勋章问题
                 */
                FamilyTools.FillFamilyBadge(this, familyInfo, mFamilyBadge);

                hasFamily(true);
            } catch (Exception e) {
                e.printStackTrace();
                mFamilyContainer.setVisibility(View.VISIBLE);
                hasFamily(false);
            }
        } else {
            hasFamily(false);
        }
        if (mFamilyEnable) {
            mFamilyContainer.setVisibility(View.VISIBLE);
        } else {
            mFamilyContainer.setVisibility(View.GONE);
        }
    }

    private void handlePresentGift() {
        if (TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId()) || Utils.checkUserIdIsTourist(GlobalUserState.getGlobalState().getUserId())) {
            presentGift.setVisibility(View.GONE);
        } else {
            presentGift.setVisibility(View.VISIBLE);
        }
    }

    private void handleBlock() throws GoToException {
        if (mUserInfoResult.isBlock) {
            setBlocked();
            throw new GoToException();
        }
    }

    private void handleInitGiftRecords() {
        UserInfoResult result = mUserInfoResult;
        if (result.gift != null) {
            ArrayList<ExhibitionsRecordItem> records = new ArrayList<>();
            for (UserInfoResult.GiftItem item : result.gift) {
                if (EffectsManager.SupportThisGiftType(item.type)) {
                    ExhibitionsRecordItem temp = new ExhibitionsRecordItem(item.type, item.count);
                    records.add(temp);
                }
            }
            result.gift.clear();
            initGiftsRecord(records);
        }
    }

    private void handleInitPhotos() {
        UserInfoResult result = mUserInfoResult;
        if (result.photos != null) {
            ArrayList<String> photos = new ArrayList<>();
            ArrayList<String> thumbs = new ArrayList<>();
            for (UserInfoResult.PhotoItem item : result.photos) {
                photos.add(item.url);
                thumbs.add(item.thumbnail_url);
            }
            initPhotos(photos, thumbs);
        }
    }

    private void handleTitle() {
        if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
            mReport.setVisibility(View.GONE);
        } else {
            if (!GlobalUserState.getGlobalState().isTourist()) {
                mReport.setVisibility(View.VISIBLE);
            }
            if (Utils.checkUserIdIsTourist(mUserId)) {
                /**
                 * 游客不展示
                 */
                mReport.setVisibility(View.GONE);
            }
        }
    }

    private boolean needAddFriend() {
        boolean result = false;
        if (Utils.targetIsNotNull(mUserInfoResult)
                && !mUserInfoResult.is_friend
                && !TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId())) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    private boolean needShowBlack() {
        boolean result = false;
        if (Utils.targetIsNotNull(mUserInfoResult) && !mUserInfoResult.isBlock && !TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId())) {
            return true;
        } else {
            result = false;
        }
        return result;
    }

    private void updateLikeValue(int like) {
        /**
         * 显示点赞信息
         */
        mLikeValue.setText(MessageUtils.getString(R.string.string_personal_into_like, Integer.toString(like)));

    }

    private void handlePersonalData() {

        updateLikeValue(mUserInfoResult.be_liked);

        if (needAddFriend()) {
            mAddFriend.setVisibility(View.VISIBLE);
            /**
             * 不是好友不展示
             */
            mEnterRoom.setVisibility(View.GONE);
        } else {
            /**
             * 是还有要判断是否展示「进入房间」
             */
            if (TextUtils.equals(mUserInfoResult.id, GlobalUserState.getGlobalState().getUserId()) || GlobalUserState.getGlobalState().isGaming()) {
                mEnterRoom.setVisibility(View.GONE);
            } else {
                String roomId = mUserInfoResult.getRoomId();
                String roomType = mUserInfoResult.getRoomType();
                if (!TextUtils.isEmpty(roomId) && !TextUtils.isEmpty(roomType) && mUserInfoResult.isCanEnterRoomType()) {
                    mEnterRoom.setVisibility(View.VISIBLE);
                    mEnterRoom.setOnClickListener(v -> {
                        LaunchActivity.LaunchForRoom(this, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, roomId, roomType);
                    });
                }
            }
        }
        if (mUserInfoResult.id.equals(GlobalUserState.getGlobalState().getUserId())) {
            mChat.setVisibility(View.GONE);
        } else {
            if (!mFromLeaderBoard) {
                if (Utils.checkUserIdIsTourist(mUserInfoResult.id)) {
                    mChat.setVisibility(View.GONE);
                } else {
                    mChat.setVisibility(View.VISIBLE);
                }
            }
        }
        mUserHeadView.setUserHeadImageUrl(mUserInfoResult.image);
        mUserHeadView.setUserSex(mUserInfoResult.sex);
        mUserHeadView.setHeadDecorate(mUserInfoResult.avatar_box);
        mUserName.setText(mUserInfoResult.name);
        mUserName.handleVipInfo(mUserInfoResult.vipInfo, Color.BLACK);
        mGameLevel.setText(MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(mUserInfoResult.game.level)));
        if (!TextUtils.isEmpty(mUserInfoResult.signature)) {
            personalMoto.setText(mUserInfoResult.signature);
        }
        personalPopular.setText(MessageUtils.getString(R.string.user_popular_text, Integer.toString(mUserInfoResult.popular)));
        personalPopular.setVisibility(View.VISIBLE);

        try {
            if (mUserInfoResult.uid == null) {
                mUid.setVisibility(View.GONE);
            } else {
                mUid.setVisibility(View.VISIBLE);
                mUid.setText(MessageUtils.getString(R.string.string_user_info_uid, Integer.toString(mUserInfoResult.uid)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            UserInfoResult result = mUserInfoResult;
            List<String> roleData = new ArrayList<>();
            if (result.role != null && result.role.customs != null && result.role.customs.size() > 0) {
                roleData.addAll(result.role.customs);
            }
            if (roleData.size() > 0) {
                mRoleLayout.setVisibility(View.VISIBLE);
                mRoleLayout.addTextViewsWithVipInfo(roleData, Color.parseColor("#2d1c4c"), 10, true, result.vipInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void blockFriend() {
        ApiManager.AddToBlackApi(mUserId, (s, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.add_into_black_list_success)).run();
                    getUserInfo();
                    BlackListManager.Add(mUserId);
                    UnifiedBridgeHelper.SendBlackListAddAction(mUserId);
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void handleGiftsPage() {
        ExhibitionsRecordActivity.enterExhibitionsRecord(this, mUserId);
    }

    private void handleChat() {
        try {
            UserData leftUser = UserData.createUserData(mUserInfoResult.id, mUserInfoResult.name, mUserInfoResult
                    .sex, mUserInfoResult.image);
            UserData rightUser = UserData.createUserData(
                    GlobalUserState.getGlobalState().getUserId(),
                    GlobalUserState.getGlobalState().getUserName(),
                    GlobalUserState.getGlobalState().getUserSex(),
                    GlobalUserState.getGlobalState().getUserIcon()
            );
            Intent intent = new Intent(this, LCIMConversationActivity.class);
            ChatConfig chatConfig = new ChatConfig();
            chatConfig.leftUser = leftUser;
            chatConfig.rightUser = rightUser;
            chatConfig.isSingleChat = true;
            chatConfig.peerId = leftUser.userId;
            chatConfig.from = ChatConfig.PERSONAL_INFO;
            LCIMIntentDataHelper.setChatConfig(intent, chatConfig);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteFriend() {
        ApiManager.DeleteFriend(mUserId, (s, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.delete_friend_success)));
                    UnifiedBridgeHelper.SendDeleteFriendAction(mUserId);
                    getUserInfo();
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });

    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.action_add_friend) {
            ApiManager.AddFriendApi(mUserId, null, null, null);
        } else if (viewId == R.id.action_chat) {
            if (GlobalUserState.getGlobalState().isTourist()) {
                Toast.makeText(PersonalInfoActivity.this, MessageUtils.getString(R.string.tourist_chat), Toast.LENGTH_SHORT).show();
            } else {
                handleChat();
            }
        } else if (viewId == R.id.fragment_chat_present_gift) {
            ExhibitionsActivity.startActivityFromMainPage(this, mUserInfoResult.id, mUserInfoResult.name, mUserInfoResult.sex, mUserInfoResult.image);
        } else if (viewId == R.id.gifts_page) {
            if (GlobalUserState.getGlobalState().isTourist()) {
                Toast.makeText(this, R.string.gifts_preset_from_tourist, Toast.LENGTH_SHORT).show();
            } else {
                handleGiftsPage();
            }
        } else if (viewId == R.id.report) {
            ActionSheetDialog dialog = new ActionSheetDialog(this)
                    .builder()
                    .setCancelable(true)
                    .setCanceledOnTouchOutside(true);
            dialog.addSheetItem(MessageUtils.getString(R.string.report_msg), ActionSheetDialog.SheetItemColor.Blue, which -> {
                ReportDialog reportDialog = new ReportDialog(PersonalInfoActivity.this, mUserInfoResult.id, mUserInfoResult.name);
                reportDialog.show();
            });
            if (!needAddFriend() && !isBlocked) {
                dialog.addSheetItem(MessageUtils.getString(R.string.delete_friend_msg), ActionSheetDialog.SheetItemColor.Blue, which -> {
                    deleteFriend();
                });
            }
            if (needShowBlack()) {
                dialog.addSheetItem(MessageUtils.getString(R.string.add_into_black_list), ActionSheetDialog.SheetItemColor.Blue, which -> {
                    blockFriend();
                });
            }
            dialog.show();
        } else if (viewId == R.id.id_personal_info_combat_detail_container) {
            if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
                WebViewActivity.load(this, URLManager.GetDefaultURL() + URLManager.GetStandingsURL(GlobalUserState.getGlobalState().getToken()));
            }
        }
//        switch (view.getId()) {
//            case R.id.action_add_friend:
//                // ApiManager.AddFriendApi(mUserId, null, null, null);
//                break;
//            case R.id.action_chat:
//                if (GlobalUserState.getGlobalState().isTourist()) {
//                    Toast.makeText(PersonalInfoActivity.this, MessageUtils.getString(R.string.tourist_chat), Toast.LENGTH_SHORT).show();
//                } else {
//                    handleChat();
//                }
//                break;
//            case R.id.fragment_chat_present_gift:
//                ExhibitionsActivity.startActivityFromMainPage(this, mUserInfoResult.id, mUserInfoResult.name, mUserInfoResult.sex, mUserInfoResult.image);
//                break;
//            case R.id.gifts_page:
//                if (GlobalUserState.getGlobalState().isTourist()) {
//                    Toast.makeText(this, R.string.gifts_preset_from_tourist, Toast.LENGTH_SHORT).show();
//                    break;
//                } else {
//                    handleGiftsPage();
//                }
//                break;
//            case R.id.report: {
//                ActionSheetDialog dialog = new ActionSheetDialog(this)
//                        .builder()
//                        .setCancelable(true)
//                        .setCanceledOnTouchOutside(true);
//                dialog.addSheetItem(MessageUtils.getString(R.string.report_msg), ActionSheetDialog.SheetItemColor.Blue, which -> {
//                    ReportDialog reportDialog = new ReportDialog(PersonalInfoActivity.this, mUserInfoResult.id, mUserInfoResult.name);
//                    reportDialog.show();
//                });
//                if (!needAddFriend() && !isBlocked) {
//                    dialog.addSheetItem(MessageUtils.getString(R.string.delete_friend_msg), ActionSheetDialog.SheetItemColor.Blue, which -> {
//                        deleteFriend();
//                    });
//                }
//                if (needShowBlack()) {
//                    dialog.addSheetItem(MessageUtils.getString(R.string.add_into_black_list), ActionSheetDialog.SheetItemColor.Blue, which -> {
//                        blockFriend();
//                    });
//                }
//                dialog.show();
//            }
//            break;
//            case R.id.id_personal_info_combat_detail_container:
//                if (TextUtils.equals(mUserId, GlobalUserState.getGlobalState().getUserId())) {
//                    WebViewActivity.load(this, URLManager.GetDefaultURL() + URLManager.GetStandingsURL(GlobalUserState.getGlobalState().getToken()));
//                }
//                break;
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isFirstResume && isEnterWithUserInfo) {
            isFirstResume = false;
            /**
             * 这里少进行一次UserInfo
             */
        } else {
            getUserInfo();
        }
        initListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        RxToolKit.UnRegister(personalInfoKey);
        listenerIsAdded = false;
        Log.i(TAG, "onStop: unregisterListener");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        RxToolKit.UnRegister(personalInfoKey);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void showLikeAnim() {
        /**
         * 展示礼物特效
         */
        int[] location = new int[2];
        mLikeValue.getLocationOnScreen(location);

        int width = mLikeValue.getWidth();
        int height = mLikeValue.getHeight();

        ImageView imageView = new ImageView(this);

        int animWidth = ScreenUtils.dip2px(12);
        int distance = animWidth / 2;

        /**
         * 坐标点的校准
         */

        int[] containerLocation = new int[2];
        mUserDataContainer.getLocationOnScreen(containerLocation);
        int orgX = containerLocation[0];
        int orgY = containerLocation[1];
        PointF animStartPoint = new PointF(location[0] + width, location[1] - orgY - animWidth);
        PointF animEndPoint = new PointF(animStartPoint.x + distance, animStartPoint.y - distance);

        /**
         * 路径动画
         */
        Animator pathAnim = EffectsTaskFactory.INSTANCE.createStraightLineAnim(imageView, animStartPoint, animEndPoint, 400l);

        pathAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                runOnUiThreadSafely(() -> {
                    mUserDataContainer.removeView(imageView);
                });
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                runOnUiThreadSafely(() -> {
                    mUserDataContainer.removeView(imageView);
                });
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);

            }
        });
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(animWidth, animWidth);
        imageView.setX(animStartPoint.x);
        imageView.setY(animStartPoint.y);
        imageView.setImageResource(R.mipmap.ico_personal_info_like_add_anim);
        mUserDataContainer.addView(imageView, layoutParams);
        pathAnim.start();

    }

    private static class PhotosGridViewAdapter extends BaseAdapter {
        private List<String> imageList;
        private Context context;

        public PhotosGridViewAdapter(Context context, List<String> imageList) {
            if (imageList == null) {
                this.imageList = new ArrayList<>();
                return;
            }
            this.imageList = imageList;
            this.context = context;
        }


        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public String getItem(int position) {
            return imageList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.personal_photos_item, null);
                holder = new ViewHolder();
                holder.photo = (ImageView) convertView.findViewById(R.id.photos_item);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            String imgUrl = getItem(position);
            PicassoUtils.loadImage(
                    context,
                    imgUrl,
                    ScreenUtils.dip2px(90),
                    ScreenUtils.dip2px(90),
                    holder.photo,
                    R.mipmap.ic_me_photo_default
            );
            return convertView;
        }

        private static class ViewHolder {
            ImageView photo;
        }
    }
}
