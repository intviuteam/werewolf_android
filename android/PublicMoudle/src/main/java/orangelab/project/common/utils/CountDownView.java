package orangelab.project.common.utils;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.R;

/**
 * Author:1727
 * 2017/3/7
 */

public class CountDownView extends CountDownTimer {

    private static final String TAG = "CountDownView";

    private TextView textView;
    private CountDownFinish countDownFinish;
    private static final int SECONDS = 1000;
    private Handler uiHandler = null;
    private int mCountDownRule = -1;

    public CountDownView(TextView textView, int seconds, CountDownFinish countDownFinish) {
        super(seconds * SECONDS, SECONDS);
        this.textView = textView;
        this.countDownFinish = countDownFinish;
        uiHandler = new Handler(Looper.getMainLooper());
    }

    public void setCountDownRule(int countDownRule) {
        mCountDownRule = countDownRule;
    }

    @Override
    public void onTick(final long millisUntilFinished) {
        if (uiHandler != null) {
            uiHandler.post(() -> {
                if (textView != null) {
                    int countdown = (int) (millisUntilFinished / SECONDS);
                    if (mCountDownRule != -1) {
                        textView.setText(MessageUtils.getString(mCountDownRule, Integer.toString(countdown)));
                    } else {
                        textView.setText(countdown + "");

                    }
                }
            });
        }
    }

    @Override
    public void onFinish() {
        PLog.i(TAG, "onFinish");
        cancel();
        if (this.countDownFinish != null) {
            this.countDownFinish.onFinish();
            this.countDownFinish = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
        if (textView != null) {
            textView.setText("");
            textView = null;
        }
    }

    public static interface CountDownFinish {
        public void onFinish();
    }
}
