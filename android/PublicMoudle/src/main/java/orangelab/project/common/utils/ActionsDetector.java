package orangelab.project.common.utils;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;

import java.util.HashMap;
import java.util.Map;

import orangelab.project.game.event.GameEvent;

/**
 * game_werewolf
 * 2017/5/24 下午5:53
 * Mystery
 * <p>
 * 行为检测器工具，可以用于检测某种行为工具类
 */

public enum ActionsDetector {


    INSTANCE;

    public static final String ACTION_SPEECH_CHECK = "action_speech_check";   //用于检测狼人杀游戏时的个人发言的时间检测
    public static final long ACTION_SPEECH_CHECK_TIME = 35 * 1000;            //发言检测的时间限定

    private static final String TAG = "ActionsDetector";

    private Handler mUIHandler = null;

    private Map<String, ActionRunnable> mRunnableMap;

    private ActionsDetector() {
        mRunnableMap = new HashMap<>(0);
        mUIHandler = new Handler(Looper.getMainLooper());
    }

    public static boolean contains(String key) {
        return INSTANCE.containsAction(key);
    }

    public static void add(String key, Action action) {
        INSTANCE.addAction(key, action);
    }

    public static void cancel(String key) {
        INSTANCE.cancelAction(key);
    }

    public static void add(String key, Action action, long delay) {
        INSTANCE.addAction(key, action, delay);
    }

    private boolean containsAction(String key) {
        return mRunnableMap.containsKey(key);
    }

    private void cancelAction(String key) {
        ActionRunnable actionRunnable = mRunnableMap.remove(key);
        if (actionRunnable != null) {
            mUIHandler.removeCallbacks(actionRunnable);
            actionRunnable.cancel();
        }
        PLog.i(TAG, "Actions Map " + mRunnableMap);
    }

    private void addAction(String key, Action action) {
        addAction(key, action, 0);
    }

    private void addAction(String key, Action action, long delay) {
        if (delay > 0) {
            ActionRunnable actionRunnable = new ActionRunnable(key, action);
            mRunnableMap.put(key, actionRunnable);
            mUIHandler.postDelayed(actionRunnable, delay);
        } else {
            if (action != null) {
                action.onStart();
                action.onTime();
            }
        }
        PLog.i(TAG, "Actions Map " + mRunnableMap);
    }

    public static abstract class ActionAdapter implements Action {

        @Override
        public void onStart() {

        }

        @Override
        public void onTime() {

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(Throwable e) {

        }
    }

    public interface Action {

        void onStart();

        void onTime();

        void onCancel();

        void onError(Throwable e);

    }

    public static class ActionRunnable implements Runnable {

        private Action mAction;
        private String mKey;

        public ActionRunnable(String key, Action action) {
            mAction = action;
            mKey = key;
        }

        public void cancel() {
            try {
                PLog.i(TAG, "[" + mKey + "] cancel");
                if (mAction != null) {
                    mAction.onCancel();
                }
            } catch (Exception e) {
                if (mAction != null) {
                    mAction.onError(e);
                }
            }
        }

        @Override
        public void run() {
            try {
                PLog.i(TAG, "[" + mKey + "] on time");
                if (mAction != null) {
                    mAction.onTime();
                }
                /**
                 * 准时执行之后需要删除这个Action
                 */
                ActionsDetector.cancel(mKey);
            } catch (Exception e) {
                if (mAction != null) {
                    mAction.onError(e);
                }
            }
        }
    }
}
