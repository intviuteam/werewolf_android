package orangelab.project.common.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.R;
import orangelab.project.common.view.ArrowRectangleViewGroup;
import orangelab.project.common.view.HintPopWindow;
import com.androidtoolkit.UIActuator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.intviu.support.SharePreStoreUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/6/19 下午1:35
 * Mystery
 */

public class HintManager {

    private static Map<String, HintPopWindow> recordHints = new HashMap<>();
    private static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            clearHint(HintPopWindow.CHANGE_OWNER_HINT);
        }
    };

    public static void clearHint(String type) {
        if (recordHints.containsKey(type)) {
            HintPopWindow popWindow = recordHints.remove(type);
            if (popWindow != null && popWindow.isShowing()) {
                popWindow.dismiss();
            }
            if (TextUtils.equals(type, HintPopWindow.CHANGE_OWNER_HINT)) {
                UIActuator.remove(runnable);
            }
        }
    }

    public static void clearAllHint() {
        Iterator iterator = recordHints.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, HintPopWindow> entry = (Map.Entry) iterator.next();
            HintPopWindow value = entry.getValue();
            if (value != null) {
                if (value.isShowing()) {
                    value.dismiss();
                }
            }
        }
        recordHints.clear();
        UIActuator.remove(runnable);
    }

    public static void prepareButtonHint(View prepareButton, boolean needShow) {
        if (prepareButton.getVisibility() != View.GONE && prepareButton != null && needShow) {
            if (judgeNeedShowPrepareHint(prepareButton.getContext())) {
                if (!recordHints.containsKey(HintPopWindow.PREPARE_HINT)) {
                    int[] location = new int[2];
                    prepareButton.getLocationOnScreen(location);
                    HintPopWindow popWindow = new HintPopWindow.Builder()
                            .setType(HintPopWindow.PREPARE_HINT)
                            .setText(MessageUtils.getString(R.string.hint_prepare_button))
                            .setArrowDirection(ArrowRectangleViewGroup.ARROW_BOTTOM_LEFT)
                            .build(prepareButton.getContext());
                    recordHints.put(HintPopWindow.PREPARE_HINT, popWindow);
                    popWindow.show(
                            prepareButton,
                            location[0],
                            location[1] - popWindow.getWindowHeight()
                    );
                }
            }
        }
    }

    public static void prepareChangeRoomOwner(View changeOwner, boolean needShow) {
        if (changeOwner.getVisibility() != View.GONE && changeOwner != null && needShow) {
            if (judgeNeedShowChangeOwnerHint(changeOwner.getContext())) {
                if (!recordHints.containsKey(HintPopWindow.CHANGE_OWNER_HINT)) {
                    int[] location = new int[2];
                    changeOwner.getLocationOnScreen(location);
                    HintPopWindow popWindow = new HintPopWindow.Builder()
                            .setType(HintPopWindow.CHANGE_OWNER_HINT)
                            .setText(MessageUtils.getString(R.string.hint_change_room_owner_request))
                            .build(changeOwner.getContext());
                    recordHints.put(HintPopWindow.CHANGE_OWNER_HINT, popWindow);
                    popWindow.show(
                            changeOwner,
                            location[0] - popWindow.getWindowWidth() + changeOwner.getWidth(),
                            location[1] + changeOwner.getHeight()
                    );
                    UIActuator.postDelay(runnable, 10000);
                }
            }
        }
    }

    private static boolean judgeNeedShowPrepareHint(Context context) {
        boolean result = SharePreStoreUtils.getBoolean(
                GlobalUserState.getGlobalState().getUserId(),
                context,
                HintPopWindow.PREPARE_HINT
        );
        return !result;
    }

    public static void recordHint(Context context, String type) {
        SharePreStoreUtils.save(
                GlobalUserState.getGlobalState().getUserId(),
                context,
                type,
                true
        );
    }

    private static boolean judgeNeedShowChangeOwnerHint(Context context) {
        boolean result = SharePreStoreUtils.getBoolean(
                GlobalUserState.getGlobalState().getUserId(),
                context,
                HintPopWindow.CHANGE_OWNER_HINT
        );
        return !result;
    }
}
