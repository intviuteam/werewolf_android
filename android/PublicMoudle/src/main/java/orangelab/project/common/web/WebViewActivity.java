package orangelab.project.common.web;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alibaba.fastjson.JSONObject;
import com.androidtoolkit.RxToolKit;
import com.R;

import cn.intviu.support.ShareTools;
import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.dialog.BoxAwardDialog;
import orangelab.project.common.dialog.UserDetailDialog;
import orangelab.project.common.event.ShareWeChatEvent;
import orangelab.project.common.event.WebToNativeHideEvent;
import orangelab.project.common.event.WebViewEvent;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.AwardInfo;
import orangelab.project.common.model.FortuneResult;
import orangelab.project.common.pay.PayActivity;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedBridgeEvent;
import orangelab.project.common.union.UnifiedBridgeJSEvent;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.ImageFilePath;
import orangelab.project.common.utils.ImageUtils;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.TitleView;
import orangelab.project.common.view.WereWolfWebView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.transport.TransportCompat;
import com.networktoolkit.transport.GlobalDecorateManager;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.TransportConfig;
import com.networktoolkit.transport.URLManager;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;
import rx.android.schedulers.AndroidSchedulers;

import com.datasource.GlobalUserState;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.action.Action;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

/**
 * game_werewolf
 * 2017/6/28 上午10:13
 * Mystery
 */

public class WebViewActivity extends SafeActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, UMShareListener {

    private static final String TAG = "WebViewActivity";
    private WereWolfWebView mWebView;
    private TitleView mTitleView;
    private View mTitleViewContainer;
    private boolean isLoading = false;

    public static final String LOAD_URL = "LOAD_URL";
    public static final String LOAD_PAGE_TYPE = "LOAD_PAGE_TYPE";
    public static final String TITLE_COLOR = "TITLE_COLOR";
    private boolean mIsErrorPage;
    private View mErrorView;

    private BoxAwardDialog mBoxAwardDialog;

    private boolean isRefreshing = false;
    private boolean isRelease = false;
    private boolean isForceBack = false;

    private SwipeRefreshLayout mRefreshView;

    //相机权限
    private RxPermissions mRxPermissions;

    private View mBottomContainer;
    private View mShareWeChat;
    private View mShareGroup;
    private View mShareQQ;
    private View mShareLine;
    private View mShareFaceBook;

    private ViewGroup mMainContainer;
    private Bitmap mRankShareBitmap;


    private void setRefreshing(boolean isLoading) {
        isRefreshing = isLoading;
        mRefreshView.setRefreshing(isRefreshing);
    }

    public static void loadWithTitleColor(Context context, String url, String titleColor) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(LOAD_URL, url);
        intent.putExtra(TITLE_COLOR, titleColor);
        context.startActivity(intent);
    }

    public static void load(Context context, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(LOAD_URL, url);
        context.startActivity(intent);
    }

    public static void load(Context context, String url, String type) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(LOAD_URL, url);
        intent.putExtra(LOAD_PAGE_TYPE, type);
        context.startActivity(intent);
    }

    public static void loadStore(Context context) {
        Intent intent = new Intent(context, WebViewActivity.class);
        //TODO wangxu 修改为进入商城default tab为充值
        intent.putExtra(LOAD_URL, URLManager.GetDefaultURL() + "/recharge?access_token="
                + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
        context.startActivity(intent);
    }

    public static void LoadFamilyGrade(Context context) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(LOAD_URL, URLManager.GetDefaultURL() + "/family_grade");
        context.startActivity(intent);
    }


    protected void showErrorPage() {
        mIsErrorPage = true;
        initErrorPage();
        if (mErrorView != null) {
            mErrorView.setVisibility(View.VISIBLE);
        }
    }

    protected void hideErrorPage() {
        mIsErrorPage = false;
        if (mErrorView != null) {
            mErrorView.setVisibility(View.GONE);
        }
    }

    protected void initErrorPage() {
        if (mErrorView == null) {
            mErrorView = findViewById(R.id.webview_error);
            mErrorView.setOnClickListener(v -> {
                if (!isLoading) {
                    reload();
                }
            });
        }
    }

    public void reload() {
        if (mIsErrorPage) {
            mIsErrorPage = false;
        }
        isLoading = true;
        if (mWebView != null)
            mWebView.reload();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        EventBus.getDefault().register(this);
        mRxPermissions = new RxPermissions(this);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        mWebView = (WereWolfWebView) findViewById(R.id.webview);
        mTitleView = (TitleView) findViewById(R.id.titleView);
        mTitleViewContainer = mTitleView.findViewById(R.id.title_view_container);
        mRefreshView = (SwipeRefreshLayout) findViewById(R.id.webview_refresh);
        mRefreshView.setOnRefreshListener(this);
        mRefreshView.setEnabled(false);
        mTitleView.initRight(R.drawable.ic_action_name, v -> {
            if (!isLoading) {
                reload();
            }
        });

        /**
         * init Title
         */
        initTitleColor();

        mTitleView.setActionBack(v -> {
            goBack();
        });

        mWebView.setDrawingCacheEnabled(true);
        WebSettings settings = mWebView.getSettings();
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        settings.setJavaScriptEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(false);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setAppCachePath(this.getApplicationContext().getCacheDir().getAbsolutePath());

        String loadUrl = getIntent().getStringExtra(LOAD_URL);
        mWebView.setWebViewClient(new CustomClient());
        mWebView.setWebChromeClient(mWebChromeClient);
        judgeShouldForceBack(loadUrl);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && ProjectConfig.DEBUG_ABLE) {
            mWebView.setWebContentsDebuggingEnabled(true);
        }

        mWebView.addJavascriptInterface(new Object() {

            /**
             * 计数事件，type为事件名称
             * @param type
             */
            @JavascriptInterface
            public void reportEvent(String type) {
                ReportEventUtils.reportType(type);
            }

            @JavascriptInterface
            public void updateTitleColor(String color) {
                runOnUiThreadSafely(() -> {
                    try {
                        try {
                            mTitleViewContainer.setBackgroundColor(Color.parseColor(color));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(Color.parseColor(color));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

            @JavascriptInterface
            public void finishWhenGoBack() {
                runOnUiThreadSafely(() -> {
                    isForceBack = true;
                });
            }

            @JavascriptInterface
            public void buy(String type) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "buy: " + type);
                    PayActivity.preparePay(WebViewActivity.this, type);
                });
            }

            /**
             *
             * @param productId 产品ID
             * @param type   "inapp"  "subs" 两种类型
             *
             * inapp 普通商品
             *
             * subs 订阅类型
             */
            @JavascriptInterface
            public void purchase(String productId, String type) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "purchase productId=" + productId + " type= " + type);
                    PayActivity.preparePay(WebViewActivity.this, productId, type);
                });
            }

            @JavascriptInterface
            public void setAd(boolean config) {

            }

            @JavascriptInterface
            public void enterRoomWithFrom(String roomId, String roomType, String roomPassword, String from) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "enterRoom roomId=" + roomId + " roomType =" + roomType + " roomPassword=" + roomPassword + ",from=" + from);
                    EnterRoomPayload payload = new EnterRoomPayload();
                    payload.from = TextUtils.isEmpty(from) ? "" : from;
                    payload.password = roomPassword;
                    payload.gameType = roomType;
                    payload.roomId = roomId;
                    payload.userName = GlobalUserState.getGlobalState().getUserName();
                    payload.userId = GlobalUserState.getGlobalState().getUserId();
                    payload.userSex = GlobalUserState.getGlobalState().getUserSex();
                    payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
                    payload.exp = GlobalUserState.getGlobalState().getUserExp();
                    payload.token = GlobalUserState.getGlobalState().getToken();
                    LaunchActivity.LaunchForRoom(WebViewActivity.this, IntentDataHelper.TYPE_EVENT_ROOM_FROM_CHAT, payload);
                });
            }

            @JavascriptInterface
            public void enterRoom(String roomId, String roomType, String roomPassword) {
                /**
                 *  所有这个地方由此处进入是召集
                 *  2017-09-25 由庆勇确认此改动
                 */
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "enterRoom roomId=" + roomId + " roomType =" + roomType + " roomPassword=" + roomPassword);
                    EnterRoomPayload payload = new EnterRoomPayload();
                    payload.from = Constant.EXPORT;
                    payload.password = roomPassword;
                    payload.gameType = roomType;
                    payload.roomId = roomId;
                    payload.userName = GlobalUserState.getGlobalState().getUserName();
                    payload.userId = GlobalUserState.getGlobalState().getUserId();
                    payload.userSex = GlobalUserState.getGlobalState().getUserSex();
                    payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
                    payload.exp = GlobalUserState.getGlobalState().getUserExp();
                    payload.token = GlobalUserState.getGlobalState().getToken();
                    LaunchActivity.LaunchForRoom(WebViewActivity.this, IntentDataHelper.TYPE_EVENT_ROOM_FROM_CHAT, payload);
                });
            }

            @JavascriptInterface
            public void showSuccessDialog(String data) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "data:" + data);
                    List<AwardInfo> awardInfoList = JSONObject.parseArray(data, AwardInfo.class);
                    mBoxAwardDialog = new BoxAwardDialog(WebViewActivity.this, false, (ArrayList) awardInfoList);
                    mBoxAwardDialog.showBoxAwardDialog();
                });
            }

            @JavascriptInterface
            public void fortuneResult(String data) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "fortuneResult:" + data);
                    FortuneResult result = GsonHelper.getGson().fromJson(data, FortuneResult.class);
                    mBoxAwardDialog = new BoxAwardDialog(WebViewActivity.this, false, result);
                    mBoxAwardDialog.showBoxAwardDialog();
                });
            }

            @JavascriptInterface
            public void showUserInfo(String userId) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "userId" + userId);
                    if (!TextUtils.isEmpty(userId)) {
                        PersonalInfoActivity.Launch(WebViewActivity.this, userId, true);
                    }
                });
            }

            @JavascriptInterface
            public void sendJSCommend(String json) {
                runOnUiThreadSafely(() -> {
                    PLog.i(TAG, "sendJSCommend json =" + json);
                    /**
                     * 发射JS数据
                     */
                    UnifiedBridge.EmitReceive(json);
                });
            }
        }, "WereWolf");

        Map<String, String> extraHeaders = new HashMap<String, String>();
        if (GlobalDecorateManager.INSTANCE.getHeaderDecorate() != null) {
            GlobalDecorateManager.INSTANCE.getHeaderDecorate().decorateWebViewHeader(extraHeaders);
        }

        PLog.i(TAG, "loadUrl = " + loadUrl + "\n" + extraHeaders);

        mWebView.loadUrl(loadUrl, extraHeaders);

        initListener();
        initHandler();
        initMainView();
        initBottomView();
    }

    private void initMainView() {
        mMainContainer = (ViewGroup) findViewById(R.id.main_container);
        mMainContainer.setOnClickListener(this);
    }

    private void initTitleColor() {
        String tempColor = getIntent().getStringExtra(TITLE_COLOR);
        try {
            try {
                if (!TextUtils.isEmpty(tempColor)) {
                    mTitleViewContainer.setBackgroundColor(Color.parseColor(tempColor));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(Color.parseColor(tempColor));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.id_wechat) {
            /**
             * 分享到微信
             */
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
            if (ShareTools.existsApp(this, ShareTools.SHARE_WEIXIN)) {
                shareImage(SHARE_MEDIA.WEIXIN);
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            }
        } else if (v.getId() == R.id.id_wechat_group) {
            /**
             * 分享到朋友圈
             */
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
            if (ShareTools.existsApp(this, ShareTools.SHARE_WEIXIN)) {
                shareImage(SHARE_MEDIA.WEIXIN_CIRCLE);
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            }
        } else if (v.getId() == R.id.id_qq) {
            /**
             * 分享到QQ
             */
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
            if (ShareTools.existsApp(this, ShareTools.SHARE_QQ)) {
                if (mRxPermissions != null) {
                    mRxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if (aBoolean) {
                                    shareImage(SHARE_MEDIA.QQ);
                                } else {
                                    ToastToolKit.showShort(MessageUtils.getString(R.string.permission_read_store_error));
                                }
                            });
                }
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            }

        } else if (v.getId() == R.id.id_line) {
            /**
             * 分享到line
             */
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
            if (ShareTools.existsApp(this, ShareTools.SHARE_LINE)) {
                if (mRxPermissions != null) {
                    mRxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aBoolean -> {
                                if (aBoolean) {
                                    shareImage(SHARE_MEDIA.LINE);
                                } else {
                                    ToastToolKit.showShort(MessageUtils.getString(R.string.permission_read_store_error));
                                }
                            });
                }
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            }

        } else if (v.getId() == R.id.id_facebook) {
            /**
             * 分享到facebook
             */
            ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
            if (ShareTools.existsApp(this, ShareTools.SHARE_FACEBOOK)) {
                shareImage(SHARE_MEDIA.FACEBOOK);
            } else {
                ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
            }
        } else if (v.getId() == R.id.main_container) {
            dismissBottomView();
        }
    }

    private void shareImage(SHARE_MEDIA share_media) {
        if (mRankShareBitmap != null) {
            ShareMedia.getShareMedia().setType(Constant.SHARE);
            UMImage image = new UMImage(this, mRankShareBitmap);
            new ShareAction(this).setPlatform(share_media).withMedia(image).setCallback(this).share();
        }
    }

    private void releaseShareBitmap() {
        if (mRankShareBitmap != null) {
            mRankShareBitmap = null;
            Log.i(TAG, "releaseShareBitmap: ");
        }
        try {
            if (mWebView != null) {
                mWebView.destroyDrawingCache();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart(SHARE_MEDIA share_media) {

    }

    @Override
    public void onResult(SHARE_MEDIA share_media) {
    }

    @Override
    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
    }

    @Override
    public void onCancel(SHARE_MEDIA share_media) {
    }

    private void showBottomView() {
        mBottomContainer.setVisibility(View.VISIBLE);
    }

    private boolean bottomViewIsShowing() {
        return mBottomContainer.getVisibility() == View.VISIBLE;
    }

    private void dismissBottomView() {
        mBottomContainer.setVisibility(View.GONE);
        mMainContainer.removeAllViews();
        mMainContainer.setVisibility(View.GONE);
        UnifiedBridge.Api().SendCommendIntoJS(UnifiedConstant.SHOW_YOURSELF_DONE);
        releaseShareBitmap();
    }

    private void initBottomView() {
        mBottomContainer = findViewById(R.id.bottom_container);

        mShareWeChat = findViewById(R.id.id_wechat);
        mShareWeChat.setOnClickListener(this);

        mShareGroup = findViewById(R.id.id_wechat_group);
        mShareGroup.setOnClickListener(this);

        mShareQQ = findViewById(R.id.id_qq);
        mShareQQ.setOnClickListener(this);

        mShareLine = findViewById(R.id.id_line);
        mShareLine.setOnClickListener(this);

        mShareFaceBook = findViewById(R.id.id_facebook);
        mShareFaceBook.setOnClickListener(this);
    }

    private void initHandler() {
        UnifiedBridge.RegisterHandler(this, UnifiedConstant.ACTION_GAME_RECORD_USER_INFO, value -> {
            /**
             * 处理个人详情页面
             */
            Log.i(TAG, "initHandler: " + value);
            UserDetailDialog dialog = new UserDetailDialog(this, false, value.getModel().params.optString(Constant.USER_ID_LOWER), -1, false);
            dialog.show();
        }, false);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    private void initListener() {
        RxToolKit.Build(this, WebViewEvent.WebViewPayResultEvent.class).action(v -> runOnUiThreadSafely(() -> loadPayResult(v.getCode(), v.getMessage()))).register();
        RxToolKit.Build(this, UnifiedBridgeJSEvent.class).action(v -> runOnUiThreadSafely(() -> {
            sendNativeCommend(v.getCommend());
        })).register();

        UnifiedBridge.RegisterHandler(this, UnifiedConstant.SHOW_YOURSELF, value -> runOnUiThreadSafely(() -> {

            String type = value.getModel().params.optString(Constant.TYPE);
            String msg = value.getModel().params.optString(Constant.MSG);
            String type_mode = value.getModel().params.optString("type_mode");
            showBottomView();
            runOnUiThreadSafely(() -> {
                /**
                 * 展示一个图片
                 */
                try {
                    ImageView imageView = new ImageView(this);

                    WebViewShowYourselfView viewShowYourselfView = new WebViewShowYourselfView(this);

                    Bitmap bitmap = Bitmap.createBitmap(mWebView.getWidth(), mWebView.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    mWebView.draw(canvas);

                    Log.i(TAG, "initListener: rankShareBitmap" + bitmap.getByteCount());

                    viewShowYourselfView.updateType(type_mode);
                    viewShowYourselfView.setImage(bitmap);
                    viewShowYourselfView.setText(msg, 0);

                    mRankShareBitmap = viewShowYourselfView.createBitmap();
                    imageView.setImageBitmap(mRankShareBitmap);

                    Log.i(TAG, "initListener: rankShareBitmap" + mRankShareBitmap.getByteCount());

                    mMainContainer.setVisibility(View.VISIBLE);
                    mMainContainer.removeAllViews();
                    mMainContainer.addView(imageView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                } catch (OutOfMemoryError e) {
                    /**
                     * 如果出现了OOM这里直接不管了,认为这次操作失败
                     */
                    ToastToolKit.showShort(R.string.string_action_failed_oom);

                    dismissBottomView();
                    releaseShareBitmap();
                }
            });
        }), false);
    }

    private void loadPayResult(int code, String msg) {
        if (mWebView != null)
            mWebView.loadUrl("javascript: payCallBack('" + code + "','" + msg + "')");
    }

    private void sendNativeCommend(String json) {
        if (mWebView != null) {
            PLog.i(TAG, "sendNativeCommend " + json);
            mWebView.loadUrl("javascript:sendNativeCommend('" + json + "')");
        }
    }

    @Override
    public void onRefresh() {
        setRefreshing(true);
        if (mWebView != null) {
            mWebView.reload();
        }
    }

    public void setTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            return;
        }
        if (title.contains(".")) {
            /**
             * 这里做了一个最简单粗暴的过滤，含有 . 的字符串不显示
             */
            return;
        }
        mTitleView.setTitle(title);
    }

    private class CustomClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            PLog.i(TAG, "onPageStarted: ");
            isLoading = true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            PLog.i(TAG, "onPageFinished: ");
            runOnUiThreadSafely(() -> {
                isLoading = false;
                if (mIsErrorPage) {
                    setTitle(MessageUtils.getString(R.string.webview_error_title));
                } else {
                    setTitle(view.getTitle());
                    hideErrorPage();
                }
                setRefreshing(false);
            });
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            PLog.i(TAG, "shouldOverrideUrlLoading: ");
            runOnUiThreadSafely(() -> {
                PLog.i(TAG, "shouldOverrideUrlLoading: " + url);
                if (mWebView != null)
                    mWebView.loadUrl(url);
            });
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            PLog.i(TAG, "onReceivedError: " + errorCode);
            runOnUiThreadSafely(() -> {
                mIsErrorPage = true;
                if (mIsErrorPage) {
                    showErrorPage();
                } else {
                    hideErrorPage();
                }
            });
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            Log.i(TAG, "shouldInterceptRequest: WebView view, String url[" + Thread.currentThread() + "]");
            WebResourceResponse resourceResponse = TransportCompat.compatWebViewInterceptRequest(view, url);
            if (resourceResponse == null) {
                return super.shouldInterceptRequest(view, url);
            } else {
                return resourceResponse;
            }
        }

        @Nullable
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            Log.i(TAG, "shouldInterceptRequest: WebView view, WebResourceRequest request");
            return super.shouldInterceptRequest(view, request);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void finish() {
        super.finish();
        release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        release();
    }

    private void release() {
        if (!isRelease) {
            UnifiedBridge.UnRegisterHandler(this);
            releaseShareBitmap();
            try {
                mRefreshView.removeAllViews();

                if (mWebView != null) {
                    mWebView.removeAllViews();
                    mWebView.clearHistory();
                    mWebView.onPause();
                    mWebView.destroyDrawingCache();
                    mWebView.destroy();
                    mWebView = null;
                }
                RxToolKit.UnRegister(this);
                UnifiedBridge.UnRegisterHandler(this);
                RoomSocketEngineHelper.userLeaveFromWeb();
                PLog.i(TAG, "onDestroy: GameHelper.userLeaveFromWeb();");
            } catch (Exception e) {
                e.printStackTrace();
            }
            isRelease = true;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (isForceBack) {
            finish();
            return;
        }
        if (mWebView == null)
            return;
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    public static final int INPUT_FILE_REQUEST_CODE = 1;
    private final static int FILECHOOSER_RESULTCODE = 2;
    private final static int FILECHOOSER_RESULTCODE_OTHER = 3;
    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> mFilePathCallback;
    private ValueCallback<String[]> mFilePathCallbackOther;

    private String mCameraPhotoPath;

    //在sdcard卡创建缩略图
    //createImageFileInSdcard
    @SuppressLint("SdCardPath")
    private File createImageFile() {
        File file = new File(Environment.getExternalStorageDirectory() + "/", "tmp.png");
        mCameraPhotoPath = file.getAbsolutePath();
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView webView, int newProgress) {
            super.onProgressChanged(webView, newProgress);
            if (mWebView.progressbar == null) return;
            if (newProgress >= 100) {
                mWebView.progressbar.setVisibility(View.GONE);
            } else {
                if (mWebView.progressbar.getVisibility() == View.GONE)
                    mWebView.progressbar.setVisibility(View.VISIBLE);
                mWebView.progressbar.setProgress(newProgress);
            }
        }

        // android 5.0 这里需要使用android5.0 sdk
        @TargetApi(21)
        @Override
        public boolean onShowFileChooser(
                WebView webView, ValueCallback<Uri[]> filePathCallback,
                WebChromeClient.FileChooserParams fileChooserParams) {

            mRxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aBoolean -> {
                        if (aBoolean) {
                            mRxPermissions.request(Manifest.permission.CAMERA)
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(can -> {
                                        if (can) {
                                            try {
                                                if (mFilePathCallback != null) {
                                                    mFilePathCallback.onReceiveValue(null);
                                                }

                                                mFilePathCallback = filePathCallback;
                                                openFile(INPUT_FILE_REQUEST_CODE);
                                            } catch (Exception e) {
                                                PLog.e(TAG, "onShowFileChooser error: " + e.getMessage());
                                            }

                                        } else {
                                            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_permission_camera_error)));
                                        }
                                    });
                        } else {
                            ToastToolKit.showShort(R.string.permission_read_store_error);
                        }
                    });

            return true;
        }


        //The undocumented magic method override
        //Eclipse will swear at you if you try to put @Override here
        // For Android <3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            mUploadMessage = uploadMsg;
//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//            i.addCategory(Intent.CATEGORY_OPENABLE);
//            i.setType("image/*");
//            WebViewActivity.this.startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILECHOOSER_RESULTCODE);

            openFile(FILECHOOSER_RESULTCODE);
        }

        // For Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//            i.addCategory(Intent.CATEGORY_OPENABLE);
//            i.setType("image/*");
//            WebViewActivity.this.startActivityForResult(
//                    Intent.createChooser(i, "Image Chooser"),
//                    FILECHOOSER_RESULTCODE);

            openFile(FILECHOOSER_RESULTCODE);
        }

        //For Android 4.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            mUploadMessage = uploadMsg;
//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//            i.addCategory(Intent.CATEGORY_OPENABLE);
//            i.setType("image/*");
//            WebViewActivity.this.startActivityForResult(Intent.createChooser(i, "Image Chooser"), WebViewActivity.FILECHOOSER_RESULTCODE);

            openFile(FILECHOOSER_RESULTCODE);
        }

        // Work on Android 4.4.2 Zenfone 5
        public void showFileChooser(ValueCallback<String[]> filePathCallback, String acceptType, boolean paramBoolean) {
            //只有android 4.4.2 走这里，其他4.0-5.0不走，this is really fuck dog!!!!!
            PLog.e(TAG, "showFileChooser + [ filePathCallback = " + filePathCallback + "]");
            mFilePathCallbackOther = filePathCallback;

            openFile(FILECHOOSER_RESULTCODE_OTHER);
        }

        public void showFileChooser(ValueCallback<String[]> uploadFileCallback, FileChooserParams fileChooserParams) {
            PLog.e(TAG, "showFileChooser + [ showFileChooser = " + uploadFileCallback + "]");
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            String titleString = mWebView.getTitle();
//            if (!Utils.isWebsite(titleString)) {
//                mTitleView.setTitle(mWebView.getTitle());
//            }
            setTitle(mWebView.getTitle());
        }

        private void openFile(int code) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    //设置MediaStore.EXTRA_OUTPUT路径,相机拍照写入的全路径
                    photoFile = createImageFile();
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                } catch (Exception ex) {
                    // Error occurred while creating the File
                    PLog.e("WebViewSetting", "Unable to create Image File", ex);
                }

                // Continue only if the File was successfully created
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                    System.out.println(mCameraPhotoPath);
                } else {
                    takePictureIntent = null;
                }
            }

            Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
            contentSelectionIntent.setType("image/*");
            Intent[] intentArray;
            if (takePictureIntent != null) {
                intentArray = new Intent[]{takePictureIntent};
                System.out.println(takePictureIntent);
            } else {
                intentArray = new Intent[0];
            }

            Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

            startActivityForResult(chooserIntent, code);
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        try {
            UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == mUploadMessage) return;
                Uri result = data == null || resultCode != RESULT_OK ? null
                        : data.getData();
                if (result != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        String imagePath = ImageFilePath.getPath(this, result);

                        if (!TextUtils.isEmpty(imagePath)) {
                            result = Uri.parse("file:///" + imagePath);
                            PLog.e("image_photo_path", result.getPath());
                        }
                    }
                } else if (resultCode == RESULT_OK) {
                    if (mCameraPhotoPath != null) {
                        result = Uri.parse(mCameraPhotoPath.replace("file:", "file:///"));
                        PLog.e("camera_photo_path", result.getPath());
                    }
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;

                ReportEventUtils.reportUpLoadChangeFace("dress");
            } else if (requestCode == INPUT_FILE_REQUEST_CODE && mFilePathCallback != null) {
                // 5.0的回调
                Uri[] results = null;

                // Check that the response is a good one
                if (resultCode == Activity.RESULT_OK) {
                    if (data == null) {
                        // If there is not data, then we may have taken a photo
                        if (mCameraPhotoPath != null) {
                            PLog.e("camera_photo_path", mCameraPhotoPath);
                            results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        }
                    } else {
                        String dataString = data.getDataString();
                        PLog.e("camera_dataString", dataString);
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }

                mFilePathCallback.onReceiveValue(results);
                mFilePathCallback = null;

                ReportEventUtils.reportUpLoadChangeFace("dress");

            } else if (requestCode == FILECHOOSER_RESULTCODE_OTHER && mFilePathCallbackOther != null) {
                String[] results = null;
                Uri result = data == null || resultCode != RESULT_OK ? null
                        : data.getData();
                if (result != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        String imagePath = ImageFilePath.getPath(this, result);

                        if (!TextUtils.isEmpty(imagePath)) {
                            result = Uri.parse("file:///" + imagePath);
                            PLog.e("image_photo_path", result.getPath());
                        }
                    }
                } else if (resultCode == RESULT_OK) {
                    if (mCameraPhotoPath != null) {
                        result = Uri.parse(mCameraPhotoPath.replace("file:", "file:///"));
                        PLog.e("camera_photo_path", result.getPath());
                    }
                }
                if (result != null) {
                    String path1 = result.getPath();
                    if (path1 != null) {
                        results = new String[]{path1};
                    }
                }

                mFilePathCallbackOther.onReceiveValue(results);
                mFilePathCallbackOther = null;
                ReportEventUtils.reportUpLoadChangeFace("dress");

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
        } catch (Throwable e) {
            PLog.e(TAG, "onActivityResult error: " + e.getMessage());
        }
    }

    /**
     * 微信分享成功的回调
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShareWeChatEvent event) {
        org.json.JSONObject jsParams = new org.json.JSONObject();
        try {
            jsParams.put("action", "ACTION_SHARE_H5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendNativeCommend(jsParams.toString());
    }

    /**
     * Web通知本地隐藏刷新按钮
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(WebToNativeHideEvent event) {
        if (mTitleView != null) {
            mTitleView.hideRight();
        }
    }

    private void judgeShouldForceBack(String loadUrl) {
        if (!TextUtils.isEmpty(loadUrl) && loadUrl.contains("change_face")) {
            isForceBack = false;
        } else {
            isForceBack = true;
        }
    }
}
