package orangelab.project.common;

import orangelab.project.common.model.PersonalData;

/**
 * Author:1727
 * 2017/3/7
 */

public class PersonalDataHelper {

    public static PersonalData personalData;

    public static synchronized PersonalData getPersonalData() {
        return personalData;
    }

    public static synchronized void setPersonalData(PersonalData personalData) {
        PersonalDataHelper.personalData = personalData;
    }
}
