package orangelab.project.common.model;

import orangelab.project.common.dialog.IntviuFriendDialog;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/9/19 下午7:11
 * Mystery
 */

public class BlockListResult {

    public int skip;
    public int limit;
    public int total;
    public ArrayList<IntviuFriendDialog.FriendItem> users;
}
