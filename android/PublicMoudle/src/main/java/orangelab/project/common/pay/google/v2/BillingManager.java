/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package orangelab.project.common.pay.google.v2;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClient.FeatureType;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.Purchase.PurchasesResult;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Handles all the interactions with Play Store (via Billing library), maintains connection to
 * it through BillingClient and caches temporary states/data if needed
 */
public class BillingManager implements PurchasesUpdatedListener {

    private static final String TAG = "GooglePayV2";

    /**
     * A reference to BillingClient
     **/
    private BillingClient mBillingClient;

    /**
     * True if billing service is connected now.
     */
    private boolean mIsServiceConnected;

    private final BillingManagerObserver mBillingManagerObserver;

    private final Activity mActivity;

    private final List<Purchase> mPurchases = new ArrayList<>();

    private Set<String> mTokensToBeConsumed;

    /**
     * Listener to the updates that happen when purchases list was updated or consumption of the
     * item was finished
     */
    public interface BillingManagerObserver {

        /**
         * 这里更改了原本的BillingManager流程,将原本Listener 进行了更细的划分
         */
        void onSetupFinished();

        void onSetupError(int errorCode);

        void onPurchasesQuery(List<Purchase> purchases);

        void onPurchasesQueryError(int errorCode);

        void onPurchasesUpdated(List<Purchase> purchases);

        void onPurchaseFlowError(int errorCode);

        void onPurchasesUpdatedError(int errorCode);

        void onConsumeSuccess(String token);

        void onConsumeError(int errorCode);
    }

    /**
     * Listener for the Billing client state to become connected
     */
    public interface ServiceConnectedListener {
        void onServiceConnected(@BillingResponse int resultCode);
    }

    public BillingManager(Activity activity, final BillingManagerObserver updatesListener) {
        Log.i(TAG, "Creating Billing client.");
        mActivity = activity;
        mBillingManagerObserver = updatesListener;
        mBillingClient = BillingClient.newBuilder(mActivity).setListener(this).build();
        // Start setup. This is asynchronous and the specified listener will be called
        // once setup completes.
        // It also starts to report all the new purchases through onPurchasesQuery() callback.
    }

    /**
     * Handle a callback that purchases were updated from the Billing library
     */
    @Override
    public void onPurchasesUpdated(int resultCode, List<Purchase> purchases) {
        if (resultCode == BillingResponse.OK) {
            Log.i(TAG, "onPurchasesUpdated: purchases = " + purchases);
            if (mBillingManagerObserver != null) {
                mBillingManagerObserver.onPurchasesUpdated(purchases);
            }
        } else if (resultCode == BillingResponse.USER_CANCELED) {
            Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
            if (mBillingManagerObserver != null) {
                mBillingManagerObserver.onPurchasesUpdatedError(BillingResponse.USER_CANCELED);
            }
        } else {
            Log.i(TAG, "onPurchasesUpdated() got unknown resultCode: " + resultCode);
            if (mBillingManagerObserver != null) {
                mBillingManagerObserver.onPurchasesUpdatedError(resultCode);
            }
        }
    }

    /**
     * Start a purchase flow
     */
    public void initiatePurchaseFlow(final String skuId, final @SkuType String billingType) {
        initiatePurchaseFlow(skuId, null, billingType);
    }

    /**
     * Start a purchase or subscription replace flow
     */
    public void initiatePurchaseFlow(final String skuId, final ArrayList<String> oldSkus,
                                     final @SkuType String billingType) {
        Runnable purchaseFlowRequest = () -> {
            Log.i(TAG, "initiatePurchaseFlow: sku=" + skuId);
            Log.i(TAG, "Launching in-app purchase flow. Replace old SKU? " + (oldSkus != null));
            BillingFlowParams purchaseParams = BillingFlowParams.newBuilder().setSku(skuId).setType(billingType).setOldSkus(oldSkus).build();
            int resultCode = mBillingClient.launchBillingFlow(mActivity, purchaseParams);
            if (resultCode == BillingResponse.OK) {
                /**
                 * 购买流程OK
                 */

            } else {
                /**
                 * 购买流程不OK
                 */
                if (mBillingManagerObserver != null) {
                    mBillingManagerObserver.onPurchaseFlowError(resultCode);
                }
            }
        };

        executeServiceRequest(purchaseFlowRequest);
    }

    public Context getContext() {
        return mActivity;
    }

    /**
     * Clear the resources
     */
    public void destroy() {
        Log.i(TAG, "Destroying the manager.");

        if (mBillingClient != null && mBillingClient.isReady()) {
            mBillingClient.endConnection();
            mBillingClient = null;
        }
    }

    public void querySkuDetailsAsync(@SkuType final String itemType, final List<String> skuList,
                                     final SkuDetailsResponseListener listener) {
        // Creating a runnable from the request to use it inside our connection retry policy below
        Runnable queryRequest = new Runnable() {
            @Override
            public void run() {
                // Query the purchase async
                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(itemType);
                mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                        Log.i(TAG, "onSkuDetailsResponse: [" + responseCode + "] details " + skuDetailsList);
                        listener.onSkuDetailsResponse(responseCode, skuDetailsList);
                    }
                });
            }
        };

        executeServiceRequest(queryRequest);
    }

    public void consumeAsync(final String purchaseToken, final ConsumeResponseListener consumeResponseListener) {
        if (mTokensToBeConsumed == null) {
            mTokensToBeConsumed = new HashSet<>();
        } else if (mTokensToBeConsumed.contains(purchaseToken)) {
            Log.i(TAG, "Token was already scheduled to be consumed - skipping...");
            return;
        }

        mTokensToBeConsumed.add(purchaseToken);

        Runnable consumeRequest = () -> {
            // Consume the purchase async
            Log.i(TAG, "consumeAsync: begin");
            mBillingClient.consumeAsync(purchaseToken, consumeResponseListener);
        };

        executeServiceRequest(consumeRequest);
    }

    public void consumeAsync(final String purchaseToken) {

        final ConsumeResponseListener onConsumeListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@BillingResponse int responseCode, String purchaseToken) {
                // If billing service was disconnected, we try to reconnect 1 time
                // (feel free to introduce your retry policy here).
                if (responseCode == BillingResponse.OK) {
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onConsumeSuccess(purchaseToken);
                    }
                } else {
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onConsumeError(responseCode);
                    }
                }
            }
        };

        consumeAsync(purchaseToken, onConsumeListener);

        // If we've already scheduled to consume this token - no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()
//        if (mTokensToBeConsumed == null) {
//            mTokensToBeConsumed = new HashSet<>();
//        } else if (mTokensToBeConsumed.contains(purchaseToken)) {
//            Log.i(TAG, "Token was already scheduled to be consumed - skipping...");
//            return;
//        }
//        mTokensToBeConsumed.add(purchaseToken);
//
//        // Generating Consume Response listener
//        final ConsumeResponseListener onConsumeListener = new ConsumeResponseListener() {
//            @Override
//            public void onConsumeResponse(@BillingResponse int responseCode, String purchaseToken) {
//                // If billing service was disconnected, we try to reconnect 1 time
//                // (feel free to introduce your retry policy here).
//                if (responseCode == BillingResponse.OK) {
//                    if (mBillingManagerObserver != null) {
//                        mBillingManagerObserver.onConsumeSuccess(purchaseToken);
//                    }
//                } else {
//                    if (mBillingManagerObserver != null) {
//                        mBillingManagerObserver.onConsumeError(responseCode);
//                    }
//                }
//            }
//        };
//
//        // Creating a runnable from the request to use it inside our connection retry policy below
//        Runnable consumeRequest = new Runnable() {
//            @Override
//            public void run() {
//                // Consume the purchase async
//                mBillingClient.consumeAsync(purchaseToken, onConsumeListener);
//            }
//        };
//
//        executeServiceRequest(consumeRequest);
    }

    /**
     * Returns the value Billing client response code or BILLING_MANAGER_NOT_INITIALIZED if the
     * clien connection response was not received yet.
     */
//    public int getBillingClientResponseCode() {
//        return mBillingClientResponseCode;
//    }

    /**
     * Handle a result from querying of purchases and report an updated list to the listener
     */
    private void onQueryPurchasesFinished(PurchasesResult result) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingResponse.OK) {
            Log.i(TAG, "Billing client was null or result code (" + result.getResponseCode() + ") was bad - quitting");
            if (mBillingManagerObserver != null) {
                mBillingManagerObserver.onPurchasesQueryError(result.getResponseCode());
            }
            return;
        }

        Log.i(TAG, "Query inventory was successful.");

        // Update the UI and purchases inventory with new list of purchases
        mPurchases.clear();

        /**
         * 查询Purchases单独写一个方法
         */
        if (mBillingManagerObserver != null) {
            mBillingManagerObserver.onPurchasesQuery(result.getPurchasesList());
        }
    }

    /**
     * Checks if subscriptions are supported for current client
     * <p>Note: This method does not automatically retry for RESULT_SERVICE_DISCONNECTED.
     * It is only used in unit tests and after queryPurchases execution, which already has
     * a retry-mechanism implemented.
     * </p>
     */
    public boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS);
        if (responseCode != BillingResponse.OK) {
            Log.i(TAG, "areSubscriptionsSupported() got an error response: " + responseCode);
        }
        return responseCode == BillingResponse.OK;
    }

    /**
     * 查询In-App类型
     */
    public void queryPurchasesInApp() {
        Runnable runnable = () -> {
            long time = System.currentTimeMillis();
            /**
             * 查询App-in结果
             */
            PurchasesResult purchasesResult = mBillingClient.queryPurchases(SkuType.INAPP);
            Log.i(TAG, "Querying In-app purchases elapsed time: " + (System.currentTimeMillis() - time) + "ms");
            if (purchasesResult.getResponseCode() == BillingResponse.OK) {

            } else {
                Log.i(TAG, "queryPurchases() In-app got an error response code: " + purchasesResult.getResponseCode());
                if (mBillingManagerObserver != null) {
                    mBillingManagerObserver.onPurchasesQueryError(purchasesResult.getResponseCode());
                }
                return;
            }
            onQueryPurchasesFinished(purchasesResult);
        };
        executeServiceRequest(runnable);
    }

    /**
     * 查询订阅类型产品
     */
    public void queryPurchasesSubs() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                // If there are subscriptions supported, we add subscription rows as well
                if (areSubscriptionsSupported()) {
                    /**
                     * 查询订阅结果
                     */
                    PurchasesResult subscriptionResult = mBillingClient.queryPurchases(SkuType.SUBS);
                    Log.i(TAG, "Querying subscriptions elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                    Log.i(TAG, "Querying subscriptions result code: " + subscriptionResult.getResponseCode() + " res: " + subscriptionResult.getPurchasesList().size());
                    if (subscriptionResult.getResponseCode() == BillingResponse.OK) {
                        onQueryPurchasesFinished(subscriptionResult);
                    } else {
                        Log.i(TAG, "Got an error response trying to query subscription purchases");
                        if (mBillingManagerObserver != null) {
                            mBillingManagerObserver.onPurchasesQueryError(subscriptionResult.getResponseCode());
                        }
                        return;
                    }
                } else {
                    Log.i(TAG, "queryPurchases() got an error response code: " + BillingResponse.FEATURE_NOT_SUPPORTED);
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onPurchasesQueryError(BillingResponse.FEATURE_NOT_SUPPORTED);
                    }
                    return;
                }
            }
        };
        executeServiceRequest(runnable);
    }

    public void queryPurchases() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                /**
                 * 查询App-in结果
                 */
                PurchasesResult purchasesResult = mBillingClient.queryPurchases(SkuType.INAPP);
                Log.i(TAG, "Querying purchases elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                // If there are subscriptions supported, we add subscription rows as well
                if (areSubscriptionsSupported()) {
                    /**
                     * 查询订阅结果
                     */
                    PurchasesResult subscriptionResult = mBillingClient.queryPurchases(SkuType.SUBS);
                    Log.i(TAG, "Querying purchases and subscriptions elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                    Log.i(TAG, "Querying subscriptions result code: " + subscriptionResult.getResponseCode() + " res: " + subscriptionResult.getPurchasesList().size());

                    if (subscriptionResult.getResponseCode() == BillingResponse.OK) {
                        purchasesResult.getPurchasesList().addAll(subscriptionResult.getPurchasesList());
                    } else {
                        Log.i(TAG, "Got an error response trying to query subscription purchases");
                        if (mBillingManagerObserver != null) {
                            mBillingManagerObserver.onPurchasesQueryError(purchasesResult.getResponseCode());
                        }
                        return;
                    }
                } else if (purchasesResult.getResponseCode() == BillingResponse.OK) {
                    Log.i(TAG, "Skipped subscription purchases query since they are not supported");
                } else {
                    Log.i(TAG, "queryPurchases() got an error response code: " + purchasesResult.getResponseCode());
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onPurchasesQueryError(purchasesResult.getResponseCode());
                    }
                    return;
                }
                onQueryPurchasesFinished(purchasesResult);
            }
        };
        executeServiceRequest(runnable);
    }

    private void startServiceConnection(final Runnable executeOnSuccess) {
        Log.i(TAG, "Starting setup.");
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingResponse int billingResponseCode) {
                Log.i(TAG, "Setup finished. Response code: " + billingResponseCode);

                if (billingResponseCode == BillingResponse.OK) {
                    mIsServiceConnected = true;
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onSetupFinished();
                    }
                    if (executeOnSuccess != null) {
                        executeOnSuccess.run();
                    }
                } else {
                    mIsServiceConnected = false;
                    if (mBillingManagerObserver != null) {
                        mBillingManagerObserver.onSetupError(billingResponseCode);
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                Log.i(TAG, "onBillingServiceDisconnected: ");
                mIsServiceConnected = false;
            }
        });
    }

    public void executeServiceRequest(Runnable successRunnable) {
        if (mIsServiceConnected) {
            successRunnable.run();
        } else {
            // If billing service was disconnected, we try to reconnect 1 time.
            // (feel free to introduce your retry policy here).
            startServiceConnection(successRunnable);
        }
    }
}

