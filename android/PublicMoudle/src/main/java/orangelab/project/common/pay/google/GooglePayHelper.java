package orangelab.project.common.pay.google;

import android.app.Activity;
import android.content.Intent;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.RxToolKit;
import com.R;
import orangelab.project.common.db.DBConstant;
import orangelab.project.common.db.UserPayOrderDaoHelper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.WebViewEvent;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.PayHelper;
import orangelab.project.common.pay.PayHelperListener;
import orangelab.project.common.pay.google.util.IabHelper;
import orangelab.project.common.pay.google.util.IabResult;
import orangelab.project.common.pay.google.util.Inventory;
import orangelab.project.common.pay.google.util.Purchase;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/7/20 下午5:37
 * Mystery
 * Google支付的帮助类
 */

public class GooglePayHelper extends PayHelper implements
        IabHelper.OnIabSetupFinishedListener,
        IabHelper.QueryInventoryFinishedListener,
        IabHelper.OnIabPurchaseFinishedListener,
        IabHelper.OnConsumeFinishedListener {

    private static final String TAG = "GooglePayHelper";
    private static final int RC_REQUEST = 10001;
    private static final int RETRY_COUNT_MAX = 3;

    private Activity mActivity;
    private IabHelper mIabHelper;
    private boolean mSetUpSuccess = false;
    private SafeHandler mSafeHandler = null;
    private UserPayOrderDaoHelper mDaoHelper = null;
    private int retryCount = 0;

    public GooglePayHelper(Activity activity, String productId, PayHelperListener mPayListener) {
        super(productId, mPayListener);
        this.mActivity = activity;
        mIabHelper = new IabHelper(activity, "");
        mIabHelper.enableDebugLogging(PLog.needLog, TAG);
        mSafeHandler = new SafeHandler(Looper.getMainLooper());
        mDaoHelper = new UserPayOrderDaoHelper(activity);
        PLog.i(TAG, "productId " + productId);
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        Log.i(TAG, "onIabSetupFinished: ");
        if (!result.isSuccess()) {
            Log.i(TAG, "Error:" + result);
            onErrorFromGoogle(result);
            return;
        }

        if (mIabHelper == null) {
            onErrorFromGoogle(result);
            return;
        }

        mSetUpSuccess = true;

        Log.i(TAG, "onIabSetupFinished: IabSetUp Success");
        /**
         * 开始查询列表
         */
        mIabHelper.queryInventoryAsync(this);
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
        if (mIabHelper == null) {
            onErrorFromGoogle(result);
            return;
        }
        if (!result.isSuccess()) {
            onErrorFromGoogle(result);
            return;
        }

        /**
         * 判断是否还有这个产品
         */
        Purchase ownProduct = inv.getPurchase(mType);
        if (ownProduct != null) {
            try {
                /**
                 * 拥有这个产品我们先去数据库里面查询查看是否会有这条数据
                 */
                PLog.i(TAG, ownProduct.toString());
                UserPayOrderEntity entity = mDaoHelper.getUserPayOrderByOrderId(ownProduct.getOrderId());
                if (Utils.targetIsNull(entity)) {
                    //没有这条数据那么 我们就汇报给服务器按照当前的数据
                    PLog.i(TAG, "we don't have this entity in dataBase so we report use curToken");
                    reportServerIfHasProduct(ownProduct, GlobalUserState.getGlobalState().getToken());
                } else {
                    //如果数据库里有这条数据那么我们就判断这个数据的Token是否存在
                    String token = entity.getOtherData2();
                    if (TextUtils.isEmpty(token)) {
                        PLog.i(TAG, "we  have this entity in dataBase,but this token is null, so we report use curToken");
                        reportServerIfHasProduct(ownProduct, GlobalUserState.getGlobalState().getToken());
                    } else {
                        PLog.i(TAG, "we have this entity in dataBase ,so we report use entity curToken");
                        reportServerIfHasProduct(ownProduct, token);
                    }
                }
            } catch (Exception e) {
                onErrorFromGoogle(new IabResult(IabHelper.IABHELPER_BAD_RESPONSE, ""));
            }
            return;
        }
        /**
         * 没有这个库存直接进行购买操作
         */
        buy(mType);

    }

    private void reportServerIfHasProduct(Purchase purchase1, String token) {
        reportServerDataInner(purchase1.getOrderId(), purchase1.getSignature(), purchase1.getOriginalJson(), token, () -> {
            /**
             * 汇报成功进行消费
             */
            PLog.i(TAG, "we finish report own product order " + purchase1.toString());
            PLog.i(TAG, "we begin consumeAsync ");
            mIabHelper.consumeAsync(purchase1, ((purchase, result1) -> {
                if (mIabHelper == null) {
                    onErrorFromGoogle(result1);
                    return;
                }
                if (result1.isFailure()) {
                    Log.i(TAG, "Error:" + result1);
                    onErrorFromGoogle(result1);
                    return;
                }
                PLog.i(TAG, "we finish consumeAsync success ");
                /**
                 * 处理购买
                 */
                buy(mType);
            }));
        });
    }

    public boolean needRetry() {
        if (retryCount < RETRY_COUNT_MAX) {
            return true;
        } else {
            return false;
        }
    }

    public void reportServerDataInner(String orderId, String signature, String data, String token, Runnable success) {
        ThreadToolKit.Async(() -> {
            JSONObject json = new JSONObject();
            try {
                json.put(Constant.SIGNATURE, signature);
                json.put(Constant.DATA, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestTask mRequestTask = RequestTask.create(
                    false,
                    Constant.GOOGLE_PAY,
                    token,
                    json,
                    new RequestTaskCallBack() {
                        @Override
                        public void onSuccess(String response) {
                            mSafeHandler.postSafely(() -> {
                                PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                                if (result.purchase.size() == 0) {
                                    onErrorFromGoogle(new IabResult(IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR, ""));
                                    return;
                                } else {
                                    PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                    switch (item.getOp_result()) {
                                        case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                            Utils.runSafely(() -> {
                                                if (mDaoHelper != null) {
                                                    mDaoHelper.deleteData(
                                                            GlobalUserState.getGlobalState().getUserId(),
                                                            orderId
                                                    );
                                                }
                                            });
                                            ReportRobot.robot.report("GOOGLE_PAY_SERVER_SUCCESS");
                                            if (success != null) {
                                                success.run();
                                            }
                                        }
                                        break;
                                        default: {
                                            onErrorFromGoogle(new IabResult(IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR, ""));
                                        }
                                        break;
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailed(int code, String failed) {
                            onErrorFromGoogle(new IabResult(IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR, ""));
                        }

                        @Override
                        public void onError(Exception e) {
                            onErrorFromGoogle(new IabResult(IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR, ""));
                        }
                    }
            );
            mRequestTask.execute();
        });
    }

    public void reportServerData(String orderId, String signature, String data, Runnable success, Runnable failed) {
        JSONObject json = new JSONObject();
        try {
            json.put(Constant.SIGNATURE, signature);
            json.put(Constant.DATA, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask mRequestTask = RequestTask.create(
                false,
                Constant.GOOGLE_PAY,
                GlobalUserState.getGlobalState().getToken(),
                json,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        /**
                         * 这里说明支付成了，需要把这个数据删除
                         */
                        mSafeHandler.postSafely(() -> {
                            PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                            if (result.purchase.size() == 0) {
                                ReportRobot.robot.report("SERVER_RETURN_DATA_ERROR_SIZE_0");
                            } else {
                                PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                switch (item.getOp_result()) {
                                    case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                        retryCount = 0;
                                        Utils.runSafely(() -> {
                                            if (mDaoHelper != null) {
                                                mDaoHelper.deleteData(
                                                        GlobalUserState.getGlobalState().getUserId(),
                                                        orderId
                                                );
                                            }
                                        });
                                        ReportRobot.robot.report("GOOGLE_PAY_SERVER_SUCCESS");
                                        if (success != null) {
                                            success.run();
                                        }
                                    }
                                    break;
                                    case GooglePayUtils.SERVER_PAY_DOING://处理中
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_DOING, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_DOING));
                                        }
                                        break;
                                    case GooglePayUtils.SERVER_PAY_UPDATE_FAIL://更新失败
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_UPDATE_FAIL, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_UPDATE_FAIL));
                                        }
                                        break;
                                    case GooglePayUtils.SERVER_PAY_SERVER_ERROR://服务器处理错误
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                                        }
                                        break;
                                    case GooglePayUtils.SERVER_PAY_FAIL:// 支付失败
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_FAIL, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_FAIL));
                                        }
                                        break;
                                    case GooglePayUtils.SERVER_PAY_UNKNOWN_PRODUCT: //未知产品
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_UNKNOWN_PRODUCT, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_UNKNOWN_PRODUCT));
                                        }
                                        break;
                                    case GooglePayUtils.SERVER_PAY_VERIFY_FAIL: //校验失败
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_VERIFY_FAIL, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_VERIFY_FAIL));
                                        }
                                        break;
                                    default:
                                        if (needRetry()) {
                                            retryCount++;
                                            reportServerData(orderId, signature, data, success, failed);
                                        } else {
                                            onErrorFromServer(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                                        }
                                        break;
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailed(int code, String fail) {
                        Log.i(TAG, "onFailed: " + code + " fail reason " + fail);
                        /**
                         * 此处如果失败，将订单存根据Code存放到异常订单系列
                         */
                        onErrorFromServer(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.i(TAG, "onErrorFromGoogle: ");
                        /**
                         * 此处如果失败，将订单存放到异常订单系列
                         */
                        onErrorFromServer(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                    }
                }
        );
        mRequestTask.execute();
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (mIabHelper == null) {
            onErrorFromGoogle(result);
            return;
        }

        if (result.isFailure()) {
            onErrorFromGoogle(result);
            return;
        }

        /**
         * 向服务器请求校验接口
         */
        if (info != null && info.getPurchaseState() == 0) {
            /**
             * 报告支付成功
             */
            ReportRobot.robot.report("GOOGLE_PAY_SUCCESS");
            /**
             * 向数据库插入一条数据，作为订单的一个记录
             */
            UserPayOrderEntity entity = new UserPayOrderEntity();
            entity.setUserId(GlobalUserState.getGlobalState().getUserId());
            entity.setOrderState(DBConstant.PAY_SUCCESS_BUT_NOT_REPORT_SERVER_INAPP);
            entity.setProductId(info.getSku());
            entity.setPackageName(info.getPackageName());
            entity.setOrderId(info.getOrderId());
            entity.setPurchaseTime(info.getPurchaseTime());
            entity.setPurchaseToken(info.getToken());
            entity.setPurchaseData(info.getOriginalJson());
            entity.setSignature(info.getSignature());
            entity.setOtherData1("0");
            entity.setOtherData2(GlobalUserState.getGlobalState().getToken());
            try {
                if (mDaoHelper != null) {
                    mDaoHelper.insertData(entity);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.runSafely(() -> {
                    ReportRobot.robot.report("INSERT_PAY_ORDER_FAILED");
                });
            }
            reportServerData(info.getOrderId(), info.getSignature(), info.getOriginalJson(),
                    () -> {
                        mIabHelper.consumeAsync(info, this);
                    },
                    () -> {

                    });
        } else {
            onErrorFromGoogle(new IabResult(IabHelper.IABHELPER_BAD_RESPONSE, ""));
            return;
        }
    }

    @Override
    public void onConsumeFinished(Purchase purchase, IabResult result) {
        if (mIabHelper == null) {
            onErrorFromGoogle(result);
            return;
        }

        if (result.isFailure()) {
            onErrorFromGoogle(result);
            return;
        }

        Log.i(TAG, "onConsumeFinished: consume Success");
        reportSuccess(mType);
    }

    private void reportSuccess(String type) {
        /**
         * 如果执行成功，则返回回调并进行相应的通知
         */
        if (mPayListener != null) {
            mPayListener.onSuccess();
            RxToolKit.Emit(new WebViewEvent.WebViewPayResultEvent(0, "Success"));
        }
    }

    private void buy(String type) {
        /**
         * 开始处理购买的商品，请求订单号
         */
        if (mIabHelper == null) {
            onErrorFromGoogle(MessageUtils.getString(R.string.pay_google_init_failed));
            return;
        }
        String payload = "";
        mIabHelper.launchPurchaseFlow(mActivity, type, RC_REQUEST, this, payload);
    }

    private void onErrorFromServer(int code, String reason) {
        if (mPayListener != null) {
            mPayListener.onErrorFromServer(code, reason);
        }
    }


    private void onErrorFromGoogle(IabResult reason) {
        if (mPayListener != null) {
            mPayListener.onErrorFromGoogle(GooglePayUtils.getHintMessage(reason));
        }
    }

    private void onErrorFromGoogle(String reason) {
        if (mPayListener != null) {
            RxToolKit.Emit(new WebViewEvent.WebViewPayResultEvent(-1, reason));
            mPayListener.onErrorFromGoogle(reason);
        }
    }

    @Override
    public void repay() {
        retryCount = 0;
        if (mSetUpSuccess) {
            //如果重新支付的时候，初始化已经成功则从下一步开始
            if (mIabHelper != null) {
                mIabHelper.queryInventoryAsync(this);
            }
        } else {
            pay();
        }
    }

    @Override
    public void pay() {
        /**
         * 初始化Google支付必要参数
         */
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken())) {
            /**
             * Token为空那么直接报错
             */
            onErrorFromGoogle(new IabResult(IabHelper.IABHELPER_BAD_RESPONSE, ""));
            return;
        }
        if (mPayListener != null) {
            mPayListener.onStart();
        }
        mIabHelper.startSetup(this);
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mIabHelper == null) {
            return false;
        }
        return mIabHelper.handleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void release() {
        if (mIabHelper != null) {
            mIabHelper.dispose();
            mIabHelper = null;
        }
        if (mSafeHandler != null) {
            mSafeHandler.release();
            mSafeHandler = null;
        }
        if (mDaoHelper != null) {
            mDaoHelper.release();
            mDaoHelper = null;
        }
        mActivity = null;
        RxToolKit.Emit(new ActivityEvent.StartPayTaskEvent());
    }

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        return true;
    }
}
