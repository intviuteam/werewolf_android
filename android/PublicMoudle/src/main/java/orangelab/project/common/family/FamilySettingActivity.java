package orangelab.project.common.family;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.androidtoolkit.RxToolKit;
import com.networktoolkit.transport.Constant;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;

import orangelab.project.MainApplication;

import com.R;

import orangelab.project.common.activity.LockAbleActivity;
import orangelab.project.common.config.GlobalConfig;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.event.FamilyEvent.QuitFamilyEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.family.model.FamilyLightInfo;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.ActionSheetDialog;
import orangelab.project.common.view.CornerShapeLayout;
import orangelab.project.common.view.TitleView;

import com.qiniu.android.storage.UploadOptions;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.tbruyelle.rxpermissions.RxPermissions;

import com.androidtoolkit.view.ScreenUtils;

import orangelab.thirdparty.qiniu.QiNiuManager;
import rx.android.schedulers.AndroidSchedulers;

/**
 * game_werewolf
 * 2017/10/18 下午12:02
 * Mystery
 */

public class FamilySettingActivity extends LockAbleActivity implements OnClickListener {

    private static final int IMAGE_PICKER_REQUEST = 61110;
    private static final int CAMERA_PICKER_REQUEST = 61111;
    private static final int CANCEL_PICKER_REQUEST = 69;//取消选取照片

    private static final String TAG = "FamilySettingActivity";

    private TitleView mTitleView;

    private Switch mBlockMsg = null;
    private boolean isOpenBlockMsg = false;

    private View mEditContainer = null;
    private ImageView mEditSwitch = null;
    private View mEditInnerContainer = null;
    private boolean isOpenEdit = false;
    private boolean isOpenEditing = false;

    private View mImageContainer = null;
    private ImageView mRealImage = null;
    private View mRealImagePlaceHolder = null;

    private EditText mFamilyName;
    private EditText mFamilyShortName;
    private EditText mFamilyIntroduce;
    private CornerShapeLayout mFamilyImageContainer;
    private ProgressBar mUpLoading;

    private String mFamilyNameString = Constant.EMPTY;
    private String mFamilyShortNameString = Constant.EMPTY;
    private String mFamilyIntroduceString = Constant.EMPTY;
    private String mFamilyImageString = Constant.EMPTY;
    private String mFamilyImageIdString = Constant.EMPTY;
    private String mSelectFilePathString = Constant.EMPTY;

    private Button mExit;
    private Button mSave;

    private ActionSheetDialog mImagePickerDialog;
    private WritableMap mImageConfig;
    private Animator mCurAnimator;
    private RxPermissions mRxPermissions;

    private String mFamilyId = Constant.EMPTY;
    private boolean mUploadCancel = false;
    private boolean isUploading = false;

    private FamilyLightInfo mFamilyInfo = null;

    public static void Launch(Context context, String familyId, String familyName, String familyShortName, String familyDesc, String familyImage) {
        if (TextUtils.isEmpty(familyId)) {
            return;
        }
        FamilyLightInfo i = new FamilyLightInfo();
        i.familyName = familyName;
        i.familyDesc = familyDesc;
        i.familyShortName = familyShortName;
        i.familyImage = familyImage;
        Launch(context, familyId, i);
    }


    public static void Launch(Context context, String familyId, FamilyLightInfo info) {
        if (TextUtils.isEmpty(familyId)) {
            return;
        }
        Intent intent = new Intent(context, FamilySettingActivity.class);
        intent.putExtra(FamilyConstant.FAMILY_ID, familyId);
        intent.putExtra(FamilyConstant.FAMILY_LIGHT_INFO, info);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        setContentView(R.layout.layout_activity_family_setting);
        mRxPermissions = new RxPermissions(this);
        initData();
        initView();
        initListener();
        executeActivityDrawFinish(() -> {
            initLoading(ScreenUtils.dip2px(45), 0, 0, 0);
        });
    }

    private void initListener() {
        RxToolKit.Build(mActivityKey, QuitFamilyEvent.class).action(v -> finish()).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.TransferEvent.class).action(v -> finish()).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.QuitFamilyEvent.class).action(v -> finish()).register();
    }

    private void initData() {
        mFamilyId = getIntent().getStringExtra(FamilyConstant.FAMILY_ID);
        mFamilyInfo = getIntent().getParcelableExtra(FamilyConstant.FAMILY_LIGHT_INFO);
        mFamilyNameString = mFamilyInfo.familyName;
        mFamilyShortNameString = mFamilyInfo.familyShortName;
        mFamilyIntroduceString = mFamilyInfo.familyDesc;
        mFamilyImageString = mFamilyInfo.familyImage;
    }

    private void initView() {
        /**
         * 头部相关
         */
        mTitleView = (TitleView) findViewById(R.id.id_family_setting_title);
        mTitleView.setTitle(R.string.string_family_setting_title);
        mTitleView.setActionBack(v -> {
            finish();
        });

        /**
         * 消息免打扰
         */
        mBlockMsg = (Switch) findViewById(R.id.id_family_shield_msg_switch);
        mBlockMsg.setOnCheckedChangeListener((buttonView, isChecked) -> {
            GlobalConfig.UpdateGroupNoticeDisable(this, isChecked);
        });
        mBlockMsg.setChecked(GlobalConfig.GetGroupNoticeDisable(this));

        /**
         * 编辑相关
         */
        mEditContainer = findViewById(R.id.id_family_edit_container);
        mEditInnerContainer = findViewById(R.id.id_family_detail_container);
        mEditSwitch = (ImageView) findViewById(R.id.id_family_edit_switch);
        mRealImage = (ImageView) findViewById(R.id.id_family_image);
        mEditInnerContainer.setVisibility(View.GONE);
        mEditSwitch.setOnClickListener(this);

        mFamilyName = (EditText) findViewById(R.id.id_family_name_edittext);
        mFamilyShortName = (EditText) findViewById(R.id.id_family_short_name_edittext);
        mFamilyIntroduce = (EditText) findViewById(R.id.id_family_introduce_edittext);
        mFamilyImageContainer = (CornerShapeLayout) findViewById(R.id.id_family_upload_image);
        float radio = ScreenUtils.dip2px(6f);
        mFamilyImageContainer.setBottomLeftRadius(radio);
        mFamilyImageContainer.setTopLeftRadius(radio);
        mFamilyImageContainer.setBottomRightRadius(radio);
        mFamilyImageContainer.setTopRightRadius(radio);
        mFamilyImageContainer.postInvalidate();
        mUpLoading = (ProgressBar) findViewById(R.id.id_family_image_loading);
        mFamilyImageContainer.setOnClickListener(this);
        mRealImagePlaceHolder = findViewById(R.id.id_family_add);
        /**
         * 按钮相关
         */
        mSave = (Button) findViewById(R.id.id_family_btn_save);
        mSave.setOnClickListener(this);

        mExit = (Button) findViewById(R.id.id_family_btn_exit);

        if (FamilyMemo.iMemo.isOwner()) {
            mEditContainer.setVisibility(View.VISIBLE);
            changeEditSwitch();
            /**
             * 这个人是族长
             */
            mExit.setText(MessageUtils.getString(R.string.string_family_transfer));
            mExit.setOnClickListener(v -> {
                addLockViewRect(mExit);
                FamilyMembersActivity.LaunchForTransferBoss(this, mFamilyId);
                removeLockViewRect(mExit);
            });
        } else {
            /**
             * 这个人不是族长
             */
            mEditContainer.setVisibility(View.GONE);
            mExit.setOnClickListener(v -> {
                addLockViewRect(mExit);
                FamilyApiManager.QuitFamily((integer, e) -> {
                    runOnUiThreadSafely(() -> {
                        if (e != null) {
                            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
                        } else {
                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_quit_family_success)).run();
                            FamilyMemo.ClearFamilyMemo();
                            GlobalConfig.UpdateGroupNoticeDisable(this, false);
                            UnifiedBridgeHelper.SendQuitFamilyToRn();
                            RxToolKit.Emit(new QuitFamilyEvent());
                        }
                        removeLockViewRect(mExit);
                        return;
                    });
                });
            });
        }
        refreshEditTextMsg();
    }

    private void refreshEditTextMsg() {
        try {
            if (mFamilyInfo != null) {
                mFamilyName.setText(mFamilyNameString);
                mFamilyShortName.setText(mFamilyShortNameString);
                mFamilyIntroduce.setText(mFamilyIntroduceString);
                if (TextUtils.isEmpty(mFamilyImageString)) {
                    mRealImagePlaceHolder.setVisibility(View.VISIBLE);
                } else {
                    mRealImagePlaceHolder.setVisibility(View.GONE);
                    PicassoUtils.loadImage(this, mFamilyImageString, mRealImage);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void release() {
        dismissImagePickerDialog();
        if (mCurAnimator != null && mCurAnimator.isRunning()) {
            mCurAnimator.cancel();
            mCurAnimator = null;
        }
        try {
            MainApplication.getInstance().getPickerModule().clearResultCollector();
        } catch (Exception e) {
            e.printStackTrace();
            //getPickerModule may be null
        }
        mRxPermissions = null;
    }

    @Override
    public void finish() {
        super.finish();
        RxToolKit.UnRegister(mActivityKey);
        release();
        if (isUploading) {
            isUploading = false;
            mUploadCancel = true;
        }
    }

    private void showImagePickerDialog() {
        if (mImagePickerDialog == null) {
            mImagePickerDialog = new ActionSheetDialog(this).builder().setCancelable(true).setCanceledOnTouchOutside(true)
                    .addSheetItem(MessageUtils.getString(R.string.string_image_pick_from_camera), ActionSheetDialog.SheetItemColor.Blue, which -> {
                        if (mRxPermissions != null)
                            mRxPermissions.request(Manifest.permission.CAMERA)
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(aBoolean -> {
                                        if (aBoolean) {
                                            openCameraPicker();
                                        } else {
                                            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_permission_camera_error)));
                                        }
                                    });
                    })
                    .addSheetItem(MessageUtils.getString(R.string.string_image_pick_from_phone), ActionSheetDialog.SheetItemColor.Blue, which -> {
                        if (mRxPermissions != null)
                            mRxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE)
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(aBoolean -> {
                                        if (aBoolean) {
                                            openImagePicker();
                                        } else {
                                            runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.permission_read_store_error)));
                                        }
                                    });

                    });
        }
        if (mImagePickerDialog.isShowing()) {
            return;
        }
        mImagePickerDialog.show();
    }

    private void dismissImagePickerDialog() {
        if (mImagePickerDialog != null) {
            mImagePickerDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_family_upload_image) {
            showImagePickerDialog();
        } else if (id == R.id.id_family_edit_switch) {
            changeEditSwitch();
        } else if (id == R.id.id_family_btn_save) {
            handleSave();
        }
//        switch (v.getId()) {
//            case R.id.id_family_upload_image:
//                showImagePickerDialog();
//                break;
//            case R.id.id_family_edit_switch:
//                changeEditSwitch();
//                break;
//            case R.id.id_family_btn_save: {
//                handleSave();
//            }
//            break;
//        }
    }

    private void requestRefreshInfo() {
        RxToolKit.Emit(new FamilyEvent.RefreshFamilyInfoEvent());
    }

    private void saveEditInfo() {
        FamilyApiManager.EditFamilyDetail(mFamilyNameString, mFamilyShortNameString, mFamilyIntroduceString, mFamilyImageString, mFamilyImageIdString, (familyInfoApiResult, error) ->
                runOnUiThreadSafely(() -> {
                    if (error == null) {
//                        changeEditSwitch();
                        RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_action_success)).run();
                        requestRefreshInfo();
                    } else {
                        RunnableFactory.createMsgToastRunnable(error.getMessage()).run();
                    }
                    dismissBlockLoading();
                }));
    }

    private void handleSave() {
        String tempFamilyName = mFamilyName.getText().toString();
        Log.i(TAG, "handleSave: tempFamilyName bytes()" + tempFamilyName.getBytes().length);
        if (!FamilyTools.FamilyNameFilter(tempFamilyName)) {
            removeLockViewRect(mSave);
            return;
        }
        mFamilyNameString = tempFamilyName;

        String tempFamilyShortName = mFamilyShortName.getText().toString();
        if (!FamilyTools.FamilyShortNameFilter(tempFamilyShortName)) {
            removeLockViewRect(mSave);
            return;
        }
        mFamilyShortNameString = tempFamilyShortName;

        String tempFamilyDes = mFamilyIntroduce.getText().toString();
        if (!FamilyTools.FamilyDescFilter(tempFamilyDes)) {
            removeLockViewRect(mSave);
            return;
        }
        mFamilyIntroduceString = tempFamilyDes;

        /**
         * 以上判断均正确，进行保存操作,先上传图片
         */
        showBlockLoading();
        ApiManager.RefreshUpLoadToken((upLoadTokenResult, e) -> {
            runOnUiThreadSafely(() -> {
                if (e == null) {
                    if (TextUtils.isEmpty(upLoadTokenResult.token)) {
                        RunnableFactory.createNetworkErrorRunnable().run();
                        dismissBlockLoading();
                    } else {
                        if (TextUtils.isEmpty(mSelectFilePathString)) {
                            saveEditInfo();
                        } else {
                            uploadImage(mSelectFilePathString, upLoadTokenResult.token, () -> {
                                saveEditInfo();
                            }, () -> {
                                runOnUiThreadSafely(() -> {
                                    dismissBlockLoading();
                                });
                            });
                        }
                    }
                } else {
                    RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    dismissBlockLoading();
                }
            });
        });
    }

    private void changeEditSwitch() {
        if (isOpenEditing)
            return;
        isOpenEditing = true;
        float left = 0f;
        float right = 0f;
        if (isOpenEdit) {
            /**
             * 如果当前状态是打开，那么变为关闭
             */
            left = 90f;
            right = -0f;
        } else {
            /**
             * 如果当前状态是关闭，那么变为打开
             */
            left = 0f;
            right = 90f;
        }
        mCurAnimator = ObjectAnimator.ofFloat(mEditSwitch, View.ROTATION, left, right);
        mCurAnimator.setDuration(300);
        mCurAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (isOpenEdit) {
                    /**
                     * 如果当前是打开
                     */
                    mEditInnerContainer.setVisibility(View.GONE);
                } else {
                    /**
                     * 如果当前是关闭
                     */
                    mEditInnerContainer.setVisibility(View.VISIBLE);
                }
                isOpenEdit = !isOpenEdit;
                isOpenEditing = false;
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        mCurAnimator.start();
    }

    private void openImagePicker() {
        try {
            initImageConfig();
            MainApplication.getInstance().getPickerModule().openPickerWithActivity(mImageConfig, new Promise() {
                @Override
                public void resolve(@javax.annotation.Nullable Object value) {
                    handleResolve(value);
                }

                @Override
                public void reject(String code, String message) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(String code, Throwable e) {

                }

                @Override
                public void reject(String code, String message, Throwable e) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(String message) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(Throwable reason) {

                }
            }, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCameraPicker() {
        try {
            initImageConfig();
            MainApplication.getInstance().getPickerModule().openCameraWithActivity(mImageConfig, new Promise() {
                @Override
                public void resolve(@javax.annotation.Nullable Object value) {
                    handleResolve(value);
                }

                @Override
                public void reject(String code, String message) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(String code, Throwable e) {

                }

                @Override
                public void reject(String code, String message, Throwable e) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(String message) {
//                    runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(message));
                }

                @Override
                public void reject(Throwable reason) {

                }
            }, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initImageConfig() {
        if (mImageConfig == null) {
            mImageConfig = Arguments.createMap();
            mImageConfig.putInt("width", 150);
            mImageConfig.putInt("height", 150);
            mImageConfig.putBoolean("cropping", true);
            mImageConfig.putString("cropperTintColor", "#9c4db7");
            mImageConfig.putBoolean("multiple", false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PICKER_REQUEST || requestCode == IMAGE_PICKER_REQUEST || requestCode == CANCEL_PICKER_REQUEST) {
            if (MainApplication.getInstance().getPickerModule() != null) {
                MainApplication.getInstance().getPickerModule().onActivityResult(this, requestCode, resultCode, data);
            } else {
                PLog.i(TAG, "onActivityResult: getPickerModule==null");
            }
        }
    }

    public void handleResolve(@javax.annotation.Nullable Object value) {
        runOnUiThreadSafely(() -> {
            try {
                ReadableMap map = (ReadableMap) value;
                String path = map.getString("path");

                if (!FamilyTools.FamilyImageFilter(path)) {
                    /**
                     * 选择了一个无效的图片
                     */
                    return;
                }
                Log.i(TAG, "resolve: path:" + path);

                PicassoUtils.loadImage(FamilySettingActivity.this, path, mRealImage, 150, 150);
                /**
                 * 将图片加载🔪ImageView 中
                 */

                try {
                    /**
                     * 进行一个替换字符串的操作
                     */
                    if (path.contains("file://")) {
                        path = path.replace("file://", "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mSelectFilePathString = path;
                mRealImagePlaceHolder.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_image_filter_error)));
            }
        });
    }

    private void startLoading() {
        mUpLoading.setVisibility(View.VISIBLE);
        isUploading = true;
    }

    private void endLoading() {
        mUpLoading.setVisibility(View.GONE);
        isUploading = false;
    }

    private void uploadImage(String path, String token, Runnable success, Runnable failed) {
        QiNiuManager.UpLoadImage(path, null, token, (key, info, response) -> {
                    runOnUiThreadSafely(() -> {
                        if (info.isOK()) {
                            String urlString = response.optString("url");
                            String keyString = response.optString("key");
                            if (TextUtils.isEmpty(urlString)) {
                                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_image_upload_error)).run();
                                return;
                            }
                            if (TextUtils.isEmpty(keyString)) {
                                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_image_upload_error)).run();
                                return;
                            }
                            mFamilyImageString = urlString;
                            mFamilyImageIdString = keyString;
                            if (success != null) {
                                success.run();
                            }
                        } else {
                            try {
                                ReportEventUtils.reportException(response.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_image_upload_error)).run();
                            if (failed != null) {
                                failed.run();
                            }
                        }
                        PLog.i(TAG, "response=" + response);
                    });
                },
                new UploadOptions(null, null, false, (key, percent) -> {

                }, () -> mUploadCancel));
    }
}
