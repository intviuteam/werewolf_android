package orangelab.project.common.event;

/**
 * game_werewolf
 * 2017/6/19 上午10:06
 * Mystery
 */

public class ServerEvent {

    public static final int DISCONNECT_FROM_AUDIO_SERVER = 0;
    public static final int RECONNECT_FROM_AUDIO_SERVER = 1;

    public static class AudioServerEvent {

        private int action;

        public AudioServerEvent(int action) {
            this.action = action;
        }

        public int getAction() {
            return action;
        }
    }

}
