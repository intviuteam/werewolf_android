package orangelab.project.common.model;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/7/3 下午5:20
 * Mystery
 */

public class GiftListResult {

    public ArrayList<GiftListItem> prices;

    public static class GiftListItem {
        public String type;
        public ArrayList<GiftItemPrice> price;
        public int popular;
    }

    public static class GiftItemPrice {
        public String type;
        public int count;
    }
}
