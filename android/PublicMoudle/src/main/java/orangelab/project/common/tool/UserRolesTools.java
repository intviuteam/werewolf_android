package orangelab.project.common.tool;

import com.R;

/**
 * game_werewolf
 * 2017/10/26 上午10:34
 * Mystery
 */

public class UserRolesTools {

    public static int getVipUserRolesBackGround() {
        return R.drawable.drawable_shape_vip_tv;
    }

    public static int getUserRolesBackGround(int index, int size) {
        int part = index % 3;
        int bg = R.drawable.drawable_shape_ff9ec8_tv;
        switch (part) {
            case 0:
                bg = R.drawable.drawable_shape_ff9ec8_tv;
                break;
            case 1:
                bg = R.drawable.drawable_shape_b6e4fe_tv;
                break;
            case 2:
                bg = R.drawable.drawable_shape_ffed00_tv;
                break;
        }
        return bg;
    }


    public static int getUserRolesForUserDetailBackGround(int index, int size) {
        int part = index % 3;
        int bg = R.drawable.drawable_werewolf_user_detail_46eaff;
        switch (part) {
            case 0:
                bg = R.drawable.drawable_werewolf_user_detail_46eaff;
                break;
            case 1:
                bg = R.drawable.drawable_werewolf_user_detail_fdff03;
                break;
            case 2:
                bg = R.drawable.drawable_werewolf_user_detail_ff6dff;
                break;
        }
        return bg;
    }

}
