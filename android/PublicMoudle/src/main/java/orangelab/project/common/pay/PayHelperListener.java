package orangelab.project.common.pay;

/**
 * game_werewolf
 * 2017/7/21 下午3:27
 * Mystery
 */

public interface PayHelperListener {

    void onErrorFromGoogle(String reason);//支付失败原因 此失败原因为因Google而起

    void onErrorFromServer(int code, String reason);

    void onDestroy(); //销毁

    void onSuccess();

    void onStart();
}
