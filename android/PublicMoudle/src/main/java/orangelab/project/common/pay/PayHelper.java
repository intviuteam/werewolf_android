package orangelab.project.common.pay;

import android.content.Intent;

import orangelab.project.common.pay.google.GooglePayUtils;

/**
 * game_werewolf
 * 2017/7/21 下午3:29
 * Mystery
 */

public abstract class PayHelper {

    protected String mType;
    protected PayHelperListener mPayListener;

    public PayHelper(String mType, PayHelperListener payHelperListener) {
        this.mType = mType;
        this.mPayListener = payHelperListener;
    }

    public abstract void repay();

    public abstract void pay(); //开始支付 type 为商品类型

    public abstract boolean onActivityResult(int requestCode, int resultCode, Intent data);

    public abstract void release();

    protected void googleError(int errorCode) {
        if (mPayListener != null) {
            mPayListener.onErrorFromGoogle(GooglePayUtils.googleBillingResponseCodeTranslate(errorCode));
        }
    }


    protected void serverError(int errorCode, String reason) {
        if (mPayListener != null) {
            mPayListener.onErrorFromServer(errorCode, reason);
        }
    }

}
