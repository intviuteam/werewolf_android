package orangelab.project.common.db;

/**
 * game_werewolf
 * 2017/8/11 上午11:38
 * Mystery
 */

public class DBConstant {

    public static final String DB_NAME = "werewolf_db";

    public static final int PAY_SUCCESS_BUT_NOT_REPORT_SERVER_INAPP = -1;
    public static final int PAY_SUCCESS_AND_REPORT_SERVER = 0;
    public static final int PAY_SUCCESS_BUT_NOT_REPORT_SERVER_SUBS = -2;

    public static final String MINI_GAME_RECORD_HELPER = "MiniGameRecord";
    public static final String MINI_GAME_AI_RECORD_HELPER = "MiniGameAiRecord";
    public static final String PAY_ORDER_RECORD_HELPER = "PayOrderRecord";
    public static final String MINI_GAME_LAUNCH_HELPER = "MiniGameLaunchRecord";
    public static final String PAY_SUBS_OERDER_ASSIT_HELPER = "PaySubsOrderAssitRecord";
}
