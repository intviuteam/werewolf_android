package orangelab.project.common.pay.google.v2;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.android.billingclient.api.Purchase;
import com.androidtoolkit.ThreadToolKit;
import com.datasource.GlobalUserState;
import com.google.gson.JsonSyntaxException;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import orangelab.project.MainApplication;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.google.GooglePayUtils;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import rx.Observable;
import rx.Subscriber;

/**
 * game_werewolf
 * 2018/6/20 下午2:24
 * Mystery
 */
public class GooglePayInAppV2Task implements BillingManager.BillingManagerObserver, Destroyable {

    private static final String TAG = "GooglePayV2Task";
    private BillingManager mBillingManager;
    private UserPayOrderDaoV2Helper mDaoV2Helper;
    private SafeHandler mSafeHandler = new SafeHandler();

    public GooglePayInAppV2Task(Activity activity) {
        mBillingManager = new BillingManager(activity, this);
        mDaoV2Helper = MainApplication.getInstance().getUserPayOrderDaoV2Helper();
    }

    public void startTask() {
        Log.i(TAG, "startTask: ");
        mBillingManager.queryPurchasesInApp();
    }

    @Override
    public void onSetupFinished() {

    }

    @Override
    public void onSetupError(int errorCode) {
        executeGooglePayV2OrderTask();
    }

    @Override
    public void onPurchasesQuery(List<Purchase> purchases) {
        /**
         * 获取查询的purchases
         */
        Log.i(TAG, "onPurchasesQuery: size=" + purchases.size());
        if (Utils.targetListIsNull(purchases)) {
            executeGooglePayV2OrderTask();
        } else {
            purchasesCheck(purchases);
        }
    }

    @Override
    public void onPurchasesQueryError(int errorCode) {
        executeGooglePayV2OrderTask();
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {

    }

    @Override
    public void onPurchaseFlowError(int errorCode) {

    }

    @Override
    public void onPurchasesUpdatedError(int errorCode) {

    }

    @Override
    public void onConsumeSuccess(String token) {

    }

    @Override
    public void onConsumeError(int errorCode) {

    }

    private void executeGooglePayV2OrderTask() {
        ThreadToolKit.Async(new GooglePayV2OrderTask());
    }

    private void purchasesCheck(List<Purchase> purchases) {
        ThreadToolKit.Async(() -> {
            Observable.from(purchases)
                    .onBackpressureBuffer()
                    .subscribe(new Subscriber<Purchase>() {

                        @Override
                        public void onStart() {
                            Log.i(TAG, "purchasesCheck onStart: ");
                        }

                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "purchasesCheck onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Purchase purchase) {
                            /**
                             * 处理流程
                             * 1。现象服务器汇报
                             * 2。消费这个商品
                             */
                            final String orderId = purchase.getOrderId();
                            Log.i(TAG, "onNext: begin report [" + orderId + "]");
                            if (mDaoV2Helper != null) {
                                UserPayOrderEntity entity = mDaoV2Helper.getUserPayOrderByOrderId(orderId);
                                if (entity != null) {
                                    /**
                                     * 数据库中有这个订单，消费这个数据
                                     */
                                    reportToServer(
                                            entity.getUserId(),
                                            entity.getOtherData2(),
                                            orderId,
                                            purchase.getSignature(),
                                            purchase.getOriginalJson(),
                                            () -> mSafeHandler.postSafely(() -> {
                                                Log.i(TAG, "onNext: begin consume[" + orderId + "]");
                                                mBillingManager.consumeAsync(purchase.getPurchaseToken(), (responseCode, purchaseToken) -> {
                                                    /**
                                                     * 这里无论是否消费成功，都进行下一个数据传递
                                                     */
                                                    Log.i(TAG, "onNext consume finish[" + orderId + "]" + responseCode);
                                                    request(1);
                                                });
                                            }),
                                            () -> {
                                                Log.i(TAG, "onNext: report error[" + orderId + "]");
                                                request(1);
                                            }
                                    );
                                } else {
                                    /**
                                     * 数据库中没有这个订单，判断当前用户是否存在,也不能是游客
                                     */
                                    if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken()) ||
                                            TextUtils.isEmpty(GlobalUserState.getGlobalState().getUserId()) ||
                                            Utils.checkUserIdIsTourist(GlobalUserState.getGlobalState().getUserId())) {
                                        request(1);
                                    } else {
                                        /**
                                         * 汇报数据
                                         */
                                        reportToServer(
                                                GlobalUserState.getGlobalState().getUserId(),
                                                GlobalUserState.getGlobalState().getToken(),
                                                purchase.getOrderId(),
                                                purchase.getSignature(),
                                                purchase.getOriginalJson(),
                                                () -> mSafeHandler.postSafely(() -> {
                                                    mBillingManager.consumeAsync(purchase.getPurchaseToken(), (responseCode, purchaseToken) -> {
                                                        /**
                                                         * 这里无论是否消费成功，都进行下一个数据传递
                                                         */
                                                        Log.i(TAG, "onNext consume " + responseCode);
                                                        request(1);
                                                    });
                                                }),
                                                () -> {
                                                    Log.i(TAG, "onNext: report Error ");
                                                    request(1);
                                                }
                                        );
                                    }
                                }
                                Log.i(TAG, "onNext: end report [" + orderId + "]");
                            }

                        }
                    });
            /**
             * 汇报完毕后，进行数据库的汇报操作
             */
            executeGooglePayV2OrderTask();
        });
    }

    @Override
    public void destroy() {
        mBillingManager.destroy();
    }

    private void reportToServer(String userId, String token, String orderId, String signature, String data, Runnable success, Runnable failed) {
        Log.i(TAG, "reportToServer: " + userId + "," + orderId + "," + token);
        JSONObject json = new JSONObject();
        try {
            json.put(Constant.SIGNATURE, signature);
            json.put(Constant.DATA, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).withToken(token).request(Constant.GOOGLE_PAY).load(json)
                .action(new RequestTaskCallBack() {

                    private void retry(int code, String msg) {
                        // TODO: 2018/6/20 这里需要汇报服务器
                        if (failed != null) {
                            failed.run();
                        }
                    }

                    @Override
                    public void onSuccess(String response) {
                        try {
                            PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                            if (result.purchase.size() == 0) {
                                ReportRobot.robot.report("SERVER_RETURN_DATA_ERROR_SIZE_0");
                                retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                            } else {
                                PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                switch (item.getOp_result()) {
                                    case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                        Utils.runSafely(() -> {
                                            if (mDaoV2Helper != null) {
                                                mDaoV2Helper.deleteData(userId, orderId);
                                            }
                                        });
                                        ReportRobot.robot.report("GOOGLE_PAY_SERVER_SUCCESS");
                                        Log.i(TAG, "reportToServer onSuccess:");
                                        if (success != null) {
                                            success.run();
                                        }
                                    }
                                    break;
                                    case GooglePayUtils.SERVER_PAY_DOING://处理中
                                    case GooglePayUtils.SERVER_PAY_UPDATE_FAIL://更新失败
                                    case GooglePayUtils.SERVER_PAY_SERVER_ERROR://服务器处理错误
                                    case GooglePayUtils.SERVER_PAY_FAIL:// 支付失败
                                    case GooglePayUtils.SERVER_PAY_UNKNOWN_PRODUCT: //未知产品
                                    case GooglePayUtils.SERVER_PAY_VERIFY_FAIL: //校验失败
                                        Log.i(TAG, "reportToServer onError:");
                                        retry(item.getOp_result(), GooglePayUtils.getServerHint(item.getOp_result()));
                                        break;
                                    default:
                                        Log.i(TAG, "reportToServer onError:");
                                        retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                                        break;
                                }
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                            retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                        }
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(code, failed);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(-1, MessageUtils.getString(R.string.network_is_unavailable));
                        });
                    }
                }).build().executeInCurThread();
    }
}
