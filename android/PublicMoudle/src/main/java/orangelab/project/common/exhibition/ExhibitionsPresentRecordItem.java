package orangelab.project.common.exhibition;

import com.R;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import cn.intviu.support.TimeUtils;

/**
 * game_werewolf
 * 2017/7/3 下午4:29
 * Mystery
 */

public class ExhibitionsPresentRecordItem {

    private String mFromName;
    private String mFromImageUrl;
    private String mGiftType;
    private String mFromId;
    private long time;

    private String mExplain;

    public ExhibitionsPresentRecordItem(String fromName, String fromImageUrl, String giftType, long time) {
        mFromName = fromName;
        mFromImageUrl = fromImageUrl;
        mGiftType = giftType;
        this.time = time;
        mExplain = MessageUtils.getString(
                R.string.gifts_exhibitions_present_record_explain,
                fromName
        );
    }

    public String getFromId() {
        return mFromId;
    }

    public void setFromId(String fromId) {
        mFromId = fromId;
    }

    public String getFromImageUrl() {
        return mFromImageUrl;
    }

    public String getTime() {
        return Utils.formatTime(time);
    }

    public String getExplain() {
        return mExplain;
    }

    public String getGiftType() {
        return mGiftType;
    }
}
