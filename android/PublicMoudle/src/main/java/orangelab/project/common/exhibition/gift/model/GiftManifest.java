package orangelab.project.common.exhibition.gift.model;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2017/11/9 上午10:56
 * Mystery
 */

public class GiftManifest implements Keepable {
    public int rev;
    public List<GiftManifestItem> gifts;

    public GiftManifestItem find(String type) {
        if (gifts != null) {
            for (GiftManifestItem item : gifts) {
                if (TextUtils.equals(type, item.gift_type)) {
                    return item;
                }
            }
        }
        return null;
    }
}
