package orangelab.project.common.engine;

/**
 * game_werewolf
 * 2018/3/22 下午1:13
 * Mystery
 */

public interface IOrbitEngineObserver {

    void onEngineDisConnect();

    void onEngineConnect();
}
