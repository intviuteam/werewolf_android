package orangelab.project.common.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Author:1727
 * 2017/3/23
 */

public class StartResult {

    public int duration;
    public HashMap<String, Integer> role_map;
    public ArrayList<String> optional;
}
