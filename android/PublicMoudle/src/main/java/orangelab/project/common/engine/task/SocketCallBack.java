package orangelab.project.common.engine.task;

/**
 * Author:1727
 * 2017/3/29
 */

public interface SocketCallBack {
    public void invoke(String result);
}
