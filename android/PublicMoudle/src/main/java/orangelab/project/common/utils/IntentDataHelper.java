package orangelab.project.common.utils;

import android.content.Intent;

import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.EnterRoomResult;

import java.util.ArrayList;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * Author:1727
 * 2017/3/6
 */

public class IntentDataHelper {

    private static final String ENTER_ROOM_RESULT = "ENTER_ROOM_RESULT";
    private static final String USER_NAME = "USER_NAME";
    private static final String USER_ID = "USER_ID";
    private static final String MEDIA_TYPE = "media_type";
    private static final String MEDIA_DUR_TIME = "MEDIA_DUR_TIME";
    private static final String USER_AVATER = "USER_AVATER";
    private static final String USER_TOKEN = "USER_TOKEN";
    private static final String GAME_TYPE = "GAME_TYPE";
    private static final String PASSWORD = "password";
    private static final String UPGRADE_TYPE = "upgrade_type";
    private static final String DOWNLOAD_APK_URL = "download_apk_url";
    private static final String DOWNLOAD_TOKEN = "download_token";
    private static final String CHAT_LEFT_USER = "CHAT_LEFT_USER";
    private static final String CHAT_RIGHT_USER = "CHAT_RIGHT_USER";
    private static final String USER_SEX = "USER_SEX";
    private static final String IS_PLAYING = "IS_PLAYING";
    private static final String FROM_LEADER_BOARD = "FROM_LEADER_BOARD";
    private static final String USER_EXP = "USER_EXP";
    public static final String INTENT_TYPE = "INTENT_TYPE";
    public static final String TYPE_ENTER_ROOM_FROM_RN = "TYPE_ENTER_ROOM_FROM_RN";
    public static final String TYPE_NOTIFACTION_FROM_INTVIU = "TYPE_NOTIFACTION_FROM_INTVIU";
    public static final String TYPE_EVENT_ROOM_FROM_CHAT = "TYPE_EVENT_ROOM_FROM_CHAT";
    public static final String TYPE_EVENT_AMUSEMENT_ACTIVITY = "TYPE_EVENT_AMUSEMENT_ACTIVITY";
    private static final String ENTER_ROOM_PAYLOAD = "ENTER_ROOM_PAYLOAD";
    private static final String ROOM_ID = "ROOM_ID";
    private static final String INITIATIVE_CHECK = "initiative_check";
    private static final String IMAGE_LIST = "IMAGE_LIST";
    private static final String IMAGE_CUR_POSITION = "IMAGE_CUR_POSITION";
    private static final String PRODUCE_ID = "product_id";

    private static final String VOICE_ROOM_TITLE = "voice_room_title";
    private static final String VOICE_FORCE_UP_SEAT = "voice_force_up_seat";
    private static final String VOICE_CHAIR_POSITION = "voice_new_chair_position";
    private static final String VOICE_ONLINE_USERS = "voice_online_users";

    private static final String URL_INTENT = "URL_INTENT";
    private static final String IS_COMEIN_BY_SW = "is_commein_by_sw";//是否是从小窗进入
    private static final String TYPE = "TYPE";

    public static void setType(Intent intent, String data) {
        intent.putExtra(TYPE, data);
    }

    public static String getType(Intent intent) {
        return intent.getStringExtra(TYPE);
    }

    private static final String SOCKET_ENGINE_TOKEN = "SOCKET_ENGINE_TOKEN"; //Socket Engine Token

    public static void setSocketEngineToken(Intent intent, String token) {
        intent.putExtra(SOCKET_ENGINE_TOKEN, token);
    }

    public static String getSocketEngineToken(Intent intent) {
        return intent.getStringExtra(SOCKET_ENGINE_TOKEN);
    }

    public static void setUrlIntent(Intent intent, String intentString) {
        intent.putExtra(URL_INTENT, intentString);
    }

    public static String getUrlIntent(Intent intent) {
        return intent.getStringExtra(URL_INTENT);
    }

    public static void setVoiceRoomTitle(Intent intent, String voiceRoomTitle) {
        intent.putExtra(VOICE_ROOM_TITLE, voiceRoomTitle);
    }

    public static String getVoiceRoomTitle(Intent intent) {
        return intent.getStringExtra(VOICE_ROOM_TITLE);
    }

    public static void setVoiceForceUpSeat(Intent intent, boolean isForceUpSeat) {
        intent.putExtra(VOICE_FORCE_UP_SEAT, isForceUpSeat);
    }

    public static boolean getVoiceForceUpSeat(Intent intent) {
        return intent.getBooleanExtra(VOICE_FORCE_UP_SEAT, false);
    }

    public static void setVoiceChairPosition(Intent intent, int newPosition) {
        intent.putExtra(VOICE_CHAIR_POSITION, newPosition);
    }

    public static int getVoiceChairPosition(Intent intent) {
        return intent.getIntExtra(VOICE_CHAIR_POSITION, -1);
    }

    public static void setProductId(Intent intent, String productId) {
        intent.putExtra(PRODUCE_ID, productId);
    }

    public static String getProductId(Intent intent) {
        return intent.getStringExtra(PRODUCE_ID);
    }

    public static void setImageCurPosition(Intent intent, int curPosition) {
        intent.putExtra(IMAGE_CUR_POSITION, curPosition);
    }

    public static int getImageCurPosition(Intent intent) {
        return intent.getIntExtra(IMAGE_CUR_POSITION, 0);
    }

    public static void setImageList(Intent intent, ArrayList<String> imageList) {
        intent.putStringArrayListExtra(IMAGE_LIST, imageList);
    }

    public static ArrayList<String> getImageList(Intent intent) {
        return intent.getStringArrayListExtra(IMAGE_LIST);
    }

    public static void setIntentType(Intent intent, String type) {
        intent.putExtra(INTENT_TYPE, type);
    }

    public static String getIntentType(Intent intent) {
        return intent.getStringExtra(INTENT_TYPE);
    }

    public static void setEnterRoomPayload(Intent intent, EnterRoomPayload payload) {
        intent.putExtra(ENTER_ROOM_PAYLOAD, payload);
    }

    public static EnterRoomPayload getEnterRoomPayload(Intent intent) {
        return intent.getParcelableExtra(ENTER_ROOM_PAYLOAD);
    }

    public static void setUserSex(Intent intent, int sex) {
        intent.putExtra(USER_SEX, sex);
    }

    public static int getUserSex(Intent intent) {
        return intent.getIntExtra(USER_SEX, 2);
    }

    public static void setRoomPassword(Intent intent, String password) {
        intent.putExtra(PASSWORD, password);
    }

    public static String getRoomPassword(Intent intent) {
        return intent.getStringExtra(PASSWORD);
    }

    public static void setGameType(Intent intent, String gameType) {
        intent.putExtra(GAME_TYPE, gameType);
    }

    public static String getGameType(Intent intent) {
        return intent.getStringExtra(GAME_TYPE);
    }

    public static void setUserAvater(Intent intent, String avater) {
        intent.putExtra(USER_AVATER, avater);
    }

    public static String getUserAvater(Intent intent) {
        return intent.getStringExtra(USER_AVATER);
    }

    public static String getUserToken(Intent intent) {
        return intent.getStringExtra(USER_TOKEN);
    }

    public static void setUserToken(Intent intent, String token) {
        intent.putExtra(USER_TOKEN, token);
    }

    public static void setEnterRoomResult(Intent intent, EnterRoomResult result) {
        intent.putExtra(ENTER_ROOM_RESULT, result);
    }

    public static EnterRoomResult getEnterRoomResult(Intent intent) {
        return (EnterRoomResult) intent.getSerializableExtra(ENTER_ROOM_RESULT);
    }

    public static void setUserId(Intent intent, String userid) {
        intent.putExtra(USER_ID, userid);
    }

    public static int getUserExp(Intent intent) {
        return intent.getIntExtra(USER_EXP, 0);
    }

    public static void setUserExp(Intent intent, int value) {
        intent.putExtra(USER_EXP, value);
    }

    public static String getUserID(Intent intent) {
        return intent.getStringExtra(USER_ID);
    }

    public static void setUserName(Intent intent, String userName) {
        intent.putExtra(USER_NAME, userName);
    }

    public static String getUserName(Intent intent) {
        return intent.getStringExtra(USER_NAME);
    }

    public static void setMediaType(Intent intent, String type) {
        intent.putExtra(MEDIA_TYPE, type);
    }

    public static String getMediaType(Intent intent) {
        return intent.getStringExtra(MEDIA_TYPE);
    }

    public static void setMediaDurTime(Intent intent, int durTime) {
        intent.putExtra(MEDIA_DUR_TIME, durTime);
    }

    public static int getMediaDurTime(Intent intent) {
        return intent.getIntExtra(MEDIA_DUR_TIME, 0);
    }

    public static void setUpgradeType(Intent intent, String type) {
        intent.putExtra(UPGRADE_TYPE, type);
    }

    public static String getUpgradeType(Intent intent) {
        return intent.getStringExtra(UPGRADE_TYPE);
    }

    public static void setCheckType(Intent intent, boolean initiative) {
        intent.putExtra(INITIATIVE_CHECK, initiative);
    }

    public static boolean getCheckType(Intent intent) {
        return intent.getBooleanExtra(INITIATIVE_CHECK, false);
    }

    public static void setDownloadApkUrl(Intent intent, String url) {
        intent.putExtra(DOWNLOAD_APK_URL, url);
    }

    public static String getDownloadApkUrl(Intent intent) {
        return intent.getStringExtra(DOWNLOAD_APK_URL);
    }

    public static UserData getChatLeftUserData(Intent intent) {
        return intent.getParcelableExtra(CHAT_LEFT_USER);
    }

    public static void setChatLeftUser(Intent intent, UserData data) {
        intent.putExtra(CHAT_LEFT_USER, data);
    }

    public static void setChatRightUser(Intent intent, UserData data) {
        intent.putExtra(CHAT_RIGHT_USER, data);
    }

    public static UserData getChatRightUserData(Intent intent) {
        return intent.getParcelableExtra(CHAT_RIGHT_USER);
    }

    public static void setIsPlaying(Intent intent, boolean isPlaying) {
        intent.putExtra(IS_PLAYING, isPlaying);
    }

    public static boolean getIsPlaying(Intent intent) {
        return intent.getBooleanExtra(IS_PLAYING, false);
    }

    public static void setIsFromLeaderBoard(Intent intent, boolean fromLeaderBoard) {
        intent.putExtra(FROM_LEADER_BOARD, fromLeaderBoard);
    }

    public static boolean getIsFromLeaderBoard(Intent intent) {
        return intent.getBooleanExtra(FROM_LEADER_BOARD, false);
    }

    public static void setRoomId(Intent intent, String roomId) {
        intent.putExtra(ROOM_ID, roomId);
    }

    public static String getRoomID(Intent intent) {
        return intent.getStringExtra(ROOM_ID);
    }

    public static void setIsCommeInBySw(Intent intent, boolean isCommeInBySw) {
        intent.putExtra(IS_COMEIN_BY_SW, isCommeInBySw);
    }

    public static boolean getIsCommeInBySw(Intent intent) {
        return intent.getBooleanExtra(IS_COMEIN_BY_SW, false);
    }

}
