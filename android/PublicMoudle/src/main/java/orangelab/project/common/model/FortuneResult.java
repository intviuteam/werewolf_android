package orangelab.project.common.model;

/**
 * Created by wangxu on 2017/8/21.
 */

public class FortuneResult {
    public int result;
    public Gift gift;

   public class Gift {
        public String txt;
        public String img;
        public String type;
    }
}
