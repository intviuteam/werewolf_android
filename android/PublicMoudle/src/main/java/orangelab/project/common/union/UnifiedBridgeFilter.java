package orangelab.project.common.union;

import com.toolkit.action.Destroyable;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2017/9/19 下午4:31
 * Mystery
 */

public interface UnifiedBridgeFilter extends Destroyable{
    boolean filterAction(String action, JSONObject params, UnifiedBridgeModel.UnifiedBridgeModelOption option, IUnifiedBridge bridge);
}
