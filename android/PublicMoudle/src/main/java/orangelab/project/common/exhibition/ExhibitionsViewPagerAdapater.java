package orangelab.project.common.exhibition;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

/**
 * game_werewolf
 * 2017/6/21 下午1:21
 * Mystery
 */

public class ExhibitionsViewPagerAdapater<T extends View> extends PagerAdapter {

    private List<T> items;

    public ExhibitionsViewPagerAdapater(List<T> items) {
        this.items = items;
    }

    public void removeLastPage() {
        if (items.size() > 1) {
            items.remove(items.size() - 1);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(items.get(position));
        return items.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
