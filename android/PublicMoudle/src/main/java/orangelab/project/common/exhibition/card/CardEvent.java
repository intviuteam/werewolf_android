package orangelab.project.common.exhibition.card;

import android.graphics.PointF;

import orangelab.project.common.exhibition.ExhibitionPackage;

/**
 * game_werewolf
 * 2017/6/28 下午5:53
 * Mystery
 */

public class CardEvent {

    public static class WereWolfDismissCardEvent {
        private String type;

        public WereWolfDismissCardEvent(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class WereWolfKeyBoardChangeEvent {
        private PointF pointF;

        public WereWolfKeyBoardChangeEvent(PointF pointF) {
            this.pointF = pointF;
        }

        public PointF getPointF() {
            return pointF;
        }
    }

    public static class WereWolfTryUseCheckCardEvent {

    }

    public static class WereWolfRequestUseCardEvent {
        private WereWolfRequestShowCardEvent event;
        private boolean isDisposable;

        public WereWolfRequestUseCardEvent(WereWolfRequestShowCardEvent event, boolean isDisposable) {
            this.event = event;
            this.isDisposable = isDisposable;
        }

        public WereWolfRequestShowCardEvent getEvent() {
            return event;
        }

        public void setEvent(WereWolfRequestShowCardEvent event) {
            this.event = event;
        }

        public boolean isDisposable() {
            return isDisposable;
        }

        public void setDisposable(boolean disposable) {
            isDisposable = disposable;
        }
    }

    public static class WereWolfRequestShowCardEvent {
        private String type;
        private PointF referencePoint;

        public WereWolfRequestShowCardEvent(String type, PointF referencePoint) {
            this.type = type;
            this.referencePoint = referencePoint;
        }

        public PointF getReferencePoint() {
            return referencePoint;
        }

        public String getType() {
            return type;
        }
    }


    public static class RequestShowCardEvent {
        private String type;

        public RequestShowCardEvent(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public static class RequestDismissCardEvent {
        private String type;

        public RequestDismissCardEvent(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public static class UseCardCheckEvent {
    }

    public static class CardForGameWrapperEvent {
        private Card mCard;
        private ExhibitionPackage mExhibitionPackage;

        public CardForGameWrapperEvent(Card card, ExhibitionPackage exhibitionPackage) {
            mCard = card;
            mExhibitionPackage = exhibitionPackage;
        }

        public Card getCard() {
            return mCard;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }
    }

    public static class CardForChatWrapperEvent {
        private Card mCard;
        private ExhibitionPackage mExhibitionPackage;

        public CardForChatWrapperEvent(Card card, ExhibitionPackage exhibitionPackage) {
            mCard = card;
            mExhibitionPackage = exhibitionPackage;
        }

        public Card getCard() {
            return mCard;
        }

        public void setCard(Card card) {
            mCard = card;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }

        public void setExhibitionPackage(ExhibitionPackage exhibitionPackage) {
            mExhibitionPackage = exhibitionPackage;
        }
    }

    public static class CardForMainPageWrapperEvent {
        private Card mCard;
        private ExhibitionPackage mExhibitionPackage;

        public CardForMainPageWrapperEvent(Card card, ExhibitionPackage exhibitionPackage) {
            mCard = card;
            mExhibitionPackage = exhibitionPackage;
        }

        public Card getCard() {
            return mCard;
        }

        public void setCard(Card card) {
            mCard = card;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }

        public void setExhibitionPackage(ExhibitionPackage exhibitionPackage) {
            mExhibitionPackage = exhibitionPackage;
        }
    }

    public static class CardForOutSideWrapperEvent {
        private Card mCard;
        private ExhibitionPackage mExhibitionPackage;

        public CardForOutSideWrapperEvent(Card card, ExhibitionPackage exhibitionPackage) {
            mCard = card;
            mExhibitionPackage = exhibitionPackage;
        }

        public Card getCard() {
            return mCard;
        }

        public void setCard(Card card) {
            mCard = card;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }

        public void setExhibitionPackage(ExhibitionPackage exhibitionPackage) {
            mExhibitionPackage = exhibitionPackage;
        }
    }

    public static class RequestMoveCardEvent {
        private String type;

        public RequestMoveCardEvent(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}
