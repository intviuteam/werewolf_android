package orangelab.project.common.exhibition;

import android.text.TextUtils;

import orangelab.project.common.exhibition.card.CardUtils;
import orangelab.project.common.exhibition.gift.GiftUtils;

/**
 * game_werewolf
 * 2017/6/27 下午1:31
 * Mystery
 */

public class ExhibitionsUtils {

    public static boolean priceTypeIsGold(String type) {
        return TextUtils.equals(type, ExhibitionsConstant.PRICE_TYPE_GOLD);
    }

    public static boolean priceTypeIsDiamond(String type) {
        return TextUtils.equals(type, ExhibitionsConstant.PRICE_TYPE_DIAMOND);
    }

    public static boolean isCard(String type) {
        return TextUtils.equals(type, ExhibitionsConstant.EXHIBITIONS_TYPE_CARD);
    }
}
