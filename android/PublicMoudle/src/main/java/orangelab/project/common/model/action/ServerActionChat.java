package orangelab.project.common.model.action;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/19 下午6:22
 * Mystery
 */

public class ServerActionChat extends ServerActionContainsPosition implements Keepable {

    public static final String CHAT_TYPE_IMAGE = "image";
    public static final String PAGE_PLANE = "page_plane";

    public String message;
    public String channel;
    public ServerActionChatGift gift;
    public ServerActionChatImage image;
    public ClientActionSendPaperPlane.ClientActionSendPaperPlaneTo to;
    public ClientActionSendPaperPlane.ClientActionSendPaperItem from;
    public String type = "";

    public boolean isChatImage() {
        return TextUtils.equals(type, CHAT_TYPE_IMAGE);
    }

    public boolean isPaperPlane() {
        return TextUtils.equals(type, PAGE_PLANE);
    }

    public static class ServerActionChatGift implements Keepable {
        public int from = -1;
        public int to = -1;
        public String type;
        public String ticket;
    }

    public static class ServerActionChatImage implements Keepable {
        public String image_id;
        public String url;
        public long expire_time;
    }
}
