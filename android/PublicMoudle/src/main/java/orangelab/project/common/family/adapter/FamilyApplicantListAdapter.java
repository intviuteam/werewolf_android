package orangelab.project.common.family.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.R;

import orangelab.project.common.activity.PersonalInfoActivity;

import com.toolkit.action.Destroyable;

import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.model.UserInfoResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.view.UserHeadView;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/10/20 上午10:02
 * Mystery
 */

public class FamilyApplicantListAdapter extends RecyclerView.Adapter<FamilyApplicantListAdapter.FamilyApplicantViewHolder> implements Destroyable {

    private List<UserInfoResult> mData = new ArrayList<>();
    private SafeHandler mSafeHandler = new SafeHandler();
    private List<String> mMemo = new ArrayList<>();


    public FamilyApplicantListAdapter() {

    }

    public void updateData(List<UserInfoResult> updateData) {
        if (mData != null) {
            mData.clear();
        }
        mData = updateData;
        notifyDataSetChanged();
    }

    public void clear() {
        if (mData != null) {
            mData.clear();
            notifyDataSetChanged();
        }
    }

    public void addData(List<UserInfoResult> data) {
        if (mData != null) {
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    @Override
    public FamilyApplicantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FamilyApplicantViewHolder(View.inflate(parent.getContext(), R.layout.layout_item_family_applicant, null), mSafeHandler, mMemo);
    }

    @Override
    public void onBindViewHolder(FamilyApplicantViewHolder holder, int position) {
        UserInfoResult item = mData.get(position);
        try {
            holder.mName.setText(item.name);
            holder.mName.setTag(item.id);
            holder.mHead.setUserHeadImageUrl(item.image);
            holder.mHead.setUserSex(item.sex);
            holder.mLevel.setText(MessageUtils.getLevelString(item.game.level));
            if (mMemo.contains(item.id)) {
                holder.hasApprove();
            } else {
                holder.hasNotApprove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void destroy() {
        mSafeHandler.release();
    }

    public static class FamilyApplicantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private UserHeadView mHead;
        private TextView mName;
        private TextView mLevel;
        private Button mAction;
        private SafeHandler mSafeHandler;
        private List<String> mApproveMemo;

        public FamilyApplicantViewHolder(View itemView, SafeHandler safeHandler, List<String> approveMemo) {
            super(itemView);
            mSafeHandler = safeHandler;
            mApproveMemo = approveMemo;
            mHead = (UserHeadView) itemView.findViewById(R.id.id_item_family_applicant_head);
            mLevel = (TextView) itemView.findViewById(R.id.id_item_family_applicant_level);
            mName = (TextView) itemView.findViewById(R.id.id_item_family_applicant_name);
            mAction = (Button) itemView.findViewById(R.id.id_item_family_applicant_action);

            mHead.setOnClickListener(this);
        }

        public void hasApprove() {
            mAction.setEnabled(false);
            mAction.setBackgroundResource(R.drawable.drawable_family_btn_has_approve);
            mAction.setText(MessageUtils.getString(R.string.string_family_apply_has_approve));
            mAction.setTextColor(Color.parseColor("#f27121"));
        }

        public void hasNotApprove() {
            mAction.setEnabled(true);
            mAction.setBackgroundResource(R.drawable.drawable_family_yellow_big_radio_button_bg);
            mAction.setText(MessageUtils.getString(R.string.string_family_apply_has_not_approve));
            mAction.setTextColor(Color.parseColor("#88450e"));
            mAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.id_item_family_applicant_action) {
                String userId = (String) mName.getTag();
                FamilyApiManager.ApproveFamilyApply(userId, (s, e) -> {
                    if (e != null) {
                        mSafeHandler.postSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
                    } else {
                        mSafeHandler.postSafely(() -> {
                            mApproveMemo.add(userId);
                            hasApprove();
                        });
                    }
                });
            } else if (id == R.id.id_item_family_applicant_head) {
                String userId = (String) mName.getTag();
                PersonalInfoActivity.Launch(mHead.getContext(), userId);
            }
//            switch (v.getId()) {
//                case R.id.id_item_family_applicant_action: {
//                    String userId = (String) mName.getTag();
//                    FamilyApiManager.ApproveFamilyApply(userId, (s, e) -> {
//                        if (e != null) {
//                            mSafeHandler.postSafely(RunnableFactory.createMsgToastRunnable(e.getMessage()));
//                        } else {
//                            mSafeHandler.postSafely(() -> {
//                                mApproveMemo.add(userId);
//                                hasApprove();
//                            });
//                        }
//                    });
//                }
//                break;
//                case R.id.id_item_family_applicant_head: {
//                    String userId = (String) mName.getTag();
//                    PersonalInfoActivity.Launch(mHead.getContext(), userId);
//                }
//                break;
//
//            }
        }
    }
}
