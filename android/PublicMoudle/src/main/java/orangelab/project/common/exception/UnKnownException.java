package orangelab.project.common.exception;

import com.R;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2017/10/28 下午2:59
 * Mystery
 */

public class UnKnownException extends Exception {

    public UnKnownException() {
        super(MessageUtils.getString(R.string.string_unkown_error));
    }
}
