package orangelab.project.common.effect.view;

import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.androidtoolkit.HttpToolkit;
import com.androidtoolkit.StreamToolKit;
import com.androidtoolkit.ThreadToolKit;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.event.RoomEffectExpressionEvent;
import orangelab.project.common.utils.SafeHandler;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2018/7/24 下午2:33
 * Mystery
 * <p>
 * 房间表情ViewGroup
 */
public class RoomEffectsExpressionViewGroup extends FrameLayout implements Destroyable {

    private static final String TAG = "RoomEffectsExpressionViewGroup";
    private Map<PointF, RoomGifImageView> mExpressionImageViewMap = new HashMap<>();
    private SafeHandler mSafeHandler = new SafeHandler();
    private RoomEffectsContext mRoomEffectsContext;

    public RoomEffectsExpressionViewGroup(@NonNull Context context) {
        this(context, null);
    }

    public RoomEffectsExpressionViewGroup(@NonNull RoomEffectsContext roomEffectsContext, @NonNull Context context) {
        this(context);
        mRoomEffectsContext = roomEffectsContext;
    }

    public RoomEffectsExpressionViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomEffectsExpressionViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RoomEffectExpressionEvent event) {

//        ThreadToolKit.Async(() -> {
//            InputStream inputStream = ApkResourceManager.GetApkAssetInputStream(event.getType());
//            if (inputStream == null) {
//                inputStream = EffectsManager.GetEmotionInputStream(event.getUrl());
//            }
//            byte[] data = StreamToolKit.readStream(inputStream);
//            try {
//                inputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            mSafeHandler.postSafely(() -> {
//                if (data != null) {
//                    try {
//                        RoomGifImageView imageView = mExpressionImageViewMap.get(event.getPoint());
//                        if (imageView == null) {
//                            imageView = new RoomGifImageView(getContext());
//                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(event.getWidth(), event.getHeight());
//                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                            imageView.setX(event.getPoint().x);
//                            imageView.setY(event.getPoint().y);
//                            addView(imageView, layoutParams);
//                            mExpressionImageViewMap.put(event.getPoint(), imageView);
//                        }
//                        imageView.start(new RoomGifImageView.Task(data, false, event.getTime()));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    EffectsManager.RefreshEmotionManifestForEmotions(null);
//                }
//            });
//        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    @Override
    public void destroy() {
        EventBus.getDefault().unregister(this);
        mSafeHandler.release();
    }
}
