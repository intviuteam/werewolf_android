package orangelab.project.common.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/10/31 下午7:27
 * Mystery
 */

public class UpLoadFileResultParams implements Keepable{

    public int code;
    public String message;
    public ArrayList<UpLoadFinishItem> data;
}