package orangelab.project.common.family.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.family.FamilyConstant;
import orangelab.project.common.family.event.FamilyChangeTitleEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.hint.SafePopWindow;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.view.ArrowRectangleViewGroup;

import com.toolkit.ClickProtector;
import com.toolkit.IClickIntercept;
import com.toolkit.action.ApiResult;

/**
 * game_werewolf
 * 2017/11/24 下午3:07
 * Mystery
 */

public class FamilyMemberReNamePopUpWindow extends SafePopWindow implements View.OnClickListener {

    private Context mContext;
    private ArrowRectangleViewGroup mContainer;
    private Button mDeputy;
    private Button mElder;
    private Button mElite;
    private Button mSlave;

    private int mWidth;
    private int mHeight;

    private ClickProtector mClickProtector;
    private boolean isLoading = false;
    private String mUserId;

    private FamilyMemberReNamePopUpWindow(Context context, String userId) {
        mContext = context;
        mUserId = userId;
        mContainer = (ArrowRectangleViewGroup) View.inflate(context, R.layout.layout_popwindow_family_member_rename, null);
        mDeputy = (Button) mContainer.findViewById(R.id.id_family_members_rename_deputy);
        mElder = (Button) mContainer.findViewById(R.id.id_family_members_rename_elder);
        mElite = (Button) mContainer.findViewById(R.id.id_family_members_rename_elite);
        mSlave = (Button) mContainer.findViewById(R.id.id_family_members_rename_slave);
        mContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        mWidth = ScreenUtils.dip2px(135);
        mHeight = mContainer.getMeasuredHeight();
        setContentView(mContainer);
        setWidth(ScreenUtils.dip2px(135));
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mClickProtector = new ClickProtector() {
            @Override
            public void onClickWithProtector(View v) {
                FamilyMemberReNamePopUpWindow.this.onClick(v);
            }
        };
        mClickProtector.addIntercept(view -> isLoading);
        mElder.setOnClickListener(mClickProtector);
        mDeputy.setOnClickListener(mClickProtector);
        mElite.setOnClickListener(mClickProtector);
        mSlave.setOnClickListener(mClickProtector);
    }

    public int getRealWidth() {
        return mWidth;
    }

    public int getRealHeight() {
        return mHeight;
    }

    public void setArrowDirection(int direction) {
        mContainer.setArrowDirection(direction);
    }

    public static void ShowAtView(View targetView, View referenceView, String userId) {
        int[] location = new int[2];
        referenceView.getLocationOnScreen(location);


        int viewX = location[0];
        int viewY = location[1];

        int curScreenWidth = ScreenUtils.getCurrentScreenWidth();
        int curScreenHeight = ScreenUtils.getCurrentScreenHeight();

        FamilyMemberReNamePopUpWindow popUpWindow = new FamilyMemberReNamePopUpWindow(targetView.getContext(), userId);
        int popWindowWidth = popUpWindow.getRealWidth();
        int popWindowHeight = popUpWindow.getRealHeight();

        int dif = viewY + popWindowHeight - curScreenHeight;
        int minDif = ScreenUtils.dip2px(95); //最低差值
        int margin = ScreenUtils.dip2px(6);
        int showX = viewX + referenceView.getWidth() + margin;

        if (dif > 0) {
            /**
             * 此时PopWindow已经显示不开了
             */
            if (dif >= minDif + margin) {
                /**
                 * 暴露出来的空间太小，直接忽略
                 */
                popUpWindow = null;
                return;
            } else {
                popUpWindow.setArrowDirection(ArrowRectangleViewGroup.ARROW_LEFT_BOTTOM);
                popUpWindow.showAtLocation(targetView, Gravity.NO_GRAVITY, showX, viewY - dif - margin);
            }
        } else {
            /**
             * 此时PopWindow是可以正常显示的
             */
            popUpWindow.setArrowDirection(ArrowRectangleViewGroup.ARROW_LEFT_TOP);
            popUpWindow.showAtLocation(targetView, Gravity.NO_GRAVITY, showX, viewY + margin);
        }
    }

    private void updateSuccess(String userId, String title) {
        dismiss();
        RxToolKit.Emit(new FamilyChangeTitleEvent(userId, title));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_family_members_rename_deputy) {
            isLoading = true;
            FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_DEPUTY, (integer, e) -> {
                runSafely(() -> {
                    isLoading = false;
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        updateSuccess(mUserId, FamilyConstant.FAMILY_DEPUTY);
                    }
                });
            });
        } else if (id == R.id.id_family_members_rename_elder) {
            isLoading = true;
            FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_ELDER, (integer, e) -> {
                runSafely(() -> {
                    isLoading = false;
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        updateSuccess(mUserId, FamilyConstant.FAMILY_ELDER);
                    }
                });
            });
        } else if (id == R.id.id_family_members_rename_elite) {
            isLoading = true;
            FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_ELITE, (integer, e) -> {
                runSafely(() -> {
                    isLoading = false;
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        updateSuccess(mUserId, FamilyConstant.FAMILY_ELITE);
                    }
                });
            });
        } else if (id == R.id.id_family_members_rename_slave) {
            isLoading = true;
            FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_MEMBER, (integer, e) -> {
                runSafely(() -> {
                    isLoading = false;
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        updateSuccess(mUserId, FamilyConstant.FAMILY_MEMBER);
                    }
                });
            });
        }
//        switch (v.getId()) {
//            case R.id.id_family_members_rename_deputy:
//                isLoading = true;
//                FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_DEPUTY, (integer, e) -> {
//                    runSafely(() -> {
//                        isLoading = false;
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            updateSuccess(mUserId, FamilyConstant.FAMILY_DEPUTY);
//                        }
//                    });
//                });
//                break;
//            case R.id.id_family_members_rename_elder:
//                isLoading = true;
//                FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_ELDER, (integer, e) -> {
//                    runSafely(() -> {
//                        isLoading = false;
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            updateSuccess(mUserId, FamilyConstant.FAMILY_ELDER);
//                        }
//                    });
//                });
//                break;
//            case R.id.id_family_members_rename_elite:
//                isLoading = true;
//                FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_ELITE, (integer, e) -> {
//                    runSafely(() -> {
//                        isLoading = false;
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            updateSuccess(mUserId, FamilyConstant.FAMILY_ELITE);
//                        }
//                    });
//                });
//                break;
//            case R.id.id_family_members_rename_slave:
//                isLoading = true;
//                FamilyApiManager.FamilyChangeTitleApi(mUserId, FamilyConstant.FAMILY_MEMBER, (integer, e) -> {
//                    runSafely(() -> {
//                        isLoading = false;
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            updateSuccess(mUserId, FamilyConstant.FAMILY_MEMBER);
//                        }
//                    });
//                });
//                break;
//        }
    }

    @Override
    protected void release() {
        mContext = null;
    }
}
