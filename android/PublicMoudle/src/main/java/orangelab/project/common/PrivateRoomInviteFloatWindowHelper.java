package orangelab.project.common;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.WindowManager;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Keepable;

import org.greenrobot.eventbus.EventBus;

import orangelab.project.common.event.GlobalEvent;
import orangelab.project.common.floatwindow.view.PrivateRoomInviteView;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.config.VoiceRoomConfig;

/**
 * game_werewolf
 * 2018/5/23 下午3:48
 * Mystery
 */
public class PrivateRoomInviteFloatWindowHelper extends BaseGlobalFloatWindowHelper implements PrivateRoomInviteView.PrivateRoomInviteViewObserver, AppLifeCycle {

    public static final String DESTROY_INVITE = "destroy_private_room_invite";

    /**
     * 私密房邀请的提示
     */
    public static class PrivateRoomInvite implements Keepable {
        public String userName;
        public String userId;
        public String userIcon;
        public String roomId;
        public String roomPassword;
        public String roomType;
    }

    private SafeHandler mSafeHandler;
    private PrivateRoomInviteView mPrivateRoomInviteView;
    private long mShowTimeOut = 10000L;
    private Runnable mRunnable = () -> {
        if (mPrivateRoomInviteView != null) {
            destroyView();
        }
    };

    public PrivateRoomInviteFloatWindowHelper(Context context, IGlobalService IGlobalService, WindowManager windowManager) {
        super(context, IGlobalService, windowManager);
        mSafeHandler = new SafeHandler();
    }

    public void destroyPrivateRoomInvite() {
        destroyView();
    }

    public void showPrivateRoomInvite(PrivateRoomInvite invite) {
        if (invite == null) {
            return;
        }
        stopCheck();

        if (isShowing()) {
            /**
             * 已经有了则直接更换数据即可
             */
            mPrivateRoomInviteView.setUp(invite.userIcon, invite.userName, invite.roomId, invite.roomPassword, invite.roomType);
            startCheck();
            return;
        }

        destroyView();

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        /**
         * 设定宽度
         */
        layoutParams.width = ScreenUtils.dip2px(350);
        layoutParams.height = ScreenUtils.dip2px(80);
        layoutParams.gravity = Gravity.TOP;

        /**
         * 设定Type
         */
        setUpLayoutParamsType(layoutParams);

        layoutParams.format = PixelFormat.RGBA_8888;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        /**
         * 创建View PrivateRoomInviteView
         */
        mPrivateRoomInviteView = new PrivateRoomInviteView(mContext);
        mPrivateRoomInviteView.setObserver(this);
        mPrivateRoomInviteView.setUp(invite.userIcon, invite.userName, invite.roomId, invite.roomPassword, invite.roomType);

        int x = (ScreenUtils.getCurrentScreenWidth() - layoutParams.width) / 2;
        int y = layoutParams.height;

        layoutParams.x = x;
        layoutParams.y = y;

        mPrivateRoomInviteView.setLayoutParams(layoutParams);

        showInviteView();

        startCheck();
    }

    private void startCheck() {
        mSafeHandler.postDelaySafely(mRunnable, mShowTimeOut);
    }

    private void stopCheck() {
        mSafeHandler.removeCallbacks(mRunnable);
    }

    private boolean isShowing() {
        if (mPrivateRoomInviteView != null && mPrivateRoomInviteView.isShowing()) {
            return true;
        } else {
            return false;
        }
    }

    private void showInviteView() {
        try {
            if (mPrivateRoomInviteView != null && mWindowManager != null) {
                mWindowManager.addView(mPrivateRoomInviteView, mPrivateRoomInviteView.getLayoutParams());
                mPrivateRoomInviteView.setShowing(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void destroyView() {
        dismiss();
        if (mPrivateRoomInviteView != null) {
            mPrivateRoomInviteView.destroy();
            mPrivateRoomInviteView = null;
        }
    }

    private void dismiss() {
        try {
            if (isShowing()) {
                mWindowManager.removeView(mPrivateRoomInviteView);
                mPrivateRoomInviteView.setShowing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpLayoutParamsType(WindowManager.LayoutParams layoutParams) {
        if (Build.VERSION.SDK_INT >= 24) { /*android7.0不能用TYPE_TOAST*/
            layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        } else { /*以下代码块使得android6.0之后的用户不必再去手动开启悬浮窗权限*/
            String packname = mContext.getPackageName();
            PackageManager pm = mContext.getPackageManager();
            boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.SYSTEM_ALERT_WINDOW", packname));
            if (permission) {
                layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;
            } else {
                layoutParams.type = WindowManager.LayoutParams.TYPE_TOAST;
            }
        }
    }

    @Override
    public void onClose() {
        destroyView();
    }

    @Override
    public void onAccept(String roomId, String roomPassword, String roomType) {
        /**
         * 执行进入房间操作
         */
        if (GlobalServiceUtils.canShowPrivateRoomInvite()) {
            /**
             * 可以执行进入房间操作
             */
            destroyView();

            if (UserBehaviorObserver.isInAudioRoom()) {
                /**
                 * 如果是在语音房里面
                 */
                if (VoiceRoomConfig.isIsSmallWindowStyle()) {
                    /**
                     * 当前是小窗模式
                     */
                    EventBus.getDefault().post(new GlobalEvent.PrivateRoomInviteForNormal(roomId, roomPassword, roomType));
                } else {
                    /**
                     * 当前处于语音房非小窗模式
                     */
                    EventBus.getDefault().post(new GlobalEvent.PrivateRoomInviteForVoiceRoom(roomId, roomPassword, roomType));
                }
            } else {
                /**
                 * 不在语音房里
                 */
                EventBus.getDefault().post(new GlobalEvent.PrivateRoomInviteForNormal(roomId, roomPassword, roomType));
            }
        }
    }

    @Override
    public void onRefuse() {
        /**
         * 执行拒绝房间操作
         */
        destroyView();
    }

    @Override
    public void appOnTop() {
        /**
         * 应用进入了前台
         */
        if (mPrivateRoomInviteView != null) {
            /**
             * 说明了又一个View消失了，此时需要恢复
             */
            showInviteView();
        }
    }

    @Override
    public void appInBackGround() {
        /**
         * 应用进入了后台
         */
        dismiss();
    }

    @Override
    public void destroy() {
        super.destroy();
        destroyView();
    }
}
