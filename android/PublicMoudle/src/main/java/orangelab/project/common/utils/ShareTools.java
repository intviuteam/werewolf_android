package orangelab.project.common.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

import com.networktoolkit.transport.Constant;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import orangelab.project.ProjectConfig;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.share.SharePoolManager;

/**
 * game_werewolf
 * 2017/6/13 下午3:49
 * Mystery
 */

public class ShareTools {

    private static final String TAG = "ShareTools";

    public static void shareGameOverImage(
            Activity activity,
            GameShareFactory.Builder builder,
            SHARE_MEDIA share_media
    ) {
        //标记为分享还是邀请
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        Bitmap bitmap = SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).getGameOverShareView(activity, builder);
        UMImage image = new UMImage(activity, bitmap);
        new ShareAction(activity)
                .setPlatform(share_media)
                .withMedia(image)
                .setCallback(new UMShareListener() {

                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onResult(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onResult: ");
                        releaseShareBitmap(bitmap);
                        shareSuccessReported(Constant.SHARE_TO_QQ);
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                        Log.i(TAG, "onError: ", throwable);
                        releaseShareBitmap(bitmap);
                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media) {
                        Log.i(TAG, "onCancel: ");
                        releaseShareBitmap(bitmap);
                    }
                })
                .share();
    }

    private static void releaseShareBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    public static void shareSuccessReported(String type) {
        ApiManager.DoShareApi(type, 1);
    }

}
