package orangelab.project.common.pay.google;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import orangelab.project.common.db.UserPayOrderDaoHelper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;
import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import rx.Observable;
import rx.Subscriber;

/**
 * game_werewolf
 * 2017/8/19 下午6:03
 * Mystery
 */

public class GooglePayOrderTask implements Runnable {

    private static final String TAG = "GooglePayOrderTask";

    private Context mContext;
    private UserPayOrderDaoHelper helper = null;

    private List<UserPayOrderEntity> entityList;
    private int taskLength = 0;
    private boolean hasRelease = false;

    public GooglePayOrderTask(Context context) {
        mContext = context;
    }

    private void release() {
        if (!hasRelease) {
            hasRelease = true;
            if (helper != null) {
                helper.release();
                helper = null;
            }
            if (mContext != null) {
                mContext = null;
            }
        }
        Log.i(TAG, "release Finish");
    }

    private void threadInfo() {
        PLog.i(TAG, "Thread Name =" + Thread.currentThread().getName());
    }

    public static void reportErrorOrder(String reason, String orderId, String userId) {
        Utils.runSafely(() -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ErrorOrder\n");
            stringBuilder.append("Reason: " + reason);
            stringBuilder.append("OrderId:" + orderId + "\n");
            stringBuilder.append("UserId: " + userId + "\n");
            ReportEventUtils.reportException(stringBuilder.toString());
        });
    }

    public static void reportErrorOrder(UserPayOrderEntity entity) {
        if (Utils.targetIsNull(entity)) {
            return;
        }
        Utils.runSafely(() -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ErrorOrder\n");
            stringBuilder.append("OrderId:" + entity.getOrderId() + "\n");
            stringBuilder.append("UserId: " + entity.getUserId() + "\n");
            stringBuilder.append("Data:" + entity.toString());
            ReportEventUtils.reportException(stringBuilder.toString());
        });
    }

    public static void reportErrorOrder(String reason, UserPayOrderEntity entity) {
        if (Utils.targetIsNull(entity)) {
            return;
        }
        Utils.runSafely(() -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ErrorOrder\n");
            stringBuilder.append("Reason: " + reason);
            stringBuilder.append("OrderId:" + entity.getOrderId() + "\n");
            stringBuilder.append("UserId: " + entity.getUserId() + "\n");
            stringBuilder.append("Data:" + entity.toString());
            ReportEventUtils.reportException(stringBuilder.toString());
        });
    }

    @Override
    public void run() {
        helper = new UserPayOrderDaoHelper(mContext);
        entityList = helper.getAllUserOrderEntity();
        if (Utils.targetListIsNull(entityList)) {
            PLog.i(TAG, "no error order");
            release();
            return;
        }
        taskLength = entityList.size();
        if (taskLength == 0) {
            /**
             * 无须执行任务直接 释放资源
             */
            PLog.i(TAG, "Task length is 0");
            release();
            return;
        }
        PLog.i(TAG, "begin Async Task");
        PLog.i(TAG, entityList.toString());
        Observable.from(entityList)
                .onBackpressureBuffer(Constant.RX_BACK_PRESSURE)
                .subscribe(new Subscriber<UserPayOrderEntity>() {
                    @Override
                    public void onStart() {
                        PLog.i(TAG, "onStart: ");
                        threadInfo();
                    }

                    @Override
                    public void onCompleted() {
                        PLog.i(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        PLog.i(TAG, "onError: " + e.getMessage());
                    }

                    @Override
                    public void onNext(UserPayOrderEntity entity) {
                        PLog.i(TAG, "start entity: " + entity);
                        if (intEqualString(entity.getOtherData1(), Constant.MAX_RETRY)) {
                            PLog.i(TAG, "retry has been max so ingore it");
                            reportErrorOrder("RetryMax", entity);
                            return;
                        }
                        threadInfo();
                        JSONObject json = new JSONObject();
                        try {
                            json.put(Constant.SIGNATURE, entity.getSignature());
                            json.put(Constant.DATA, entity.getPurchaseData());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        PLog.i(TAG, "start RequestTask");
                        String token = entity.getOtherData2();
                        if (TextUtils.isEmpty(token)) {
                            /**
                             * Token 为空上报异常订单
                             */
                            PLog.i(TAG, "Token is Null report");
                            reportErrorOrder("TokenNull", entity);
                            request(1);
                            return;
                        }
                        ReportRobot.robot.report("INNER_REPORT_ORDER");
                        RequestTask.create(
                                false,
                                Constant.GOOGLE_PAY,
                                token,
                                json,
                                new RequestTaskCallBack() {
                                    @Override
                                    public void onSuccess(String response) {
                                        threadInfo();
                                        PLog.i(TAG, response);
                                        PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                                        PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                        switch (item.getOp_result()) {
                                            case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                                PLog.i(TAG, "Server pay success");
                                                if (helper != null) {
                                                    helper.deleteData(
                                                            entity.getUserId(),
                                                            entity.getOrderId()
                                                    );
                                                    PLog.i(TAG, "deleteFinish");
                                                }
                                            }
                                            break;
                                            default: {
                                                PLog.i(TAG, "Server pay failed");
                                                entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                                if (helper != null) {
                                                    helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                                }
                                                PLog.i(TAG, "Server pay update record retry count");
                                                reportErrorOrder("ServerError", entity);
                                            }
                                            break;
                                        }
                                        request(1);
                                    }

                                    @Override
                                    public void onFailed(int code, String failed) {
                                        Utils.runSafely(() -> {
                                            entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                            if (helper != null) {
                                                helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                            }
                                        });
                                        reportErrorOrder("ServerError", entity);
                                        request(1);
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Utils.runSafely(() -> {
                                            entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                            if (helper != null) {
                                                helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                            }
                                        });
                                        reportErrorOrder("RequestException", entity);
                                        request(1);
                                    }
                                }
                        ).executeInCurThread();
                    }
                });
        PLog.i(TAG, "end Async Task");
        release();
    }

    public boolean intEqualString(String targetString, int targetInt) {
        try {
            return Integer.parseInt(targetString) == targetInt;
        } catch (Exception e) {
            return false;
        }
    }

    public String intStringAdd(String target) {
        try {
            if (TextUtils.isEmpty(target)) {
                return "1";
            }
            int result = Integer.parseInt(target);
            result++;
            return Integer.toString(result);
        } catch (Exception e) {
            return target;
        }
    }
}
