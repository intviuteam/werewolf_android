package orangelab.project.common.exhibition;


/**
 * game_werewolf
 * 2017/6/20 下午6:18
 * Mystery
 */

public class ExhibitionsEvent {

    public static class ExhibitionsShowCaseEvent {
        public static final int ACTION_SHOW = 1;
        public static final int ACTION_DISMISS = 2;
        public static final int ACTION_CANCEL = 3;

        private int action;

        public ExhibitionsShowCaseEvent(int action) {
            this.action = action;
        }

        public int getAction() {
            return action;
        }
    }

    public static class ExhibitionsRemoveEvent {
        private String exhibitionType;

        public ExhibitionsRemoveEvent(String exhibitionType) {
            this.exhibitionType = exhibitionType;
        }

        public String getExhibitionType() {
            return exhibitionType;
        }
    }

    public static class ExhibitionsByGameEvent {

    }
}
