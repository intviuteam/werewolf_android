package orangelab.project.common.engine.task;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.RxToolKit;
import orangelab.project.common.engine.SocketEngine;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.utils.ReportEventUtils;
import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * game_werewolf
 * 2018/3/5 下午3:25
 * Mystery
 */

public class SocketEngineARQTask implements Destroyable {

    public static final String MSG_RESPOND_SERVER = "msg_respond_server";
    public static final String MSG_RESPOND = "msg_respond";
    public static final String RESPOND_ID = "respond_id";
    public static final String SERVER_RESPOND_ID = "msg_respond_id";

    private static final long RUNNABLE_DELAY_CHECK_TIME = 2000l;

    private String mTaskId;

    private String mMsgId;
    private String mTaskType;

    private String mCurRespondId;
    private SocketEngine mSocketEngine;
    private String mSocketType;
    private JSONObject mReSendMessage;
    private boolean mDestroy = false;

    private Handler mHandler;
    private Runnable mReSendRunnable = () -> {
        if (mSocketEngine != null) {
//            replaceRespondId();
//            reSendMessage();
            /**
             * 这里不在做重传，直接上报未命中
             */
            ReportEventUtils.reportNotHitCount();
            printLog("report not hit [" + mTaskType + "]");
            destroy();
        }
    };

    public SocketEngineARQTask(SocketEngine socketEngine, String taskType, JSONObject reSendMessage, String msgId) {
        mSocketEngine = socketEngine;
        mReSendMessage = reSendMessage;
        mTaskId = UUID.randomUUID().toString();
        mTaskType = taskType;
        mMsgId = msgId;
        mSocketType = socketEngine.getSocketType();
        initListener();
        printLog("SocketEngineARQTask: [" + mTaskId + "] create ");
    }

    public String getTaskId() {
        return mTaskId;
    }

    private void initListener() {
        RxToolKit.Build(mTaskId, ServerResponseEvent.class).action(v -> {
            if (mHandler != null) {
                mHandler.post(() -> {
                    if (TextUtils.equals(v.getFromSocketType(), mSocketType)) {
                        if (TextUtils.equals(mCurRespondId, OptServerRespondId(v.payload))) {
                            /**
                             * 命中消息，清除Runnable
                             */
                            printLog("[" + mTaskId + "]Hint RespondId: ");
                            clearRunnable();
                            destroy();
                        }
                    }
                });
            }
        }).register();
    }

    private void releaseListener() {
        RxToolKit.UnRegister(mTaskId);
    }

    public void replaceRespondId() {
        if (mReSendMessage != null && mSocketEngine != null) {
            try {
                mMsgId = mSocketEngine.getSocketMsgId();
                mCurRespondId = mSocketEngine.getSocketARQRespondId(mMsgId);
                mReSendMessage.put(RESPOND_ID, mCurRespondId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void reSendMessage() {
        if (mSocketEngine != null) {
            mSocketEngine.printLog("[" + mTaskId + "] exec resend Message: ");
            mSocketEngine.sendMessageActionMsg(mTaskType, mReSendMessage, mMsgId);
        }
    }

    public void bindHandler(Handler handler) {
        mHandler = handler;
    }

    public void clearRunnable() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mReSendRunnable);
        }
    }

    public void startCheck() {
        if (mHandler != null) {
            mHandler.postDelayed(mReSendRunnable, RUNNABLE_DELAY_CHECK_TIME);
        }
    }

    public void unbindHandler() {
        mHandler = null;
    }

    private void printLog(String log) {
        if (mSocketEngine != null) {
            mSocketEngine.printLog(log);
        }
    }

    public void destroyOutSide() {
        if (!mDestroy) {
            mDestroy = true;
            printLog("[" + getTaskId() + "] destroyOutSide begin");
            releaseListener();
            clearRunnable();
            unbindHandler();
            printLog("[" + getTaskId() + "] destroyOutSide finish");
            mSocketEngine = null;
        }
    }

    @Override
    public void destroy() {
        if (!mDestroy) {
            mDestroy = true;
            printLog("[" + getTaskId() + "] begin destroy");
            releaseListener();
            clearRunnable();
            unbindHandler();
            if (mSocketEngine != null) {
                mSocketEngine.removeARQTask(this);
                printLog("[" + mTaskId + "] destroy finish");
                mSocketEngine = null;
            }
        }
    }

    public static String OptServerRespondId(JSONObject payload) {
        if (payload != null) {
            return payload.optString(SERVER_RESPOND_ID);
        }
        return null;
    }


    public static String OptRespondId(JSONObject payload) {
        if (payload != null) {
            return payload.optString(RESPOND_ID);
        }
        return null;
    }

    public static JSONObject CreateARQPayload(String respondId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(RESPOND_ID, respondId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
