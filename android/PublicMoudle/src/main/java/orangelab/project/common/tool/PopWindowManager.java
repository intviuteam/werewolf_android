package orangelab.project.common.tool;

import android.widget.PopupWindow;

import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;
import com.toolkit.action.TravelMap;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2018/5/4 下午7:03
 * Mystery
 */

public class PopWindowManager implements Destroyable {

    private Map<String, PopupWindow> mPopupWindowMap;

    public PopWindowManager() {
        mPopupWindowMap = new HashMap<>(0);
    }

    public void put(String key, PopupWindow popupWindow) {
        if (popupWindow == null) {
            return;
        }
        mPopupWindowMap.put(key, popupWindow);
    }

    public PopupWindow get(String key) {
        return mPopupWindowMap.get(key);
    }

    public boolean has(String key) {
        return mPopupWindowMap.containsKey(key);
    }

    public PopupWindow remove(String key) {
        return mPopupWindowMap.remove(key);
    }

    public void removeAndDismiss(String key) {
        PopupWindow popupWindow = remove(key);
        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
    }

    @Override
    public void destroy() {
        ToolKit.travelMap(mPopupWindowMap, (key, value) -> {
            if (value.isShowing()) {
                value.dismiss();
            }
        });
        mPopupWindowMap.clear();
    }
}
