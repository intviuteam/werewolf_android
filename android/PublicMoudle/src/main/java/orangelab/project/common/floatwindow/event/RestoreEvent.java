package orangelab.project.common.floatwindow.event;

/**
 * Created by lloydfinch on 26/12/2017.
 */

public class RestoreEvent {

    private boolean needHandOver;

    public RestoreEvent() {
        this(false);
    }

    public RestoreEvent(boolean needHandOver) {
        this.needHandOver = needHandOver;
    }

    public boolean isNeedHandOver() {
        return needHandOver;
    }
}
