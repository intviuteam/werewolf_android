package orangelab.project.common.event;

/**
 * game_werewolf
 * 2018/1/6 下午5:46
 * Mystery
 */

public class GlobalEvent {

    public static class VoiceDestroyEvent {

    }

    public static class VoiceDestroyCallBackEvent {

    }

    public static class GameDestroyEvent {

    }

    public static class GameDestroyCallBackEvent {

    }

    public static abstract class PrivateRoomInviteEvent {
        private String roomId;
        private String roomPassword;
        private String roomType;

        public PrivateRoomInviteEvent(String roomId, String roomPassword, String roomType) {
            this.roomId = roomId;
            this.roomPassword = roomPassword;
            this.roomType = roomType;
        }

        public String getRoomId() {
            return roomId;
        }

        public String getRoomPassword() {
            return roomPassword;
        }

        public String getRoomType() {
            return roomType;
        }
    }

    public static class PrivateRoomInviteForNormal extends PrivateRoomInviteEvent{

        public PrivateRoomInviteForNormal(String roomId, String roomPassword, String roomType) {
            super(roomId, roomPassword, roomType);
        }
    }

    public static class PrivateRoomInviteForVoiceRoom extends PrivateRoomInviteEvent {

        public PrivateRoomInviteForVoiceRoom(String roomId, String roomPassword, String roomType) {
            super(roomId, roomPassword, roomType);
        }
    }
}
