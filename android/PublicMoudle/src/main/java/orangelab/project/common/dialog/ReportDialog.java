package orangelab.project.common.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.R;

import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;

import com.networktoolkit.transport.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/8/21 下午5:39
 * Mystery
 */

public class ReportDialog extends SafeDialog implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private String toUserID;
    private String toUserName;
    private String mReportText = "";

    private TextView mReportName;

    private List<RadioButton> mRadioButtonList;

    private boolean mHasChooseWay = false;
    private String mReportType = "";
    private Button mConfirm;
    private Button mCancel;
    private EditText mReportEditText;


    public ReportDialog(@NonNull Context context, String reportUserId, String reportName) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.layout_report_dialog);
        this.toUserID = reportUserId;
        this.toUserName = reportName;
        this.mRadioButtonList = new ArrayList<>();
        mReportName = (TextView) findViewById(R.id.report_name);
        mReportName.setText(reportName);

        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_adv));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_escape));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_av));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_cheat));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_gangup));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_abuse));
        this.mRadioButtonList.add((RadioButton) findViewById(R.id.report_abusemic));
        this.mReportEditText = (EditText) findViewById(R.id.report_detail_msg);
        mConfirm = (Button) findViewById(R.id.confirm);
        mCancel = (Button) findViewById(R.id.cancel);
        mConfirm.setOnClickListener(this);
        mCancel.setOnClickListener(this);

        for (RadioButton radioButton : mRadioButtonList) {
            radioButton.setOnCheckedChangeListener(this);
        }
//        setCancelable(true);
//        setCanceledOnTouchOutside(true);
    }

    private void clearCheck() {
        for (RadioButton radioButton : mRadioButtonList) {
            radioButton.setChecked(false);
        }
    }

    private String getReportType(int id) {
        String result = "";
        if (id == R.id.report_adv) {
            result = Constant.REPORT_AD;
        } else if (id == R.id.report_escape) {
            result = Constant.REPORT_ESCAPE;
        } else if (id == R.id.report_av) {
            result = Constant.REPORT_SEX;
        } else if (id == R.id.report_cheat) {
            result = Constant.REPORT_CHEAT;
        } else if (id == R.id.report_gangup) {
            result = Constant.REPORT_GANG_UP;
        } else if (id == R.id.report_abuse) {
            result = Constant.REPORT_ABUSE;
        } else if (id == R.id.report_abusemic) {
            result = Constant.REPORT_ABUSE;
        }
//        switch (id) {
//            case R.id.report_adv:
//                result = Constant.REPORT_AD;
//                break;
//            case R.id.report_escape:
//                result = Constant.REPORT_ESCAPE;
//                break;
//            case R.id.report_av:
//                result = Constant.REPORT_SEX;
//                break;
//            case R.id.report_cheat:
//                result = Constant.REPORT_CHEAT;
//                break;
//            case R.id.report_gangup:
//                result = Constant.REPORT_GANG_UP;
//                break;
//            case R.id.report_abuse:
//                result = Constant.REPORT_ABUSE;
//                break;
//            case R.id.report_abusemic:
//                result = Constant.REPORT_ABUSE;
//                break;
//        }
        return result;
    }

    private void setChecked(int id, boolean isChecked) {
        clearCheck();
        for (RadioButton radioButton : mRadioButtonList) {
            if (radioButton.getId() == id) {
                radioButton.setChecked(isChecked);
                if (isChecked) {
                    mHasChooseWay = true;
                    mReportType = getReportType(id);
                }
            }
        }
    }

    @Override
    protected void release() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setChecked(buttonView.getId(), isChecked);
    }


    private void report(String type) {
        ApiManager.TipOff(toUserID, type, mReportEditText.getText().toString(), (s, e) -> runSafely(() -> {
            if (e == null) {
                ToastToolKit.showLong(MessageUtils.getString(R.string.game_report_success));
                dismiss();
            } else {
                ToastToolKit.showLong(MessageUtils.getString(R.string.game_report_fail));
                dismiss();
            }
        }));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.confirm) {
            if (mHasChooseWay) {
                report(mReportType);
            } else {
                runSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.report_please_choose_a_way)));
            }
        } else if (id == R.id.cancel) {
            dismiss();
        }
//        switch (v.getId()) {
//            case R.id.confirm: {
//                if (mHasChooseWay) {
//                    report(mReportType);
//                } else {
//                    runSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.report_please_choose_a_way)));
//                }
//            }
//            break;
//            case R.id.cancel:
//                dismiss();
//                break;
//        }
    }
}
