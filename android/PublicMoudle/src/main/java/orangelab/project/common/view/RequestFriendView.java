package orangelab.project.common.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.R;

import orangelab.project.common.component.BasicViewComponent;
import orangelab.project.game.view.IFriendRequestView;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.union.UnifiedBridgeHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;

import com.androidtoolkit.PicassoUtils;

import java.util.Set;


import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2017/8/22 下午2:16
 * Mystery
 */

public class RequestFriendView extends BasicViewComponent implements View.OnClickListener, IFriendRequestView {

    private View mRoot;

    private CircleImageView mCircleImageView;
    private TextView mTextView;
    private Button mApprove;
    private Button mRefuse;

    private String mUserId;
    private String mUserName;
    private String mUserIv;
    private Set<String> mMemo;

    private Runnable mDismissListener;

    public RequestFriendView(Context context, String userId, String userName, String userIv, Set<String> memo) {
        mRoot = View.inflate(context, R.layout.layout_add_friend_view, null);
        mCircleImageView = (CircleImageView) mRoot.findViewById(R.id.request_friend_iv);
        mTextView = (TextView) mRoot.findViewById(R.id.request_friend_msg);
        mApprove = (Button) mRoot.findViewById(R.id.approve);
        mRefuse = (Button) mRoot.findViewById(R.id.refuse);
        mApprove.setOnClickListener(this);
        mRefuse.setOnClickListener(this);
        this.mUserId = userId;
        this.mUserName = userName;
        this.mUserIv = userIv;
        this.mMemo = memo;
        PicassoUtils.loadImage(context, this.mUserIv, mCircleImageView, R.mipmap.default_head);
        this.mTextView.setText(MessageUtils.getString(R.string.in_game_add_friend_msg, Utils.filterUserName(userName, 9)));
    }

    @Override
    public int getHeight() {
        return ScreenUtils.dip2px(70);
    }

    @Override
    public int getWidth() {
        return ScreenUtils.dip2px(220);
    }

    @Override
    public int getX() {
        return (ScreenUtils.getCurrentScreenWidth() - getWidth()) / 2;
    }

    @Override
    public int getY() {
        return (int) (ScreenUtils.getCurrentScreenHeight() * 0.15f);
    }

    public View getView() {
        return mRoot;
    }

    public void setDismissListener(Runnable dismissListener) {
        mDismissListener = dismissListener;
    }

    public void dismiss() {
        postSafely(() -> {
            if (mDismissListener != null) {
                mDismissListener.run();
            }
            mRoot.setVisibility(View.GONE);
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.approve) {
            approve();
        } else if (id == R.id.refuse) {
            refuse();
        }
//        switch (v.getId()) {
//            case R.id.approve:
//                approve();
//                break;
//            case R.id.refuse:
//                refuse();
//                break;
//        }
    }

    private void putIntoMemo() {
        mMemo.add(mUserId);
    }

    private void approve() {
        ApiManager.AcceptFriend(mUserId, (s, e) -> {
            postSafely(() -> {
                if (e == null) {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.add_friend_success));
                    UnifiedBridgeHelper.SendAddFriendAction(mUserId);
                    dismiss();
                    putIntoMemo();
                } else {
                    ToastToolKit.showShort(e.getMessage());
                }
            });
        });
    }

    private void refuse() {
        ApiManager.RefuseFriend(mUserId, null);
        postSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.refuse_friend_success)));
        dismiss();
        putIntoMemo();
    }

    public String getUserId() {
        return mUserId;
    }
}
