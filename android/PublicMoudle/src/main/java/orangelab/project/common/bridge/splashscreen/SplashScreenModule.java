package orangelab.project.common.bridge.splashscreen;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.greenrobot.eventbus.EventBus;

import orangelab.project.common.event.ActivityEvent;

/**
 * SplashScreen
 * 启动屏
 */
public class SplashScreenModule extends ReactContextBaseJavaModule {
    public SplashScreenModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SplashScreen";
    }

    /**
     * 打开启动屏
     */
    @ReactMethod
    public void show() {
    }

    /**
     * 关闭启动屏
     */
    @ReactMethod
    public void hide() {
        EventBus.getDefault().post(new ActivityEvent.SplashDialogDismissEvent());
    }
}