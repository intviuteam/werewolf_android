package orangelab.project.common.model.action;

import com.toolkit.action.ActionToJson;
import com.toolkit.action.Keepable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/4/10 上午11:13
 * Mystery
 */

public class ClientActionSendPaperPlane implements Keepable, ActionToJson {

    public String type = "page_plane";
    public String message = "";
    public ClientActionSendPaperPlaneTo to;
    public ClientActionSendPaperItem from;

    @Override
    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        try {
            result = new JSONObject(GsonHelper.createJson(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static class ClientActionSendPaperPlaneTo implements Keepable {
        public boolean unseat_users = false;
        public ArrayList<ClientActionSendPaperItem> users;
    }

    public static class ClientActionSendPaperItem implements Keepable {
        public String id;
        public int position;
    }
}
