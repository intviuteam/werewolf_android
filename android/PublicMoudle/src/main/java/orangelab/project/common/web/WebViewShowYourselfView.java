package orangelab.project.common.web;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.common.share.ShareableView;

/**
 * game_werewolf
 * 2018/5/21 下午4:16
 * Mystery
 */
public class WebViewShowYourselfView extends ShareableView {

    private ImageView mImageView;
    private TextView mTextView;
    private View mBottomView;

    public WebViewShowYourselfView(@NonNull Context context) {
        this(context, null);
    }

    public WebViewShowYourselfView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WebViewShowYourselfView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.activity_web_showyourself_view, this);
        setLayoutParams(new ViewGroup.LayoutParams(540, 960));
        mImageView = (ImageView) view.findViewById(R.id.imageview);
        mTextView = (TextView) view.findViewById(R.id.text);
        mBottomView = view.findViewById(R.id.bottom_container);
    }

    public void updateType(String type) {
        if (TextUtils.equals(type, "voice_rank")) {
            mBottomView.setBackgroundResource(R.mipmap.ico_web_showyourself_1);
        } else {
            mBottomView.setBackgroundResource(R.mipmap.ico_web_showyourself_2);
        }
    }

    public void setImage(Bitmap image) {
        mImageView.setImageBitmap(image);
    }

    public void setText(String text, int color) {
        mTextView.setText(text);
        if (color > 0) {
            mTextView.setTextColor(color);
        }
    }
}
