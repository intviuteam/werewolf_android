package orangelab.project.common.family;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.R;

import orangelab.project.common.activity.LockAbleActivity;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.family.adapter.FamilyInfoMemberListAdapter;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.family.model.FamilyInfoApiResult;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.RecycleViewClickListener;
import orangelab.project.common.view.SpaceItemDecoration;

import com.androidtoolkit.PicassoUtils;

import com.androidtoolkit.view.ScreenUtils;

import orangelab.project.common.web.WebViewActivity;

import com.toolkit.action.ApiResult;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscription;

/**
 * game_werewolf
 * 2017/10/17 下午2:03
 * Mystery
 */

public class FamilyInfoActivity extends LockAbleActivity implements View.OnClickListener {

    private static final String TAG = "FamilyInfoActivity";

    private static final int Columns = 4;
    private static final int MAX_SHOW_MEMBER = 30;

    private ScrollView scrollView;

    private View mGoBack;
    private View mSetting;

    private TextView mFamilyName;
    private Button mFamilyLevel;
    private TextView mFamilyIntroduce;
    private CircleImageView mFamilyImage;
    private TextView mFamilyMembersText;
    private View mFamilyMembersArrow;
    private View mFamilyMembersMore;

    private GridView mFamilyMembersShortList;
    private FamilyInfoMemberListAdapter mFamilyInfoShortListAdapter;
    private RecycleViewClickListener mRecyclerViewClickListener;

    private String mFamilyId = "";
    private View mFamilyNewNotice;
    private View mFamilyNewNotice2;
    private FamilyInfoApiResult mFamilyInfoResult;

    private Button mFamilyApplyIn;
    private boolean hasApply = false;
    private boolean applying = false;
    private TextView mFamilyIdTextView;
    private ImageView mFamilyLevelIv;
    private TextView mFamilyLivingValue;

    public static void Launch(Context context, String familyId) {
        if (GlobalUserState.getGlobalState().isTourist()) {
            Toast.makeText(context, R.string.string_family_info_for_tourist, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(familyId)) {
            return;
        }
        Intent intent = new Intent(context, FamilyInfoActivity.class);
        intent.putExtra(FamilyConstant.FAMILY_ID, familyId);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        Utils.setWindowStatusBarColor(this, R.color.color_family_info_statusbar);
        setContentView(R.layout.layout_activity_family_info);
        initView();
//        executeActivityDrawFinish(() -> {
//            addUnLockViewRect(mGoBack);
//            requestAllLock(); //锁住所有事件
//
//        });
        initListener();
        moveToTop();
    }

    private void moveToTop() {
        scrollView.smoothScrollTo(0, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestFamilyInfoData();
    }

    private void initListener() {
        RxToolKit.Build(mActivityKey, FamilyEvent.QuitFamilyEvent.class).action(v -> finish()).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.TransferEvent.class).action(v -> finish()).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.RefreshFamilyInfoEvent.class).action(v -> runOnUiThreadSafely(() -> requestFamilyInfoData())).register();
    }

    private void initData() {
        mFamilyId = getIntent().getStringExtra(FamilyConstant.FAMILY_ID);
    }

    protected boolean canVerticalScroll(TextView editText) {
        int scrollRange = editText.getLayout().getHeight();
        int scrollExtent = editText.getHeight() - editText.getCompoundPaddingTop() - editText.getCompoundPaddingBottom();
        return !(scrollRange <= scrollExtent);
    }

    private void initView() {
        scrollView = (ScrollView) findViewById(R.id.id_family_scrollview);
        mGoBack = findViewById(R.id.id_family_info_back);
        mSetting = findViewById(R.id.id_family_info_setting);
        mSetting.setVisibility(View.GONE);
        mFamilyName = (TextView) findViewById(R.id.id_family_info_name);
        mFamilyLevel = (Button) findViewById(R.id.id_family_info_level);
        mFamilyIntroduce = (TextView) findViewById(R.id.id_family_info_introduce);
        mFamilyIntroduce.setMovementMethod(ScrollingMovementMethod.getInstance());
        mFamilyIntroduce.setOnTouchListener((v, event) -> {
            if (canVerticalScroll(mFamilyIntroduce)) {
                if (event.getAction() == MotionEvent.ACTION_DOWN
                        || event.getAction() == MotionEvent.ACTION_MOVE) {
                    //按下或滑动时请求父节点不拦截子节点
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //抬起时请求父节点拦截子节点
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });
        mFamilyImage = (CircleImageView) findViewById(R.id.id_family_info_image);
        mFamilyImage.setBorderWidth(ScreenUtils.dip2px(2));
        mFamilyIdTextView = (TextView) findViewById(R.id.id_family_id);
        mFamilyImage.setBorderColorResource(R.color.color_family_info_image_stroke);
        mFamilyMembersText = (TextView) findViewById(R.id.id_family_info_members);
        mFamilyMembersShortList = (GridView) findViewById(R.id.id_family_info_members_short_list);
        mFamilyNewNotice = findViewById(R.id.id_family_info_new_notice);
        mFamilyMembersArrow = findViewById(R.id.id_family_info_members_arrow);
        mFamilyMembersMore = findViewById(R.id.id_family_info_want_more);
        mFamilyNewNotice2 = findViewById(R.id.id_family_info_new_notice2);
        mFamilyApplyIn = (Button) findViewById(R.id.id_family_apply_in);

        mFamilyLevelIv = (ImageView) findViewById(R.id.id_family_info_level_iv);
        mFamilyLivingValue = (TextView) findViewById(R.id.id_family_info_living_value);
        mFamilyLivingValue.setVisibility(View.GONE);

        mFamilyApplyIn.setOnClickListener(this);
        mFamilyApplyIn.setVisibility(View.GONE);
        mFamilyNewNotice.setVisibility(View.GONE);
        mFamilyNewNotice2.setVisibility(View.GONE);
        mFamilyMembersArrow.setVisibility(View.GONE);
        mFamilyMembersMore.setVisibility(View.GONE);
        mFamilyInfoShortListAdapter = new FamilyInfoMemberListAdapter(MAX_SHOW_MEMBER, Columns);
        WrapContentLinearLayoutManager linearLayoutManager = new WrapContentLinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFamilyMembersShortList.setAdapter(mFamilyInfoShortListAdapter);
        mFamilyMembersShortList.setOnItemClickListener((parent, view, position, id) -> {
            String selectId = mFamilyInfoShortListAdapter.getItem(position).user_id;
            PersonalInfoActivity.Launch(FamilyInfoActivity.this, selectId, false, false);
        });
        mRecyclerViewClickListener = new RecycleViewClickListener(this);
        if (FamilyMemo.IsMyFamily(mFamilyId)) {
            mRecyclerViewClickListener.setOnClickListener(v -> {
                try {
                    FamilyMembersActivity.LaunchForMemberDetails(this, mFamilyInfoResult.group._id);
                } catch (Exception e) {
                    e.printStackTrace();
                    // "mFamilyInfoResult.group._id" may be null
                }
            });
            mFamilyMembersShortList.setOnTouchListener(mRecyclerViewClickListener);
        }
        mGoBack.setOnClickListener(this);
        mSetting.setOnClickListener(this);
    }

    private void requestFamilyInfoData() {
        mFamilyNewNotice.setVisibility(View.GONE);
        mFamilyNewNotice2.setVisibility(View.GONE);
        FamilyApiManager.GetFamilyGroupInfo(mFamilyId, (familyInfoResult, e) -> {
            runOnUiThreadSafely(() -> {
                if (e != null) {
                    RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    return;
                } else {
                    if (familyInfoResult == null) {
                        return;
                    }
                    mFamilyInfoResult = familyInfoResult;
                    FamilyMemo.UpdateMemo(mFamilyInfoResult);
                    handleFamilyInfoResult();
                    handleApplyFamilyButton();
                }
                moveToTop();
            });
        });
    }

    private void handleApplyFamilyButton() {
        if (FamilyMemo.iMemo.isHasJoinFamily()) {
//
        } else {
            mFamilyApplyIn.setVisibility(View.VISIBLE);
        }
    }

    private void handleNewNotice(View mFamilyNewNotice, Integer requireCount) {
        if (requireCount == null) {
            mFamilyNewNotice.setVisibility(View.GONE);
            return;
        }
        if (requireCount > 0) {
            mFamilyNewNotice.setVisibility(View.VISIBLE);
        } else {
            mFamilyNewNotice.setVisibility(View.GONE);
        }
    }

    private void handleFamilyInfoResult() {
        handleTitle();
        handleFamilyDetail();
        handleFamilyMember();
    }

    private void handleFamilyMember() {
        if (mFamilyInfoResult.group.members != null) {
            List<FamilyInfoApiResult.FamilyInfoMember> groupMembers = mFamilyInfoResult.group.members;
            mFamilyInfoShortListAdapter.updateData(groupMembers);
            Utils.customGridViewAndChildHeightAndWidth(mFamilyMembersShortList, 4, ScreenUtils.dip2px(73), ScreenUtils.dip2px(73));

            int targetSize = groupMembers.size();
            /**
             * 这里要做一个根据成员数量做一个判断
             */
            View needNewView = null;
            if (targetSize <= Columns) {
                mFamilyMembersArrow.setVisibility(View.VISIBLE);
                mFamilyMembersMore.setVisibility(View.GONE);
            } else if (targetSize > Columns && targetSize < MAX_SHOW_MEMBER) {
                mFamilyMembersArrow.setVisibility(View.VISIBLE);
                mFamilyMembersMore.setVisibility(View.GONE);

            } else {
                mFamilyMembersArrow.setVisibility(View.GONE);
                mFamilyMembersMore.setVisibility(View.VISIBLE);
            }

            if (!FamilyMemo.IsMyFamily(mFamilyId)) {
                mFamilyMembersArrow.setVisibility(View.GONE);
            }

            if (targetSize == MAX_SHOW_MEMBER || targetSize <= Columns) {
                needNewView = mFamilyNewNotice;
            } else {
                if (targetSize % Columns == 0) {
                    needNewView = mFamilyNewNotice;
                } else {
                    needNewView = mFamilyNewNotice2;
                }
            }
            handleNewNotice(needNewView, mFamilyInfoResult.group.require_count);
        }

        try {
            if (mFamilyInfoResult.group.gid == null) {
                mFamilyIdTextView.setVisibility(View.GONE);
            } else {
                mFamilyIdTextView.setVisibility(View.VISIBLE);
                mFamilyIdTextView.setText(MessageUtils.getString(R.string.string_user_info_uid, mFamilyInfoResult.group.gid));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mFamilyInfoResult.group.member_count > 0) {
            int curCount = mFamilyInfoResult.group.member_count;
            int maxCount = mFamilyInfoResult.group.max_members;
            mFamilyMembersText.setText(MessageUtils.getString(R.string.string_family_detail_members, Integer.toString(curCount), Integer.toString(maxCount)));
            mFamilyMembersText.setVisibility(View.VISIBLE);
        }
    }

    private void handleFamilyDetail() {
        FamilyInfoApiResult result = mFamilyInfoResult;

        if (!TextUtils.isEmpty(result.group.image)) {
            PicassoUtils.loadImage(this, result.group.image, mFamilyImage, R.mipmap.ico_family_default);
        }

        if (!TextUtils.isEmpty(result.group.name)) {
            mFamilyName.setVisibility(View.VISIBLE);
            mFamilyName.setText(result.group.name);
        }

        FamilyTools.FillFamilyLevelButton(result.group, mFamilyLevel);
        mFamilyLevel.setOnClickListener(v -> {
            WebViewActivity.LoadFamilyGrade(this);
        });

        if (!TextUtils.isEmpty(result.group.desc)) {
            mFamilyIntroduce.setText(result.group.desc);
        }

        FamilyTools.FillFamilyBadge(this, result, mFamilyLevelIv);

        Utils.runSafely(() -> {
            mFamilyLivingValue.setText(MessageUtils.getString(R.string.string_family_living_value, Integer.toString(result.group.active_point)));
            mFamilyLivingValue.setVisibility(View.VISIBLE);
        });

    }

    private void handleTitle() {
        if (FamilyMemo.IsMyFamily(mFamilyInfoResult.group._id)) {
            mSetting.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finish() {
        super.finish();
        RxToolKit.UnRegister(mActivityKey);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_family_info_back) {
            finish();
        } else if (id == R.id.id_family_info_setting) {
            FamilySettingActivity.Launch(this, mFamilyId, mFamilyInfoResult.group.name, mFamilyInfoResult.group.short_name, mFamilyInfoResult.group.desc, mFamilyInfoResult.group.image);
        } else if (id == R.id.id_family_apply_in) {
            if (applying) {
                return;
            }
            applying = true;
            if (hasApply) {
                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_apply_in)).run();
                return;
            }
            FamilyApiManager.FamilyApplyIn(mFamilyId, new ApiResult<Integer>() {
                @Override
                public void onResult(Integer integer, Exception e) {
                    runOnUiThreadSafely(() -> {
                        if (e != null) {
                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                        } else {
                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_apply_in)).run();
                            hasApply = true;
                        }
                        applying = false;
                    });
                }
            });
        } else if (id == R.id.id_family_info_want_more) {
            try {
                FamilyMembersActivity.LaunchForMemberDetails(this, mFamilyInfoResult.group._id);
            } catch (Exception e) {
                e.printStackTrace();
                // "mFamilyInfoResult.group._id" may be null
            }
        }
//        switch (v.getId()) {
//            case R.id.id_family_info_back:
//                finish();
//                break;
//            case R.id.id_family_info_setting: {
//                FamilySettingActivity.Launch(this, mFamilyId, mFamilyInfoResult.group.name, mFamilyInfoResult.group.short_name, mFamilyInfoResult.group.desc, mFamilyInfoResult.group.image);
//            }
//            break;
//            case R.id.id_family_apply_in: {
//                if (applying) {
//                    return;
//                }
//                applying = true;
//                if (hasApply) {
//                    RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_apply_in)).run();
//                    return;
//                }
//                FamilyApiManager.FamilyApplyIn(mFamilyId, new ApiResult<Integer>() {
//                    @Override
//                    public void onResult(Integer integer, Exception e) {
//                        runOnUiThreadSafely(() -> {
//                            if (e != null) {
//                                RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                            } else {
//                                RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_apply_in)).run();
//                                hasApply = true;
//                            }
//                            applying = false;
//                        });
//                    }
//                });
//            }
//            break;
//        }
    }
}
