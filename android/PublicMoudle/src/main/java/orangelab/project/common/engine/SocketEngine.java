package orangelab.project.common.engine;


import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.engine.task.SocketEngineARQTask;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;

import com.networktoolkit.transport.GlobalDecorateManager;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.engine.task.SocketCallBack;
import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.utils.MessageCache;

import com.androidtoolkit.PLog;

import orangelab.project.common.utils.Utils;

import com.toolkit.ToolKit;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import cn.intviu.analytics.IUmengEventDefines;
import cn.intviu.service.IntviuApplication;

import io.socket.engineio.client.Socket;
import io.socket.engineio.client.Transport;
import orangelab.project.game.event.GameEvent;

/**
 * Author:1727
 * 2017/3/2
 */

public class SocketEngine {


    private static final String RECONNECT = "reconnect";
    private static final String CONNECT_INFO = "connect_info";
    private static final String CONNECT_ID = "connect_id";

    private final String TAG;

    private static final String RESPONSE = "response";
    private static final String MESSAGE = "message";
    private static final String CONFIRM = "confirm";
    private static final String RECEIVED = "received";

    public static final String WEREWOLF = "werewolf";
    public static final String AUDIO = "audio";
    public static final String MINI_GAME = "mini_game";
    public static final String GLOBAL = "global";
    public static final String PRIVACY = "privacy";
    public static final String SPY_ROOM = "undercover";
    public static final String PAIR = "pair"; //游戏大厅


    private static final JSONObject EMPTY_JSON = new JSONObject();

    private static final long OPEN_TIME_OUT = 8000;

    private GameSocketIO mSocketIO;

    private String mSocketKey;

    private String mSocketIntent = WEREWOLF;
    private String mSocketConnectId = "";
    private Object mSocketConnectIdLock = new Object();
    private boolean mSocketNeedReconnect = false;

    private SocketEngineOpenListener mSocketEngineOpenListener;
    private SocketEngineLifeCycleListener mSocketEngineLifeCycleController;

    private HandlerThread mSocketThread = null;
    private Handler mSocketThreadHandler = null;

    private boolean isDisConnected = false;
    private boolean isOpen = false;
    private boolean isSocketError = false;
    private boolean isBootFinish = false;

    private boolean isOutSideThread = false; //是否采用外部提供的线程

    private Object mSocketStatesLock = new Object();

    private SocketEngineTaskManager mEngineTaskManager;
    private Map<String, SocketEngineARQTask> mARQTasks;
    private List<String> mARQTaskActionList;

    private int mSocketReconnectCount = 0;
    private int mSocketMaxReconnectCount = 12;

    private Runnable mSocketReconnectRunnable = new Runnable() {
        @Override
        public void run() {
            mSocketIO.openSocket();
            mSocketReconnectCount++;
        }
    };
    private Runnable mHeartBeatRunnable = () ->
    {
        nativeRequestMessage(Constant.PING, null, null, null);
        postHeartEvent();
    };

    private Runnable mSocketOpenCheckRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isSocketOpen()) {
                if (mSocketEngineOpenListener != null) {
                    mSocketEngineOpenListener.onError();
                    mSocketEngineOpenListener = null;
                }
                if (mSocketEngineLifeCycleController != null) {
                    mSocketEngineLifeCycleController.onSocketOpenError();
                }
            }
        }
    };

    private MessageCache<Object> messageCache;

    Map<String, String> map_value = new HashMap<String, String>();

    private String mSocketMsgToken = Constant.EMPTY;
    private Object mSocketMsgTokenLock = new Object();
    private long mServerTimeDelta = 0;

    private static final int MAX_RANDOM = 99;
    private static final int MIN_RANDOM = 10;
    private final Random mRandom;

    private SocketEngineObserver mSocketEngineObserver;

    public SocketEngine(String tag, String socketType) {
        TAG = tag;
        mSocketKey = TAG + hashCode();
        mRandom = new Random();
        if (TextUtils.isEmpty(socketType)) {
            throw new IllegalArgumentException("Socket Type Must Be Not Null!!!!");
        }
        mSocketIntent = socketType;
        mSocketKey = TAG + mSocketIntent + Integer.toString(hashCode());
        messageCache = new MessageCache<>();
        mARQTasks = new HashMap<>();
        mARQTaskActionList = new ArrayList<>();
        startSocketThread();
        mEngineTaskManager = new SocketEngineTaskManager(TAG, mSocketIntent, mSocketThreadHandler);
        isOutSideThread = false;
    }

    public SocketEngine(String tag, String socketType, Handler outHandler, HandlerThread outHandlerThread) {
        TAG = tag;
        mSocketKey = TAG + hashCode();
        mRandom = new Random();
        if (TextUtils.isEmpty(socketType)) {
            throw new IllegalArgumentException("Socket Type Must Be Not Null!!!!");
        }
        if (outHandler == null || outHandlerThread == null) {
            throw new IllegalArgumentException("Socket Thread or Handler Must Be Not Null!!!!");
        }
        mSocketIntent = socketType;
        mSocketKey = TAG + mSocketIntent + Integer.toString(hashCode());
        messageCache = new MessageCache<>();
        mARQTasks = new HashMap<>();
        mARQTaskActionList = new ArrayList<>();
        mSocketThreadHandler = outHandler;
        mSocketThread = outHandlerThread;
        mEngineTaskManager = new SocketEngineTaskManager(TAG, mSocketIntent, mSocketThreadHandler);
        isOutSideThread = true;
    }

    public void registerARQTaskActions(List<String> actions) {
        mARQTaskActionList.addAll(actions);
    }

    public String getSocketType() {
        return mSocketIntent;
    }

    public void setSocketEngineObserver(SocketEngineObserver observer) {
        synchronized (mSocketStatesLock) {
            this.mSocketEngineObserver = observer;
        }
    }

    public long getServerTime() {
        return System.currentTimeMillis() + mServerTimeDelta;
    }

    private void reportSocketError() {
        try {
            map_value.put(IUmengEventDefines.EVENT_STATUS, IUmengEventDefines.EVENT_GAME_SOCKET_ERROR);
            IntviuApplication.getInstance().getReporter().onEvent(IUmengEventDefines.EVENT_GAME_SOCKET, map_value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reportSocketOpen() {
        try {
            map_value.put(IUmengEventDefines.EVENT_STATUS, IUmengEventDefines.EVENT_GAME_SOCKET_OPEN);
            IntviuApplication.getInstance().getReporter().onEvent(IUmengEventDefines.EVENT_GAME_SOCKET, map_value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reportSocketClose() {
        try {
            map_value.put(IUmengEventDefines.EVENT_STATUS, IUmengEventDefines.EVENT_GAME_SOCKET_CLOSE);
            IntviuApplication.getInstance().getReporter().onEvent(IUmengEventDefines.EVENT_GAME_SOCKET, map_value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reportSocketExit() {
        try {
            map_value.put(IUmengEventDefines.EVENT_STATUS, IUmengEventDefines.EVENT_GAME_SOCKET_EXIT);
            IntviuApplication.getInstance().getReporter().onEvent(IUmengEventDefines.EVENT_GAME_SOCKET, map_value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startPollMessage() {
        postRunnableIntoSocketThread(() -> {
            setBootFinish(true);
            postMessageCache();
        });
    }

    private String createQueryData(String roomType, String roomId) {
        StringBuilder sb = new StringBuilder();
        sb.append("room_id=" + roomId);
        sb.append("&room_type=" + roomType);
        return sb.toString();
    }

    public void setSocketUrl(String url, String roomType, String roomId, String path) {
        if (mSocketIO == null) {
            mSocketIO = new GameSocketIO(url, path, createQueryData(roomType, roomId));
        }
    }

    public void setSocketUrl(String url, String roomType, String roomId) {
        setSocketUrl(url, roomType, roomId, GameSocketIO.GAME_SOCKET_WEREWOLF_PATH);
    }

    public void setSocketEngineOpenListener(SocketEngineOpenListener listener) {
        this.mSocketEngineOpenListener = listener;
    }

    public void setSocketEngineLifeCycleListener(SocketEngineLifeCycleListener socketEngineLifeCycle) {
        this.mSocketEngineLifeCycleController = socketEngineLifeCycle;
    }

    public void openSocketEngine() {
        if (mSocketIO != null) {
            mSocketIO.registerListener(Socket.EVENT_OPEN, args -> postRunnableIntoSocketThread(() -> {
                try {
                    printLog("Socket.EVENT_OPEN");
                    setSocketOpen(true);

                    reportSocketOpen();

                    startHeart();

                    if (mSocketEngineLifeCycleController != null) {
                        if (mSocketEngineLifeCycleController.onSocketOpen()) {
                            return;
                        }
                    }

                    if (getSocketNeedReconnect()) {
                        if (!TextUtils.isEmpty(getConnectId())) {
                            requestReconnect(getConnectId());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
            mSocketIO.registerListener(Socket.EVENT_MESSAGE, args -> {
                postRunnableIntoSocketThread(() -> {
                    String data = args.length > 0 ? args[0].toString() : null;
                    analyzeResultMessage(data);
                });
            });
            mSocketIO.registerListener(Socket.EVENT_CLOSE, args -> postRunnableIntoSocketThread(() -> {
                try {
                    printLog("Socket.EVENT_CLOSE");
                    reportSocketClose();

                    if (!isDisConnected) {

                        stopHeart();

                        setSocketError(true);

                        if (mSocketEngineOpenListener != null) {
                            mSocketEngineOpenListener.onError();
                            mSocketEngineOpenListener = null;
                        }
                        if (mSocketEngineObserver != null) {
                            mSocketEngineObserver.onSocketDisConnect();
                        }
                        addOrPostEvent(new SocketEvent.SocketConnectStateEvent(mSocketIntent, SocketEvent.SOCKET_DISCONNECT));
                        setSocketOpen(false);

                        if (mSocketEngineLifeCycleController != null) {
                            if (mSocketEngineLifeCycleController.onSocketClose()) {
                                return;
                            }
                        }

                        requestSocketNeedReconnect(true);
                        executeReconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
            mSocketIO.registerListener(Socket.EVENT_ERROR, args -> postRunnableIntoSocketThread(() -> {
                try {
                    printLog("Socket.EVENT_ERROR");
                    reportSocketError();
                    if (!isDisConnected) {

                        stopHeart();

                        setSocketError(true);

                        if (mSocketEngineOpenListener != null) {
                            mSocketEngineOpenListener.onError();
                            mSocketEngineOpenListener = null;
                        }
                        if (mSocketEngineObserver != null) {
                            mSocketEngineObserver.onSocketDisConnect();
                        }
                        addOrPostEvent(new SocketEvent.SocketConnectStateEvent(mSocketIntent, SocketEvent.SOCKET_DISCONNECT));

                        setSocketOpen(false);

                        if (mSocketEngineLifeCycleController != null) {
                            if (mSocketEngineLifeCycleController.onSocketError()) {
                                return;
                            }
                        }

                        requestSocketNeedReconnect(true);
                        executeReconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
            mSocketIO.registerListener(Socket.EVENT_TRANSPORT, args -> Utils.runSafely(() -> {
                /**
                 * Header里添加数据
                 */
                Transport transport = (Transport) args[0];
                transport.on(Transport.EVENT_REQUEST_HEADERS, args1 -> Utils.runSafely(() -> {
                    Map<String, List<String>> headers = (Map<String, List<String>>) args1[0];

                    if (GlobalDecorateManager.INSTANCE.getHeaderDecorate() != null) {
                        GlobalDecorateManager.INSTANCE.getHeaderDecorate().decorateSocketHeader(headers);
                    }
                    printLog("openSocketEngine: header " + headers);
                }));
            }));
            mSocketIO.openSocket();
            postRunnableIntoSocketThread(mSocketOpenCheckRunnable, OPEN_TIME_OUT);
        }
    }

    public void setSocketMaxReconnectCount(int socketMaxReconnectCount) {
        mSocketMaxReconnectCount = socketMaxReconnectCount;
    }

    public void setSocketMsgToken(String token) {
        synchronized (mSocketMsgTokenLock) {
            this.mSocketMsgToken = token;
        }
    }

    public String getSocketARQRespondId(String msgId) {
        return msgId + "_" + (mRandom.nextInt(MAX_RANDOM) + MIN_RANDOM);
    }

    public String getSocketMsgId() {
        return Utils.createMsgId();
    }

    private String getSocketMsgToken() {
        String result = Constant.EMPTY;
        synchronized (mSocketMsgTokenLock) {
            result = this.mSocketMsgToken;
        }
        return result;
    }

    private void executeReconnect() {
        if (mSocketReconnectCount == Integer.MAX_VALUE) {
            /**
             * 代表无限重连
             */
        } else {
            /**
             * 代表非无限重连
             */
            if (mSocketReconnectCount == mSocketMaxReconnectCount) {
                cancelReconnect();
                if (mSocketEngineObserver != null) {
                    mSocketEngineObserver.onSocketReConnectFailed(-1, "");
                }
                addOrPostEvent(new SocketEvent.ReConnectedFailedEvent(mSocketIntent, 0));
                mSocketReconnectCount = 0;
                return;
            }
        }

        cancelReconnect();

        if (getSocketNeedReconnect()) {
            postRunnableIntoSocketThread(mSocketReconnectRunnable, 5000);
        }
    }

    private void cancelReconnect() {
        removeRunnableFromSocketThread(mSocketReconnectRunnable);
    }

    private void sendMessageInternal(String message) {
        if (mSocketIO != null) {
            RxToolKit.Emit(new GameEvent.SendMessageEvent("发送的数据\n" + message));
            printLog("Send Msg: " + message);
            mSocketIO.sendMessage(message);
        }
    }

    public void sendMessageActionMsg(String message) {
        sendMessageInternal(decorateSendMessage(MESSAGE, message));
    }

    private String decorateSendMessage(String action, String msg) {
        StringBuilder sb = new StringBuilder();
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(action);
        jsonArray.put(msg);
        if (!TextUtils.isEmpty(getSocketMsgToken())) {
            jsonArray.put(getSocketMsgToken());
        }
        sb.append("2");
        sb.append(jsonArray.toString());
        return sb.toString();
    }

    private String trimReceiveMsg(String data) {
        while (true) {
            if (data.length() > 0) {
                char item = data.charAt(0);
                if (item >= '0' && item < '9') {
                    data = data.substring(1);
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        return data;
    }

    private void updateDelta(long serverTime) {
        mServerTimeDelta = serverTime - System.currentTimeMillis();
    }

    private void analyzeResultMessage(String data) {
        printLog("Receive Msg :" + data);
        if (data != null) {
            data = trimReceiveMsg(data);
            try {
                if (TextUtils.isEmpty(data)) {
                    printLog("receive Server Msg is Empty");
                    return;
                }
                JSONArray jsonObject = new JSONArray(data);
                final String action = (String) jsonObject.get(0);
                final String resultData = (String) jsonObject.get(1);
                if (jsonObject.length() > 2) {
                    String token = (String) jsonObject.get(2);
                    if (!TextUtils.isEmpty(token)) {
                        setSocketMsgToken(token);
                    }
                }
                try {
                    if (action.equals(RESPONSE)) {

                        ServerResponseEvent event = new ServerResponseEvent(mSocketIntent, new JSONObject(resultData));
                        mEngineTaskManager.handleTask(event);
                        updateDelta(event.getMsgTime());

                        String respondId = SocketEngineARQTask.OptRespondId(event.payload);
                        if (!TextUtils.isEmpty(respondId)) {
                            sendMessageActionMsg(SocketEngineARQTask.MSG_RESPOND, SocketEngineARQTask.CreateARQPayload(respondId), Utils.createMsgId());
                        } else {

                        }
                        if (mSocketEngineObserver != null) {
                            mSocketEngineObserver.onServerResponseEvent(event);
                        }
                        postServerResponseEvent(event);
                    } else {
                        if (action.equals(MESSAGE)) {

                            ServerMessageEvent event = new ServerMessageEvent(mSocketIntent, new JSONObject(resultData));
                            mEngineTaskManager.handleTask(event);
                            updateDelta(event.getMsgTime());

                            String respondId = SocketEngineARQTask.OptRespondId(event.payload);
                            if (!TextUtils.isEmpty(respondId)) {
                                sendMessageActionMsg(SocketEngineARQTask.MSG_RESPOND, SocketEngineARQTask.CreateARQPayload(respondId), Utils.createMsgId());
                            } else {

                            }

                            if (event.type.equals(CONNECT_INFO)) {
                                if (TextUtils.isEmpty(getConnectId())) {
                                    String id = event.payload.optString(CONNECT_ID);
                                    setSocketConnectId(id);
                                }

                                if (mSocketEngineOpenListener != null) {
                                    mSocketEngineOpenListener.onOpen();
                                    mSocketEngineOpenListener = null;
                                }

                                if (mSocketEngineLifeCycleController != null) {
                                    mSocketEngineLifeCycleController.onSocketConnectInfo();
                                }

                                setSocketError(false);

                                return;
                            }
                            if (!isBootFinish()) {
                                addMessageIntoCache(event);
                            } else {
                                if (mSocketEngineObserver != null) {
                                    mSocketEngineObserver.onServerMessageEvent(event);
                                }
                                postServerMessageEvent(event);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

        } else {
            printLog("receive data is null");
        }
    }

    private void requestReconnect(String connectIdString) {
        /**
         * 删除上次任务
         */
        mEngineTaskManager.endTaskForce(RECONNECT);
        printLog("requestReconnect " + connectIdString);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(CONNECT_ID, connectIdString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /**
         * 加入新的任务
         */

        mEngineTaskManager.addTask(
                RECONNECT,
                result -> {
                    /**
                     * 重新连接成功
                     */
                    addOrPostEvent(new SocketEvent.SocketConnectStateEvent(mSocketIntent, SocketEvent.SOCKET_CONNECT));
                    requestSocketNeedReconnect(false);
                    mSocketReconnectCount = 0;
                    setSocketError(false);

                    /**
                     * 更新connectId
                     */
                    try {
                        String id = new JSONObject(result).optString(CONNECT_ID);
                        if (!TextUtils.isEmpty(id)) {
                            setSocketConnectId(id);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (mSocketEngineObserver != null) {
                        mSocketEngineObserver.onSocketReConnectSuccess();
                        mSocketEngineObserver.onSocketConnect();
                    }

                    addOrPostEvent(new SocketEvent.SocketConnectStateEvent(mSocketIntent, SocketEvent.SOCKET_CONNECT));
                    requestSocketNeedReconnect(false);
                    mSocketReconnectCount = 0;
                    setSocketError(false);
                    printLog("reconnect Success");
                },
                result -> {
                    if (mSocketEngineObserver != null) {
                        mSocketEngineObserver.onSocketReConnectFailed(-1, "");
                    }
                    addOrPostEvent(new SocketEvent.ReConnectedFailedEvent(mSocketIntent, -1));
                    printLog("reconnect Failed");
                });
        printLog("reconnect Start");
        if (mSocketEngineObserver != null) {
            mSocketEngineObserver.onSocketReConnectStart();
        }
        sendMessageActionMsg(RECONNECT, jsonObject, Utils.createMsgId());

    }

    private void setSocketConnectId(String socketConnectId) {
        synchronized (mSocketConnectIdLock) {
            this.mSocketConnectId = socketConnectId;
            printLog("setSocketConnectId " + socketConnectId);
        }
    }

    private String getConnectId() {
        String result = "";
        synchronized (mSocketConnectIdLock) {
            result = this.mSocketConnectId;
            printLog("getConnectId " + result);
        }
        return result;
    }

    private void requestSocketNeedReconnect(boolean flag) {
        synchronized (mSocketConnectIdLock) {
            this.mSocketNeedReconnect = flag;
            printLog("requestSocketNeedReconnect " + flag);
        }
    }

    private boolean getSocketNeedReconnect() {
        boolean result = false;
        synchronized (mSocketConnectIdLock) {
            result = this.mSocketNeedReconnect;
            printLog("getSocketNeedReconnect " + result);
        }
        return result;
    }

    public void releaseDelay() {
        printLog("releaseDelay: ");
        postRunnableIntoSocketThread(() -> {
            release();
        }, 1000);
    }

    public void release() {
        postRunnableIntoSocketThread(() -> {
            releaseInner();
        });
    }

    private void releaseInner() {
        printLog("releaseInner: begin release");

        reportSocketExit();

        stopHeart();

        if (mSocketIO != null) {
            mSocketIO.closeSocket();
            setSocketDisconnect(true);
            setSocketOpen(false);
            mSocketIO = null;
        }

        if (messageCache != null) {
            clearMessageCache();
            messageCache = null;
        }

        mEngineTaskManager.destroy();

        if (mARQTasks != null) {
            clearARQTask();
            mARQTasks = null;
        }

        mARQTaskActionList.clear();

        if (mSocketEngineLifeCycleController != null) {
            mSocketEngineLifeCycleController = null;
        }

        if (mSocketEngineOpenListener != null) {
            mSocketEngineOpenListener = null;
        }

        RxToolKit.UnRegister(mSocketKey);

        printLog("releaseInner: socket release finish");

        stopSocketThread();

        printLog("releaseInner: socket Thread stopped");

        if (mSocketEngineObserver != null) {
            mSocketEngineObserver.onSocketRelease();
            setSocketEngineObserver(null);
        }
        printLog("report finish");
    }

    public void sendMessageActionMsg(String type, JSONObject payload, String msgId) {
        if (mARQTaskActionList.contains(type)) {
            ReportEventUtils.reportTotalCount();
            /**
             * 需要ARQTask
             */
            SocketEngineARQTask arqTask = new SocketEngineARQTask(this, type, payload, msgId);
            /**
             * 替换一下respondId
             */
            arqTask.replaceRespondId();
            arqTask.bindHandler(mSocketThreadHandler);
            arqTask.startCheck();
            addARQTask(arqTask);
        }
        sendMessageActionMsg(Utils.createSendMessageFromJsonToString(type, payload, msgId));
    }

    private String getCareType(String orgType) {
        if (TextUtils.equals(mSocketIntent, WEREWOLF) || TextUtils.equals(mSocketIntent, AUDIO)) {
            return orgType;
        }
        if (TextUtils.equals(mSocketIntent, MINI_GAME) || TextUtils.equals(mSocketIntent, GLOBAL)) {
            return orgType + "_server";
        }
        return orgType;
    }

    private void nativeRequestMessageInner(
            final String type,
            JSONObject json,
            final SocketCallBack success,
            final SocketCallBack failed
    ) {
        try {
            if (TextUtils.equals(type, Constant.LEAVE)) {
                /**
                 * leave 直接 认为断开
                 */
                setSocketDisconnect(true);
            }
            /**
             * 如果 socket处于断线状态 直接执行failed
             */
            if (isSocketError) {
                if (failed != null) {
                    failed.invoke(MessageUtils.getString(R.string.network_is_unavailable));
                }
                return;
            }
            /**
             * 如果传入参数为空则直接默认赋值
             */
            String msgId = Utils.createMsgId();
            if (json == null) {
                json = EMPTY_JSON;
            }
            if (success == null && failed == null) {
                /**
                 * 如果不传入回调则直接发送数据
                 */
                sendMessageActionMsg(type, json, msgId);
            } else {
                if (TextUtils.equals(type, Constant.SOCKET_SEND_GIFT) || TextUtils.equals(type, Constant.SOCKET_SEND_CARD)) {
                    mEngineTaskManager.addTask(getCareType(type), success, failed, OPEN_TIME_OUT, msgId);
                    sendMessageActionMsg(type, json, msgId);
                    return;
                }
                if (TextUtils.equals(type, Constant.TYPE_ENTER)) {
                    mEngineTaskManager.addTask(getCareType(type), success, failed, OPEN_TIME_OUT);
                    sendMessageActionMsg(type, json, msgId);
                    return;
                }
                mEngineTaskManager.addTask(getCareType(type), success, failed);
                sendMessageActionMsg(type, json, msgId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void nativeRequestMessage(
            final String type,
            JSONObject json,
            final SocketCallBack success,
            final SocketCallBack failed
    ) {
        postRunnableIntoSocketThread(() -> {
            nativeRequestMessageInner(type, json, success, failed);
        });
    }

    private void startSocketThread() {
        mSocketThread = new HandlerThread(TAG + "_WorkerThread");
        mSocketThread.start();
        mSocketThreadHandler = new Handler(mSocketThread.getLooper());
    }

    private void postRunnableIntoSocketThread(Runnable runnable) {
        postRunnableIntoSocketThread(runnable, 0);
    }

    private void postRunnableIntoSocketThread(Runnable runnable, long delay) {
        try {
            if (mSocketThreadHandler != null && mSocketThread != null && mSocketThread.isAlive()) {
                mSocketThreadHandler.postDelayed(runnable, delay);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeRunnableFromSocketThread(Runnable runnable) {
        if (mSocketThreadHandler != null) {
            mSocketThreadHandler.removeCallbacks(runnable);
        }
    }

    private void removeAllRunnableFromSocketThread() {
        if (mSocketThreadHandler != null) {
            mSocketThreadHandler.removeCallbacksAndMessages(null);
            mSocketThreadHandler = null;
        }
    }

    private void stopSocketThread() {
        if (!isOutSideThread) {
            /**
             * 这里说明使用的线程是 SocketEngine内部自己创建的
             */
            removeAllRunnableFromSocketThread();
            if (mSocketThread != null) {
                mSocketThread.quit();
                mSocketThread = null;
            }
        } else {
            /**
             * 说明SocketEngine 采用了外部提供的线程，此种情况下，SocketEngine 不对线程做处理功能
             */
            //do nothing
        }

    }

    private void startHeart() {
        if (mSocketThreadHandler != null) {
            stopHeart();
            mSocketThreadHandler.post(() -> {
                postHeartEvent();
            });
            printLog("startHeart Beat");
        }
    }

    private void stopHeart() {
        if (mSocketThreadHandler != null) {
            mSocketThreadHandler.removeCallbacks(mHeartBeatRunnable);
            printLog("stopHeart Beat");
        }
    }

    private void postHeartEvent() {
        if (mSocketThreadHandler != null) {
            mSocketThreadHandler.postDelayed(mHeartBeatRunnable, 15000);
        }
    }

    private void addMessageIntoCache(ServerMessageEvent messageEvent) {
        if (messageCache != null) {
            messageCache.push(messageEvent);
        }
    }

    private void addOrPostEvent(Object o) {
        if (isBootFinish()) {
            RxToolKit.Emit(o);
            EventBus.getDefault().post(o);
        } else {
            addEventIntoCache(o);
        }
    }

    private void addEventIntoCache(Object o) {
        if (messageCache != null) {
            messageCache.push(o);
        }
    }

    private void clearMessageCache() {
        if (messageCache != null)
            messageCache.clear();
    }

    private void postMessageCache() {
        if (isBootFinish()) {
            if (messageCache != null) {
                while (!messageCache.isEmpty()) {
                    Object object = messageCache.pop();
                    if (object instanceof ServerMessageEvent) {
                        postServerMessageEvent((ServerMessageEvent) object);
                        if (mSocketEngineObserver != null) {
                            mSocketEngineObserver.onServerMessageEvent((ServerMessageEvent) object);
                        }
                    } else if (object instanceof SocketEvent.SocketConnectStateEvent) {
                        RxToolKit.Emit((SocketEvent.SocketConnectStateEvent) object);
                        EventBus.getDefault().post((SocketEvent.SocketConnectStateEvent) object);
                    } else if (object instanceof SocketEvent.ReConnectedFailedEvent) {
                        RxToolKit.Emit((SocketEvent.ReConnectedFailedEvent) object);
                        EventBus.getDefault().post((SocketEvent.ReConnectedFailedEvent) object);
                    }
                }
            }
        }
    }

    private void postServerMessageEvent(ServerMessageEvent event) {
        RxToolKit.Emit(event);
    }

    private void postServerResponseEvent(ServerResponseEvent event) {
        RxToolKit.Emit(event);
    }

    public void printLog(String log) {
        PLog.i(TAG, "Thread[" + ToolKit.GetThreadInfo() + "]:[" + mSocketIntent + "]:[" + hashCode() + "]:" + log);
    }

    public void addARQTask(SocketEngineARQTask task) {
        if (mARQTasks != null)
            mARQTasks.put(task.getTaskId(), task);
    }

    public void clearARQTask() {
        ToolKit.travelMap(mARQTasks, (key, value) -> {
            value.destroyOutSide();
        });
        mARQTasks.clear();
    }

    public void removeARQTask(SocketEngineARQTask task) {
        if (task != null) {
            if (mARQTasks != null) {
                printLog("AROTask remove [" + task.getTaskId() + "]");
                mARQTasks.remove(task.getTaskId());
            }
        }
    }

    public void setBootFinish(boolean bootFinish) {
        synchronized (mSocketStatesLock) {
            isBootFinish = bootFinish;
        }
    }

    private boolean isBootFinish() {
        boolean result = false;
        synchronized (mSocketStatesLock) {
            result = isBootFinish;
        }
        return result;
    }

    public void setSocketError(boolean flag) {
        synchronized (mSocketStatesLock) {
            isSocketError = flag;
            printLog("setSocketError " + isSocketError);
        }
    }

    public boolean isSocketError() {
        boolean result = true;
        synchronized (mSocketStatesLock) {
            result = isSocketError;
        }
        return result;
    }

    public void setSocketOpen(boolean flag) {
        synchronized (mSocketStatesLock) {
            isOpen = flag;
            printLog("setSocketOpen " + flag);
        }
    }

    public boolean isSocketOpen() {
        boolean result = false;
        synchronized (mSocketStatesLock) {
            result = isOpen;
            printLog("isSocketOpen " + result);
        }
        return result;
    }

    public void setSocketDisconnect(boolean flag) {
        synchronized (mSocketStatesLock) {
            isDisConnected = flag;
            printLog("setSocketDisconnect " + flag);
        }
    }

    public boolean isDisConnected() {
        boolean result = true;
        synchronized (mSocketStatesLock) {
            result = isSocketError;
        }
        return result;
    }

    /**
     * SocketEngine 打开监听器，仅限与正常打开的监听，一旦打开成功或者失败则直接置空
     */
    public interface SocketEngineOpenListener {

        void onOpen();

        void onError();
    }

    /**
     * SocketEngine 的生命周期控制监听器
     */
    public interface SocketEngineLifeCycleListener {

        boolean onSocketOpen();

        boolean onSocketOpenError();

        boolean onSocketConnectInfo();

        boolean onSocketError();

        boolean onSocketClose();
    }

    public interface SocketEngineObserver {

        void onServerMessageEvent(ServerMessageEvent event);

        void onServerResponseEvent(ServerResponseEvent event);

        void onSocketRelease();

        void onSocketConnect();

        void onSocketDisConnect();

        void onSocketReConnectStart();

        void onSocketReConnectSuccess();

        void onSocketReConnectFailed(int code, String reason);
    }

}
