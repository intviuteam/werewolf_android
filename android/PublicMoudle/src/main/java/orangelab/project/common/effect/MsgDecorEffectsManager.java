package orangelab.project.common.effect;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.toolkit.action.Action;

import java.io.InputStream;
import java.util.Set;

import orangelab.project.common.effect.manifest.MsgDecorManifestHolder;
import orangelab.project.common.effect.msgdecor.MsgDecorManiFest;
import orangelab.project.common.manager.ApiManager;

/**
 * game_werewolf
 * 2018/7/13 下午1:51
 * Mystery
 */
class MsgDecorEffectsManager {

    private static final String TAG = "MsgDecorEffectsManager";

    private boolean isChecking = false;
    private MsgDecorManifestHolder mHolder;

    public MsgDecorEffectsManager() {

    }

    public void bindHolder() {
        mHolder = EffectsMainFestManager.aManager.mMsgDecorManifestHolder;
    }

    private boolean isChecking() {
        return isChecking;
    }

    private void setChecking(boolean checking) {
        isChecking = checking;
    }

    private void beginCheck(Action<Integer> callback) {
        MsgDecorManiFest msgDecorManiFest = mHolder.getManifestData();
        if (msgDecorManiFest == null) {
            mHolder.removeVersion();
            mHolder.removeManifest();
            setChecking(false);
            refreshMsgDecorManifest(null);
        } else {

            Set<String> result = mHolder.verifyManifest(msgDecorManiFest);
            if (result == null || result.size() == 0) {
                mHolder.updateManifest(msgDecorManiFest);
                if (callback != null) {
                    callback.func(0);
                }
            } else {
                mHolder.downloadManifestResource(msgDecorManiFest, false);
                mHolder.verifyManifest(msgDecorManiFest);
            }
        }
    }

    public void refreshMsgDecorManifest(Action<Integer> callback) {
        if (isChecking()) {
            return;
        }
        setChecking(true);
        final int version = mHolder.getVersion();
        ApiManager.GetMsgDecorManiFest(version, (value, e) -> {
            if (e != null) {
                beginCheck(callback);
            } else {
                if (value != null) {
                    diffCheckManifest(value.rev, value, callback);
                } else {
                    beginCheck(callback);
                }
            }
        });
    }

    public MsgDecorManiFest.MsgDecorManiFestItem findItem(String type) {
        if (TextUtils.isEmpty(type)) {
            return null;
        }
        MsgDecorManiFest msgDecorManiFest = mHolder.getManifestData();
        if (msgDecorManiFest != null) {
            return msgDecorManiFest.find(type);
        }
        return null;
    }

    private void diffCheckManifest(int localVersion, MsgDecorManiFest input, Action<Integer> callback) {
        MsgDecorManiFest maniFest = mHolder.getManifestData();
        if (maniFest == null) {
            /**
             * 本地还没有HeadDecor文件
             */
            try {
                mHolder.saveManifest(input);
                downloadAndVerifyHeadDecorManifest(input, false, callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            /**
             * 本地已经有了需要判断版本号
             */
            try {
                if (maniFest.rev == input.rev) {
                    if (callback != null) {
                        callback.func(0);
                    }
                } else {
                    downloadAndVerifyHeadDecorManifest(input, false, callback);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (callback != null) {
                    callback.func(-1);
                }
            }
        }
    }

    private void downloadAndVerifyHeadDecorManifest(MsgDecorManiFest input, boolean force, Action<Integer> callback) {

        /**
         * 拉取头像框数据
         */
        mHolder.downloadManifestResource(input, force);

        /**
         * 下载完毕后，进行文件完整性校验
         */
        Set<String> verifyResult = mHolder.verifyManifest(input);
        PLog.i(TAG, "Msg Decor Result " + verifyResult);
        if (verifyResult != null && verifyResult.size() > 0) {
            if (callback != null) {
                callback.func(-1);
            }
        } else {
            if (callback != null) {
                callback.func(0);
            }
        }

    }

    public InputStream getInputStream(String url) {
        return mHolder.getInputStream(url);
    }

}
