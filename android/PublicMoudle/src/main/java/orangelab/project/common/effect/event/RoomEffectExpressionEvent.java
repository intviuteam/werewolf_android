package orangelab.project.common.effect.event;

import android.graphics.PointF;

/**
 * game_werewolf
 * 2018/7/20 下午6:27
 * Mystery
 */
public class RoomEffectExpressionEvent {

    private String type;
    private String url;
    private PointF point;
    private int height;
    private int width;
    private long time;
    private int position;

    public RoomEffectExpressionEvent(String type, String url, PointF point,
                                     int height, int width, long time, int position) {
        this.type = type;
        this.url = url;
        this.point = point;
        this.height = height;
        this.width = width;
        this.time = time;
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public PointF getPoint() {
        return point;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public long getTime() {
        return time;
    }

    public int getPosition() {
        return position;
    }
}
