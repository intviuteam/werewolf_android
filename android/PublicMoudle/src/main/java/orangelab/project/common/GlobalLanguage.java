package orangelab.project.common;

import android.text.TextUtils;

import com.R;
import com.androidtoolkit.ApkToolKit;

/**
 * game_werewolf
 * 2018/1/9 下午1:57
 * Mystery
 */

public class GlobalLanguage {


    public static int GetSunUp() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_sunup_ja_jp;
        }
        return R.raw.media_sunup;
    }

    public static int GetSunSetWithSeer() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_sunset_with_seer_ja_jp;
        }
        return R.raw.media_sunset_with_seer;
    }

    public static int GetSunSetWithOutSeer() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_sunset_without_seer_ja_jp;
        }
        return R.raw.media_sunset_without_seer;
    }

    public static int GetSunSetNormalPeople() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_sunset_normal_people_ja_jp;
        }
        return R.raw.media_sunset_normal_people;
    }

    public static int GetGameOverPeopleWin() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_gameover_people_win_ja_jp;
        }
        return R.raw.media_gameover_people_win;
    }

    public static int GetGameOverWolfWin() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_gameover_wolf_win_ja_jp;
        }
        return R.raw.media_gameover_wolf_win;
    }

    public static int GetGameOverCupidWin() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_gameover_cupid_win_ja_jp;
        }
        return R.raw.media_gameover_cupid_win;
    }

    public static int GetLink() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_link_ja_jp;
        }
        return R.raw.media_link;
    }

    public static int GetStart() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_start_ja_jp;
        }
        return R.raw.media_start;
    }

    public static int GetVote() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_vote_ja_jp;
        }
        return R.raw.media_vote;
    }

    public static int GetAssginRole() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_assgined_ja_jp;
        }
        return R.raw.media_assgined;
    }

    public static int GetSunSetOnlySeer() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_sunset_only_seer_ja_jp;
        }
        return R.raw.media_sunset_only_seer;
    }

    public static int GetBoom() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_boom_ja_jp;
        }
        return R.raw.media_boom;
    }

    public static int GetSunSetSeerWolf() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_seer_wolf_ja_jp;
        }
        return R.raw.media_seer_wolf;
    }

    public static int GetSunSetSeerWitchWolf() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_seer_witch_wolf_ja_jp;
        }
        return R.raw.media_seer_witch_wolf;
    }

    public static int GetSunSetSeerGuardWolf() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_seer_guard_wolf_ja_jp;
        }
        return R.raw.media_seer_guard_wolf;
    }

    public static int GetSunSetSeerGuardWitchWolf() {
        String curLanguage = ApkToolKit.GetApkLanguage();
        if (IsJp(curLanguage)) {
            return R.raw.media_seer_guard_witch_wolf_ja_jp;
        }
        return R.raw.media_seer_guard_witch_wolf;
    }


    public static boolean IsCn(String lg) {
        if (IsJp(lg)) {
            return false;
        }
        return true;
    }


    public static boolean IsJp(String lg) {
        if (TextUtils.isEmpty(lg)) {
            return false;
        }
        if (lg.contains("JP")) {
            return true;
        }
        return false;
    }

    public static String CurApkLanguage() {
        String s = ApkToolKit.GetApkLanguage();
        if (IsJp(s)) {
            return "ja";
        }
        if (IsCn(s)) {
            return "cn";
        }
        return "cn";
    }

}
