package orangelab.project.common.engine.event;

import com.androidtoolkit.PLog;

import org.webrtc.StatsReport;
import org.webrtc.SurfaceViewRenderer;

import java.util.List;

import cn.intviu.connect.model.ConnectMessage;
import cn.intviu.connect.model.Message;
import cn.intviu.orbit.manager.IOrbitEvent;
import cn.intviu.orbit.manager.OrbitRoomConfig;
import cn.intviu.sdk.model.User;

/**
 * Author:1727
 * 2017/3/3
 */

public class BaseOrbitEvent implements IOrbitEvent {

    private static final String TAG = "SimpleRoomEvent";

    @Override
    public void onError(int code, String reason) {
        PLog.i(TAG, "onError: ");
    }

    @Override
    public void onUserJoined(User user) {

    }

    @Override
    public void onUserLeave(User user) {
        PLog.i(TAG, "onUserLeave: ");
    }

    @Override
    public void onChangeMuteStatus(String uuid, boolean mute, boolean video) {

    }

    @Override
    public void onConnectStateChange(ConnectMessage message) {

    }

    @Override
    public void onShareScreen(OrbitRoomConfig roomConfig) {

    }

    @Override
    public void onEnterRoomSuccess(String uuid) {

    }

    @Override
    public void onMuteAll(String name) {

    }

    @Override
    public void onRemoteRenderView(User user, SurfaceViewRenderer surfaceView) {

    }

    @Override
    public void onRoomUserList(List<User> userList) {

    }

    @Override
    public void onRemoteUserDisconnected(User user) {
        PLog.i(TAG, "onRemoteUserDisconnected: ");
    }

    @Override
    public void onRemoteUserConnected(User user) {
        PLog.i(TAG, "onRemoteUserConnected: ");
    }

    @Override
    public void onRemoteUserFailed(User user) {
        PLog.i(TAG, "onRemoteUserFailed: ");
    }

    @Override
    public void onRemoteUserLoading(User user) {
        PLog.i(TAG, "onRemoteUserLoading: ");
    }

    @Override
    public void onDisconnectFromRoom(String roomId) {
        PLog.i(TAG, "onDisconnectFromRoom: ");
    }

    @Override
    public void onOtherUserComingRoom(String uuid, Message.UserInfo userInfo) {

    }

    @Override
    public void onStatesReport(StatsReport[] reports) {

    }

    @Override
    public void onGetMessageFromRoom(String key, String gsonString) {

    }
}
