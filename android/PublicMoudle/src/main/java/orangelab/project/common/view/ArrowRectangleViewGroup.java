package orangelab.project.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.R;

/**
 * game_werewolf
 * 2017/6/17 下午12:31
 * Mystery
 */

public class ArrowRectangleViewGroup extends ViewGroup {
    public static final int ARROW_TOP_RIGHT = 1;
    public static final int ARROW_TOP_LEFT = 2;
    public static final int ARROW_LEFT_TOP = 3;
    public static final int ARROW_LEFT_BOTTOM = 4;
    public static final int ARROW_RIGHT_TOP = 5;
    public static final int ARROW_RIGHT_BOTTOM = 6;
    public static final int ARROW_BOTTOM_RIGHT = 7;
    public static final int ARROW_BOTTOM_LEFT = 8;

    private int mArrowWidth = 40;
    private int mArrowHeight = 20;
    private int mRadius = 18;
    private int mBackgroundColor = Color.WHITE;
    private int mArrowOffset = 0;
//    private int mShadowColor = Color.BLACK;
//    private int mShadowThickness = 0;

    private int mArrowDirection = 1;

    private boolean needStroke = false;
    private int strokeWidth = 0;
    private int strokeColor = Color.WHITE;

    public void setNeedStroke(boolean needStroke) {
        this.needStroke = needStroke;
    }

    public void setArrowHeight(int arrowHeight) {
        mArrowHeight = arrowHeight;
        invalidate();
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public void setStrokeColor(int color) {
        this.strokeColor = color;
    }

    public ArrowRectangleViewGroup(Context context) {
        this(context, null);
    }

    public ArrowRectangleViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArrowRectangleViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ArrowRectangleViewGroup, defStyleAttr, 0);
        for (int i = 0; i < a.getIndexCount(); i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.ArrowRectangleViewGroup_arrow_width) {
                mArrowWidth = a.getDimensionPixelSize(attr, mArrowWidth);
            } else if (attr == R.styleable.ArrowRectangleViewGroup_arrow_height) {
                mArrowHeight = a.getDimensionPixelSize(attr, mArrowHeight);
            } else if (attr == R.styleable.ArrowRectangleViewGroup_rectangle_radius) {
                mRadius = a.getDimensionPixelSize(attr, mRadius);
            } else if (attr == R.styleable.ArrowRectangleViewGroup_background_color) {
                mBackgroundColor = a.getColor(attr, mBackgroundColor);
            } else if (attr == R.styleable.ArrowRectangleViewGroup_arrow_offset) {
                mArrowOffset = a.getDimensionPixelSize(attr, mArrowOffset);
            } else if (attr == R.styleable.ArrowRectangleViewGroup_arrow_direction) {
                mArrowDirection = a.getInt(attr, mArrowDirection);
            }
//            switch (attr) {
//                case R.styleable.ArrowRectangleViewGroup_arrow_width:
//                    mArrowWidth = a.getDimensionPixelSize(attr, mArrowWidth);
//                    break;
//                case R.styleable.ArrowRectangleViewGroup_arrow_height:
//                    mArrowHeight = a.getDimensionPixelSize(attr, mArrowHeight);
//                    break;
//                case R.styleable.ArrowRectangleViewGroup_rectangle_radius:
//                    mRadius = a.getDimensionPixelSize(attr, mRadius);
//                    break;
//                case R.styleable.ArrowRectangleViewGroup_background_color:
//                    mBackgroundColor = a.getColor(attr, mBackgroundColor);
//                    break;
//                case R.styleable.ArrowRectangleViewGroup_arrow_offset:
//                    mArrowOffset = a.getDimensionPixelSize(attr, mArrowOffset);
//                    break;
////                case R.styleable.ArrowRectangleViewGroup_shadow_color:
////                    mShadowColor = a.getColor(attr, mShadowColor);
////                    break;
////                case R.styleable.ArrowRectangleViewGroup_shadow_thickness:
////                    mShadowThickness = a.getDimensionPixelSize(attr, mShadowThickness);
////                    break;
//                case R.styleable.ArrowRectangleViewGroup_arrow_direction:
//                    mArrowDirection = a.getInt(attr, mArrowDirection);
//                    break;
//            }
        }
        a.recycle();
    }

    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
        postInvalidate();
    }

    public void setArrowDirection(int direction) {
        mArrowDirection = direction;
        requestLayout();
    }

    public void setArrowOffset(int offset) {
        mArrowOffset = offset;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();
        /**
         * 测量整体宽高
         */
        int maxWidth = 0;
        int maxHeight = 0;
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
                maxWidth = Math.max(maxWidth, child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
                maxHeight = maxHeight + child.getMeasuredHeight() + lp.topMargin + lp.bottomMargin;
            }
        }
        switch (mArrowDirection) {
            case ARROW_BOTTOM_LEFT:
            case ARROW_BOTTOM_RIGHT:
            case ARROW_TOP_LEFT:
            case ARROW_TOP_RIGHT:
                maxHeight += mArrowHeight + mRadius;
//                maxWidth += mRadius;
                break;
            case ARROW_RIGHT_TOP:
            case ARROW_RIGHT_BOTTOM:
            case ARROW_LEFT_TOP:
            case ARROW_LEFT_BOTTOM:
                maxWidth += mArrowHeight + mRadius;
                maxHeight += mRadius;
                break;
        }
        maxWidth =
                maxWidth + getPaddingLeft() + getPaddingRight();
        maxHeight =
                maxHeight + getPaddingTop() + getPaddingBottom();

        setMeasuredDimension(maxWidth, maxHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();

        /**
         * 根据mArrowDirection来布局
         */
        int topOffset = 0;
        int bottomOffset = 0;
        int rightOffset = 0;
        int leftOffset = 0;

        int top = 0;
        int left = 0;
        int bottom = 0;
        int right = 0;

        switch (mArrowDirection) {
            case ARROW_BOTTOM_LEFT:
            case ARROW_BOTTOM_RIGHT: {
                topOffset = mRadius / 2;
                bottomOffset = mArrowHeight + mRadius / 2;
                b = b - bottomOffset;
                for (int i = 0; i < count; i++) {
                    final View child = getChildAt(i);
                    top = topOffset + i * child.getMeasuredHeight();
                    bottom = top + child.getMeasuredHeight();
                    left = l;
                    right = r;
                    if (bottom >= b) {
                        bottom = b;
                    }
                    child.layout(left, top, right, bottom);
                }
            }
            break;
            case ARROW_TOP_LEFT:
            case ARROW_TOP_RIGHT: {
                topOffset = mArrowHeight + mRadius / 2;
                for (int i = 0; i < count; i++) {
                    final View child = getChildAt(i);
                    top = t + topOffset + i * child.getMeasuredHeight();
                    bottom = top + child.getMeasuredHeight();
                    left = l;
                    right = r;
                    child.layout(left, top, right, bottom);
                }
            }
            break;
            case ARROW_RIGHT_TOP:
            case ARROW_RIGHT_BOTTOM:
                topOffset = mRadius / 2;
                rightOffset = mArrowHeight + mRadius / 2;
                for (int i = 0; i < count; i++) {
                    final View child = getChildAt(i);
                    top = topOffset + i * child.getMeasuredHeight();
                    left = l;
                    right = r - rightOffset;
                    bottom = top + child.getMeasuredHeight();
                    child.layout(left, top, right, bottom);
                }
                break;
            case ARROW_LEFT_TOP:
            case ARROW_LEFT_BOTTOM:
                topOffset = mRadius / 2;
                leftOffset = mArrowHeight + mRadius / 2;
                for (int i = 0; i < count; i++) {
                    final View child = getChildAt(i);
                    top = topOffset + i * child.getMeasuredHeight();
                    left = l + leftOffset;
                    right = r;
                    bottom = top + child.getMeasuredHeight();
                    child.layout(left, top, right, bottom);
                }
                break;
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(mBackgroundColor);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));


        Paint arrowPaint = new Paint();
        arrowPaint.setAntiAlias(true);
        arrowPaint.setStyle(Paint.Style.FILL);
        arrowPaint.setColor(mBackgroundColor);
        arrowPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        Paint strokePain = null;
        if (needStroke) {
            strokePain = new Paint();
            strokePain.setStyle(Paint.Style.STROKE);
            strokePain.setColor(strokeColor);
            strokePain.setAntiAlias(true);
            strokePain.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
            strokePain.setStrokeWidth(strokeWidth);
        }

        switch (mArrowDirection) {
            case ARROW_BOTTOM_LEFT:
            case ARROW_BOTTOM_RIGHT: {
                canvas.drawRoundRect(new RectF(
                        0 + strokeWidth / 2,
                        0 + strokeWidth / 2,
                        getMeasuredWidth() - strokeWidth / 2,
                        getMeasuredHeight() - mArrowHeight - strokeWidth / 2
                ), mRadius, mRadius, paint);
                if (needStroke) {
                    canvas.drawRoundRect(new RectF(
                            0 + strokeWidth / 2,
                            0 + strokeWidth / 2,
                            getMeasuredWidth() - strokeWidth / 2,
                            getMeasuredHeight() - mArrowHeight - strokeWidth / 2
                    ), mRadius, mRadius, strokePain);
                }
            }
            break;
            case ARROW_TOP_LEFT:
            case ARROW_TOP_RIGHT: {
                canvas.drawRoundRect(new RectF(
                        0 + strokeWidth / 2,
                        mArrowHeight + strokeWidth / 2,
                        getMeasuredWidth() - strokeWidth / 2,
                        getMeasuredHeight() - strokeWidth / 2
                ), mRadius, mRadius, paint);
                if (needStroke) {
                    canvas.drawRoundRect(new RectF(
                            0 + strokeWidth / 2,
                            mArrowHeight + strokeWidth / 2,
                            getMeasuredWidth() - strokeWidth / 2,
                            getMeasuredHeight() - strokeWidth / 2
                    ), mRadius, mRadius, strokePain);
                }
            }
            break;
            case ARROW_RIGHT_TOP:
            case ARROW_RIGHT_BOTTOM: {
                canvas.drawRoundRect(new RectF(
                        0 + strokeWidth / 2,
                        0 + strokeWidth / 2,
                        getMeasuredWidth() - mArrowHeight - strokeWidth / 2,
                        getMeasuredHeight() - mArrowWidth / 2
                ), mRadius, mRadius, paint);
                if (needStroke) {
                    canvas.drawRoundRect(new RectF(
                            0 + strokeWidth / 2,
                            0 + strokeWidth / 2,
                            getMeasuredWidth() - mArrowHeight - strokeWidth / 2,
                            getMeasuredHeight() - strokeWidth / 2
                    ), mRadius, mRadius, strokePain);
                }
            }
            break;
            case ARROW_LEFT_TOP:
            case ARROW_LEFT_BOTTOM: {
                canvas.drawRoundRect(new RectF(
                        mArrowHeight + strokeWidth / 2,
                        0 + strokeWidth / 2,
                        getMeasuredWidth() - strokeWidth / 2,
                        getMeasuredHeight() - strokeWidth / 2
                ), mRadius, mRadius, paint);
                if (needStroke) {
                    canvas.drawRoundRect(new RectF(
                            mArrowHeight + strokeWidth / 2,
                            0 + strokeWidth / 2,
                            getMeasuredWidth() - strokeWidth / 2,
                            getMeasuredHeight() - strokeWidth / 2
                    ), mRadius, mRadius, strokePain);
                }
            }
            break;
        }


        // draw arrow
        Path path = new Path();
        Path path1 = new Path();
        int startPointX = 0, startPointY = 0;
        switch (mArrowDirection) {
            case ARROW_BOTTOM_LEFT: {
                startPointX = 0 + mArrowOffset;
                startPointY = getMeasuredHeight() - mArrowHeight - strokeWidth / 2;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX + mArrowWidth / 2, startPointY + mArrowHeight);
                path.lineTo(startPointX + mArrowWidth, startPointY);
                path.close();

                path1.moveTo(startPointX + strokeWidth, startPointY);
                path1.lineTo(startPointX + mArrowWidth - strokeWidth, startPointY);
                path1.close();
            }
            break;
            case ARROW_BOTTOM_RIGHT: {
                /**
                 * 计算右下角
                 * 由于目前仅仅使用右下角，先改动右下角计算方式
                 */
                startPointX = getMeasuredWidth() - mArrowWidth - mArrowOffset;
                startPointY = getMeasuredHeight() - mArrowHeight - strokeWidth - strokeWidth / 2;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX + mArrowWidth / 2, startPointY + mArrowHeight);
                path.lineTo(startPointX + mArrowWidth, startPointY);
                path.close();
                /**
                 * 去掉三角形的顶边路径
                 */
                path1.moveTo(startPointX - strokeWidth, startPointY);
                path1.lineTo(startPointX + mArrowWidth + strokeWidth, startPointY);
                path1.close();
            }
            break;
            case ARROW_TOP_LEFT: {
                startPointX = 0 + mArrowOffset;
                startPointY = 0 + mArrowHeight + strokeWidth / 2;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX + mArrowWidth / 2, startPointY - mArrowHeight);
                path.lineTo(startPointX + mArrowWidth, startPointY);
                path.close();

                path1.moveTo(startPointX + strokeWidth, startPointY);
                path1.lineTo(startPointX + mArrowWidth - strokeWidth, startPointY);
                path1.close();
            }
            break;
            case ARROW_TOP_RIGHT: {
                startPointX = getMeasuredWidth() - mArrowOffset;
                startPointY = 0 + mArrowHeight + strokeWidth / 2;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX - mArrowWidth / 2, startPointY - mArrowHeight);
                path.lineTo(startPointX - mArrowWidth, startPointY);
                path.close();

                path1.moveTo(startPointX + strokeWidth, startPointY);
                path1.lineTo(startPointX - mArrowWidth + strokeWidth, startPointY);
                path1.close();
            }
            break;
            //---------------------
            case ARROW_RIGHT_TOP: {
                startPointX = getMeasuredWidth() - mArrowHeight - strokeWidth / 2;
                startPointY = 0 + mArrowOffset + strokeWidth / 2;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX + mArrowHeight, startPointY + mArrowWidth / 2);
                path.lineTo(startPointX, startPointY + mArrowWidth);
                path.close();

                path1.moveTo(startPointX, startPointY + strokeWidth);
                path1.lineTo(startPointX, startPointY + mArrowWidth - strokeWidth);
                path1.close();
            }
            break;
            case ARROW_RIGHT_BOTTOM: {
                startPointX = getMeasuredWidth() - mArrowHeight - strokeWidth / 2;
                startPointY = getMeasuredHeight() - mArrowOffset;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX + mArrowHeight, startPointY - mArrowWidth / 2);
                path.lineTo(startPointX, startPointY - mArrowWidth);
                path.close();

                path1.moveTo(startPointX, startPointY - strokeWidth);
                path1.lineTo(startPointX, startPointY - mArrowWidth + strokeWidth);
                path1.close();
            }
            break;
            case ARROW_LEFT_TOP: {
                startPointX = 0 + mArrowHeight + strokeWidth / 2;
                startPointY = 0 + mArrowOffset;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX - mArrowHeight, startPointY + mArrowWidth / 2);
                path.lineTo(startPointX, startPointY + mArrowWidth);
                path.close();

                path1.moveTo(startPointX, startPointY + strokeWidth);
                path1.lineTo(startPointX, startPointY + mArrowWidth - strokeWidth);
                path1.close();
            }
            break;
            case ARROW_LEFT_BOTTOM: {
                startPointX = 0 + mArrowHeight + strokeWidth / 2;
                startPointY = getMeasuredHeight() - mArrowOffset;
                path.moveTo(startPointX, startPointY);
                path.lineTo(startPointX - mArrowHeight, startPointY - mArrowWidth / 2);
                path.lineTo(startPointX, startPointY - mArrowWidth);
                path.close();

                path1.moveTo(startPointX, startPointY - strokeWidth);
                path1.lineTo(startPointX, startPointY - mArrowWidth + strokeWidth);
                path1.close();
            }
            break;
        }
        canvas.drawPath(path, arrowPaint);
        /**
         * 绘制三角形的边框
         */
        if (needStroke) {
            path.setFillType(Path.FillType.EVEN_ODD);
            canvas.drawPath(path, strokePain);

            /**
             * 去掉三角型的一条边
             */
            Paint temp = new Paint();
            temp.setStyle(Paint.Style.FILL_AND_STROKE);
            temp.setStrokeWidth(strokeWidth);
            temp.setColor(mBackgroundColor);
            temp.setAntiAlias(true);
            temp.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
            canvas.drawPath(path1, temp);
        }
        super.dispatchDraw(canvas);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }
}
