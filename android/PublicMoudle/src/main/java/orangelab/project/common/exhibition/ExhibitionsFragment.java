package orangelab.project.common.exhibition;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidtoolkit.RxToolKit;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.game.model.WereWolfGiftRebate;

import com.toolkit.action.Action;
import com.toolkit.action.Destroyable;

import orangelab.project.common.event.SocketEvent;
import orangelab.project.common.exhibition.card.CardUtils;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.model.CardListResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.SafeHandler;

import com.androidtoolkit.ToastToolKit;

import orangelab.project.common.utils.Utils;

import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;

import com.androidtoolkit.view.ScreenUtils;

import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

import com.datasource.GlobalUserState;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2017/6/20 下午5:28
 * Mystery
 */
public class ExhibitionsFragment extends DialogFragment implements Destroyable {

    private static final String TAG = "ExhibitionsFragment";

    private static final int PAGE_MAX_SIZE = 8;

    private String fromType;
    private int toPosition;
    private UserData toUserData;
    private String displayType = "";

    private ViewPager giftViewPager;
    private ExhibitionsViewPagerAdapater giftViewPageAdapter;
    private TextView giftTitle;
    private TextView giftLoading;
    private View giftRefreshing;
    private LinearLayout giftIndex;
    private ImageView[] giftIndexPoints;
    private List<IExhibition> giftList = new ArrayList<>();
    private List<GridView> viewList;
    private int totalPages;
    private int lastTotalPages = 0;
    private int curPages = 0;

    private RequestTask mCardRequestTask;
    private SafeHandler mSafeHandler = new SafeHandler(Looper.getMainLooper());
    private boolean isDestroy = false;

    private String mFragmentKey = TAG + Integer.toString(hashCode());
    private TextView mIntroduce;
    private static final String DIM = "<img src=\"" + R.mipmap.icon_dim + "\"/>";
    private static final String GOLD = "<img src=\"" + R.mipmap.icon_coin + "\"/>";
    final Html.ImageGetter imageGetter = source -> {
        Drawable drawable = null;
        int rId = Integer.parseInt(source);
        drawable = getContext().getResources().getDrawable(rId);
        drawable.setBounds(0, 0, ScreenUtils.dip2px(10), ScreenUtils.dip2px(10));
        return drawable;
    };

    @Override
    public void onStart() {
        super.onStart();
        PLog.i(TAG, "onStart()");
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.dimAmount = 0f;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        PLog.i(TAG, "onCreateView()");
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_gift, container, false);
        preInitData();
        initView(view);
        initRequest();
        initListener();
        return view;
    }

    private void initListener() {
        RxToolKit.Build(mFragmentKey, FinishConversationEvent.class).action(v -> {
            dismiss();
        }).register();
    }

    private void addCardListResultIntoGiftList(CardListResult cardListResult) {
        giftList.addAll(CardUtils.cardListResultTurnToList(cardListResult));
    }

    private void refresh() {
        giftRefreshing.setVisibility(View.GONE);
        giftLoading.setVisibility(View.VISIBLE);
        if (mCardRequestTask != null) {
            mCardRequestTask.cancel();
        }
        initRequest();
    }

    private void initCardRequest() {
        if (giftList != null && giftList.size() == 0) {
            /**
             * 这里说明礼物没有信息，需要展示一下loading
             */
            giftLoading.setVisibility(View.VISIBLE);
        }
        if (mCardRequestTask != null && mCardRequestTask.isExecuting()) {
            mCardRequestTask.cancel();
        }
        mCardRequestTask = RequestTask.create(
                false,
                Constant.GET_CARD_LIST,
                GlobalUserState.getGlobalState().getToken(),
                new JSONObject(),
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        CardListResult cardListResult = GsonHelper.getGson().fromJson(response, CardListResult.class);
                        executeUI(() -> {
                            addCardListResultIntoGiftList(cardListResult);
                            initData();
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        executeUI(() -> {
                            initData();
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        executeUI(() -> {
                            initData();
                        });
                    }
                }
        );
    }


    private void initRequest() {
        EffectsManager.QueryGiftList(value -> {
            /**
             * 从礼物清单获取礼物
             */
            executeUI(() -> {
                if (!Utils.targetListIsNull(value)) {
                    giftLoading.setVisibility(View.GONE);
                    giftList.addAll(value);
                    /**
                     * 这里先计算一下礼物个数
                     */
                    initData();
                }
                initCardRequest();
                mCardRequestTask.execute();
            });
        });
        EffectsManager.RefreshGiftsManifestForExhibition(null);
    }

    private void executeUI(Runnable runnable) {
        executeUI(runnable, 0);
    }

    private void executeUI(Runnable runnable, long delay) {
        if (mSafeHandler != null && !isDestroy) {
            mSafeHandler.postDelayed(runnable, delay);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PLog.i(TAG, "onDestroyView()");
        destroy();

    }

    private void removeType(String type) {
        IExhibition target = null;
        for (IExhibition temp : giftList) {
            if (TextUtils.equals(type, temp.getRealType())) {
                target = temp;
                break;
            }
        }
        if (target != null) {
            giftList.remove(target);
            updateTotalPages();
            if (lastTotalPages != totalPages) {
                giftViewPageAdapter.removeLastPage();
                lastTotalPages = totalPages;
            }
            noticeAllAdapter();
        }
    }

    private void noticeAllAdapter() {
        if (viewList != null) {
            for (GridView temp : viewList) {
                ExhibitionsGridViewAdapter a = (ExhibitionsGridViewAdapter) temp.getAdapter();
                a.notifyDataSetChanged();
            }
        }
    }

    private void preInitData() {
        fromType = getArguments().getString(ExhibitionsConstant.FROM_TYPE);
        toPosition = getArguments().getInt(ExhibitionsConstant.USER_POSITION);
        toUserData = getArguments().getParcelable(ExhibitionsConstant.TO_USER_DATA);
        displayType = getArguments().getString(ExhibitionsConstant.DISPLAY_TYPE);
    }

    private void handlePurchaseCard(IExhibition exhibition, Runnable runnable) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", exhibition.getRealType());
            jsonObject.put("peer", toUserData.userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask mPurchaseTask = RequestTask.create(
                false,
                "/gift/give",
                GlobalUserState.getGlobalState().getToken(),
                jsonObject,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        executeUI(() -> {
                            ReportEventUtils.reportGiftEvent(exhibition.getRealType());
                            Utils.runSafely(() -> {
                                /**
                                 * setTicket
                                 */
                                try {
                                    JSONObject json = new JSONObject(response);
                                    exhibition.setExhibitionTicket(json.optString(Constant.GIFT_TICKET));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            });
                            runnable.run();
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        executeUI(() -> {
                            ToastToolKit.showShort(failed);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        executeUI(() -> {
                            ToastToolKit.showShort(MessageUtils.getString(R.string.network_is_unavailable));
                        });
                    }
                });
        mPurchaseTask.execute();
    }

    private void handlePurchaseGift(IExhibition exhibition, Action<WereWolfGiftRebate> runnable) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", exhibition.getRealType());
            jsonObject.put("peer", toUserData.userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask mPurchaseTask = RequestTask.create(
                false,
                "/gift/buy_v2",
                GlobalUserState.getGlobalState().getToken(),
                jsonObject,
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        executeUI(() -> {
                            ReportEventUtils.reportGiftEvent(exhibition.getRealType());
                            Utils.runSafely(() -> {
                                /**
                                 * setTicket
                                 */
                                try {
                                    JSONObject json = new JSONObject(response);
                                    exhibition.setExhibitionTicket(json.optString(Constant.GIFT_TICKET));
                                    WereWolfGiftRebate result = null;
                                    if (json.has("rebate")) {
                                        result = GsonHelper.getGson().fromJson(json.optJSONObject("rebate").toString(), WereWolfGiftRebate.class);
                                    }
                                    if (runnable != null) {
                                        runnable.func(result);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            });

                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        executeUI(() -> {
                            Utils.runSafely(() -> {
                                switch (code) {
                                    case Constant.ERROR_CODE_BUY_ERROR:
                                        Toast.makeText(getContext(), R.string.buy_gift_error, Toast.LENGTH_SHORT).show();
                                        break;
                                    case Constant.ERROR_CODE_NOT_ENOUGH_MONEY: {
                                        Utils.runSafely(() -> {
                                            if (ExhibitionsUtils.priceTypeIsGold(exhibition.getExhibitionPriceType())) {
                                                ToastToolKit.showShort(MessageUtils.getString(R.string.not_enough_coins));
                                            } else {
                                                ToastToolKit.showShort(MessageUtils.getString(R.string.not_enough_diamonds));
                                            }
                                        });
                                    }
                                    break;
                                    default:
                                        Toast.makeText(getContext(), failed, Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            });
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        executeUI(() -> {
                                    Toast.makeText(getContext(), R.string.network_is_unavailable, Toast.LENGTH_SHORT).show();
                                }
                        );
                    }
                }
        );
        mPurchaseTask.execute();
    }

    private void noGiftList() {
        giftLoading.setVisibility(View.GONE);
        giftRefreshing.setVisibility(View.VISIBLE);
        giftList.clear();
        giftViewPager.setVisibility(View.GONE);
        giftIndex.setVisibility(View.GONE);

    }

    private void updateTotalPages() {
        totalPages = (int) Math.ceil(giftList.size() * 1.0 / PAGE_MAX_SIZE);
        giftIndex.removeAllViews();
        giftIndexPoints = new ImageView[totalPages];
        for (int i = 0; i < totalPages; i++) {
            giftIndexPoints[i] = new ImageView(getContext());
            if (i == curPages) {
                giftIndexPoints[i].setImageResource(R.mipmap.gift_index_selected);
            } else {
                giftIndexPoints[i].setImageResource(R.mipmap.gift_index_normal);
            }
            giftIndexPoints[i].setPadding(3, 3, 3, 3);
            giftIndex.addView(giftIndexPoints[i]);
        }
    }

    private void showToastIfNeed() {
        if (TextUtils.equals(fromType, ExhibitionsConstant.FROM_MAIN_PAGE) || TextUtils.equals(fromType, ExhibitionsConstant.FROM_OUT_SIDER)) {
            Toast.makeText(getContext(), R.string.present_success, Toast.LENGTH_SHORT).show();
        }
    }

    private void initViewList() {
        for (int i = 0; i < totalPages; i++) {
            final GridView gridView = (GridView) View.inflate(getContext(), R.layout.fragment_gift_viewpage_item, null);
            gridView.setAdapter(new ExhibitionsGridViewAdapter(getContext(), giftList, i, PAGE_MAX_SIZE));
            gridView.setOnItemClickListener((parent, view, position, id) -> {
                IExhibition exhibition = (IExhibition) gridView.getItemAtPosition(position);
                if (EffectsManager.IsCardByExhibitionsType(exhibition.getExhibitionType())) {
                    if (TextUtils.equals(fromType, ExhibitionsConstant.FROM_GAME)) {
                        /**
                         * 游戏内送卡片
                         */
                        RxToolKit.Emit(new SocketEvent.SocketSendCardEvent(toUserData.userId, exhibition.getRealType(),
                                value -> {
                                    executeUI(() -> {
                                        ExhibitionsGridViewAdapter.ViewHolder viewHoler = (ExhibitionsGridViewAdapter.ViewHolder) view.getTag();
                                        if (viewHoler != null) {
                                            viewHoler.decNumber();
                                            if (viewHoler.getNumber() == 0) {
                                                /**
                                                 *  移除卡片
                                                 */
                                                removeType(exhibition.getRealType());
                                            }
                                        }
                                    });
                                },
                                value -> {
                                    executeUI(RunnableFactory.createMsgToastRunnable(value));
                                }));
                    } else {
                        handlePurchaseCard(exhibition, () -> {
                            try {
                                showToastIfNeed();
                                ExhibitionsGridViewAdapter.ViewHolder viewHoler = (ExhibitionsGridViewAdapter.ViewHolder) view.getTag();
                                if (viewHoler != null) {
                                    viewHoler.decNumber();
                                    if (viewHoler.getNumber() == 0) {
                                        /**
                                         *  移除卡片
                                         */
                                        removeType(exhibition.getRealType());
                                    }
                                    handleExhibitions(exhibition);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                    }
                } else {
                    /**
                     * 游戏外的走以前接口，游戏内走socket接口
                     */
                    if (TextUtils.equals(fromType, ExhibitionsConstant.FROM_GAME)) {
                        RxToolKit.Emit(new SocketEvent.SocketSendGiftEvent(toUserData.userId, exhibition.getRealType(), this.displayType, value -> {

                        }, value -> {
                            executeUI(RunnableFactory.createMsgToastRunnable(value));
                        }));
                    } else {
                        handlePurchaseGift(exhibition, value -> {
                            showToastIfNeed();
                            if (value != null && value.value > 0) {
                                exhibition.setDisCount(Integer.toString(value.value));
                            }
                            handleExhibitions(exhibition);
                        });
                    }
                }
            });
            viewList.add(gridView);
        }

        giftViewPageAdapter = new ExhibitionsViewPagerAdapater<GridView>(viewList);
        giftViewPager.setAdapter(giftViewPageAdapter);
        giftViewPager.setOffscreenPageLimit(0);
        giftViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < totalPages; i++) {
                    if (i == position) {
                        giftIndexPoints[i].setImageResource(R.mipmap.gift_index_selected);
                    } else {
                        giftIndexPoints[i].setImageResource(R.mipmap.gift_index_normal);
                    }
                }
                curPages = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initData() {
        if (giftList == null || giftList.size() == 0) {
            noGiftList();
        } else {
            giftLoading.setVisibility(View.GONE);
            giftViewPager.setVisibility(View.VISIBLE);
            giftIndex.setVisibility(View.VISIBLE);
        }
        viewList = new ArrayList<>();
        updateTotalPages();
        lastTotalPages = totalPages;
        initViewList();
    }

    /**
     * @param exhibition 用户选择的展品
     */
    private void handleExhibitions(IExhibition exhibition) {
        switch (fromType) {
            case ExhibitionsConstant.FROM_GAME:
                ExhibitionsFMS.dispatchExhibitionWithType(
                        fromType,
                        exhibition,
                        PersonalDataHelper.getPersonalData().getSelfPosition(),
                        toPosition,
                        toUserData,
                        displayType
                );
                break;
            case ExhibitionsConstant.FROM_CHAT:
                /**
                 * 从聊天中送礼物
                 */
                ExhibitionsFMS.dispatchExhibition(
                        fromType,
                        exhibition,
                        GlobalUserState.getGlobalState().getUserId(),
                        toUserData
                );
                break;
            case ExhibitionsConstant.FROM_MAIN_PAGE:
            case ExhibitionsConstant.FROM_OUT_SIDER:
                /**
                 * 个人主页进行送礼物操作
                 */
                ExhibitionsFMS.dispatchExhibition(
                        fromType,
                        exhibition,
                        GlobalUserState.getGlobalState().getUserId(),
                        toUserData
                );
                break;
        }

    }

    private void initView(View view) {
        giftViewPager = (ViewPager) view.findViewById(R.id.gift_viewpager);
        giftIndex = (LinearLayout) view.findViewById(R.id.gift_index);
        giftTitle = (TextView) view.findViewById(R.id.gift_title);
        giftLoading = (TextView) view.findViewById(R.id.gift_loading);
        giftRefreshing = view.findViewById(R.id.gift_loading_refresh);
        mIntroduce = (TextView) view.findViewById(R.id.gift_introduce);
        giftRefreshing.setOnClickListener(v -> {
            refresh();
        });
        giftLoading.setVisibility(View.VISIBLE);
        giftViewPager.setVisibility(View.VISIBLE);
        giftIndex.setVisibility(View.VISIBLE);
        giftTitle.setText(MessageUtils.getString(
                R.string.gift_to_somebody, toUserData.userName));

        Utils.runSafely(() -> {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mIntroduce.setText(Html.fromHtml(
                        MessageUtils.getString(R.string.diamond_to_coin_hint, DIM, GOLD), Html.FROM_HTML_MODE_LEGACY, imageGetter, null));
            } else {
                mIntroduce.setText(Html.fromHtml(MessageUtils.getString(R.string.diamond_to_coin_hint, DIM, GOLD), imageGetter, null));
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        PLog.i(TAG, "dismiss()");
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        PLog.i(TAG, "onDismiss()");
        cancelRequestIfNeed();
        RxToolKit.Emit(new ExhibitionsEvent.ExhibitionsShowCaseEvent(ExhibitionsEvent.ExhibitionsShowCaseEvent.ACTION_DISMISS));
        destroy();
    }

    private void cancelRequestIfNeed() {
        if (mCardRequestTask != null) {
            mCardRequestTask.cancel();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        PLog.i(TAG, "onCancel");
        cancelRequestIfNeed();
        RxToolKit.Emit(new ExhibitionsEvent.ExhibitionsShowCaseEvent(ExhibitionsEvent.ExhibitionsShowCaseEvent.ACTION_CANCEL));
    }

    @Override
    public void destroy() {

        cancelRequestIfNeed();

        if (mSafeHandler != null) {
            mSafeHandler.release();
            mSafeHandler = null;
        }

        if (Utils.targetIsNotNull(giftIndexPoints)) {
            giftIndexPoints = null;
        }

        giftTitle = null;
        giftIndex = null;
        giftLoading = null;
        giftRefreshing = null;
        mIntroduce = null;
        giftViewPager = null;
        giftViewPageAdapter = null;


        if (viewList != null) {
            try {
                for (GridView temp : viewList) {
                    ExhibitionsGridViewAdapter adpater = (ExhibitionsGridViewAdapter) temp.getAdapter();
                    adpater.destroy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            viewList.clear();
        }
        isDestroy = true;
        RxToolKit.UnRegister(mFragmentKey);
    }
}
