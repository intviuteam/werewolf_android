package orangelab.project.common.db;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.toolkit.action.Destroyable;

import orangelab.project.common.db.entity.MiniGameAIRecordEntity;
import orangelab.project.common.db.gen.MiniGameAIRecordEntityDao;
import orangelab.project.common.db.model.MiniGameAiRecord;

/**
 * game_werewolf
 * 2018/5/22 下午1:54
 * Mystery
 */
public class MiniGameAIRecordDaoHelper implements Destroyable {

    private static final String TAG = "MiniGameAIRecordDaoHelp";

    private MiniGameAIRecordEntityDao mMiniGameAIRecordEntityDao;

    public MiniGameAIRecordDaoHelper(MiniGameAIRecordEntityDao miniGameAIRecordEntityDao) {
        mMiniGameAIRecordEntityDao = miniGameAIRecordEntityDao;
    }

    public void insertNewRecord(String userId, String userToken, String gameType, int api_level, int winning_streak) {
        if (TextUtils.isEmpty(userId)) return;
        if (TextUtils.isEmpty(userToken)) return;
        if (TextUtils.isEmpty(gameType)) return;

        if (mMiniGameAIRecordEntityDao != null) {
            MiniGameAIRecordEntity entity = new MiniGameAIRecordEntity();
            entity.setUser_id(userId);
            entity.setUser_token(userToken);
            entity.setAi_level(api_level);
            entity.setWinning_streak(winning_streak);
            entity.setGame_type(gameType);
            mMiniGameAIRecordEntityDao.insert(entity);
            PLog.i(TAG, "insert " + entity);
        }
    }

    public void updateAiRecord(String userId, String gameType, int api_level, int winning_streak) {
        if (TextUtils.isEmpty(userId)) return;
        if (TextUtils.isEmpty(gameType)) return;

        if (mMiniGameAIRecordEntityDao != null) {
            MiniGameAIRecordEntity entity = mMiniGameAIRecordEntityDao.queryBuilder().where(
                    MiniGameAIRecordEntityDao.Properties.User_id.eq(userId), MiniGameAIRecordEntityDao.Properties.Game_type.eq(gameType)).build().unique();
            if (entity == null) {
                return;
            }
            entity.setAi_level(api_level);
            entity.setWinning_streak(winning_streak);
            mMiniGameAIRecordEntityDao.update(entity);
            PLog.i(TAG, "update " + entity);
        }
    }

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    public MiniGameAiRecord getAIRecord(String userId, String gameType) {
        if (TextUtils.isEmpty(userId)) return null;
        if (TextUtils.isEmpty(gameType)) return null;

        if (mMiniGameAIRecordEntityDao != null) {
            MiniGameAIRecordEntity entity = mMiniGameAIRecordEntityDao.queryBuilder().where(
                    MiniGameAIRecordEntityDao.Properties.User_id.eq(userId),
                    MiniGameAIRecordEntityDao.Properties.Game_type.eq(gameType)
            ).build().unique();

            if (entity != null) {
                MiniGameAiRecord aiRecord = new MiniGameAiRecord();
                aiRecord.ai_level = entity.getAi_level();
                aiRecord.winning_streak = entity.getWinning_streak();
                PLog.i(TAG, "get " + entity);
                return aiRecord;
            }
            return null;
        }
        return null;
    }

    @Override
    public void destroy() {
        if (mMiniGameAIRecordEntityDao != null) {
            mMiniGameAIRecordEntityDao = null;
        }
    }
}
