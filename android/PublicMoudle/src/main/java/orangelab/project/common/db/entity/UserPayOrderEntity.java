package orangelab.project.common.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2017/8/11 下午1:13
 * Mystery
 * 用户支付订单记录表
 */
@Entity
public class UserPayOrderEntity {
    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    private String userId;//用户的UserId

    private String orderId;//用户订单号

    private String packageName;//包名

    private String productId;//产品ID

    private long purchaseTime; //购买时间

    private String purchaseToken;// 购买的Token

    private String purchaseData;//购买的原数据

    private String signature;//购买数据签名

    private String otherData1;//预留字段1  用于失败次数的校验

    private String otherData2 = "";//预留字段2  用于存放用户的Token

    /**
     * 订单状态
     * 0   支付成功并且金币添加成功
     * -1  支付成功但是金币未添加成功
     */
    private int orderState;

    @Generated(hash = 1629732784)
    public UserPayOrderEntity(Long id, String userId, String orderId,
                              String packageName, String productId, long purchaseTime,
                              String purchaseToken, String purchaseData, String signature,
                              String otherData1, String otherData2, int orderState) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
        this.packageName = packageName;
        this.productId = productId;
        this.purchaseTime = purchaseTime;
        this.purchaseToken = purchaseToken;
        this.purchaseData = purchaseData;
        this.signature = signature;
        this.otherData1 = otherData1;
        this.otherData2 = otherData2;
        this.orderState = orderState;
    }

    @Generated(hash = 2053112215)
    public UserPayOrderEntity() {
    }

    public String getOtherData1() {
        return otherData1;
    }

    public void setOtherData1(String otherData1) {
        this.otherData1 = otherData1;
    }

    public String getOtherData2() {
        return otherData2;
    }

    public void setOtherData2(String otherData2) {
        this.otherData2 = otherData2;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public long getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    public String getPurchaseData() {
        return purchaseData;
    }

    public void setPurchaseData(String purchaseData) {
        this.purchaseData = purchaseData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserPayOrderEntity{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", packageName='" + packageName + '\'' +
                ", productId='" + productId + '\'' +
                ", purchaseTime=" + purchaseTime +
                ", purchaseToken='" + purchaseToken + '\'' +
                ", purchaseData='" + purchaseData + '\'' +
                ", signature='" + signature + '\'' +
                ", otherData1='" + otherData1 + '\'' +
                ", otherData2='" + otherData2 + '\'' +
                ", orderState=" + orderState +
                '}';
    }
}
