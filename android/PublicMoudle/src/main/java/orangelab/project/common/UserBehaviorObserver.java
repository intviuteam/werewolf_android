package orangelab.project.common;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.PLog;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.engine.SocketEngine;
import orangelab.project.minigame.model.MiniGameLaunch;

/**
 * game_werewolf
 * 2018/5/24 上午10:44
 * Mystery
 * <p>
 * 用户统一行为观察器
 */
public class UserBehaviorObserver {

    private static final String TAG = "UserBehavior";

    /**
     * 用户当前意图进入 小游戏 Activity
     *
     * @param context
     * @param miniGameLaunch
     * @return true 则会阻断进入小游戏房间，false 会正常进入小游戏房间
     */
    public static boolean enterMiniGameIntentInterrupt(Context context, MiniGameLaunch miniGameLaunch) {
        PLog.i(TAG, "enterMiniGameIntentInterrupt: ");
        /**
         * 拦截处理
         */
        GlobalService.destroyPrivateRoomInvite(context);

        return false;
    }

    /**
     * 用户当前意图进入 非小游戏的房间
     *
     * @param context
     * @param roomId
     * @param roomType
     * @param roomPassword
     * @return true 会阻塞进入房间，false会正常进入房间
     */
    public static boolean enterRoomIntentInterrupt(Context context, String roomId, String roomType, String roomPassword) {
        PLog.i(TAG, "enterRoomIntentInterrupt: " + roomId + "," + roomType + "," + roomPassword);
        /**
         * 拦截的处理
         */
        if (TextUtils.equals(roomType, Constant.GAME_TYPE_AUDIO)) {
            /**
             * 进入语音房
             */

        } else if (TextUtils.equals(roomType, Constant.GAME_TYPE_PRIVACY)) {
            /**
             * 进入私密房
             */
            GlobalService.destroyPrivateRoomInvite(context);

        } else {
            /**
             * 进入其他房间
             */
            GlobalService.destroyPrivateRoomInvite(context);

        }
        return false;
    }

    /**
     * 当前用户是否在语音房房间
     *
     * @return
     */
    public static boolean isInAudioRoom() {
        return TextUtils.equals(SocketEngine.AUDIO, GlobalUserState.getGlobalState().getCurGameType());
    }

    public static boolean isInPrivacyRoom() {
        return TextUtils.equals(SocketEngine.PRIVACY, GlobalUserState.getGlobalState().getCurGameType());
    }


}
