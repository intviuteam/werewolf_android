package orangelab.project.common.engine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.BuildConfig;
import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.TimeZoneToolKit;
import com.androidtoolkit.UIActuator;
import com.datasource.GlobalUserState;
import com.facebook.react.bridge.Callback;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.TransportConfig;
import com.toolkit.action.Action;

import org.json.JSONObject;

import java.util.Locale;
import java.util.UUID;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.UserBehaviorObserver;
import orangelab.project.common.engine.task.SocketCallBack;
import orangelab.project.common.floatwindow.FloatWindowCommander;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.GameEnterRoom;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.WereWolfGameActivity;
import orangelab.project.spyroom.SpyRoomActivity;
import orangelab.project.spyroom.model.SpyRoomBridgeBean;
import orangelab.project.voice.activity.VoiceRoomActivity;
import orangelab.project.voice.lobby.LobbyRoomActivity;
import orangelab.project.voice.manager.VoiceRoomCommander;
import orangelab.project.voice.model.VoiceToRoomBridgeBean;
import orangelab.project.voice.privateroom.PrivateRoomActivity;
import orangelab.project.voice.privateroom.PrivateRoomContext;

/**
 * Author:1727
 * 2017/3/24
 * 只能存在一个Socket
 */

public class RoomSocketEngine {

    private static final String TAG = "RoomSocketEngine";
    private SocketEngine engine = null;
    private String engineToken = "";    //socketEngine的token
    private Object engineLock = new Object();

    public RoomSocketEngine() {
    }

    public long getServerTime() {
        if (engine == null) {
            return System.currentTimeMillis();
        } else {
            return engine.getServerTime();
        }
    }

    public void setSocketEngineObserver(SocketEngine.SocketEngineObserver socketEngineObserver) {
        synchronized (engineLock) {
            if (engine != null) {
                engine.setSocketEngineObserver(socketEngineObserver);
            }
        }
    }

    public void nativeRequestMessage(final String type, JSONObject payload, final SocketCallBack
            success, final
                                     SocketCallBack failed) {
        if (getEngine() != null) {
            engine.nativeRequestMessage(type, payload, success, failed);
        } else {
            PLog.i(TAG, "sendMessage: Hell! Engine is null");
            PLog.i(TAG, type + ":" + payload);
        }
    }

    public void startPollMessage() {
        if (getEngine() != null) {
            getEngine().startPollMessage();
        }
    }

    public void enterRoom(
            Context context,
            EnterRoomPayload payload,
            final Callback success,
            final Callback failed
    ) {

        releaseCurEngine();

        try {
            final GameEnterRoom.GameEnterRoomItem item = new GameEnterRoom.GameEnterRoomItem();
            item.id = payload.userId;
            item.name = payload.userName;
            item.sex = payload.userSex;
            item.experience = payload.exp;
            item.avatar = payload.avatar;
            final GameEnterRoom.GameExtra extra = new GameEnterRoom.GameExtra();
            extra.pt = "android";
            extra.sv = Integer.parseInt(TransportConfig.SV_VALUE);
            extra.tz = TimeZoneToolKit.GetCurrentTimeZone();
            extra.v = BuildConfig.VERSION_CODE;
            extra.lg = Locale.getDefault().getLanguage();
            extra.invitee = payload.invitee;
            extra.inviter = payload.inviter;
            GameEnterRoom room = new GameEnterRoom();
            if (TextUtils.isEmpty(payload.from)) {
                room.from = "";
            } else {
                room.from = payload.from;
            }
            room.room_id = payload.roomId;
            room.password = payload.password;
            room.token = payload.token;
            room.type = payload.gameType;
            room.user = item;
            room.extra = extra;


            if (TextUtils.isEmpty(payload.socketUrl)) {
                UIActuator.post(() -> {
                    if (failed != null) {
                        failed.invoke(MessageUtils.getString(R.string.string_action_failed));
                    }
                });
                return;
            }

            final JSONObject sendJson = new JSONObject(GsonHelper.getGson().toJson(room));
            SocketEngine engine = null;
            if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_AUDIO)) {

                engine = new SocketEngine(TAG, SocketEngine.AUDIO);

            } else if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_SPY)) {
                engine = new SocketEngine(TAG, SocketEngine.SPY_ROOM);

            } else if (TextUtils.equals(payload.gameType, SocketEngine.PRIVACY)) {

                engine = new SocketEngine(TAG, SocketEngine.PRIVACY);

            } else {

                engine = new SocketEngine(TAG, SocketEngine.WEREWOLF);

            }
            /**
             * Engine创建完毕后生成SocketEngine的唯一Token
             */
            final String engineToken = UUID.randomUUID().toString();
            PLog.i(TAG, "create SocketToken " + engineToken);

            engine.setSocketUrl(payload.socketUrl, payload.gameType, payload.roomId);
            /**
             * 设定Token
             */
            setEngineToken(engineToken);
            /**
             * 设定Engine
             */
            setEngine(engine);

            engine.setSocketEngineOpenListener(new SocketEngine.SocketEngineOpenListener() {
                @Override
                public void onOpen() {
                    nativeRequestMessage(Constant.TYPE_ENTER, sendJson, result -> UIActuator.post(() -> {
                        try {

                            EnterRoomResult enterResult = GsonHelper.getGson().fromJson(result, EnterRoomResult.class);
                            if (UserBehaviorObserver.enterRoomIntentInterrupt(context, enterResult.room.room_id, payload.gameType, enterResult.room.password)) {
                                /**
                                 * 直接阻塞此操作
                                 */
                                if (failed != null) {
                                    failed.invoke(MessageUtils.getString(R.string.string_action_failed));
                                }
                                releaseEngine(engineToken);
                                return;
                            }

                            /**
                             * 在这里仅当自己是游客
                             */
                            if (Utils.checkUserIdIsTourist(GlobalUserState.getGlobalState().getUserId())) {
                                /**
                                 * 自己是游客，根据位置刷新id
                                 */
                                int selfPosition = enterResult.position;
                                if (enterResult.room.users.containsKey(selfPosition)) {
                                    EnterRoomResult.EnterRoomUserItem self = enterResult.room.users.get(selfPosition);
                                    if (self != null) {
                                        GlobalUserState.getGlobalState().setUserId(self.id);
                                    }
                                } else if (enterResult.room.observers.containsKey(selfPosition)) {
                                    EnterRoomResult.EnterRoomUserItem self = enterResult.room.observers.get(selfPosition);
                                    if (self != null) {
                                        GlobalUserState.getGlobalState().setUserId(self.id);
                                    }
                                }
                            }

                            if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_AUDIO)) {
                                if (TextUtils.equals(enterResult.room.child_type, Constant.GAME_TYPE_KTV)) {
                                    VoiceRoomActivity.Launch(context, payload.gameType, payload.password, new VoiceToRoomBridgeBean(enterResult, false, engineToken));
                                } else {
                                    VoiceRoomActivity.Launch(context, payload.gameType, payload.password, new VoiceToRoomBridgeBean(enterResult, false, engineToken));
                                }
                            } else if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_PRIVACY)) {
                                /**
                                 * 私密房添加Token
                                 */
                                VoiceRoomCommander.ContextManager.INSTANCE.InitPrivateRoomContext(engineToken, enterResult);
                                PrivateRoomActivity.Companion.Launch(context);
                            } else if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_SPY)) {
                                SpyRoomActivity.Companion.launch(context, new SpyRoomBridgeBean(payload.gameType, payload.password, engineToken, enterResult));
                            } else if (TextUtils.equals(payload.gameType, Constant.GAME_TYPE_PAIR)) {
                                LobbyRoomActivity.Launch(context, payload.gameType, payload.password, new VoiceToRoomBridgeBean(enterResult, false, engineToken));
                            } else {
                                WereWolfGameActivity.Launch(context, payload.gameType, payload.password, enterResult, engineToken);
                            }

                            /**
                             * 启动Activity之后进行回调，下面的代码不可能抛出Exception
                             */
                            if (success != null) {
                                success.invoke(result);
                            }

                        } catch (Exception e) {
                            PLog.e(TAG, "error: " + e.getMessage());
                            e.printStackTrace();

                            if (failed != null) {
                                failed.invoke(MessageUtils.getString(R.string.string_action_failed));
                            }

                            releaseEngine(engineToken);
                        }
                    }), result -> UIActuator.post(() -> {

                        if (failed != null) {
                            failed.invoke(result);
                        }

                        releaseEngine(engineToken);

                    }));
                }

                @Override
                public void onError() {
                    UIActuator.post(() -> {
                        if (failed != null) {
                            failed.invoke(MessageUtils.getString(R.string.network_is_unavailable));
                        }
                        releaseEngine(engineToken);
                    });
                }
            });
            engine.openSocketEngine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void restoreRoom(Context context, boolean needHandOver, Action<Exception> restoreResult) {
        if (getEngine() == null) {
            /**
             * 出了意外情况，当前socket被清理掉了
             */
            if (restoreResult != null) {
                restoreResult.func(new Exception(MessageUtils.getString(R.string.string_float_enter_failed1)));
            }
            return;
        }
        if (getEngine().isSocketError()) {
            /**
             * 当前的socket连通性出了问题
             */
            if (restoreResult != null) {
                restoreResult.func(new Exception(MessageUtils.getString(R.string.string_float_enter_failed2)));
            }
            return;
        }

        PrivateRoomContext privateRoomContext = VoiceRoomCommander.ContextManager.INSTANCE.GetPrivateRoomContext();
        if (privateRoomContext != null) {
            /**
             * 当前小窗模式是私密房
             */
            FloatWindowCommander.StopService(context, null);
            PrivateRoomActivity.Companion.Launch(context);
            return;
        }

        SocketEngine engine = getEngine();
        engine.nativeRequestMessage("reenter", null
                , result -> {
                    EnterRoomResult enterResult = GsonHelper.getGson().fromJson(result, EnterRoomResult.class);
                    Intent intent = null;
                    if (TextUtils.equals(enterResult.room.config.type, Constant.GAME_TYPE_AUDIO)) {
                        if (TextUtils.equals(enterResult.room.child_type, Constant.GAME_TYPE_KTV)) {
                            VoiceRoomActivity.Launch(context,
                                    enterResult.room.config.type,
                                    enterResult.room.config.room_info.password,
                                    new VoiceToRoomBridgeBean(enterResult, true, needHandOver, engineToken));
                        } else {
                            VoiceRoomActivity.Launch(context,
                                    enterResult.room.config.type,
                                    enterResult.room.config.room_info.password,
                                    new VoiceToRoomBridgeBean(enterResult, true, needHandOver, engineToken));
                        }
                    } else {
                        return;//狼人杀不支持 reenter
                    }
                    FloatWindowCommander.StopService(context, null);
                }
                , result -> {
                    if (restoreResult != null) {
                        restoreResult.func(new Exception(result));
                    }
                });
    }

    private void releaseCurEngine() {
        synchronized (engineLock) {
            if (getEngine() != null) {
                engine.release();
                engine = null;
                PLog.i(TAG, "releaseEngine: release Success!!");
            }
        }
    }

    public void releaseEngine(String token) {
        PLog.i(TAG, "want releaseEngine " + token);
        if (TextUtils.equals(token, getEngineToken())) {
            synchronized (engineLock) {
                releaseCurEngine();
            }
        }
    }

    public SocketEngine getEngine() {
        SocketEngine result = null;
        synchronized (engineLock) {
            result = engine;
        }
        return result;
    }

    public String getEngineToken() {
        String token = "";
        synchronized (engineLock) {
            token = engineToken;
        }
        return token;
    }

    private void setEngineToken(String token) {
        synchronized (engineLock) {
            this.engineToken = token;
        }
    }

    private void setEngine(SocketEngine engine) {
        synchronized (engineLock) {
            this.engine = engine;
        }
    }

}
