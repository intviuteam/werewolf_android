package orangelab.project.common.pay.google.v2;

import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.project.MainApplication;
import orangelab.project.common.db.DBConstant;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.google.GooglePayUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;
import rx.Observable;
import rx.Subscriber;

/**
 * game_werewolf
 * 2018/6/20 下午2:30
 * Mystery
 * <p>
 * Google异常订单汇报任务
 * <p>
 * 每次执行任务会将app中数据库存放的异常订单向服务器进行汇报
 */
public class GooglePayV2OrderTask implements Runnable, Destroyable {

    private static final String TAG = "GooglePayV2OrderTask";

    private UserPayOrderDaoV2Helper mDaoV2Helper;

    public GooglePayV2OrderTask() {
        mDaoV2Helper = MainApplication.getInstance().getUserPayOrderDaoV2Helper();
    }

    @Override
    public void run() {
        Log.i(TAG, "run: begin googlePayV2OrderTask ");
        if (mDaoV2Helper != null) {
            List<UserPayOrderEntity> entities = mDaoV2Helper.getAllUserOrderEntity();
            if (Utils.targetListIsNull(entities)) {
                Log.i(TAG, "no error order");
                destroy();
                return;
            }
            final int taskLength = entities.size();
            if (taskLength == 0) {
                /**
                 * 无须执行任务直接 释放资源
                 */
                Log.i(TAG, "Task length is 0");
                destroy();
                return;
            }
            Log.i(TAG, "begin Async Task");
            Observable.from(entities)
                    .onBackpressureBuffer()
                    .subscribe(new Subscriber<UserPayOrderEntity>() {
                        @Override
                        public void onStart() {
                            Log.i(TAG, "onStart: ");
                        }

                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: " + e.getMessage());
                        }

                        @Override
                        public void onNext(UserPayOrderEntity entity) {
                            /**
                             * 进行app内的清单请求
                             */
                            if (entity == null) {
                                request(1);
                                return;
                            }

                            if (entity.getOrderState() == DBConstant.PAY_SUCCESS_BUT_NOT_REPORT_SERVER_SUBS) {
                                /**
                                 * 进行订阅的失败订单处理
                                 */
                                try {

                                    JSONObject jsonObject = new JSONObject(entity.getPurchaseData());

                                    JSONObject requestJson = new JSONObject();

                                    requestJson.put("receipt", jsonObject);

                                    Log.i(TAG, "start subs RequestTask");
                                    String token = entity.getOtherData2();
                                    if (TextUtils.isEmpty(token)) {
                                        /**
                                         * Token 为空上报异常订单
                                         */
                                        Log.i(TAG, "Token is Null report");
                                        reportErrorOrder("TokenNull", entity);
                                        request(1);
                                        return;
                                    }
                                    RequestTask.create().isGet(false).withToken(token).request(Constant.GOOGLE_SUBS_PAY).load(requestJson)
                                            .action(new RequestTaskCallBack() {
                                                @Override
                                                public void onSuccess(String response) {
                                                    if (mDaoV2Helper != null) {
                                                        mDaoV2Helper.deleteData(entity.getUserId(), entity.getOrderId());
                                                        Log.i(TAG, "subs order deleteFinish");
                                                    }
                                                    request(1);
                                                }

                                                @Override
                                                public void onFailed(int code, String failed) {
                                                    reportErrorOrder("subs order ServerError", entity);
                                                    request(1);
                                                }

                                                @Override
                                                public void onError(Exception e) {
                                                    reportErrorOrder("subs order ServerError", entity);
                                                    request(1);
                                                }
                                            }).build().executeInCurThread();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    reportErrorOrder("subs order ServerError", entity);
                                    request(1);
                                }
                            } else {
                                /**
                                 * 进行普通的失败订单处理
                                 */
                                PLog.i(TAG, "start entity: " + entity);
                                if (intEqualString(entity.getOtherData1(), Constant.MAX_RETRY)) {
                                    Log.i(TAG, "retry has been max so ingore it");
                                    reportErrorOrder("RetryMax", entity);
                                    request(1);
                                    return;
                                }
                                JSONObject json = new JSONObject();
                                try {
                                    json.put(Constant.SIGNATURE, entity.getSignature());
                                    json.put(Constant.DATA, entity.getPurchaseData());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.i(TAG, "start RequestTask");
                                String token = entity.getOtherData2();
                                if (TextUtils.isEmpty(token)) {
                                    /**
                                     * Token 为空上报异常订单
                                     */
                                    Log.i(TAG, "Token is Null report");
                                    reportErrorOrder("TokenNull", entity);
                                    request(1);
                                    return;
                                }
                                RequestTask.create().request(Constant.GOOGLE_PAY).load(json).withToken(token).isGet(false)
                                        .action(new RequestTaskCallBack() {
                                            @Override
                                            public void onSuccess(String response) {
                                                Log.i(TAG, response);
                                                PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                                                PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                                switch (item.getOp_result()) {
                                                    case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                                        Log.i(TAG, "Server pay success");
                                                        if (mDaoV2Helper != null) {
                                                            mDaoV2Helper.deleteData(entity.getUserId(), entity.getOrderId());
                                                            Log.i(TAG, "deleteFinish");
                                                        }
                                                    }
                                                    break;
                                                    default: {
                                                        Log.i(TAG, "Server pay failed");
                                                        entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                                        if (mDaoV2Helper != null) {
                                                            mDaoV2Helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                                        }
                                                        Log.i(TAG, "Server pay update record retry count");
                                                        reportErrorOrder("ServerError", entity);
                                                    }
                                                    break;
                                                }
                                                request(1);
                                            }

                                            @Override
                                            public void onFailed(int code, String failed) {
                                                Utils.runSafely(() -> {
                                                    entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                                    if (mDaoV2Helper != null) {
                                                        mDaoV2Helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                                    }
                                                });
                                                reportErrorOrder("ServerError", entity);
                                                request(1);
                                            }

                                            @Override
                                            public void onError(Exception e) {
                                                Utils.runSafely(() -> {
                                                    entity.setOtherData1(intStringAdd(entity.getOtherData1()));
                                                    if (mDaoV2Helper != null) {
                                                        mDaoV2Helper.updateDataOtherData(entity.getUserId(), entity.getOrderId(), entity.getOtherData1());
                                                    }
                                                });
                                                reportErrorOrder("RequestException", entity);
                                                request(1);
                                            }
                                        }).build().executeInCurThread();
                            }
                        }
                    });
            Log.i(TAG, "end Async Task");
        }
        Log.i(TAG, "run: end googlePayV2OrderTask ");
    }

    public boolean intEqualString(String targetString, int targetInt) {
        try {
            return Integer.parseInt(targetString) == targetInt;
        } catch (Exception e) {
            return false;
        }
    }

    public String intStringAdd(String target) {
        try {
            if (TextUtils.isEmpty(target)) {
                return "1";
            }
            int result = Integer.parseInt(target);
            result++;
            return Integer.toString(result);
        } catch (Exception e) {
            return target;
        }
    }

    public static void reportErrorOrder(String reason, UserPayOrderEntity entity) {
        if (Utils.targetIsNull(entity)) {
            return;
        }
        Utils.runSafely(() -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("ErrorOrder\n");
            stringBuilder.append("Reason: " + reason);
            stringBuilder.append("OrderId:" + entity.getOrderId() + "\n");
            stringBuilder.append("UserId: " + entity.getUserId() + "\n");
            stringBuilder.append("Data:" + entity.toString());
            ReportEventUtils.reportException(stringBuilder.toString());
        });
    }

    @Override
    public void destroy() {
        mDaoV2Helper = null;
    }
}
