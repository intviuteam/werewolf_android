package orangelab.project.common.engine;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/2/7 下午5:52
 * Mystery
 */

public class SocketEngineConfig implements Keepable {

    public String type;
    public String level;
    public String server;
    public String extraData;
}
