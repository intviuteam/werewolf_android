package orangelab.project.common.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.R;
import orangelab.project.common.utils.CountDownTaskManager;
import orangelab.project.common.utils.MessageUtils;

/**
 * Author:1727
 * 2017/3/23
 */

public class MsgDialog extends BaseCustomDialog {
    private CountDownTaskManager.CountDownTask task;

    public MsgDialog(Context context, String title, String msg, View.OnClickListener confirm) {
        super(context);
        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_game_dialog_title, null);
        TextView titleText = (TextView) titleView.findViewById(R.id.dialog_title);
        titleText.setText(title);
        addTitleView(titleView);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_msg_dialog, null);
        TextView textView = (TextView) view.findViewById(R.id.msg);
        addContentView(view);
        textView.setText(msg);
        setButtonType(ONE_BUTTON);
        setOnlyButtonListener(confirm);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    public MsgDialog(
            Context context, String title, String msg,
            View.OnClickListener confirm,
            View.OnClickListener cancel, boolean isGaming) {
        super(context);
        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_game_dialog_title, null);
        TextView titleText = (TextView) titleView.findViewById(R.id.dialog_title);
        titleText.setText(title);
        addTitleView(titleView);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_msg_dialog, null);
        TextView textView = (TextView) view.findViewById(R.id.msg);
        addContentView(view);
        textView.setText(msg);
        setButtonType(TWO_BUTTON);
        setCancelListener(cancel);
        setConfirmListener(confirm);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        final Button confirmButton = getButtonConfirm();
        if (isGaming) {
            confirmButton.setEnabled(false);
            task = new CountDownTaskManager.CountDownTask(5, new CountDownTaskManager.CountDownTaskListener() {
                @Override
                public void onTick(int seconds) {
                    confirmButton.setText(MessageUtils.getString(R.string.do_confirm) + "(" + seconds + ")");
                }

                @Override
                public void onStart(int startSeconds) {
                    confirmButton.setText(MessageUtils.getString(R.string.do_confirm) + "(" + startSeconds + ")");
                }

                @Override
                public void onFinish() {
                    confirmButton.setEnabled(true);
                    confirmButton.setText(MessageUtils.getString(R.string.do_confirm));
                }
            });
            task.startTask();
        }
    }

    public MsgDialog(Context context, MsgDialogBuilder builder) {
        super(context);

        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_game_dialog_title, null);

        TextView titleText = (TextView) titleView.findViewById(R.id.dialog_title);
        titleText.setText(builder.getTitle());
        addTitleView(titleView);

        View view = LayoutInflater.from(context).inflate(R.layout.layout_msg_dialog, null);
        TextView textView = (TextView) view.findViewById(R.id.msg);
        addContentView(view);

        textView.setText(builder.getMessage());

        setButtonType(TWO_BUTTON);
        setButtonCancelText(builder.getLeftButtonText());
        setButtonConfirmText(builder.getRightButtonText());

        if (builder.isAutoDismiss()) {
            setCancelListener(v -> {
                builder.getLeftClick().onClick(v);
                dismiss();
            });
            setConfirmListener(v -> {
                builder.getRightClick().onClick(v);
                dismiss();
            });
        } else {
            setCancelListener(builder.getLeftClick());
            setConfirmListener(builder.getRightClick());
        }
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (task != null) {
            task.cancelTask();
        }
    }

    public static class MsgDialogBuilder {
        private String title;
        private String message;
        private String leftButtonText;
        private String rightButtonText;
        private boolean autoDismiss = false;
        private View.OnClickListener leftClick;
        private View.OnClickListener rightClick;

        private MsgDialogBuilder() {
        }

        public String getTitle() {
            return title;
        }

        public MsgDialogBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public MsgDialogBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public String getLeftButtonText() {
            return leftButtonText;
        }

        public MsgDialogBuilder setLeftButtonText(String leftButtonText) {
            this.leftButtonText = leftButtonText;
            return this;
        }

        public String getRightButtonText() {
            return rightButtonText;
        }

        public MsgDialogBuilder setRightButtonText(String rightButtonText) {
            this.rightButtonText = rightButtonText;
            return this;
        }

        public boolean isAutoDismiss() {
            return autoDismiss;
        }

        public MsgDialogBuilder setAutoDismiss(boolean autoDismiss) {
            this.autoDismiss = autoDismiss;
            return this;
        }

        public View.OnClickListener getLeftClick() {
            return leftClick;
        }

        public MsgDialogBuilder setLeftClick(View.OnClickListener leftClick) {
            this.leftClick = leftClick;
            return this;
        }

        public View.OnClickListener getRightClick() {
            return rightClick;
        }

        public MsgDialogBuilder setRightClick(View.OnClickListener rightClick) {
            this.rightClick = rightClick;
            return this;
        }

        public static MsgDialogBuilder build() {
            return new MsgDialogBuilder();
        }
    }
}
