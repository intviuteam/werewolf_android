package orangelab.project.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2017/7/6 下午6:12
 * Mystery
 */

public abstract class SafeDialog extends Dialog {

    private SafeHandler mHandler;
    private boolean isDestroy = false;
    private boolean actionBlock = false;

    public boolean isActionBlock() {
        return actionBlock;
    }

    public void setActionBlock(boolean actionBlock) {
        this.actionBlock = actionBlock;
    }

    public SafeDialog(@NonNull Context context) {
        super(context);
        init();
    }

    public SafeDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        init();
    }

    protected SafeDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    protected void resizeDialog(int width, int height) {
        android.view.WindowManager.LayoutParams params = getWindow()
                .getAttributes();
        params.width = width;
        params.height = height;
        getWindow().setAttributes(params);
    }

    protected void runSafely(Runnable runnable, long delay) {
        mHandler.postDelaySafely(runnable, delay);
    }

    protected void runSafely(Runnable runnable) {
        runSafely(runnable, 0);
    }

    private void init() {
        mHandler = new SafeHandler(Looper.getMainLooper());
    }

    private void destroy() {
        if (!isDestroy) {
            mHandler.release();
            release();
            isDestroy = true;
        }
    }

    public boolean isDestroy() {
        return isDestroy;
    }

    protected void showKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放资源
     */
    protected abstract void release();

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        destroy();
    }
}
