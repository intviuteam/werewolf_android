package orangelab.project.common.effect.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.R;
import com.toolkit.ToolKit;

import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.EffectsUtils;
import orangelab.project.common.effect.frames.BitmapFramesAnim;
import orangelab.project.common.utils.BitmapDecodeUtils;

/**
 * game_werewolf
 * 2018/7/31 上午10:37
 * Mystery
 * <p>
 * 房间内骰子效果
 */
public class RoomDiceView extends FrameLayout {

    private static class DiceAnimFrame extends BitmapFramesAnim<Integer> {

        public DiceAnimFrame(ImageView imageView) {
            super(imageView);
        }

        @Override
        protected Bitmap beforeFrameToBitmap(Integer integer) {
            return null;
        }

        @Override
        protected Bitmap onFrameToBitmap(Context context, Integer integer) {
            return BitmapFactory.decodeResource(context.getResources(), integer);
        }

        @Override
        protected Bitmap afterFrameToBitmap(Integer integer, Bitmap bitmap) {
            return bitmap;
        }
    }

    private ImageView mDice; //5个骰子
    private int mDiceHeight;
    private int mDiceWidth;
    private int mDiceMargin = 0;
    private ImageView mDiceHolder; //骰盅

    private int mDiceHolderWidth = 0;
    private int mDiceHolderHeight = 0;

    private List<Animator> mAnimatorRecord;
    private List<Rect> mDiceRects = new ArrayList<>(5);

    private DiceAnimFrame mDicesAnim;

    private boolean withDiceBox = false;
    private boolean hasOpen = false;

    public RoomDiceView(@NonNull Context context) {
        this(context, null);
    }

    public RoomDiceView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomDiceView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setClipChildren(false);
        mAnimatorRecord = new ArrayList<>(3);
    }

    /**
     * 计算骰子的宽高
     *
     * @param availableWidth
     * @param availableHeight
     */
    private void calculateDice(final int availableWidth, final int availableHeight) {

        /**
         * UI给出的骰子比例为99：66
         */

        /**
         * 先以宽度为标准
         */
        int temp1 = availableWidth;

        int wantHeight = 66 * temp1 / 99;

        if (wantHeight > availableHeight) {
            /**
             * 以宽度为基准，但是高度不够，更改为以高度为基准
             */
            int temp2 = availableHeight;
            int wantWidth = 99 * temp2 / 66;

            if (wantWidth > availableWidth) {
                /**
                 * 无论以那条边作为基准都是有问题的，那么不在进行协商，直接设定宽高
                 */
                mDiceHeight = availableHeight;
                mDiceWidth = availableWidth;
            } else {
                /**
                 * 宽高合适
                 */
                mDiceWidth = wantWidth;
                mDiceHeight = availableHeight;
            }
        } else {
            /**
             * 宽高合适
             */
            mDiceHeight = wantHeight;
            mDiceWidth = availableWidth;
        }

        mDiceMargin = (mDiceWidth / 99) * 2;

        /**
         * 计算各个骰子的Rect区域
         */
        final int littleDiceWidth = mDiceHeight / 2;

        /**
         * 第1个骰子
         */
        int rect1Left = (mDiceWidth - ((2 * littleDiceWidth) + mDiceMargin)) / 2;
        Rect rect1 = new Rect(rect1Left, 0, rect1Left + littleDiceWidth, 0 + littleDiceWidth);
        mDiceRects.add(rect1);
        /**
         * 第2个骰子
         */
        int rect2Left = rect1.right + mDiceMargin;
        Rect rect2 = new Rect(rect2Left, rect1.top, rect2Left + littleDiceWidth, rect1.bottom);
        mDiceRects.add(rect2);
        /**
         * 第3个骰子
         */
        int rect3Left = 0;
        Rect rect3 = new Rect(rect3Left, rect1.bottom, rect3Left + littleDiceWidth, mDiceHeight);
        mDiceRects.add(rect3);
        /**
         * 第4个骰子
         */
        int rect4Left = rect3.right + mDiceMargin;
        Rect rect4 = new Rect(rect4Left, rect3.top, rect4Left + littleDiceWidth, rect3.bottom);
        mDiceRects.add(rect4);
        /**
         * 第5个骰子
         */
        int rect5Left = rect4.right + mDiceMargin;
        Rect rect5 = new Rect(rect5Left, rect3.top, mDiceWidth, rect3.bottom);
        mDiceRects.add(rect5);

    }

    public void initDice(int width, int height, boolean needDiceHolder) {
        /**
         * 初始化骰子与骰盅
         */
        int diceAvailableWidth = (int) (width * 0.85f);
        int diceAvailableHeight = (int) (height * 0.5f);

        mDice = new ImageView(getContext());
        mDice.setScaleType(ImageView.ScaleType.FIT_XY);

        calculateDice(diceAvailableWidth, diceAvailableHeight);

        LayoutParams diceParams = new LayoutParams(mDiceWidth, mDiceHeight);
        diceParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        mDice.setLayoutParams(diceParams);
        addView(mDice);

        if (needDiceHolder) {
            withDiceBox = true;
            mDiceHolder = new ImageView(getContext());
            mDiceHolder.setScaleType(ImageView.ScaleType.FIT_XY);
            mDiceHolderWidth = width;
            mDiceHolderHeight = height;
            LayoutParams layoutParams = new LayoutParams(width, height);
            mDiceHolder.setLayoutParams(layoutParams);
            addView(mDiceHolder);
        }
    }

    public Bitmap createDiceBitmap(List<Integer> dices) {
        if (ToolKit.isEmpty(dices)) return null;

        if (dices.size() != 5) return null;

        /**
         * 创建一个bitmap
         */
        Bitmap bitmap = Bitmap.createBitmap(mDiceWidth, mDiceHeight, Bitmap.Config.ARGB_4444);
        /**
         * 创建canvas
         */
        Canvas canvas = new Canvas(bitmap);
        /**
         * 开始绘制骰子
         */
        Resources resources = getResources();
        for (int i = 0; i < dices.size(); i++) {
            int diceValue = dices.get(i);
            Rect rect = mDiceRects.get(i);
            int resourceId = ApkResourceManager.GetApkDrawableResource(getContext(), "dice_" + diceValue);
            if (resourceId > 0) {
                canvas.drawBitmap(BitmapFactory.decodeResource(resources, resourceId), null, rect, null);
            } else {
                return null;
            }
        }
        return bitmap;
    }

    public void setDiceHolderBackGround(int resource) {
        if (mDiceHolder != null) {
            mDiceHolder.setImageResource(resource);
        }
    }

    public boolean isHasOpen() {
        return hasOpen;
    }

    public boolean isWithDiceBox() {
        return withDiceBox;
    }

    /**
     * 没有筛盅开始搅动
     */
    public void startDiceRock() {
        if (mDicesAnim != null && mDicesAnim.isRunning()) {
            mDicesAnim.stop();
        }
        mDicesAnim = new DiceAnimFrame(mDice);
        List<Integer> anims = new ArrayList<>();
        anims.add(R.drawable.dice_anim_1);
        anims.add(R.drawable.dice_anim_2);
        anims.add(R.drawable.dice_anim_3);
        anims.add(R.drawable.dice_anim_4);
        anims.add(R.drawable.dice_anim_5);
        mDicesAnim.setFrames(anims);
        mDicesAnim.setRepeat(true);
        mDicesAnim.setInterval(150);
        mDicesAnim.start();
    }

    public void stopDiceRock(List<Integer> dis) {
        hasOpen = true;
        mDicesAnim.setListener(new BitmapFramesAnim.BitmapFramesAnimListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onStop() {

            }

            @Override
            public void onRelease() {
                mDice.setImageBitmap(createDiceBitmap(dis));
            }
        });
        mDicesAnim.stop();
    }

    /**
     * 开始进行骰盅
     */
    public void startDiceHolderRock() {
        Animator animator = EffectsUtils.createShakeAnimator(mDiceHolder);
        animator.start();
        mAnimatorRecord.add(animator);
    }

    /**
     * 打开骰盅
     */
    public void openDiceHolder(List<Integer> dis) {
        hasOpen = true;
        clearAnimator();
        mDice.setImageBitmap(createDiceBitmap(dis));
        Animator animator = EffectsUtils.combineWithAnimator(
                EffectsUtils.createAlphaOut(mDiceHolder, 500),
                EffectsUtils.createTranslationY(mDiceHolder, 0, -mDiceHolderHeight, 500)
        );
        animator.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        clearAnimator();
        if (mDicesAnim != null) {
            mDicesAnim.release();
            mDicesAnim = null;
        }
    }

    private void clearAnimator() {
        for (Animator animator : mAnimatorRecord) {
            animator.cancel();
        }
        mAnimatorRecord.clear();
    }

}
