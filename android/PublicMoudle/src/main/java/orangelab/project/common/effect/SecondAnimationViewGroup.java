package orangelab.project.common.effect;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import orangelab.project.common.exhibition.gift.giftboard.GiftBoardManager;

/**
 * game_werewolf
 * 2017/11/10 下午1:35
 * Mystery
 */

public class SecondAnimationViewGroup extends FrameLayout implements IBaseAnimationSurface {

    private GiftBoardManager mGiftBoardManager;

    public SecondAnimationViewGroup(@NonNull Context context) {
        this(context, null);
    }

    public SecondAnimationViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SecondAnimationViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mGiftBoardManager = new GiftBoardManager(context, this);
    }

    @Override
    public void destroy() {
        if (mGiftBoardManager != null) {
            mGiftBoardManager.destroy();
        }
    }
}
