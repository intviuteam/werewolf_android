package orangelab.project.common.exhibition.card;

import android.graphics.PointF;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.utils.WereWolfString;
import orangelab.project.common.model.CardListResult;
import orangelab.project.common.utils.ActionsDetector;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;

import com.networktoolkit.transport.Constant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/6/28 下午4:23
 * Mystery
 */

public class CardUtils {

    private static final String TAG = "GameCardManager";


    public static int getCardBgResource(String cardType) {
        int result = -1;
        switch (cardType) {
            case ExhibitionsConstant.CARD_DELAY_TIME:
                result = R.mipmap.room_card_delay;
                break;
            case ExhibitionsConstant.CARD_CHECK:
                result = R.mipmap.room_card_check;
                break;
            case ExhibitionsConstant.CARD_CHECKER:
                result = R.mipmap.room_card_checker;
                break;
            case ExhibitionsConstant.CARD_CUPID:
                result = R.mipmap.room_card_cupid;
                break;
            case ExhibitionsConstant.CARD_HUNTER:
                result = R.mipmap.room_card_hunter;
                break;
            case ExhibitionsConstant.CARD_PEOPLE:
                result = R.mipmap.room_card_people;
                break;
            case ExhibitionsConstant.CARD_WITCH:
                result = R.mipmap.room_card_witch;
                break;
            case ExhibitionsConstant.CARD_WOLF:
                result = R.mipmap.room_card_wolf;
                break;
            case ExhibitionsConstant.CARD_WEREWOLF_KING:
                result = R.mipmap.room_card_werewolf_king;
                break;
            case ExhibitionsConstant.CARD_GUARD:
                result = R.mipmap.room_card_guard;
                break;
            case WereWolfConstant.ROLE_DEMON:
                result = R.mipmap.room_card_demon;
                break;
            case WereWolfConstant.ROLE_MAGICIAN:
                result = R.mipmap.room_card_magican;
                break;
            case WereWolfConstant.ROLE_KNIGHT:
                result = R.mipmap.room_card_knight;
                break;
            case WereWolfConstant.ROLE_BLACK_WOLF_KING:
                result = R.mipmap.room_card_black_wolf_king;
                break;
        }
        return result;
    }

    public static CardAction getCardAction(String cardType) {
        CardAction cardAction = null;
        switch (cardType) {
            case ExhibitionsConstant.CARD_DELAY_TIME:
                cardAction = new CardAction() {
                    @Override
                    public void onClick(View v) {
                        ActionsDetector.cancel(ActionsDetector.ACTION_SPEECH_CHECK);
                        RoomSocketEngineHelper.sendEmptyType(Constant.APPEND_TIME);
                    }
                };
                cardAction.setDisposable(false);
                break;
            case ExhibitionsConstant.CARD_CHECK:
                cardAction = new CardAction() {
                    @Override
                    public void onClick(View v) {
                        RxToolKit.Emit(new CardEvent.UseCardCheckEvent());
                    }
                };
                cardAction.setDisposable(false);
                break;
            case ExhibitionsConstant.CARD_CHECKER:
                break;
            case ExhibitionsConstant.CARD_CUPID:
                break;
            case ExhibitionsConstant.CARD_HUNTER:
                break;
            case ExhibitionsConstant.CARD_PEOPLE:
                break;
            case ExhibitionsConstant.CARD_WITCH:
                break;
            case ExhibitionsConstant.CARD_WOLF:
                break;
        }
        return cardAction;
    }

    public static boolean IsCard(String card) {
        if (TextUtils.isEmpty(card))
            return false;
        switch (card) {
            case ExhibitionsConstant.CARD_DELAY_TIME:
            case ExhibitionsConstant.CARD_CHECK:
            case ExhibitionsConstant.CARD_CHECKER:
            case ExhibitionsConstant.CARD_CUPID:
            case ExhibitionsConstant.CARD_HUNTER:
            case ExhibitionsConstant.CARD_PEOPLE:
            case ExhibitionsConstant.CARD_WITCH:
            case ExhibitionsConstant.CARD_WOLF:
            case ExhibitionsConstant.CARD_GUARD:
            case ExhibitionsConstant.CARD_WEREWOLF_KING:
            case WereWolfConstant.ROLE_DEMON:
            case WereWolfConstant.ROLE_MAGICIAN:
            case WereWolfConstant.ROLE_BLACK_WOLF_KING:
            case WereWolfConstant.ROLE_KNIGHT:
                return true;
        }
        return false;
    }

    public static String getCardName(String cardType) {
        String result = null;
        switch (cardType) {
            case ExhibitionsConstant.CARD_DELAY_TIME:
                result = MessageUtils.getString(R.string.card_delay_time);
                break;
            case ExhibitionsConstant.CARD_CHECK:
                result = MessageUtils.getString(R.string.card_check);
                break;
            case ExhibitionsConstant.CARD_CHECKER:
                result = MessageUtils.getString(R.string.card_checker);
                break;
            case ExhibitionsConstant.CARD_CUPID:
                result = MessageUtils.getString(R.string.card_cupid);
                break;
            case ExhibitionsConstant.CARD_HUNTER:
                result = MessageUtils.getString(R.string.card_hunter);
                break;
            case ExhibitionsConstant.CARD_PEOPLE:
                result = MessageUtils.getString(R.string.card_people);
                break;
            case ExhibitionsConstant.CARD_WITCH:
                result = MessageUtils.getString(R.string.card_witch);
                break;
            case ExhibitionsConstant.CARD_WOLF:
                result = MessageUtils.getString(R.string.card_wolf);
                break;
            case ExhibitionsConstant.CARD_GUARD:
                result = MessageUtils.getString(R.string.card_guard);
                break;
            case ExhibitionsConstant.CARD_WEREWOLF_KING:
                result = MessageUtils.getString(R.string.card_werewolf_king);
                break;
            case WereWolfConstant.ROLE_DEMON:
                result = WereWolfString.getString(R.string.werewolf_role_card_demon);
                break;
            case WereWolfConstant.ROLE_MAGICIAN:
                result = WereWolfString.getString(R.string.werewolf_role_card_magican);
                break;
            case WereWolfConstant.ROLE_BLACK_WOLF_KING:
                result = MessageUtils.getString(R.string.werewolf_role_card_black_wolf_king);
                break;
            case WereWolfConstant.ROLE_KNIGHT:
                result = MessageUtils.getString(R.string.werewolf_role_card_knight);
                break;
        }
        return result;
    }

    public static PointF getCardPosition(String cardType) {
        PointF result = null;
        switch (cardType) {
            /**
             * 延时卡位置
             */
            case ExhibitionsConstant.CARD_DELAY_TIME:
                result = new PointF(
                        ScreenUtils.getScreenWidth() / 2 - Card.CARD_DEFAULT_WIDTH / 2,
                        ScreenUtils.getScreenHeight() - ScreenUtils.dip2px(115) - Card.CARD_DEFAULT_HEIGHT
                );
                break;
            /**
             * 查验卡位置
             */
            case ExhibitionsConstant.CARD_CHECK:
                result = new PointF(
                        ScreenUtils.getScreenWidth() / 2 - Card.CARD_DEFAULT_WIDTH / 2,
                        ScreenUtils.getScreenHeight() - ScreenUtils.dip2px(30) - Card.CARD_DEFAULT_HEIGHT
                );
                break;
        }
        return result;
    }

    public static void fillImageViewByCardType(ImageView imageView, String cardType) {
        int result = getCardBgResource(cardType);
        if (result != -1) {
            imageView.setImageResource(result);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    public static void fillImageViewByCardRoleType(ImageView imageView, String cardType) {
        int result = MessageUtils.getRoleImage(cardType);
        if (result != -1) {
            imageView.setImageResource(result);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

    public static List<Card> cardListResultTurnToList(CardListResult cardListResult) {
        List<Card> result = new ArrayList<>(0);
        if (cardListResult != null && cardListResult.cards != null && cardListResult.cards.size() > 0) {
            Iterator iterator = cardListResult.cards.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, CardListResult.CardListResultItem> entry =
                        (Map.Entry<String, CardListResult.CardListResultItem>) iterator.next();
                if (entry.getValue().count > 0) {
                    if (EffectsManager.SupportThisCard(entry.getKey())) {
                        Card tempCard = new Card(
                                entry.getKey(),
                                ExhibitionsConstant.PRICE_TYPE_NONE,
                                ExhibitionsConstant.PRICE_NONE,
                                entry.getValue().count);
                        result.add(tempCard);
                    }
                }
            }
        }
        return result;
    }
}
