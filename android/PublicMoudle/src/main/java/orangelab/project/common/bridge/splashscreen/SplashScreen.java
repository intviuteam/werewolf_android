package orangelab.project.common.bridge.splashscreen;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;

import com.androidtoolkit.UIActuator;
import com.R;

import orangelab.project.ProjectConfig;
import orangelab.project.common.effect.ApkResourceManager;

/**
 * SplashScreen
 * 启动屏
 */
public class SplashScreen {

    private static Dialog mSplashDialog;

    /**
     * 打开启动屏
     */
    public static void show(final Activity activity) {
        try {
            UIActuator.post(() -> {
                if (activity == null) return;

                try {
                    if (mSplashDialog != null && mSplashDialog.isShowing()) {
                        mSplashDialog.dismiss();
                        mSplashDialog = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (mSplashDialog != null) {
                    mSplashDialog = null;
                }

                if (!activity.isFinishing() && mSplashDialog == null) {

                    mSplashDialog = new Dialog(activity, R.style.SplashScreen_Fullscreen);

                    View view = View.inflate(activity, R.layout.launch_screen, null);

                    int resoure = ApkResourceManager.GetApkImageResource(ProjectConfig.BG_SPLASH_NAME);

                    if (resoure > 0) {
                        view.setBackgroundResource(resoure);
                    }

                    mSplashDialog.setContentView(view);
                    mSplashDialog.setCancelable(false);

                    if (!mSplashDialog.isShowing()) {
                        mSplashDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭启动屏
     */
    public static void hide() {
        try {
            UIActuator.post(() -> {
                try {
                    if (mSplashDialog != null && mSplashDialog.isShowing()) {
                        mSplashDialog.dismiss();
                        mSplashDialog = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
