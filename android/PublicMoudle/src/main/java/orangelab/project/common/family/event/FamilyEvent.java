package orangelab.project.common.family.event;

/**
 * game_werewolf
 * 2017/10/27 下午2:00
 * Mystery
 */

public class FamilyEvent {

    public static class KickOutEvent {
        private String userId;

        public KickOutEvent(String userId) {
            this.userId = userId;
        }

        public String getUserId() {
            return userId;
        }
    }

    public static class TransferEvent {
        private String userId;

        public TransferEvent(String userId) {
            this.userId = userId;
        }

        public String getUserId() {
            return userId;
        }
    }

    public static class QuitFamilyEvent {

    }

    public static class RefreshFamilyInfoEvent {

    }

}
