package orangelab.project.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.ThreadToolKit;
import com.toolkit.ToolKit;


import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.effect.frames.HeadDecorFramesAnim;
import orangelab.project.common.effect.headdecor.HeadDecorAnimData;
import orangelab.project.common.effect.headdecor.HeadDecorManiFest;
import orangelab.project.common.model.UserInfoResult;

/**
 * game_werewolf
 * 2018/6/2 下午4:29
 * Mystery
 */
public class HeadDecorView extends FrameLayout {
    private static final int MALE_SEX = 1;
    public static final int NONE_SEX = -1;
    private static final String TAG = "HeadDecorView";
    private static final float HEAD_DECOR_SCALE = 1.2f;

    private CircleImageView mCircleImageView;
    private HeadDecorateImageView mDecorateImageView;
    private ImageView mSexImageView;

    private String mUrl;

    private boolean isDetached = false;

    public HeadDecorView(@NonNull Context context) {
        this(context, null);
    }

    public HeadDecorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeadDecorView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mCircleImageView = new CircleImageView(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        addView(mCircleImageView, layoutParams);

        /**
         * 初始化基本属性
         */
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HeadDecorView, defStyleAttr, 0);

        int mBorderWidth = a.getDimensionPixelSize(R.styleable.HeadDecorView_border_width, 0);
        int mBorderColor = a.getColor(R.styleable.HeadDecorView_border_color, Color.BLACK);
        boolean mBorderOverlay = a.getBoolean(R.styleable.HeadDecorView_border_overlay, false);
        Drawable mSrcDrawable = a.getDrawable(R.styleable.HeadDecorView_src);

        a.recycle();

        mCircleImageView.setBorderWidth(mBorderWidth);
        mCircleImageView.setBorderColor(mBorderColor);
        mCircleImageView.setBorderOverlay(mBorderOverlay);
        if (mSrcDrawable != null) {
            mCircleImageView.setImageDrawable(mSrcDrawable);
        }

        setClipChildren(false);
    }

    public void setBorderColor(int color) {
        mCircleImageView.setBorderColor(color);
    }

    public void setBordOverlay(boolean overlay) {
        mCircleImageView.setBorderOverlay(overlay);
    }

    public void setBorderWidth(int width) {
        mCircleImageView.setBorderWidth(width);
    }

    public void setImageResource(int resource) {
        mCircleImageView.setImageResource(resource);
    }

    public CircleImageView getRealHeadImage() {
        return mCircleImageView;
    }

    public void setUserHeadImageUrl(String icon) {
        mUrl = icon;
        PicassoUtils.loadImage(getContext(), icon, mCircleImageView, 150, 150, R.mipmap.default_head);
    }

    public void setUserSex(int sex) {
        if (mSexImageView != null) {
            updateUserSex(mSexImageView, sex);
        } else {
            post(() -> {

                mSexImageView = new ImageView(getContext());
                int width = getWidth();
                int setWidth = (int) (width * 0.25);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(setWidth, setWidth);
                layoutParams.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                mSexImageView.setLayoutParams(layoutParams);

                updateUserSex(mSexImageView, sex);

                addView(mSexImageView);

            });
        }
    }

    private void updateUserSex(ImageView view, int sex) {
        switch (sex) {
            case MALE_SEX:
                view.setVisibility(View.VISIBLE);
                view.setImageResource(R.mipmap.ic_male);
                break;
            case NONE_SEX:
                view.setVisibility(View.GONE);
                break;
            default:
                view.setVisibility(View.VISIBLE);
                view.setImageResource(R.mipmap.ic_female);
                break;
        }
    }

    public String getHeadUrl() {
        return mUrl;
    }

    public void clearHeadDecorate() {
        if (mDecorateImageView != null && mDecorateImageView.getParent() != null) {
            removeView(mDecorateImageView);
        }
    }

    public void setHeadDecorate(UserInfoResult.AvatarBox box) {
        if (box != null) {
            setHeadDecorate(box.avatar_box_type);
        }
    }

    public void setHeadDecorate(String type) {
        post(() -> {
            if (mDecorateImageView != null) {
                if (mDecorateImageView.needReplace(type)) {
                    clearHeadDecorate();
                    createAndAddDecorImageView();
                    setDecorImageType(type);
                } else {
                    return;
                }
            } else {
                createAndAddDecorImageView();
                setDecorImageType(type);
            }
        });
    }

    private void setDecorImageType(String type) {
        post(() -> {
            if (mDecorateImageView != null) {
                mDecorateImageView.setDecorAnim(type);
            }
        });
    }

    private void createAndAddDecorImageView() {
        /**
         * 生成一个View
         */
        int width = getWidth();
        int height = getHeight();

        int decorateWidth = (int) (width * HEAD_DECOR_SCALE);
        int decorateHeight = (int) (height * HEAD_DECOR_SCALE);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(decorateWidth, decorateHeight);
        layoutParams.gravity = Gravity.CENTER;
        mDecorateImageView = new HeadDecorateImageView(getContext());
        mDecorateImageView.setLayoutParams(layoutParams);
        addView(mDecorateImageView);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        isDetached = true;
    }


    public void pause() {
        if (mDecorateImageView != null) mDecorateImageView.pause();
    }

    public void resume() {
        if (mDecorateImageView != null) mDecorateImageView.resume();
    }

    public static class HeadDecorateImageView extends AppCompatImageView {

        private HeadDecorFramesAnim mAnimFrame;
        private boolean isDetached = false;
        private String mCurHeadDecorType;
        private int missCount = 0;
        private Runnable reCheck = () -> {
            setDecorAnim(mCurHeadDecorType);
        };

        public HeadDecorateImageView(Context context) {
            this(context, null);
        }

        public HeadDecorateImageView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public HeadDecorateImageView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public boolean needReplace(String decorType) {
            if (TextUtils.equals(decorType, mCurHeadDecorType) && mAnimFrame != null && mAnimFrame.isRunning()) {
                return false;
            }
            return true;
        }

        public void setDecorAnim(String decorate) {
            post(() -> {
                if (!isDetached) {

                    PLog.i(TAG, "setDecorAnim: " + decorate);

                    if (TextUtils.isEmpty(decorate)) {
                        return;
                    }

                    if (TextUtils.equals(mCurHeadDecorType, decorate) && mAnimFrame != null && mAnimFrame.isRunning()) {
                        /**
                         * 设定的类型和原本类型相同 ,并且处于运行状态
                         */
                        return;
                    }

                    mCurHeadDecorType = decorate;

                    /**
                     * 先清理原本的头像框特效
                     */
                    clearDecorAnim();

                    /**
                     * 根据Type来装配数据
                     */
                    long start = System.currentTimeMillis();
                    final HeadDecorManiFest.HeadDecorManiFestItem item = EffectsMainFestManager.FindHeadDecorManiFestItem(decorate);
                    Log.i(TAG, "setDecorAnim: find item cost " + (System.currentTimeMillis() - start));
                    post(() -> {
                        if (isDetached) return;

                        if (item == null) {
                            missCount++;
                            removeCallbacks(reCheck);
                            postDelayed(reCheck, 10000);
                            return;
                        }

                        /**
                         * count清零
                         */
                        missCount = 0;
                        removeCallbacks(reCheck);

                        HeadDecorAnimData animData = item.animate;
                        if (animData == null) return;

                        HeadDecorFramesAnim anim;

                        if (!ToolKit.isEmpty(animData.preRepeatFrames)) {

                            anim = new HeadDecorFramesAnim(this);
                            anim.setRepeat(false);
                            anim.setInterval(animData.preRepeatIFS);
                            anim.setFrames((animData.createCopy(animData.preRepeatFrames)));

                            HeadDecorFramesAnim animNext = new HeadDecorFramesAnim(this);
                            animNext.setRepeat(true);
                            animNext.setInterval(animData.repeatIFS);
                            animNext.setFrames((animData.createCopy(animData.repeatFrames)));

                            anim.setNext(animNext);
                        } else {
                            anim = new HeadDecorFramesAnim(this);
                            anim.setRepeat(true);
                            anim.setInterval(animData.repeatIFS);
                            anim.setFrames((animData.createCopy(animData.repeatFrames)));
                        }

                        mAnimFrame = anim;

                        post(() -> {
                            start();
                        });
                    });
                }
            });
        }

        public void clearDecorAnim() {
            if (mAnimFrame != null) {
                mAnimFrame.stopAll();
                mAnimFrame.releaseAll();
            }
        }

        public void pause() {
            if (isDetached) return;

            if (mAnimFrame != null) {
                mAnimFrame.pause();
            }
        }

        public void resume() {
            if (isDetached) return;

            if (mAnimFrame != null) {
                mAnimFrame.resume();
            }
        }

        public void start() {
            if (isDetached) return;

            if (mAnimFrame != null) {
                mAnimFrame.start();
            }
        }

        public boolean isRunning() {
            if (isDetached) return false;

            if (mAnimFrame != null) {
                return mAnimFrame.isRunning();
            }
            return false;
        }

        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            isDetached = true;
            clearDecorAnim();
            setImageDrawable(null);
        }
    }

}
