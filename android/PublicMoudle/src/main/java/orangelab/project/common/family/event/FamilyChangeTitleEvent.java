package orangelab.project.common.family.event;

/**
 * game_werewolf
 * 2017/11/24 下午4:23
 * Mystery
 */

public class FamilyChangeTitleEvent {

    private String userId;
    private String title;

    public FamilyChangeTitleEvent(String userId, String title) {
        this.userId = userId;
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }
}
