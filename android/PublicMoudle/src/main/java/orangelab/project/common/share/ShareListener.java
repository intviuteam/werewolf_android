package orangelab.project.common.share;

/**
 * game_werewolf
 * 2018/2/27 上午10:28
 * Mystery
 */

public interface ShareListener {

    void onShareWeChat();

    void onShareQQ();

    void onShareLine();

    void onShareFaceBook();

    void onShareWeChatFriend();
}
