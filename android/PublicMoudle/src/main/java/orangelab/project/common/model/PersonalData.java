package orangelab.project.common.model;

import android.text.TextUtils;

/**
 * Author:1727
 * 2017/3/6
 */

public class PersonalData {

    private int selfPosition;
    private String userId;
    private String userName;
    private int userSex;
    private String imageUrl;
    private boolean isRoomOwner;
    private String selfRole = "";
    private boolean isDead;
    private String gameType;
    private String token;
    private String curPassword;
    private String roomId;
    private int userExp;
    private boolean isSpeaking;
    private boolean isBanMic;
    private boolean isFreeStyle;
    private boolean isPrepared;
    private boolean isOut;
    private boolean isOffline;
    private boolean canSpeak;
    private boolean isMaster;
    private boolean isSpeech;
    private int speechTime;
    private boolean isObserver;
    private String role;//自己的角色:卧底/好人
    private boolean canSing = false;//当前是否有伴奏权限
    private boolean isSing = false;//当前是否在进行伴奏
    private int uid; //uid
    private boolean isAdmin = false;
    private boolean isGuest = false;


    public int getUserExp() {
        return userExp;
    }

    public void setUserExp(int userExp) {
        this.userExp = userExp;
    }

    public int getUserSex() {
        return userSex;
    }

    public void setUserSex(int userSex) {
        this.userSex = userSex;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCurPassword() {
        return curPassword;
    }

    public void setCurPassword(String curPassword) {
        if (TextUtils.isEmpty(curPassword)) {
            curPassword = "";
        }
        this.curPassword = curPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public int getSelfPosition() {
        return selfPosition;
    }

    public void setSelfPosition(int selfPosition) {
        this.selfPosition = selfPosition;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl == null ? "" : imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSelfRole() {
        return selfRole;
    }

    public void setSelfRole(String selfRole) {
        this.selfRole = selfRole;
    }

    public boolean isRoomOwner() {
        return isRoomOwner;
    }

    public void setRoomOwner(boolean roomOwner) {
        isRoomOwner = roomOwner;
    }

    public boolean isBanMic() {
        return isBanMic;
    }

    public void setBanMic(boolean banMic) {
        isBanMic = banMic;
    }

    public boolean isFreeStyle() {
        return isFreeStyle;
    }

    public void setFreeStyle(boolean freeStyle) {
        isFreeStyle = freeStyle;
    }

    public boolean isSpeaking() {
        return isSpeaking;
    }

    public void setSpeaking(boolean speaking) {
        isSpeaking = speaking;
    }

    public boolean isPrepared() {
        return isPrepared;
    }

    public void setPrepared(boolean prepared) {
        isPrepared = prepared;
    }

    public boolean isOut() {
        return isOut;
    }

    public void setOut(boolean out) {
        isOut = out;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

    public boolean isCanSpeak() {
        return canSpeak;
    }

    public void setCanSpeak(boolean canSpeak) {
        this.canSpeak = canSpeak;
    }

    public boolean isMaster() {
        return isMaster;
    }

    public void setMaster(boolean master) {
        isMaster = master;
    }

    public boolean isSpeech() {
        return isSpeech;
    }

    public void setSpeech(boolean speech) {
        isSpeech = speech;
    }

    public int getSpeechTime() {
        return speechTime;
    }

    public void setSpeechTime(int speechTime) {
        this.speechTime = speechTime;
    }

    public boolean isObserver() {
        return isObserver;
    }

    public void setObserver(boolean observer) {
        isObserver = observer;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isCanSing() {
        return canSing;
    }

    public void setCanSing(boolean canSing) {
        this.canSing = canSing;
    }

    public boolean isSing() {
        return isSing;
    }

    public void setSing(boolean sing) {
        isSing = sing;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isGuest() {
        return isGuest;
    }

    public void setGuest(boolean guest) {
        isGuest = guest;
    }
}
