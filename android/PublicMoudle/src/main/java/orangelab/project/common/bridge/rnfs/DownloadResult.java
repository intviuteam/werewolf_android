package orangelab.project.common.bridge.rnfs;

public class DownloadResult {
  public int statusCode;
  public int bytesWritten;
  public Exception exception;
}
