package orangelab.project.common.share

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.androidtoolkit.PicassoUtils
import com.R

/**
 *game_werewolf
 *2018/2/28 上午11:52
 *Mystery
 */
class FamilyRankShareableView : ShareableView {

    constructor(context: Context) : super(context) {
        fillView()
    }

    private fun fillView() {
        View.inflate(context, R.layout.layout_share_family_picture, this)
        val layoutParams = ViewGroup.LayoutParams(776, 432)
        setLayoutParams(layoutParams)
    }

    fun initData(rankSharePayload: RankSharePayload) {

        val userName: TextView = findViewById(R.id.id_user_name) as TextView;
        val userHead: ImageView = findViewById(R.id.id_user_head) as ImageView;

        val rank: TextView = findViewById(R.id.id_rank_list) as TextView;
        val popular: TextView = findViewById(R.id.id_popular) as TextView;

        PicassoUtils.loadImage(context, rankSharePayload.image, userHead, R.mipmap.default_head);
        userName.setText(rankSharePayload.name);
        rank.setText(rankSharePayload.rankIndex);
        popular.setText(rankSharePayload.rankScore);
    }
}