package orangelab.project.common.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;

import orangelab.project.common.model.UserVipInfo;

/**
 * game_werewolf
 * 2018/7/17 下午3:35
 * Mystery
 */
public class VipNameView extends AppCompatTextView {

    public VipNameView(Context context) {
        this(context, null);
    }

    public VipNameView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VipNameView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void parseTextColor(String color) {
        try {
            setTextColor(Color.parseColor("#" + color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleVipInfo(UserVipInfo vipInfo) {
        handleVipInfo(vipInfo, Color.WHITE);
    }

    public void handleVipInfo(UserVipInfo vipInfo, int color) {
        if (vipInfo != null) {
            if (vipInfo.active) {
                parseTextColor(vipInfo.nameColor);
            } else {
                setTextColor(color);
            }
        } else {
            setTextColor(color);
        }
    }

}
