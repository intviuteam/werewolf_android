package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/20 上午10:32
 * Mystery
 */

public class ServerActionChangeUserState extends ServerActionContainsPosition implements Keepable {

    public String state;
}
