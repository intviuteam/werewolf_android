package orangelab.project.common.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2017/9/21 下午2:17
 * Mystery
 */
@Entity
public class BlackListLightEntity {
    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    private String fromUserId;//自己的UserId
    private String userId;
    private long createTime;
    private String arg1;   //预留字段
    private String arg2;   //预留字段
    private long updateTime;

    @Generated(hash = 311161929)
    public BlackListLightEntity(Long id, String fromUserId, String userId,
                                long createTime, String arg1, String arg2, long updateTime) {
        this.id = id;
        this.fromUserId = fromUserId;
        this.userId = userId;
        this.createTime = createTime;
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.updateTime = updateTime;
    }

    @Generated(hash = 1024646600)
    public BlackListLightEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromUserId() {
        return this.fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getArg1() {
        return this.arg1;
    }

    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    public String getArg2() {
        return this.arg2;
    }

    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }


}
