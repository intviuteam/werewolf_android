package orangelab.project.common.family.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.datasource.GlobalUserState;
import com.R;
import orangelab.project.common.family.FamilyConstant;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.event.FamilyChangeTitleEvent;
import orangelab.project.common.family.model.FamilyMemberInfo;
import orangelab.project.common.family.view.FamilyMemberActionPopUpWindow;
import orangelab.project.common.model.OnlineModel;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.view.UserHeadView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;

import rx.Subscription;
import rx.functions.Action1;

/**
 * game_werewolf
 * 2017/10/19 下午2:18
 * Mystery
 */

public class FamilyMembersAdapter extends RecyclerView.Adapter<FamilyMembersAdapter.FamilyMembersItemViewHolder> implements Destroyable {

    private int mIntentType;

    private List<FamilyMemberInfo> mData = new ArrayList<>();
    private int onlineColor = Color.parseColor("#359a85");
    private int offlineColor = Color.parseColor("#a9a9a9");
    private int bossColor = Color.parseColor("#f47103");
    private int slaveColor = Color.parseColor("#7f7f7f");
    private String groupId;

    public FamilyMembersAdapter(String groupId, int intentType) {
        this.groupId = groupId;
        this.mIntentType = intentType;
        initEvent();
    }

    private void initEvent() {
        RxToolKit.Build(this, FamilyChangeTitleEvent.class).action(familyChangeTitleEvent -> {
            FamilyMemberInfo result = findItem(familyChangeTitleEvent.getUserId());
            if (result != null) {
                result.title = familyChangeTitleEvent.getTitle();
            }
            notifyDataSetChanged();
        }).register();
    }

    public FamilyMemberInfo findItem(String userId) {
        FamilyMemberInfo result = null;
        for (FamilyMemberInfo info : mData) {
            if (TextUtils.equals(userId, info.id)) {
                result = info;
                break;
            }
        }
        return result;
    }

    public void updateData(List<FamilyMemberInfo> updateData) {
        if (mData != null) {
            mData.clear();
        }
        mData = updateData;
        notifyDataSetChanged();
    }

    public void clear() {
        if (mData != null) {
            mData.clear();
        }
        notifyDataSetChanged();
    }

    public void add(List<FamilyMemberInfo> updateData) {
        if (mData != null) {
            mData.addAll(updateData);
            notifyDataSetChanged();
        }
    }

    public void remove(String userId) {
        if (mData != null) {
            Iterator<FamilyMemberInfo> iterator = mData.iterator();
            while (iterator.hasNext()) {
                FamilyMemberInfo temp = iterator.next();
                if (TextUtils.equals(temp.id, userId)) {
                    iterator.remove();
                    break;
                }
            }
            notifyDataSetChanged();
        }
    }

    @Override
    public FamilyMembersItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FamilyMembersItemViewHolder(View.inflate(parent.getContext(), R.layout.layout_item_family_members, null), mIntentType);
    }

    @Override
    public void onBindViewHolder(FamilyMembersItemViewHolder holder, int position) {
        FamilyMemberInfo item = mData.get(position);
        try {
            holder.mMemberLevel.setText(MessageUtils.getLevelString(item.game.level));
            holder.mMemberHead.setUserSex(item.sex);
            holder.mMemberHead.setUserHeadImageUrl(item.image);
            holder.mMemberName.setText(item.name);
            holder.mMemberName.setTag(item.id);
            holder.setTargetGroupId(groupId);
            if (TextUtils.equals(item.title, "owner")) {
                holder.mMemberPosition.setText(MessageUtils.getString(R.string.string_family_boss));
                holder.mMemberPosition.setTextColor(bossColor);
                /**
                 * 这个人成为了族长
                 */
                if (TextUtils.equals(item.id, GlobalUserState.getGlobalState().getUserId()) && FamilyMemo.IsMyFamily(groupId)) {
                    /**
                     * 这个人是自己把自己更新为族长
                     */
                    FamilyMemo.iMemo.updateOwner(true);
                }
            } else if (TextUtils.equals(item.title, FamilyConstant.FAMILY_DEPUTY)) {
                holder.mMemberPosition.setText(MessageUtils.getString(R.string.string_family_deputy));
                holder.mMemberPosition.setTextColor(onlineColor);
            } else if (TextUtils.equals(item.title, FamilyConstant.FAMILY_ELDER)) {
                holder.mMemberPosition.setText(MessageUtils.getString(R.string.string_family_elder));
                holder.mMemberPosition.setTextColor(onlineColor);
            } else if (TextUtils.equals(item.title, FamilyConstant.FAMILY_ELITE)) {
                holder.mMemberPosition.setText(MessageUtils.getString(R.string.string_family_elite));
                holder.mMemberPosition.setTextColor(onlineColor);
            } else {
                holder.mMemberPosition.setText(MessageUtils.getString(R.string.string_family_members_slave));
                holder.mMemberPosition.setTextColor(slaveColor);
            }
            holder.mMemberLiveNess.setText(Integer.toString(item.active_point));
            OnlineModel model = MessageUtils.GetOnlineMessage(item.status);
            if (model.isOnline) {
                holder.mMemberLoginTime.setTextColor(onlineColor);
            } else {
                holder.mMemberLoginTime.setTextColor(offlineColor);
            }
            holder.mMemberLoginTime.setText(model.state);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        mData.clear();
    }

    public static class FamilyMembersItemViewHolder extends RecyclerView.ViewHolder {
        public int mIntentType;
        public UserHeadView mMemberHead;
        public TextView mMemberName;
        public TextView mMemberLevel;
        public TextView mMemberPosition;
        public TextView mMemberLiveNess;
        public TextView mMemberLoginTime;
        public String targetGroupId;

        public void setTargetGroupId(String targetGroupId) {
            this.targetGroupId = targetGroupId;
        }

        public FamilyMembersItemViewHolder(View itemView, int intentType) {
            super(itemView);
            mIntentType = intentType;
            mMemberHead = (UserHeadView) itemView.findViewById(R.id.id_item_family_members_head);
            mMemberName = (TextView) itemView.findViewById(R.id.id_item_family_members_name);
            mMemberLevel = (TextView) itemView.findViewById(R.id.id_item_family_members_level);
            mMemberPosition = (TextView) itemView.findViewById(R.id.id_item_family_members_position);
            mMemberLiveNess = (TextView) itemView.findViewById(R.id.id_item_family_members_liveness);
            mMemberLoginTime = (TextView) itemView.findViewById(R.id.id_item_family_members_last_login);

            itemView.setOnClickListener(v -> {
                String userId = (String) mMemberName.getTag();
                if (TextUtils.isEmpty(userId)) {
                    return;
                }

                /**
                 * 获取View的位置
                 */
                FamilyMemberActionPopUpWindow.ShowAtView(itemView, mMemberHead, userId, mIntentType, targetGroupId);
            });
        }


    }
}
