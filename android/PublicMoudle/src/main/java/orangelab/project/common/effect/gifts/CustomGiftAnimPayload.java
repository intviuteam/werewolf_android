package orangelab.project.common.effect.gifts;

import android.graphics.PointF;

/**
 * game_werewolf
 * 2017/11/8 下午4:37
 * Mystery
 */

public class CustomGiftAnimPayload {

    public PointF start;  //Android 这里是默认的起点，默认为发起者的起点
    public PointF end;    //Android 这里是默认的重点，默认为接收者的终点

    public int startPosition = -1;
    public int endPosition = -1;

    public int startOrientation = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;
    public int endOrientation = GiftAnimConstant.MEMBER_ORIENTATION_LEFT;

    public int viewWidth = -1;
    public int viewHeight = -1;
}
