package orangelab.project.common.view;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.R;
import com.androidtoolkit.PicassoUtils;

/**
 * Created by lloydfinch on 05/09/2017.
 */

public class FloatView extends LinearLayout {

    private WindowManager.LayoutParams wmParams;
    private WindowManager wm;
    private float mTouchStartX;
    private float mTouchStartY;
    private float x;
    private float y;
    private IFloatViewClick listener;
    private boolean isAllowTouch = true;

    public FloatView(Context context, int x, int y, int layoutres, String url) {
        super(context);
        View view = LayoutInflater.from(getContext()).inflate(layoutres, null);
        init(view, x, y, url);
    }

    public FloatView(Context context, int x, int y, View childView, String url) {
        super(context);
        init(childView, x, y, url);
    }

    private void init(View childView, int x, int y, String url) {
        wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        wmParams = new WindowManager.LayoutParams();

        wmParams.type = WindowManager.LayoutParams.TYPE_TOAST;
        //位于屏幕右中部
        wmParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        wmParams.format = PixelFormat.RGBA_8888;
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.x = x;
        wmParams.y = y;
        if (childView != null) {
            addView(childView);
            if (!TextUtils.isEmpty(url)) {
                ImageView im = (ImageView) childView.findViewById(R.id.iv_head);
                PicassoUtils.loadImage(getContext(), url, im);
            }
        }
        //   wm.addView(this, wmParams);
    }

    public void updateFloatViewPosition(int x, int y) {
        wmParams.x = x;
        wmParams.y = y;
        wm.updateViewLayout(this, wmParams);
    }

    public void setFloatViewClickListener(IFloatViewClick listener) {
        this.listener = listener;
    }

    public boolean addToWindow() {
        if (wm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (!isAttachedToWindow()) {
                    wm.addView(this, wmParams);
                    return true;
                } else {
                    return false;
                }
            } else {
                try {
                    if (getParent() == null) {
                        wm.addView(this, wmParams);
                    }
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }


        } else {
            return false;
        }
    }

    public void setIsAllowTouch(boolean flag) {
        isAllowTouch = flag;
    }

    public boolean removeFromWindow() {
        if (wm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (isAttachedToWindow()) {
                    wm.removeViewImmediate(this);
                    return true;
                } else {
                    return false;
                }
            } else {
                try {
                    if (getParent() != null) {
                        wm.removeViewImmediate(this);
                    }
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }


        } else {
            return false;
        }

    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // TODO Auto-generated method stub
        return isAllowTouch;
    }


//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                mTouchStartX = (int) event.getRawX() - this.getMeasuredWidth() / 2;
//                mTouchStartY = (int) event.getRawY() - this.getMeasuredHeight() / 2;
//
//                return true;
//            case MotionEvent.ACTION_MOVE:
//                wmParams.x = (int) event.getRawX() - this.getMeasuredWidth() / 2;
//                // 减25为状态栏的高度
//                wmParams.y = (int) event.getRawY() - this.getMeasuredHeight() / 2;
//                // 刷新
//                if (Math.abs(wmParams.y - mTouchStartY) > 10 || Math.abs(wmParams.x - mTouchStartX) > 10) {
//                    wm.updateViewLayout(this, wmParams);
//                }
//                return true;
//            case MotionEvent.ACTION_UP:
//                y = (int) event.getRawY() - this.getMeasuredHeight() / 2;
//                x = (int) event.getRawX() - this.getMeasuredWidth() / 2;
//                if (Math.abs(y - mTouchStartY) > 10 || Math.abs(x - mTouchStartX) > 10) {
//                    wm.updateViewLayout(this, wmParams);
//                } else {
//                    if (listener != null) {
//                        listener.onFloatViewClick();
//                    }
//
//                }
//                return true;
//            default:
//                break;
//        }
//        return false;
//
//    }


    public interface IFloatViewClick {
        void onFloatViewClick();
    }
}
