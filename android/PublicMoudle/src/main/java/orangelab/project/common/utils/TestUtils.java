package orangelab.project.common.utils;

import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.exhibition.ExhibitionsPresentRecordItem;
import orangelab.project.common.exhibition.IExhibition;
import orangelab.project.common.exhibition.gift.Gift;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/7/3 下午6:18
 * Mystery
 */

public class TestUtils {

    public static List<ExhibitionsPresentRecordItem> getTestExhibitionsPresentRecords() {
        List<ExhibitionsPresentRecordItem> items = new ArrayList<>();
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        items.add(new ExhibitionsPresentRecordItem(
                "1727",
                "http://img1.mydrivers.com/img/20170703/S9457a97e-7387-4532-aac8-284de5935553.jpg",
                ExhibitionsConstant.GIFT_ROSE,
                System.currentTimeMillis()
        ));
        return items;
    }

    public static List<IExhibition> getTestGift() {
        List<IExhibition> giftList = new ArrayList<>();
        Gift gift1 = new Gift(
                ExhibitionsConstant.GIFT_ROSE, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift2 = new Gift(
                ExhibitionsConstant.GIFT_KISS, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift3 = new Gift(
                ExhibitionsConstant.GIFT_ROSE3, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift4 = new Gift(
                ExhibitionsConstant.GIFT_ROSE99, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift5 = new Gift(
                ExhibitionsConstant.GIFT_VEGETABLE, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift6 = new Gift(
                ExhibitionsConstant.GIFT_EGG, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift7 = new Gift(
                ExhibitionsConstant.GIFT_CLAP, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift8 = new Gift(
                ExhibitionsConstant.GIFT_CHOCOLATE, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift9 = new Gift(
                ExhibitionsConstant.GIFT_CAKE, ExhibitionsConstant.PRICE_TYPE_GOLD, 4);
        Gift gift10 = new Gift(
                ExhibitionsConstant.GIFT_WEDDINGDRESS, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 4);
        Gift gift11 = new Gift(
                ExhibitionsConstant.GIFT_JAWBREAKER, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 4);
        Gift gift12 = new Gift(
                ExhibitionsConstant.GIFT_BOAT, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 4);
        Gift gift13 = new Gift(
                ExhibitionsConstant.GIFT_TEDDYBEAR, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift14 = new Gift(
                ExhibitionsConstant.GIFT_HEART, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift15 = new Gift(
                ExhibitionsConstant.GIFT_RING, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift16 = new Gift(
                ExhibitionsConstant.GIFT_CROWN, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift17 = new Gift(
                ExhibitionsConstant.GIFT_CAR, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift18 = new Gift(
                ExhibitionsConstant.GIFT_AIRPLANE, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        Gift gift19 = new Gift(
                ExhibitionsConstant.GIFT_ROCK, ExhibitionsConstant.PRICE_TYPE_DIAMOND, 12);
        giftList.add(gift1);
        giftList.add(gift2);
        giftList.add(gift3);
        giftList.add(gift4);
        giftList.add(gift5);
        giftList.add(gift6);
        giftList.add(gift7);
        giftList.add(gift8);
        giftList.add(gift9);
        giftList.add(gift10);
        giftList.add(gift11);
        giftList.add(gift12);
        giftList.add(gift13);
        giftList.add(gift14);
        giftList.add(gift15);
        giftList.add(gift16);
        giftList.add(gift17);
        giftList.add(gift18);
        giftList.add(gift19);
        return giftList;
    }
}
