package orangelab.project.common.db;

import android.content.Context;

import com.toolkit.action.Destroyable;

import orangelab.project.common.db.gen.DaoMaster;
import orangelab.project.common.db.gen.DaoSession;
import orangelab.project.common.db.gen.GooglePaySubsOrderAssitEntityDao;
import orangelab.project.common.db.gen.MiniGameAIRecordEntityDao;
import orangelab.project.common.db.gen.MiniGameLaunchRecordEntityDao;
import orangelab.project.common.db.gen.MiniGameRecordEntityDao;
import orangelab.project.common.db.gen.UserPayOrderEntityDao;

/**
 * game_werewolf
 * 2018/5/17 下午4:47
 * Mystery
 */
public class DaoHelper implements Destroyable {

    private static final String TAG = "DaoHelper";

    private DaoMaster.DevOpenHelper mHelper;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;

    public DaoHelper(Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context, DBConstant.DB_NAME, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = mDaoMaster.newSession();
    }

    public MiniGameRecordEntityDao getMiniGameEntityDao() {
        if (mDaoSession != null) {
            return mDaoSession.getMiniGameRecordEntityDao();
        }
        return null;
    }

    public MiniGameLaunchRecordEntityDao getMiniGameLaunchRecord() {
        if (mDaoSession != null) {
            return mDaoSession.getMiniGameLaunchRecordEntityDao();
        }
        return null;
    }

    public UserPayOrderEntityDao getUserPayOrderEntity() {
        if (mDaoSession != null) {
            return mDaoSession.getUserPayOrderEntityDao();
        }
        return null;
    }

    public MiniGameAIRecordEntityDao getMiniGameAIEntityDao() {
        if (mDaoSession != null) {
            return mDaoSession.getMiniGameAIRecordEntityDao();
        }
        return null;
    }

    public GooglePaySubsOrderAssitEntityDao getGoogleSubsOrderAssitEntity() {
        if (mDaoSession != null) {
            return mDaoSession.getGooglePaySubsOrderAssitEntityDao();
        }
        return null;
    }


    @Override
    public void destroy() {
        if (mDaoSession != null) {
            mDaoSession.clear();
            mDaoSession = null;
        }
        if (mDaoMaster != null) {
            mDaoMaster = null;
        }
        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }
    }
}
