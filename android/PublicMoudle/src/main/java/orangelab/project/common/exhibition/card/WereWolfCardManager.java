package orangelab.project.common.exhibition.card;

import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.manager.AppManager;
import orangelab.project.common.model.CardListResult;

import com.androidtoolkit.PLog;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/7/6 下午1:38
 * Mystery
 * <p>
 * 游戏中的卡片管理器
 */

public class WereWolfCardManager implements IWereWolfCardService {

    private static final String TAG = "GameCardManager";

    private Context mContext;
    private ViewGroup mViewGroup;
    private Handler mHandler;
    private RequestTask mRequestTask;

    private List<Card> mCard;
    private Map<String, View> mShownCard;

    public WereWolfCardManager(Context context, ViewGroup viewGroup) {
        mContext = context;
        mViewGroup = viewGroup;
        mHandler = new Handler(Looper.getMainLooper());
        mCard = new ArrayList<>();
        mShownCard = new HashMap<>();
        initListener();
        refreshCardList();
    }

    private void requestDismissCard(CardEvent.WereWolfDismissCardEvent value) {
        if (TextUtils.equals(value.getType(), ExhibitionsConstant.ALL_CARD)) {
            removeAllCardView();
        } else {
            removeCardView(value.getType());
        }
    }

    private void requestUseCard(CardEvent.WereWolfRequestUseCardEvent value) {
        refreshCardList(() -> {
            if (!value.isDisposable()) {
                requestShowWereWolfCardIfHas(value.getEvent());
            }
        });
    }

    private void initListener() {
        RxToolKit.Build(this, CardEvent.WereWolfRequestShowCardEvent.class).action(value -> {
            /**
             * 用于新的UI展示
             */
            requestShowWereWolfCardIfHas(value);

        }).register();

        RxToolKit.Build(this, CardEvent.WereWolfRequestUseCardEvent.class).action(value -> {

            requestUseCard(value);

        }).register();
        RxToolKit.Build(this, CardEvent.WereWolfDismissCardEvent.class).action(value -> {
            /**
             * 用于新的UI展示
             */
            requestDismissCard(value);

        }).register();

        RxToolKit.Build(this, CardEvent.WereWolfKeyBoardChangeEvent.class).action(value -> {
            /**
             * 监听了键盘变化
             */try {
                if (mShownCard.containsKey(ExhibitionsConstant.CARD_DELAY_TIME)) {
                    removeCardView(ExhibitionsConstant.CARD_DELAY_TIME);
                    requestShowWereWolfCardIfHas(new CardEvent.WereWolfRequestShowCardEvent(ExhibitionsConstant.CARD_DELAY_TIME, value.getPointF()));
                }

                if (mShownCard.containsKey(ExhibitionsConstant.CARD_CHECK)) {
                    removeCardView(ExhibitionsConstant.CARD_CHECK);
                    requestShowWereWolfCardIfHas(new CardEvent.WereWolfRequestShowCardEvent(ExhibitionsConstant.CARD_CHECK, value.getPointF()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).register();
    }

    private void refreshCardData(CardListResult cardListResult) {
        if (cardListResult != null && cardListResult.cards != null && cardListResult.cards.size() > 0) {
            mCard.clear();
            mCard.addAll(CardUtils.cardListResultTurnToList(cardListResult));
        }
    }

    public void refreshCardList() {
        refreshCardList(null);
    }

    public void refreshCardList(Runnable successRunnable) {
        if (mRequestTask != null) {
            /**
             * 代表上一个RequestTask 还存在则尝试释放
             */
            mRequestTask.cancel();
        }
        mRequestTask = RequestTask.create(
                false,
                Constant.GET_CARD_LIST,
                GlobalUserState.getGlobalState().getToken(),
                new JSONObject(),
                new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        CardListResult cardListResult = GsonHelper.getGson().fromJson(response, CardListResult
                                .class);
                        runOnHandler(() ->
                        {   //先刷新卡片
                            refreshCardData(cardListResult);
                            //执行成功回调
                            if (successRunnable != null) {
                                successRunnable.run();
                            }
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        /**
                         * ignore
                         */
                    }

                    @Override
                    public void onError(Exception e) {
                        /**
                         * ignore
                         */
                    }
                }
        );
        mRequestTask.execute();
    }

    private void runOnHandler(Runnable run, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(run, delay);
        }
    }

    private void runOnHandler(Runnable run) {
        runOnHandler(run, 0);
    }

    public void destroy() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        if (mRequestTask != null) {
            mRequestTask.cancel();
        }
        mShownCard.clear();
        mCard.clear();
        RxToolKit.UnRegister(this);
    }

    private Card getCard(String cardType) {
        Card target = null;
        for (Card temp : mCard) {
            if (TextUtils.equals(cardType, temp.getRealType())) {
                target = temp;
                break;
            }
        }
        return target;
    }

    public boolean hasShown(String cardType) {
        return mShownCard.containsKey(cardType);
    }

    public boolean canShow(String cardType) {
        return TextUtils.equals(cardType, ExhibitionsConstant.CARD_DELAY_TIME) || TextUtils.equals(cardType, ExhibitionsConstant.CARD_CHECK);
    }

    public boolean hasCard(String cardType) {
        Card target = getCard(cardType);
        if (target != null && target.getExhibitionCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void userCard(String cardType, boolean isDisposable) {
        refreshCardList(() -> {
            /**
             * 这里应该重新刷新一下卡片数据
             */
            refreshCardList();
        });
    }

    public void requestShowWereWolfCardIfHas(CardEvent.WereWolfRequestShowCardEvent requestShowCardEvent) {
        PLog.i(TAG, "requestShowCardIfHas: " + requestShowCardEvent.getType());
        if (hasShown(requestShowCardEvent.getType())) {
            /**
             * 已经展示的卡片不在重复展示
             */
            return;
        }
        Card target = getCard(requestShowCardEvent.getType());
        if (target != null && target.getExhibitionCount() > 0 && canShow(requestShowCardEvent.getType())) {
            /**
             * 创建卡片
             */
            Button card = (Button) View.inflate(mContext, R.layout.layout_werewolf_card_check, null);
            if (TextUtils.equals(ExhibitionsConstant.CARD_DELAY_TIME, target.getRealType())) {
                decorDelayTimeCard(requestShowCardEvent, card);
            } else if (TextUtils.equals(ExhibitionsConstant.CARD_CHECK, target.getRealType())) {
                decorCheckCard(requestShowCardEvent, card);
            } else {
                /**
                 * 由于目前游戏中只支持 延时卡 与 查验卡，所以其他类型暂时跳过
                 */
                return;
            }
            CardAction action = CardUtils.getCardAction(target.getRealType());
            if (action != null) {
                card.setOnClickListener(v -> {
                    if (action != null) {
                        action.onClick(v);
                        if (action.isDisposable()) {
                            /**
                             * 如果是一次性的卡片需要再使用后移除
                             */
                            removeCardView(requestShowCardEvent.getType());
                        } else {
                            /**
                             * 如果不是一次性的那么需要先移除，然后再刷新卡片，之后再次显示出来
                             */
                            removeCardView(requestShowCardEvent.getType());
                        }
                    }
                });
            }
            /**
             * 添加一个card的时候先判断当前有没有一个已经存在的Card
             */
            removeCardView(requestShowCardEvent.getType());
            mViewGroup.addView(card);
            mShownCard.put(requestShowCardEvent.getType(), card);
            PLog.i(TAG, "requestShowCardIfHas: " + mShownCard);
        } else {
            removeCardView(requestShowCardEvent.getType());
        }
    }

    private void decorDelayTimeCard(CardEvent.WereWolfRequestShowCardEvent requestShowCardEvent, Button card) {
        int cardWidth = ScreenUtils.dip2px(45);
        int cardHeight = ScreenUtils.dip2px(25);
        card.setLayoutParams(new ViewGroup.LayoutParams(cardWidth, cardHeight));
        /**
         * 确定位置坐标
         */
        int x = (int) ScreenUtils.getCurrentScreenWidth() - cardWidth - ScreenUtils.dip2px(14);
        /**
         * 这里需要加一个判断，如果是5.0以下，那么还需要在多减去一个状态栏
         */
        int d = ScreenUtils.dip2px(8);
        int y = 0;
        if (AppManager.GreaterLollipop()) {
            y = (int) requestShowCardEvent.getReferencePoint().y + d;
        } else {
            y = (int) requestShowCardEvent.getReferencePoint().y + ScreenUtils.dip2px(25) + d;
        }
        card.setX(x);
        card.setY(y);

        card.setText(R.string.string_werewolf_delay_text);
        card.setBackgroundResource(R.drawable.drawable_werewolf_delay_time);
    }

    private void decorCheckCard(CardEvent.WereWolfRequestShowCardEvent requestShowCardEvent, Button card) {
        int cardWidth = ScreenUtils.dip2px(45);
        int cardHeight = ScreenUtils.dip2px(25);
        card.setLayoutParams(new ViewGroup.LayoutParams(cardWidth, cardHeight));
        /**
         * 确定位置坐标
         */
        int x = (int) ScreenUtils.dip2px(14);
        /**
         * 这里需要加一个判断，如果是5.0以下，那么还需要在多减去一个状态栏
         */
        int d = ScreenUtils.dip2px(8);
        int y = 0;
        if (AppManager.GreaterLollipop()) {
            y = (int) requestShowCardEvent.getReferencePoint().y + d;
        } else {
            y = (int) requestShowCardEvent.getReferencePoint().y + ScreenUtils.dip2px(25) + d;
        }
        card.setX(x);
        card.setY(y);

        card.setText(R.string.string_werewolf_check_text);
        card.setBackgroundResource(R.drawable.drawable_werewolf_card_check);
    }

    public void removeAllCardView() {
        /**
         * 移除所有的卡片View
         */
        Iterator iterator = mShownCard.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, View> entry = (Map.Entry<String, View>) iterator.next();
            mViewGroup.removeView(entry.getValue());
        }
        mShownCard.clear();
    }

    public void removeCardView(String type) {
        View view = mShownCard.remove(type);
        if (view != null) {
            mViewGroup.removeView(view);
        }
    }

    @Override
    public void showCard(CardEvent.WereWolfRequestShowCardEvent event) {
        requestShowWereWolfCardIfHas(event);
    }

    @Override
    public void dismissCard(CardEvent.WereWolfDismissCardEvent event) {
        requestDismissCard(event);
    }

    @Override
    public void useCard(CardEvent.WereWolfRequestUseCardEvent event) {
        requestUseCard(event);
    }
}
