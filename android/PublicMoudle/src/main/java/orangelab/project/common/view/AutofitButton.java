package orangelab.project.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;

public class AutofitButton extends Button {

    private float autoTextSize;

    public AutofitButton(Context context) {
        super(context);
    }

    public AutofitButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutofitButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AutofitButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        autoTextSize = getTextSize();
        fixTextSize();
        setTextSize(TypedValue.COMPLEX_UNIT_PX, autoTextSize);
    }

    private void fixTextSize() {
        Paint p = getPaint();
        p.setTypeface(getTypeface());
        p.setTextSize(autoTextSize);
        float needWidth = getPaddingLeft() + getPaddingRight() + p.measureText(getText().toString());
        if (needWidth > getWidth()) {
            autoTextSize = getTextSize() / 3 * 2;
            fixTextSize();
            //setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() / 3 * 2);
            //autoFitTextSize();
        }
    }

}
