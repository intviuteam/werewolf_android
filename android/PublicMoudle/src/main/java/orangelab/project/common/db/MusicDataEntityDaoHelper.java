package orangelab.project.common.db;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.androidtoolkit.FileToolKit;
import com.androidtoolkit.PLog;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import orangelab.project.common.db.entity.MusicDataEntity;
import orangelab.project.common.db.gen.DaoMaster;
import orangelab.project.common.db.gen.DaoSession;
import orangelab.project.common.db.gen.MusicDataEntityDao;
import orangelab.project.voice.musiccompany.event.MusicEvent;

public class MusicDataEntityDaoHelper {

    private static final String TAG = "MusicDaoHelper";
    private DaoMaster.DevOpenHelper mHelper;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    private MusicDataEntityDao musicDataEntityDao;

    public MusicDataEntityDaoHelper(Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context, DBConstant.DB_NAME, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = mDaoMaster.newSession();
        this.musicDataEntityDao = mDaoSession.getMusicDataEntityDao();
    }

    /**
     * 插入一条数据
     */
    public long insert(@NonNull MusicDataEntity entity) {
        Log.i(TAG, "insert: ");
        try {
            if (musicDataEntityDao != null) {
                return musicDataEntityDao.insert(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 删除一条数据
     */
    public void delete(MusicDataEntity entity) {
        Log.i(TAG, "delete: ");
        try {
            if (musicDataEntityDao != null) {
                musicDataEntityDao.delete(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 更新一条数据/同一个实体
     */
    public void update(MusicDataEntity entity) {
        Log.i(TAG, "update: ");
        try {
            if (musicDataEntityDao != null) {
                musicDataEntityDao.update(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据id更改状态
     */
    public void update(String id, int status) {
        Log.i(TAG, "update: ");
        try {
            if (musicDataEntityDao != null) {
                MusicDataEntity unique = musicDataEntityDao.queryBuilder().where(MusicDataEntityDao.Properties.Id.eq(id)).unique();
                if (unique != null) {
                    unique.setState(status);
                    musicDataEntityDao.update(unique);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查找所有数据
     */
    public List<MusicDataEntity> queryForAll() {
        Log.i(TAG, "queryForAll: ");
        List<MusicDataEntity> existsMusics = new ArrayList<>();
        List<MusicDataEntity> removeMusics = new ArrayList<>();
        try {
            if (musicDataEntityDao != null) {
                List<MusicDataEntity> allMusics = musicDataEntityDao.queryBuilder().build().list();
                for (MusicDataEntity entity : allMusics) {
                    File file = FileToolKit.loadFile2Mp3(entity.getPlay_url());
                    if (file != null) {
                        //本地还存在
                        existsMusics.add(entity);
                    } else {
                        removeMusics.add(entity);
                    }
                }
            }
            for (MusicDataEntity entity : removeMusics) {
                delete(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return existsMusics;
    }
}
