package orangelab.project.common.bridge.minigame;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.R;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.view.ScreenUtils;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import orangelab.project.common.effect.EffectsUtils;
import orangelab.project.game.view.MiniGameRoundProgressBar;
import orangelab.project.minigame.manager.MiniGameDownloadManager;

/**
 * game_werewolf
 * 2018/8/1 下午6:56
 * Mystery
 */
public class MiniGameDownloadView extends MiniGameRoundProgressBar implements MiniGameDownloadManager.MiniGameDownloadObserver {

    private static final String TAG = "MiniGameDownloadView";

    private JSONObject mJSONObject;
    private String mGameType;
    private String mDownload;

    public MiniGameDownloadView(@NonNull Context context) {
        this(context, null);
    }

    public MiniGameDownloadView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MiniGameDownloadView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        int padding = ScreenUtils.dip2px(6);
        setPadding(padding, padding, padding, padding);
        clear();
    }

    public void setJSONObject(JSONObject JSONObject) {
        post(() -> {
            mJSONObject = JSONObject;
            if (mJSONObject != null) {
                mGameType = mJSONObject.optString("type");
                mDownload = mJSONObject.optString("download");
            }
            setOnClickListener(v -> {
                /**
                 * 点击判断是否需要下载
                 */
                if (!TextUtils.isEmpty(mGameType) && !TextUtils.isEmpty(mDownload)) {
                    if (MiniGameDownloadManager.INSTANCE.checkIsDownloading(mGameType)) {
                        /**a
                         * 正在下载
                         */
                        sendDataToRn("0");
                        return;
                    }
                    /**
                     * 校验
                     */
                    if (MiniGameDownloadManager.INSTANCE.checkDownloaded(mGameType, mDownload)) {
                        /**
                         * 已经下载了
                         */
                        sendDataToRn("1");
                    } else {
                        /**
                         * 还没有下载则执行下载
                         */
                        unclear();
                        setProgress(0);
                        MiniGameDownloadManager.INSTANCE.addObserver(this);
                        MiniGameDownloadManager.INSTANCE.beginDownload(mGameType, mDownload);
                        sendDataToRn("0");
                    }
                } else {
                    /**
                     * 否则按照最原始逻辑处理
                     */
                    sendDataToRn("1");
                }
            });

            if (TextUtils.isEmpty(mGameType)) {
                if (MiniGameDownloadManager.INSTANCE.checkIsDownloading(mGameType)) {
                    int progress = MiniGameDownloadManager.INSTANCE.checkProgress(mGameType);
                    if (progress >= 0) {
                        unclear();
                        setProgress(progress);
                    }
                }
            }
        });
    }

    public void sendDataToRn(String state) {
        if (mJSONObject != null) {
            WritableMap event = Arguments.createMap();
            event.putString("type", mJSONObject.optString("type"));
            event.putString("help", mJSONObject.optString("help"));
            event.putString("game_bg", mJSONObject.optString("game_bg"));
            event.putString("name", mJSONObject.optString("name"));
            event.putString("icon", mJSONObject.optString("icon"));
            event.putString("download", mJSONObject.optString("download"));
            event.putString("url", mJSONObject.optString("url"));
            event.putString("state", state);
            Context context = getContext();
            if (context instanceof ReactContext) {
                ReactContext reactContext = (ReactContext) context;
                reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(getId(), "checkGameEvent", event);
            }
        }
    }

    @Override
    public void onDownloadStart(@NotNull String gameType) {
        if (TextUtils.equals(gameType, mGameType)) {
            setProgress(0);
        }
    }

    @Override
    public void onDownloadProgress(@NotNull String gameType, int progress) {
        if (TextUtils.equals(gameType, mGameType)) {
            setProgress(progress);
        }
    }

    @Override
    public void onDownloadFinish(@NotNull String gameType) {
        if (TextUtils.equals(gameType, mGameType)) {
            clear();
        }
    }

    @Override
    public void onDownloadError(@NotNull String gameType, @NotNull Exception e) {
        if (TextUtils.equals(gameType, mGameType)) {
            ToastToolKit.showShort(R.string.network_is_unavailable);
            clear();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        MiniGameDownloadManager.INSTANCE.removeObserver(this);
    }
}
