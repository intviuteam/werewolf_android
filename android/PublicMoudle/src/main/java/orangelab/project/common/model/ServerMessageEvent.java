package orangelab.project.common.model;

import com.networktoolkit.transport.Constant;

import org.json.JSONObject;

/**
 * Author:1727
 * 2017/3/2
 */

public class ServerMessageEvent extends BaseServerEvent {
    public int code;
    public String request_id;
    public String message = "";//20180122添加了message，用于提示用户

    public ServerMessageEvent(String fromSocketType, JSONObject jsonObject) {
        super(fromSocketType, jsonObject);
        try {
            code = 1000; // TODO: 2018/2/7 对于Messsage类型的消息 默认为code1000
            message = jsonObject.optString(Constant.MESSAGE);
            request_id = jsonObject.optString(REQUEST_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRequestId() {
        return request_id;
    }

    @Override
    public String toString() {
        return "ServerMessageEvent{\n"
                + super.toString() +
                " \n}";
    }
}
