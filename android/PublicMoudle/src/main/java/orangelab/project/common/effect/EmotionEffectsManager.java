package orangelab.project.common.effect;

import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.PLog;
import com.androidtoolkit.ThreadToolKit;
import com.toolkit.action.Action;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import orangelab.project.common.effect.emotion.EmotionManiFest;

import orangelab.project.common.manager.ApiManager;

/**
 * Created by hwx on 25/07/2018.
 */

class EmotionEffectsManager {

    private static final String TAG = "EmotionEffectsManager";

    private boolean isChecking = false;

    public EmotionEffectsManager() {
    }

    private boolean isChecking() {
        return isChecking;
    }

    private void setChecking(boolean checking) {
        isChecking = checking;
    }

    public void refreshEmotionManifest(Action<Integer> callback) {
        if (isChecking()) return;
        setChecking(true);
        final int version = EffectsMainFestManager.aManager.getEmotionVersion();
        ApiManager.GetEmotionManiFest(version, (value, e) -> {
            if (e == null) {
                /**
                 * 服务器返回了正常的数据，说明需要刷新数据，并重新校验
                 */
                PLog.i(TAG, "Need Refreash All Emotion Data");
                if (value != null) {
                    diffCheckEmotionManifest(version, value, callback);
                }
            } else {
                PLog.i(TAG, "Need Check All Emotion Data");
                /**
                 * 说明不需要更新
                 */
                EmotionManiFest temp = EffectsMainFestManager.aManager.getEmotionManiFest();
                if (temp != null) {
                    Set<String> result = EffectsMainFestManager.aManager.verifyEmotionManifestData(temp);
                    PLog.i(TAG, "Emotion result " + result);
                    if (result != null && result.size() > 0) {

                        /**
                         * 下载一下
                         */
                        EffectsMainFestManager.aManager.downloadAndSaveEmotionManifestResource(temp, false);
                        /**
                         * 校验一下
                         */
                        EffectsMainFestManager.aManager.verifyEmotionManifestData(temp);

                    } else {
                        EffectsMainFestManager.aManager.updateEmotionManifest(temp);
                        if (callback != null) {
                            callback.func(0);
                        }
                    }
                } else {
                    PLog.i(TAG, "BAD Action");
                    /**
                     * local 文件不存在了这里操作比较复杂,说明之前存在过，但是因为何种原因文件不存在了
                     *
                     * 1.移除版本号
                     * 2.移除清单文件，重新进行下载
                     */
                    EffectsMainFestManager.aManager.removeEmotionManifest();
                    EffectsMainFestManager.aManager.removeEmotionManifestVersion();
                    setChecking(false);
                    refreshEmotionManifest(null);
                    return;
                }
            }
            setChecking(false);
        });
    }

    private void downloadAndVerifyEmotionManifest(EmotionManiFest input, boolean force, Action<Integer> callback) {

        /**
         * 拉取表情数据
         */
        EffectsMainFestManager.aManager.downloadAndSaveEmotionManifestResource(input, force);

        /**
         * 下载完毕后，进行文件完整性校验
         */
        Set<String> verifyResult = EffectsMainFestManager.aManager.verifyEmotionManifestData(input);
        PLog.i(TAG, "Emotion Result " + verifyResult);
        if (verifyResult != null && verifyResult.size() > 0) {
            if (callback != null) {
                callback.func(-1);
            }
        } else {
            if (callback != null) {
                callback.func(0);
            }
        }

    }

    private void diffCheckEmotionManifest(int localVersion, EmotionManiFest input, Action<Integer> callback) {
        EmotionManiFest maniFest = EffectsMainFestManager.aManager.getEmotionManiFest();
        if (maniFest == null) {
            /**
             * 本地还没有Emotion文件
             */
            try {
                EffectsMainFestManager.aManager.saveEmotionManifest(input);
                downloadAndVerifyEmotionManifest(input, false, callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            /**
             * 本地已经有了需要判断版本号
             */
            try {
                if (maniFest.version == input.version) {
                    if (callback != null) {
                        callback.func(0);
                    }
                } else {
                    downloadAndVerifyEmotionManifest(input, false, callback);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (callback != null) {
                    callback.func(-1);
                }
            }
        }
    }


    public void queryEmotionManifest(Action<List<EmotionManiFest.Emotion>> emotionListCallback) {
        ThreadToolKit.Async(() -> {
            EmotionManiFest emotionManifest = EffectsMainFestManager.aManager.getEmotionManiFest();
            if (emotionManifest != null) {
                /**
                 * 1。先校验一下表情的完备性
                 */
                try {
                    Set<String> verifyResult = EffectsMainFestManager.aManager.verifyEmotionManifestData(emotionManifest);
                    if (verifyResult != null) {
                        List<EmotionManiFest.Emotion> result = new ArrayList<>();
                        for (EmotionManiFest.Emotion item : emotionManifest.data) {
                            if (verifyResult.contains(item.type) || TextUtils.isEmpty(item.thumbnail)) {
                                /**
                                 * 包含校验失败的表情
                                 */
                                continue;
                            }
                            /**
                             * 这里是正常的表情
                             */
                            EmotionManiFest.Emotion emotion = new EmotionManiFest.Emotion(item.type, item.mark, item.thumbnail,item.url, 2000);
                            result.add(emotion);
                        }
                        /**
                         * 返回本地存储的礼物列表
                         */
                        if (emotionListCallback != null) {
                            emotionListCallback.func(result);
                        }
                        EffectsMainFestManager.aManager.downloadAndSaveEmotionManifestResource(emotionManifest, false);
                    } else {
                        /**
                         * 校验失败,原因是没有礼物信息，进行一次比对下载
                         */
                        EffectsMainFestManager.aManager.downloadAndSaveEmotionManifestResource(emotionManifest, false);
                        /**
                         * 比对完成这次认为直接失败
                         */
                        if (emotionListCallback != null) {
                            emotionListCallback.func(null);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    /**
                     * 异常，认为表情加载失败
                     */
                    if (emotionListCallback != null) {
                        emotionListCallback.func(null);
                    }
                }

            } else {
                if (emotionListCallback != null) {
                    emotionListCallback.func(null);
                }
            }
        });
    }

    public void refreshEmotionManifestForEmotions(Action<Integer> callback) {
        if (isChecking()) {
            return;
        }
        setChecking(true);
        if (EffectsMainFestManager.aManager.getEmotionManiFest() == null) {
            /**
             * 目前没有表情
             */
            Log.i(TAG, "RefreshEmotionManifestForEmotions: we don't find Manifest");
            final int version = EffectsMainFestManager.aManager.getEmotionVersion();
            ApiManager.GetEmotionManiFest(version, (value, e) -> {
                if (e == null) {
                    diffCheckEmotionManifest(version, value, callback);
                }
                setChecking(false);
            });
        } else {
            Log.i(TAG, "RefreshEmotionManifestForEmotions: we find Manifest");
            setChecking(false);
            return;
        }
    }

    public InputStream getInputStream(String url) {
        return EffectsMainFestManager.GetEmotionInputStream(url);
    }
}
