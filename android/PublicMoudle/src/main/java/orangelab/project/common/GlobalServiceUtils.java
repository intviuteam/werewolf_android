package orangelab.project.common;

import android.text.TextUtils;

import com.datasource.GlobalUserState;

import orangelab.project.common.engine.SocketEngine;

/**
 * game_werewolf
 * 2018/5/23 下午4:24
 * Mystery
 */
public class GlobalServiceUtils {

    public static boolean canShowPrivateRoomInvite() {
        String curType = GlobalUserState.getGlobalState().getCurGameType();
        return (TextUtils.isEmpty(curType) || TextUtils.equals(curType, SocketEngine.AUDIO)) && !GlobalUserState.getGlobalState().isActionBlock();
    }
}
