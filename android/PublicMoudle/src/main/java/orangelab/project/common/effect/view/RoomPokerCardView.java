package orangelab.project.common.effect.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.R;
import com.androidtoolkit.view.PokerCard;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2018/7/30 下午4:35
 * Mystery
 */
public class RoomPokerCardView extends LinearLayout {

    private PokerCard card1;
    private PokerCard card2;
    private PokerCard card3;

    public RoomPokerCardView(Context context) {
        this(context, null);
    }

    public RoomPokerCardView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomPokerCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(HORIZONTAL);
    }

    private PokerCard createPokerCard(int cardWidth, int cardHeight, int backResource, int frontResource, int margin) {
        PokerCard result = new PokerCard(getContext());
//        result.setBack(R.drawable.card_back);
//        result.setCard(R.drawable.diamond_a);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(cardWidth, cardHeight);
        layoutParams1.leftMargin = margin;
        layoutParams1.rightMargin = margin;
        result.setLayoutParams(layoutParams1);
        return result;
    }

    /**
     * 测试卡牌
     *
     * @param width
     * @param height
     */
    public void initCardTest(int width, int height) {

//        int cardWidth = (width - 36) / 3;
//        int cardHeight = height;
//
//        card1 = createPokerCard(cardWidth, cardHeight, R.drawable.card_back, R.drawable.diamond_a, 6);
//        addView(card1);
//
//        card2 = createPokerCard(cardWidth, cardHeight, R.drawable.card_back, R.drawable.heart_7, 6);
//        addView(card2);
//
//        card3 = createPokerCard(cardWidth, cardHeight, R.drawable.card_back, R.drawable.spade_8, 6);
//        addView(card3);
//
//        post(() -> {
//            card1.setListener(new PokerCard.PokerCardFlipListener() {
//                @Override
//                public void onFlipStart() {
//
//                }
//
//                @Override
//                public void onFlipEnd() {
//                    card2.setListener(new PokerCard.PokerCardFlipListener() {
//                        @Override
//                        public void onFlipStart() {
//
//                        }
//
//                        @Override
//                        public void onFlipEnd() {
//                            card3.setListener(new PokerCard.PokerCardFlipListener() {
//                                @Override
//                                public void onFlipStart() {
//
//                                }
//
//                                @Override
//                                public void onFlipEnd() {
//
//                                }
//                            });
//                            card3.flip();
//                        }
//                    });
//                    card2.flip();
//                }
//            });
//            card1.flip();
//        });
    }

}
