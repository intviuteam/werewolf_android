package orangelab.project.common.utils;

import android.content.Context;
import android.text.TextUtils;

import orangelab.project.common.PersonalDataHelper;

import com.R;

import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.WereWolfGameContext;
import orangelab.project.game.config.GameRuleConfig;
import orangelab.project.common.model.ApplyResult;
import orangelab.project.common.model.OnlineModel;

import com.networktoolkit.transport.Constant;

import orangelab.project.common.model.DeathInfo;
import orangelab.project.common.model.GameOverResult;
import orangelab.project.common.model.LinkResult;
import orangelab.project.common.model.StartResult;
import orangelab.project.common.model.VoteResultInfo;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;


/**
 * Author:1727
 * 2017/3/11
 * 用于显示系统的信息
 */

public class MessageUtils {

    private static Context bindContext;

    /**
     * 游戏开始
     *
     * @param payload
     * @return
     */
    public static String getGameStartSystemMessage(JSONObject payload) {
        StartResult result = GsonHelper.getGson().fromJson(payload.toString(), StartResult.class);
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.game_start));
        sb.append(getString(R.string.game_start_p1));
        Map<String, Integer> roleMap = result.role_map;
        if (roleMap != null) {
            Iterator iterator = roleMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                String key = (String) entry.getKey();
                int value = (int) entry.getValue();
                if (value != 0) {
                    sb.append("\n");
                    sb.append(bindContext.getString(R.string.system_message_start_role_map, Integer.toString(value),
                            getRole(key)));
                }
            }
        }
        return sb.toString();
    }

    public static String getRoleForBoard(String role) {
        if (role.equals(Constant.ROLE_SEER)) {
            return getString(R.string.role_seer_simple);
        } else if (role.equals(Constant.ROLE_WOLF)) {
            return getString(R.string.role_wolf_simple);
        } else if (role.equals(Constant.ROLE_HUNTER)) {
            return getString(R.string.role_hunter_simple);
        } else if (role.equals(Constant.ROLE_WITCH)) {
            return getString(R.string.role_witch_simple);
        } else if (role.equals(Constant.ROLE_PEOPLE)) {
            return getString(R.string.role_people_simple);
        } else if (role.equals(Constant.ROLE_CUPID)) {
            return getString(R.string.role_cupid);
        } else if (role.equals(Constant.ROLE_GUARD)) {
            return getString(R.string.role_guard_simple);
        } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
            return getString(R.string.role_werewolf_king);
        } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
            return getString(R.string.werewolf_role_magician);
        } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
            return getString(R.string.werewolf_role_demon);
        } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
            return getString(R.string.werewolf_role_knight);
        } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
            return getString(R.string.werewolf_role_dark_wolf_king);
        }
        return "";
    }

    public static String getGameStartSystemMessageForBoard(Map<String, Integer> roleMap) {
        StringBuilder sb = new StringBuilder();
        if (roleMap != null) {
            Iterator iterator = roleMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                String key = (String) entry.getKey();
                int value = (int) entry.getValue();
                if (value > 1) {
                    sb.append(bindContext.getString(R.string.werewolf_game_start_for_board, getRoleForBoard(key), Integer.toString(value)) + "  ");
                } else if (value == 1) {
                    sb.append(getRoleForBoard(key) + "  ");
                } else {

                }
            }
        }
        return sb.toString();
    }

    public static String getGameStartSystemMessageForBoard(StartResult result) {
        return getGameStartSystemMessageForBoard(result.role_map);
    }

    public static String getGameLinkResultMessage(WereWolfGameContext context, LinkResult linkResult) {
        if (linkResult != null) {
            Map<Integer, Boolean> lovers = linkResult.lovers;
            int select1 = -1, select2 = -1;
            int otherPosition = -1;
            boolean has = false;
            if (lovers != null) {
                Iterator iterator = lovers.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    int key = (int) entry.getKey() + 1;
                    if (context.getPersonalPosition() != key) {
                        otherPosition = key;
                    } else {
                        has = true;
                    }
                    if (select1 == -1) {
                        select1 = key;
                    } else {
                        select2 = key;
                    }
                }
            }
            if (context.getSelfRole().equals(Constant.ROLE_CUPID)) {
                return getString(R.string.system_link_result_cupid, Integer.toString(select1), Integer.toString
                        (select2));

            } else {
                if (has) {
                    return getString(R.string.system_link_result_normal, Integer.toString(otherPosition));
                }
            }
        }
        return "";
    }

    public static String getSunSetSystemMessage(int index) {
        return getString(R.string.system_sun_set, Integer.toString(index));
    }

    public static String getApplySystemMessage() {
        return getString(R.string.system_apply);
    }

    public static String getApplyResultSystemMessage(ApplyResult applyResult) {
        if (applyResult != null && applyResult.positions != null) {
            return "";
        } else {
            return getString(R.string.board_vote_result_no_sheriff);
        }
    }

    public static String getDeathInfoSystemMessage(DeathInfo death) {
        StringBuilder result = new StringBuilder();
        if (death != null) {
            if (death.death_info.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (DeathInfo.DeathInfoItem key : death.death_info) {
                    sb.append((getSimplePositionMessage(key.killed + 1)));
                }
                result.append(getString(R.string.system_sunup_dead, sb.toString()));

                StringBuilder roleString = new StringBuilder();
                for (DeathInfo.DeathInfoItem key : death.death_info) {
                    if (TextUtils.isEmpty(key.killed_role)) {
                        continue;
                    } else {
                        if (key.killed_role.equals(Constant.ROLE_HUNTER)) {
                            roleString.append(getHunterSystemMessage(key.killed + 1));
                        }
                    }
                }
                if (TextUtils.isEmpty(roleString.toString())) {

                } else {
                    result.append("," + roleString.toString());
                }
            } else {
                result.append(getSunUpNoDeath());
            }
        }
        return result.toString();
    }

    public static String getTakeAwayResultSystemMessage(DeathInfo death) {
        StringBuilder sb = new StringBuilder();
        if (death != null && death.death_info != null && death.death_info.size() > 0) {
            for (DeathInfo.DeathInfoItem key : death.death_info) {
                if (key != null) {
                    if (key.reason.equals(Constant.STATE_TOKEN_AWAY)) {
                        sb.append(getHunterKillerMessage(key.killer + 1, key.killed + 1));
                    }
                    if (key.dead) {
                        sb.append("," + getString(R.string.werewolf_member_dead, Integer.toString(key.killed + 1)));
                    } else {
                        if (key.real_dead != -1) {
                            sb.append("," + getString(R.string.werewolf_member_dead, Integer.toString(key.real_dead + 1)));
                        }
                    }
                    if (key.reason.equals(Constant.STATE_LINKED)) {
                        sb.append("\n");
                        sb.append(
                                getString(R.string.system_vote_death_by_link, getSimplePositionMessage(key.killed + 1))
                        );
                    }
                }
            }
        }
        return sb.toString();
    }

    public static String getVoteResultSystemMessage(VoteResultInfo info) {
        List<Integer> positions = info.positions;
        StringBuilder result = new StringBuilder();
        result.append(getString(R.string.game_vote_result_p1));
        if (info.type.equals(Constant.APPLY)) {
            if (info.need_pk) {
                result.append("\n");
                for (int a : positions) {
                    result.append(MessageUtils.getGameMemberPositionString(a + 1) + " ");
                }
                result.append(MessageUtils.getString(R.string.system_apply_pk_result));
            } else {
                if (Utils.targetListIsNull(positions)) {
                    result.append("\n" + MessageUtils.getString(R.string.system_apply_no_result));
                } else {
                    int position = positions.get(0) + 1;
                    result.append("\n" + getString(R.string.system_apply_vote_result, getSimplePositionMessage(position)));
                }
            }
        } else if (info.type.equals(Constant.TYPE_VOTE_RESULT_EQUAL)) {
            StringBuilder sb = new StringBuilder();
            for (Integer i : positions) {
                sb.append(getSimplePositionMessage(i + 1));
            }
            result.append("\n" + getString(R.string.system_vote_equal, sb.toString()));
        } else if (info.type.equals(Constant.TYPE_VOTE_RESULT_DEATH)) {
            for (DeathInfo.DeathInfoItem item : info.death_info) {
                if (TextUtils.equals(item.reason, Constant.STATE_VOTED)) {
                    result.append("\n");
                    result.append(
                            getString(R.string.system_vote_death_by_voted, getSimplePositionMessage(item.killed + 1)));

                }

                if (TextUtils.equals(item.reason, Constant.STATE_LINKED)) {
                    result.append("\n");
                    result.append(
                            getString(R.string.system_vote_death_by_link, getSimplePositionMessage(item.killed + 1)));
                    continue;
                }
                if (TextUtils.equals(item.killed_role, Constant.ROLE_HUNTER)) {
                    result.append("\n");
                    result.append(MessageUtils.getHunterSystemMessage(item.killed + 1));
                    continue;
                }
            }
        } else if (info.type.equals(Constant.TYPE_VOTE_RESULT_NO_DEATH)) {
            result.append("\n");
            result.append(getString(R.string.system_vote_all_quit));
        }
        return result.toString();
    }

    public static String getSunUpNoDeath() {
        return getString(R.string.system_sunup_no_dead);
    }

    public static void setBindContext(Context bindContext) {
        MessageUtils.bindContext = bindContext;
    }

    public static String getGameMemberPositionString(int position) {
        return position + "号玩家";
    }

    public static String getVoteMemberString(int position) {
        return "[" + position + "]";
    }

    public static String getPoisonMember(int position) {
        return "本轮使用毒药毒死" + position + "号玩家";
    }

    public static String getSaveMemberString(int position) {
        return "本轮使用灵药救了" + position + "号玩家";
    }

    public static String getCheckResultString(int position, String role) {
        return getString(R.string.system_check_result,
                getSimplePositionMessage(position),
                getManTypeString(role));
    }

    public static String getRoleIdentityDialogTitle() {
        return getString(R.string.game_check_myself_role);
    }

    public static String getBoardSunSetString() {
        return getString(R.string.game_waiting_other);
    }

    public static String getBoardVoteResult(VoteResultInfo info) {
        StringBuilder result = new StringBuilder();
        String type = info.type;
        List<Integer> position = info.positions;
        if (TextUtils.equals(type, Constant.TYPE_VOTE_RESULT_DEATH)) {
            StringBuilder sb = new StringBuilder();
            for (Integer i : position) {
                sb.append(getSimplePositionMessage(i + 1));
            }
            result.append(getString(R.string.system_vote_death_by_voted, sb.toString()));
            appendDeathInfoMessage(result, info.death_info);
            return result.toString();
        } else if (TextUtils.equals(type, Constant.TYPE_VOTE_RESULT_EQUAL)) {
            StringBuilder sb = new StringBuilder();
            for (Integer i : position) {
                sb.append(getSimplePositionMessage(i + 1));
            }
            result.append(getString(R.string.system_vote_equal, sb.toString()));
            return result.toString();
        } else {
            result.append(getSystemBoardAllQuitVote());
            return result.toString();
        }
    }
    //==================SystemBoardView==================================================

    public static String getSystemBoardGameStartString() {
        return getSystemBoardSunStateString(1, Constant.SUNUP);
    }

    public static String getSystemBoardAssignedRoleString() {
        return bindContext.getString(R.string.board_assigned_role);
    }

    public static String getSystemBoardLinkString() {
        return getString(R.string.board_link);
    }

    public static String getSystemBoardLinkResultString() {
        return bindContext.getString(R.string.board_link_result);
    }

    public static String getSystemBoardSunStateString(int dayindex, String type) {
        if (type.equals(Constant.SUNSET)) {
            return bindContext.getString(R.string.board_game_sunset, Integer.toString(dayindex));
        } else {
            return bindContext.getString(R.string.board_game_sunup, Integer.toString(dayindex));
        }
    }

    public static String getSystemBoardApplyString() {
        return bindContext.getString(R.string.board_apply);
    }

    public static String getSystemBoardSpeechString(int position, boolean isDead) {
        if (isDead) {
            return bindContext.getString(R.string.board_speech_death, Integer.toString(position));
        } else {
            return bindContext.getString(R.string.board_speech, Integer.toString(position));
        }
    }

    public static String getSystemBoardVoteString() {
        return bindContext.getString(R.string.board_vote);
    }

    public static String getSystemBoardApplyResultNoSheriffString() {
        return bindContext.getString(R.string.board_vote_result_no_sheriff);
    }

    public static String getSystemBoardDeathInfoString(DeathInfo death) {
        StringBuilder result = new StringBuilder();
        if (death != null) {
            if (death.death_info.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (DeathInfo.DeathInfoItem key : death.death_info) {
                    sb.append((getSimplePositionMessage(key.killed + 1)));
                }
                result.append(getString(R.string.system_sunup_dead, sb.toString()));
                StringBuilder roleString = new StringBuilder();
                for (DeathInfo.DeathInfoItem key : death.death_info) {
                    if (TextUtils.isEmpty(key.killed_role)) {
                        continue;
                    } else {
                        if (key.killed_role.equals(Constant.ROLE_HUNTER)) {
                            roleString.append(getHunterSystemMessage(key.killed + 1));
                        }
                    }
                }
                if (TextUtils.isEmpty(roleString.toString())) {

                } else {
                    result.append("," + roleString.toString());
                }
            } else {
                result.append(getSystemBoardSaveNightString());
            }
        }
        return result.toString();
    }

    public static String getSystemBoardSaveNightString() {
        return bindContext.getString(R.string.board_save_night);
    }

    public static String getSystemBoardHandOverMessage() {
        return getString(R.string.board_hand_over);
    }

    public static String getSystemBoardApplyResultString() {
        return getString(R.string.board_apply_result);
    }

    public static String getSystemBoardVoteSheriffString(int position) {
        return getString(R.string.system_apply_vote_result, getSimplePositionMessage(position));
    }

    public static String getSystemBoardAllQuitVote() {
        return getString(R.string.board_all_quit);
    }

    public static String getSystemBoardTakeAwayString() {
        return getString(R.string.board_take_away);
    }

    //=====================================================================================
    private static void appendDeathInfoMessage(StringBuilder sb, List<DeathInfo.DeathInfoItem> death) {
        StringBuilder roleString = new StringBuilder();
        for (DeathInfo.DeathInfoItem key : death) {
            if (TextUtils.isEmpty(key.killed_role)) {
                continue;
            } else {
                if (key.killed_role.equals(Constant.ROLE_HUNTER)) {
                    roleString.append("," + MessageUtils.getHunterSystemMessage(key.killed
                            + 1));
                }
            }
        }
        if (TextUtils.isEmpty(roleString.toString())) {
        } else {
            sb.append(roleString.toString());
        }
    }

    public static int getManType(String role) {
        if (GameRuleConfig.IsWolfTeam(role)) {
            return R.string.badman;
        } else {
            if (TextUtils.equals(role, WereWolfConstant.ROLE_GOD)) {
                return (R.string.werewolf_role_god);
            } else if (TextUtils.equals(role, WereWolfConstant.ROLE_NOT_GOD)) {
                return (R.string.werewolf_role_not_god);
            }
            return R.string.goodman;
        }
    }

    public static String getGameOverSystemMessage(GameOverResult result) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.game_over_p1));
        if (result.win_type.equals(Constant.WIN_PEOPELE)) {
            sb.append(getString(R.string.game_people_win));
        } else if (result.win_type.equals(Constant.WIN_Wolf)) {
            sb.append(getString(R.string.game_wolf_win));
        } else if (result.win_type.equals(Constant.WIN_THIRD)) {
            sb.append(getString(R.string.game_third_win));
        }
        Iterator iterator = result.roles.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            String type = (String) entry.getKey();
            List<GameOverResult.GameOverResultRoleItem> item = (List<GameOverResult.GameOverResultRoleItem>) entry
                    .getValue();
            sb.append("\n" + getTeamName(type) + ":");
            for (GameOverResult.GameOverResultRoleItem temp : item) {
                String roleString = getRole(temp.role);
                if (TextUtils.isEmpty(roleString)) {
                    continue;
                }
                sb.append("\n");
                sb.append(getVoteMemberString(temp.position + 1) + " ");
                sb.append(getRole(temp.role) + " ");
                sb.append(getOverReason(temp.status));
            }
        }

        return sb.toString();
    }

    public static String getTeamName(String type) {
        if (type.equals(Constant.WIN_PEOPELE)) {
            return getString(R.string.game_people_group);
        } else if (type.equals(Constant.WIN_Wolf)) {
            return getString(R.string.game_wolf_group);
        } else if (type.equals(Constant.WIN_THIRD)) {
            return getString(R.string.game_third_group);
        }
        return null;
    }

    public static int getRoleImage(String role) {
        try {
            if (role.equals(Constant.ROLE_SEER)) {
                return R.mipmap.room_role_checker;
            } else if (role.equals(Constant.ROLE_WOLF)) {
                return R.mipmap.room_role_wolf;
            } else if (role.equals(Constant.ROLE_HUNTER)) {
                return R.mipmap.room_role_hunter;
            } else if (role.equals(Constant.ROLE_WITCH)) {
                return R.mipmap.room_role_witch;
            } else if (role.equals(Constant.ROLE_PEOPLE)) {
                return R.mipmap.room_role_people;
            } else if (role.equals(Constant.ROLE_CUPID)) {
                return R.mipmap.room_role_cupid;
            } else if (role.equals(Constant.ROLE_GUARD)) {
                return R.mipmap.room_role_guard;
            } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
                return R.mipmap.room_role_werewolf_king;
            } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
                return R.mipmap.room_role_demon;
            } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
                return R.mipmap.room_role_magican;
            } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
                return R.mipmap.room_role_knight;
            } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
                return R.mipmap.room_role_black_wolf_king;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static String getRole(String role) {
        try {
            if (role.equals(Constant.ROLE_SEER)) {
                return getString(R.string.role_seer);
            } else if (role.equals(Constant.ROLE_WOLF)) {
                return getString(R.string.role_wolf);
            } else if (role.equals(Constant.ROLE_HUNTER)) {
                return getString(R.string.role_hunter);
            } else if (role.equals(Constant.ROLE_WITCH)) {
                return getString(R.string.role_witch);
            } else if (role.equals(Constant.ROLE_PEOPLE)) {
                return getString(R.string.role_people);
            } else if (role.equals(Constant.ROLE_CUPID)) {
                return getString(R.string.role_cupid);
            } else if (role.equals(Constant.ROLE_GUARD)) {
                return getString(R.string.role_guard);
            } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
                return getString(R.string.role_werewolf_king);
            } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
                return getString(R.string.werewolf_role_demon);
            } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
                return getString(R.string.werewolf_role_magician);
            } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
                return getString(R.string.werewolf_role_dark_wolf_king);
            } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
                return getString(R.string.werewolf_role_knight);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getOverReason(String state) {
        try {
            if (state.equals(Constant.STATE_ALIVE)) {
                return getString(R.string.string_werewolf_game_over_role_status_live);
            } else if (state.equals(Constant.STATE_KILLED)) {
                return getString(R.string.string_werewolf_game_over_role_status_wolf_kill);
            } else if (state.equals(Constant.STATE_POISON)) {
                return getString(R.string.string_werewolf_game_over_role_status_poison_kill);
            } else if (state.equals(Constant.STATE_VOTED)) {
                return getString(R.string.string_werewolf_game_over_role_status_vote_kill);
            } else if (state.equals(Constant.STATE_LINKED)) {
                return getString(R.string.string_werewolf_game_over_role_status_love_kill);
            } else if (state.equals(Constant.STATE_TOKEN_AWAY)) {
                return getString(R.string.string_werewolf_game_over_role_status_hunter_shut);
            } else if (state.equals(Constant.STATE_LEAVED)) {
                return getString(R.string.string_werewolf_game_over_role_status_run_away);
            } else if (state.equals(Constant.STATE_BOOM)) {
                return getString(R.string.string_werewolf_game_over_role_status_self_boom);
            } else if (state.equals(Constant.STATE_BOOM_AWAY)) {
                return getString(R.string.string_werewolf_game_over_role_status_boom_kill);
            } else if (state.equals(WereWolfConstant.FIGHT_AWAY)) {
                return getString(R.string.string_werewolf_game_over_role_status_fight_kill);
            } else if (state.equals(WereWolfConstant.FIGHT_APOLOGIZE_DEATH)) {
                return getString(R.string.string_werewolf_game_over_role_status_sorry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getManTypeString(String role) {
        return bindContext.getString(getManType(role));
    }

    public static String getWolfKillMessage(int position) {
        return getString(R.string.system_wolf_kill_result, Integer.toString(position));
    }

    public static String getHunterSystemMessage(int position) {
        return getString(R.string.system_hunter_hint, getSimplePositionMessage(position));
    }

    public static String getHunterKillerMessage(int killer, int killed) {
        return getString(
                R.string.system_hunter_kill, getSimplePositionMessage(killer), getSimplePositionMessage(killed));
    }

    public static String getLeavingMessage(String userName, int position, boolean remove) {
        StringBuilder sb = new StringBuilder();
        if (!remove) {
            sb.append(getGameMemberPositionString(position));
            sb.append(getString(R.string.game_gaming_leave));
        } else {
            sb.append(userName);
            sb.append(getString(R.string.game_leave_room));
        }
        return sb.toString();
    }

    public static String getSheriffTurnResult(int from, int to) {
        return getString(R.string.sheriff_associate_text, getSimplePositionMessage(from), getSimplePositionMessage(to));
    }

    public static String getSheriffResultString(int position) {
        return getString(R.string.system_hand_over_result_sheriff, getSimplePositionMessage(position));
    }

    public static String getHandoverResultNoSheriffSystemMessage() {
        return getString(R.string.system_hand_over_result_no_sheriff);
    }

    public static String getGiveUpPositionString(int position) {
        return getString(R.string.give_up_apply_string, getSimplePositionMessage(position));
    }

    public static String getString(int stringId) {
        return bindContext.getString(stringId);
    }

    public static String getString(int stringId, Object... data) {
        return bindContext.getString(stringId, data);
    }

    public static String getSimplePositionMessage(int position) {
        return getString(R.string.system_position, Integer.toString(position));
    }

    public static String getKickOutMessage(String name) {
        return getString(R.string.system_kick_out, name);
    }

    public static String updateRoomOwner(WereWolfGameContext wereWolfGameContext, int position) {
        if (wereWolfGameContext.getPersonalPosition() == position) {
            return getString(R.string.update_room_owner_yourself);
        } else {
            return getString(R.string.update_room_owner, Integer.toString(position));
        }
    }

    public static String updateRoomOwner(int position) {
        if (PersonalDataHelper.getPersonalData().getSelfPosition() == position) {
            return getString(R.string.update_room_owner_yourself);
        } else {
            return getString(R.string.update_room_owner, Integer.toString(position));
        }
    }

    public static String getLevelLimit(String type) {
        if (TextUtils.isEmpty(type)) {
            return getString(R.string.no_level_limit);
        }
        if (TextUtils.equals(type, Constant.LEVEL_LIMIT_NONE)) {
            return getString(R.string.no_level_limit);
        }
        return getString(R.string.level, type);
    }

    public static String getRealRoleForMember(String role) {
        if (TextUtils.isEmpty(role)) {
            return "";
        }
        if (role.equals(Constant.ROLE_SEER)) {
            return getString(R.string.role_seer_simple);
        } else if (role.equals(Constant.ROLE_WOLF)) {
            return getString(R.string.role_wolf_simple);
        } else if (role.equals(Constant.ROLE_HUNTER)) {
            return getString(R.string.role_hunter_simple);
        } else if (role.equals(Constant.ROLE_WITCH)) {
            return getString(R.string.role_witch_simple);
        } else if (role.equals(Constant.ROLE_PEOPLE)) {
            return getString(R.string.role_people_simple);
        } else if (role.equals(Constant.ROLE_CUPID)) {
            return getString(R.string.role_cupid_simple);
        } else if (role.equals(Constant.ROLE_GUARD)) {
            return getString(R.string.role_guard_simple);
        } else if (role.equals(Constant.ROLE_WEREWOLF_KING)) {
            return getString(R.string.short_werewolf_king);
        } else if (role.equals(WereWolfConstant.ROLE_MAGICIAN)) {
            return getString(R.string.werewolf_role_magician_short);
        } else if (role.equals(WereWolfConstant.ROLE_DEMON)) {
            return getString(R.string.werewolf_role_demon_short);
        } else if (role.equals(WereWolfConstant.ROLE_BLACK_WOLF_KING)) {
            return getString(R.string.werewolf_role_dark_wolf_king_short);
        } else if (role.equals(WereWolfConstant.ROLE_KNIGHT)) {
            return getString(R.string.werewolf_role_knight_short);
        }
        return null;
    }

    public static String getLevelString(int level) {
        return MessageUtils.getString(R.string.dialog_user_detail_user_level, Integer.toString(level));
    }

    public static final long HOUR = 1000 * 60 * 60;
    public static final long ONE_DAY_TIME = HOUR * 24;
    public static final long THREE_DAYS = ONE_DAY_TIME * 3;
    public static final long TWO_DAYS = ONE_DAY_TIME * 2;

    public static OnlineModel GetOnlineMessage(String state) {
        if (TextUtils.isEmpty(state)) {
            return null;
        }
        String[] states = state.split(":");

        try {
            int stateInt = Integer.parseInt(states[0]);
            long onlineTime = Long.parseLong(states[1]);
            OnlineModel onlineModel = new OnlineModel();
            if (stateInt != 0) {
                onlineModel.isOnline = true;
            }
            onlineModel.state = getOnlineMessage(stateInt, onlineTime);
            return onlineModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private static String getOnlineMessage(int state, long lastOnlineTime) {
        String result = "";
        if (state != 0) {
            result = MessageUtils.getString(R.string.string_online);
            return result;
        }
        /**
         * 计算差值
         */
        long curTime = System.currentTimeMillis();
        long dur = curTime - lastOnlineTime;
        if (dur < 0) {
            result = MessageUtils.getString(R.string.string_online);
            return result;
        }

        /**
         * 这里一定是>0的
         */

        if (dur >= THREE_DAYS) {
            result = MessageUtils.getString(R.string.string_online_3_days);
        } else if (dur < THREE_DAYS && dur >= TWO_DAYS) {
            result = MessageUtils.getString(R.string.string_online_2_days);
        } else if (dur < TWO_DAYS && dur >= ONE_DAY_TIME) {
            result = MessageUtils.getString(R.string.string_online_1_days);
        } else if (dur < ONE_DAY_TIME && dur >= HOUR) {
            result = MessageUtils.getString(R.string.string_online_hours, Integer.toString((int) (dur / HOUR)));
        } else {
            result = MessageUtils.getString(R.string.string_online_just);
        }
        return result;
    }
}
