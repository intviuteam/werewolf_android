package orangelab.project.common.pay;

import android.content.Context;
import android.support.annotation.NonNull;

import com.R;
import orangelab.project.common.dialog.SafeDialog;

/**
 * game_werewolf
 * 2017/7/24 上午11:27
 * Mystery
 */

public class PayLoadingDialog extends SafeDialog {

    public PayLoadingDialog(@NonNull Context context) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.dialog_pay_loading_layout);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void release() {

    }
}
