package orangelab.project.common.event;

import orangelab.project.voice.privateroom.PrivateRoomTools;
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData;

/**
 * Author:1727
 * 2017/3/3
 */

public class ViewEvent {

    public static class SpeakViewEvent {

        private int motionEvent;

        public SpeakViewEvent(int motionEvent) {
            this.motionEvent = motionEvent;
        }

        public int getMotionEvent() {
            return motionEvent;
        }
    }

    public static class GiftButtonViewEvent {

        public static class RequestShow {

        }

        public static class RequestDismiss {

        }
    }

    public static class AudioPermissionError {

    }

    public static class SimpleOneSelectMemberEvent {
        public static final int ACTION_SELECTED = 1;

        private int number;
        private int action;

        public SimpleOneSelectMemberEvent(int action, int number) {
            this.action = action;
            this.number = number;
        }

        public int getNumber() {
            return number;
        }

        public int getAction() {
            return action;
        }
    }

    public static class GameChairEvent {
        public static final int ACTION_LOCK = 1;
        public static final int ACTION_UNLOCK = 2;

        private int action;
        private int number;

        public GameChairEvent(int number, int action) {
            this.action = action;
            this.number = number;
        }

        public int getAction() {
            return action;
        }

        public int getNumber() {
            return number;
        }
    }

    public static class HideKeyBoardEvent {

    }

    public static class LeaveMessageEvent {

    }

    public static class SpeechDangerTimeEvent {

    }

    public static class SpeechNoDangerTimeEvent {

    }

    public static class DestroyEvent {
        public static final int ACTION_NORMAL = 1;
        public static final int ACTION_BY_SOCKET_RECONNECT_FAIL = 2;

        private int action;

        public DestroyEvent(int action) {
            this.action = action;
        }

        public boolean isNormal() {
            return this.action == ACTION_NORMAL;
        }

        public boolean isReconnectionFail() {
            return this.action == ACTION_BY_SOCKET_RECONNECT_FAIL;
        }
    }

    public static class RequestAddFriendEvent {

        private String toUserId;

        public RequestAddFriendEvent(String toUserId) {
            this.toUserId = toUserId;
        }

        public String getToUserId() {
            return toUserId;
        }

        public void setToUserId(String toUserId) {
            this.toUserId = toUserId;
        }
    }

    public static class ReceiveAddFriendEvent {
        private String fromType = "";
        private String fromUserID;
        private String fromUserName;
        private String fromUserIv;

        private PrivateRoomMemberData privateRoomMemberData = null;

        public ReceiveAddFriendEvent(PrivateRoomMemberData privateRoomMemberData) {
            this.fromType = PrivateRoomTools.INSTANCE.getREQUEST_FRIEND();
            this.privateRoomMemberData = privateRoomMemberData;
            this.fromUserID = privateRoomMemberData.getMemberId();
            this.fromUserName = privateRoomMemberData.getMemberName();
            this.fromUserIv = privateRoomMemberData.getMemberIcon();
        }

        public ReceiveAddFriendEvent(String fromUserID, String fromUserName, String fromUserIv) {
            this.fromUserID = fromUserID;
            this.fromUserName = fromUserName;
            this.fromUserIv = fromUserIv;
        }

        public PrivateRoomMemberData getPrivateRoomMemberData() {
            return privateRoomMemberData;
        }

        public String getFromType() {
            return fromType;
        }

        /**
         * 设置Type 用于区分新老模式
         *
         * @param fromType
         */
        public ReceiveAddFriendEvent setFromType(String fromType) {
            this.fromType = fromType;
            return this;
        }

        public String getFromUserID() {
            return fromUserID;
        }

        public String getFromUserName() {
            return fromUserName;
        }

        public String getFromUserIv() {
            return fromUserIv;
        }

        @Override
        public String toString() {
            return "RequestFriendEvent{" +
                    "fromUserID='" + fromUserID + '\'' +
                    ", fromUserName='" + fromUserName + '\'' +
                    ", fromUserIv='" + fromUserIv + '\'' +
                    '}';
        }
    }
}

