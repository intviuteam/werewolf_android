package orangelab.project.common.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import java.util.ArrayList;


public class ShareDialogAdapter extends BaseAdapter {
    private final Context mContext;
    private ArrayList mNewText;
    private ArrayList mNewImage;
    private int textColor = -1;
    private int textDrawable = -1;

    public ShareDialogAdapter(Context context, ArrayList newText, ArrayList newImage) {
        this(context, newText, newImage, -1, -1);
    }

    public ShareDialogAdapter(Context context, ArrayList newText, ArrayList newImage, int textColor, int drawable) {
        super();
        this.mContext = context;
        this.mNewText = newText;
        this.mNewImage = newImage;
        this.textColor = textColor;
        this.textDrawable = drawable;
    }

    @Override
    public int getCount() {
        return mNewText.size();
    }

    @Override
    public Object getItem(int position) {
        return mNewText.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(mContext, R.layout.item_dialog_list_share, null);
        TextView textView = (TextView) view.findViewById(R.id.text);

        if (textColor != -1) {
            textView.setTextColor(mContext.getResources().getColor(textColor));
        }

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_image);
        imageView.setBackgroundResource((Integer) mNewImage.get(position));
        textView.setText((Integer) mNewText.get(position));

        if (textDrawable != -1) {
            Drawable drawable_n = mContext.getResources().getDrawable(textDrawable);
            drawable_n.setBounds(0, 0, drawable_n.getMinimumWidth(), drawable_n.getMinimumHeight());  //此为必须写的
            textView.setCompoundDrawables(null, null, drawable_n, null);
        }
        return view;
    }

}
