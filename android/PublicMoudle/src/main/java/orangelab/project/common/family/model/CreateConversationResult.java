package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;
import com.networktoolkit.transport.Constant;

/**
 * game_werewolf
 * 2017/10/24 上午10:24
 * Mystery
 */

public class CreateConversationResult implements Keepable {

    public String conversationId = Constant.EMPTY;
    public String reason = Constant.EMPTY;
}
