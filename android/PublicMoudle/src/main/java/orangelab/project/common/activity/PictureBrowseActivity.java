package orangelab.project.common.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.R;

import orangelab.project.common.utils.ImageUtils;
import orangelab.project.common.utils.IntentDataHelper;

import com.androidtoolkit.view.ScreenUtils;
import com.photoview.PhotoView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/6/13 下午2:02
 * Mystery
 */

public class PictureBrowseActivity extends SafeActivity {

    private ViewPager viewPager;
    private TextView indexTextView;
    private ArrayList<String> imageList;
    private int curPosition = 0;
    private int lastPosition = 0;
    private PictureBrowseAdapter adapter;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_picturebrowse);
        imageList = IntentDataHelper.getImageList(getIntent());
        curPosition = IntentDataHelper.getImageCurPosition(getIntent());
        indexTextView = (TextView) findViewById(R.id.photos_index);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new PictureBrowseAdapter(this, imageList);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(curPosition);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                curPosition = position;
                adapter.resetPosition(lastPosition);
                setIndexTextView(position + 1, imageList.size());
                lastPosition = curPosition;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setIndexTextView(curPosition + 1, imageList.size());
    }

    private void setIndexTextView(int curPosition, int total) {
        indexTextView.setText(curPosition + "/" + total);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageList.clear();
//        if (adapter != null) {
//            adapter.destroy();
//        }
        System.gc(); //try gc

    }

    private static class PictureBrowseAdapter extends PagerAdapter {

        private ArrayList<String> imageList;
        private Activity context;
        private SparseArray<PhotoView> photoViewSparseArray;

        public void resetPosition(int position) {
            PhotoView photoView = photoViewSparseArray.get(position);
            if (photoView != null) {
                photoView.reset();
            }
        }

        public PictureBrowseAdapter(Activity context, ArrayList<String> imageList) {
            this.context = context;
            this.imageList = imageList;
            photoViewSparseArray = new SparseArray<>();
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            PhotoView imageView = photoViewSparseArray.get(position);
            if (imageView == null) {
                imageView = new PhotoView(context);
                imageView.setOnClickListener(v -> context.finish());
                photoViewSparseArray.put(position, imageView);
            }
            Picasso.get()
                    .load(imageList.get(position))
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE) //不在内存中缓存
                    .placeholder(R.mipmap.me_photo_default)
                    .resize(ScreenUtils.getCurrentScreenWidth(), ScreenUtils.getScreenHeight())
                    .centerInside()
                    .error(R.mipmap.me_photo_default)
                    .into(imageView);
            container.addView(imageView);
            return imageView;
        }

//        public void destroy() {
//            int key = 0;
//            for (int i = 0; i < photoViewSparseArray.size(); i++) {
//                key = photoViewSparseArray.keyAt(i);
//                PhotoView photoView = photoViewSparseArray.get(key);
//                ImageUtils.releaseImageViewResouce(photoView);
//            }
//        }
    }


}
