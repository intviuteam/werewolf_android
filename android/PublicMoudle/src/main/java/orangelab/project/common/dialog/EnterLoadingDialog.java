package orangelab.project.common.dialog;

import android.content.Context;
import android.support.annotation.NonNull;

import com.R;

/**
 * game_werewolf
 * 2017/12/21 下午2:16
 * Mystery
 */

public class EnterLoadingDialog extends SafeDialog {

    public EnterLoadingDialog(@NonNull Context context) {
        super(context, R.style.DarkDialog);
        setContentView(R.layout.dialog_enter_loading);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void release() {

    }
}
