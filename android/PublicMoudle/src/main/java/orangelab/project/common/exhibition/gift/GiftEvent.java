package orangelab.project.common.exhibition.gift;

import orangelab.project.common.exhibition.ExhibitionPackage;

/**
 * game_werewolf
 * 2017/6/28 下午5:18
 * Mystery
 */

public class GiftEvent {

    public static class GiftForGameWrapperEvent {
        private Gift mGift;
        private ExhibitionPackage mExhibitionPackage;

        public GiftForGameWrapperEvent(Gift gift, ExhibitionPackage exhibitionPackage) {
            mGift = gift;
            mExhibitionPackage = exhibitionPackage;
        }

        public Gift getGift() {
            return mGift;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }
    }

    public static class GiftForChatWrapperEvent {
        private Gift mGift;
        private ExhibitionPackage mExhibitionPackage;

        public GiftForChatWrapperEvent(Gift gift, ExhibitionPackage exhibitionPackage) {
            mGift = gift;
            mExhibitionPackage = exhibitionPackage;
        }

        public Gift getGift() {
            return mGift;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }
    }

    public static class GiftForMainPageWrapperEvent {
        private Gift mGift;
        private ExhibitionPackage mExhibitionPackage;

        public GiftForMainPageWrapperEvent(Gift gift, ExhibitionPackage exhibitionPackage) {
            mGift = gift;
            mExhibitionPackage = exhibitionPackage;
        }

        public Gift getGift() {
            return mGift;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }
    }

    public static class GiftForOutSideWrapperEvent {
        private Gift mGift;
        private ExhibitionPackage mExhibitionPackage;

        public GiftForOutSideWrapperEvent(Gift gift, ExhibitionPackage exhibitionPackage) {
            mGift = gift;
            mExhibitionPackage = exhibitionPackage;
        }

        public Gift getGift() {
            return mGift;
        }

        public ExhibitionPackage getExhibitionPackage() {
            return mExhibitionPackage;
        }
    }
}
