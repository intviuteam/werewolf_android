package orangelab.project.common.model;

import android.support.annotation.NonNull;

import com.androidtoolkit.transport.Transportable;
import com.networktoolkit.transport.Constant;
import com.toolkit.action.Keepable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.intviu.sdk.model.AudioServerInfo;
import orangelab.project.voice.config.VoiceRoomConfig;
import orangelab.project.voice.model.VoiceRoomConfigBean;
import orangelab.project.voice.musiccompany.model.PrepareSingBean;

/**
 * Author:1727
 * 2017/3/6
 */

public class EnterRoomResult implements Serializable, Transportable {

    public EnterRoomMessage room;
    public RoomServerMessage media_server;
    public int position = -1;
    public long block_upseat_duration = 0;
    public long enter_time = 0;
    public boolean is_observer = false;


    public KTV ktv;  //ktv二期数据

    public static class RoomTheme implements Serializable, Keepable {
        public String title = "";
        public String description = "";
        public String tag_color = "";
        public String tag = "";

    }

    public static class RoomConfigChange implements Serializable, Keepable {
        public RoomTheme theme;
        public String password = null;
        public Boolean enable_share_image = null;
    }

    public static class RoomConfig implements Serializable, Keepable {
        public String type;
        public boolean mini_mode;//是否可以开启小窗模式
        public int min_user;
        public int max_user;
        public int lock_start;
        public int active_count;//20180123语音房座位数
        public long image_expire_duration = 30000;
        public boolean can_boom;
        public boolean has_sheriff;
        public boolean show_role_if_dead;
        public int can_save_self;
        public boolean kill_all;
        public boolean can_cut_speaker;
        public boolean night_speak = false;
        public int min_level;
        public int max_level;
        public int level;
        public Map<String, Integer> experience;
        public List<Integer> level_limitations;
        public ExportConfig export_request;
        public VoiceRoomConfigBean.UnderCoverConfig undercover;
        public HashMap<String, Integer> role_map;
        public RoomConfigRoomInfo room_info;
        public RoomConfigRoleSets high_king;
        public MiniGameRoomConfig mini_game;
        public boolean purchase_room;//是否支持房契功能
        public RoomTheme theme; //私密房房间主题
        public boolean enable_share_image = false; //是否可以发送临时图片开关
    }

    public static class MiniGameRoomConfig implements Serializable, Keepable {
        public String url;
    }

    public static class RoomConfigRoomInfoBackGround implements Serializable, Keepable {
        public String url = "";
        public String type = "";
        public String image_id = "";
    }

    public static class RoomConfigRoomInfo implements Serializable {
        public RoomConfigRoomInfoBackGround background;
        public String owner_type = "";//20180119    可以根据这个判断是否可以购买房间
        public String server;
        public String room_id;
        public String level;
        public String password = "";
        public String status;
        public String owner_id = "";//20180118判断是否是自己，如果是自己，发请求房主消息

        @Override
        public String toString() {
            return "RoomConfigRoomInfo{" +
                    "owner_type='" + owner_type + '\'' +
                    ", server='" + server + '\'' +
                    ", room_id='" + room_id + '\'' +
                    ", level='" + level + '\'' +
                    ", password='" + password + '\'' +
                    ", status='" + status + '\'' +
                    ", owner_id='" + owner_id + '\'' +
                    '}';
        }
    }

    public static class RoomConfigRoleSets implements Serializable {
        public String people_parts = "";
        public String werewolf_parts = "";
    }

    public static class ExportConfig implements Serializable {
        public int level;
        public ExportCostConfig cost;
    }

    public static class ExportCostConfig implements Serializable {
        public String type;
        public int value;
    }

    public static class EnterRoomMessage implements Serializable {

        public String room_id;
        public int min_user;
        public int max_user;
        public String title = "";
        public String export_type;
        public String type;
        public String child_type;
        public Theme theme;
        public int likeCount;
        public String password = "";
        public List<Integer> locked_positions;
        public HashMap<Integer, EnterRoomUserItem> users;
        public HashMap<Integer, EnterRoomUserItem> observers; //观战人员列表
        public boolean isPlaying;
        public boolean can_cut_speaker;
        public RoomConfig config;
        public int user_count;                                //房间人数
        public MusicLive broadcast_data; //伴奏数据

        @Override
        public String toString() {
            return "EnterRoomMessage{" +
                    "room_id='" + room_id + '\'' +
                    ", min_user=" + min_user +
                    ", max_user=" + max_user +
                    ", title='" + title + '\'' +
                    ", export_type='" + export_type + '\'' +
                    ", type='" + type + '\'' +
                    ", child_type='" + child_type + '\'' +
                    ", theme=" + theme +
                    ", likeCount=" + likeCount +
                    ", password='" + password + '\'' +
                    ", locked_positions=" + locked_positions +
                    ", users=" + users +
                    ", observers=" + observers +
                    ", isPlaying=" + isPlaying +
                    ", can_cut_speaker=" + can_cut_speaker +
                    ", config=" + config +
                    ", user_count=" + user_count +
                    ", broadcast_data=" + broadcast_data +
                    '}';
        }
    }

    public static class Theme implements Serializable, Keepable {

        public int version;
        public String background;
        public boolean blur = true;

        @Override
        public String toString() {
            return "Theme{" +
                    "version='" + version + '\'' +
                    ", background='" + background + '\'' +
                    '}';
        }
    }

    //音乐伴奏相关数据
    public static class MusicLive implements Serializable, Keepable {

        public String broadcast_host = "";           //推流地址
        public String broadcast_receive = "";        //收流地址
        public boolean broadcast_record;             //是否录制
        public String live_broadcast_id = "";        //唱歌人的id
        public String broadcast_lyric_url = "";      //歌词地址
        public String music_id = "";                 //正在播放的music的id
        public boolean is_live_broadcast;            //是否正在直播

    }

    //ktv二期数据
    public static class KTV implements Serializable, Keepable {
        public List<PrepareSingBean.PrepareSong> songs;        //歌单列表
        public PrepareSingBean.PrepareSong prepared_song;      //当前正在显示的歌曲
        public PrepareSingBean.PrepareSong current_song;       //当前正在播放的歌曲
        public AuthorizedUser authorized_user;    //授权用户


        public static class AuthorizedUser implements Serializable, Keepable {
            public String user_id = "";
        }
    }

    public static class EnterRoomUserItem implements Serializable, Comparable<EnterRoomUserItem> {
        public String id;
        public int uid;
        public String name = "";//防止名字为null
        public String avatar;
        public int level;
        public String mini_game_title = "";
        public int sex;
        public String experience;
        public SerializableObject store;
        public SerializableObject binded_actions;
        public String status;
        public String state;//语音房状态标志:自由模式、禁言、一般
        public boolean isOut;//是否出局
        public boolean isOffline;//是否离线
        public boolean prepared;
        public boolean is_master;
        public String enter_time;
        public int position;
        public boolean is_leaved;
        public boolean is_disconnected;
        public boolean is_observer = false;
        public boolean speaking;
        public boolean canSpeak;//能否说话
        public String signature;//个性签名
        public boolean canSing = false; //当前是否有伴奏权限
        public boolean isSing = false; //当前是否在进行伴奏
        public UserInfoResult.UserActiveInfo active;
        public UserInfoResult.AvatarBox avatar_box;
        public UserInfoResult.MessageBox message_box;
        public UserVipInfo vipInfo;

        public boolean isBanMic() {
            return Constant.LIMIT.equals(state);
        }

        public boolean isFreeStyle() {
            return Constant.FREE.equals(state);
        }

        @Override
        public String toString() {
            return "EnterRoomUserItem{" +
                    "id='" + id + '\'' +
                    ", uid=" + uid +
                    ", name='" + name + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", level=" + level +
                    ", mini_game_title='" + mini_game_title + '\'' +
                    ", sex=" + sex +
                    ", experience='" + experience + '\'' +
                    ", store=" + store +
                    ", binded_actions=" + binded_actions +
                    ", status='" + status + '\'' +
                    ", state='" + state + '\'' +
                    ", isOut=" + isOut +
                    ", isOffline=" + isOffline +
                    ", prepared=" + prepared +
                    ", is_master=" + is_master +
                    ", enter_time='" + enter_time + '\'' +
                    ", position=" + position +
                    ", is_leaved=" + is_leaved +
                    ", is_disconnected=" + is_disconnected +
                    ", is_observer=" + is_observer +
                    ", speaking=" + speaking +
                    ", canSpeak=" + canSpeak +
                    ", signature='" + signature + '\'' +
                    ", canSing=" + canSing +
                    ", isSing=" + isSing +
                    ", active=" + active +
                    '}';
        }

        @Override
        public int compareTo(@NonNull EnterRoomUserItem o) {

            //比较顺序：房主最靠前，然后在麦上的，然后按进入时间，进入早的在前

            //你是房主
            if (position == Constant.MASTER_POSITION) {
                return -1;
            }
            //他是房主
            if (o.position == Constant.MASTER_POSITION) {
                return 1;
            }
            //你在麦上他在麦下
            if (position <= VoiceRoomConfig.getMaxChairNumber() && o.position > VoiceRoomConfig.getMaxChairNumber()) {
                return -1;
            }
            //你在麦下他在麦上
            if (position > VoiceRoomConfig.getMaxChairNumber() && o.position <= VoiceRoomConfig.getMaxChairNumber()) {
                return 1;
            }

            //都在麦上或都在麦下 比进入时间
            return this.enter_time.compareToIgnoreCase(o.enter_time);
        }
    }

    @Override
    public String toString() {
        return "EnterRoomResult{" +
                "room=" + room +
                '}';
    }

    public static class RoomServerMessage implements Serializable, Keepable {
        public int type;
        public AudioServerInfo standby_relay;
        public ArrayList<AudioServerInfo> turn_servers;
        public boolean relay_only;
        public String server_type;
        public String media_server;

        @Override
        public String toString() {
            return "RoomServerMessage{" +
                    "type=" + type +
                    ", standby_relay=" + standby_relay +
                    ", turn_servers=" + turn_servers +
                    ", relay_only=" + relay_only +
                    ", server_type='" + server_type + '\'' +
                    ", media_server='" + media_server + '\'' +
                    '}';
        }
    }
}
