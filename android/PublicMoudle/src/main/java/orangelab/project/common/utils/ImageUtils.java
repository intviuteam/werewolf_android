package orangelab.project.common.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.androidtoolkit.StreamToolKit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import orangelab.project.MainApplication;

/**
 * game_werewolf
 * 2017/6/21 下午12:20
 * Mystery
 */

public class ImageUtils {

    public static Bitmap compressImage(Bitmap image, int maxSizeKB) {
        Bitmap result = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > maxSizeKB) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        result = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        try {
            baos.close();
            isBm.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void releaseImageViewResouce(ImageView imageView) {
        if (imageView == null) return;
        Drawable drawable = imageView.getDrawable();
        if (drawable != null && drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
    }

    public static void releaseViewBackGround(View imageView) {
        if (imageView == null) return;
        Drawable drawable = imageView.getBackground();
        if (drawable != null && drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
    }

    /**
     * 在bitmap指定区域画bitmap
     *
     * @param container
     * @param child
     * @param top       上边距
     * @param left      左边距
     * @return
     */
    public static Bitmap drawBitmapByIcon(Bitmap container, Bitmap child, int top, int left) {
        Bitmap target = Bitmap.createBitmap(container);
        Canvas canvas = new Canvas(target);
        Paint paint = new Paint();
        canvas.drawBitmap(child, left, top, paint);

        return target;
    }

    /**
     * 在bitmap指定区域画bitmap
     *
     * @param container
     * @param childId
     * @param top
     * @param left
     * @return
     */
    public static Bitmap drawBitmapByIcon(Bitmap container, int childId, int top, int left) {
        Bitmap target = container.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(target);
        Paint paint = new Paint();
        Bitmap child = BitmapFactory.decodeResource(MainApplication.getInstance().getResources(), childId);
        canvas.drawBitmap(child, left, top, paint);
        return target;
    }

    /**
     * bitmap转byte数组
     */
    public static byte[] bitmapToByteArray(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] byteArray;
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
        } finally {
            StreamToolKit.close(stream);
        }

        return byteArray;
    }

    /**
     * @param container   外部容器
     * @param waterMarkId 水印
     * @param qrId        二维码
     * @return 绘制过的图片
     */
    public static Bitmap decorateBitmapByWatermark(Bitmap container, int waterMarkId, int qrId) {
        Bitmap target = container.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(target);
        Paint paint = new Paint();

        //画水印
        Bitmap waterMark = BitmapFactory.decodeResource(MainApplication.getInstance().getResources(), waterMarkId);
        canvas.drawBitmap(waterMark, 10, 5, paint);

        //画二维码
        Bitmap qrCode = BitmapFactory.decodeResource(MainApplication.getInstance().getResources(), qrId);
        int top = target.getHeight() - qrCode.getHeight() - 10;     //距右边10个像素
        int left = target.getWidth() - qrCode.getWidth() - 10;      //距底部10个像素
        canvas.drawBitmap(qrCode, left, top, paint);

        return target;
    }
}
