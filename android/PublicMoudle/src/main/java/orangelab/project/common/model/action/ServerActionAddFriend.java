package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/4/23 下午3:22
 * Mystery
 */

public class ServerActionAddFriend implements Keepable {
    public String user_id;
    public String name;
    public String avatar;
}
