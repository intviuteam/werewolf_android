package orangelab.project.common.tool;

import android.app.Dialog;
import android.text.TextUtils;

import com.toolkit.ToolKit;
import com.toolkit.action.Destroyable;
import com.toolkit.action.TravelMap;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2018/3/24 下午7:32
 * Mystery
 */

public class DialogManager implements Destroyable {

    private Map<String, Dialog> mDialogMap;

    public DialogManager() {
        mDialogMap = new HashMap<>();
    }

    private void dismissIfHas(String key) {
        if (mDialogMap.containsKey(key)) {
            Dialog temp = remove(key);
            if (temp != null && temp.isShowing()) {
                temp.dismiss();
            }
        }
    }

    public void removeAndDismiss(String key) {
        Dialog dialog = remove(key);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void put(String key, Dialog dialog) {
        if (TextUtils.isEmpty(key) || dialog == null) {
            return;
        }

        dismissIfHas(key);

        mDialogMap.put(key, dialog);
    }

    public void putAndShow(String key, Dialog dialog) {
        if (TextUtils.isEmpty(key) || dialog == null) {
            return;
        }

        put(key, dialog);

        dialog.show();
    }

    public Dialog remove(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        return mDialogMap.remove(key);
    }


    public Dialog get(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        return mDialogMap.get(key);
    }

    @Override
    public void destroy() {
        ToolKit.travelMap(mDialogMap, (key, value) -> {

            if (value.isShowing()) {
                value.dismiss();
            }

        });

        mDialogMap.clear();
    }
}
