package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/10/26 下午6:11
 * Mystery
 */

public class FamilyMembersApiResult implements Keepable {
    public int skip;
    public int limit;
    public String sort;
    public String groupId;
    public Integer require_count;
    public int total;
    public ArrayList<FamilyMemberInfo> datas;
}
