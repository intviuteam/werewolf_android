package orangelab.project.common.union;

import android.text.TextUtils;
import android.util.Log;

import com.R;

import orangelab.project.common.family.manager.FamilyConversationManager;
import orangelab.project.common.family.model.CreateConversationResult;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.UpLoadFinishItem;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;

import com.qiniu.android.storage.UploadOptions;
import com.androidtoolkit.PLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;
import orangelab.thirdparty.qiniu.QiNiuManager;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2017/10/30 上午11:35
 * Mystery
 */

public class UnifiedBridgeTaskManager {

    private static final String TAG = "UnifiedBridgeTaskManage";

    private static volatile UnifiedBridgeTaskManager mInstance;
    private Map<String, UnifiedBridgeTask> mBridgeTaskMap;

    public UnifiedBridgeTaskManager() {
        mBridgeTaskMap = new HashMap<>();
    }

    public static UnifiedBridgeTaskManager getInstance() {
        if (mInstance == null) {
            synchronized (UnifiedBridgeTaskManager.class) {
                if (mInstance == null) {
                    mInstance = new UnifiedBridgeTaskManager();
                }
            }
        }
        return mInstance;
    }

    public UnifiedBridgeTask findTask(String action) {
        if (mBridgeTaskMap != null) {
            return mBridgeTaskMap.get(action);
        }
        return null;
    }

    public void addTask(String action, UnifiedBridgeTask unifiedBridgeTask) {
        if (mBridgeTaskMap != null) {
            mBridgeTaskMap.put(action, unifiedBridgeTask);
        }
    }

    public void removeTask(String action) {
        if (mBridgeTaskMap != null) {
            mBridgeTaskMap.remove(action);
        }
    }


    public static void CreateAndExecuteQiNiuUpLoadFileTask(String action, List<String> imagePath, UnifiedBridgeModel.UnifiedBridgeModelOption option, IUnifiedBridge bridge) {
        UnifiedBridgeTask upLoadFileTask = new UnifiedBridgeTask(action, option, bridge) {
            @Override
            protected void onExecuting(IUnifiedBridge bridge) {
                /**
                 * 这里执行上传
                 */
                ApiManager.RefreshUpLoadToken((upLoadTokenResult, e) -> {
                    if (!isExecuting()) {
                        forceEndTask();
                        return;
                    }
                    if (e == null) {
                        final int allCount = imagePath.size();
                        if (TextUtils.isEmpty(upLoadTokenResult.token)) {
                            UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, MessageUtils.getString(R.string.network_is_unavailable));
                            forceEndTask();
                            return;
                        } else {
                            Observable.from(imagePath)
                                    .onBackpressureBuffer()
                                    .observeOn(Schedulers.io())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new Subscriber<String>() {
                                        private int successCount = 0;
                                        private ArrayList<UpLoadFinishItem> successResult = new ArrayList<UpLoadFinishItem>();

                                        @Override
                                        public void onStart() {
                                            request(1);
                                            Log.i(TAG, "onStart: ");
                                        }

                                        @Override
                                        public void onCompleted() {
                                            Log.i(TAG, "onCompleted: ");
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Log.i(TAG, "onError: ");
                                        }

                                        @Override
                                        public void onNext(String s) {
                                            Log.i(TAG, "prepare upload path=" + s);
                                            if (!isExecuting()) {
                                                unsubscribe();
                                                forceEndTask();
                                                return;
                                            }
                                            try {
                                                if (s.contains("file://")) {
                                                    s = s.replace("file://", "");
                                                }
                                                PLog.i(TAG, "upload file " + s);
                                                QiNiuManager.UpLoadImage(s, null, upLoadTokenResult.token, (key, info, response) -> {
                                                            Utils.runSafely(() -> {
                                                                PLog.i(TAG, response.toString());
                                                            });
                                                            if (info.isOK()) {
                                                                String urlResult = response.optString("url");
                                                                String keyResult = response.optString("key");
                                                                PLog.i(TAG, "upload file success url " + urlResult);

                                                                if (TextUtils.isEmpty(keyResult)) {
                                                                    if (!isExecuting()) {
                                                                        unsubscribe();
                                                                        forceEndTask();
                                                                        return;
                                                                    }
                                                                    UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, MessageUtils.getString(R.string
                                                                            .string_family_image_upload_error));
                                                                    unsubscribe();
                                                                    forceEndTask();
                                                                    return;
                                                                }

                                                                if (TextUtils.isEmpty(urlResult)) {
                                                                    if (!isExecuting()) {
                                                                        unsubscribe();
                                                                        forceEndTask();
                                                                        return;
                                                                    }
                                                                    UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, MessageUtils.getString(R.string
                                                                            .string_family_image_upload_error));
                                                                    unsubscribe();
                                                                    forceEndTask();
                                                                    return;
                                                                }
                                                                UpLoadFinishItem item = new UpLoadFinishItem();
                                                                item.key = keyResult;
                                                                item.url = urlResult;
                                                                successResult.add(item);
                                                                successCount++;
                                                                if (successCount == allCount) {
                                                                    if (!isExecuting()) {
                                                                        unsubscribe();
                                                                        forceEndTask();
                                                                        return;
                                                                    }
                                                                    UnifiedBridgeHelper.SendUpLoadSuccessResultToRn(bridge, action, successResult);
                                                                    forceEndTask();
                                                                } else {
                                                                    request(1);
                                                                }
                                                            } else {
                                                                try {
                                                                    ReportEventUtils.reportException(response.toString());
                                                                } catch (Exception e1) {
                                                                    e1.printStackTrace();
                                                                }
                                                                if (!isExecuting()) {
                                                                    unsubscribe();
                                                                    forceEndTask();
                                                                    return;
                                                                }
                                                                UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, MessageUtils.getString(R.string.string_family_image_upload_error));
                                                                unsubscribe();
                                                                forceEndTask();
                                                                return;
                                                            }
                                                        },
                                                        new UploadOptions(null, null, false, (key, percent) -> {

                                                        }, () -> isCancel()));
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                                if (!isExecuting()) {
                                                    unsubscribe();
                                                    forceEndTask();
                                                    return;
                                                }
                                                UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, MessageUtils.getString(R.string.string_family_image_upload_error));
                                                unsubscribe();
                                                forceEndTask();
                                            }
                                        }
                                    });
                        }
                    } else {
                        UnifiedBridgeHelper.SendRequestCommonResultError(bridge, action, -1, e.getMessage());
                        forceEndTask();
                        return;
                    }
                });
            }

            @Override
            protected void onTimeOut(IUnifiedBridge bridge) {
                /**
                 * 这里返回超时结果
                 */
                if (bridge != null) {
                    UnifiedBridgeHelper.SendTaskTimeOut(bridge, action);
                }
            }

            @Override
            protected void onCancel(IUnifiedBridge bridge) {
                /**
                 * 这里处理任务被取消
                 */
                PLog.i(TAG, "onCancel: task has been cancel");
            }
        };
        getInstance().addTask(action, upLoadFileTask);
        upLoadFileTask.execute();
    }

    public static void CreateAndExecuteFamilyConversationTask(String action, UnifiedBridgeModel.UnifiedBridgeModelOption option, IUnifiedBridge bridge) {
        UnifiedBridgeTask createConverstaionTask = new UnifiedBridgeTask(action, option, bridge) {

            private CreateConversationResult createResult = new CreateConversationResult();

            {
                createResult.reason = MessageUtils.getString(R.string.string_create_family_error_1);
            }

            @Override
            protected void onExecuting(IUnifiedBridge bridge) {
                UnifiedBridgeWorker.Work(() -> {
                    FamilyConversationManager.CreateFamilyConversation((code, result) -> UnifiedBridgeWorker.Work(() -> {
                        if (code == 0) {
                            /**
                             * 创建家族成功
                             */
                            createResult.conversationId = result;
                        } else {
                            /**
                             * 创建家族失败
                             */
                            createResult.reason = result;
                        }
                        if (!isExecuting()) {
                            forceEndTask();
                            return;
                        }
                        sendResponse(bridge);
                        forceEndTask();
                    }));
                });
            }

            @Override
            protected void onTimeOut(IUnifiedBridge bridge) {
                sendResponse(bridge);
            }

            @Override
            protected void onCancel(IUnifiedBridge bridge) {

            }

            private void sendResponse(IUnifiedBridge bridge) {
                try {
                    JSONObject returnJson = new JSONObject(GsonHelper.getGson().toJson(createResult));
                    bridge.SendCommendIntoRN(action, returnJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        getInstance().addTask(action, createConverstaionTask);
        createConverstaionTask.execute();
    }


}
