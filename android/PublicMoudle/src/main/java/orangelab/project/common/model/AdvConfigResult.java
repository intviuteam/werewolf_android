package orangelab.project.common.model;

public class AdvConfigResult {

    public boolean show;

    @Override
    public String toString() {
        return "AdvConfigResult{" +
                ", show='" + show +
                '}';
    }
}
