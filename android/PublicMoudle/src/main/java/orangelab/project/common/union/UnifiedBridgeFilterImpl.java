package orangelab.project.common.union;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.R;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.URLManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import cn.intviu.support.GsonHelper;
import orangelab.project.ProjectConfig;
import orangelab.project.common.activity.BlackListActivity;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.WebToNativeHideEvent;
import orangelab.project.common.family.FamilyInfoActivity;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.family.model.FamilyInfo;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.model.UpLoadFileModel;
import orangelab.project.common.share.RankShareActivity;
import orangelab.project.common.share.RankSharePayload;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.utils.ImageUtils;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.web.WebViewActivity;
import orangelab.project.game.dialog.WereWolfIntviuFriendDialog;

import com.androidtoolkit.FileToolKit;

import orangelab.project.voice.model.VoiceDeedShareBean;

import com.androidtoolkit.HttpToolkit;

import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * game_werewolf
 * 2017/9/21 下午4:02
 * Mystery
 */

public class UnifiedBridgeFilterImpl implements UnifiedBridgeFilter {

    private static final String TAG = "UnifiedBridgeFilterImpl";

    private Activity bindActivity;

    public UnifiedBridgeFilterImpl(Activity bindActivity) {
        this.bindActivity = bindActivity;
    }

    @Override
    public boolean filterAction(String action, JSONObject params, UnifiedBridgeModel.UnifiedBridgeModelOption options, IUnifiedBridge bridge) {
        PLog.e(TAG, "action = " + action);
        switch (action) {
            case UnifiedConstant.ACTION_ENTERAUDIOROOM:
                try {
                    String roomId = params.optString("roomId");
                    String roomPassword = params.optString("roomPassword");
                    EnterRoomPayload payload = new EnterRoomPayload();
                    payload.password = roomPassword;
                    payload.gameType = Constant.GAME_TYPE_AUDIO;
                    payload.roomId = roomId;
                    payload.userName = GlobalUserState.getGlobalState().getUserName();
                    payload.userId = GlobalUserState.getGlobalState().getUserId();
                    payload.userSex = GlobalUserState.getGlobalState().getUserSex();
                    payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
                    payload.token = GlobalUserState.getGlobalState().getToken();
                    LaunchActivity.LaunchForRoom(bindActivity, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case UnifiedConstant.ACTION_BLACK_LIST:
                try {
                    BlackListActivity.Start(bindActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case UnifiedConstant.ACTION_STANDINGS:
                try {
                    WebViewActivity.load(bindActivity,
                            URLManager.GetDefaultURL() + URLManager.GetStandingsURL(GlobalUserState.getGlobalState().getToken()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case UnifiedConstant.ACTION_ENTERROOM:
                try {
                    String roomId = params.optString(UnifiedConstant.ROOM_ID_LOWER);
                    String roomPassword = params.optString(UnifiedConstant.ROOM_PASSWORD_LOWER);
                    String roomType = params.optString(UnifiedConstant.ROOM_TYPE_LOWER);
                    EnterRoomPayload payload = new EnterRoomPayload();
                    payload.password = roomPassword;
                    payload.gameType = roomType;
                    payload.roomId = roomId;
                    payload.userName = GlobalUserState.getGlobalState().getUserName();
                    payload.userId = GlobalUserState.getGlobalState().getUserId();
                    payload.userSex = GlobalUserState.getGlobalState().getUserSex();
                    payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
                    payload.token = GlobalUserState.getGlobalState().getToken();
                    LaunchActivity.LaunchForRoom(bindActivity, IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case UnifiedConstant.ACTION_CREATE_FAMILY_CONVERSATION:
                UnifiedBridgeTaskManager.CreateAndExecuteFamilyConversationTask(action, options, bridge);
                return true;
            case UnifiedConstant.ACTION_SET_AUDIO_TOPIC_TYPE:
                return true;
            case UnifiedConstant.ACTION_UP_LOAD_USER_HEAD:
            case UnifiedConstant.ACTION_UP_LOAD_FAMILY_HEAD:
            case UnifiedConstant.ACTION_UP_LOAD_PHOTOS: {
                try {
                    UpLoadFileModel model = GsonHelper.getGson().fromJson(params.toString(), UpLoadFileModel.class);
                    if (model.cancel) {
                        UnifiedBridgeTask task = UnifiedBridgeTaskManager.getInstance().findTask(action);
                        if (task != null) {
                            task.cancel();
                        }
                        return true;
                    }
                    UnifiedBridgeTask task = UnifiedBridgeTaskManager.getInstance().findTask(action);
                    if (task == null) {
                        UnifiedBridgeTaskManager.CreateAndExecuteQiNiuUpLoadFileTask(action, model.path, options, bridge);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_ENTER_FAMILY_CONVERSATION: {
                try {
                    String conversationId = params.optString(UnifiedConstant.CONVERSATION_ID);
                    if (!TextUtils.isEmpty(conversationId)) {
                        LCIMConversationActivity.Launch(bindActivity, conversationId, "", null, null, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_FAMILY_INFO_INIT: {
                try {
                    JSONObject groupJson = params.optJSONObject("group");
                    if (groupJson == null) {
                        FamilyMemo.ClearFamilyMemo();
                        return true;
                    }
                    FamilyInfo familyInfo = GsonHelper.getGson().fromJson(groupJson.toString(), FamilyInfo.class);

                    if (familyInfo != null) {
                        FamilyMemo.UpdateMemo(familyInfo);
                    } else {
                        /**
                         * 数据出错也认为没有家族
                         */
                        FamilyMemo.ClearFamilyMemo();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
            case UnifiedConstant.ACTION_SHOW_FAMILY_INFO: {
                try {
                    String group_id = params.optString("group_id");
                    FamilyInfoActivity.Launch(bindActivity, group_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_LOGIN_OUT: {
                RxToolKit.Emit(new ActivityEvent.ReactNativeLogOutEvent());
                return true;
            }

            case UnifiedConstant.SHARE_HOUSE_DEEDS:
                try {
                    //房契分享回调
                    Utils.runSafely(() -> {
                        String name = params.optString("name");
                        int uid = params.optInt("uid");
                        long applyTime = params.optLong("apply_time");
                        String roomId = params.optString("room_id");
                        String image = params.optString("image");
                        VoiceDeedShareBean deedShareBean = new VoiceDeedShareBean("", name, uid, image);
                        deedShareBean.roomId = roomId;
                        deedShareBean.time = applyTime;

                        if (bindActivity != null) {
                            SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onDeedShare(bindActivity, deedShareBean);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            case UnifiedConstant.ACTION_OPEN_SHARE_MODAL: {
                try {
                    String url = params.optString("url");
                    String title = params.optString("title");
                    String message = params.optString("message");
                    String shareType = params.optString("shareType");

                    ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_FROM_JS);
                    data.title = title;
                    data.url = url;
                    data.message = message;
                    data.shareType = shareType;

                    if (bindActivity != null) {
                        SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onJsCallShare(bindActivity, data);
                    }

                } catch (Exception e) {
                    PLog.e(TAG, "error:" + e.getMessage());
                    e.printStackTrace();
                }
                return true;
            }
            case UnifiedConstant.ACTION_FRUIT_SHOW: {
                try {
                    RankSharePayload payload = GsonHelper.getGson().fromJson(params.toString(), RankSharePayload.class);
                    RankShareActivity.Launch(bindActivity, payload);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
            case UnifiedConstant.ACTION_INVITE_FRIENDS_MINI_GAME: {
                try {
                    WereWolfIntviuFriendDialog dialog = new WereWolfIntviuFriendDialog(UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST_MINI_GAME, bindActivity);
                    dialog.setNeedBackAndDestroy(false);
                    dialog.showDialog(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_SHOW_NATIVE_TOAST: {
                String msg = params.optString("message");
                if (!TextUtils.isEmpty(msg)) {
                    ToastToolKit.showLong(msg);
                }
            }
            return true;
            case UnifiedConstant.ACTION_SHOW_USER_INFO: {
                try {
                    String userId = params.optString("user_id");
                    boolean isFromWeb = params.optBoolean("isFromWeb");
                    if (!TextUtils.isEmpty(userId)) {
                        PersonalInfoActivity.Launch(bindActivity, userId, isFromWeb);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }

            case UnifiedConstant.ACTION_SHARE_H5: {
                PLog.e(TAG, "ACTION_SHARE_H5");
                try {
                    ReportEventUtils.reportShareChangeFace("dress");
                    Utils.runSafely(
                            () -> {
                                ShareBridgeData data = new ShareBridgeData(ShareBridgeData.SHARE_ONLY_IMAGE);
                                data.url = params.optString("img");
                                PLog.e(TAG, "url = " + data.url);
                                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF).onImageShare(bindActivity, data);
                            }
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return true;
            case UnifiedConstant.ACTION_SAVE_IMAGE: {
                try {
                    ReportEventUtils.reportSaveChangeFace("dress");
                    //保存图片到本地
                    String url = params.optString("img");
                    Observable.just(url).map(s -> {
                        byte[] data = FileToolKit.loadFile(url);
                        if (data != null) {
                            //说明本地已经存在
                            return new byte[0];
                        } else {
                            return HttpToolkit.doGet(url);
                        }
                    }).subscribeOn(Schedulers.newThread())
                            .subscribe(bytes -> {
                                if (bytes != null) {
                                    if (bytes.length > 0) {

                                        //在这里进行重新绘制
                                        Bitmap container = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                        Bitmap bitmap = ImageUtils.decorateBitmapByWatermark(container, ProjectConfig.WATER_MARK, ProjectConfig.QR_CODE_SHOW);
                                        byte[] data = ImageUtils.bitmapToByteArray(bitmap);
                                        FileToolKit.saveFile(url, data);
                                        FileToolKit.saveFileToJpg(url, data);
                                    }

                                    UIActuator.postSafely(() -> ToastToolKit.showShort(R.string.string_save_success));
                                } else {
                                    UIActuator.postSafely(() -> ToastToolKit.showShort(R.string.string_save_success));
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_REPORT: {
                try {
                    String type = params.optString("key");
                    ReportEventUtils.reportType(type);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
            case UnifiedConstant.ACTION_HIDDEN_LOADING: {
                EventBus.getDefault().post(new WebToNativeHideEvent());
            }
            return true;
            default:
                return false;
        }
    }

    @Override
    public void destroy() {
        bindActivity = null;
    }
}
