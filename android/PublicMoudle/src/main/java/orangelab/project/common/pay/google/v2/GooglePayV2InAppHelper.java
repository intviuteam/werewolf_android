package orangelab.project.common.pay.google.v2;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.R;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.datasource.GlobalUserState;
import com.google.gson.JsonSyntaxException;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import orangelab.project.MainApplication;
import orangelab.project.common.db.DBConstant;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;
import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.model.PurchaseResult;
import orangelab.project.common.pay.PayHelper;
import orangelab.project.common.pay.PayHelperListener;
import orangelab.project.common.pay.google.GooglePayUtils;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2018/6/19 上午10:59
 * Mystery
 */
public class GooglePayV2InAppHelper extends PayHelper implements BillingManager.BillingManagerObserver {

    private static final String TAG = "GooglePayV2";

    private BillingManager mBillingManager;
    private String mProductType;
    private UserPayOrderDaoV2Helper mDaoV2Helper;
    private SafeHandler mSafeHandler = new SafeHandler();

    public GooglePayV2InAppHelper(Activity mActivity, String productId, PayHelperListener listener) {
        super(productId, listener);
        mProductType = BillingClient.SkuType.INAPP;
        mBillingManager = new BillingManager(mActivity, this);
        mDaoV2Helper = MainApplication.getInstance().getUserPayOrderDaoV2Helper();
    }

    @Override
    public void repay() {
        if (mPayListener != null) {
            mPayListener.onStart();
        }
        mBillingManager.queryPurchasesInApp();
    }

    @Override
    public void pay() {
        if (mPayListener != null) {
            mPayListener.onStart();
        }
        mBillingManager.queryPurchasesInApp();
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return false;
    }

    @Override
    public void release() {
        mBillingManager.destroy();
        mSafeHandler.release();
        mDaoV2Helper = null;
    }

    @Override
    public void onSetupFinished() {
        /**
         * 初始化成功,开始处理支付流程支付流程如下
         *
         * 用户想买的 productId
         *
         * 1。向Google查询用户已经有的库存
         * 2。在已有库存中是否有productId，如果含有则走3，否则走5
         * 3。如果含有productId，则先向服务器校验此订单，如果校验成功则进行消费操作
         * 4。消费已有的productId
         * 5。执行购买操作
         * 6。向服务器验证购买后的结果
         * 7。消费已经购买的productId
         *
         */

    }

    @Override
    public void onSetupError(int errorCode) {
        /**
         * 初始化失败
         */
        googleError(errorCode);
    }

    @Override
    public void onPurchasesQuery(List<Purchase> purchases) {
        /**
         * 查询结果
         */
        Log.i(TAG, "onPurchasesQuery: " + purchases);

        final Runnable purchasesFlowRunnable = new Runnable() {
            @Override
            public void run() {
                mBillingManager.initiatePurchaseFlow(mType, mProductType);
            }
        };

        /**
         * 这里要判断之前有没有同样的产品
         */
        for (Purchase purchase : purchases) {
            if (TextUtils.equals(purchase.getSku(), mType)) {
                /**
                 * 还有一个产品需要消费,先去数据查看是否需要消费
                 */
                Log.i(TAG, "onPurchasesQuery: Find a old Sku,report And Consume it first");
                if (mDaoV2Helper != null) {
                    UserPayOrderEntity entity = mDaoV2Helper.getUserPayOrderByOrderId(purchase.getOrderId());
                    if (entity == null) {
                        Log.i(TAG, "onPurchasesQuery: We don't find this order in database");
                        /**
                         * 数据库没有这条记录，进行当前数据汇报
                         */
                        reportToServerWithOutRetry(
                                GlobalUserState.getGlobalState().getUserId(),
                                GlobalUserState.getGlobalState().getToken(),
                                purchase.getOrderId(),
                                purchase.getSignature(),
                                purchase.getOriginalJson(),
                                () -> {
                                    mBillingManager.consumeAsync(purchase.getPurchaseToken(), (responseCode, purchaseToken) -> {
                                        if (responseCode == BillingClient.BillingResponse.OK) {
                                            Log.i(TAG, "onPurchasesQuery: consumeAsync ok");
                                            mBillingManager.executeServiceRequest(purchasesFlowRunnable);
                                        } else {
                                            Log.i(TAG, "onPurchasesQuery: consumeAsync error");
                                            googleError(BillingClient.BillingResponse.ERROR);
                                        }
                                    });
                                }
                        );
                    } else {
                        Log.i(TAG, "onPurchasesQuery: We  find this order in database");

                        /**
                         * 数据库有这条数据，判断Token与UserId
                         */
                        String userId = entity.getUserId();
                        String token = entity.getOtherData2();

                        if (TextUtils.isEmpty(userId) || TextUtils.isEmpty(token) || Utils.checkUserIdIsTourist(userId)) {
                            /**
                             * 发现数据出问题了
                             */
                            serverError(GooglePayUtils.SERVER_PAY_FAIL, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_FAIL));
                            return;
                        }

                        reportToServerWithOutRetry(
                                userId,
                                token,
                                purchase.getOrderId(),
                                purchase.getSignature(),
                                purchase.getOriginalJson(),
                                () -> {
                                    mBillingManager.consumeAsync(purchase.getPurchaseToken(), (responseCode, purchaseToken) -> {
                                        if (responseCode == BillingClient.BillingResponse.OK) {
                                            Log.i(TAG, "onPurchasesQuery: consumeAsync ok");
                                            mBillingManager.executeServiceRequest(purchasesFlowRunnable);
                                        } else {
                                            Log.i(TAG, "onPurchasesQuery: consumeAsync error");
                                            googleError(BillingClient.BillingResponse.ERROR);
                                        }
                                    });
                                }
                        );
                    }
                    return;
                }
            }
        }
        Log.i(TAG, "onPurchasesQuery: begin initiatePurchaseFlow");
        mBillingManager.executeServiceRequest(purchasesFlowRunnable);
    }

    @Override
    public void onPurchasesQueryError(int errorCode) {
        /**
         * 查询库存失败
         */
        googleError(errorCode);
    }

    @Override
    public void onPurchaseFlowError(int errorCode) {
        /**
         * 购买失败了
         */
        googleError(errorCode);
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        Log.i(TAG, "onPurchasesUpdated: " + purchases);
        if (purchases == null) {
            googleError(BillingClient.BillingResponse.ERROR);
            return;
        }

        /**
         * 拿到刚刚支付的结果
         */
        for (Purchase purchase : purchases) {
            if (TextUtils.equals(purchase.getSku(), mType)) {
                Runnable consumeRunnable = new Runnable() {
                    @Override
                    public void run() {
                        mBillingManager.consumeAsync(purchase.getPurchaseToken());
                    }
                };

                /**
                 * 报告支付成功
                 */
                ReportRobot.robot.report("GOOGLE_PAY_SUCCESS");

                /**
                 * 向数据库插入一条数据，作为订单的一个记录
                 */
                UserPayOrderEntity entity = new UserPayOrderEntity();
                entity.setUserId(GlobalUserState.getGlobalState().getUserId());
                entity.setOrderState(DBConstant.PAY_SUCCESS_BUT_NOT_REPORT_SERVER_INAPP);
                entity.setProductId(purchase.getSku());
                entity.setPackageName(purchase.getPackageName());
                entity.setOrderId(purchase.getOrderId());
                entity.setPurchaseTime(purchase.getPurchaseTime());
                entity.setPurchaseToken(purchase.getPurchaseToken());
                entity.setPurchaseData(purchase.getOriginalJson());
                entity.setSignature(purchase.getSignature());
                entity.setOtherData1("0");
                entity.setOtherData2(GlobalUserState.getGlobalState().getToken());
                try {
                    if (mDaoV2Helper != null) {
                        mDaoV2Helper.insertData(entity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.runSafely(() -> {
                        ReportRobot.robot.report("INSERT_PAY_ORDER_FAILED");
                    });
                }
                /**
                 * 向服务器汇报
                 */
                reportToServerWithRetry(
                        entity.getUserId(),
                        entity.getOtherData2(),
                        purchase.getOrderId(),
                        purchase.getSignature(),
                        purchase.getOriginalJson(),
                        () -> {
                            mBillingManager.executeServiceRequest(consumeRunnable);
                        }
                );

            }
        }
    }

    @Override
    public void onPurchasesUpdatedError(int errorCode) {
        googleError(errorCode);
    }

    @Override
    public void onConsumeSuccess(String token) {
        /**
         * 消费成功
         */
        Log.i(TAG, "onConsumeSuccess: " + token);
        if (mPayListener != null) {
            mPayListener.onSuccess();
        }
    }

    @Override
    public void onConsumeError(int errorCode) {
        /**
         * 消费失败
         */
        googleError(errorCode);
    }

    private void reportToServerWithRetry(String userId, String token, String orderId, String signature, String data, Runnable success) {
        reportToServer(userId, token, orderId, signature, data, success, 5);
    }

    private void reportToServerWithOutRetry(String userId, String token, String orderId, String signature, String data, Runnable success) {
        reportToServer(userId, token, orderId, signature, data, success, -1);
    }

    private void reportToServer(String userId, String token, String orderId, String signature, String data, Runnable success, final int retryCount) {
        Log.i(TAG, "reportToServer: " + userId + "," + orderId + "," + token);
        JSONObject json = new JSONObject();
        try {
            json.put(Constant.SIGNATURE, signature);
            json.put(Constant.DATA, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestTask.create().isGet(false).withToken(token).request(Constant.GOOGLE_PAY).load(json)
                .action(new RequestTaskCallBack() {

                    private boolean needRetry() {
                        return retryCount > 0;
                    }

                    private void retry(int code, String msg) {
                        if (needRetry()) {
                            final int tempRetryCount = retryCount - 1;
                            reportToServer(userId, token, orderId, signature, data, success, tempRetryCount);
                        } else {
                            serverError(code, msg);
                        }
                    }

                    @Override
                    public void onSuccess(String response) {
                        mSafeHandler.postSafely(() -> {
                            try {
                                PurchaseResult result = GsonHelper.getGson().fromJson(response, PurchaseResult.class);
                                if (result.purchase.size() == 0) {
                                    ReportRobot.robot.report("SERVER_RETURN_DATA_ERROR_SIZE_0");
                                    retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                                } else {
                                    PurchaseResult.PurchaseResultItem item = result.purchase.get(0);
                                    switch (item.getOp_result()) {
                                        case GooglePayUtils.SERVER_PAY_SUCCESS: {
                                            Utils.runSafely(() -> {
                                                if (mDaoV2Helper != null) {
                                                    mDaoV2Helper.deleteData(userId, orderId);
                                                }
                                            });
                                            ReportRobot.robot.report("GOOGLE_PAY_SERVER_SUCCESS");
                                            Log.i(TAG, "reportToServer onSuccess:");
                                            if (success != null) {
                                                success.run();
                                            }

                                        }
                                        break;
                                        case GooglePayUtils.SERVER_PAY_DOING://处理中
                                        case GooglePayUtils.SERVER_PAY_UPDATE_FAIL://更新失败
                                        case GooglePayUtils.SERVER_PAY_SERVER_ERROR://服务器处理错误
                                        case GooglePayUtils.SERVER_PAY_FAIL:// 支付失败
                                        case GooglePayUtils.SERVER_PAY_UNKNOWN_PRODUCT: //未知产品
                                        case GooglePayUtils.SERVER_PAY_VERIFY_FAIL: //校验失败
                                            Log.i(TAG, "reportToServer onError:");
                                            retry(item.getOp_result(), GooglePayUtils.getServerHint(item.getOp_result()));
                                            break;
                                        default:
                                            Log.i(TAG, "reportToServer onError:");
                                            retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                                            break;
                                    }
                                }
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                                retry(GooglePayUtils.SERVER_PAY_SERVER_ERROR, GooglePayUtils.getServerHint(GooglePayUtils.SERVER_PAY_SERVER_ERROR));
                            }
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(code, failed);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        mSafeHandler.postSafely(() -> {
                            Log.i(TAG, "reportToServer onError:");
                            retry(-1, MessageUtils.getString(R.string.network_is_unavailable));
                        });
                    }
                }).build().execute();
    }
}
