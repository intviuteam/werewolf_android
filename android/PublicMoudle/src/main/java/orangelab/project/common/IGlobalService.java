package orangelab.project.common;

/**
 * game_werewolf
 * 2018/1/30 下午2:15
 * Mystery
 */

public interface IGlobalService {

    void sendMessage(String type, String payload);
}
