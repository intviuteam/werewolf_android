package orangelab.project.common.union;

/**
 * game_werewolf
 * 2017/9/19 下午4:12
 * Mystery
 */

public class UnifiedConstant {

    public static final String RN_NATIVE = "RN_Native";
    public static final String JS_NATIVE = "JS_Native";

    public static final String ACTION_GAME_RECORD_USER_INFO = "game_record_user_info";
    public static final String ACTION_REQUEST_FRIENDS_LIST = "ACTION_REQUEST_FRIENDS_LIST";
    public static final String ACTION_REQUEST_FRIENDS_LIST_MINI_GAME = "ACTION_REQUEST_FRIENDS_LIST_MINI_GAME";
    public static final String ACTION_INVITE_FRIENDS_MINI_GAME = "ACTION_INVITE_FRIENDS_MINI_GAME";
    public static final String ACTION_DELETE_FRIEND = "ACTION_DELETE_FRIEND";
    public static final String ACTION_ADD_FRIEND = "ACTION_ADD_FRIEND";
    public static final String ACTION_CREATE_FAMILY_CONVERSATION = "ACTION_CREATE_FAMILY_CONVERSATION";
    public static final String ACTION_LEANCLOUD_SERVER_CONFIG = "ACTION_LEANCLOUD_SERVER_CONFIG";
    public static final String EVENT_REACT_NATIVE_BRIDGE = "ReactNativeBridge";
    public static final String ACTION_ENTERAUDIOROOM = "enterAudioRoom";
    public static final String ACTION_ENTERROOM = "enterRoom";
    public static final String ACTION_SHOW_NATIVE_TOAST = "ACTION_SHOW_NATIVE_TOAST";

    //通知rn分享成功
    public static final String EVENT_SHARE_MINI_GAME_SUCCESS = "EVENT_SHARE_MINI_GAME_SUCCESS";


    public static final String ACTION_LEAVE_AUDIO_ROOM = "LEAVE_AUDIO_ROOM";//语音房离开通知RN的action
    public static final String PARAMS_DATA = "Data";
    public static final String PARAMS_DATA_LOWER = "data";

    public static final String ACTION_BLACK_LIST = "black_list";
    public static final String ACTION_BLACK_LIST_ADD = "black_list_add";
    public static final String ACTION_BLACK_LIST_REMOVE = "black_list_remove";

    public static final String ACTION_STANDINGS = "standings";

    public static final String ROOM_ID_LOWER = "room_id";
    public static final String ROOM_PASSWORD_LOWER = "room_password";
    public static final String ROOM_TYPE_LOWER = "room_type";

    public static final String ACTION_SET_AUDIO_TOPIC_TYPE = "setAudioChildType";

    public static final String ACTION_UP_LOAD_USER_HEAD = "ACTION_UP_LOAD_USER_HEAD";
    public static final String ACTION_UP_LOAD_FAMILY_HEAD = "ACTION_UP_LOAD_FAMILY_HEAD";
    public static final String ACTION_UP_LOAD_PHOTOS = "ACTION_UP_LOAD_PHOTOS";
    public static final String ACTION_QUIT_FAMILY = "ACTION_QUIT_FAMILY";
    public static final String ACTION_ENTER_FAMILY_CONVERSATION = "ACTION_ENTER_FAMILY_CONVERSATION";
    public static final String ACTION_FAMILY_INFO_INIT = "ACTION_FAMILY_INFO_INIT";
    public static final String ACTION_SHOW_FAMILY_INFO = "ACTION_SHOW_FAMILY_INFO";
    public static final String ACTION_OPEN_SHARE_MODAL = "openShareModal";
    public static final String PLATFORM = "platform";
    public static final String CANCEL = "CANCEL";

    public static final String CODE = "code";
    public static final String MESSAGE = "message";
    public static final String URL = "url";
    public static final String PATH = "path";
    public static final String CONVERSATION_ID = "conversationId";

    public static final String ACTION_LOGIN_OUT = "ACTION_LOGIN_OUT";

    public static final String SHARE_HOUSE_DEEDS = "SHARE_HOUSE_DEEDS";//房契分享与RN交互的action
    public static final String ACTION_FRUIT_SHOW = "FRUIT_SHOW";

    public static final String ACTION_MINI_GAME_OVER = "ACTION_MINI_GAME_OVER";
    public static final String ACTION_REACT_NATIVE_ON_TOP = "ACTION_REACT_NATIVE_ON_TOP"; //用于通知RN RN当前页面在顶端
    public static final String ACTION_REACT_NATIVE_LEAVE_TOP = "ACTION_REACT_NATIVE_LEAVE_TOP";//用于通知RN页面当前不在顶端

    public static final String ACTION_RECEIVE_MSG_FROM_GLOBAL_SOCKET = "ACTION_RECEIVE_MSG_FROM_GLOBAL_SOCKET";

    public static final String ACTION_SHOW_USER_INFO = "ACTION_SHOW_USER_INFO";
    public static final String ACTION_SHARE_H5 = "ACTION_SHARE_H5";
    public static final String ACTION_SAVE_IMAGE = "ACTION_SAVE_IMAGE";
    public static final String ACTION_HIDDEN_LOADING = "ACTION_HIDDEN_LOADING";


    public static final String ACTION_REPORT = "ACTION_REPORT";

    public static final String SHOW_YOURSELF = "SHOW_YOURSELF";
    public static final String SHOW_YOURSELF_DONE = "SHOW_YOURSELF_DONE";

}
