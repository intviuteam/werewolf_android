package orangelab.project.common.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2018/5/31 下午2:28
 * Mystery
 */
@Entity
public class MiniGameLaunchRecordEntity {

    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    private String userId; //用户Id

    private String game_type;//游戏类型

    private String game_launch_json;//游戏Launch报文

    @Generated(hash = 1085536265)
    public MiniGameLaunchRecordEntity(Long id, String userId, String game_type,
                                      String game_launch_json) {
        this.id = id;
        this.userId = userId;
        this.game_type = game_type;
        this.game_launch_json = game_launch_json;
    }

    @Generated(hash = 335131617)
    public MiniGameLaunchRecordEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGame_type() {
        return this.game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public String getGame_launch_json() {
        return this.game_launch_json;
    }

    public void setGame_launch_json(String game_launch_json) {
        this.game_launch_json = game_launch_json;
    }


}
