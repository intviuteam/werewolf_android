package orangelab.project.common.db.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.androidtoolkit.FileToolKit;
import com.toolkit.action.Keepable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import orangelab.project.voice.musiccompany.concrete.MusicState;

import org.greenrobot.greendao.annotation.Generated;

/**
 * 音乐伴奏实体类，GreenDao数据库使用
 */
@Entity
public class MusicDataEntity implements Keepable, Parcelable {

    @Id(autoincrement = true)
    private Long _mid;

    private String id = "";
    private long filesize;
    private String img = "";
    private String play_url = "";
    private String lyric_url = "";
    private String song_name = "";
    private String timelength = "";
    private String author_name = "";
    private int bitrate = 128;
    private String music_type = "";
    private String lyrics = "";
    private String localPath = "";
    private int progress = 0;
    private String speed = "";
    private int state = MusicState.Companion.getSTATE_DOWN_NON();

    @Generated(hash = 1908269043)
    public MusicDataEntity(Long _mid, String id, long filesize, String img, String play_url,
                           String lyric_url, String song_name, String timelength, String author_name, int bitrate,
                           String music_type, String lyrics, String localPath, int progress, String speed, int state) {
        this._mid = _mid;
        this.id = id;
        this.filesize = filesize;
        this.img = img;
        this.play_url = play_url;
        this.lyric_url = lyric_url;
        this.song_name = song_name;
        this.timelength = timelength;
        this.author_name = author_name;
        this.bitrate = bitrate;
        this.music_type = music_type;
        this.lyrics = lyrics;
        this.localPath = localPath;
        this.progress = progress;
        this.speed = speed;
        this.state = state;
    }

    @Generated(hash = 514614179)
    public MusicDataEntity() {
    }

    protected MusicDataEntity(Parcel in) {
        id = in.readString();
        filesize = in.readLong();
        img = in.readString();
        play_url = in.readString();
        lyric_url = in.readString();
        song_name = in.readString();
        timelength = in.readString();
        author_name = in.readString();
        bitrate = in.readInt();
        music_type = in.readString();
        lyrics = in.readString();
        localPath = in.readString();
        progress = in.readInt();
        speed = in.readString();
        state = in.readInt();
    }

    public static final Creator<MusicDataEntity> CREATOR = new Creator<MusicDataEntity>() {
        @Override
        public MusicDataEntity createFromParcel(Parcel in) {
            return new MusicDataEntity(in);
        }

        @Override
        public MusicDataEntity[] newArray(int size) {
            return new MusicDataEntity[size];
        }
    };

    /**
     * 获取大小
     */
    public String sizeDes() {
        return FileToolKit.FormatFileSize(filesize);
    }

    /**
     * 判断本地是否存在
     */
    public boolean existLocal() {
        byte[] file = FileToolKit.loadFile(play_url);
        return file != null && file.length > 0;
    }

    /**
     * 刷新数据
     */
    public MusicDataEntity refresh(MusicDataEntity entity) {

        this.speed = entity.speed;
        this.progress = entity.progress;
        this.state = entity.state;
        this.localPath = entity.localPath;

        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeLong(filesize);
        dest.writeString(img);
        dest.writeString(play_url);
        dest.writeString(lyric_url);
        dest.writeString(song_name);
        dest.writeString(timelength);
        dest.writeString(author_name);
        dest.writeInt(bitrate);
        dest.writeString(music_type);
        dest.writeString(lyrics);
        dest.writeString(localPath);
        dest.writeInt(progress);
        dest.writeString(speed);
        dest.writeInt(state);
    }

    @Override
    public String toString() {
        return "MusicDataEntity{" +
                "_mid=" + _mid +
                ", id='" + id + '\'' +
                ", filesize=" + filesize +
                ", img='" + img + '\'' +
                ", play_url='" + play_url + '\'' +
                ", lyric_url='" + lyric_url + '\'' +
                ", song_name='" + song_name + '\'' +
                '}';
    }

    public Long get_mid() {
        return this._mid;
    }

    public void set_mid(Long _mid) {
        this._mid = _mid;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(long filesize) {
        this.filesize = filesize;
    }

    public String getImg() {
        return this.img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPlay_url() {
        return this.play_url;
    }

    public void setPlay_url(String play_url) {
        this.play_url = play_url;
    }

    public String getLyric_url() {
        return this.lyric_url;
    }

    public void setLyric_url(String lyric_url) {
        this.lyric_url = lyric_url;
    }

    public String getSong_name() {
        return this.song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getTimelength() {
        return this.timelength;
    }

    public void setTimelength(String timelength) {
        this.timelength = timelength;
    }

    public String getAuthor_name() {
        return this.author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public int getBitrate() {
        return this.bitrate;
    }

    public void setBitrate(int bitrate) {
        this.bitrate = bitrate;
    }

    public String getMusic_type() {
        return this.music_type;
    }

    public void setMusic_type(String music_type) {
        this.music_type = music_type;
    }

    public String getLyrics() {
        return this.lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getLocalPath() {
        return this.localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public int getProgress() {
        return this.progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getSpeed() {
        return this.speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

}
