package orangelab.project.common.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androidtoolkit.RxToolKit;
import com.R;
import com.networktoolkit.transport.Constant;
import orangelab.project.common.event.DebugEvent;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.event.GameEvent;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Author:1727
 * 2017/3/30
 */

public class DebugDialog extends BaseGameSelectDialog {

    private ListView listView = null;
    private EditText debugSearch = null;
    private DebugListViewItemAdapter adapter;

    public DebugDialog(Context context) {
        super(context);
        setTitle("控制台");
        setButtonType(ONE_BUTTON);
        View view = View.inflate(context, R.layout.layout_debugdialog, null);
        listView = (ListView) view.findViewById(R.id.debug_list);
        debugSearch = (EditText) view.findViewById(R.id.debug_search);
        adapter = new DebugListViewItemAdapter(context);
        listView.setAdapter(adapter);
        setCustomView(view);
        setOnlyButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        FrameLayout.LayoutParams viewlayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewlayoutParams.width = ScreenUtils.dip2px(250);
        viewlayoutParams.height = ScreenUtils.dip2px(350);
        view.setLayoutParams(viewlayoutParams);
        debugSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.search(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        initListener();
    }

    public void destroy() {
        if (adapter != null) {
            adapter.destroy();
        }
        RxToolKit.UnRegister(this);
    }

    private void initListener() {
        RxToolKit.Build(this, DebugEvent.DeveloperShowDebugEvent.class).action(value -> {
            showDialog(0);
        }).register();
    }

    public static class DebugListViewItemAdapter extends BaseAdapter {
        private List<String> curMessages;
        private List<String> allMessage;
        private List<String> searchMessage;
        private Context context;

        public DebugListViewItemAdapter(Context context) {
            curMessages = null;
            searchMessage = new ArrayList<>();
            allMessage = new ArrayList<>();
            setCurMessageList(allMessage);
            this.context = context;
            initListener();
        }

        public void search(String input) {
            searchMessage.clear();
            if (TextUtils.isEmpty(input)) {
                setCurMessageList(allMessage);
            } else {
                for (String temp : allMessage) {
                    if (temp.contains(input)) {
                        searchMessage.add(temp);
                    }
                }
                setCurMessageList(searchMessage);
            }
        }

        public void setCurMessageList(List<String> serverMessage) {
            this.curMessages = serverMessage;
            notifyDataSetChanged();
        }

        private void clearCurMessageList() {
            curMessages.clear();
            notifyDataSetChanged();
        }

        private void addMessage(String msg) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Utils.getCurTime() + "\n");
            stringBuilder.append(msg);
            allMessage.add(stringBuilder.toString());
            notifyDataSetChanged();
        }

        private void initListener() {
            RxToolKit.Build(this, ServerMessageEvent.class).action(serverMessageEvent -> {
                if (serverMessageEvent != null) {
                    if (TextUtils.equals(serverMessageEvent.type, Constant.START)) {
                        clearCurMessageList();
                    }
                    addMessage(serverMessageEvent.toString());
                }
            }).register();
            RxToolKit.Build(this, ServerResponseEvent.class).action(serverMessageEvent -> {
                if (serverMessageEvent != null) {
                    addMessage(serverMessageEvent.toString());
                }
            }).register();
            RxToolKit.Build(this, GameEvent.SendMessageEvent.class).action(serverMessageEvent -> {
                if (serverMessageEvent != null) {
                    addMessage(serverMessageEvent.getMessage());
                }
            }).register();
        }

        public void destroy() {
            RxToolKit.UnRegister(this);
            searchMessage.clear();
            allMessage.clear();
        }

        @Override
        public int getCount() {
            return curMessages.size();
        }

        @Override
        public String getItem(int position) {
            return curMessages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.layout_debugdialog_item, null);
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView.findViewById(R.id.debug_item);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.textView.setText(getItem(position));
            return convertView;
        }

        private static class ViewHolder {
            public TextView textView;
        }
    }
}
