package orangelab.project.common.effect.emotion;
import android.text.TextUtils;

import com.toolkit.action.Keepable;
import java.util.List;

/**
 * Created by hwx on 25/07/2018.
 */


public class EmotionManiFest implements Keepable{

    public int version = 0;
    public List<Emotion> data;

    public Emotion find(String type) {
        if (data != null) {
            for (Emotion item : data) {
                if (TextUtils.equals(type, item.type)) {
                    return item;
                }
            }
        }
        return null;
    }

    public static class Emotion implements Keepable {
        public String type;
        public Name mark;
        public String thumbnail;
        public String url;
        public int delay;

        public Emotion() {
        }

        public Emotion(String type, Name mark, String thumbnail, String url, int time) {
            this.type = type;
            this.mark = mark;
            this.thumbnail = thumbnail;
            this.url = url;
            this.delay = time;
        }
    }

    public static class Name implements Keepable {
        //简体名
        public String j;
        //繁体名
        public String f;
    }
}
