package orangelab.project.common.pay;

/**
 * game_werewolf
 * 2017/7/21 下午2:00
 * Mystery
 */

public class PayUtils {

    public static final String PAY_TEST_PAY = "android.test.purchased";
    public static final String PAY_TEST_CANCEL = "android.test.canceled";
    public static final String PAY_DATE_PAYTEST = "cn.orangelab.werewolf.60diamonds";

    public static final String GOOGLE = "GOOGLE";
    public static final String WECHAT = "WECHAT";

}
