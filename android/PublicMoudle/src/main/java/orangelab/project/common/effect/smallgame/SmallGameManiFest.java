package orangelab.project.common.effect.smallgame;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

import java.util.List;

import orangelab.project.common.effect.emotion.EmotionManiFest;

/**
 * Created by hwx on 02/08/2018.
 */


public class SmallGameManiFest implements Keepable {
    public SmallGameManiFest(){}
    public List<SmallGame> data;
    public static class SmallGame extends EmotionManiFest.Emotion implements Keepable {
        private int cost;//消耗金币或钻石
    }
}
