package orangelab.project.common.effect;

import com.androidtoolkit.PLog;
import com.toolkit.action.Action;

import java.util.Set;

import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.effect.headdecor.HeadDecorManiFest;
import orangelab.project.common.manager.ApiManager;

/**
 * game_werewolf
 * 2018/7/13 下午1:46
 * Mystery
 */
class HeadDecorEffectsManager {

    private static final String TAG = "HeadDecorEffectsManager";

    private boolean isChecking = false;

    public HeadDecorEffectsManager() {

    }

    private boolean isChecking() {
        return isChecking;
    }

    private void setChecking(boolean checking) {
        isChecking = checking;
    }

    public void refreshHeadDecorManifest(Action<Integer> callback) {
        if (isChecking()) return;
        setChecking(true);
        final int version = EffectsMainFestManager.aManager.getHeadDecorVersion();
        ApiManager.GetHeadDecorManiFest(version, (value, e) -> {
            if (e == null) {
                /**
                 * 服务器返回了正常的数据，说明需要刷新数据，并重新校验
                 */
                PLog.i(TAG, "Need Refreash All HeadDecor Data");
                if (value != null) {
                    diffCheckHeadDecorManifest(version, value, callback);
                }
            } else {
                PLog.i(TAG, "Need Check All HeadDecor Data");
                /**
                 * 说明不需要更新
                 */
                HeadDecorManiFest temp = EffectsMainFestManager.aManager.getHeadDecorManiFest();
                if (temp != null) {
                    Set<String> result = EffectsMainFestManager.aManager.verifyHeadDecorManifestData(temp);
                    PLog.i(TAG, "HeadDecor result " + result);
                    if (result != null && result.size() > 0) {

                        /**
                         * 下载一下
                         */
                        EffectsMainFestManager.aManager.downloadAndSaveHeadDecorManifestResource(temp, false);
                        /**
                         * 校验一下
                         */
                        EffectsMainFestManager.aManager.verifyHeadDecorManifestData(temp);

                    } else {
                        EffectsMainFestManager.aManager.updateHeadDecorManifest(temp);
                        if (callback != null) {
                            callback.func(0);
                        }
                    }
                } else {
                    PLog.i(TAG, "BAD Action");
                    /**
                     * local 文件不存在了这里操作比较复杂,说明之前存在过，但是因为何种原因文件不存在了
                     *
                     * 1.移除版本号
                     * 2.移除清单文件，重新进行下载
                     */
                    EffectsMainFestManager.aManager.removeHeadDecorManifest();
                    EffectsMainFestManager.aManager.removeHeadDecorVersion();
                    setChecking(false);
                    refreshHeadDecorManifest(null);
                    return;
                }
            }
            setChecking(false);
        });
    }

    private void downloadAndVerifyHeadDecorManifest(HeadDecorManiFest input, boolean force, Action<Integer> callback) {

        /**
         * 拉取头像框数据
         */
        EffectsMainFestManager.aManager.downloadAndSaveHeadDecorManifestResource(input, force);

        /**
         * 下载完毕后，进行文件完整性校验
         */
        Set<String> verifyResult = EffectsMainFestManager.aManager.verifyHeadDecorManifestData(input);
        PLog.i(TAG, "Head Decor Result " + verifyResult);
        if (verifyResult != null && verifyResult.size() > 0) {
            if (callback != null) {
                callback.func(-1);
            }
        } else {
            if (callback != null) {
                callback.func(0);
            }
        }

    }

    private void diffCheckHeadDecorManifest(int localVersion, HeadDecorManiFest input, Action<Integer> callback) {
        HeadDecorManiFest maniFest = EffectsMainFestManager.aManager.getHeadDecorManiFest();
        if (maniFest == null) {
            /**
             * 本地还没有HeadDecor文件
             */
            try {
                EffectsMainFestManager.aManager.saveHeadDecorManifest(input);
                downloadAndVerifyHeadDecorManifest(input, false, callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            /**
             * 本地已经有了需要判断版本号
             */
            try {
                if (maniFest.rev == input.rev) {
                    if (callback != null) {
                        callback.func(0);
                    }
                } else {
                    downloadAndVerifyHeadDecorManifest(input, false, callback);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (callback != null) {
                    callback.func(-1);
                }
            }
        }
    }

}
