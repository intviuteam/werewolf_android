package orangelab.project.common.effect.view;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;

import com.toolkit.action.Destroyable;

import orangelab.project.common.effect.frames.ThreadFramesAnim;
import orangelab.project.common.effect.gif.GifFramesAnim;

/**
 * game_werewolf
 * 2018/7/21 上午11:07
 * Mystery
 */
public class RoomGifImageView extends AppCompatImageView implements ThreadFramesAnim.Listener {

    public static class Task {
        public byte[] data;   //gif 数据
        public int repeatTime;  //gif 重复次数
        public long forceEndDuration;  //持续时间，用于强行掐断gif播放
        public ThreadFramesAnim.FrameCreator mFrameCreator;

        public Task(byte[] data, int repeatTime, long time) {
            this.data = data;
            this.repeatTime = repeatTime;
            this.forceEndDuration = time;
        }
    }

    private static final String TAG = "RoomExpressionImageView";

    private GifFramesAnim mGifFramesAnim;

    public RoomGifImageView(Context context) {
        this(context, null);
    }

    public RoomGifImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomGifImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mGifFramesAnim = new GifFramesAnim(this);
    }

    public void setFinishListener(Runnable runnable) {
        if (mGifFramesAnim != null) {
            mGifFramesAnim.setFinishListener(runnable);
        }
    }

    public void start(Task task) {
        if (task == null) return;
        if (!mGifFramesAnim.isInit()) {
            mGifFramesAnim.init();
            mGifFramesAnim.setListener(this);
        }

        mGifFramesAnim.setBytes(task.data);
        mGifFramesAnim.setRepeat(task.repeatTime);
        mGifFramesAnim.start();
        mGifFramesAnim.setLastFrameCreator(task.mFrameCreator);
        /**
         * 如果设定了强制结束
         */
        if (task.forceEndDuration > 0) {
            mGifFramesAnim.stopDelay(task.forceEndDuration);
        }
    }

    public void destroy() {
        if (mGifFramesAnim != null) {
            mGifFramesAnim.release();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart: ");
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause: ");
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume: ");
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop: ");
    }

    @Override
    public void onFinish() {
        Log.i(TAG, "onFinish: ");
    }
}
