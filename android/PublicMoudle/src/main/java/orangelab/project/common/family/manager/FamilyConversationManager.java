package orangelab.project.common.family.manager;

import android.text.TextUtils;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.R;
import com.toolkit.action.Action;
import com.toolkit.action.ResultAction;
import orangelab.project.common.utils.MessageUtils;

import java.util.Arrays;
import java.util.List;

import orangelab.thirdparty.leancloud.chatkit.LCChatKit;

/**
 * game_werewolf
 * 2017/10/20 上午11:24
 * Mystery
 */

public class FamilyConversationManager {

    /**
     * 创建会话
     *
     * @param result
     */
    public static void CreateFamilyConversation(ResultAction<String> result) {
        if (LCChatKit.getInstance().getClient() != null) {
            LCChatKit.getInstance().getClient().createConversation(Arrays.asList(""), "", null, false, false, new AVIMConversationCreatedCallback() {
                @Override
                public void done(AVIMConversation avimConversation, AVIMException e) {
                    if (e != null) {
                        if (result != null) {
                            result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
                        }
                    } else {
                        if (result != null) {
                            result.func(0, avimConversation.getConversationId());
                        }
                    }
                }
            });
        } else {
            if (result != null) {
                result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
            }
        }
    }

    /**
     * 向家族中添加成员
     *
     * @param conversationId
     * @param result
     */
    public static void AddMemberIntoFamilyConversation(String conversationId, String memberId, ResultAction<String> result) {
        FindFamilyConversationById(conversationId, conversation -> {
            if (conversation != null) {
                conversation.addMembers(Arrays.asList(memberId), new AVIMConversationCallback() {
                    @Override
                    public void done(AVIMException e) {
                        if (e == null) {
                            if (result != null) {
                                result.func(0, "");
                            }
                        } else {
                            e.printStackTrace();
                            if (result != null)
                                result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
                        }
                    }
                });
            } else {
                if (result != null)
                    result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
            }
        });
    }

    /**
     * 向家族中添加成员
     *
     * @param conversationId
     * @param members
     * @param result
     */
    public static void AddMemberIntoFamilyConversation(String conversationId, List<String> members, ResultAction<String> result) {
        FindFamilyConversationById(conversationId, conversation -> {
            if (conversation != null) {
                conversation.addMembers(members, new AVIMConversationCallback() {
                    @Override
                    public void done(AVIMException e) {
                        if (e == null) {
                            if (result != null) {
                                result.func(0, "");
                            }
                        } else {
                            e.printStackTrace();
                            if (result != null)
                                result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
                        }
                    }
                });
            } else {
                if (result != null)
                    result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
            }
        });
    }

    /**
     * 从家族中踢出某人
     *
     * @param conversationId
     * @param memberId
     * @param result
     */
    public static void KickOutMemberFromFamilyConversation(String conversationId, String memberId, ResultAction<String> result) {
        FindFamilyConversationById(conversationId, conversation -> {
            if (conversation != null) {
                conversation.kickMembers(Arrays.asList(memberId), new AVIMConversationCallback() {
                    @Override
                    public void done(AVIMException e) {
                        if (e == null) {
                            if (result != null) {
                                result.func(0, "");
                            }
                        } else {
                            e.printStackTrace();
                            if (result != null)
                                result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
                        }
                    }
                });
            } else {
                if (result != null)
                    result.func(-1, MessageUtils.getString(R.string.string_create_family_error_1));
            }
        });
    }

    /**
     * 根据家族会话Id查找家族会话
     *
     * @param conversationId
     * @param result
     */
    public static void FindFamilyConversationById(String conversationId, Action<AVIMConversation> result) {
        if (TextUtils.isEmpty(conversationId)) {
            if (result != null) {
                result.func(null);
            }
            return;
        }
        if (LCChatKit.getInstance().getClient() != null) {
            LCChatKit.getInstance().getClient().getConversationsQuery().whereEqualTo("objectId", conversationId)
                    .findInBackground(new AVIMConversationQueryCallback() {
                        @Override
                        public void done(List<AVIMConversation> list, AVIMException e) {
                            if (e != null) {
                                if (result != null) {
                                    result.func(null);
                                }
                            } else {
                                if (list != null && !list.isEmpty()) {
                                    AVIMConversation conversation = list.get(0);
                                    if (result != null) {
                                        result.func(conversation);
                                    }
                                } else {
                                    if (result != null) {
                                        result.func(null);
                                    }
                                    return;
                                }
                            }
                        }
                    });
        } else {
            if (result != null) {
                result.func(null);
            }
        }
    }

}
