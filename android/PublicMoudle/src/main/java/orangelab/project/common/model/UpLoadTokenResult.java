package orangelab.project.common.model;

/**
 * game_werewolf
 * 2017/10/28 下午2:51
 * Mystery
 */

public class UpLoadTokenResult {

    public String token;
    public int expires;
    public String saveKey = "";
}
