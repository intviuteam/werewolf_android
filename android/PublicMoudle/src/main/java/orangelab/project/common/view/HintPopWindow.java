package orangelab.project.common.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.R;
import orangelab.project.game.config.GameRoomConfig;
import orangelab.project.common.utils.HintManager;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/6/17 上午10:36
 * Mystery
 */

public class HintPopWindow extends PopupWindow {

    public static final String PREPARE_HINT = "PREPARE_HINT";
    public static final String CHANGE_OWNER_HINT = "CHANGE_OWNER_HINT";

    public static class Builder {
        private String type;
        private String text;
        private int arrowDirection = ArrowRectangleViewGroup.ARROW_TOP_RIGHT;
        private int arrowOffset = ScreenUtils.dip2px(16);
        private int backgroundColor = Color.TRANSPARENT;

        public Builder setText(String text) {
            this.text = text;
            return this;
        }

        public Builder setArrowDirection(int arrowDirection) {
            this.arrowDirection = arrowDirection;
            return this;
        }

        public Builder setArrowOffset(int arrowOffset) {
            this.arrowOffset = arrowOffset;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public HintPopWindow build(Context context) {
            return new HintPopWindow(context, this);
        }

        public String getText() {
            return text;
        }

        public int getBackgroundColor() {
            return backgroundColor;
        }

        public void setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public int getArrowDirection() {
            return arrowDirection;
        }

        public int getArrowOffset() {
            return arrowOffset;
        }

        public String getType() {
            return type;
        }
    }

    private TextView textView;
    private Context context;
    private ArrowRectangleViewGroup container;
    private int windowWidth;
    private int windowHeight;
    private String type;

    private HintPopWindow(Context context, Builder builder) {
        super(context);
        this.context = context;
        type = builder.getType();
        View containerHolder = View.inflate(context, R.layout.layout_hint_popwindow, null);
        container = (ArrowRectangleViewGroup) containerHolder.findViewById(R.id.container);
        container.setArrowOffset(builder.getArrowOffset());
        container.setArrowDirection(builder.getArrowDirection());
        container.setOnClickListener(v -> {
            dismiss();
        });
        container.setBackgroundColor(GameRoomConfig.GetHintPopBgColor(GlobalUserState.getGlobalState().getCurGameType()));
        textView = (TextView) containerHolder.findViewById(R.id.pop_hint_message);
        textView.setText(builder.getText());
        textView.setTextColor(GameRoomConfig.GetHintPopTextColor(GlobalUserState.getGlobalState().getCurGameType()));

        containerHolder.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        setContentView(containerHolder);
        setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setFocusable(false);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        windowWidth = containerHolder.getMeasuredWidth();
        windowHeight = containerHolder.getMeasuredHeight();
    }

    public int getWindowWidth() {
        return windowWidth;
    }

    public int getWindowHeight() {
        return windowHeight;
    }

    public void show(View parent, int xOffset, int yOffset) {
        showAtLocation(parent, Gravity.NO_GRAVITY, xOffset, yOffset);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        HintManager.recordHint(context, type);
    }
}
