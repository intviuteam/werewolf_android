package orangelab.project.common.family;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.androidtoolkit.RxToolKit;
import com.R;
import orangelab.project.common.activity.LockAbleActivity;
import orangelab.project.common.family.adapter.FamilyMembersAdapter;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.model.FamilyMembersApiResult;
import orangelab.project.common.family.view.FamilyMemberActionPopUpWindow;
import orangelab.project.common.tool.RecycleViewPageHelper;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.RecycleViewDivider;
import orangelab.project.common.view.TitleView;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import cn.intviu.support.GsonHelper;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/10/19 下午12:08
 * Mystery
 */

public class FamilyMembersActivity extends LockAbleActivity implements SwipeRefreshLayout.OnRefreshListener, RecycleViewPageHelper.RecycleViewPageHelperAction {

    private TitleView mTitleView;
    private RecyclerView mFamilyMembersList;
    private FamilyMembersAdapter mFamilyMembersAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecycleViewPageHelper mRecycleViewPageHelper;

    private final int mLimit = 20;
    private String mFamilyId = "";
    private int mIntent = FamilyMemberActionPopUpWindow.Config.FOR_MEMBERS_ACTION;
    private final String mRule = "online";
    private RequestTask mRequestTask;

    private View.OnClickListener turnApply = v -> {
        FamilyApplicantListActivity.Launch(this);
    };

    public static final void LaunchForMemberDetails(Context context, String familyId) {
        Intent intent = new Intent(context, FamilyMembersActivity.class);
        intent.putExtra(FamilyConstant.FAMILY_ID, familyId);
        intent.putExtra(FamilyConstant.FAMILY_MEMBERS_INTENT, FamilyMemberActionPopUpWindow.Config.FOR_MEMBERS_ACTION);
        context.startActivity(intent);
    }

    public static final void LaunchForTransferBoss(Context context, String familyId) {
        Intent intent = new Intent(context, FamilyMembersActivity.class);
        intent.putExtra(FamilyConstant.FAMILY_ID, familyId);
        intent.putExtra(FamilyConstant.FAMILY_MEMBERS_INTENT, FamilyMemberActionPopUpWindow.Config.FOR_TRANSFER_BOSS);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        initData();
        setContentView(R.layout.layout_activity_family_members);
        initView();
        executeActivityDrawFinish(() -> {
            addUnLockViewRect(mTitleView);
            requestAllLock();
            initRequest();
        });
        initListener();
    }

    private void initListener() {
        RxToolKit.Build(mActivityKey, FamilyEvent.KickOutEvent.class).action(v -> {
            runOnUiThreadSafely(() -> {
                if (mFamilyMembersAdapter != null) {
                    mFamilyMembersAdapter.remove(v.getUserId());
                }
            });
        }).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.TransferEvent.class).action(v -> {
            finish();
        }).register();
        RxToolKit.Build(mActivityKey, FamilyEvent.QuitFamilyEvent.class).action(v -> {
            finish();
        }).register();
    }

    private void initData() {
        mFamilyId = getIntent().getStringExtra(FamilyConstant.FAMILY_ID);
        mIntent = getIntent().getIntExtra(FamilyConstant.FAMILY_MEMBERS_INTENT, mIntent);
    }

    private void initRequest() {
        refreshRequestTaskAndExecute(mFamilyId, 0, mRule, mFamilyMembersAdapter);
    }

    private void refreshRequestTaskAndExecute(String id, int skip, String rule, RecyclerView.Adapter adapter) {
        if (mRequestTask != null && mRequestTask.isExecuting()) {
            mRequestTask.cancel();
            mRequestTask = null;
        }
        if (TextUtils.isEmpty(mFamilyId)) {
            return;
        }
        mRequestTask = RequestTask.create()
                .request("/group/members/" + id + "?skip=" + skip + "&limit=" + mLimit + "&sort=" + rule)
                .isGet(true)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        final FamilyMembersApiResult result = GsonHelper.getGson().fromJson(response, FamilyMembersApiResult.class);
                        runOnUiThreadSafely(() -> {
                            FamilyMembersAdapter fadapter = (FamilyMembersAdapter) adapter;
                            if (skip == 0) {
                                fadapter.clear();
                                fadapter.updateData(result.datas);
                            } else {
                                fadapter.add(result.datas);
                                mRecycleViewPageHelper.loadMoreFinish();
                            }
                            mRecycleViewPageHelper.addPage();
                            mSwipeRefreshLayout.setRefreshing(false);
                            handleNotice(result);
                            requestAllUnLock();
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        runOnUiThreadSafely(() -> {
                            RunnableFactory.createMsgToastRunnable(failed).run();
                            mSwipeRefreshLayout.setRefreshing(false);
                            mRecycleViewPageHelper.loadMoreFinish();
                            requestAllUnLock();
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        runOnUiThreadSafely(() -> {
                            RunnableFactory.createNetworkErrorRunnable().run();
                            mSwipeRefreshLayout.setRefreshing(false);
                            mRecycleViewPageHelper.loadMoreFinish();
                            requestAllUnLock();
                        });
                    }
                }).build();
        mRequestTask.execute();
    }

    private void handleNotice(FamilyMembersApiResult result) {
        try {
            if (mIntent == FamilyMemberActionPopUpWindow.Config.FOR_MEMBERS_ACTION) {
                if (result.require_count != null) {
                    if (result.require_count != null && result.require_count > 0) {
                        mTitleView.initRight(R.mipmap.ico_family_members_notice_with_red, turnApply);
                    } else {
                        mTitleView.initRight(R.mipmap.ico_family_members_notice, turnApply);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initView() {
        mTitleView = (TitleView) findViewById(R.id.id_family_members_title);
        mTitleView.setActionBack(v -> {
            finish();
        });
        mTitleView.setTitle(R.string.string_family_members_title);
        mFamilyMembersList = (RecyclerView) findViewById(R.id.id_family_members_detail_list);
        mFamilyMembersList.setLayoutManager(new WrapContentLinearLayoutManager(this));
        mFamilyMembersList.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, ScreenUtils.dip2px(0.5f), Color.parseColor("#e5e5e5")));
        mFamilyMembersAdapter = new FamilyMembersAdapter(mFamilyId, mIntent);
        mFamilyMembersList.setAdapter(mFamilyMembersAdapter);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.id_family_members_swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        RecycleViewPageHelper.RecycleViewPageHelperConfig config = new RecycleViewPageHelper.RecycleViewPageHelperConfig.Builder()
                .nextPageEnable(true).pageSize(mLimit).pageIndex(0).action(this).build();
        mRecycleViewPageHelper = new RecycleViewPageHelper(mFamilyMembersList, mFamilyMembersAdapter, config);

    }

    @Override
    public void finish() {
        super.finish();
        RxToolKit.UnRegister(mActivityKey);
        if (mRecycleViewPageHelper != null) {
            mRecycleViewPageHelper.destroy();
        }
        if (mRequestTask != null && mRequestTask.isExecuting()) {
            mRequestTask.cancel();
        }
        if (mFamilyMembersAdapter != null) {
            mFamilyMembersAdapter.destroy();
        }
    }

    @Override
    public void onRefresh() {
        if (mRecycleViewPageHelper != null) {
            mRecycleViewPageHelper.refreshPage();
        }
        /**
         * 超时检测
         */
        runOnUiThreadSafely(() -> {
            if (mRequestTask != null && mRequestTask.isExecuting()) {
                mRequestTask.cancel();
            }
            mSwipeRefreshLayout.setRefreshing(false);
        }, 30000);
    }

    @Override
    public void onRefresh(int pageIndex, int pageSize, RecyclerView.Adapter mAdapter) {
        refreshRequestTaskAndExecute(mFamilyId, pageIndex * mLimit, mRule, mAdapter);
    }

    @Override
    public void onNextPage(int pageIndex, int pageSize, RecyclerView.Adapter mAdapter) {
        refreshRequestTaskAndExecute(mFamilyId, pageIndex * mLimit, mRule, mAdapter);
    }
}
