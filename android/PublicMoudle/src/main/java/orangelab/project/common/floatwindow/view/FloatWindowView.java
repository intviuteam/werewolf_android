package orangelab.project.common.floatwindow.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import com.androidtoolkit.RxToolKit;
import orangelab.project.common.floatwindow.event.RestoreEvent;
import orangelab.project.common.floatwindow.model.FloatUser;
import com.R;
import orangelab.project.common.event.SmallWindowEvent;
import com.toolkit.action.Destroyable;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Author:xishuang
 * Date:2017.08.01
 * Des:悬浮窗的布局
 */
public class FloatWindowView extends FrameLayout implements Destroyable {

    private static final String TAG = "FloatWindowView";
    private static int TouchSlop = 16;//最小滑动距离
    private static float TimeSlop = 0.2f * 1000f;//最小点击时间

    private final WindowManager mWindowManager;

    private long startTime;

    private float mTouchStartX;

    private float mTouchStartY;

    private boolean isclick;

    private WindowManager.LayoutParams mWmParams;

    private Context mContext;

    private long endTime;

    private View child;

    private CircleImageView userHead;

    private FloatUser floatUser;

    private ImageView mWaveView;

    public FloatWindowView(@NonNull Context context) {
        this(context, null);
    }

    public FloatWindowView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FloatWindowView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        TouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        child = View.inflate(context, R.layout.layout_float_window, this).findViewById(R.id.float_window_user_container);
        child.setBackgroundResource(R.mipmap.float_view_border);
        userHead = (CircleImageView) child.findViewById(R.id.float_window_user_head);
        mWaveView = (ImageView) child.findViewById(R.id.wave_view);
        initListener();
        startWave();
    }

    private void initListener() {
        RxToolKit.Build(this, SmallWindowEvent.NotifySWChangeHead.class).action(v -> {
            if (userHead != null) {
                userHead.post(() -> {
                    changeUserHead(v.headImgUrl);
                });
            }
        }).register();
    }

    public void setFloatUser(FloatUser user) {
        this.floatUser = user;
        changeUserHead(user.userHead);
    }

    public void changeUserHead(String userHead) {
        PicassoUtils.loadImage(getContext(), userHead, this.userHead, R.mipmap.default_head);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        isclick = false;

        int x = (int) event.getRawX();
        int y = (int) event.getRawY();

        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                child.setBackgroundResource(R.mipmap.float_view_border_press);

                startTime = System.currentTimeMillis();

                mTouchStartX = event.getX();
                mTouchStartY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:

                float mMoveStartX = event.getX();
                float mMoveStartY = event.getY();

                if (Math.abs(mTouchStartX - mMoveStartX) > TouchSlop && Math.abs(mTouchStartY - mMoveStartY) > TouchSlop) {
                    // 更新浮动窗口位置参数
                    mWmParams.x = (int) (x - mTouchStartX);
                    mWmParams.y = (int) (y - mTouchStartY);
                    mWindowManager.updateViewLayout(this, mWmParams);
                    return false;
                }
                break;
            case MotionEvent.ACTION_UP:
                child.setBackgroundResource(R.mipmap.float_view_border);
                endTime = System.currentTimeMillis();
                if ((endTime - startTime) > TimeSlop) {
                    isclick = false;
                } else {
                    isclick = true;
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                child.setBackgroundResource(R.mipmap.float_view_border);
                break;
        }
        if (isclick) {
            /**
             * 这里处理点击事件
             */
            RxToolKit.Emit(new RestoreEvent());

        }
        return true;
    }

    /**
     * 将小悬浮窗的参数传入，用于更新小悬浮窗的位置。
     *
     * @param params 小悬浮窗的参数
     */
    public void setParams(WindowManager.LayoutParams params) {
        mWmParams = params;
    }

    public WindowManager.LayoutParams getWmParams() {
        return mWmParams;
    }

    //开始波形震动
    public void startWave() {
        AnimationDrawable drawable = (AnimationDrawable) mWaveView.getDrawable();
        if (drawable != null) {
            drawable.start();
        }
    }

    //停止波形震动
    public void stopWave() {
        AnimationDrawable drawable = (AnimationDrawable) mWaveView.getDrawable();
        if (drawable != null) {
            drawable.stop();
        }
    }

    @Override
    public void destroy() {
        stopWave();
        RxToolKit.UnRegister(this);
        Log.i(TAG, "destroy: ");
    }
}
