package orangelab.project.common.model;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2017/10/27 下午12:11
 * Mystery
 */

public class OnlineModel implements Keepable {

    public boolean isOnline;
    public String state;
}
