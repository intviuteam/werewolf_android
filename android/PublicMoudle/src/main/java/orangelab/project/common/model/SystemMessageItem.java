package orangelab.project.common.model;

import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

import orangelab.project.minigame.model.MiniGameItem;
import orangelab.project.spyroom.model.SpyRoomPlayer;
import orangelab.project.voice.lobby.model.LobbyAcceptGameBean;

/**
 * Author:1727
 * 2017/3/7
 */

public class SystemMessageItem {


    public static final int TYPE_MEMBER = 1;//成员消息
    public static final int TYPE_SYSTEM = 2;//系统消息
    public static final int TYPE_JUDGE = 3;//法官消息
    public static final int TYPE_SMALL_GAME = 4;
    public static final int TYPE_SPY_MSG = 5;//谁是卧底消息
    public static final int TYPE_LOBBY = 6;//语音房大厅小游戏消息


    private int type;
    private String msg;
    private String userName;//成员名字
    private EnterRoomResult.EnterRoomUserItem user;//发这条消息的成员
    private int number = -1;//成员
    public TextView msgbackgroud;
    private String name;
    private String exhibitionType;
    private int score = -1;

    private int color;
    private int gravity;//对齐方式

    private SpyRoomPlayer spyRoomPlayer;//发这条消息的成员

    private String icon = "";               //消息展示的icon
    private String invite_id = "";          //大厅处理的独立的id
    private String gameType = "";           //游戏类型
    private String uuid = "";               //uuid小游戏使用
    private String leaveType = "";          //离开类型 play_game表示离开去参加小游戏，empty表示正常离开
    private LobbyAcceptGameBean.Acceptor acceptor;//接收者

    public SystemMessageItem() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public String getUserName() {
        return userName;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public static SystemMessageItem createJudgeMessage(String msg) {
        SystemMessageItem item = new SystemMessageItem();
        item.setType(TYPE_JUDGE);
        item.setMsg(msg);
        return item;
    }

    public void setExhibitionType(String exhibitionType) {
        this.exhibitionType = exhibitionType;
    }

    public String getExhibitionType() {
        return exhibitionType;
    }

    public static SystemMessageItem createMemberMessage(int position, String name, String msg) {
        SystemMessageItem item = new SystemMessageItem();
        item.setType(TYPE_MEMBER);
        item.setNumber(position);
        item.setName(name);
        item.setMsg(msg);
        return item;
    }

    public static SystemMessageItem createGiftMessage(int position, String msg, String exhibitionType, String name) {
        SystemMessageItem item = new SystemMessageItem();
        item.setType(TYPE_MEMBER);
        item.setNumber(position);
        item.setName(name);
        item.setMsg(msg);
        item.setExhibitionType(exhibitionType);
        return item;
    }

    public static SystemMessageItem createSystemMessage(String msg) {
        SystemMessageItem item = new SystemMessageItem();
        item.setType(TYPE_SYSTEM);
        item.setMsg(msg);
        return item;
    }

    public static SystemMessageItem createGiftMessage(int color, int position, String msg, String exhibitionType, String name) {
        SystemMessageItem item = createGiftMessage(position, msg, exhibitionType, name);
        item.setColor(color);
        return item;
    }

    public static SystemMessageItem createMemberMessage(int color, int position, String name, String msg) {
        SystemMessageItem item = createMemberMessage(position, name, msg);
        item.setColor(color);
        return item;
    }

    public static SystemMessageItem createSystemMessage(int color, String msg) {
        SystemMessageItem item = createSystemMessage(msg);
        item.setColor(color);
        return item;
    }

    public static SystemMessageItem createJudgeMessage(int color, String msg) {
        SystemMessageItem item = createJudgeMessage(msg);
        item.setColor(color);
        return item;
    }

    public static SystemMessageItem createScoreMessage(int position, int score, String name) {
        SystemMessageItem item = new SystemMessageItem();
        item.setType(TYPE_SMALL_GAME);
        item.setNumber(position);
        item.setScore(score);
        item.setName(name);
        return item;
    }

    public static SystemMessageItem createSpyGameMessage(String title, String msg, int gravity) {
        SystemMessageItem item = new SystemMessageItem();
        item.setColor(Color.WHITE);
        item.setType(TYPE_SPY_MSG);
        item.setName(title);
        item.setMsg(msg);
        item.setGravity(gravity);

        return item;
    }

    public static SystemMessageItem createSpyRoomGameMessage(String title, String msg, int gravity) {
        SystemMessageItem item = new SystemMessageItem();
        item.setColor(Color.parseColor("#444444"));
        item.setType(TYPE_SPY_MSG);
        item.setName(title);
        item.setMsg(msg);
        item.setGravity(gravity);

        return item;
    }

    public static SystemMessageItem createLobbyRoomGameMessage(String icon, String name, String msg) {
        SystemMessageItem item = new SystemMessageItem();
        item.setColor(Color.parseColor("#ffffff"));
        item.setIcon(icon);
        item.setType(TYPE_LOBBY);
        item.setName(name);
        item.setMsg(msg);
        item.setGravity(Gravity.CENTER);

        return item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public SystemMessageItem changeColor(int color) {
        this.color = color;
        return this;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getGravity() {
        return gravity;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public EnterRoomResult.EnterRoomUserItem getUser() {
        //add user
        return user;
    }

    public void setUser(EnterRoomResult.EnterRoomUserItem user) {
        //set user
        this.user = user;
    }

    public SpyRoomPlayer getSpyUser() {
        return spyRoomPlayer;
    }

    public void setUser(SpyRoomPlayer user) {
        //set user
        this.spyRoomPlayer = user;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public String getInvite_id() {
        return invite_id;
    }

    public void setInvite_id(String invite_id) {
        this.invite_id = invite_id;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public LobbyAcceptGameBean.Acceptor getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(LobbyAcceptGameBean.Acceptor acceptor) {
        this.acceptor = acceptor;
    }

    @Override
    public String toString() {
        return "SystemMessageItem{" +
                "type=" + type +
                ", msg='" + msg + '\'' +
                ", userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", gameType='" + gameType + '\'' +
                '}';
    }
}
