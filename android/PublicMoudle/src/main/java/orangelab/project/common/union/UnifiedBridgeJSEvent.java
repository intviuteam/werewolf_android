package orangelab.project.common.union;

/**
 * game_werewolf
 * 2017/9/20 上午10:46
 * Mystery
 */

public class UnifiedBridgeJSEvent {

    private String commend;

    public UnifiedBridgeJSEvent(String commend) {
        this.commend = commend;
    }

    public String getCommend() {
        return commend;
    }
}
