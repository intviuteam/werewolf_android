package orangelab.project.common.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.R;
import orangelab.project.common.utils.ImageUtils;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.TitleView;
import com.photoview.PhotoView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

/**
 * game_werewolf
 * 2017/7/28 下午3:47
 * Mystery
 */

public class PictureActivity extends SafeActivity {

    private PhotoView photoView;
    private TitleView mTitleView;

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        setContentView(R.layout.layout_picture);
        photoView = (PhotoView) findViewById(R.id.photoview);
        mTitleView = (TitleView) findViewById(R.id.titleview);
        mTitleView.setTitle("个人头像");
        mTitleView.setActionBack(v -> {
            finish();
        });
        String string = IntentDataHelper.getUserAvater(getIntent());
        if (TextUtils.isEmpty(string)) {
            return;
        }
        Picasso.get()
                .load(string)
                .into(photoView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ImageUtils.releaseImageViewResouce(photoView);
    }
}
