package orangelab.project.common.engine;

import orangelab.project.common.model.EnterRoomResult;

import cn.intviu.sdk.model.User;

/**
 * Author:1727
 * 2017/3/3
 */

public interface IOrbitEngine {

    public void start(String roomId, EnterRoomResult.RoomServerMessage message, User user);

    void setDefaultOpenSpeaker(boolean defaultOpenSpeaker);

    void shutdown();

    void shutdown(boolean needNotify, Runnable shutDownComplete);

    void closeRecord();

    void openRecord();

    void closeSpeaker();

    void openSpeaker();
}
