package orangelab.project.common.tool;

import com.google.gson.JsonSyntaxException;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/3/20 上午10:55
 * Mystery
 */

public class GsonTurnToolKit {

    public static <T> T turn(String string, Class<T> tClass) {
        try {
            T result = GsonHelper.getGson().fromJson(string, tClass);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
