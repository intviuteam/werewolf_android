package orangelab.project.common.pay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

import orangelab.project.common.dialog.SafeDialog;
import orangelab.project.common.utils.MessageUtils;

import cn.intviu.connect.model.Message;

/**
 * game_werewolf
 * 2017/7/24 下午1:55
 * Mystery
 */

public class PayResultDialog extends SafeDialog implements View.OnClickListener {

    private TextView mTitle;
    private ImageView mSuccessView;

    private boolean isSuccess = false;

    private View mFailContainer;
    private TextView mFailReason;
    private Button mRetry;
    private Button mCancel;
    private Runnable mRetryRunnable;
    private Runnable mDismissRunnable;
    private boolean needReportDismiss = true;

    public PayResultDialog(@NonNull Context context, Runnable dismissRunnable, Runnable mRetryRunnable) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.dialog_pay_state_layout);
        mTitle = (TextView) findViewById(R.id.pay_state_title);
        mSuccessView = (ImageView) findViewById(R.id.pay_state_success);
        mFailContainer = findViewById(R.id.pay_btn_container);
        mFailReason = (TextView) findViewById(R.id.pay_state_fail_reason);
        mRetry = (Button) findViewById(R.id.pay_retry);
        mCancel = (Button) findViewById(R.id.pay_cancel);
        mRetry.setOnClickListener(this);
        mCancel.setOnClickListener(this);
        this.mRetryRunnable = mRetryRunnable;
        this.mDismissRunnable = dismissRunnable;
    }

    public void setTitle(String msg) {
        mTitle.setText(msg);
    }

    public void needReTryButton(boolean flag) {
        if (flag) {
            mRetry.setVisibility(View.VISIBLE);
        } else {
            mRetry.setVisibility(View.GONE);
            mCancel.setText(MessageUtils.getString(R.string.action_cancel));
            mCancel.setTextColor(getContext().getResources().getColor(R.color.mdtp_white));
            mCancel.setBackgroundResource(R.drawable.user_temp_dialog_kick_btn_bg);
        }
    }

    public void isSuccess(boolean flag) {
        this.isSuccess = flag;
        if (flag) {
            mTitle.setText(MessageUtils.getString(R.string.pay_state_success));
            mSuccessView.setVisibility(View.VISIBLE);
            mFailContainer.setVisibility(View.GONE);
            mFailReason.setVisibility(View.GONE);
        } else {
            mTitle.setText(MessageUtils.getString(R.string.pay_state_fail));
            mSuccessView.setVisibility(View.GONE);
            mFailContainer.setVisibility(View.VISIBLE);
            mFailReason.setVisibility(View.VISIBLE);
        }
    }

    public void setReason(String msg) {
        mFailReason.setText(msg);
    }

    @Override
    public void show() {
        super.show();
        needReportDismiss = true;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (needReportDismiss) {
            if (mDismissRunnable != null) {
                mDismissRunnable.run();
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.pay_retry) {
            needReportDismiss = false;
            dismiss();
            if (mRetryRunnable != null) {
                mRetryRunnable.run();
            }
        } else if (id == R.id.pay_cancel) {
            dismiss();
        }
//        switch (v.getId()) {
//            case R.id.pay_retry:
//                needReportDismiss = false;
//                dismiss();
//                if (mRetryRunnable != null) {
//                    mRetryRunnable.run();
//                }
//                break;
//            case R.id.pay_cancel:
//                dismiss();
//                break;
//        }
    }

    @Override
    protected void release() {

    }
}
