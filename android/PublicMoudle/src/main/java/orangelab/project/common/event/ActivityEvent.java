package orangelab.project.common.event;

import android.text.TextUtils;

/**
 * game_werewolf
 * 2017/6/19 下午1:43
 * Mystery
 */

public class ActivityEvent {
    public static final String onCreate = "onCreate";
    public static final String onResume = "onResume";
    public static final String onPause = "onPause";
    public static final String onStop = "onStop";
    public static final String onDestroy = "onDestroy";


    public static class SplashDialogDismissEvent {

    }

    public static class ActivityWindowInitEvent {

    }

    public static class ReactNativeInitFinishEvent {

    }

    public static class ReactNativeInitDataFinishEvent {

    }

    public static class ReactNativeLogOutEvent {

    }

    public static class GameActivityEvent {

        private String event;

        public GameActivityEvent(String event) {
            this.event = event;
        }

        public String getEvent() {
            return event;
        }

        public boolean isResume() {
            return TextUtils.equals(event, onResume);
        }

        public boolean isPause() {
            return TextUtils.equals(event, onPause);
        }

        public boolean isStop() {
            return TextUtils.equals(event, onStop);
        }

        public boolean isCreate() {
            return TextUtils.equals(event, onCreate);
        }

        public boolean isDestroy() {
            return TextUtils.equals(event, onDestroy);
        }
    }

    public static class StartPayTaskEvent {

    }
}
