package orangelab.project.common.utils;

import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.UIActuator;

/**
 * game_werewolf
 * 2018/2/28 下午2:38
 * Mystery
 */

public enum LinkTaskActuator {

    INSTANCE;


    private static class Task implements Runnable {

        private Runnable mRunnable;
        private Task mNext = null;
        private Task mPre = null;

        private boolean mWantedThread = false;//false UI thread true  async thread

        public Task(Runnable runnable) {
            mRunnable = runnable;
            mWantedThread = false;
        }

        public Task(Runnable runnable, boolean wantedThread) {
            mRunnable = runnable;
            mWantedThread = wantedThread;
        }

        private Task getNext() {
            return mNext;
        }

        private void setNext(Task next) {
            mNext = next;
        }

        private Task getPre() {
            return mPre;
        }

        private void setPre(Task pre) {
            mPre = pre;
        }

        public Task link(Task task) {
            task.setPre(this);
            this.mNext = task;
            return task;
        }

        @Override
        public void run() {
            if (mRunnable != null) {
                try {
                    mRunnable.run();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mRunnable = null;
                }
            }
        }

        public void execute() {
            Task firstTask = mPre;
            while (firstTask != null) {
                firstTask = mPre.getPre();
            }

        }
    }

}
