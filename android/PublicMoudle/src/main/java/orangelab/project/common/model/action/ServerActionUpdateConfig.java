package orangelab.project.common.model.action;

import orangelab.project.common.model.EnterRoomResult;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/3/19 下午7:01
 * Mystery
 */

public class ServerActionUpdateConfig implements Keepable {

    public EnterRoomResult.RoomConfig config;
    public EnterRoomResult.RoomConfigChange change;
}
