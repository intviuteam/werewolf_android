package orangelab.project.common.bridge.channel;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.R;
import com.androidtoolkit.DeviceToolKit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.RxToolKit;
import com.androidtoolkit.ToastToolKit;
import com.androidtoolkit.UIActuator;
import com.androidtoolkit.location.LocationToolKit;
import com.androidtoolkit.transport.TransportHelper;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.datasource.GlobalUserState;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.JsonSyntaxException;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.TransportConfig;
import com.networktoolkit.transport.URLManager;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.toolkit.ToolKit;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import cn.intviu.service.IntviuApplication;
import cn.intviu.support.AppUtil;
import cn.intviu.support.ConstInfo;
import cn.intviu.support.GsonHelper;
import cn.intviu.support.ReportRobot;
import cn.intviu.support.SharePreStoreUtils;
import orangelab.project.ProjectConfig;
import orangelab.project.common.GlobalService;
import orangelab.project.common.RNActivityManager;
import orangelab.project.common.activity.LaunchActivity;
import orangelab.project.common.activity.PersonalInfoActivity;
import orangelab.project.common.engine.GlobalSocketEngine;
import orangelab.project.common.event.ActivityEvent;
import orangelab.project.common.event.AdvEvent;
import orangelab.project.common.event.ShareWeChatEvent;
import orangelab.project.common.exhibition.ExhibitionsRecordActivity;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.family.FamilyMemo;
import orangelab.project.common.floatwindow.FloatWindowCommander;
import orangelab.project.common.floatwindow.permission.FloatPermissionManager;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.EnterRoomPayload;
import orangelab.project.common.share.ShareBridgeData;
import orangelab.project.common.share.SharePoolManager;
import orangelab.project.common.system.SystemController;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedBridgeFilter;
import orangelab.project.common.union.UnifiedBridgeFilterImpl;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.upgrade.UpgradeService;
import orangelab.project.common.utils.CheckApkExist;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.LeanCloudChatHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.ShareMedia;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.web.WebViewActivity;
import orangelab.project.voice.privateroom.PrivateRoomTools;
import orangelab.project.voice.privateroom.model.PrivateRoomEnterWithInvite;
import orangelab.thirdparty.leancloud.chatkit.ChatConfig;
import orangelab.thirdparty.leancloud.chatkit.LCChatKit;
import orangelab.thirdparty.leancloud.chatkit.activity.LCIMConversationActivity;
import orangelab.thirdparty.leancloud.chatkit.utils.LCIMIntentDataHelper;
import orangelab.thirdparty.leancloud.chatkit.utils.LeanCloudFeedbackAgent;
import orangelab.thirdparty.leancloud.chatkit.utils.NotificationUtils;
import orangelab.thirdparty.leancloud.chatkit.utils.UserData;
import rx.android.schedulers.AndroidSchedulers;


public class NativeJSModule extends ReactContextBaseJavaModule {

    private static final String TAG = "NativeJSModule";
    private static final String SHARE_TO_WECHAN = "SendMessageToWX.Resp";

    private ReactApplicationContext mContext;
    private UnifiedBridgeFilter mBridgeFilter = null;

    public NativeJSModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    public void sendEvent(String eventName, WritableMap params) {
        try {
            mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "NativeJSModule";
    }

    /**
     * 用于RN传递消息
     *
     * @param msg
     */
    @ReactMethod
    public void rnCallNative(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @ReactMethod
    public void enterExhibitionRecord(String userID) {
        Utils.runSafely(() -> {
            ExhibitionsRecordActivity.enterExhibitionsRecord(getCurrentActivity(), userID);
        });
    }

    @ReactMethod
    public void LaunchPersonalInfoActivity(String userId) {
        Activity activity = getCurrentActivity();
        if (activity != null) {
            PersonalInfoActivity.Launch(activity, userId);
        }
    }

    /**
     * RN 启动原生的用户详情页面
     *
     * @param activityName
     * @param userId
     * @param token
     * @param isPlaying
     */
    @ReactMethod
    public void rnStartPersonalInfoActivtiy(String activityName, String userId, String token, Boolean isPlaying) {
        try {
            Activity currentActivity = getCurrentActivity();
            if (currentActivity != null) {
                Intent intent = new Intent(currentActivity, PersonalInfoActivity.class);
                IntentDataHelper.setIsPlaying(intent, isPlaying);
                IntentDataHelper.setUserToken(intent, token);
                IntentDataHelper.setUserId(intent, userId);
                currentActivity.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void reactNativeInitDone() {
        RxToolKit.Emit(new ActivityEvent.ReactNativeInitFinishEvent());
    }

    @ReactMethod
    public void launchFeedBack() {
        LeanCloudFeedbackAgent.launchFeedBack(getCurrentActivity());
    }

    @ReactMethod
    public void closeLeanCloud(String userId) {
        if (LCChatKit.getInstance() != null) {
            LCChatKit.getInstance().close(new AVIMClientCallback() {
                @Override
                public void done(AVIMClient avimClient, AVIMException e) {
                    if (null == e) {
                        PLog.i(TAG, "done:Close is Success");
                    } else {
                        /**
                         * ignore
                         */
                    }
                }
            });
        }
        SystemController.clear(userId);
    }

    @ReactMethod
    public void uMengReport(String value) {
        ReportRobot.robot.report(value);
    }

    @ReactMethod
    public void uMengErrorReport(String type, String value) {
        Exception exception = new Exception(type + value);
        // ReportRobot.robot.reportNormalError(type, exception);
        ReportEventUtils.reportError(exception);
    }

    @ReactMethod
    public void quickEnterRoom(String roomId, String roomPassword) {

    }

    @ReactMethod
    public void openTreasureBox() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/task?access_token="
                    + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1(), Constant.OPEN_BOX);
        });
    }

    @ReactMethod
    public void initUserData(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            GlobalUserState.getGlobalState().setUserId(jsonObject.optString("id"));
            GlobalUserState.getGlobalState().setUserSex(jsonObject.optInt("sex"));
            GlobalUserState.getGlobalState().setUserIcon(jsonObject.optString("image"));
            GlobalUserState.getGlobalState().setUserUid(jsonObject.optString("uid"));
            GlobalUserState.getGlobalState().setTourist(jsonObject.optBoolean("isTourist"));
            GlobalUserState.getGlobalState().setToken(jsonObject.optString("token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @ReactMethod
    public void initLeanCloud(String token, boolean isTourist, String userId, String userName, int userSex, String userIv) {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (isTourist) {
            /**
             * 如果是游客清除所有的家族有关信息
             */
            FamilyMemo.ClearFamilyMemo();
        }
        GlobalUserState.getGlobalState().setTourist(isTourist);
        GlobalUserState.getGlobalState().setUserId(userId);
        updateGlobalUserState(userName, userSex, userIv);
        GlobalUserState.getGlobalState().setToken(token);
        if (!isTourist) {
            openLeanCloud(userId);
            /**
             * 这里注意以后在这个init的方法的地方，使用RNActivityManager去管理 getCurrentActivity() 方法并不能保证Activity一定不为null
             */
            GlobalService.startGlobalSocketEngine(RNActivityManager.INSTANCE.GetRNActivity());
        }
        /**
         * 数据初始化完成发射事件
         */
        RxToolKit.Emit(new ActivityEvent.ReactNativeInitDataFinishEvent());
    }

    @ReactMethod
    public void updateGlobalUserState(String userName, int userSex, String userIv) {
        GlobalUserState.getGlobalState().setUserName(userName);
        GlobalUserState.getGlobalState().setUserSex(userSex);
        GlobalUserState.getGlobalState().setUserIcon(userIv);
    }

    private long reTryTime = 5000;
    private long reTryMaxTime = 60000;

    private void openLeanCloud(String userId) {
        LCChatKit.getInstance().open(userId, new AVIMClientCallback() {
            @Override
            public void done(AVIMClient avimClient, AVIMException e) {
                if (null == e) {
                    PLog.i(TAG, "done: 创建客户端成功");
                    ReportRobot.robot.reportLeanCloudOpenSuccess();
                    reTryTime = 5000;
                } else {

                    if (TextUtils.isEmpty(userId)) {
                        ReportRobot.robot.reportLeanCloudOpenFailIdIsNULL();
                    }
                    PLog.e(TAG, e.toString());
                    PLog.i(TAG, "打开LeanCloud失败" + reTryTime + "ms 后进行重新尝试");
                    try {
                        ReportRobot.robot.reportLeanCloudOpenFail(e);
                        ReportRobot.robot.reportLeanCloudOpenFail();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    /**
                     * 打开失败，进行重试
                     */
                    UIActuator.postDelay(() -> {
                        openLeanCloud(userId);
                    }, reTryTime);
                    reTryTime = reTryTime * 2;
                    if (reTryTime >= reTryMaxTime) {
                        reTryTime = reTryMaxTime;
                    }
                }
            }
        });
    }

    @ReactMethod
    public void startChat(
            String conversationId,
            String leftUserId, String leftUserName, int leftSex, String leftUserIv,
            String rightUserId, String rightUserName, int rightSex, String rightUserIv
    ) {
        Utils.runSafely(() -> {
            UserData leftUser = UserData.createUserData(
                    leftUserId, leftUserName, leftSex, leftUserIv
            );
            UserData rightUser = UserData.createUserData(
                    rightUserId, rightUserName, rightSex, rightUserIv
            );
            ChatConfig chatConfig = new ChatConfig();
            chatConfig.leftUser = leftUser;
            chatConfig.rightUser = rightUser;
            chatConfig.conversationId = conversationId;
            chatConfig.peerId = leftUserId;
            chatConfig.isSingleChat = true;

            Intent intent = new Intent(getCurrentActivity(), LCIMConversationActivity.class);
            LCIMIntentDataHelper.setChatConfig(intent, chatConfig);
            getCurrentActivity().startActivity(intent);
        });
    }

    /**
     * 小游戏
     *
     * @param gameType
     */
    @ReactMethod
    public void rnStartMiniGame(String gameType) {
        Utils.runSafely(() -> {
            LaunchActivity.LaunchForAmusementRandom(getCurrentActivity(), gameType);
        });
    }

    /**
     * 随机匹配
     *
     * @param roomId
     * @param gameType
     */
    @ReactMethod
    public void rnStartMiniGameWithRoomId(String roomId, String gameType) {
        Utils.runSafely(() -> {
            LaunchActivity.LaunchForAmusement(getCurrentActivity(), roomId, gameType);
        });
    }

    /**
     * 单机游戏
     *
     * @param gameType 小游戏类型
     */
    @ReactMethod
    public void rnStartConsoleMiniGame(String gameType) {
        Utils.runSafely(() -> {
            LaunchActivity.LaunchForConsoleMiniGame(getCurrentActivity(), gameType);
        });
    }

    @ReactMethod
    public void rnStartMiniGameIntviu(String title, String message, String url, String type, String json) {
        Utils.runSafely(() -> {

            //TODO share
            //ShareUtilsActivity.shareFromMiniGame(getCurrentActivity(), title, url, message);

            Activity context = getCurrentActivity();
            if (context != null) {
                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF)
                        .onMiniGameShare(context, new ShareBridgeData(ShareBridgeData.SHARE_FROM_MINIGAME, title, url, message, type, json));
            }
        });
    }

    @ReactMethod
    public void rnEnterRoom(String roomId, String roomType, String roomPassword) {
        UIActuator.postSafely(() -> {
            LaunchActivity.LaunchForRoom(getCurrentActivity(), IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, roomId, roomType, roomPassword);
        });
    }

    /**
     * 创建私密房间 并包含邀请人员列表
     *
     * @param json
     */
    @ReactMethod
    public void rnEnterPrivacyRoom(String json) {
        UIActuator.postSafely(() -> {
            try {
                PrivateRoomEnterWithInvite invite = GsonHelper.getGson().fromJson(json, PrivateRoomEnterWithInvite.class);
                /**
                 * 进入房间
                 */
                if (!ToolKit.isEmpty(invite.ids)) {
                    TransportHelper.putTransportable(PrivateRoomTools.INSTANCE.getPRIVATE_ROOM_INVITE_LIST(), invite.ids);
                }
                LaunchActivity.LaunchForRoom(getCurrentActivity(), IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, invite.room_id, invite.gameType, invite.password);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        });
    }

    @ReactMethod
    public void rnStartGame(String password, String gameType, String roomId, String userName, String userId, int
            userSex, String avatar, int exp, String token, Callback successCallback, final Callback failedCallback) {
        Utils.runSafely(() -> {
            if (successCallback != null) {
                successCallback.invoke("");
            }
            EnterRoomPayload payload = new EnterRoomPayload();
            payload.password = password;
            payload.gameType = gameType;
            payload.roomId = roomId;
            payload.userName = userName;
            payload.userId = userId;
            payload.userSex = userSex;
            payload.avatar = avatar;
            payload.exp = exp;
            payload.token = token;
            LaunchActivity.LaunchForRoom(getCurrentActivity(), IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
        });
    }

    @ReactMethod
    public void rnEnterAmateurGame() {
        Utils.runSafely(() -> {

            EnterRoomPayload payload = new EnterRoomPayload();
            payload.password = "";
            payload.gameType = Constant.GAME_TYPE_PRE_SIMPLE;
            payload.roomId = "";
            payload.userName = GlobalUserState.getGlobalState().getUserName();
            payload.userId = GlobalUserState.getGlobalState().getUserId();
            payload.userSex = GlobalUserState.getGlobalState().getUserSex();
            payload.avatar = GlobalUserState.getGlobalState().getUserIcon();
            payload.exp = GlobalUserState.getGlobalState().getUserExp();
            payload.token = GlobalUserState.getGlobalState().getToken();
            LaunchActivity.LaunchForRoom(getCurrentActivity(), IntentDataHelper.TYPE_ENTER_ROOM_FROM_RN, payload);
        });
    }

    @ReactMethod
    public void sendTextWhenAgreeFriend(String otherId, String otherName, int otherSex, String otherIv) {
        Utils.runSafely(() -> {
            int messageSource = R.string.add_friend_message;
//            if (ProjectConfig.APP_TYPE == Constant.APP_TYPE_XIAOYU) {
//                messageSource = R.string.add_xiaoyu_friend_message;
//            }
            LeanCloudChatHelper.sendTextMessageIntoConversation(
                    MessageUtils.getString(messageSource),
                    otherId,
                    otherName,
                    otherIv,
                    otherSex,
                    orangelab.thirdparty.leancloud.chatkit.utils.Constant.APPREOVE_FRIEND
            );
        });
    }

    @ReactMethod
    public void getDeviceId(Callback callback) {
        Utils.runSafely(() -> {
            callback.invoke(ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
        });
    }

    @ReactMethod
    public void getUniquePsuedoID(Callback callback) {
        Utils.runSafely(() -> {
            callback.invoke(DeviceToolKit.GetDeviceId());
        });
    }

    @ReactMethod
    public void getUserInfoForMiniGame(String userId) {
        Utils.runSafely(() -> {
            PersonalInfoActivity.LaunchForMiniGameRecord(getCurrentActivity(), userId);
        });
    }

    @ReactMethod
    public void rnShareMessage(String userName) {
        try {
            //TODO share
            //ShareUtilsActivity.shareFromSetting(getCurrentActivity(), userName);

            Activity context = getCurrentActivity();
            if (context != null) {
                SharePoolManager.getProvider(ProjectConfig.APP_TYPE_WEREWOLF)
                        .onSettingShare(context, new ShareBridgeData(ShareBridgeData.SHARE_FROM_SETTING, userName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void checkUpgrade(boolean initiative) {
        Utils.runSafely(() -> {
            Intent intent = new Intent(getCurrentActivity(), UpgradeService.class);
            IntentDataHelper.setUpgradeType(intent, Constant.CHECK_UPGRADE);
            IntentDataHelper.setCheckType(intent, initiative);
            getCurrentActivity().startService(intent);
        });
    }

    /**
     * 启动到应用商店app详情界面
     *
     * @param marketPkg 应用商店包名 ,如果为""则由系统弹出应用商店列表供用户选择,否则调转到目标市场的应用详情界面，某些应用商店可能会失败
     */
    @ReactMethod
    public void launchAppDetail(String marketPkg) {
        try {
            Uri uri = Uri.parse("market://details?id=" + getCurrentActivity().getPackageName());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (!TextUtils.isEmpty(marketPkg)) {
                intent.setPackage(marketPkg);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getCurrentActivity().startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ReactMethod
    public void setLocation(String location) {
        Utils.runSafely(() -> {
            GlobalUserState.getGlobalState().setUserLocation(location);
        });
    }

    @ReactMethod
    public void appVersion(Callback successCallback) {
        Utils.runSafely(() -> {
            String appVersionName = AppUtil.getVersionName(getCurrentActivity());
            if (!TextUtils.isEmpty(appVersionName)) {
                successCallback.invoke(appVersionName);
            }
        });
    }

    @ReactMethod
    public void checkApkExist(String appName, Callback successCallback) {
        Utils.runSafely(() -> {
            if (TextUtils.isEmpty(appName)) {
                successCallback.invoke(CheckApkExist.checkFacebookExist(getCurrentActivity()));
            } else {
                successCallback.invoke(CheckApkExist.checkApkExist(getCurrentActivity(), appName));
            }
        });
    }

    @ReactMethod
    public void applyPermission() {
        UIActuator.postSafely(() -> {
            final Activity rnActivity = RNActivityManager.INSTANCE.GetRNActivity();
            if (rnActivity != null) {
                RxPermissions rx = new RxPermissions(rnActivity);
                // TODO: 2018/3/1 尝试取获取一次地理位置权限，如果有权限则进行更新一次地理位置
                rx.request(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                LocationToolKit.UpdateGlobalLocation(rnActivity);
                            } else {
                                //do nothing
                            }
                        });
                FloatPermissionManager.getInstance().applyFloatWindow(rnActivity);
            }
        });
    }

    @ReactMethod
    public void weChatCallback(String type, int errCode) {
        try {
            if (TextUtils.equals(SHARE_TO_WECHAN, type)) {
                String shareType = "";
                if (errCode == 0) {
                    SHARE_MEDIA shareMedia = ShareMedia.getShareMedia().getShareMediaType();
                    String category = ShareMedia.getShareMedia().getType();
                    if (shareMedia == SHARE_MEDIA.WEIXIN) {
                        Log.i(TAG, "weChatCallback: WEIXIN category:" + category);
                        EventBus.getDefault().post(new ShareWeChatEvent()); //回调成功的通知
                        if (TextUtils.equals(category, Constant.INVITE)) {
                            shareType = Constant.INVITE_TO_WEIXIN;
                        } else {
                            shareType = Constant.SHARE_TO_WEIXIN;
                        }
                    } else if (shareMedia == SHARE_MEDIA.WEIXIN_CIRCLE) {
                        Log.i(TAG, "weChatCallback: WEIXIN_CIRCLE category:" + category);
                        EventBus.getDefault().post(new ShareWeChatEvent()); //回调成功的通知
                        if (TextUtils.equals(category, Constant.INVITE)) {
                            shareType = Constant.INVITE_TO_WEIXIN_CIRCLE;
                        } else {
                            shareType = Constant.SHARE_TO_WEIXIN_CIRCLE;
                        }
                    }
                }
                String globalShareType = ProjectConfig.shareType;
                if (!TextUtils.isEmpty(globalShareType)) {
                    ApiManager.DoShareApi(shareType, 1, globalShareType);
                    ProjectConfig.shareType = "";
                } else {
                    ApiManager.DoShareApi(shareType, 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @ReactMethod
    public void permissionExit(Callback callback) {
        /**
         * 检查是否可以退出账号
         */
        if (FloatWindowCommander.IsFloatWindowOpen()) {
            /**
             * 当前有小窗模式
             */
            ToastToolKit.showLong(MessageUtils.getString(R.string.str_voice_can_not_exit_because_small_window));

            if (callback != null) {
                callback.invoke(-1);
            }
            return;
        }

        /**
         * 退出当前账号
         */
        GlobalService.destroyGlobalSocketEngine(getCurrentActivity());

        if (callback != null) {
            callback.invoke(0);
        }
    }

    @ReactMethod
    public void startLoadAv() {
        PLog.i("MainActivity", "startLoadAv:----- ");
        RxToolKit.Emit(new AdvEvent.AdvStartEvent());
    }

    @ReactMethod
    public void stopLoadAv() {
        PLog.i("MainActivity", "stopLoadAv:----- ");
        RxToolKit.Emit(new AdvEvent.AdvEndEvent());
    }

    @ReactMethod
    public void updateRequestUrl(String httpUrl, String audioHttpUrl) {
        Utils.runSafely(() -> {
            SharePreStoreUtils.save(
                    Constant.REQUEST_NAME,
                    getCurrentActivity(),
                    Constant.REQUEST_URL_KEY,
                    httpUrl
            );
            SharePreStoreUtils.save(
                    Constant.REQUEST_NAME,
                    getCurrentActivity(),
                    Constant.REQUEST_AUDIO_URL_KEY,
                    audioHttpUrl
            );
            Utils.exitApp();
        });
    }

    @ReactMethod
    public void clearAllNotification() {
        Utils.runSafely(() -> {
            NotificationUtils.IMPL().clearAllNotification(getCurrentActivity());
        });
    }

    /**
     * 打开商城
     */
    @ReactMethod
    public void openStore() {
        Utils.runSafely(() -> {
            WebViewActivity.loadStore(getCurrentActivity());
        });
    }

    /**
     * 打开我的道具
     */
    @ReactMethod
    public void openStoreWealth() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/wealth?access_token="
                    + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
        });
    }

    /**
     * 打开充值
     */
    @ReactMethod
    public void openStoreRecharge() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/recharge?access_token="
                    + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
        });
    }

    /**
     * 打开常见问题
     */
    @ReactMethod
    public void openPayFAQ() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/pay_faq" + TransportConfig.getUrlAttrs2());
        });
    }

    /**
     * 打开召集房间，与特色房
     */
    @ReactMethod
    public void openCallUp() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/recommend?access_token="
                    + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
        });
    }

    @ReactMethod
    public void sendMessageToNative(String json) {
        PLog.e(TAG, "" + json);
        if (mBridgeFilter == null && getCurrentActivity() != null) {
            mBridgeFilter = new UnifiedBridgeFilterImpl(getCurrentActivity());
        }
        if (mBridgeFilter != null) {
            UnifiedBridge.RegisterFilter(mBridgeFilter);
        }
        UIActuator.post(() -> {
            /**
             * 此处需要让RN传入一个JSON，根据解析的Json来执行不同的操作
             */
            Utils.runSafely(() -> {
                UnifiedBridge.EmitReceive(json);
            });
        });
    }

    public void unRegisterBridgeFilter() {
        if (mBridgeFilter != null) {
            UnifiedBridge.UnRegisterFilter(mBridgeFilter);
            mBridgeFilter.destroy();
            mBridgeFilter = null;
        }
    }

    /**
     * 回调方法
     *
     * @param json
     */
    public void sendMessageToReact(String json) {
        PLog.i("UnifiedBridge", "sendMessageToReact: " + json);
        WritableMap map = Arguments.createMap();
        map.putString(UnifiedConstant.PARAMS_DATA, json);
        sendEvent(UnifiedConstant.EVENT_REACT_NATIVE_BRIDGE, map);
    }


    /**
     * 打开排行榜
     */
    @ReactMethod
    public void openLeaderboard() {
        Utils.runSafely(() -> {
            WebViewActivity.load(getCurrentActivity(), URLManager.GetDefaultURL() + "/rank" + TransportConfig.getUrlAttrs2() + "&share_version=3");
        });
    }

    @ReactMethod
    public void getGiftInfo(String giftType, Callback callback) {
        /**
         * 获取礼物详细信息
         */
        if (!TextUtils.isEmpty(giftType)) {
            EffectsManager.FindGiftInfoForReactNativeAsync(giftType, callback);
        }
    }

    @ReactMethod
    public void openWebView(String url) {
        /**
         * 打开自定义web
         */
        if (!TextUtils.isEmpty(url)) {
            Utils.runSafely(() -> {
                String urlCharacter = "&";
                if (url.indexOf("?") == -1) {
                    urlCharacter = "?";
                }
                WebViewActivity.load(getCurrentActivity(), url + urlCharacter + "access_token="
                        + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1());
            });
        }
    }

    @ReactMethod
    public void openWebViewWithColor(String url, String color) {
        /**
         * 打开自定义web
         */
        if (!TextUtils.isEmpty(url)) {
            Utils.runSafely(() -> {
                String urlCharacter = "&";
                if (url.indexOf("?") == -1) {
                    urlCharacter = "?";
                }
                WebViewActivity.loadWithTitleColor(getCurrentActivity(), url + urlCharacter + "access_token="
                        + GlobalUserState.getGlobalState().getToken() + TransportConfig.getUrlAttrs1(), color);
            });
        }
    }

    @ReactMethod
    public void sendMessageToGlobalSocket(String type, String payload) {
        GlobalSocketEngine.INSTANCE.sendMessage(type, payload);
    }
}
