package orangelab.project.common.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.R;

/**
 * game_werewolf
 * 2017/5/19 下午12:00
 * Mystery
 */

public class LoadingDialog extends BaseCustomDialog {

    private TextView title;
    private TextView main;

    public LoadingDialog(Context context) {
        super(context, true);
        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_game_dialog_title, null);
        title = (TextView) titleView.findViewById(R.id.dialog_title);
        View mainlayout = LayoutInflater.from(context).inflate(R.layout.layout_loading_dialog, null);
        main = (TextView) mainlayout.findViewById(R.id.loading_data);
        addTitleView(titleView);
        addContentView(mainlayout);
        setButtonType(ONE_BUTTON);
        setButtonOnlyConfirmText("");
        setOneButtonEnable(false);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    public void setMessage(String msg) {
        if (main != null) {
            main.setText(msg);
        }
    }

    public void setTitle(String msg) {
        if (title != null) {
            title.setText(msg);
        }
    }
}
