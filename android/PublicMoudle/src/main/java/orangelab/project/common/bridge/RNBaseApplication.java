package orangelab.project.common.bridge;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.BuildConfig;
import orangelab.project.common.bridge.channel.GameReactPackage;
import orangelab.project.common.bridge.channel.NativeJSModule;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.service.IntviuApplication;

/**
 * Author:1727
 * 2017/3/2
 */

public class RNBaseApplication extends IntviuApplication implements ReactApplication {
    private final GameReactNativeHost mReactNativeHost = new GameReactNativeHost(this);

    public static class GameReactNativeHost extends ReactNativeHost {
        List<ReactPackage> packages = new ArrayList<>();

        protected GameReactNativeHost(Application application) {
            super(application);
        }

        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            packages.add(new MainReactPackage());
            packages.add(new GameReactPackage());
            return packages;
        }

        public GameReactPackage getGamePackage() {
            if (packages != null && packages.size() >= 1) {
                return (GameReactPackage) packages.get(1);
            } else {
                return null;
            }
        }
    }

    public NativeJSModule getNativeJSModule() {
        if (mReactNativeHost != null && mReactNativeHost.getGamePackage() != null) {
            return mReactNativeHost.getGamePackage().getNativeJSModule();
        }
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }
}
