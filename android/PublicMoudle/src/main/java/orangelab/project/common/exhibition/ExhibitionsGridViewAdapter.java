package orangelab.project.common.exhibition;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.utils.MessageUtils;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/6/21 下午1:06
 * Mystery
 */

public class ExhibitionsGridViewAdapter extends BaseAdapter {

    private Context context;
    private List<IExhibition> giftList;
    private List<ViewHolder> mHolders;
    private int curIndex;
    private int maxSize;

    private static final String DIM = "<img src=\"" + R.mipmap.icon_dim + "\"/>";
    final Html.ImageGetter imageGetter = new Html.ImageGetter() {

        public Drawable getDrawable(String source) {
            Drawable drawable = null;
            int rId = Integer.parseInt(source);
            drawable = context.getResources().getDrawable(rId);
            drawable.setBounds(0, 0, ScreenUtils.dip2px(10), ScreenUtils.dip2px(10));
            return drawable;
        }

    };

    public ExhibitionsGridViewAdapter(Context context, List<IExhibition> giftList, int curIndex, int maxSize) {
        this.context = context;
        this.giftList = giftList;
        this.curIndex = curIndex;
        this.maxSize = maxSize;
        mHolders = new ArrayList<>();
    }

    private int getCurIndexStartIndex() {
        return curIndex * maxSize;
    }

    @Override
    public int getCount() {
        return giftList.size() > (curIndex + 1) * maxSize ?
                maxSize : (giftList.size() - getCurIndexStartIndex());
    }

    @Override
    public IExhibition getItem(int position) {
        return giftList.get(position + getCurIndexStartIndex());
    }

    @Override
    public long getItemId(int position) {
        return position + curIndex * maxSize;
    }

    public void destroy() {
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IExhibition exhibition = getItem(position);
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(context, R.layout.fragment_gift_viewpage_item_item, null);
            holder.icon = (ImageView) convertView.findViewById(R.id.gift_image);
            holder.name = (TextView) convertView.findViewById(R.id.gift_name);
            holder.price = (TextView) convertView.findViewById(R.id.gift_price);
            holder.priceType = (ImageView) convertView.findViewById(R.id.gift_price_type);
            holder.popular = (TextView) convertView.findViewById(R.id.gift_popular);
            holder.disCount = (TextView) convertView.findViewById(R.id.gift_discount);
            holder.backgroud = convertView.findViewById(R.id.backgroud);
            mHolders.add(holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.bindExhibition(exhibition);
        holder.name.setText(exhibition.getExhibitionName());
        holder.price.setText(exhibition.getExhibitionPrice() + "");
        /**
         * 这里需要判断一下，如果是卡片 使用卡片的资源
         */
        EffectsManager.FillGiftImageView(exhibition.getRealType(), holder.icon);

        if (exhibition.getPopularValue() != 0) {
            holder.popular.setVisibility(View.VISIBLE);
            if (exhibition.getPopularValue() > 0) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_popular_add, exhibition.getPopularValue() + ""), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_popular_add, exhibition.getPopularValue() + "")));
                }
            } else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_popular_dec, Math.abs(exhibition.getPopularValue()) + ""), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_popular_dec, Math.abs(exhibition.getPopularValue()) + "")));
                }
            }
        } else {
            holder.popular.setVisibility(View.INVISIBLE);
        }

        if (position > 3) {
            holder.backgroud.setBackgroundResource(R.drawable.drawable_gift_list_2_item);
        } else {
            holder.backgroud.setBackgroundResource(R.drawable.drawable_gift_list_item);
        }

        if (ExhibitionsUtils.priceTypeIsGold(exhibition.getExhibitionPriceType())) {
            holder.priceType.setImageResource(R.mipmap.room_gift_icon_coin);
            holder.price.setTextColor(context.getResources().getColor(R.color.exhibition_price_text_color_coin));
        }

        if (ExhibitionsUtils.priceTypeIsDiamond(exhibition.getExhibitionPriceType())) {
            holder.priceType.setImageResource(R.mipmap.room_gift_icon_diamond);
            holder.price.setTextColor(context.getResources().getColor(R.color.exhibition_price_text_color_diamond));
        }

        if (TextUtils.isEmpty(exhibition.getDisCount())) {
            holder.disCount.setVisibility(View.INVISIBLE);
        } else {
            holder.disCount.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.disCount.setText(
                        Html.fromHtml(
                                MessageUtils.getString(R.string.string_gift_discount, DIM, exhibition.getDisCount()), Html.FROM_HTML_MODE_LEGACY, imageGetter, null));
            } else {
                holder.disCount.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_discount, DIM, exhibition.getDisCount()), imageGetter, null));
            }
        }

        if (TextUtils.equals(exhibition.getExhibitionType(), ExhibitionsConstant.EXHIBITIONS_TYPE_CARD)) {
            holder.priceType.setVisibility(View.INVISIBLE);
            holder.price.setVisibility(View.INVISIBLE);
            holder.name.setVisibility(View.VISIBLE);
            holder.popular.setVisibility(View.INVISIBLE);
            if (exhibition.getExhibitionCount() == ExhibitionsConstant.GIFT_COUNT_NONE) {
                holder.popular.setVisibility(View.INVISIBLE);
            } else {
                holder.popular.setVisibility(View.VISIBLE);
                String count = "";
                int realCount = exhibition.getExhibitionCount();
                holder.setNumber(realCount);
                if (realCount < 999) {
                    count = Integer.toString(exhibition.getExhibitionCount());
                } else {
                    count = MessageUtils.getString(R.string.gifts_record_item_number_max_string);
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_card_count, count), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_card_count, count)));
                }
            }
        } else {
            holder.priceType.setVisibility(View.VISIBLE);
            holder.price.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    public static class ViewHolder {
        private IExhibition bindExhibition;
        public ImageView icon;
        public TextView name;
        public ImageView priceType;
        public TextView price;
        public TextView popular;
        public View backgroud;
        public TextView disCount;

        public int number;

        public void bindExhibition(IExhibition iExhibition) {
            this.bindExhibition = iExhibition;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumber() {
            return number;
        }

        public void decNumber() {
            number--;
            if (bindExhibition != null) {
                bindExhibition.decCount();
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_card_count, Integer.toString(number)), Html.FROM_HTML_MODE_LEGACY));
            } else {
                popular.setText(Html.fromHtml(MessageUtils.getString(R.string.string_gift_item_card_count, Integer.toString(number))));
            }
        }
    }
}
