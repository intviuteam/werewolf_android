package orangelab.project.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.R;

/**
 * game_werewolf
 * 2017/7/3 下午2:57
 * Mystery
 */

public class TitleView extends RelativeLayout {

    private TextView mMainTitleText;
    private View mActionBack;
    private ImageView mRightContainer;

    public TitleView(Context context) {
        this(context, null);
    }

    public TitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.layout_title_view, this);
        mMainTitleText = (TextView) findViewById(R.id.title_view_main_title);
        mActionBack = findViewById(R.id.title_view_action_back);
        mRightContainer = (ImageView) findViewById(R.id.right_container);
    }

    public void initRight(int res, OnClickListener onClickListener) {
        mRightContainer.setImageResource(res);
        if (onClickListener != null) {
            mRightContainer.setOnClickListener(onClickListener);
        }
        mRightContainer.setVisibility(View.VISIBLE);
    }

    public void hideRight() {
        mRightContainer.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String str) {
        mMainTitleText.setText(str);
    }

    public void setTitle(int res) {
        mMainTitleText.setText(res);
    }

    public void setActionBack(OnClickListener view) {
        mActionBack.setOnClickListener(view);
    }
}
