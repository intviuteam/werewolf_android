package orangelab.project.common.effect.gifts;

import com.toolkit.action.Keepable;

import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/11/6 下午5:53
 * Mystery
 */

public class CustomGiftAnim implements Keepable {

    public static final int DEFAULT_WIDTH = ScreenUtils.dip2px(75);   //Android 这里默认的图片宽度
    public static final int DEFAULT_HEIGHT = ScreenUtils.dip2px(75);  //Android 这里默认的图片高度

    public static final int DEFAULT_PRIASE_WIDTH = ScreenUtils.dip2px(36);
    public static final int DEFAULT_PRIASE_HEIGHT = ScreenUtils.dip2px(36);

    //以下为结构需要
    public String giftType;    //礼物类型的唯一表示
    public String type;        //礼物动画的type

    public CustomAnimFrame defaultAndroidFrame;  //Android用于通过String展示一个默认的图片
    public List<CustomGiftAnimPoint> point;  //动画点
    public List<CustomGiftAnimPath> path;    //动画路径

    /**
     * 点的动画
     */
    public static class CustomGiftAnimPoint implements Keepable {
        public String location;
        public float scale = 1f;

        public CustomAnim anim;
        /**
         * 残留物特效
         */
        public CustomRemainAnim remainAnim = null;
        public int startOffSetX = 0;
        public int startOffSetY = 0;
        public int endOffSetX = 0;
        public int endOffSetY = 0;

    }

    public static class CustomRemainAnim extends CustomAnim implements Keepable {
        public float scale = 1f;
    }

    /**
     * 路径动画
     */
    public static class CustomGiftAnimPath implements Keepable {

        public String from;
        public String to;
        public String interpolator;

        public float fromScale = 1f;
        public float toScale = 1f;

        public long duration;

        public boolean supportRotate = false;//是否支持随路径旋转
        public float speed = 0;  //路径的速度 用于计算路径的时间，并是不真正意义上的速度  单位为 px/ms

        public CustomAnim anim;
    }

    public static class CustomAnim implements Keepable {
        public List<CustomAnimFrame> imageAndroid;
        public long duration = 0;
        public int frameTime = 60;
        public boolean needRepeat = false;
    }

    public static class CustomAnimFrame implements Keepable {
        public String local;
        public String remote;
    }

}
