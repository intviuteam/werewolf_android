package orangelab.project.common.effect.msgdecor;

import android.text.TextUtils;

import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2018/7/12 下午7:52
 * Mystery
 */
public class MsgDecorManiFest implements Keepable {

    public int rev = 0;
    public List<MsgDecorManiFestItem> message_boxs;

    public MsgDecorManiFestItem find(String type) {
        if (message_boxs == null) {
            return null;
        }
        for (MsgDecorManiFestItem temp : message_boxs) {
            if (TextUtils.equals(temp.message_box_type, type)) {
                return temp;
            }
        }
        return null;
    }

    public static class MsgDecorManiFestItem implements Keepable {
        public String message_box_type;
        public MsgDecorManiFestItemResource animate;
    }

    public static class MsgDecorManiFestItemResource implements Keepable {
        public String remote;
        public String local;
        public String textColor;
        public byte[] chunk;
    }
}
