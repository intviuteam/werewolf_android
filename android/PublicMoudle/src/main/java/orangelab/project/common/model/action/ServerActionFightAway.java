package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

import orangelab.project.common.model.DataWithAlives;

/**
 * game_werewolf
 * 2018/4/24 上午11:27
 * Mystery
 */

public class ServerActionFightAway extends DataWithAlives implements Keepable {

    public long duration;
}
