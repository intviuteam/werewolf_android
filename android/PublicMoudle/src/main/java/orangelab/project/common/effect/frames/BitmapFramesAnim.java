package orangelab.project.common.effect.frames;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import java.lang.ref.SoftReference;
import java.util.List;

/**
 * game_werewolf
 * 2018/6/23 上午11:33
 * Mystery
 */
public abstract class BitmapFramesAnim<CustomFrame> implements Runnable {

    private static final String TAG = "BitmapFramesAnim";

    private SoftReference<ImageView> mSoftReferenceImageView;
    private Handler mUIHandler;

    private Bitmap mBitmap;

    /**
     * 帧数据详情
     */
    private List<CustomFrame> mCustomFrames;

    /**
     * 当前帧下标
     */
    private int mIndex = -1;
    /**
     * 上一次的帧下标
     */
    private int mLastIndex = -1;

    /**
     * 帧间间隔
     */
    private long mInterval = 0;

    /**
     * 是否重复
     */
    private boolean mIsRepeat = false;
    private boolean mShouldRun = false;
    private boolean mPause = false;
    private boolean mSingleFrames = false;

    private BitmapFramesAnimListener mListener;

    public BitmapFramesAnim(ImageView imageView) {
        mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
        mUIHandler = new Handler();
    }

    public void setListener(BitmapFramesAnimListener listener) {
        mListener = listener;
    }

    public void setFrames(List<CustomFrame> customFrames) {
        this.mCustomFrames = customFrames;
        if (mCustomFrames != null && mCustomFrames.size() == 1) {
            mSingleFrames = true;
        }
    }

    public void setRepeat(boolean repeat) {
        mIsRepeat = repeat;
    }

    public void setInterval(long interval) {
        mInterval = interval;
    }

    public boolean isRunning() {
        return mShouldRun;
    }

    public synchronized void start() {
        if (mShouldRun) {
            return;
        }
        mShouldRun = true;
        /**
         * 开始执行
         */
        mUIHandler.post(this);
        Log.i(TAG, "start: ");
        if (mListener != null) {
            mListener.onStart();
        }
    }

    public synchronized void stop() {
        mShouldRun = false;
        Log.i(TAG, "stop: ");
    }

    public synchronized void pause() {
        mPause = true;
    }

    public synchronized void resume() {
        mPause = false;
        if (mShouldRun) {
            mUIHandler.removeCallbacks(this);
            mUIHandler.post(this);
        }
    }

    public synchronized void release() {
        Log.i(TAG, "release: ");
        stop();
        /**
         * 动画在没有循环的情况下结束了
         */
        if (mListener != null) {
            mListener.onRelease();
        }
        mBitmap = null;
        mListener = null;
        mUIHandler.removeCallbacksAndMessages(null);
        mCustomFrames.clear();
    }

    protected abstract Bitmap beforeFrameToBitmap(CustomFrame customFrame);

    protected abstract Bitmap onFrameToBitmap(Context context, CustomFrame customFrame);

    protected abstract Bitmap afterFrameToBitmap(CustomFrame customFrame, Bitmap bitmap);

    @Override
    public void run() {
        ImageView imageView = mSoftReferenceImageView.get();
        if (imageView == null) {
            release();
            return;
        }
        if (!mShouldRun) {
            release();
            return;
        }

        if (mPause) {
            /**
             * 已经暂停
             */
            return;
        }

        /**
         * 这里无论如何都要拿出来一帧，根据图片是否展示来判断是否要生成Bitmap
         */
        mLastIndex = mIndex;
        CustomFrame customFrame = getNext();
        if (customFrame == null) {
            if (mListener != null) {
                mListener.onStop();
            }
            release();
            return;
        }

        boolean filter = false;

        if (mLastIndex == mIndex && mIndex != -1) {
            /**
             * 如果上次绘制的下标和本次的相同则跳过Bitmap Transform阶段
             */
            Log.i(TAG, "use bitmap same index");
            drawBitmap(imageView, mBitmap);
            filter = true;
        }
        Log.i(TAG, "use bitmap diff index");
        if (!filter) {
            if (imageView.isShown()) {
                /**
                 * 图片在展示
                 */
                mBitmap = beforeFrameToBitmap(customFrame);
                if (mBitmap != null) {
                    Log.i(TAG, "use bitmap from beforeFrameToBitmap");
                    drawBitmap(imageView, mBitmap);
                } else {
                    BitmapAsyncTask task = new BitmapAsyncTask(imageView, customFrame);
                    task.start();
                }
            } else {
                /**
                 * 图片不在展示
                 *
                 * do nothing
                 */
            }
        }
        /**
         * 进行下一帧的执行,如果只有一张图片就不进行下一次的执行
         */
        if (!mSingleFrames) {
            mUIHandler.postDelayed(this, mInterval);
        }
    }

    private void drawBitmap(final ImageView imageView, final Bitmap bitmap) {
        mUIHandler.post(() -> {
            if (imageView != null && bitmap != null) {
                imageView.setImageBitmap(bitmap);
                Log.i(TAG, "drawBitmap: " + bitmap.getByteCount());
            }
        });
    }

    private void drawBitmap(final ImageView imageView, final Bitmap bitmap, final CustomFrame frame) {
        mUIHandler.post(() -> {
            if (imageView != null && bitmap != null && frame != null) {
                imageView.setImageBitmap(afterFrameToBitmap(frame, bitmap));
                Log.i(TAG, "drawBitmap: " + bitmap.getByteCount());
            }
        });
    }

    private CustomFrame getNext() {
        if (mCustomFrames == null) {
            throw new NullPointerException("frames is null");
        }
        mIndex++;
        if (mIndex >= mCustomFrames.size()) {
            if (mIsRepeat) {
                mIndex = 0;
            } else {
                return null;
            }
        }
        return mCustomFrames.get(mIndex);
    }

    private class BitmapAsyncTask extends AsyncTask<CustomFrame, Void, Bitmap> {

        private ImageView mImageView;
        private CustomFrame mCustomFrame;

        public BitmapAsyncTask(ImageView imageView, CustomFrame customFrame) {
            mImageView = imageView;
            mCustomFrame = customFrame;
        }

        public void start() {
            execute(mCustomFrame);
        }

        @Override
        protected Bitmap doInBackground(CustomFrame... customFrames) {
            CustomFrame customFrame = customFrames[0];
            if (customFrame == null) {
                return null;
            }
            mBitmap = onFrameToBitmap(mImageView.getContext(), customFrame);
            if (mBitmap != null) {
                Log.i(TAG, "use bitmap onFrameToBitmap");
            }

            return mBitmap;
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            drawBitmap(mImageView, bitmap, mCustomFrame);
        }
    }


    public interface BitmapFramesAnimListener {
        /**
         * 开始
         */
        void onStart();

        /**
         * 结束
         */
        void onStop();

        /**
         * 释放
         */
        void onRelease();
    }
}
