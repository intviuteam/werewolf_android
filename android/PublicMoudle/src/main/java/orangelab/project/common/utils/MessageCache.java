package orangelab.project.common.utils;

import java.util.ArrayList;
import java.util.Objects;

public class MessageCache<T> {

    private ArrayList<T> cache;
    private Object cacheLock = new Object();

    public MessageCache() {
        cache = new ArrayList<>();
    }

    public T pop() {
        T result = null;
        synchronized (cacheLock) {
            result = cache.remove(0);
        }
        return result;
    }

    public void push(T message) {
        synchronized (cacheLock) {
            cache.add(message);
        }
    }

    public boolean isEmpty() {
        boolean result = true;
        synchronized (cacheLock) {
            result = cache.isEmpty();
        }
        return result;
    }

    public void clear() {
        synchronized (cacheLock) {
            cache.clear();
        }
    }

    public int size() {
        int result = 0;
        synchronized (cacheLock) {
            result = cache.size();
        }
        return result;
    }
}
