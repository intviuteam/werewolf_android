package orangelab.project.common.effect.manifest;

import android.content.Context;
import android.text.TextUtils;

import com.datasource.cache.ACache;
import com.datasource.cache.DiskLruCacheManager;

import org.jetbrains.annotations.NotNull;

/**
 * game_werewolf
 * 2018/7/12 下午8:23
 * Mystery
 */
public class ManifestManagerHelper {

    private ACache mCache;

    public ManifestManagerHelper(@NotNull ACache cache) {
        mCache = cache;
    }

    public void createDiskLruCache(Context context, String type) {
        DiskLruCacheManager.aCache.createDiskLruCache(context, type);
    }

    public int getVersionFromCache(String fileName) {
        String version = mCache.getAsString(fileName);
        if (TextUtils.isEmpty(version)) {
            return 0;
        } else {
            try {
                int result = Integer.parseInt(version);
                return result;
            } catch (Exception e) {
                return 0;
            }
        }
    }

    public void putVersionIntoCache(String fileName, int value) {
        mCache.put(fileName, Integer.toString(value));
    }

    public void remove(String fileName) {
        mCache.remove(fileName);
    }

    public void putString(String fileName, String data) {
        mCache.put(fileName, data);
    }

    public String getAsString(String fileName) {
        return mCache.getAsString(fileName);
    }

}
