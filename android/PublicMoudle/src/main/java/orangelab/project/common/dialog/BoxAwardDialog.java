package orangelab.project.common.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.R;

import orangelab.project.common.model.AwardInfo;
import orangelab.project.common.model.FortuneResult;
import orangelab.project.common.engine.RoomSocketEngineHelper;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import com.androidtoolkit.PicassoUtils;
import com.networktoolkit.transport.Constant;

import java.util.ArrayList;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by wangxu on 2017/8/19.
 * 宝箱领取奖励dialog
 */

public class BoxAwardDialog extends SafeDialog implements View.OnClickListener {
    private Context mContext;
    private LinearLayout adView;
    private Button mConfirmBt;
    private TextView mAwardView;
    private TextView mAwardTitle;

    public BoxAwardDialog(Context context) {
        super(context, R.style.DarkDialogAllowDismiss);
        setContentView(R.layout.layout_box_award);
    }

    public BoxAwardDialog(Context context, boolean adLoaded, ArrayList awardData) {
        super(context, R.style.DarkDialogAllowDismiss);
        mContext = context;
        setContentView(R.layout.layout_box_award);
        mAwardView = (TextView) findViewById(R.id.award_message);
        mAwardView.setVisibility(View.VISIBLE);
        initAwardView(awardData);
        initNativeAd(adLoaded);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    public BoxAwardDialog(Context context, boolean adLoaded, FortuneResult fortuneResult) {
        super(context, R.style.DarkDialogAllowDismiss);
        mContext = context;
        setContentView(R.layout.layout_box_award);
        mAwardTitle = (TextView) findViewById(R.id.task_title);
        initJqueryrotateView(fortuneResult);
        initNativeAd(adLoaded);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    private void initJqueryrotateView(FortuneResult fortuneResult) {
        Utils.runSafely(() -> {
            if (fortuneResult != null) {
                LinearLayout awContainer = (LinearLayout) findViewById(R.id.main_award_view);
                awContainer.setOrientation(LinearLayout.VERTICAL);
                awContainer.removeAllViews();
                ImageView awardIcon = new ImageView(mContext);
                awardIcon.setLayoutParams(new LinearLayout.LayoutParams(ScreenUtils.dip2px(40), ScreenUtils.dip2px(40)));  //设置图片宽高
                awardIcon.setScaleType(ImageView.ScaleType.FIT_CENTER);
                TextView awardMessage = new TextView(mContext);
                awardMessage.setPadding(10, 20, 10, 15);
                awardMessage.setGravity(Gravity.CENTER);
                awardMessage.setTextSize(14f);
                awContainer.addView(awardIcon);
                awContainer.addView(awardMessage);
                if (fortuneResult.result == 1) {
                    mAwardTitle.setText(MessageUtils.getString(R.string.award_title));
                    awardMessage.setText(MessageUtils.getString(R.string.award_content, fortuneResult.gift.txt));
                    if (TextUtils.equals(fortuneResult.gift.type, "gold")) {
                        awardIcon.setImageResource(R.mipmap.icon_coin);

                    } else if (TextUtils.equals(fortuneResult.gift.type, "dim")) {
                        awardIcon.setImageResource(R.mipmap.icon_dim);
                    } else {
                        PicassoUtils.loadImage(mContext, fortuneResult.gift.img, ScreenUtils.dip2px(35), ScreenUtils.dip2px(45), awardIcon, R.mipmap.ic_default_gift);
                    }
                } else {
                    mAwardTitle.setText(MessageUtils.getString(R.string.no_award_title));
                    awardMessage.setText(MessageUtils.getString(R.string.no_award_message));
                    awardIcon.setImageResource(R.mipmap.icon_sorry);
                }

            }
        });
    }

    private void initAwardView(ArrayList<AwardInfo> awardData) {
        Utils.runSafely(() -> {
            if (awardData != null) {
                // Add ad into the award container.
                LinearLayout awContainer = (LinearLayout) findViewById(R.id.main_award_view);
                awContainer.setOrientation(LinearLayout.HORIZONTAL);
                awContainer.removeAllViews();
                for (int i = 0; i < awardData.size(); i++) {
                    ImageView awardIcon = new ImageView(mContext);
                    awardIcon.setLayoutParams(new LinearLayout.LayoutParams(ScreenUtils.dip2px(25), ScreenUtils.dip2px(25)));  //设置图片宽高
                    awardIcon.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    awContainer.addView(awardIcon);
                    AwardInfo awardInfo = awardData.get(i);
                    if (awardInfo != null) {
                        if (awardInfo.count > 1) {
                            TextView awardCount = new TextView(mContext);
                            awardCount.setPadding(10, 0, 10, 0);
                            awardCount.setTextSize(16f);
                            awardCount.setText(awardInfo.count + "");
                            awContainer.addView(awardCount);
                        }
                        if (TextUtils.equals(awardInfo.type, "gold")) {
                            awardIcon.setImageResource(R.mipmap.icon_coin);
                        } else if (TextUtils.equals(awardInfo.type, "dim")) {
                            awardIcon.setImageResource(R.mipmap.icon_dim);
                        } else {
                            awardIcon.setLayoutParams(new LinearLayout.LayoutParams(ScreenUtils.dip2px(25), ScreenUtils.dip2px(32)));  //设置图片宽高
                            PicassoUtils.loadImage(mContext, awardInfo.img, ScreenUtils.dip2px(25), ScreenUtils.dip2px(32), awardIcon, R.mipmap.ic_default_gift);
                        }
                    }
                    if (awardData.size() > 1 && i < awardData.size() - 1) {
                        TextView addIcon = new TextView(mContext);
                        addIcon.setPadding(10, 0, 10, 0);
                        addIcon.setTextSize(14f);
                        addIcon.setText("+");
                        awContainer.addView(addIcon);
                    }
                }

            }
        });
    }

    private void initNativeAd(boolean adLoaded) {
        mConfirmBt = (Button) findViewById(R.id.confirm);
        mConfirmBt.setOnClickListener(this);
    }

    public void showBoxAwardDialog() {
        if (!isShowing())
            show();
        //播放领取金币音效
        RoomSocketEngineHelper.playMedia(mContext, Constant.MEDIA_RECEIVE_GOLD, 0);
    }

    public void dismissBoxAwardDialog() {
        if (isShowing()) {
            removeAdView();
            dismiss();
        }
    }

    public void removeAdView() {
        if (adView != null) {
            LinearLayout adContainer = (LinearLayout) findViewById(R.id.ad_container);
            adContainer.removeView(adView);
            adView = null;
        }
    }

    @Override
    public void onClick(View v) {
        dismissBoxAwardDialog();
    }

    @Override
    protected void release() {

    }
}
