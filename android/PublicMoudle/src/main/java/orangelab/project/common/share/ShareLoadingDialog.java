package orangelab.project.common.share;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.R;
import orangelab.project.common.dialog.EnterLoadingDialog;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2018/2/27 下午2:35
 * Mystery
 */

public class ShareLoadingDialog extends EnterLoadingDialog {
    public ShareLoadingDialog(@NonNull Context context) {
        super(context);
        TextView textView = (TextView) findViewById(R.id.dialog_text);
        textView.setText(MessageUtils.getString(R.string.string_share_image_loading));
    }
}
