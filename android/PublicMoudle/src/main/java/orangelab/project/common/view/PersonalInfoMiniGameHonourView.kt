package orangelab.project.common.view

import android.content.Context
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.Environment
import com.toolkit.ToolKit
import com.toolkit.action.Destroyable
import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.utils.MessageUtils

/**
 *game_werewolf
 *2018/5/30 下午2:23
 *Mystery
 *
 * 小游戏荣耀奖励
 */
class PersonalInfoMiniGameHonourView : Destroyable {

    private var mRootView: View
    private var mUserInfoResult: UserInfoResult? = null

    private val mGridView: GridView
    private val mAdapter: Adapter

    fun getRankIcon(rank: Int): Int {
        when {
            rank in 1 until 11 -> {
                return R.mipmap.ico_mini_game_rank_1
            }
            rank in 11 until 31 -> {
                return R.mipmap.ico_mini_game_rank_2
            }
            rank in 31 until 101 -> {
                return R.mipmap.ico_mini_game_rank_3
            }
            else -> {
                return R.mipmap.ico_mini_game_rank_3
            }
        }
    }

    fun getGameName(gameType: String?, list: List<UserInfoResult.MiniGameDatasPopItem>): String {
        gameType ?: return ""

        list.forEach {
            if (TextUtils.equals(it.gameType, gameType)) {
                return it.name
            }
        }
        return ""
    }

    fun getRankLevelTextColor(rank: Int): Int {
        when {
            rank in 1 until 11 -> {
                return Environment.GetAppContext().resources.getColor(R.color.mini_game_rank1_text_color)
            }
            rank in 11 until 31 -> {
                return Environment.GetAppContext().resources.getColor(R.color.mini_game_rank2_text_color)
            }
            rank in 31 until 101 -> {
                return Environment.GetAppContext().resources.getColor(R.color.mini_game_rank3_text_color)
            }
            else -> {
                return Environment.GetAppContext().resources.getColor(R.color.mini_game_rank3_text_color)
            }
        }
    }

    inline fun getRankString(rank: Int): String {
        return MessageUtils.getString(R.string.string_mini_game_honour_rank, rank.toString())
    }

    constructor(mRootView: View) {
        this.mRootView = mRootView
        this.mGridView = mRootView.findViewById(R.id.mini_game_honour_gridview) as GridView
        this.mAdapter = Adapter(mRootView.context)
        this.mGridView.adapter = mAdapter
        this.mRootView.visibility = View.GONE
    }

    fun updateData(userInfoResult: UserInfoResult?) {
        mUserInfoResult = userInfoResult
        mUserInfoResult?.let {
            /**
             * 处理数据
             */
            val mini_game_datas = it.mini_game_datas
            mini_game_datas ?: return@let

            val mini_game_rank = it.mini_game_datas.rank
            mini_game_rank ?: return@let

            if (ToolKit.isEmpty(mini_game_datas.pop)) {
                return@let
            }

            if (ToolKit.isEmpty(mini_game_rank.all)) {
                return@let
            }


            val miniGameHonourData = ArrayList<ViewHolderData>()

            mini_game_rank.all.forEach {
                /**
                 * 遍历数据
                 */
                try {

                    val holderData = ViewHolderData()
                    val gameType = it.gameType

                    holderData.level = it.rank + 1
                    holderData.icon = getRankIcon(holderData.level)
                    holderData.rank = getRankString(holderData.level)
                    holderData.textColor = getRankLevelTextColor(holderData.level)
                    holderData.name = getGameName(gameType, mini_game_datas.pop)

                    miniGameHonourData.add(holderData)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (ToolKit.isEmpty(miniGameHonourData)) {
                    return@let
                } else {
                    mRootView.visibility = View.VISIBLE
                    mAdapter.updateData(miniGameHonourData)
                }

            }
            return@let
        }
    }

    override fun destroy() {

    }

    class Adapter : BaseAdapter {

        private var mContext: Context
        private var mData: List<ViewHolderData>? = null

        constructor(mContext: Context) : super() {
            this.mContext = mContext
        }

        fun updateData(data: List<ViewHolderData>?) {
            mData = data
            mData ?: return
            notifyDataSetChanged()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            var view = convertView
            var holder: ViewHolder? = null
            if (view == null) {
                view = View.inflate(mContext, R.layout.layout_personal_mini_game_honour_item, null)
                holder = ViewHolder()
                holder!!.icon = view!!.findViewById(R.id.id_icon) as ImageView
                holder!!.level = view!!.findViewById(R.id.id_level) as TextView
                holder!!.name = view!!.findViewById(R.id.id_name) as TextView
                holder!!.rank = view!!.findViewById(R.id.id_rank) as TextView
                view!!.setTag(holder)
            } else {
                holder = view.getTag() as ViewHolder
            }

            val data = getItem(position)

            data?.let {
                holder!!.icon?.setImageResource(it.icon)
                holder!!.level?.setText(it.level.toString())
                holder!!.name?.setText(it.name)
                holder!!.rank?.setText(it.rank)
                holder!!.level?.setTextColor(it.textColor)
                return view
            }

            return view
        }

        override fun getItem(position: Int): ViewHolderData? {
            return mData?.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mData?.size ?: 0
        }

    }

    class ViewHolder {
        var icon: ImageView? = null
        var level: TextView? = null
        var name: TextView? = null
        var rank: TextView? = null
    }

    class ViewHolderData {
        var icon: Int = 0
        var textColor: Int = 0
        var level: Int = -1
        var name: String? = null
        var rank: String? = null
    }

}