package orangelab.project.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import orangelab.project.common.PersonalDataHelper;

import com.R;
import com.datasource.GlobalUserState;

import orangelab.project.common.model.FriendsResult;
import orangelab.project.common.union.UnifiedBridge;
import orangelab.project.common.union.UnifiedConstant;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.UserHeadView;
import orangelab.project.common.utils.LeanCloudChatHelper;


import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;
import orangelab.thirdparty.leancloud.chatkit.event.FinishConversationEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * game_werewolf
 * 2017/5/18 上午11:53
 * Mystery
 */

public class IntviuFriendDialog extends BaseCustomDialog {

    private static final String TAG = "IntviuFriendDialog";

    private static final String url = "/friend/list";

    private ListView listView;
    private TextView placeHolder;
    private View placeHolderView;
    private IntviuFriendAdapter adapter;
    private Activity activity;

    public IntviuFriendDialog(Activity activity) {
        super(activity, true);
        this.activity = activity;
        View titleView = LayoutInflater.from(activity).inflate(R.layout.layout_game_dialog_title, null);
        TextView titleText = (TextView) titleView.findViewById(R.id.dialog_title);
        titleText.setText(MessageUtils.getString(R.string.intviu_friend_title));
        addTitleView(titleView);
        View view = LayoutInflater.from(activity).inflate(R.layout.layout_intviufriend, null);
        placeHolder = (TextView) view.findViewById(R.id.intviufriend_placeholder);
        placeHolderView = view.findViewById(R.id.intviufriend_placeholder_iv);
        placeHolder.setVisibility(View.VISIBLE);
        listView = (ListView) view.findViewById(R.id.intviu_friend);
        adapter = new IntviuFriendAdapter(activity);
        listView.setAdapter(adapter);
        listView.setDividerHeight(0);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            ViewHolder holder = (ViewHolder) view1.getTag();
            FriendItem item = adapter.getItem(position);
            if (item == null) {
                return;
            }
            if (item.hasCheck) {
                item.hasCheck = false;
                if (holder != null) {
                    holder.radio.setChecked(false);
                }
                adapter.removeSelected(item);
            } else {
                item.hasCheck = true;
                if (holder != null) {
                    holder.radio.setChecked(true);
                }
                Log.i("Lean", "add item " + item);
                adapter.addSelected(item);
            }
        });
        addContentView(view);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setButtonType(TWO_BUTTON);
        setConfirmString("邀请");
        setCancelString("取消");
        setConfirmListener(v -> {
            if (adapter == null) return;
            if (adapter.getCount() == 0) {
                Toast.makeText(activity, R.string.intviu_friend_select, Toast.LENGTH_SHORT).show();
                return;
            }
            if (adapter.getSelected().size() > 0) {
                Log.i("Lean", "IntviuFriendDialog: " + adapter.getSelected());
                LeanCloudChatHelper.sendIntviuToConversation(
                        GlobalUserState.getGlobalState().getCurRoomId(),
                        GlobalUserState.getGlobalState().getPassword(),
                        adapter.getSelected()
                );
                Toast.makeText(activity, R.string.intviu_friend_send, Toast.LENGTH_SHORT).show();
                dimissAndDestroy();
            } else {
                Toast.makeText(activity, R.string.intviu_friend_select, Toast.LENGTH_SHORT).show();
            }


        });
        setCancelListener(v -> {
            dimissAndDestroy();
        });
        UnifiedBridge.RegisterHandler(this, UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST, value -> {
            Utils.runSafely(() -> {
                FriendsResult result = GsonHelper.getGson().fromJson(value.getModel().params.toString(), FriendsResult.class);
                runSafely(() -> {
                    if (listView == null || adapter == null) {
                        showPlaceHolder();
                        return;
                    }
                    if (result != null && result.list != null && result.list.size() > 0) {
                        goneShowPlaceHolder();
                        if (adapter != null) {
                            adapter.updateDatas(result.list);
                        }
                    } else {
                        showPlaceHolder();
                    }
                });
            });
        });
        UnifiedBridge.Api().SendCommendIntoRN(UnifiedConstant.ACTION_REQUEST_FRIENDS_LIST, true);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FinishConversationEvent event) {
        dimissAndDestroy();
    }

    private void dimissAndDestroy() {
        onlyDismiss();
        destroy();
    }

    private void onlyDismiss() {
        dismiss();
    }

    private void destroy() {
        if (listView != null) {
            listView = null;
        }
        if (adapter != null) {
            adapter.destroy();
            adapter = null;
        }
        if (placeHolder != null) {
            placeHolder = null;
        }
        if (activity != null) {
            activity.finish();
            activity = null;
        }
        UnifiedBridge.UnRegisterHandler(this);
        EventBus.getDefault().unregister(this);
    }

    private void showPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.VISIBLE);
            placeHolder.setText(MessageUtils.getString(R.string.intviu_friend_empty));
            placeHolder.setVisibility(View.VISIBLE);
        }
        if (listView != null) {
            listView.setVisibility(View.GONE);
        }
    }

    private void goneShowPlaceHolder() {
        if (placeHolder != null && placeHolderView != null) {
            placeHolderView.setVisibility(View.GONE);
            placeHolder.setVisibility(View.GONE);
        }
        if (listView != null) {
            listView.setVisibility(View.VISIBLE);
        }
    }

    private static class IntviuFriendAdapter extends BaseAdapter {

        private Context context;
        private List<FriendItem> datas;
        private List<FriendItem> selected;

        public IntviuFriendAdapter(Context context) {
            this.context = context;
            datas = new ArrayList<>();
            selected = new ArrayList<>();
        }

        public void destroy() {
            datas.clear();
            context = null;
        }

        public List<FriendItem> getSelected() {
            return selected;
        }

        public void addSelected(FriendItem item) {
            if (selected.contains(item))
                return;
            selected.add(item);
        }

        public void removeSelected(FriendItem item) {
            selected.remove(item);
        }

        public void updateDatas(List<FriendItem> update) {
            if (update != null) {
                datas = update;
                notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public FriendItem getItem(int position) {
            return datas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (context == null) {
                return convertView;
            }
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.layout_intviufriend_item, null);
                holder.userhead = (UserHeadView) convertView.findViewById(R.id.intviufriend_ic);
                holder.level = (TextView) convertView.findViewById(R.id.intviufriend_level);
                holder.name = (TextView) convertView.findViewById(R.id.intviufriend_name);
                holder.radio = (RadioButton) convertView.findViewById(R.id.intviufriend_check);

                holder.littleOnline = (ImageView) convertView.findViewById(R.id.intviufriend_online);
                holder.littleRoom = (ImageView) convertView.findViewById(R.id.intviufriend_littleroom);
                holder.roomId = (TextView) convertView.findViewById(R.id.intviufriend_roomId);
                holder.status = (TextView) convertView.findViewById(R.id.intviufriend_status);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            FriendItem item = getItem(position);
            holder.radio.setFocusable(false);
            holder.radio.setClickable(false);
            if (item != null) {
                holder.userhead.setUserHeadImageUrl(item.image);
                holder.userhead.setUserSex(item.sex);
                holder.name.setText(item.name);
                holder.level.setText("Lv." + item.game.level);
                holder.radio.setVisibility(View.VISIBLE);
                item.game_status = item.turnToGameStatus();
                if (item.game_status == 1) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.INVISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_online_txt));
                } else if (item.game_status == 2) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_prepare));
                } else if (item.game_status == 3) {
                    holder.littleOnline.setVisibility(View.VISIBLE);
                    holder.littleRoom.setVisibility(View.VISIBLE);
                    holder.roomId.setVisibility(View.VISIBLE);
                    holder.status.setVisibility(View.VISIBLE);
                    holder.roomId.setText(item.room_id);
                    holder.status.setText(MessageUtils.getString(R.string.friend_item_status_gaming));
                } else {
                    holder.littleOnline.setVisibility(View.GONE);
                    holder.littleRoom.setVisibility(View.GONE);
                    holder.roomId.setVisibility(View.GONE);
                    holder.status.setVisibility(View.GONE);
                }

                holder.radio.setChecked(item.hasCheck);
            }
            return convertView;
        }
    }

    public static class FriendItem {
        public String id;
        public String name;
        public String image;
        public int sex;
        public int game_status;
        public String status;
        public String room_id;
        public FriendItemGame game;
        public boolean hasCheck = false;
        public long created_at;

        @Override
        public boolean equals(Object obj) {
            boolean result = false;
            if (obj == null) {
                result = false;
            }
            try {
                FriendItem other = (FriendItem) obj;
                if (other != null && TextUtils.equals(id, other.id)) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        public int turnToGameStatus() {
            if (TextUtils.isEmpty(status)) {
                return 1;
            }
            String[] strs = status.split(":");
            try {
                if (strs.length != 2) {
                    return 1;
                } else {
                    int result = Integer.parseInt(strs[0]);
                    return result;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        @Override
        public String toString() {
            return "FriendItem{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", image='" + image + '\'' +
                    ", sex=" + sex +
                    ", game_status=" + game_status +
                    ", room_id='" + room_id + '\'' +
                    ", game=" + game +
                    ", hasCheck=" + hasCheck +
                    ", created_at=" + created_at +
                    '}';
        }
    }

    public static class FriendItemGame {
        public int win;
        public int lose;
        public int level;
        public int experience;

        @Override
        public String toString() {
            return "FriendItemGame{" +
                    "win=" + win +
                    ", lose=" + lose +
                    ", level=" + level +
                    ", experience=" + experience +
                    '}';
        }
    }

    public static class ViewHolder {
        public UserHeadView userhead;
        public TextView name;
        public TextView level;
        public RadioButton radio;

        public ImageView littleOnline;
        public ImageView littleRoom;
        public TextView roomId;
        public TextView status;

    }


}
