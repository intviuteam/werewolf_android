package orangelab.project.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by wangxu on 2017/4/25.
 */

public class LeanCloudReceiver extends BroadcastReceiver {
    private static final String TAG = "LeanCloudReceiver";
    private static final String ACTION_PUSH = "cn.orangelab.werewlof.action.PUSH";
    private static final String AVOS_AVOSCLOUD_DATA = "com.avos.avoscloud.Data";
    private static final String AVOS_AVOSCLOUD_CHANNEL = "com.avos.avoscloud.Channel";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            if (action.equals(ACTION_PUSH)) {
                String channel = intent.getExtras().getString(AVOS_AVOSCLOUD_CHANNEL);
                //获取消息内容
                JSONObject json = new JSONObject(intent.getExtras().getString(AVOS_AVOSCLOUD_DATA));
                Iterator itr = json.keys();
                while (itr.hasNext()) {
                    String key = (String) itr.next();
                    Log.i(TAG, "..." + key + " => " + json.getString(key));
                }
            }

        } catch (Exception e) {
            Log.i(TAG, "JSONException: " + e.getMessage());
        }
    }
}
