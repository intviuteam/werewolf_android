package orangelab.project.common.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.androidtoolkit.ToastToolKit;

import orangelab.project.common.utils.ReportEventUtils;
import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2017/5/20 下午3:06
 * Mystery
 * <p>o
 * 一个安全的Activity
 */

public abstract class SafeActivity extends BasicActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private boolean isDestroy = false;
    private boolean activityWindowInitFinish = false;
    protected String mActivityKey = hashCode() + "";

    private SafeHandler mUIHandler = new SafeHandler(Looper.getMainLooper());

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!activityWindowInitFinish) {
            activityWindowInitFinish = true;
            onActivityWindowInitFinish();
        }
    }

    protected void closeActivity() {

    }

    @Override
    public void finish() {
        isDestroy = true;
        mUIHandler.release();
        super.finish();
    }

    public boolean isDestroy() {
        return isDestroy;
    }

    public void runOnUiThreadSafely(Runnable runnable) {
        runOnUiThreadSafely(runnable, 0);
    }

    public void runOnUiThreadSafely(Runnable runnable, long delay) {
        mUIHandler.postDelaySafely(runnable, delay);
    }

    public void removeCallBackSafely(Runnable runnable) {
        mUIHandler.removeCallbacks(runnable);
    }

    protected boolean isActivityWindowInitFinish() {
        return activityWindowInitFinish;
    }

    /**
     * Activity 初始化Window成功回调
     */
    protected abstract void onActivityWindowInitFinish();

    protected Handler getSaleHandler() {
        return mUIHandler;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReportEventUtils.reportActivityResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ReportEventUtils.reportActivityPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    protected void releaseMessage() {
        mUIHandler.release();
    }

    protected void showToast(String msg) {
        ToastToolKit.showShort(msg);
    }

    protected void showKeyBoard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void hideKeyBoard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        isDestroy = true;
        mUIHandler.release();
        super.onDestroy();
    }

}
