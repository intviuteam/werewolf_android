package orangelab.project.common.db;

import android.text.TextUtils;
import android.util.Log;

import com.toolkit.action.Destroyable;

import java.util.List;

import orangelab.project.common.db.entity.UserPayOrderEntity;
import orangelab.project.common.db.gen.UserPayOrderEntityDao;

/**
 * game_werewolf
 * 2018/6/19 下午5:37
 * Mystery
 */
public class UserPayOrderDaoV2Helper implements Destroyable {

    private static final String TAG = "UserPayOrderDaoV2Helper";
    private UserPayOrderEntityDao mUserPayDao;

    public UserPayOrderDaoV2Helper(UserPayOrderEntityDao userPayDao) {
        mUserPayDao = userPayDao;
    }

    public void insertData(UserPayOrderEntity entity) {
        Log.i(TAG, "insertData: ");
        if (mUserPayDao != null) {
            mUserPayDao.insert(entity);
        }
    }

    public void updateDataOtherData(String userId, String orderId, String otherData) {
        if (mUserPayDao != null) {
            UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                    UserPayOrderEntityDao.Properties.UserId.eq(userId),
                    UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
            ).build().unique();
            if (entity != null) {
                entity.setOtherData1(otherData);
                mUserPayDao.update(entity);
                Log.i(TAG, "updateData: update success");
            }
        }
    }

    public void updateData(String userId, String orderId, int state) {
        if (mUserPayDao != null) {
            UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                    UserPayOrderEntityDao.Properties.UserId.eq(userId),
                    UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
            ).build().unique();
            if (entity != null) {
                entity.setOrderState(state);
                mUserPayDao.update(entity);
                Log.i(TAG, "updateData: update success");
            }
        }
    }

    public void deleteData(String userId, String orderId) {
        if (TextUtils.isEmpty(orderId))
            return;
        Log.i(TAG, "deleteData: ");
        if (mUserPayDao != null) {
            UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(
                    UserPayOrderEntityDao.Properties.OrderId.eq(orderId)
            ).build().unique();
            if (entity != null) {
                mUserPayDao.delete(entity);
            }
        }
    }

    public List<UserPayOrderEntity> getAllUserOrderEntity() {
        Log.i(TAG, "getAllUserOrderEntity: ");
        if (mUserPayDao != null) {
            List<UserPayOrderEntity> entities = mUserPayDao.queryBuilder().build().list();
            return entities;
        }
        return null;
    }

    public UserPayOrderEntity getUserPayOrderByOrderId(String orderId) {
        if (TextUtils.isEmpty(orderId)) {
            return null;
        }
        if (mUserPayDao != null) {
            UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(UserPayOrderEntityDao.Properties.OrderId.eq(orderId)).build().unique();
            return entity;
        }
        return null;
    }

    public List<UserPayOrderEntity> getAllUserOrderEntity(String userId) {
        if (TextUtils.isEmpty(userId))
            return null;
        Log.i(TAG, "getAllUserOrderEntity: ");
        if (mUserPayDao != null) {
            List<UserPayOrderEntity> entities = mUserPayDao.queryBuilder().where(
                    UserPayOrderEntityDao.Properties.UserId.eq(userId)
            ).build().list();
            return entities;
        }
        return null;
    }

    public boolean containsOrderId(String orderId) {
        if (TextUtils.isEmpty(orderId)) return false;

        if (mUserPayDao != null) {
            try {
                UserPayOrderEntity entity = mUserPayDao.queryBuilder().where(UserPayOrderEntityDao.Properties.OrderId.eq(orderId)).build().unique();
                if (entity != null) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void destroy() {
        mUserPayDao = null;
    }
}
