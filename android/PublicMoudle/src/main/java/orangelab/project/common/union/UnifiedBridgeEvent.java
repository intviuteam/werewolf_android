package orangelab.project.common.union;

/**
 * game_werewolf
 * 2017/9/19 下午4:49
 * Mystery
 */

public class UnifiedBridgeEvent {
    private UnifiedBridgeModel mModel;
    private IUnifiedBridge mBridge;

    public UnifiedBridgeEvent(UnifiedBridgeModel model, IUnifiedBridge bridge) {
        mModel = model;
        mBridge = bridge;
    }

    public UnifiedBridgeModel getModel() {
        return mModel;
    }

    public IUnifiedBridge getBridge() {
        return mBridge;
    }

    @Override
    public String toString() {
        return "UnifiedBridgeEvent{" +
                "mModel=" + mModel +
                ", mBridge=" + mBridge +
                '}';
    }
}
