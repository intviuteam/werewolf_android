package orangelab.project.common.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import com.R
import com.androidtoolkit.PicassoUtils
import com.toolkit.ToolKit
import com.toolkit.action.Destroyable
import orangelab.project.common.model.UserInfoResult
import orangelab.project.common.utils.MessageUtils
import java.util.*

/**
 * game_werewolf
 * 2018/4/26 下午5:44
 * Mystery
 */

class PersonalInfoMiniGameRecordView : Destroyable {


    private val mContainer: View

    private var mDataIsEmpty = false

    private val mRootView: View
    private val mGridView: GridView
    private val mPlackHolder: View
    private val mSwitch: View
    private var mSwitchFlag = false

    private var mAnimator: Animator? = null
    private var mAdapter: Adapter? = null

    constructor(mRootView: View) {
        this.mRootView = mRootView

        this.mSwitch = mRootView.findViewById(R.id.mini_game_record_switch)
        this.mGridView = mRootView.findViewById(R.id.mini_game_record_gridview) as GridView
        this.mContainer = mRootView.findViewById(R.id.mini_game_record_container)
        mPlackHolder = mRootView.findViewById(R.id.mini_game_record_placeholder)
        this.mContainer.setOnClickListener {
            switchView()
        }
    }

    fun updateData(mMiniGameDatas: UserInfoResult.MiniGameDatas?) {
        if (mMiniGameDatas != null && !ToolKit.isEmpty(mMiniGameDatas.pop)) {
            if (mAdapter == null) {
                var data: List<UserInfoResult.MiniGameDatasPopItem> = mMiniGameDatas?.pop ?: ArrayList(0)
                Collections.sort(data)
                mAdapter = Adapter(data, mRootView.context)
                this.mGridView.adapter = mAdapter
            } else {
                var data: List<UserInfoResult.MiniGameDatasPopItem> = mMiniGameDatas?.pop ?: ArrayList(0)
                mAdapter?.update(data)
            }
            mDataIsEmpty = false
        } else {
            mDataIsEmpty = true
        }
        this.mContainer.visibility = View.VISIBLE
    }

    fun switchView() {
        val flag = mAnimator?.isRunning ?: false
        if (flag) {
            return
        }
        var left = 0f
        var right = 0f
        if (mSwitchFlag) {
            /**
             * 如果当前状态是打开，那么变为关闭
             */
            left = 90f
            right = -0f
        } else {
            /**
             * 如果当前状态是关闭，那么变为打开
             */
            left = 0f
            right = 90f
        }
        mAnimator = ObjectAnimator.ofFloat(mSwitch, View.ROTATION, left, right)
        mAnimator!!.setDuration(200)
        mAnimator!!.addListener(object : AnimatorListenerAdapter() {

            override fun onAnimationEnd(animation: Animator?) {
                mSwitchFlag = !mSwitchFlag
                if (mSwitchFlag) {

                    if (mDataIsEmpty) {
                        mPlackHolder.visibility = View.VISIBLE
                    } else {
                        mGridView.visibility = View.VISIBLE
                    }

                } else {
                    mGridView.visibility = View.GONE
                    mPlackHolder.visibility = View.GONE
                }
            }
        })
        mAnimator!!.start()
    }

    override fun destroy() {

    }

    class Adapter : BaseAdapter {
        private var mData: List<UserInfoResult.MiniGameDatasPopItem>
        private var mContext: Context

        constructor(mData: List<UserInfoResult.MiniGameDatasPopItem>, mContext: Context) : super() {
            this.mData = mData
            this.mContext = mContext
        }

        fun update(mData: List<UserInfoResult.MiniGameDatasPopItem>) {
            this.mData = mData
            notifyDataSetChanged()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            var holder: ViewHolder? = null
            if (view == null) {
                view = View.inflate(mContext, R.layout.layout_personal_mini_game_record_item, null)
                holder = ViewHolder()
                holder!!.icon = view!!.findViewById(R.id.id_icon) as ImageView
                holder!!.level = view!!.findViewById(R.id.id_level) as TextView
                holder!!.name = view!!.findViewById(R.id.id_name) as TextView
                holder!!.total = view!!.findViewById(R.id.total) as TextView
                holder!!.winRate = view!!.findViewById(R.id.win_rate) as TextView
                view!!.setTag(holder)
            } else {
                holder = view.getTag() as ViewHolder
            }

            val data = getItem(position)

            try {
                PicassoUtils.loadImage(mContext, data.icon, holder!!.icon!!, R.mipmap.ico_personal_info_mini_game_default)
                holder!!.name?.setText(data.name)
                holder!!.level?.setText("Lv.${data.level.`val`}")
                holder!!.total?.setText(MessageUtils.getString(R.string.string_mini_game_record_total, data.count.total))
                if (data.count.total == 0) {
                    holder!!.winRate?.setText(MessageUtils.getString(R.string.user_detail_win_rate, "0%"))
                } else {
                    holder!!.winRate?.setText(MessageUtils.getString(R.string.user_detail_win_rate, "${((data.count.win / data.count.total.toFloat()) * 100).toInt()}%"))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return view
        }

        override fun getItem(position: Int): UserInfoResult.MiniGameDatasPopItem {
            return mData.get(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return mData.size
        }
    }

    class ViewHolder {
        var icon: ImageView? = null
        var name: TextView? = null
        var level: TextView? = null
        var total: TextView? = null
        var winRate: TextView? = null
    }
}
