package orangelab.project.common.union;

import android.text.TextUtils;

import com.androidtoolkit.RxToolKit;

import orangelab.project.MainApplication;

import com.toolkit.action.Action;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.PLog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * game_werewolf
 * 2017/9/19 下午4:08
 * 统一桥处理 统一处理 RN&Native
 */

public class UnifiedBridge implements IUnifiedBridge {

    private static final String TAG = "UnifiedBridge";

    private static volatile UnifiedBridge mInstance;
    private List<UnifiedBridgeFilter> mBridgeFilters;

    private static UnifiedBridge Impl() {
        if (mInstance == null) {
            synchronized (UnifiedBridge.class) {
                if (mInstance == null) {
                    mInstance = new UnifiedBridge();
                }
            }
        }
        return mInstance;
    }

    public static IUnifiedBridge Api() {
        return Impl();
    }

    private UnifiedBridge() {
        mBridgeFilters = new ArrayList<>();
    }

    private void addFilter(UnifiedBridgeFilter filter) {
        if (filter != null && !mBridgeFilters.contains(filter)) {
            mBridgeFilters.add(filter);
        }
    }

    private void removeFilter(UnifiedBridgeFilter filter) {
        if (filter != null) {
            mBridgeFilters.remove(filter);
        }
    }

    private void sendMsg(UnifiedBridgeModel model) {
        RxToolKit.Emit(new UnifiedBridgeEvent(model, this));
    }

    private void filterMsg(String msg) {
        UnifiedBridgeModel model = UnifiedBridgeModel.TurnToUnifiedBridgeModel(msg);
        if (model != null && !TextUtils.isEmpty(model.action)) {
            for (UnifiedBridgeFilter filter : mBridgeFilters) {
                if (filter.filterAction(model.action, model.params, model.options, this)) {
                    return;
                }
            }
            sendMsg(model);
        }
    }

    private UnifiedBridgeModel createModel(String action, String type, JSONObject params, boolean sync, boolean needcallback, long timeout) {
        UnifiedBridgeModel model = new UnifiedBridgeModel();
        model.action = action;
        model.params = params;
        model.type = type;
        UnifiedBridgeModel.UnifiedBridgeModelOption option = new UnifiedBridgeModel.UnifiedBridgeModelOption();
        option.sync = sync;
        option.needcallback = needcallback;
        option.timeout = timeout;
        model.options = option;
        return model;
    }

    public static void RegisterHandler(Object observer, String action, Action<UnifiedBridgeEvent> handler, boolean autoDisposable) {
        if (observer == null) {
            return;
        }
        RegisterHandler(observer.getClass().getName(), action, handler, autoDisposable);
    }

    /**
     * @param action
     * @param handler
     * @param autoDisposable if disposable is true, when this action finish , this listener will be disposed .
     */
    public static void RegisterHandler(String observer, String action, Action<UnifiedBridgeEvent> handler, boolean autoDisposable) {
        PLog.i(TAG, "RegisterHandler " + action);
        RxToolKit.Build(observer, UnifiedBridgeEvent.class).actionFilter(
                unifiedBridgeEvent -> TextUtils.equals(action, unifiedBridgeEvent.getModel().action)
        ).action(unifiedBridgeEvent -> {
            try {
                if (unifiedBridgeEvent.getModel().options.sync) {
                    if (handler != null) {
                        handler.func(unifiedBridgeEvent);
                    }
                } else {
                    ThreadToolKit.Async(() -> {
                        if (handler != null) {
                            handler.func(unifiedBridgeEvent);
                        }
                    });
                }
                if (autoDisposable) {
                    UnRegisterHandler(observer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).register();
    }

    public static void RegisterFilter(UnifiedBridgeFilter filter) {
        Impl().addFilter(filter);
    }

    public static void UnRegisterFilter(UnifiedBridgeFilter filter) {
        Impl().removeFilter(filter);
    }

    public static void EmitReceive(String msg) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        Impl().filterMsg(msg);
    }

    public static void RegisterHandler(Object o, String action, Action<UnifiedBridgeEvent> handler) {
        if (o == null) {
            return;
        }
        RegisterHandler(o.getClass().getName(), action, handler);
    }

    public static void RegisterHandler(String observer, String action, Action<UnifiedBridgeEvent> handler) {
        RegisterHandler(observer, action, handler, true);
    }

    public static void UnRegisterHandler(Object o) {
        if (o == null) {
            return;
        }
        UnRegisterHandler(o.getClass().getName());
    }

    public static void UnRegisterHandler(String observer) {
        PLog.i(TAG, "UnRegisterHandler " + observer);
        RxToolKit.UnRegister(observer);
    }

    public void SendCommendIntoRN(String action) {
        SendCommendIntoRN(action, null);
    }

    @Override
    public void SendCommendIntoRN(String action, boolean needcallback) {
        SendCommendIntoRN(action, null, true, needcallback, 0);
    }

    public void SendCommendIntoRN(String action, JSONObject params) {
        SendCommendIntoRN(action, params, true, false, 0);
    }

    public void SendCommendIntoRN(String action, JSONObject params, boolean sync, boolean needcallback, long timeout) {
        UnifiedBridgeModel model = createModel(action, UnifiedConstant.RN_NATIVE, params, sync, needcallback, timeout);
        if (MainApplication.getInstance().getNativeJSModule() != null) {
            PLog.i(TAG, "sendMessageIntoRN:" + model.toString());
            MainApplication.getInstance().getNativeJSModule().sendMessageToReact(model.toJson());
        }
    }

    public void SendCommendIntoJS(String action) {
        SendCommendIntoJS(action, null);
    }

    @Override
    public void SendCommendIntoJS(String action, boolean needcallback) {
        SendCommendIntoJS(action, null, true, needcallback, 0);
    }

    public void SendCommendIntoJS(String action, JSONObject params) {
        SendCommendIntoJS(action, params, true, false, 0);
    }

    public void SendCommendIntoJS(String action, JSONObject params, boolean sync, boolean needcallback, long timeout) {
        UnifiedBridgeModel model = createModel(action, UnifiedConstant.JS_NATIVE, params, sync, needcallback, timeout);
        PLog.i(TAG, "sendMessageToJS: " + model.toString());
        RxToolKit.Emit(new UnifiedBridgeJSEvent(model.toJson()));
    }

}
