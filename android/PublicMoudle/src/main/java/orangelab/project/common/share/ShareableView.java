package orangelab.project.common.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

/**
 * game_werewolf
 * 2018/2/26 下午7:38
 * Mystery
 */

public abstract class ShareableView extends FrameLayout implements Shareable<Bitmap> {

    private static final String TAG = "ShareableView";

    private Bitmap mBitmap;

    public ShareableView(@NonNull Context context) {
        this(context, null);
    }

    public ShareableView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShareableView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void destroy() {
        removeAllViews();
        //3.0 之后 不需要 进行 recycle
        mBitmap = null;
        Log.i(TAG, "destroy: ");
    }

    @Override
    public void make(CallBack<Bitmap> callBack) {
        if (callBack == null) {
            return;
        }
        callBack.onStart();
        try {
            mBitmap = createBitmap();
            callBack.onSuccess(mBitmap);
        } catch (Exception e) {
            e.printStackTrace();// make sure you will notice this exception
            callBack.onError(e);
        }
    }

    public Bitmap createBitmap() {
        measure(getLayoutParams().width, getLayoutParams().height);
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        return bitmap;
    }
}
