package orangelab.project.common.share;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.androidtoolkit.ToastToolKit;
import com.R;

import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.ShareMedia;

import com.networktoolkit.transport.Constant;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import cn.intviu.support.ShareTools;
import rx.android.schedulers.AndroidSchedulers;

/**
 * game_werewolf
 * 2018/2/27 下午12:17
 * Mystery
 */

public class RankShareActivity extends SafeActivity implements ShareListener, Runnable {

    private static final String TAG = "RankShareActivity";


    private RxPermissions rxPermissions;
    private RankShareDialog mRankShareDialog;
    private ShareLoadingDialog mShareLoadingDialog;
    private ShareableView mShareableView;
    private RankSharePayload mRankSharePayload;


    public static void Launch(Context context, RankSharePayload payload) {
        if (payload == null) {
            return;
        }
        Intent intent = new Intent(context, RankShareActivity.class);
        intent.putExtra(Constant.DATA, payload);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRankSharePayload = getIntent().getParcelableExtra(Constant.DATA);
        rxPermissions = new RxPermissions(this);

        mShareLoadingDialog = new ShareLoadingDialog(this);

        mRankShareDialog = new RankShareDialog(this, mRankSharePayload);
        mRankShareDialog.setShareListener(this);
        mRankShareDialog.setDismissListener(this);
        mRankShareDialog.show();
    }

    @Override
    public void finish() {
        super.finish();
        rxPermissions = null;
        if (mShareableView != null) {
            mShareableView.destroy();
            mShareableView = null;
        }
        if (mShareLoadingDialog != null) {
            if (mShareLoadingDialog.isShowing()) {
                mShareLoadingDialog.dismiss();
            }
            mShareLoadingDialog = null;
        }
    }

    @Override
    public void onShareWeChat() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN);
        if (ShareTools.existsApp(this, ShareTools.SHARE_WEIXIN)) {
            share(SHARE_MEDIA.WEIXIN);
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
        }
    }

    @Override
    public void onShareQQ() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.QQ);
        if (ShareTools.existsApp(this, ShareTools.SHARE_QQ)) {
            if (rxPermissions != null) {
                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                share(SHARE_MEDIA.QQ);
                                orangelab.project.common.utils.ShareTools.shareSuccessReported(Constant.SHARE_TO_QQ);
                            } else {
                                ToastToolKit.showShort(MessageUtils.getString(R.string.permission_read_store_error));
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
        }
    }

    @Override
    public void onShareLine() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.LINE);
        if (ShareTools.existsApp(this, ShareTools.SHARE_LINE)) {
            if (rxPermissions != null) {
                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aBoolean -> {
                            if (aBoolean) {
                                share(SHARE_MEDIA.LINE);
                            } else {
                                ToastToolKit.showShort(MessageUtils.getString(R.string.permission_read_store_error));
                            }
                        });
            }
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
        }
    }

    @Override
    public void onShareFaceBook() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.FACEBOOK);
        if (ShareTools.existsApp(this, ShareTools.SHARE_FACEBOOK)) {
            share(SHARE_MEDIA.FACEBOOK);
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
        }
    }

    @Override
    public void onShareWeChatFriend() {
        ShareMedia.getShareMedia().setShareMediaType(SHARE_MEDIA.WEIXIN_CIRCLE);
        if (ShareTools.existsApp(this, ShareTools.SHARE_WEIXIN)) {
            share(SHARE_MEDIA.WEIXIN_CIRCLE);
        } else {
            ToastToolKit.showShort(MessageUtils.getString(R.string.share_error));
        }
    }

    @Override
    public void run() {
        finish();
    }

    private void share(SHARE_MEDIA share_media) {
        ShareMedia.getShareMedia().setType(Constant.SHARE);
        mShareableView = initShareableView(mRankSharePayload);
        if (mShareableView == null) {
            ToastToolKit.showShort(MessageUtils.getString(R.string.string_action_failed));
            return;
        }
        mShareableView.make(new CallBack<Bitmap>() {
            @Override
            public void onStart() {
                runOnUiThreadSafely(() -> {
                    mShareLoadingDialog.show();
                });
            }

            @Override
            public void onSuccess(Bitmap result) {
                runOnUiThreadSafely(() -> {
                    mShareLoadingDialog.dismiss();
                    UMImage image = new UMImage(RankShareActivity.this, result);
                    new ShareAction(RankShareActivity.this)
                            .setPlatform(share_media)
                            .withMedia(image)
                            .setCallback(new UMShareListener() {

                                @Override
                                public void onStart(SHARE_MEDIA share_media) {
                                    Log.i(TAG, "onStart: ");
                                }

                                @Override
                                public void onResult(SHARE_MEDIA share_media) {
                                    Log.i(TAG, "onResult: ");
                                    releaseShareableView();
                                }

                                @Override
                                public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                                    Log.i(TAG, "onError: ");
                                    releaseShareableView();
                                }

                                @Override
                                public void onCancel(SHARE_MEDIA share_media) {
                                    Log.i(TAG, "onCancel: ");
                                    releaseShareableView();
                                }
                            })
                            .share();
                });
            }

            @Override
            public void onError(Exception e) {
                runOnUiThreadSafely(() -> {
                    ToastToolKit.showShort(MessageUtils.getString(R.string.string_action_failed));
                    return;
                });
            }
        });

    }

    private void releaseShareableView() {
        if (mShareableView != null) {
            mShareableView.destroy();
            mShareableView = null;
        }
    }

    private ShareableView initShareableView(RankSharePayload payload) {
        if (payload.isOwnerYesterDay() || payload.isOwnerLastWeek() || payload.isOwnerAll()) {
            RoomRankShareableView roomRankShareableView = new RoomRankShareableView(this);
            roomRankShareableView.initData(payload);
            return roomRankShareableView;
        }

        if (payload.isTotalPopular()) {
            TotalPopularRankShareableView totalPopularRankShareableView = new TotalPopularRankShareableView(this);
            totalPopularRankShareableView.initData(payload);
            return totalPopularRankShareableView;
        }

        if (payload.isFamily()) {
            FamilyRankShareableView familyRankShareableView = new FamilyRankShareableView(this);
            familyRankShareableView.initData(payload);
            return familyRankShareableView;
        }

        if (payload.isLastWeekPopular()) {
            PopularRankShareableView popularRankShareableView = new PopularRankShareableView(this);
            popularRankShareableView.setBackGround(R.mipmap.ico_popular_last_week_share_1);
            popularRankShareableView.initData(payload);
            return popularRankShareableView;
        }

        if (payload.isTodayPopular()) {
            PopularRankShareableView popularRankShareableView = new PopularRankShareableView(this);
            popularRankShareableView.setBackGround(R.mipmap.ico_popular_today_rank_share_1);
            popularRankShareableView.initData(payload);
            return popularRankShareableView;
        }

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

}
