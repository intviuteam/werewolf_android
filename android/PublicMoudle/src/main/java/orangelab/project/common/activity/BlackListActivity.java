package orangelab.project.common.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Destroyable;
import orangelab.project.common.dialog.IntviuFriendDialog;
import orangelab.project.common.manager.BlackListManager;
import orangelab.project.common.model.BlockListResult;
import com.toolkit.ToolKit;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.RecycleViewDivider;
import orangelab.project.common.view.TitleView;
import orangelab.project.common.view.UserHeadView;
import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItem;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.GsonHelper;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/9/18 下午4:52
 * Mystery
 */

public class BlackListActivity extends SafeActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "BlackListActivity";

    private TitleView titleView;
    private SwipeRefreshLayout blackListContainer;
    private BlackListAdapter adapter;
    private SwipeMenuRecyclerView blackList;
    private boolean mTopIsRefreshing = false;
    private RequestTask mRequestTask = null;
    private WrapContentLinearLayoutManager layoutManger = new WrapContentLinearLayoutManager(this);
    private SafeHandler mSafeHandler = new SafeHandler();

    private int skip = 0;
    private final int limit = 10;
    private int lastVisibleItem;

    public static void Start(Context c) {
        Intent i = new Intent(c, BlackListActivity.class);
        c.startActivity(i);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blacklist);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        initView();
        initData();
    }

    private void initData() {
        onRefresh();
    }

    private void clearRequest() {
        if (mRequestTask != null && mRequestTask.isExecuting()) {
            mRequestTask.cancel();
        }
    }

    private void getBlockList() {
        mRequestTask = RequestTask.create().isGet(true).request("/block/list/withInfo?skip=" + skip + "&limit=" + limit)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        runOnUiThreadSafely(() -> {
                            if (isFirstPage()) {
                                adapter.clear();
                            }
                            BlockListResult result = GsonHelper.getGson().fromJson(response, BlockListResult.class);
                            if (result != null && result.users != null && result.users.size() > 0) {
                                adapter.addList(result.users);
                                addPage();
                            }
                            clearTopFresh();
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        runOnUiThreadSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.network_is_unavailable)));
                    }
                }).build();
        mRequestTask.execute();
    }

    private void addPage() {
        skip = skip + limit;
    }

    private void freshPage() {
        skip = 0;
    }

    private boolean isFirstPage() {
        return skip == 0;
    }

    private void clearTopFresh() {
        if (mTopIsRefreshing) {
            mTopIsRefreshing = false;
            blackListContainer.setRefreshing(mTopIsRefreshing);
        }
    }

    private void startTopFresh() {
        if (!mTopIsRefreshing) {
            mTopIsRefreshing = true;
//            blackListContainer.setRefreshing(mTopIsRefreshing);
        }
    }


    private void initView() {
        titleView = (TitleView) findViewById(R.id.black_list_title);
        blackListContainer = (SwipeRefreshLayout) findViewById(R.id.black_list_container);
        blackList = (SwipeMenuRecyclerView) findViewById(R.id.black_list);
        blackList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItem + 1 == adapter.getItemCount()) {
                    getBlockList();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItem = layoutManger.findLastVisibleItemPosition();
            }
        });
        blackList.setLayoutManager(layoutManger);
        blackListContainer.setOnRefreshListener(this);
        blackList.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.VERTICAL, ScreenUtils.dip2px(1), Color.parseColor("#c9c9c9")));
        blackList.setSwipeMenuCreator((swipeLeftMenu, swipeRightMenu, viewType) -> {
            SwipeMenuItem deleteItem = new SwipeMenuItem(BlackListActivity.this)
                    .setWidth(ScreenUtils.dip2px(70))
                    .setHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                    .setText(MessageUtils.getString(R.string.delete))
                    .setTextColor(Color.WHITE)
                    .setBackgroundColorResource(android.R.color.holo_red_light);
            swipeRightMenu.addMenuItem(deleteItem);
        });
        blackList.setSwipeMenuItemClickListener(menuBridge -> {
            menuBridge.closeMenu();
            int direction = menuBridge.getDirection(); // 左侧还是右侧菜单。
            int adapterPosition = menuBridge.getAdapterPosition(); // RecyclerView的Item的position。
            Log.i(TAG, "initView: " + direction + ",position=" + adapterPosition);
            if (direction == SwipeMenuRecyclerView.RIGHT_DIRECTION) {
                delete(adapterPosition);
            }
        });
        adapter = new BlackListAdapter();
        blackList.setAdapter(adapter);
        titleView.setTitle(MessageUtils.getString(R.string.black_list_title));
        titleView.setActionBack(v -> {
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void finish() {
        super.finish();
        clearRequest();
        adapter.destroy();
    }

    @Override
    public void onRefresh() {
        startTopFresh();
        freshPage();
        runOnUiThreadSafely(() -> {
            /**
             * 如果10s之后还在请求则取消请求
             */
            if (mTopIsRefreshing) {
                clearRequest();
                clearTopFresh();
            }
        }, 10000);
        getBlockList();
        BlackListManager.Refresh();
    }

    public static class BlackListAdapter extends RecyclerView.Adapter<BlackListItemHolder> implements Destroyable {
        private List<IntviuFriendDialog.FriendItem> testList = new ArrayList<>();

        public BlackListAdapter() {
        }

        public IntviuFriendDialog.FriendItem getItem(int position) {
            IntviuFriendDialog.FriendItem item = null;
            try {
                item = testList.get(position);
            } catch (Exception e) {
                item = null;
            }
            return item;
        }

        public void addList(List<IntviuFriendDialog.FriendItem> item) {
            if (ToolKit.isEmpty(item)) {
                return;
            }
            if (testList.containsAll(item)) {
                return;
            }
            testList.addAll(item);
            notifyDataSetChanged();
        }

        public void clear() {
            testList.clear();
            notifyDataSetChanged();
        }

        public void remove(int position) {
            if (position >= 0 && position < testList.size()) {
                testList.remove(position);
                notifyDataSetChanged();
            }
        }

        @Override
        public BlackListItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = View.inflate(parent.getContext(), R.layout.layout_blacklist_item, null);
            return new BlackListItemHolder(view);
        }

        @Override
        public void onBindViewHolder(BlackListItemHolder holder, int position) {
            IntviuFriendDialog.FriendItem testData = testList.get(position);
            holder.userName.setText(testData.name);
            holder.userTime.setText(Utils.formatTime(testData.created_at));
            holder.userHeadView.setUserHeadImageUrl(testData.image);
            holder.userHeadView.setUserSex(testData.sex);
        }

        @Override
        public int getItemCount() {
            return testList.size();
        }

        @Override
        public void destroy() {

        }
    }

    public void delete(int position) {
        IntviuFriendDialog.FriendItem item = null;
        if (adapter != null) {
            item = adapter.getItem(position);
        }
        if (item == null) {
            return;
        }
        final IntviuFriendDialog.FriendItem finalItem = item;
        RequestTask.create()
                .isGet(true)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .request("/block/remove/" + finalItem.id)
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        mSafeHandler.postSafely(() -> {
                            if (adapter != null) {
                                adapter.remove(position);
                            }
                            BlackListManager.Remove(finalItem.id);
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        mSafeHandler.postSafely(RunnableFactory.createMsgToastRunnable(failed));
                    }

                    @Override
                    public void onError(Exception e) {
                        mSafeHandler.postSafely(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.network_is_unavailable)));
                    }
                }).build().execute();
    }

    public static class BlackListItemHolder extends RecyclerView.ViewHolder {

        private UserHeadView userHeadView;
        private TextView userName;
        private TextView userTime;

        public BlackListItemHolder(View itemView) {
            super(itemView);
            userHeadView = (UserHeadView) itemView.findViewById(R.id.user_head);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            userTime = (TextView) itemView.findViewById(R.id.user_time);
        }

        public void setUrl(String url) {
            userHeadView.setUserHeadImageUrl(url);
        }

        public void setSex(int sex) {
            userHeadView.setUserSex(sex);
        }

        public void setName(String name) {
            if (TextUtils.isEmpty(name))
                return;
            userName.setText(name);
        }

        public void setTime(long time) {
            userTime.setText(Utils.formatTime(time));
        }
    }
}
