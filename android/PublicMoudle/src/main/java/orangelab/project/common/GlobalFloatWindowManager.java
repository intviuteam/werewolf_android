package orangelab.project.common;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.WindowManager;

import com.androidtoolkit.view.ScreenUtils;
import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import orangelab.project.common.floatwindow.view.PrivateRoomInviteView;
import orangelab.project.common.utils.SafeHandler;

/**
 * game_werewolf
 * 2018/5/17 上午11:46
 * Mystery
 */
public class GlobalFloatWindowManager implements Destroyable, AppLifeCycle {


    private Context mContext;
    private WindowManager mWindowManager;
    private GlobalService mIGlobalService;

    private PrivateRoomInviteFloatWindowHelper mPrivateRoomInviteFloatWindowHelper;

    public GlobalFloatWindowManager(Context context, GlobalService globalService) {
        mContext = context;
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mIGlobalService = globalService;
        mPrivateRoomInviteFloatWindowHelper = new PrivateRoomInviteFloatWindowHelper(context, globalService, mWindowManager);
    }

    public void show(PrivateRoomInviteFloatWindowHelper.PrivateRoomInvite invite) {
        if (invite == null) {
            return;
        }
        if (mPrivateRoomInviteFloatWindowHelper != null) {
            mPrivateRoomInviteFloatWindowHelper.showPrivateRoomInvite(invite);
        }
    }

    public void destroyPrivateRoomInvite() {
        if (mPrivateRoomInviteFloatWindowHelper != null) {
            mPrivateRoomInviteFloatWindowHelper.destroyPrivateRoomInvite();
        }
    }

    @Override
    public void destroy() {
        if (mIGlobalService != null) {
            mIGlobalService = null;
        }
        if (mPrivateRoomInviteFloatWindowHelper != null) {
            mPrivateRoomInviteFloatWindowHelper.destroy();
            mPrivateRoomInviteFloatWindowHelper = null;
        }
    }

    @Override
    public void appOnTop() {
        if (mPrivateRoomInviteFloatWindowHelper != null) {
            mPrivateRoomInviteFloatWindowHelper.appOnTop();
        }
    }

    @Override
    public void appInBackGround() {
        if (mPrivateRoomInviteFloatWindowHelper != null) {
            mPrivateRoomInviteFloatWindowHelper.appInBackGround();
        }
    }
}
