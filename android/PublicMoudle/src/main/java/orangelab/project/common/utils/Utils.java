package orangelab.project.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.os.Build;
import android.os.Process;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.Log;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.webkit.URLUtil;

import com.R;

import cn.intviu.support.NetworkHelpers;
import orangelab.project.MainApplication;
import orangelab.project.common.pay.google.GooglePayOrderTask;

import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.ToastToolKit;
import com.networktoolkit.transport.Constant;

import orangelab.project.common.model.BaseServerEvent;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.PersonalData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import cn.intviu.support.SharePreStoreUtils;
import orangelab.project.voice.manager.socketmanager.VoiceRoomGameSocketManager;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

public class Utils {

    private static final String TAG = "Utils";

    public static int[] GetViewLocationInScreen(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return location;
    }

    public static PointF GetViewLocationPointInScreen(View view) {
        int[] location = GetViewLocationInScreen(view);
        return new PointF(location[0], location[1]);
    }

    public static String formatTime(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
        return sdf.format(date);
    }

    public static String createMsgId() {
        return Long.toString(System.currentTimeMillis());
    }

    public static String createSendMessageFromJsonToString(String type, JSONObject payload, String msgId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(BaseServerEvent.TYPE, type);
            jsonObject.put(BaseServerEvent.PAYLOAD, payload);
            jsonObject.put(BaseServerEvent.MSG_ID, msgId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public static void fullScreenWindow(Window window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        window.setFormat(PixelFormat.TRANSLUCENT);
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void customGridViewHeightAndWidth(GridView gridView, int numColumns, int viewWidth, int viewHeight) {
        gridView.setNumColumns(numColumns);
        ListAdapter baseAdapter = gridView.getAdapter();
        int totalHeight = 0, totalWidth = 0;
        int columns = numColumns;
        int verWidth = gridView.getVerticalSpacing();
        int horWidth = gridView.getVerticalSpacing();
        int hor = baseAdapter.getCount() / numColumns;
        int base = baseAdapter.getCount() % numColumns == 0 ? 0 : 1;
        if (baseAdapter.getCount() > 0) {
            totalWidth =
                    viewWidth * columns + (columns - 1) * verWidth
                            + gridView.getPaddingLeft() + gridView.getPaddingRight();
            totalHeight = viewHeight * base + viewHeight * (hor) + (base + hor + 1) * horWidth
                    + gridView.getPaddingBottom() + gridView.getPaddingTop();
            ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
            layoutParams.height = totalHeight;
            layoutParams.width = totalWidth;
            gridView.setLayoutParams(layoutParams);
        }
    }

    public static void changeViewHeightAndWidth(View view, int width, int height) {
        ViewGroup.LayoutParams viewlayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams
                .WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        viewlayoutParams.width = width;
        viewlayoutParams.height = height;
        view.setLayoutParams(viewlayoutParams);
    }

    public static void customGridViewAndChildHeightAndWidth(GridView gridView, int numColumns, int childW, int childH) {
        gridView.setNumColumns(numColumns);
        ListAdapter baseAdapter = gridView.getAdapter();
        if (baseAdapter == null) {
            return;
        }
        int totalHeight = 0, totalWidth = 0;
        int columns = numColumns;
        int verWidth = gridView.getVerticalSpacing();
        int horWidth = gridView.getVerticalSpacing();
        int hor = baseAdapter.getCount() / numColumns;
        int base = baseAdapter.getCount() % numColumns == 0 ? 0 : 1;
        if (baseAdapter.getCount() > 0) {
            for (int i = 0; i < baseAdapter.getCount(); i++) {
                View view = baseAdapter.getView(i, null, gridView);
                if (view == null) {
                    continue;
                }
                int viewMeasureWidth = childW;
                int viewMeasureHeight = childH;
                AbsListView.LayoutParams viewlayoutParams = new AbsListView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                viewlayoutParams.width = viewMeasureWidth;
                viewlayoutParams.height = viewMeasureHeight;
                view.setLayoutParams(viewlayoutParams);
            }
            totalWidth = childW * columns + (columns - 1) * verWidth
                    + gridView.getPaddingLeft() + gridView.getPaddingRight();
            totalHeight = childH * base + childH * (hor) + (base + hor + 1) * horWidth
                    + gridView.getPaddingBottom() + gridView.getPaddingTop();
            ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
            layoutParams.height = totalHeight;
            layoutParams.width = totalWidth;
            gridView.setLayoutParams(layoutParams);
        }
    }


    public static void setGridViewHeightAndWidthBasedOnChildren(GridView gridView, int numColumns, int defaultWidth,
                                                                float scale) {
        gridView.setNumColumns(numColumns);
        ListAdapter baseAdapter = gridView.getAdapter();
        if (baseAdapter == null) {
            return;
        }
        int totalHeight = 0, totalWidth = 0;
        int columns = numColumns;
        int verWidth = gridView.getVerticalSpacing();
        int horWidth = gridView.getVerticalSpacing();
        int hor = baseAdapter.getCount() / numColumns;
        int base = baseAdapter.getCount() % numColumns == 0 ? 0 : 1;
        if (baseAdapter.getCount() > 0) {
            int viewWidth = 0;
            for (int i = 0; i < baseAdapter.getCount(); i++) {
                View view = baseAdapter.getView(i, null, gridView);
                if (view == null) {
                    continue;
                }
                view.measure(0, 0);
                int viewMeasureWidth = view.getMeasuredWidth() == 0 ? defaultWidth : view.getMeasuredWidth();
                int viewMeasureHeight = view.getMeasuredHeight() == 0 ? defaultWidth : view.getMeasuredHeight();
                viewMeasureWidth = viewMeasureWidth >= viewMeasureHeight ? viewMeasureWidth : view.getMeasuredHeight();
                viewWidth = (int) (scale * viewMeasureWidth);
                ViewGroup.LayoutParams viewlayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams
                        .WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                viewlayoutParams.width = viewWidth;
                viewlayoutParams.height = viewWidth;
                view.setLayoutParams(viewlayoutParams);
            }
            totalWidth = viewWidth * columns + (columns - 1) * horWidth;
            totalHeight = viewWidth * base + viewWidth * (hor) + hor * verWidth;

            ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
            layoutParams.height = totalHeight;
            layoutParams.width = totalWidth;
            gridView.setLayoutParams(layoutParams);
        }
    }

    public static String createUUID() {
        return UUID.randomUUID().toString();
    }

    public static int getMyselfLevelInGame(Map<Integer, EnterRoomResult.EnterRoomUserItem> userItemMap) {
        int level = -1;
        String userId = GlobalUserState.getGlobalState().getUserId();
        Iterator iterator = userItemMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Integer key = (Integer) entry.getKey() + 1;
            EnterRoomResult.EnterRoomUserItem value = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            if (value == null) {
                continue;
            }
            if (value.id.equals(userId)) {
                level = value.level;
                break;
            }
        }
        return level;
    }

    public static int getMyselfPositionInGame(PersonalData personalData, Map<Integer, EnterRoomResult.EnterRoomUserItem>
            userItemMap) {
        int result = -1;
        String userId = personalData.getUserId();
        Iterator iterator = userItemMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Integer key = (Integer) entry.getKey() + 1;
            EnterRoomResult.EnterRoomUserItem value = (EnterRoomResult.EnterRoomUserItem) entry.getValue();
            if (value == null) {
                continue;
            }
            if (value.id.equals(userId)) {
                result = key;
                break;
            }
        }
        return result;
    }

    public static TextView createTextView(Context context, int number) {
        TextView textView = new TextView(context);
        textView.setText(number + "");
        textView.setTextSize(ScreenUtils.sp2px(11));
        textView.setTextColor(context.getResources().getColor(android.R.color.white));
        return textView;
    }

    public static String addNumber(String cur, int number) {
        StringBuilder sb = new StringBuilder(cur);
        sb.append(",");
        sb.append(number);
        return sb.toString();
    }

    public static TextView createWolfSelectTextView(int text, Context context) {
        TextView numberText = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.width = ScreenUtils.dip2px(14);
        layoutParams.rightMargin = 2;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        numberText.setLayoutParams(layoutParams);
        numberText.setGravity(Gravity.CENTER);
        numberText.setTextSize(ScreenUtils.sp2px(4f));
        TextPaint paint = numberText.getPaint();
        paint.setFakeBoldText(true);
        numberText.setText(text + "");
        numberText.setTextColor(context.getResources().getColor(android.R.color.white));
        numberText.setBackgroundResource(R.drawable.wolf_team_background);
        return numberText;
    }

    public static void setWindowStatusBarColor(Activity activity, int colorResId) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(activity.getResources().getColor(colorResId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurTime() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    public static String formatTimeInMillis(long timeInMillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        Date date = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        String fmt = dateFormat.format(date);
        return fmt;
    }

    public static boolean checkUserIdIsTourist(String userId) {
        boolean result = false;
        try {
            long temp = Long.parseLong(userId);
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static void exitApp() {
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    public static String getLeanCloudNode(Context context) {
        return SharePreStoreUtils.getString(
                Constant.LEANCLOUD_NODE, context, Constant.LEANCLOUD_NODE_CONFIG, Constant.LEANCLOUD_CN);
    }

    public static void saveLeanCloudNode(Context context, String value) {
        SharePreStoreUtils.save(
                Constant.LEANCLOUD_NODE,
                context,
                Constant.LEANCLOUD_NODE_CONFIG,
                value
        );
    }

    public static void saveLeanCloudServerApi(Context context, String api) {
        if (TextUtils.isEmpty(api)) {
            api = "";
        }
        SharePreStoreUtils.save(Constant.LEANCLOUD_NODE, context, Constant.LEANCLOUD_SERVER_API, api);
    }

    public static String getLeanCloudServerApi(Context context) {
        return SharePreStoreUtils.getStringWithDef(Constant.LEANCLOUD_NODE, context, Constant.LEANCLOUD_SERVER_API, Constant.LEANCLOUD_SERVER_NONE);
    }

    public static void saveLeanCloudServerRtm(Context context, String rtm) {
        if (TextUtils.isEmpty(rtm)) {
            rtm = "";
        }
        SharePreStoreUtils.save(Constant.LEANCLOUD_NODE, context, Constant.LEANCLOUD_SERVER_RTM, rtm);
    }

    public static String getLeanCloudServerRtm(Context context) {
        return SharePreStoreUtils.getStringWithDef(Constant.LEANCLOUD_NODE, context, Constant.LEANCLOUD_SERVER_RTM, Constant.LEANCLOUD_SERVER_NONE);
    }

    public static boolean getApplyPermissionStatus(Context context) {
        return SharePreStoreUtils.getBoolean(
                Constant.CONFIG_DATA, context, Constant.PREMISSION_KEY, false);
    }

    public static void saveApplyPermissionStatus(Context context, boolean value) {
        SharePreStoreUtils.save(
                Constant.CONFIG_DATA,
                context,
                Constant.PREMISSION_KEY,
                value
        );
    }

    /**
     * Facebook广告回调状态
     * success 10s内回调
     * fail 10s内没有任何返回
     *
     * @param context
     * @return
     */
    public static String getShowAdvStatus(Context context) {
        return SharePreStoreUtils.getString(
                Constant.CONFIG_DATA, context, Constant.ADV_STATUS);
    }

    public static void saveAdvStatus(Context context, String value) {
        SharePreStoreUtils.save(
                Constant.CONFIG_DATA,
                context,
                Constant.ADV_STATUS,
                value
        );
    }

    public static int getAdvFailCount(Context context) {
        return SharePreStoreUtils.getInt(
                Constant.CONFIG_DATA, context, Constant.ADV_FAIL_COUNT);
    }

    public static void saveFailCount(Context context, int value) {
        SharePreStoreUtils.save(
                Constant.CONFIG_DATA,
                context,
                Constant.ADV_FAIL_COUNT,
                value
        );
    }

    public static void runSafely(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * 错误隐藏
             */
        }
    }

    public static boolean havePhonePermission(Context context, String permission) {
        boolean result;
        try {
            if (null == context) {
                result = false;
            } else {
                Log.i(TAG, "HavePhonePermission buildVersion = " + Build.VERSION.SDK_INT);
                if (Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(context, permission) != 0) {
                        Log.i(TAG, permission + "---> No permission");
                        result = false;
                    } else {
                        Log.i(TAG, permission + "---> Have permission");
                        result = true;
                    }
                } else {
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;

    }

    public static void executeAsyncPayDataReportAll(Context context) {
        ThreadToolKit.Async(() -> {
            Utils.runSafely(() -> {
                GooglePayOrderTask task = new GooglePayOrderTask(context);
                task.run();
            });
        });
    }

    public static boolean isURL(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
//        Pattern pattern = Pattern
//                .compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
//        return pattern.matcher(str).matches();
        return URLUtil.isNetworkUrl(str);
    }

    public static boolean targetIsNull(Object o) {
        if (o == null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean targetIsNotNull(Object o) {
        if (o != null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean targetListIsNull(List o) {
        if (targetIsNull(o)) {
            return true;
        }
        return o.isEmpty();
    }

    public static boolean intEqualString(String targetString, int targetInt) {
        try {
            return Integer.parseInt(targetString) == targetInt;
        } catch (Exception e) {
            return false;
        }
    }

    public static String intStringAdd(String target) {
        try {
            int result = Integer.parseInt(target);
            result++;
            return Integer.toString(result);
        } catch (Exception e) {
            return target;
        }
    }

    public static String filterUserName(String userName, int maxLength) {
        if (TextUtils.isEmpty(userName)) {
            return "";
        }
        String tempName = "";
        if (userName.length() > maxLength) {
            tempName = userName.substring(0, maxLength - 2);
        }
        if (TextUtils.isEmpty(tempName)) {
            return userName;
        }
        return tempName + "...";
    }

    public static int getTotalHeightOfListView(ListView listView) {
        ListAdapter mAdapter = listView.getAdapter();
        if (mAdapter == null) {
            return 0;
        }
        int totalHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            //mView.measure(0, 0);
            totalHeight += mView.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
        return totalHeight;
    }

    public static String filterStringForJS(String input) {
        if (TextUtils.isEmpty(input)) return Constant.EMPTY;

        String result = Constant.EMPTY;
        /**
         * 替换字符串中的单引号
         */
        result = input.replaceAll("'", "‘");

        /**
         * 替换字符串中的双引号
         */
        result = result.replaceAll("\"", "“");

        /**
         * 替换斜杠
         */
        result = result.replaceAll("\\\\", "");

        return result;
    }

    public static int stringToInt(String input) {
        if (TextUtils.isEmpty(input)) {
            return 0;
        } else {
            try {
                int result = Integer.parseInt(input);
                return result;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    public static boolean judgeConnectStatusWithToast(String toast) {
        //网络断开或者socket没连上
        boolean isConnect = NetworkHelpers.isNetworkAvailable(MainApplication.getInstance()) &&
                VoiceRoomGameSocketManager.getInstance().isConnected();
        if (!isConnect) {
            if (!toast.equals("")) {
                ToastToolKit.showShort(toast);
            }
            return false;
        }
        return true;
    }

}
