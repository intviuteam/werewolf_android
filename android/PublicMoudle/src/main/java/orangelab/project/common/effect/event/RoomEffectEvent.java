package orangelab.project.common.effect.event;

import android.graphics.PointF;
import android.text.TextUtils;

import com.networktoolkit.transport.Constant;

import orangelab.project.voice.constants.VoiceConstants;

/**
 * game_werewolf
 * 2018/8/1 上午10:28
 * Mystery
 * 房间内特效通用的Event
 */
public class RoomEffectEvent {

    public static final String SMALL_GAME = "small_game";
    public static final String ROOM_EMOTION = "room_Emotion";

    /**
     * 更新状态数据
     */
    public static class UpdateEffectContextData {
        /**
         * 自己的位置
         */
        public int mSelfPosition = -1;
        /**
         * 房主的位置
         */
        public int mMasterPosition = -1;
        /**
         * 自己是否处于麦上
         */
        public boolean mSelfIsAvailablePosition = false;
    }


    public static class RoomMemberEffect {

        /**
         * 事件类型
         */
        public String eventType = "";

        /**
         * 特效类型
         */
        public String type;

        public int position;
        public PointF point;
        public int width;
        public int height;

        /**
         * 特效url
         */
        public String url;

        /**
         * 特效持续时间，用于强制掐断gif 播放使用，防止Gif无限播放
         */
        public long duration = 0;
        /**
         * gif播放次数
         */
        public int repeatTime = 0;

        /**
         * 最后一帧持续时间
         */
        public long lastFrameDuration;
        /**
         * 最后一帧数据
         */
        public int lastFrame;
        /**
         * 背景音乐
         */
        public String backgroundMusic;

        public Runnable finishRunnable;

        public boolean isSmallGame() {
            return TextUtils.equals(eventType, SMALL_GAME);
        }

        public boolean isEmotion() {
            return TextUtils.equals(eventType, ROOM_EMOTION);
        }

        public boolean isSmallDiceGame() {
            return TextUtils.equals(type, VoiceConstants.DICE);
        }

        public boolean isCoinGame() {
            return TextUtils.equals(type, VoiceConstants.COINS);
        }

        public boolean isScoreGame() {
            return TextUtils.equals(type, VoiceConstants.POINTS);
        }

        public boolean isGuessGame() {
            return TextUtils.equals(type, VoiceConstants.FINGER_GUESSING);
        }

        public boolean isOptMachineGame() {
            return TextUtils.equals(type, VoiceConstants.OCTOPUS);
        }

        public boolean isLotteryGame() {
            return TextUtils.equals(type, VoiceConstants.MIC);
        }

        public boolean isPokerCardGame() {
            return TextUtils.equals(type, VoiceConstants.POKER);
        }

    }

    /**
     * 清除某个位置的特效
     */
    public static class RoomEffectClear {
        public int position;

        public RoomEffectClear(int position) {
            this.position = position;
        }
    }

    /**
     * 房间特效的基本数据
     */
    public static class RoomEffectHolder {
        public int position;
        public String type;
        public PointF point;
        public int width;
        public int height;
        public boolean isLeader = false;
        public boolean isSelf = false;
        public boolean isUpSeat = false;
    }
}
