package orangelab.project.common.engine

import android.os.Handler
import android.text.TextUtils
import com.R
import com.androidtoolkit.PLog
import com.toolkit.action.Destroyable
import com.toolkit.action.Executable
import orangelab.project.common.engine.task.SocketCallBack
import orangelab.project.common.model.ServerMessageEvent
import orangelab.project.common.model.ServerResponseEvent
import orangelab.project.common.utils.MessageUtils
import java.util.*

/**
 *game_werewolf
 *2018/4/12 上午10:13
 *Mystery
 */
class SocketEngineTaskManager : Destroyable {

    class SocketEngineTask : Destroyable, Executable {

        private var mTaskType: String = ""
        private var mTaskId: String
        private var mSuccessCallBack: SocketCallBack? = null
        private var mFailedCallBack: SocketCallBack? = null
        private var mRequestID: String = ""

        constructor(mTaskType: String, mRequestID: String, mSuccessCallBack: SocketCallBack?, mFailedCallBack: SocketCallBack?) {
            this.mTaskType = mTaskType
            this.mTaskId = UUID.randomUUID().toString()
            this.mSuccessCallBack = mSuccessCallBack
            this.mFailedCallBack = mFailedCallBack
            this.mRequestID = mRequestID
        }

        fun getTaskType(): String {
            return mTaskType
        }

        fun getTaskId(): String {
            return mTaskId
        }

        fun getRequestId(): String {
            return mRequestID
        }

        fun getSuccessCallBack(): SocketCallBack? {
            return mSuccessCallBack
        }

        fun getFailedCallBack(): SocketCallBack? {
            return mFailedCallBack
        }

        override fun execute() {
            /**
             * 开始执行Task
             */
        }

        override fun destroy() {
            mSuccessCallBack = null
            mFailedCallBack = null
        }

        override fun toString(): String {
            return "SocketEngineTask(mTaskType='$mTaskType', mTaskId='$mTaskId', mRequestID='$mRequestID')\n"
        }


    }

    private val TAG: String
    private val mSocketType: String

    private var mHandler: Handler? = null
    private var mTaskMapByType: MutableMap<String, MutableList<SocketEngineTask>>
    private var mTaskMapById: MutableMap<String, SocketEngineTask>


    constructor(TAG: String, socketType: String, mHandler: Handler?) {
        this.TAG = "${TAG}TaskManager"
        this.mSocketType = socketType
        this.mHandler = mHandler
        this.mTaskMapByType = HashMap()
        this.mTaskMapById = HashMap()
    }

    fun endTaskForce(taskType: String) {
        removeTaskByType(taskType)
    }

    fun addTask(taskType: String, successCallback: SocketCallBack?, failCallback: SocketCallBack?) {
        addTask(taskType, successCallback, failCallback, 0L)
    }

    fun addTask(taskType: String, successCallback: SocketCallBack?, failCallback: SocketCallBack?, timeOut: Long) {
        addTask(taskType, successCallback, failCallback, timeOut, "")
    }

    fun addTask(taskType: String, successCallback: SocketCallBack?, failCallback: SocketCallBack?, timeOut: Long, requestId: String) {
        val task = SocketEngineTask(taskType, requestId, successCallback, failCallback)
        printLog(task, "is create")
        if (timeOut > 0) {
            mHandler?.postDelayed({
                timeOutCheck(task)
            }, timeOut)
        }
        /**
         * 记录任务
         */
        mTaskMapById.put(task.getTaskId(), task)
        var taskList = mTaskMapByType.get(task.getTaskType())
        if (taskList == null) {
            taskList = LinkedList()
            taskList!!.add(task)
            mTaskMapByType.put(task.getTaskType(), taskList)
        } else {
            taskList.add(task)
        }
        printLogData("After Add Task")
    }

    fun handleTask(serverResponseEvent: ServerResponseEvent) {

        val type = serverResponseEvent.type

        if (!containsTasKType(type)) {
            printLog("${type} task not in map")
            return
        }
        val typeList = getTaskTypeFromMapByType(type)
        if (typeList == null) {
            printLog("${type} task list is null")
            return
        }
        val startTime = System.currentTimeMillis()
        typeList.forEach {
            /**
             * 遍历类型任务列表
             */
            if (it.getRequestId().isNullOrBlank()) {
                /**
                 * 不需要校验RequestId
                 */
                if (serverResponseEvent.code == 1000) {

                    try {
                        it.getSuccessCallBack()?.invoke(serverResponseEvent.payload.toString())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    try {
                        it.getFailedCallBack()?.invoke(serverResponseEvent.message)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
                printLog(it, "is success")
                it.destroy()
                removeTaskFromMapById(it)
                removeTaskFromMapByType(it)
            } else {
                /**
                 * 需要校验RequestId
                 */
                if (TextUtils.equals(it.getRequestId(), serverResponseEvent.requestId)) {
                    if (serverResponseEvent.code == 1000) {
                        try {
                            it.getSuccessCallBack()?.invoke(serverResponseEvent.payload.toString())
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        try {
                            it.getFailedCallBack()?.invoke(serverResponseEvent.message)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    printLog(it, "is success")
                    it.destroy()
                    removeTaskFromMapById(it)
                    removeTaskFromMapByType(it)
                }
            }
        }
        if (typeList.size == 0) {
            removeTaskTypeFromMapByType(type)
        }
        val endTime = System.currentTimeMillis()
        printLog("Handle ${type} task cost ${endTime - startTime}")
    }

    fun handleTask(serverMessageEvent: ServerMessageEvent) {

        val type = serverMessageEvent.type

        if (!containsTasKType(type)) {
            printLog("${type} task not in map")
            return
        }

        val typeList = getTaskTypeFromMapByType(type)
        if (typeList == null) {
            printLog("${type} task list is null")
            return
        }
        val startTime = System.currentTimeMillis()
        typeList.forEach {
            /**
             * 遍历类型任务列表
             */
            if (it.getRequestId().isNullOrBlank()) {
                /**
                 * 不需要校验RequestId
                 */
                try {
                    it.getSuccessCallBack()?.invoke(serverMessageEvent.payload.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                printLog(it, "is success")
                it.destroy()
                removeTaskFromMapById(it)
                removeTaskFromMapByType(it)
            } else {
                /**
                 * 需要校验RequestId
                 */
                if (TextUtils.equals(it.getRequestId(), serverMessageEvent.requestId)) {
                    try {
                        it.getSuccessCallBack()?.invoke(serverMessageEvent.payload.toString())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    printLog(it, "is success")
                    it.destroy()
                    removeTaskFromMapById(it)
                    removeTaskFromMapByType(it)
                }
            }
        }
        if (typeList.size == 0) {
            removeTaskTypeFromMapByType(type)
        }
        val endTime = System.currentTimeMillis()
        printLog("Handle ${type} task cost ${endTime - startTime}")
    }

    private fun timeOutCheck(task: SocketEngineTask?) {

        task ?: return

        printLog(task, "Begin check task")

        val tempTask = mTaskMapById.remove(task.getTaskId())

        if (tempTask == null) {
            printLog(task, "Task has been finished")
            return
        }

        removeTaskFromMapByType(tempTask)

        printLog(tempTask, "is time out ")

        tempTask.getFailedCallBack()?.invoke(MessageUtils.getString(R.string.string_action_timeout))

        tempTask.destroy()

        printLog(tempTask, "is destroy")
    }

    private fun removeTaskByType(type: String?) {
        TextUtils.isEmpty(type) ?: return

        val list = mTaskMapByType.get(type)

        list ?: return

        list.forEach {
            it.destroy()
            removeTaskFromMapById(it)
        }
    }

    private fun removeTaskFromMapById(task: SocketEngineTask?) {
        task ?: return
        mTaskMapById.remove(task.getTaskId())
        printLogDataById("After removeTaskFromMapById")
    }

    private fun containsTasKType(type: String): Boolean {
        return mTaskMapByType.containsKey(type)
    }

    private fun getTaskTypeFromMapByType(type: String): List<SocketEngineTask>? {
        return mTaskMapByType.get(type)
    }

    private fun removeTaskTypeFromMapByType(type: String): List<SocketEngineTask>? {
        val result = mTaskMapByType.remove(type)
        printLogDataByType("After removeTaskTypeFromMapByType")
        return result
    }

    private fun removeTaskFromMapByType(task: SocketEngineTask?) {
        task ?: return
        val list = mTaskMapByType.get(task.getTaskType())
        list ?: return
        list.remove(task)
        printLogDataByType("after removeTaskFromMapByType")
    }

    override fun destroy() {
        mTaskMapByType.forEach {
            it.value.forEach {
                it.destroy()
            }
            it.value.clear()
        }
        mTaskMapByType.clear()
        mTaskMapById.clear()
    }

    private fun printLogData(info: String) {
        printLogDataById(info)
        printLogDataByType(info)
    }

    private fun printLogDataByType(info: String) {
        printLog("${info} mTaskMapByType ${mTaskMapByType.toString()}")
    }

    private fun printLogDataById(info: String) {
        printLog("${info} mTaskMapById ${mTaskMapById.toString()}")
    }

    private fun printLog(task: SocketEngineTask?, info: String) {
        printLog("task[${task?.getTaskType()}，${task?.getTaskId()}]:${info}")
    }

    private fun printLog(info: String) {
        PLog.i(TAG, "[${Thread.currentThread().name}][${mSocketType}]${info}")
    }

}