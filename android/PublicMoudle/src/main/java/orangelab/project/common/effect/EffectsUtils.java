package orangelab.project.common.effect;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.R;

import org.greenrobot.eventbus.EventBus;

import cn.intviu.support.SystemProperties;
import orangelab.project.common.effect.event.RoomEffectEvent;
import orangelab.project.common.effect.frames.ThreadFramesAnim;
import orangelab.project.voice.manager.GameScoreReflection;

/**
 * game_werewolf
 * 2018/7/30 下午5:26
 * Mystery
 * <p>
 * 动画效果工具类
 */
public class EffectsUtils {


    public static Animator createShakeAnimator(View view) {
        ObjectAnimator translationXAnim = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, -6.0f, 6.0f, -6.0f, 6.0f);
        translationXAnim.setRepeatCount(ValueAnimator.INFINITE);
        translationXAnim.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator scale = ObjectAnimator.ofFloat(view, View.ROTATION, -2f, 2f, -2f, 2f);
        scale.setRepeatCount(ValueAnimator.INFINITE);
        scale.setRepeatMode(ValueAnimator.REVERSE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(translationXAnim).with(scale);

        return animatorSet;
    }

    public static Animator createAlphaOut(View view, long duration) {
        ObjectAnimator animatorAlphaOut = ObjectAnimator.ofFloat(view, "alpha", 1, 0);
        animatorAlphaOut.setDuration(duration);
        return animatorAlphaOut;
    }

    public static Animator createTranslationY(View view, float from, float to, long duration) {
        ObjectAnimator translationY = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, from, to);
        translationY.setDuration(duration);
        return translationY;
    }

    public static Animator combineWithAnimator(Animator animator1, Animator animator2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animator1).with(animator2);
        return animatorSet;
    }

    public static void removeViewIfNeed(ViewGroup parent, View child) {
        if (parent == null || child == null) return;

        if (child.getParent() != null) {
            ViewParent viewParent = child.getParent();
            if (viewParent instanceof ViewGroup) {
                if (((ViewGroup) viewParent) == parent) {
                    parent.removeView(child);
                }
            }
        }
    }

    public static void postUpdateContextData(int selfPosition, int masterPosition, boolean selfIsAvailablePosition) {
        RoomEffectEvent.UpdateEffectContextData data = new RoomEffectEvent.UpdateEffectContextData();
        data.mSelfPosition = selfPosition;
        data.mMasterPosition = masterPosition;
        data.mSelfIsAvailablePosition = selfIsAvailablePosition;
        EventBus.getDefault().post(data);
    }

    public static void postEmotionEffects(int position, PointF pointF, int width, int height, String type, String url, long duration) {
        RoomEffectEvent.RoomMemberEffect effect = new RoomEffectEvent.RoomMemberEffect();
        effect.eventType = RoomEffectEvent.ROOM_EMOTION;
        effect.position = position;
        effect.point = pointF;
        effect.width = width;
        effect.height = height;
        effect.type = type;
        effect.url = url;
        effect.repeatTime = 1;
        effect.duration = duration;
        EventBus.getDefault().post(effect);
    }


    /**
     * 硬币最后一帧数据构造
     */
    public static class CoinGameFrameCreator implements ThreadFramesAnim.FrameCreator {

        private Context mContext;
        private int lastFrame;
        private long duration;

        public CoinGameFrameCreator(Context context, int lastFrame, long duration) {
            mContext = context;
            this.lastFrame = lastFrame;
            this.duration = duration;
        }

        @Override
        public Bitmap createFrame() {
            return BitmapFactory.decodeResource(mContext.getResources(), GameScoreReflection.coinToImageId(lastFrame));
        }

        @Override
        public long getDuration() {
            return duration;
        }
    }

    /**
     * 猜拳最后一帧数据构造
     */
    public static class GuessGameFrameCreator implements ThreadFramesAnim.FrameCreator {

        private Context mContext;
        private int lastFrame;
        private long duration;

        public GuessGameFrameCreator(Context context, int lastFrame, long duration) {
            mContext = context;
            this.lastFrame = lastFrame;
            this.duration = duration;
        }

        @Override
        public Bitmap createFrame() {
            return BitmapFactory.decodeResource(mContext.getResources(), GameScoreReflection.guessToImageId(lastFrame));
        }

        @Override
        public long getDuration() {
            return duration;
        }
    }

    /**
     * 骰子最后一帧数据构造
     */
    public static class DiceGameFrameCreator implements ThreadFramesAnim.FrameCreator {

        private Context mContext;
        private int lastFrame;
        private long duration;

        public DiceGameFrameCreator(Context context, int lastFrame, long duration) {
            mContext = context;
            this.lastFrame = lastFrame;
            this.duration = duration;
        }

        @Override
        public Bitmap createFrame() {
            return BitmapFactory.decodeResource(mContext.getResources(), GameScoreReflection.diceToImageId(lastFrame));
        }

        @Override
        public long getDuration() {
            return duration;
        }
    }

    public static class OctopusMachineGameFrameCreator implements ThreadFramesAnim.FrameCreator {

        private Context mContext;
        private int lastFrame;
        private long duration;
        private int width;
        private int height;

        public OctopusMachineGameFrameCreator(Context context, int lastFrame, long duration, int width, int height) {
            mContext = context;
            this.lastFrame = lastFrame;
            this.duration = duration;
            this.width = width;
            this.height = height;
        }

        @Override
        public Bitmap createFrame() {


            int a = lastFrame / 100;
            int b = (lastFrame % 100) / 10;
            int c = lastFrame % 10;

            Rect bgRect = new Rect(0, 0, width, height);

            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ico_small_game_octopus_machine), null, bgRect, null);

            int aleft = 44 * width / 167;
            int atop = 47 * height / 166;
            int awidth = 24 * width / 167;
            int aheight = 26 * width / 166;
            Rect aRect = new Rect(aleft, atop, aleft + awidth, atop + aheight);

            Paint apaint = new Paint();
            apaint.setStyle(Paint.Style.FILL);
            apaint.setFakeBoldText(true);
            apaint.setColor(Color.parseColor("#a64d15"));
            apaint.setTextSize(aheight * 0.7f);
            apaint.setTextAlign(Paint.Align.CENTER);

            Paint.FontMetrics afontMetrics = apaint.getFontMetrics();
            float atextTop = afontMetrics.top;//为基线到字体上边框的距离,即上图中的top
            float atextBottom = afontMetrics.bottom;
            int abaseLineY = (int) (aRect.centerY() - atextTop / 2 - atextBottom / 2);
            canvas.drawText(Integer.toString(a), aRect.centerX(), abaseLineY, apaint);


            int bleft = 68 * width / 167;
            int btop = atop;
            int bwidth = 26 * width / 167;
            int bheight = aheight;
            Rect bRect = new Rect(bleft, btop, bleft + bwidth, btop + bheight);
            Paint bpaint = new Paint();
            bpaint.setStyle(Paint.Style.FILL);
            bpaint.setFakeBoldText(true);
            bpaint.setColor(Color.parseColor("#4082a1"));
            bpaint.setTextSize(aheight * 0.7f);
            bpaint.setTextAlign(Paint.Align.CENTER);

            Paint.FontMetrics bfontMetrics = bpaint.getFontMetrics();
            float btextTop = bfontMetrics.top;//为基线到字体上边框的距离,即上图中的top
            float btextBottom = bfontMetrics.bottom;
            int bbaseLineY = (int) (bRect.centerY() - btextTop / 2 - btextBottom / 2);
            canvas.drawText(Integer.toString(b), bRect.centerX(), bbaseLineY, bpaint);


            int cleft = 98 * width / 167;
            int ctop = atop;
            int cwidth = 24 * width / 167;
            int cheight = aheight;
            Rect cRect = new Rect(cleft, ctop, cleft + cwidth, ctop + cheight);

            Paint cpaint = new Paint();
            cpaint.setStyle(Paint.Style.FILL);
            cpaint.setFakeBoldText(true);
            cpaint.setColor(Color.parseColor("#6515a6"));
            cpaint.setTextSize(aheight * 0.7f);
            cpaint.setTextAlign(Paint.Align.CENTER);

            Paint.FontMetrics cfontMetrics = cpaint.getFontMetrics();
            float ctextTop = cfontMetrics.top;//为基线到字体上边框的距离,即上图中的top
            float ctextBottom = cfontMetrics.bottom;
            int cbaseLineY = (int) (cRect.centerY() - ctextTop / 2 - ctextBottom / 2);
            canvas.drawText(Integer.toString(c), cRect.centerX(), cbaseLineY, cpaint);

            return bitmap;
        }

        @Override
        public long getDuration() {
            return duration;
        }
    }

    public static class LotteryGameFrameCreator implements ThreadFramesAnim.FrameCreator {

        private Context mContext;
        private int lastFrame;
        private long duration;
        private int width;
        private int height;

        public LotteryGameFrameCreator(Context context, int lastFrame, long duration, int width, int height) {
            mContext = context;
            this.lastFrame = lastFrame;
            this.duration = duration;
            this.width = width;
            this.height = height;
        }

        @Override
        public Bitmap createFrame() {
            Rect rect = new Rect(0, 0, width, height);
            int left = width * 58 / 165;
            int top = height * 36 / 166;

            int textHeight = 45 * height / 166;
            int textWidth = 40 * width / 165;

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setFakeBoldText(true);
            paint.setColor(Color.parseColor("#df4b7a"));
            paint.setTextSize(textHeight * 0.7f);
            paint.setTextAlign(Paint.Align.CENTER);

            Rect textRect = new Rect(left, top, left + textWidth, top + textHeight);

            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ico_small_game_lottery), null, rect, null);

            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float textTop = fontMetrics.top;//为基线到字体上边框的距离,即上图中的top
            float textBottom = fontMetrics.bottom;
            int baseLineY = (int) (textRect.centerY() - textTop / 2 - textBottom / 2);
            canvas.drawText(Integer.toString(lastFrame), textRect.centerX(), baseLineY, paint);
            return bitmap;
        }

        @Override
        public long getDuration() {
            return duration;
        }
    }

}
