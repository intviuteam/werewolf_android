package orangelab.project.common.effect.view;

import android.content.Context;

import com.toolkit.action.Destroyable;

import java.util.List;

import orangelab.project.common.effect.smallgame.SmallGameManiFest;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.manager.SmallGameListManager;

/**
 * game_werewolf
 * 2018/8/1 上午10:41
 * Mystery
 */
public class RoomEffectsContext implements Destroyable {

    private Context mContext;
    private SmallGameListManager mSmallGameManager;
    private SafeHandler mSafeHandler = new SafeHandler();

    private long mLastSendEffectsTime = 0;
    private long mSmallGameDefaultDuration = 3000;
    private long mEmotionDefaultDuration = 3000;

    private boolean mPokerIsShown = false;

    /**
     * 三个基本数据
     *
     * @param context
     */
    private int mSelfPosition = -1;  //自己的位置
    private int mMasterPosition = -1; //房主的位置
    private boolean mSelfIsAvailablePosition = false; //自己是否处于可展示特效的位置

    public RoomEffectsContext(Context context) {
        mContext = context;
        mSmallGameManager = new SmallGameListManager(mContext);
    }

    public void updatePositionData(int selfPosition, int masterPosition, boolean selfIsAvailablePosition) {
        this.mSelfPosition = selfPosition;
        this.mMasterPosition = masterPosition;
        this.mSelfIsAvailablePosition = selfIsAvailablePosition;
    }

    public SmallGameManiFest.SmallGame getSmallGame(String type) {
        return mSmallGameManager.find(type);
    }

    public List<SmallGameManiFest.SmallGame> getSmallGameList() {
        /**
         * 这里需要根据特效情况来处理下返回数据的
         */

        return mSmallGameManager.getSmallGameList();
    }

    public void post(Runnable runnable) {
        post(runnable, 0);
    }

    public void remove(Runnable runnable) {
        mSafeHandler.removeCallbacks(runnable);
    }

    public void post(Runnable runnable, long delay) {
        mSafeHandler.postDelaySafely(runnable, delay);
    }

    public Context getContext() {
        return mContext;
    }

    public void recordLastSendEffectsTime(long time) {
        mLastSendEffectsTime = time;
    }

    public long getLastSendEffectsTime() {
        return mLastSendEffectsTime;
    }

    public long getSmallGameDefaultDuration() {
        return mSmallGameDefaultDuration;
    }

    public long getEmotionDefaultDuration() {
        return mEmotionDefaultDuration;
    }

    public int getSelfPosition() {
        return mSelfPosition;
    }

    public int getMasterPosition() {
        return mMasterPosition;
    }

    public boolean isSelfIsAvailablePosition() {
        return mSelfIsAvailablePosition;
    }

    public boolean isPokerIsShown() {
        return mPokerIsShown;
    }

    public void setPokerIsShown(boolean pokerIsShown) {
        mPokerIsShown = pokerIsShown;
    }

    @Override
    public void destroy() {
        mSafeHandler.release();
        if (mSmallGameManager != null) {
            mSmallGameManager.destroy();
        }
    }
}
