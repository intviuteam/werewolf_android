package orangelab.project.common.effect.event;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * game_werewolf
 * 2018/8/1 上午10:22
 * Mystery
 */
public class RoomGameEffectsEvent {

    /**
     * 房间内🎲游戏的Event集合
     */


    public static class DiceGameOver {
        /**
         * 结束🎲游戏
         */

    }

    public static class DiceGameStart {

        public SparseArray<RoomEffectEvent.RoomEffectHolder> mHolderSparseArray;

    }

    /**
     * 测试开牌
     */
    public static class DiceGameOpenTest {

        public int position;
        public List<Integer> diceValue;


        public static List<Integer> random() {
            List<Integer> test = new ArrayList<>();
            Random random = new Random();
            for (int i = 0; i < 5; i++) {
                test.add(random.nextInt(6) + 1);
            }
            return test;
        }
    }

    public static class EffectsClear {
        /**
         * 清除某个特效
         */
        public int position;
    }


}
