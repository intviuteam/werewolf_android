package orangelab.project.common.component;

import android.os.Handler;
import android.util.Log;

import com.toolkit.action.Destroyable;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2017/7/31 下午6:51
 * Mystery
 */

public abstract class BasicViewComponent implements Destroyable {

    private static final String TAG = "BasicViewComponent";

    private Handler mHandler = null;

    public BasicViewComponent() {
        mHandler = new Handler();
        Log.i(TAG, "BasicView: create new BasicView:" + hashCode());
    }

    protected void postDelay(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(runnable, delay);
        }
    }

    protected void post(Runnable runnable) {
        postDelay(runnable, 0);
    }

    protected void postDelaySafely(Runnable runnable, long delay) {
        if (mHandler != null) {
            mHandler.postDelayed(() -> {
                Utils.runSafely(runnable);
            }, delay);
        }
    }

    protected void removeRunnable(Runnable runnable) {
        if (mHandler != null) {
            mHandler.removeCallbacks(runnable);
        }
    }

    protected void postSafely(Runnable runnable) {
        postDelaySafely(runnable, 0);
    }

    public void destroy() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        Log.i(TAG, "destroy: relese BasicView:" + hashCode());
    }
}
