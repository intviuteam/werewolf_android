package orangelab.project.common.upgrade;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Toast;

import com.R;
import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.UIActuator;
import orangelab.project.common.manager.ApiManager;
import com.networktoolkit.transport.Constant;
import orangelab.project.common.utils.IntentDataHelper;
import com.androidtoolkit.PLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import cn.intviu.sdk.AppMiddlewareApiDefines;
import cn.intviu.sdk.model.AppUpgradeInfo;
import cn.intviu.support.AppUtil;
import cn.intviu.support.FileUtils;
import cn.intviu.support.GsonHelper;
import cn.intviu.support.HttpUtil;
import cn.intviu.support.MoreCloseables;

/**
 * Created by wangxu on 2017/4/7.
 * 用于检查／下载／安装应用
 */

public class UpgradeService extends Service implements IUpgradeServiceDefines, AppMiddlewareApiDefines {
    private static final String TAG = "UpgradeService";
    //操作的类型
    private String upgradeType = null;
    //是否主动检测升级
    private boolean initiativeCheck = false;
    //应用的下载地址
    private String apkUrl = null;
    //token
    private String token = "";
    //apk文件
    private File mPackageFile;
    //
    private int lastProcess = 0;
    //应用的详细信息
    private AppUpgradeInfo.AppUpgradeDetail mAppUpgradeDetail = null;
    //
    private final AtomicBoolean mPause = new AtomicBoolean(false);


    @Override
    public void onCreate() {
        super.onCreate();
        mPackageFile = new File(this.getExternalCacheDir(), "apk/xiaobanhui_werewolf.apk");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 处理耗时的操作
     */
    protected void onHandleIntent(Intent intent) {
        try {
            PLog.e(TAG, "onHandleIntent: ");
            if (intent != null) {
                upgradeType = IntentDataHelper.getUpgradeType(intent);
                apkUrl = IntentDataHelper.getDownloadApkUrl(intent);
                initiativeCheck = IntentDataHelper.getCheckType(intent);
                if (upgradeType.equals(Constant.DOWNLOAD_APK)) {
                    //执行下载的操作
                    ThreadToolKit.Async(() -> downloadNewApk());
                } else if (upgradeType.equals(Constant.CHECK_UPGRADE)) {
                    //执行检查更新的操作
                    checking();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized AppUpgradeInfo.AppUpgradeDetail getAppUpgradeDetail() {
        return mAppUpgradeDetail;
    }

    public synchronized void setAppUpgradeDetail(AppUpgradeInfo.AppUpgradeDetail mAppUpgradeDetail) {
        this.mAppUpgradeDetail = mAppUpgradeDetail;
    }

    private void stop() {
        stopSelf();
    }

    /**
     * 检查更新
     */
    private void checking() {
        //得到App当前版本号
        int version_code = AppUtil.getVersionCode(this);
        String apk_channel = AppUtil.getApkChannel(this);
        ApiManager.CheckUpdateApk(version_code, apk_channel, (appUpgradeDetail, e) -> {
            if (e == null) {
                if (appUpgradeDetail != null) {

                    PLog.i(TAG, "onSuccess: " + appUpgradeDetail.toString());
                    setAppUpgradeDetail(appUpgradeDetail);
                    /**
                     * 根据返回结果发送更新广播
                     */
                    sendCheckResult(appUpgradeDetail);
                } else {
                    if (initiativeCheck) {
                        UIActuator.post(() -> Toast.makeText(UpgradeService.this, R.string.upgrade_already_is_newest, Toast.LENGTH_SHORT).show());
                    }
                    PLog.e(TAG, "onSuccess: ---------------data null");
                }
            } else {
                if (initiativeCheck) {
                    UIActuator.post(() -> Toast.makeText(UpgradeService.this, R.string.upgrade_already_is_newest, Toast.LENGTH_SHORT).show());
                }
                stop();
                e.printStackTrace();
            }
        });
//        try {
//            JSONObject json = new JSONObject();
//            try {
//                json.put(PARAM_APP_UPDATE_VERSION, version_code);
//                json.put(PARAM_APP_UPDATE_SYSTEM, "android");
//                json.put(PARAM_APP_UPDATE_CHANNEL, AppUtil.getApkChannel(this));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            RequestTask.create(
//                    false,
//                    Constant.URI_APP_UPDATE,
//                    token,
//                    json,
//                    new TransportCallBack() {
//                        @Override
//                        public void onSuccess(String response) {
//                            AppUpgradeInfo.AppUpgradeDetail info = GsonHelper.getGson().fromJson(response,
//                                    AppUpgradeInfo.AppUpgradeDetail.class);
//                            if (info != null) {
//
//                                PLog.i(TAG, "onSuccess: " + info.toString());
//                                setAppUpgradeDetail(info);
//                                /**
//                                 * 根据返回结果发送更新广播
//                                 */
//                                sendCheckResult(info);
//                            } else {
//                                if (initiativeCheck) {
//                                    UIActuator.post(() -> Toast.makeText(UpgradeService.this, R.string.upgrade_already_is_newest, Toast.LENGTH_SHORT).show());
//                                }
//                                PLog.e(TAG, "onSuccess: ---------------data null");
//                            }
//                        }
//
//                        @Override
//                        public void onFailed(int code, String failed) {
//                            PLog.e(TAG, "onFailed:" + failed);
//                            if (initiativeCheck) {
//                                UIActuator.post(() -> Toast.makeText(UpgradeService.this, R.string.upgrade_already_is_newest, Toast.LENGTH_SHORT).show());
//                            }
//                            stop();
//                        }
//
//                        @Override
//                        public void onError(Exception e) {
//                            e.printStackTrace();
//                            stop();
//                        }
//                    }
//            ).execute();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /**
     * 下载APK
     */
    private void downloadNewApk() {
        if (prepareFile()) {
            long total = 0;//文件总长度
            long process = 0;  //文件下载进度
            lastProcess = 0;
            boolean downloadSwitch = true;
            while (downloadSwitch) {
                InputStream in = null;
                OutputStream out = null;
                HttpGet get = null;
                HttpEntity entity = null;

                if (process > 0 && process == total && mPackageFile.length() == total) {
                    /**
                     * 下载完成了退出循环
                     */
                    AppUpgradeInfo.AppUpgradeDetail detail = getAppUpgradeDetail();
                    if (detail != null) {
                        /**
                         * 校验MD5值
                         */
                        try {
                            String serverMD5 = detail.md5.trim().toLowerCase();
                            if (!TextUtils.isEmpty(serverMD5)) {
                                String realMD5 = AppUtil.getFileMd5(mPackageFile);
                                PLog.i("Upgrade", "realMD5=" + realMD5);
                                PLog.i("Upgrade", "serverMD5=" + serverMD5);
                                if (TextUtils.equals(serverMD5, realMD5)) {
                                    sendBroadCast(ACTION_FINISH_DOWNLOAD, process, total);
                                } else {
                                    sendBroadCast(ACTION_DOWNLOAD_ERROR, ERROR_CODE_CHECK_FILE, 0);
                                }
                            } else {
                                sendBroadCast(ACTION_DOWNLOAD_ERROR, ERROR_CODE_CHECK_FILE, 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            sendBroadCast(ACTION_DOWNLOAD_ERROR, ERROR_CODE_CHECK_FILE, 0);
                            break;
                        }
                    }
                    downloadSwitch = false;
                    continue;
                }

                try {
                    get = new HttpGet(apkUrl);
                    if (process > 0) {
                        get.addHeader("Range", "bytes=" + process + "-");
                    }

                    HttpResponse response = HttpUtil.getHttpClient().execute(get);
                    entity = response.getEntity();
                    long length = entity.getContentLength();
                    long newTotal = 0;
                    int state = response.getStatusLine().getStatusCode();
                    switch (state) {
                        case HttpStatus.SC_PARTIAL_CONTENT:
                            newTotal = length < 0 ? -1 : length + process;
                            break;
                        case HttpStatus.SC_OK:
                            newTotal = length;
                            break;
                        default:
                            /**
                             * 如果进入此处说明请求出错了
                             */
                            sendBroadCast(ACTION_DOWNLOAD_ERROR, ERROR_CDOE_NETWORK, 0);
                            break;
                    }

                    if (total >= 0 && newTotal != total) {
                        total = -1;
                        process = 0;
                        continue;
                    }

                    if (total < 0) {
                        total = newTotal;
                    }

                    sendBroadCast(ACTION_BEGIN_DOWNLOAD, process, total);

                    in = entity.getContent();
                    out = new FileOutputStream(mPackageFile,
                            state == HttpStatus.SC_PARTIAL_CONTENT);
                    byte[] buffer = new byte[16 * 1024];
                    int len = 0;
                    while ((len = in.read(buffer)) >= 0 && !mPause.get()) {
                        out.write(buffer, 0, len);
                        process += len;
                        sendBroadCast(ACTION_DOWNLOAD_PROCESS, process, total);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    sendBroadCast(ACTION_DOWNLOAD_ERROR, ERROR_CDOE_NETWORK, 0);
                    downloadSwitch = false;
                } finally {
                    MoreCloseables.closeQuietly(TAG, get);
                    MoreCloseables.closeQuietly(TAG, entity);
                    MoreCloseables.closeQuietly(TAG, in);
                    MoreCloseables.closeQuietly(TAG, out);
                }
            }
            stop();
        }
    }

    private void sendCheckResult(AppUpgradeInfo.AppUpgradeDetail detail) {
        if (detail != null) {
            Intent intent = new Intent(ACTION_NOTIFICATION);
            intent.putExtra(EXTRA_NOTIFICATION_TYPE, ACTION_CHECK_UPDATE);
            intent.putExtra(UPDATE_DETAIL, detail);
            intent.putExtra(Constant.INITIATIVE_CHECK, initiativeCheck);
            this.sendBroadcast(intent);
        }
    }

    private void sendBroadCast(int action, long process, long total) {

        switch (action) {
            case ACTION_BEGIN_DOWNLOAD: {
                /**
                 * 发送广播，通知下载开始
                 */
                Intent intent = new Intent(ACTION_NOTIFICATION);
                intent.putExtra(EXTRA_NOTIFICATION_TYPE, action);
                this.sendBroadcast(intent);
            }
            break;
            case ACTION_DOWNLOAD_PROCESS: {
                /**
                 * 计算下载进度，发送广播通知
                 */
                int curProcess = (int) (((float) process / total) * 100);
                /**
                 * 防止发送广播频繁，只有进度变更超过5时才发送进度广播
                 */
                if (curProcess - lastProcess >= 5) {
                    Intent intent = new Intent(ACTION_NOTIFICATION);
                    intent.putExtra(EXTRA_NOTIFICATION_TYPE, action);
                    intent.putExtra(PROCESS, curProcess);
                    this.sendBroadcast(intent);
                    lastProcess = curProcess;
                }
            }
            break;
            case ACTION_FINISH_DOWNLOAD: {
                /**
                 * 下载完毕，通知下载结束
                 */
                Intent intent = new Intent(ACTION_NOTIFICATION);
                intent.putExtra(DOWNLOAD_PATH, mPackageFile.getAbsolutePath());
                intent.putExtra(EXTRA_NOTIFICATION_TYPE, action);
                this.sendBroadcast(intent);
            }
            break;
            case ACTION_DOWNLOAD_ERROR: {
                /**
                 * 下载出错了
                 * 可能原因：
                 * 1 MD5错误----> ERROR_CODE_1
                 * 2 网络原因 |
                 * 3 程序报错 |---->ERROR_CODE_2;
                 * 4 其他错误 |
                 */
                Intent intent = new Intent(ACTION_NOTIFICATION);
                intent.putExtra(EXTRA_NOTIFICATION_TYPE, action);
                intent.putExtra(ERROR_CODE, (int) process);
                this.sendBroadcast(intent);
            }
            break;
        }
    }

    /**
     * 为下载Apk作准备，将原有文件删掉,并确保目录存在
     * 【将来优化措施：
     * 1.下载新apk时将原有apk文件进行备份，当新apk下载完成后，将旧apk文件删除】
     */
    private boolean prepareFile() {
        boolean result = true;
        File parent = mPackageFile.getParentFile();
        if (!parent.exists() || !parent.isDirectory()) {
            FileUtils.deletes(parent);
            parent.mkdir();
        }
        if (mPackageFile.exists() && mPackageFile.length() > 0) {
            /**
             * 说明文件已经有内容了，则删除原文件
             */
            mPackageFile.delete();
        }
        if (!mPackageFile.exists()) {
            try {
                mPackageFile.createNewFile();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }
        }
        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
