package orangelab.project.common.effect.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.androidtoolkit.PLog;
import com.toolkit.action.Destroyable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import orangelab.project.common.effect.event.RoomEffectConsoleViewEvent;
import orangelab.project.common.utils.SafeHandler;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.popwindow.EmotionPopWindow;
import orangelab.project.voice.popwindow.SmallGamePopWindow;

/**
 * game_werewolf
 * 2018/7/30 上午10:46
 * Mystery
 * <p>
 * 房间表情，控制台的操作ViewGroup
 */
public class RoomEffectsConsoleViewGroup extends FrameLayout implements Destroyable {

    private static final String TAG = "RoomEffectsConsoleViewGroup";
    private SafeHandler mSafeHandler = new SafeHandler();
    private EmotionPopWindow mEmotionPopWindow;
    private SmallGamePopWindow mSmallGamePopWindow;
    private RoomEffectsContext mRoomEffectsContext;

    public RoomEffectsConsoleViewGroup(@NonNull Context context) {
        this(context, null);
    }

    public RoomEffectsConsoleViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomEffectsConsoleViewGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        EventBus.getDefault().register(this);
    }

    public void bindEffectsContext(@NonNull RoomEffectsContext context) {
        mRoomEffectsContext = context;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RoomEffectConsoleViewEvent event) {
        if (event != null) {
            if (event.getType().equals(VoiceConstants.SHOW_EMOTION)) {
                showEmotionConsoleViewGroup();
            } else if (event.getType().equals(VoiceConstants.SHOW_GAME)) {
                showGameConsoleViewGroup();
            } else {
                PLog.e(TAG, "Event type is：" + event.getType());
            }
        }
    }

    private void showEmotionConsoleViewGroup() {
        destroyWindow();
        mEmotionPopWindow = new EmotionPopWindow(mRoomEffectsContext);
        mEmotionPopWindow.setFocusable(true);
        mEmotionPopWindow.setTouchable(true);
        mEmotionPopWindow.showAtLocation(this, Gravity.BOTTOM, 0, 0);
    }

    private void showGameConsoleViewGroup() {
        destroyWindow();
        mSmallGamePopWindow = new SmallGamePopWindow(mRoomEffectsContext);
        mSmallGamePopWindow.setFocusable(true);
        mSmallGamePopWindow.setTouchable(true);
        mSmallGamePopWindow.showAtLocation(this, Gravity.BOTTOM, 0, 0);
    }

    private void destroyWindow() {
        if (mEmotionPopWindow != null) {
            if (mEmotionPopWindow.isShowing()) {
                mEmotionPopWindow.dismiss();
            }
            mEmotionPopWindow.destroy();
            mEmotionPopWindow = null;
        }

        if (mSmallGamePopWindow != null) {
            if (mSmallGamePopWindow.isShowing()) {
                mSmallGamePopWindow.dismiss();
            }
            mSmallGamePopWindow.destroy();
            mSmallGamePopWindow = null;
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    @Override
    public void destroy() {
        EventBus.getDefault().unregister(this);
        destroyWindow();
        mSafeHandler.release();
    }
}
