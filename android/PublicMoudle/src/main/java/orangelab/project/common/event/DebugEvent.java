package orangelab.project.common.event;

/**
 * 系统名称:
 * 模块名称:
 * 功能说明:
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 */

public class DebugEvent {

    public static class DeveloperIceFailedEvent {

    }

    public static class DeveloperShowDebugEvent {

    }

    public static class DeveloperShowDeveloperEvent {

    }

    public static class DeveloperSocketIsDisConnect {

    }

    public static class DeveloperSocketIsReConnect {

    }
}
