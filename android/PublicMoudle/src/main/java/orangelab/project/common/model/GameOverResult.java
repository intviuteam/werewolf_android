package orangelab.project.common.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:1727
 * 2017/3/13
 */

public class GameOverResult {

    public String win_type;
    public List<Integer> winners;
    public Map<String, List<GameOverResultRoleItem>> roles;
    public Map<String, Integer> experience;

    public static class GameOverResultRoleItem {
        public int position;
        public String role;
        public String status;

        @Override
        public String toString() {
            return "GameOverResultRoleItem{" +
                    "position=" + position +
                    ", role='" + role + '\'' +
                    ", status='" + status + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GameOverResult{" +
                "win_type='" + win_type + '\'' +
                ", winners=" + winners +
                ", roles=" + roles +
                '}';
    }
}
