package orangelab.project.common.exhibition.card;

import android.text.TextUtils;

import com.androidtoolkit.RxToolKit;
import orangelab.project.common.exhibition.ExhibitionPackage;
import orangelab.project.common.exhibition.ExhibitionsConstant;
import orangelab.project.common.exhibition.IExhibition;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/6/27 下午1:22
 * Mystery
 */

public class Card implements IExhibition {

    private int mExhibitionBackGroundResource = 0;
    private String mCardType;
    private String mCardName;
    private String mCardPriceType;
    private int mCardPrice;
    private int mCardCount;
    private String mCardTicket = "";

    public static final int CARD_DEFAULT_WIDTH = ScreenUtils.dip2px(40);
    public static final int CARD_DEFAULT_HEIGHT = ScreenUtils.dip2px(56);

    public Card(String cardType, String cardPriceType, int cardPrice) {
        this(cardType, cardPriceType, cardPrice, ExhibitionsConstant.GIFT_COUNT_NONE);
    }

    public Card(String cardType, String cardPriceType, int cardPrice, int cardCount) {
        mCardType = cardType;
        mCardPriceType = cardPriceType;
        mCardPrice = cardPrice;
        mCardName = CardUtils.getCardName(mCardType);
        mExhibitionBackGroundResource = CardUtils.getCardBgResource(mCardType);
        mCardCount = cardCount;
    }

    @Override
    public void decCount() {
        mCardCount--;
    }

    @Override
    public String getExhibitionType() {
        return ExhibitionsConstant.EXHIBITIONS_TYPE_CARD;
    }

    @Override
    public String getRealType() {
        return mCardType;
    }

    @Override
    public int getExhibitionBackGroundResource() {
        return mExhibitionBackGroundResource;
    }

    @Override
    public String getExhibitionName() {
        return mCardName;
    }

    @Override
    public String getExhibitionPriceType() {
        return mCardPriceType;
    }

    @Override
    public int getExhibitionPrice() {
        return mCardPrice;
    }

    @Override
    public void present(ExhibitionPackage exhibitionPackage) {
        if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_GAME)) {
            /**
             * 游戏中送礼物
             */
            RxToolKit.Emit(new CardEvent.CardForGameWrapperEvent(Card.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_CHAT)) {
            /**
             * 聊天中送礼物
             */
            RxToolKit.Emit(new CardEvent.CardForChatWrapperEvent(Card.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_MAIN_PAGE)) {
            /**
             * 个人详情页面送礼物
             */
            RxToolKit.Emit(new CardEvent.CardForMainPageWrapperEvent(Card.this, exhibitionPackage));
        } else if (TextUtils.equals(exhibitionPackage.getFromType(), ExhibitionsConstant.FROM_OUT_SIDER)) {
            /**
             * 外部送礼物
             */
            RxToolKit.Emit(new CardEvent.CardForOutSideWrapperEvent(Card.this, exhibitionPackage));
        }
    }

    @Override
    public int getExhibitionCount() {
        return mCardCount;
    }

    @Override
    public void setExhibitionTicket(String ticket) {
        this.mCardTicket = ticket;
    }

    @Override
    public void setPopularValue(int value) {

    }

    @Override
    public int getPopularValue() {
        return 0;
    }

    @Override
    public String getDisCount() {
        return "";
    }

    @Override
    public void setDisCount(String disCount) {

    }

    public String getCardTicket() {
        return mCardTicket;
    }

    @Override
    public String toString() {
        return "Card{" +
                "mCardType='" + mCardType + '\'' +
                ", mCardName='" + mCardName + '\'' +
                ", mCardPriceType='" + mCardPriceType + '\'' +
                ", mCardPrice=" + mCardPrice +
                ", mCardCount=" + mCardCount +
                '}';
    }
}
