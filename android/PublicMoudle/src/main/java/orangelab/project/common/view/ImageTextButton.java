package orangelab.project.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.R;

import com.androidtoolkit.view.ScreenUtils;

/**
 * Created by lloydfinch on 11/10/2017.
 */
//TODO 完善image位置
public class ImageTextButton extends android.support.v7.widget.AppCompatButton {

    private static final int DEFAULT_IMAGE_SIZE = 30;
    private static final int DEFAULT_PADDING = 5;

    private int imageId;
    private int imageWidth;
    private int imageHeight;
    private int imageLeft;


    public ImageTextButton(Context context) {
        super(context);
    }

    public ImageTextButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ImageTextButton);
        imageId = array.getResourceId(R.styleable.ImageTextButton_btn_image, 0);
        int defaultSize = ScreenUtils.dip2px(DEFAULT_IMAGE_SIZE);
        imageWidth = (int) array.getDimension(R.styleable.ImageTextButton_btn_image_width, defaultSize);
        imageHeight = (int) array.getDimension(R.styleable.ImageTextButton_btn_image_height, defaultSize);
        int defaultPadding = ScreenUtils.dip2px(DEFAULT_PADDING);
        imageLeft = (int) array.getDimension(R.styleable.ImageTextButton_btn_image_text_distance, defaultPadding);
        array.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBitmap(canvas);
        super.onDraw(canvas);
    }

    private void drawBitmap(Canvas canvas) {
        try {
            Bitmap bitmap = getRequestBitmap();
            float contentWidth = bitmap.getWidth() + imageLeft + (this.getTextSize() * this.getText().length());
            float x = (this.getMeasuredWidth() - contentWidth) / 2;
            float y = (this.getMeasuredHeight() - bitmap.getHeight()) / 2;
            canvas.drawBitmap(bitmap, x, y, new Paint());
            float dX = (bitmap.getWidth() + imageLeft) / 2 + imageLeft;
            canvas.translate(dX, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getRequestBitmap() {
        int defaultSize = ScreenUtils.dip2px(DEFAULT_IMAGE_SIZE);
        if (imageWidth <= 0 || imageHeight <= 0 || imageWidth == defaultSize || imageHeight == defaultSize) {
            return BitmapFactory.decodeResource(getContext().getResources(), imageId);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getContext().getResources(), imageId, options);
        options.inSampleSize = calculateSampleSize(options, imageWidth, imageHeight);
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeResource(getContext().getResources(), imageId, options);
    }

    private int calculateSampleSize(BitmapFactory.Options options, int desWidth, int desHeight) {
        int sampleSize = 1;
        int width = options.outWidth;
        int height = options.outHeight;
        while (width > desWidth || height > desHeight) {
            sampleSize <<= 1;
            width >>= 1;
            height >>= 1;
        }

        return sampleSize;
    }
}
