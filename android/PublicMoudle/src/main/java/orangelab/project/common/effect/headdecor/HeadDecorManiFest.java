package orangelab.project.common.effect.headdecor;

import android.text.TextUtils;

import com.toolkit.ToolKit;
import com.toolkit.action.Keepable;

import java.util.List;

/**
 * game_werewolf
 * 2018/7/11 下午4:26
 * Mystery
 * <p>
 * 头像框清单文件
 */
public class HeadDecorManiFest implements Keepable {

    public int rev = 0;
    public List<HeadDecorManiFestItem> avatar_boxs;

    /**
     * 头像框Item信息
     */
    public static class HeadDecorManiFestItem implements Keepable {
        public String name;
        public String image;
        public String open_status;
        public int status;
        public String avatar_box_type;
        /**
         * 头像框特效数据
         */
        public HeadDecorAnimData animate;
    }

    public HeadDecorManiFestItem getItem(String type) {
        if (ToolKit.isEmpty(avatar_boxs)) return null;

        for (HeadDecorManiFestItem temp : avatar_boxs) {
            if (TextUtils.equals(temp.avatar_box_type, type)) {
                return temp;
            }
        }

        return null;
    }
}
