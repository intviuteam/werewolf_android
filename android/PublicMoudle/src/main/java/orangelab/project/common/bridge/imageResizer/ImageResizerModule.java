package orangelab.project.common.bridge.imageResizer;

import android.net.Uri;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.zxy.tiny.Tiny;
import com.zxy.tiny.callback.FileCallback;

/**
 * Created by wangxu on 2017/7/25.
 */

class ImageResizerModule extends ReactContextBaseJavaModule {
    public final static String FILE_PREFIX = "file:";

    public ImageResizerModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    /**
     * @return the name of this module. This will be the name used to {@code require()} this module
     * from javascript.
     */
    @Override
    public String getName() {
        return "ImageResizerAndroid";
    }

    @ReactMethod
    public void createResizedImage(String imagePath, int newWidth, int newHeight, String compressFormat,
                                   int quality, int rotation, String outputPath, final Callback successCb, final Callback failureCb) {
        try {
            Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();
            Tiny.getInstance().source(Uri.parse(imagePath)).asFile().withOptions(options).compress(new FileCallback() {
                @Override
                public void callback(boolean isSuccess, String outfile) {
                    //return the compressed file path
                    if (isSuccess) {
                        if (!outfile.startsWith(FILE_PREFIX)) {
                            outfile = FILE_PREFIX + outfile;
                        }
                        successCb.invoke(outfile);
                    } else {
                        failureCb.invoke("resize image error");
                    }
                }
            });
        } catch (Exception e) {
            failureCb.invoke(e.getMessage());
        }
    }

}
