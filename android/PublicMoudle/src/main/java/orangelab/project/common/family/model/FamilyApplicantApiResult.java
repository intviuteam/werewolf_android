package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;
import orangelab.project.common.model.UserInfoResult;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/10/27 下午5:27
 * Mystery
 */

public class FamilyApplicantApiResult implements Keepable {

    public ArrayList<UserInfoResult> datas;
}
