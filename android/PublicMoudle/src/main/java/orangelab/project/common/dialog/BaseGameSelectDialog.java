package orangelab.project.common.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.R;
import orangelab.project.game.dialog.IGameDialog;
import orangelab.project.common.utils.CountDownView;
import com.androidtoolkit.PLog;

/**
 * Author:1727
 * 2017/3/15
 */

public class BaseGameSelectDialog extends BaseCustomDialog implements CountDownView.CountDownFinish, IGameDialog {

    private static final String TAG = "BaseGameSelectDialog";

    private TextView title;
    private TextView countView;
    private CountDownView countDownView;

    public BaseGameSelectDialog(Context context) {
        super(context);
        View titleView = LayoutInflater.from(context).inflate(R.layout.layout_game_dialog_title, null);
        title = (TextView) titleView.findViewById(R.id.dialog_title);
        countView = (TextView) titleView.findViewById(R.id.dialog_count_down);
        addTitleView(titleView);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    public void setCustomView(View view) {
        addContentView(view);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void showDialog(int seconds) {
        if (seconds >= 0) {
            showDialog();
            if (seconds != 0) {
                countView.setVisibility(View.VISIBLE);
                countDownView = new CountDownView(countView, seconds, this);
                countDownView.start();
            } else {
                countView.setVisibility(View.GONE);
            }
        } else {
            /**
             * 如果传入负数则不显示对话框
             */
            PLog.i(TAG, "Attention seconds is negative");
        }
    }

    public void cancel() {
        if (countDownView != null) {
            countDownView.onFinish();
            countDownView = null;
        }
    }

    @Override
    public void onFinish() {
        if (!isDestroy()) {
            if (countDownView != null) {
                countDownView.cancel();
            }
            dismiss();
        }
    }
}
