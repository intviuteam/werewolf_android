package orangelab.project.common.pay.google.v2;

import android.app.Activity;
import android.util.Log;

import com.android.billingclient.api.Purchase;
import com.androidtoolkit.PLog;
import com.androidtoolkit.ThreadToolKit;
import com.datasource.GlobalUserState;
import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.EmptyRequestTaskCallBack;
import com.networktoolkit.transport.RequestTask;
import com.toolkit.action.Destroyable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import orangelab.project.MainApplication;
import orangelab.project.common.db.GooglePaySubsOrderAssitHelper;
import orangelab.project.common.db.UserPayOrderDaoV2Helper;

/**
 * game_werewolf
 * 2018/6/21 下午2:52
 * Mystery
 */
public class GooglePaySubsV2Task implements BillingManager.BillingManagerObserver, Destroyable {

    private static final String TAG = "GooglePayV2SubsTask";

    private BillingManager mBillingManager;

    public GooglePaySubsV2Task(Activity activity) {
        mBillingManager = new BillingManager(activity, this);
    }

    public void startTask() {
        mBillingManager.queryPurchasesSubs();
    }

    @Override
    public void destroy() {
        mBillingManager.destroy();
    }

    @Override
    public void onSetupFinished() {

    }

    @Override
    public void onSetupError(int errorCode) {

    }

    @Override
    public void onPurchasesQuery(List<Purchase> purchases) {
        PLog.i(TAG, "onPurchasesQuery: " + purchases);

        ThreadToolKit.Async(() -> {
            final UserPayOrderDaoV2Helper orderDaoHelper = MainApplication.getInstance().getUserPayOrderDaoV2Helper();
            if (orderDaoHelper == null) return;

            final GooglePaySubsOrderAssitHelper orderAssitHelper = MainApplication.getInstance().getGooglePaySubsOrderAssitHelper();
            if (orderAssitHelper == null) return;

            for (Purchase purchase : purchases) {
                try {

                    final String orderId = purchase.getOrderId();
                    if (orderDaoHelper.containsOrderId(orderId)) {
                        PLog.i(TAG, orderId + " has contains in OrderDB");
                        continue;
                    }
                    if (orderAssitHelper.contains(orderId)) {
                        PLog.i(TAG, orderId + " has contains in OrderAssitDB");
                        continue;
                    }

                    JSONObject jsonObject = new JSONObject(purchase.getOriginalJson());
                    JSONObject requestJson = new JSONObject();
                    requestJson.put("receipt", jsonObject);
                    RequestTask.create()
                            .isGet(false)
                            .request(Constant.GOOGLE_SUBS_PAY)
                            .withToken(GlobalUserState.getGlobalState().getToken())
                            .load(requestJson)
                            .action(new EmptyRequestTaskCallBack() {
                                @Override
                                public void onError(Exception e) {
                                    PLog.i(TAG, orderId + " report error");
                                }

                                @Override
                                public void onFailed(int code, String failed) {
                                    PLog.i(TAG, orderId + " report error");
                                }

                                @Override
                                public void onSuccess(String response) {
                                    PLog.i(TAG, orderId + " report success");
                                    orderAssitHelper.insert(orderId);
                                }
                            }).build().executeInCurThread();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onPurchasesQueryError(int errorCode) {
        Log.i(TAG, "onPurchasesQueryError: " + errorCode);
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        Log.i(TAG, "onPurchasesUpdated: " + purchases);
    }

    @Override
    public void onPurchaseFlowError(int errorCode) {
        Log.i(TAG, "onPurchaseFlowError: " + errorCode);
    }

    @Override
    public void onPurchasesUpdatedError(int errorCode) {
        Log.i(TAG, "onPurchasesUpdatedError: " + errorCode);
    }

    @Override
    public void onConsumeSuccess(String token) {

    }

    @Override
    public void onConsumeError(int errorCode) {

    }
}
