package orangelab.project.common.exhibition;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2017/6/28 下午4:56
 * Mystery
 */

public class ExhibitionPackage {

    private String mFromType;
    private int mFromPosition;
    private int mToPosition;
    private UserData mToUserData;
    private String mFromUserId;
    private String displayType;

    /**
     * 用于游戏内部打包包裹
     *
     * @param fromPosition
     * @param toPosition
     */
    public ExhibitionPackage(String fromType, int fromPosition, int toPosition, UserData userData) {
        mFromType = fromType;
        mFromPosition = fromPosition;
        mToPosition = toPosition;
        mToUserData = userData;
    }

    public ExhibitionPackage(String fromType, String fromUserId, UserData toUserData) {
        mFromType = fromType;
        mToUserData = toUserData;
        mFromUserId = fromUserId;
    }

    public ExhibitionPackage(String fromType, int fromPosition, int toPosition, UserData userData, String displayType) {
        mFromType = fromType;
        mFromPosition = fromPosition;
        mToPosition = toPosition;
        mToUserData = userData;
        this.displayType = displayType;
    }

    public UserData getToUserData() {
        return mToUserData;
    }

    public int getFromPosition() {
        return mFromPosition;
    }

    public String getFromUserId() {
        return mFromUserId;
    }

    public int getToPosition() {
        return mToPosition;
    }

    public String getToUserId() {
        return mToUserData.userId;
    }

    public String getFromType() {
        return mFromType;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }
}
