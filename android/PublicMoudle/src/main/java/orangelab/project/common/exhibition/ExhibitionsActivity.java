package orangelab.project.common.exhibition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.androidtoolkit.RxToolKit;

import orangelab.project.common.activity.SafeActivity;

import com.androidtoolkit.PLog;

import orangelab.thirdparty.leancloud.chatkit.utils.UserData;

/**
 * game_werewolf
 * 2017/6/20 下午5:23
 * Mystery
 */

//TODO 这里需要进行一次重构，参数合并成一个实体，避免过多的重载
public class ExhibitionsActivity extends SafeActivity {

    public static class Helper {
        public static void setUserData(Intent intent, UserData userData) {
            intent.putExtra(ExhibitionsConstant.TO_USER_DATA, userData);
        }

        public static UserData getUserData(Intent intent) {
            return intent.getParcelableExtra(ExhibitionsConstant.TO_USER_DATA);
        }

        public static void setFromType(Intent intent, String type) {
            intent.putExtra(ExhibitionsConstant.FROM_TYPE, type);
        }

        public static String getFromType(Intent intent) {
            return intent.getStringExtra(ExhibitionsConstant.FROM_TYPE);
        }

        public static void setToUserPosition(Intent intent, int pointF) {
            intent.putExtra(ExhibitionsConstant.USER_POSITION, pointF);
        }

        public static int getToUserPosition(Intent intent) {
            return intent.getIntExtra(ExhibitionsConstant.USER_POSITION, 1);
        }

        public static void setToUserName(Intent intent, String name) {
            intent.putExtra(ExhibitionsConstant.USER_NAME, name);
        }

        public static String getToUserName(Intent intent) {
            return intent.getStringExtra(ExhibitionsConstant.USER_NAME);
        }

        public static void setToUserId(Intent intent, String userId) {
            intent.putExtra(ExhibitionsConstant.TO_USER_ID, userId);
        }

        public static String getToUserId(Intent intent) {
            return intent.getStringExtra(ExhibitionsConstant.TO_USER_ID);
        }

        public static void setDisplayType(Intent intent, String displayType) {
            intent.putExtra(ExhibitionsConstant.DISPLAY_TYPE, displayType);
        }

        public static String getDisplayType(Intent intent) {
            return intent.getStringExtra(ExhibitionsConstant.DISPLAY_TYPE);
        }

    }

    private static final String TAG = "ExhibitionsActivity";
    private static final String DIALOG_TAG = "ExhibitionsActivity";

    private ExhibitionsFragment giftDialogFragment;
    private String mActivityKey = TAG + Integer.toString(hashCode());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initListener();
        FragmentManager fragmentManager = getSupportFragmentManager();
        giftDialogFragment = (ExhibitionsFragment) fragmentManager.findFragmentByTag(DIALOG_TAG);
        if (giftDialogFragment == null) {
            giftDialogFragment = new ExhibitionsFragment();
        }
        giftDialogFragment.setArguments(getIntent().getExtras());
        if (!isFinishing() && giftDialogFragment != null && !giftDialogFragment.isAdded()) {
            fragmentManager.beginTransaction().add(giftDialogFragment, DIALOG_TAG).commitAllowingStateLoss();
        }
    }

    private void initListener() {
        initDialogListener();
    }

    private void initDialogListener() {
        RxToolKit.Build(mActivityKey, ExhibitionsEvent.ExhibitionsShowCaseEvent.class).action(giftDialogEvent -> {
            runOnUiThreadSafely(() -> {
                switch (giftDialogEvent.getAction()) {
                    case ExhibitionsEvent.ExhibitionsShowCaseEvent.ACTION_CANCEL:
                        finish();
                        break;
                    case ExhibitionsEvent.ExhibitionsShowCaseEvent.ACTION_DISMISS:
                        finish();
                        break;
                }
            });
        }).register();
    }

    @Override
    public void finish() {
        super.finish();
        PLog.i(TAG, "finish()");
        RxToolKit.UnRegister(mActivityKey);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PLog.i(TAG, "onDestroy()");
        RxToolKit.UnRegister(mActivityKey);
    }

    public static void startActivityFromGame(
            Context context, String toUserId, String toUserName, int toUserSex, String toUserIv, int toUserPosition) {
        Intent intent = new Intent(context, ExhibitionsActivity.class);
        UserData userData = UserData.createUserData(toUserId, toUserName, toUserSex, toUserIv);
        Helper.setUserData(intent, userData);
        Helper.setToUserPosition(intent, toUserPosition);
        Helper.setFromType(intent, ExhibitionsConstant.FROM_GAME);
        context.startActivity(intent);
    }

    public static void startActivityFromMainPage(
            Context context, String toUserId, String toUserName, int toUserSex, String toUserIv) {
        Intent intent = new Intent(context, ExhibitionsActivity.class);
        UserData userData = UserData.createUserData(toUserId, toUserName, toUserSex, toUserIv);
        Helper.setUserData(intent, userData);
        Helper.setFromType(intent, ExhibitionsConstant.FROM_MAIN_PAGE);
        context.startActivity(intent);
    }

    public static void startActivityFromOutSide(Context context, String toUserId, String toUserName, int toUserSex, String toUserIv) {
        Intent intent = new Intent(context, ExhibitionsActivity.class);
        UserData userData = UserData.createUserData(toUserId, toUserName, toUserSex, toUserIv);
        Helper.setUserData(intent, userData);
        Helper.setFromType(intent, ExhibitionsConstant.FROM_OUT_SIDER);
        context.startActivity(intent);
    }

    public static void startActivityFromChat(
            Context context, String toUserId, String toUserName, int toUserSex, String toUserIv) {
        Intent intent = new Intent(context, ExhibitionsActivity.class);
        UserData userData = UserData.createUserData(toUserId, toUserName, toUserSex, toUserIv);
        Helper.setUserData(intent, userData);
        Helper.setFromType(intent, ExhibitionsConstant.FROM_CHAT);
        context.startActivity(intent);
    }

    /**
     * 添加一个类型的方法
     * displayType: sing(点击ktv房间播放器上的头像)/seat(正常送礼)
     */
    public static void startActivityFromGame(
            Context context, String toUserId, String toUserName, int toUserSex, String toUserIv, int toUserPosition, String displayType) {
        Intent intent = new Intent(context, ExhibitionsActivity.class);
        UserData userData = UserData.createUserData(toUserId, toUserName, toUserSex, toUserIv);
        Helper.setUserData(intent, userData);
        Helper.setToUserPosition(intent, toUserPosition);
        Helper.setFromType(intent, ExhibitionsConstant.FROM_GAME);
        Helper.setDisplayType(intent, displayType);
        context.startActivity(intent);
    }
}
