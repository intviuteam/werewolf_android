package orangelab.project.common.engine;

/**
 * game_werewolf
 * 2018/4/16 上午10:40
 * Mystery
 */

public interface GlobalSocketConstant {

    String JOIN_ROOM = "join_room";

    String INVITE = "invite";
    String BE_INVITED = "be_invited";
    String INVITED_SUCCESS = "invited_success";
    String INVITE_ACCEPT = "accept_invite";
    String INVITE_VAILD = "invite_is_vaild";
    String INVITE_CANCEL = "cancel_invite";
    String INVITE_REFUSE = "refuse_invite";
    String INVITE_REFUSE_SERVER = "invite_be_refused";

    String INVITE_USER_ID = "invite_user_id";

    String PRIVATE_ROOM_TYPE = "pri_room";
    String PRIVATE_ROOM_SERVER_TYPE = "pri_room_server";

    String TARGET_USER_ID = "target_user_id";
    String WAIT_TIMER = "wait_timer";
    String ROOM_TYPE = "room_type";

    String MINI_GAME_PLAY_TYPE = "play_mini_game";
    String MINI_GAME_PLAY_SERVER_TYPE = "play_mini_game_server";

    String NOTIFY_SERVER = "notify_server";
    String MG_WT_ST = "mg_wt_st";

    String MULTI_PRI_ROOM = "multi_pri_room";
    String MULTI_PRI_ROOM_SERVER = "multi_pri_room_server";

}
