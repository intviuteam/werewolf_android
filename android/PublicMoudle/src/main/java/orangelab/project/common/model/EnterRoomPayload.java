package orangelab.project.common.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/5/19 下午12:06
 * Mystery
 */

public class EnterRoomPayload implements Parcelable {
    public String token;
    public String password;
    public String gameType;
    public String roomId;
    public String userName;
    public String userId;
    public int userSex;
    public String avatar;
    public int exp;
    public String socketUrl;
    public String from = "";

    public String inviter = "";
    public String invitee = "";

    public static EnterRoomPayload CreateSimplePayload(String roomId, String roomType, String password) {
        EnterRoomPayload payload = new EnterRoomPayload();
        payload.password = password;
        payload.roomId = roomId;
        payload.gameType = roomType;
        payload.token = GlobalUserState.getGlobalState().getToken();
        payload.userId = GlobalUserState.getGlobalState().getUserId();
        return payload;
    }

    public EnterRoomPayload() {
    }

    protected EnterRoomPayload(Parcel in) {
        token = in.readString();
        password = in.readString();
        gameType = in.readString();
        roomId = in.readString();
        userName = in.readString();
        userId = in.readString();
        userSex = in.readInt();
        avatar = in.readString();
        exp = in.readInt();
        socketUrl = in.readString();
        from = in.readString();
        inviter = in.readString();
        invitee = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(password);
        dest.writeString(gameType);
        dest.writeString(roomId);
        dest.writeString(userName);
        dest.writeString(userId);
        dest.writeInt(userSex);
        dest.writeString(avatar);
        dest.writeInt(exp);
        dest.writeString(socketUrl);
        dest.writeString(from);
        dest.writeString(inviter);
        dest.writeString(invitee);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EnterRoomPayload> CREATOR = new Creator<EnterRoomPayload>() {
        @Override
        public EnterRoomPayload createFromParcel(Parcel in) {
            return new EnterRoomPayload(in);
        }

        @Override
        public EnterRoomPayload[] newArray(int size) {
            return new EnterRoomPayload[size];
        }
    };

    @Override
    public String toString() {
        return "EnterRoomPayload{" +
                "token='" + token + '\'' +
                ", password='" + password + '\'' +
                ", gameType='" + gameType + '\'' +
                ", roomId='" + roomId + '\'' +
                ", userName='" + userName + '\'' +
                ", userId='" + userId + '\'' +
                ", userSex=" + userSex +
                ", avatar='" + avatar + '\'' +
                ", exp=" + exp +
                ", socketUrl='" + socketUrl + '\'' +
                ", from='" + from + '\'' +
                ", inviter='" + inviter + '\'' +
                ", invitee='" + invitee + '\'' +
                '}';
    }
}
