package orangelab.project.common.utils;

import android.content.Context;

import com.androidtoolkit.PLog;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

/**
 * 系统名称:ReportEventUtils
 * 模块名称:ReportEventUtils
 * 功能说明:ReportEventUtils
 * 版本:
 * <p>
 * 修订纪录:
 * [格式  日期       修改人  备注
 * 例如: 2017-04-05|1727|重构XXXX
 * ]
 * 2017-04-05 1727 事件统计
 */

public class ReportEventUtils {

    private static final String TAG = "ReportEventUtils";
    private static Context mContext = null;

    private static final String EVENT_GAME_ENTER = "EVENT_GAME_ENTER";
    private static final String EVENT_GAME_LEAVE = "EVENT_GAME_LEAVE";
    private static final String EVENT_GAME_LEAVE_ISGAMING = "EVENT_GAME_LEAVE_ISGAMING";

    public static void bindContext(Context context) {
        mContext = context;
    }

    public static void reportError(Exception e) {
        try {
            MobclickAgent.reportError(mContext, e);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void reportError(Throwable e) {
        reportError(new Exception(e));
    }

    /**
     * 进入游戏
     */
    public static void reportGameEnter() {
        PLog.i(TAG, "reportGameEnter: ");
        try {
            MobclickAgent.onEvent(mContext, EVENT_GAME_ENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportEvent(String eventId, HashMap map) {
        try {
            MobclickAgent.onEvent(mContext, eventId, map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 汇报赠送礼物
     *
     * @param type
     */
    public static void reportGiftEvent(String type) {
        reportType(type);
    }

    public static void reportTypeNotHit(String type) {
        reportType(type + "_Not_Hit");
    }

    public static void reportTotalCount() {
        reportType("SocketMsgCount");
    }

    public static void reportNotHitCount() {
        reportType("SocketMsgNotHitCount");
    }

    public static void reportTotal(String type) {
        reportType(type + "_Total");
    }

    public static void reportType(String type) {
        try {
            MobclickAgent.onEvent(mContext, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportException(String msg) {
        try {
            MobclickAgent.reportError(mContext, new Exception(msg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 离开游戏
     */
    public static void reportGameLeave() {
        PLog.i(TAG, "reportGameLeave: ");
        try {
            MobclickAgent.onEvent(mContext, EVENT_GAME_LEAVE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportGameLeaveIsGaming() {
        PLog.i(TAG, "reportGameLeaveIsGaming");
        try {
            MobclickAgent.onEvent(mContext, EVENT_GAME_LEAVE_ISGAMING);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportActivityResume(Context context) {
        try {
            MobclickAgent.onResume(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reportActivityPause(Context context) {
        try {
            MobclickAgent.onPause(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 统计保存次数
     */
    public static void reportSaveChangeFace(String type) {
        reportType(type + "_save_change_face");
    }

    /**
     * 统计上传次数
     */
    public static void reportUpLoadChangeFace(String type) {
        reportType(type + "_upload_change_face");
    }

    /**
     * 统计分享次数
     */
    public static void reportShareChangeFace(String type) {
        reportType(type + "_share_change_face");
    }

    /**
     * 统计没有收到退出ktv房间事件的次数
     */
    public static void reportReceivedExitKtvEvent() {
        reportType("KTV_event_received_exit_ktv_room");
    }

    /**
     * 统计ktv回来连接语音服务器失败次数
     */
    public static void reportGetAudioFailureInKtv() {
        reportType("KTV_event_get_audio_record_failure_count_in_ktv");
    }
}
