package orangelab.project.common.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.R;
import orangelab.project.common.dialog.MsgDialog;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

/**
 * game_werewolf
 * 2017/7/11 上午10:39
 * Mystery
 */

public class HintActivity extends SafeActivity {
    private static final String TITLE = "TITLE";
    private static final String MESSAGE = "MESSAGE";

    private MsgDialog mMsgDialog;
    private String mMsgTitle = MessageUtils.getString(R.string.dialog_hint);
    private String mMsgContent = MessageUtils.getString(R.string.exit_app_by_leancloud_change);

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, android.R.color.transparent);
        if (getIntent() != null) {
            String title = getIntent().getStringExtra(TITLE);
            String content = getIntent().getStringExtra(MESSAGE);
            if (!TextUtils.isEmpty(title)) {
                mMsgTitle = title;
            }
            if (!TextUtils.isEmpty(content)) {
                mMsgContent = content;
            }
        }
        mMsgDialog = new MsgDialog(
                this,
                mMsgTitle,
                mMsgContent,
                v -> {
                    Utils.exitApp();
                    mMsgDialog.dismiss();
                }
        );
        mMsgDialog.showDialog();
    }

    public static void showHint(Context context) {
        context.startActivity(new Intent(context, HintActivity.class));
    }

    public static void showHint(Context context, String title, String message) {
        Intent intent = new Intent(context, HintActivity.class);
        intent.putExtra(TITLE, title);
        intent.putExtra(MESSAGE, message);
        context.startActivity(intent);
    }
}
