package orangelab.project.common.share;

/**
 * game_werewolf
 * 2018/2/26 下午7:35
 * Mystery
 */

public interface CallBack<T> {

    void onStart();

    void onSuccess(T result);

    void onError(Exception e);
}
