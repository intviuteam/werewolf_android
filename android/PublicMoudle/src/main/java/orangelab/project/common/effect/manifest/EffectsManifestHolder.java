package orangelab.project.common.effect.manifest;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.datasource.cache.DiskLruCacheManager;
import com.toolkit.ToolKit;

import java.io.File;
import java.io.InputStream;
import java.util.Set;

import orangelab.project.MainApplication;
import orangelab.project.common.effect.ApkResourceManager;

/**
 * game_werewolf
 * 2018/7/12 下午8:19
 * Mystery
 */
abstract class EffectsManifestHolder<T> {

    private static final String TAG = "EffectsManifestHolder";

    protected Object lock = new Object();
    protected T mQuickData;

    protected ManifestManagerHelper mHelper;
    protected String mCacheManifestFile;
    protected String mCacheManifestVersionFile;
    protected String mDiskLruType;

    public EffectsManifestHolder(
            ManifestManagerHelper helper,
            String cacheManifestFile,
            String cacheManifestVersionFile,
            String diskLruType) {
        mHelper = helper;
        mCacheManifestFile = cacheManifestFile;
        mCacheManifestVersionFile = cacheManifestVersionFile;
        mDiskLruType = diskLruType;
    }

    public abstract T transformData(String data);

    public abstract void saveManifest(T data);

    public abstract Set<String> verifyManifest(T data);

    public abstract void downloadManifestResource(T data, boolean forceRefresh);

    public void putVersion(int value) {
        mHelper.putVersionIntoCache(mCacheManifestVersionFile, value);
    }

    public int getVersion() {
        return mHelper.getVersionFromCache(mCacheManifestVersionFile);
    }

    public void updateManifest(T data) {
        synchronized (lock) {
            mQuickData = data;
        }
    }


    public String getManifestDataString() {
        return mHelper.getAsString(mCacheManifestFile);
    }

    public T getManifestData() {
        T result = null;
        synchronized (lock) {
            result = mQuickData;
            if (result == null) {
                try {
                    mQuickData = transformData(getManifestDataString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                result = mQuickData;
            }
        }
        return result;
    }

    public void downloadAndSaveIfNeed(String local, String remote, boolean force) {
        if (ApkResourceManager.GetApkImageResource(local) > 0) {
            return;
        } else {
            downloadUrlIntoCache(remote, force);
        }
    }

    public void downloadAndSaveIfNeedForGif(String local, String remote, boolean force) {
        if (ApkResourceManager.GetApkAssetInputStream(local) != null) {
            return;
        } else {
            downloadUrlIntoCache(remote, force);
        }
    }

    private void downloadUrlIntoCache(String url, boolean forceRefresh) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        /**
         * 这里加一个判断，如果传入强制刷新，那么就需要强制下载，否则则判断是否有这个图片存在
         */
        if (!forceRefresh) {
            if (findFileByUrl(url)) {
                /**
                 * 已经有了这个图片就不需要下载了
                 */
                PLog.i(TAG, "this [" + url + "] picture has been download");
                return;
            }
        }
        /**
         * 下载这个图片
         */
        if (DiskLruCacheManager.aCache.putImageUrlIntoCache(mDiskLruType, url)) {
            PLog.i(TAG, "this [" + url + "] download success!");
        } else {
            PLog.i(TAG, "this [" + url + "] download failed!");
        }
    }

    public boolean findFileByUrl(String url) {
        if (DiskLruCacheManager.aCache.getLengthFromCacheByUrl(mDiskLruType, url) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifyUrlData(String url) {
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        if (findFileByUrl(url)) {
            return true;
        } else {
            return false;
        }
    }

    public void removeVersion() {
        mHelper.remove(mCacheManifestFile);
    }

    public void removeManifest() {
        mHelper.remove(mCacheManifestFile);
    }

    public String getUrlFileCacheName(String url) {
        return ToolKit.GetHashKey(url) + ".0";
    }

    public String getCacheFileDir(String url) {
        String cachePath = DiskLruCacheManager.aCache.getCacheDirPath(mDiskLruType);
        return cachePath + File.separator + getUrlFileCacheName(url);
    }

    public InputStream getInputStream(String url) {
        return DiskLruCacheManager.aCache.getInputStreamFromCacheByUrl(mDiskLruType, url);
    }

}