package orangelab.project.common.effect.msgdecor;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.androidtoolkit.PLog;
import com.androidtoolkit.ThreadToolKit;

import java.io.IOException;
import java.io.InputStream;

import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.EffectsMainFestManager;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.effect.ninepatch.NinePatchChunk;

/**
 * game_werewolf
 * 2018/7/12 下午4:08
 * Mystery
 */
public class MsgDecorToolKit {

    private static final String TAG = "MsgDecorToolKit";

    public static boolean setNinePatchForViewBackGround(View background, TextView msg, MsgDecorManiFest.MsgDecorManiFestItemResource msgDecor) {


        if (msgDecor == null) return false;

        Resources resources = background.getResources();

        String textColor = msgDecor.textColor;
        try {
            if (!TextUtils.isEmpty(textColor)) {
                msg.setTextColor(Color.parseColor(textColor));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {

            String local = msgDecor.local;
            if (TextUtils.isEmpty(local)) {
                return false;
            }

            long start = System.currentTimeMillis();

            String remote = msgDecor.remote;
            byte[] chunk = msgDecor.chunk;

            /**
             * 先去缓存中查找
             */
            Bitmap bitmap = EffectsMainFestManager.GetDrawableFromCache(local);
            if (bitmap != null) {
                if (NinePatch.isNinePatchChunk(chunk)) {
                    Rect rect = NinePatchChunk.deserialize(chunk).mPaddings;
                    final NinePatchDrawable patchy = new NinePatchDrawable(resources, bitmap, chunk, rect, null);
                    if (patchy != null) {
                        background.setBackground(patchy);
                        PLog.i(TAG, "fill bg use cache");
                        return true;
                    }
                }
            } else {
                if (chunk == null) return false;

                int resourceId = ApkResourceManager.GetApkImageResource(local);
                if (resourceId > 0) {
                    bitmap = BitmapFactory.decodeResource(resources, resourceId);
                } else {
                    InputStream inputStream = EffectsManager.GetMsgDecorInputStream(remote);
                    if (inputStream == null) return false;

                    try {
                        bitmap = BitmapFactory.decodeStream(inputStream);
                    } finally {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (NinePatch.isNinePatchChunk(chunk)) {
                    Rect rect = NinePatchChunk.deserialize(chunk).mPaddings;
                    final NinePatchDrawable patchy = new NinePatchDrawable(resources, bitmap, chunk, rect, null);
                    if (patchy != null) {
                        /**
                         * 存入缓存
                         */
                        EffectsMainFestManager.PutIntoCache(local, bitmap);
                        background.setBackground(patchy);
                        PLog.i(TAG, "fill bg use new");
                        return true;
                    }
                }
            }

            long end = System.currentTimeMillis();
            PLog.i(TAG, "fill bg cost " + (end - start));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
