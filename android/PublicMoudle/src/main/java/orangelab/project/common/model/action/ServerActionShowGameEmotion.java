package orangelab.project.common.model.action;

import com.toolkit.action.Keepable;

/**
 * game_werewolf
 * 2018/5/4 下午6:16
 * Mystery
 */

public class ServerActionShowGameEmotion implements Keepable {

    public String mark;
    public int position;
    public ServerActionShowGameEmotionData data;

    public static class ServerActionShowGameEmotionData implements Keepable {
        public int code = 1000;
        public String msg = "";

        public int stay_time;
        public String res;
        public String sound;
    }
}
