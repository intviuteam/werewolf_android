package orangelab.project.common.context

import android.text.TextUtils
import com.androidtoolkit.PLog
import com.androidtoolkit.RxToolKit
import orangelab.project.common.engine.SocketEngine
import orangelab.project.common.event.SocketEvent
import orangelab.project.common.model.ServerMessageEvent
import orangelab.project.common.model.action.*
import orangelab.project.common.tool.GsonTurnToolKit
import orangelab.project.voice.constants.VoiceConstants
import com.networktoolkit.transport.Constant
import com.toolkit.action.Action
import com.toolkit.action.ActionFilter
import kotlin.collections.ArrayList

/**
 *game_werewolf
 *2018/3/19 上午10:54
 *Mystery
 */
open class BaseServerContext : BaseContext, Action<ServerMessageEvent>, ServerMessageProcess {

    private var TAG: String
    private var mServerType = ""
    private val mLifeCycle: ArrayList<BaseServerContextLifeCycleListener>
    private var mProcessManager: ArrayList<ServerMessageProcess>

    private var mSocketConnect = true

    constructor(serverType: String) : super() {
        mServerType = serverType
        TAG = "${mServerType}_server_context"
        mProcessManager = ArrayList()
        mLifeCycle = ArrayList();
        initListener()
    }

    private fun initListener() {

        RxToolKit.Build(this, ServerMessageEvent::class.java).action(this).register()

        RxToolKit.Build(this, SocketEvent.ReConnectedFailedEvent::class.java)
                .actionFilter(ActionFilter {
                    return@ActionFilter TextUtils.equals(it.intent, mServerType)
                })
                .action(Action {
                    exec(Runnable {
                        /**
                         * 处理与房间退出链接
                         */
                        //todo 这里先把原因传空
                        handleServerSocketReconnectFailed("")
                    })
                }).register()
        RxToolKit.Build(this, SocketEvent.SocketConnectStateEvent::class.java)
                .actionFilter(ActionFilter {
                    return@ActionFilter TextUtils.equals(it.intent, mServerType)
                })
                .action(Action {
                    exec(Runnable {
                        /**
                         * 处理Socket链接状态
                         */
                        when (it.action) {
                            SocketEvent.SOCKET_CONNECT -> {
                                mSocketConnect = true
                                handleServerSocketConnect()
                            }
                            SocketEvent.SOCKET_DISCONNECT -> {
                                if (!mSocketConnect) {
                                    return@Runnable
                                }
                                mSocketConnect = false
                                handleServerSocketDisconnect()
                            }
                        }
                    })
                }).register()
    }

    fun addLifeCycleListener(cycleListener: BaseServerContextLifeCycleListener) {
        mLifeCycle.add(cycleListener)
    }

    fun removeLifeCycleListener(cycleListener: BaseServerContextLifeCycleListener) {
        mLifeCycle.remove(cycleListener)
    }

    override fun registerProcess(process: ServerMessageProcess) {
        mProcessManager.add(process)
        printlnLog("registerProcess ${process}")
    }

    override fun unRegisterProcess(process: ServerMessageProcess) {
        mProcessManager.remove(process)
        printlnLog("unRegisterProcess ${process}")
    }

    override fun func(value: ServerMessageEvent?) {
        /**
         * handle Server message
         */
        exec(Runnable {

            if (value == null) {
                return@Runnable
            }

            if (!TextUtils.equals(value.fromSocketType, mServerType)) {
                return@Runnable
            }

            val begin = System.currentTimeMillis()

            printlnLog("begin handle message ${value.type}")

            try {

                mLifeCycle.forEach {
                    it.onStart(value!!.type, value!!.payload)
                }

                onServerMessage(value!!)

                mLifeCycle.forEach {
                    it.onEnd(value!!.type, value!!.payload)
                }

            } catch (e: Exception) {
                try {
                    e.printStackTrace()
                    mLifeCycle.forEach {
                        it.onError(e)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            val end = System.currentTimeMillis()

            printlnLog("end handle message ${value.type} cost ${(end - begin)}")
        })
    }

    private fun onServerMessage(value: ServerMessageEvent) {
        val type: String = value.type;
        var payload: String = value?.payload?.toString() ?: ""

        when (type) {
            VoiceConstants.TYPE_ADD_FRIEND -> {
                val requestAddFriend = GsonTurnToolKit.turn(payload, ServerActionAddFriend::class.java)
                handleServerActionAddFriend(requestAddFriend)
            }
            VoiceConstants.ACCEPT_FREE_MODE -> {
                val acceptFreeMode = GsonTurnToolKit.turn(payload, ServerActionAcceptFreeMode::class.java)
                handleServerActionAcceptFreeMode(acceptFreeMode)
            }
            VoiceConstants.CHANGE_USER_STATE -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionChangeUserState::class.java)
                handleServerActionChangeUserState(temp)
            }
            VoiceConstants.TYPE_CHAT -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionChat::class.java)
                handleServerActionChat(temp)
            }
            VoiceConstants.DOWN_SEAT -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionDownSeat::class.java)
                handleServerActionDownSeat(temp)
            }
            VoiceConstants.FORCE_SEAT -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionForceSeat::class.java)
                handleServerActionForceSeat(temp)
            }
            VoiceConstants.HAND_OVER_MASTER -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionHandOverMaster::class.java)
                handleServerActionHandOverMaster(temp)
            }
            Constant.TYPE_JOIN -> {
                val join = GsonTurnToolKit.turn(payload, ServerActionJoin::class.java)
                handleServerActionJoin(join)
            }
            VoiceConstants.KICK_OUT -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionKickOut::class.java)
                handleServerActionKickOut(temp)
            }
            Constant.LEAVE -> {
                val leave = GsonTurnToolKit.turn(payload, ServerActionLeave::class.java)
                handleServerActionLeave(leave)
            }
            VoiceConstants.LIKE_ROOM -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionLikeRoom::class.java)
                handleServerActionLikeRoom(temp)
            }
            VoiceConstants.LOCK -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionLock::class.java)
                handleServerActionLock(temp)
            }
            VoiceConstants.REJECT_FREE_MODE -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionRejectFreeMode::class.java)
                handleServerActionRejectFreeMode(temp)
            }
            VoiceConstants.REQUEST_FREE_MODE -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionRequestFreeMode::class.java)
                handleServerActionRequestFreeMode(temp)
            }
            VoiceConstants.RESTORE_ROOM -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionRestoreRoom::class.java)
                handleServerActionRestoreRoom(temp)
            }
            VoiceConstants.SHOW_EMOTION -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionShowEmoticon::class.java)
                handleServerActionShowEmoticon(temp)
            }
            VoiceConstants.SPEAK -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionSpeak::class.java)
                handleServerActionSpeak(temp)
            }
            VoiceConstants.SYSTEM_MSG -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionSystemMsg::class.java)
                handleServerActionSystemMsg(temp)
            }
            VoiceConstants.UNSPEAK -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionUnSpeak::class.java)
                handleServerActionUnSpeak(temp)
            }
            VoiceConstants.UPDATE_CONFIG -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionUpdateConfig::class.java)
                handleServerActionUpdateConfig(temp)
            }
            VoiceConstants.TYPE_UPDATE_MASTER -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionUpdateMaster::class.java)
                handleServerActionUpdateMaster(temp)
            }
            VoiceConstants.UPDATE_TITLE -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionUpdateTitle::class.java)
                handleServerActionUpdateTitle(temp)
            }
            VoiceConstants.UP_SEAT -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionUpSeat::class.java)
                handleServerActionUpSeat(temp)
            }
            VoiceConstants.CHANGE_PASSWORD -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionChangePassword::class.java)
                handleServerActionChangePassword(temp)
            }
            VoiceConstants.SHOW_GAME -> {
                val temp = GsonTurnToolKit.turn(payload, ServerActionShowGameEmotion::class.java)
                handleServerActionShowGameEmoticon(temp)
            }
            else -> {
                return
            }
        }
    }

    override fun handleServerActionAddFriend(serverActionAddFriend: ServerActionAddFriend) {

    }

    override fun handleServerActionAcceptFreeMode(serverActionAcceptFreeMode: ServerActionAcceptFreeMode) {
    }

    override fun handleServerActionChangeUserState(serverActionChangeUserState: ServerActionChangeUserState) {
    }

    override fun handleServerActionChat(serverActionChat: ServerActionChat) {
    }

    override fun handleServerActionDownSeat(serverActionDownSeat: ServerActionDownSeat) {
    }

    override fun handleServerActionForceSeat(serverActionForceSeat: ServerActionForceSeat) {

    }

    override fun handleServerActionHandOverMaster(serverActionHandOverMaster: ServerActionHandOverMaster) {
    }

    override fun handleServerActionJoin(serverActionJoin: ServerActionJoin) {
    }

    override fun handleServerActionKickOut(serverActionKickOut: ServerActionKickOut) {
    }

    override fun handleServerActionLeave(serverActionLeave: ServerActionLeave) {
    }

    override fun handleServerActionLikeRoom(serverActionLikeRoom: ServerActionLikeRoom) {
    }

    override fun handleServerActionLock(serverActionLock: ServerActionLock) {
    }

    override fun handleServerActionRejectFreeMode(serverActionRejectFreeMode: ServerActionRejectFreeMode) {
    }

    override fun handleServerActionRequestFreeMode(serverActionRequestFreeMode: ServerActionRequestFreeMode) {
    }

    override fun handleServerActionRestoreRoom(serverActionRestoreRoom: ServerActionRestoreRoom) {
    }

    override fun handleServerActionShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon) {
    }

    override fun handleServerActionSpeak(serverActionSpeak: ServerActionSpeak) {
    }

    override fun handleServerActionSystemMsg(serverActionSystemMsg: ServerActionSystemMsg) {
    }

    override fun handleServerActionUnSpeak(serverActionUnSpeak: ServerActionUnSpeak) {
    }

    override fun handleServerActionUpdateConfig(serverActionUpdateConfig: ServerActionUpdateConfig) {

    }

    override fun handleServerActionUpdateMaster(serverActionUpdateMaster: ServerActionUpdateMaster) {
    }

    override fun handleServerActionUpdateTitle(serverActionUpdateTitle: ServerActionUpdateTitle) {
    }

    override fun handleServerActionUpSeat(serverActionUpSeat: ServerActionUpSeat) {
    }

    override fun handleServerActionChangePassword(serverActionChangePassword: ServerActionChangePassword) {

    }

    override fun handleServerActionShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion) {

    }

    override fun handleServerSocketDisconnect() {
        //To change body of created functions use File | Settings | File Templates.
        proceedServerSocketDisConnect()
    }

    override fun handleServerSocketConnect() {
        //To change body of created functions use File | Settings | File Templates.
        proceedServerSocketConnect()
    }

    override fun handleServerSocketReconnectFailed(reason: String) {
        //To change body of created functions use File | Settings | File Templates.
        proceedServerSocketReConnectFailed(reason)
    }

    protected fun proceedServerSocketConnect() {
        mProcessManager.forEach {
            it.handleServerSocketConnect()
        }
    }

    protected fun proceedServerSocketReConnectFailed(reason: String) {
        mProcessManager.forEach {
            it.handleServerSocketReconnectFailed(reason)
        }
    }

    protected fun proceedServerSocketDisConnect() {
        mProcessManager.forEach {
            it.handleServerSocketDisconnect()
        }
    }

    override fun destroy() {
        super.destroy()
        RxToolKit.UnRegister(this)
        mLifeCycle.clear()
        mProcessManager.clear()
    }

    open protected fun printlnLog(info: String) {
        PLog.i(TAG, info)
    }

}