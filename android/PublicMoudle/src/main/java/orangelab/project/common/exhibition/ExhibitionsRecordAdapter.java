package orangelab.project.common.exhibition;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import orangelab.project.common.exhibition.gift.ExhibitionsRecordItem;
import orangelab.project.common.effect.EffectsManager;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/7/3 上午11:37
 * Mystery
 */

public class ExhibitionsRecordAdapter extends BaseAdapter {

    private List<ExhibitionsRecordItem> mRecordItemList;
    private Context mContext;

    public ExhibitionsRecordAdapter(Context context) {
        this.mContext = context;
    }

    public void updateRecord(List<ExhibitionsRecordItem> recordItems) {
        this.mRecordItemList = recordItems;
        if (this.mRecordItemList == null) {
            this.mRecordItemList = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mRecordItemList.size();
    }

    @Override
    public ExhibitionsRecordItem getItem(int position) {
        return mRecordItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.layout_gifts_record_item, null);
            holder.iv = (ImageView) convertView.findViewById(R.id.gift_record_item_iv);
            holder.tv = (TextView) convertView.findViewById(R.id.gift_record_tv);
            holder.number = (TextView) convertView.findViewById(R.id.gift_record_item_number);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ExhibitionsRecordItem item = getItem(position);
        EffectsManager.FillGiftImageView(item.getType(), holder.iv);
        holder.tv.setText(EffectsManager.GetSupportedTypeName(item.getType()));
        holder.number.setText(item.getNumber());
        return convertView;
    }


    private static class ViewHolder {
        private ImageView iv;
        private TextView tv;
        private TextView number;
    }
}
