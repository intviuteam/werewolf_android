package orangelab.project.common.family;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;
import com.R;
import orangelab.project.common.family.model.FamilyInfo;
import orangelab.project.common.family.model.FamilyInfoApiResult;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import com.androidtoolkit.UIActuator;

import orangelab.thirdparty.leancloud.chatkit.utils.Constant;

/**
 * game_werewolf
 * 2017/10/28 上午11:21
 * Mystery
 */

public class FamilyTools {
    public static final String TYPE = "/^(?:[a-z\\d_]{4,16}|[\\u4E00-\\u9FA5]{2,8})$/";
    public static final String PINYIN = "/^[\\u4e00-\\u9fa5]{2,8}$/";
    public static final String PINYIN2 = "[a-zA-Z]{4,16}";//|[\u4E00-\u9FA5]{2,8}

    public static boolean FamilyNameFilter(String input) {
        if (TextUtils.isEmpty(input)) {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
            return false;
        }
        try {
            int length = input.trim().length();
            if (length < 2 || length > 16) {
                UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
//        if (input.matches("[a-zA-Z\\u4E00-\\u9FA5]{0,16}")) {
//            try {
//                int length = input.getBytes("gb2312").length;
//                if (length > 16) {
//                    UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
//                    return false;
//                } else if (length < 2) {
//                    UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
//                    return false;
//                } else {
//                    return true;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
//                return false;
//            }
//        } else {
//            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_name_filter_error)));
//            return false;
//        }
    }

    public static boolean FamilyShortNameFilter(String input) {
        if (TextUtils.isEmpty(input)) {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_short_name_filter_error)));
            return false;
        }
        if (input.matches("[\\u4E00-\\u9FA5]{2}")) {
            return true;
        } else {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_short_name_filter_error)));
            return false;
        }
    }

    public static boolean FamilyDescFilter(String input) {
        if (TextUtils.isEmpty(input)) {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_desc_filter_error2)));
            return false;
        }
        if (input.length() > 200) {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_desc_filter_error)));
            return false;
        }
        return true;
    }

    public static boolean FamilyImageFilter(String path) {
        if (TextUtils.isEmpty(path)) {
            UIActuator.post(RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_family_image_filter_error)));
            return false;
        }
        if (path.endsWith(".png") || path.endsWith(".jpeg") || path.endsWith(".jpg") || path.endsWith("bmp")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean IsFamilyMsg(String msgType) {
        return TextUtils.equals(msgType, Constant.MSG_TYPE_FAMILY);
    }

    public static String GetFamilyMemberTitle(String title) {
        if (TextUtils.equals(title, "owner")) {
            return MessageUtils.getString(R.string.string_family_boss);
        } else if (TextUtils.equals(title, FamilyConstant.FAMILY_DEPUTY)) {
            return MessageUtils.getString(R.string.string_family_deputy);
        } else if (TextUtils.equals(title, FamilyConstant.FAMILY_ELDER)) {
            return (MessageUtils.getString(R.string.string_family_elder));
        } else if (TextUtils.equals(title, FamilyConstant.FAMILY_ELITE)) {
            return (MessageUtils.getString(R.string.string_family_elite));
        } else {
            return (MessageUtils.getString(R.string.string_family_members_slave));
        }
    }

    public static int GetFamilyLevelButtonBgResource(int level) {
        int result = R.drawable.drawable_family_level_btn_1;
        if (level >= 1 && level <= 3) {
            result = R.drawable.drawable_family_level_btn_1;
        } else if (level > 3 && level <= 6) {
            result = R.drawable.drawable_family_level_btn_2;
        } else if (level > 7 && level <= 9) {
            result = R.drawable.drawable_family_level_btn_3;
        } else if (level > 9) {
            result = R.drawable.drawable_family_level_btn_4;
        }
        return result;
    }

    public static int GetFamilyLevelButtonTextBgResource(int level) {
        int result = R.color.color_family_level_1_btn_text_color;
        if (level >= 1 && level <= 3) {
            result = R.color.color_family_level_1_btn_text_color;
        } else if (level > 3 && level <= 6) {
            result = R.color.color_family_level_2_btn_text_color;
        } else if (level > 7 && level <= 9) {
            result = R.color.color_family_level_3_btn_text_color;
        } else if (level > 9) {
            result = R.color.color_family_level_4_btn_text_color;
        }
        return result;
    }

    public static int GetFamilyLevelBadgeBgResource(int level) {
        int result = -1;
        switch (level) {
            case 1:
                result = R.mipmap.ico_family_level_1;
                break;
            case 2:
                result = R.mipmap.ico_family_level_2;
                break;
            case 3:
                result = R.mipmap.ico_family_level_3;
                break;
            case 4:
                result = R.mipmap.ico_family_level_4;
                break;
            case 5:
                result = R.mipmap.ico_family_level_5;
                break;
            case 6:
                result = R.mipmap.ico_family_level_6;
                break;
            case 7:
                result = R.mipmap.ico_family_level_7;
                break;
            case 8:
                result = R.mipmap.ico_family_level_8;
                break;
            case 9:
                result = R.mipmap.ico_family_level_9;
                break;
            case 10:
                result = R.mipmap.ico_family_level_10;
                break;
            case 11:
                result = R.mipmap.ico_family_level_10;
                break;
            case 12:
                result = R.mipmap.ico_family_level_10;
                break;
        }
        return result;
    }

    public static void FillFamilyBadge(Context context, FamilyInfoApiResult infoApiResult, ImageView targetView) {
        try {
            String levelUrl = infoApiResult.group.level_image;
            int level = infoApiResult.group.level_val;
            int bgResource = GetFamilyLevelBadgeBgResource(level);
            if (bgResource > 0) {
                /**
                 * 本地支持这个勋章
                 */
                targetView.setImageResource(bgResource);
                targetView.setVisibility(View.VISIBLE);
            } else {
                /**
                 * 本地不支持用远程图片
                 */
                PicassoUtils.loadImage(context, levelUrl, targetView, R.mipmap.ico_family_level_1);
                targetView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void FillFamilyBadge(Context context, FamilyInfo familyInfo, ImageView targetView) {

        String levelUrl = familyInfo.level_image;
        int level = familyInfo.level_val;

        int bgResource = GetFamilyLevelBadgeBgResource(level);
        if (bgResource > 0) {
            /**
             * 本地支持这个勋章
             */
            targetView.setImageResource(bgResource);
            targetView.setVisibility(View.VISIBLE);
        } else {
            /**
             * 本地不支持用远程图片
             */
            PicassoUtils.loadImage(context, levelUrl, targetView, R.mipmap.ico_family_level_1);
            targetView.setVisibility(View.VISIBLE);
        }
        return;
    }

    public static void FillFamilyLevelButton(FamilyInfoApiResult.FamilyInfoApiResultGroup infoApiResultGroup, Button targetButton) {
        String level = infoApiResultGroup.level;
        int levelInteger = infoApiResultGroup.level_val;

        if (TextUtils.isEmpty(level)) {
            targetButton.setText(MessageUtils.getString(R.string.string_family_default_level));
        } else {
            targetButton.setText(level);
        }

        targetButton.setBackgroundResource(GetFamilyLevelButtonBgResource(levelInteger));
        targetButton.setTextColor(targetButton.getContext().getResources().getColor(GetFamilyLevelButtonTextBgResource(levelInteger)));
    }


}
