package orangelab.project.common.share;


import android.content.Context;
import android.graphics.Bitmap;

import com.toolkit.action.Destroyable;
import com.toolkit.action.Keepable;

import java.util.HashMap;

import orangelab.project.common.utils.GameShareFactory;
import orangelab.project.voice.model.VoiceDeedShareBean;
import orangelab.project.voice.model.VoiceSAFBean;

/**
 * 存放所有的分享类
 */
public class SharePoolManager implements Keepable, Destroyable {

    public static final String ShareVoiceActivity = "VoiceShareActivity";
    public static final String ShareGameActivity = "WereWolfShareActivity";
    public static final String ShareUtilsActivity = "ShareUtilsActivity";

    public static HashMap<String, ShareProvider> mShareProvider = new HashMap<>();


    public static void addProvider(String key, ShareProvider provider) {
        mShareProvider.put(key, provider);
    }

    public static ShareProvider getProvider(String key) {
        return mShareProvider.get(key);
    }

    public void destroy() {
        mShareProvider.clear();
    }

    /**
     * 分享提供者
     */
    public interface ShareProvider {

        //富媒体类分享 start==================================================================================================================//

        /**
         * 游戏房分享
         */
        void onGameRoomShare(Context context, ShareBridgeData data);

        /**
         * 语音房分享
         */
        void onVoiceRoomShare(Context context, ShareBridgeData data);

        /**
         * 私密房分享
         */
        void onPrivacyRoomShare(Context context, ShareBridgeData data);

        /**
         * 小游戏分享
         */
        void onMiniGameShare(Context context, ShareBridgeData data);

        /**
         * 设置页面分享
         */
        void onSettingShare(Context context, ShareBridgeData data);

        /**
         * js回调分享
         */
        void onJsCallShare(Context context, ShareBridgeData data);

        /**
         * js回调房契分享
         */
        void onDeedShare(Context context, VoiceDeedShareBean data);

        /**
         * 单纯图片分享
         */
        void onImageShare(Context context, ShareBridgeData data);

        /**
         * 谁是卧底房分享
         */
        void onSpyRoomShare(Context context, ShareBridgeData data);

        /**
         * 大厅分享
         */
        void onLobbyRoomShare(Context context, ShareBridgeData data);
        //富媒体类分享 end==================================================================================================================//


        //View类分享 start==================================================================================================================//

        /**
         * 游戏结束图片分享
         */
        Bitmap getGameOverShareView(Context context, GameShareFactory.Builder builder);

        /**
         * 谁是卧底图片分享
         */
        Bitmap getSpyShareView(Context context, VoiceSAFBean bean);

        /**
         * 房契图片分享
         */
        Bitmap getRoomDeedShareView(Context context, VoiceDeedShareBean bean);
        //View类分享 end==================================================================================================================//
    }
}
