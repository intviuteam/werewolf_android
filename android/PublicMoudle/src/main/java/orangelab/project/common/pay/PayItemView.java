package orangelab.project.common.pay;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;

/**
 * game_werewolf
 * 2017/7/21 下午2:17
 * Mystery
 */

public class PayItemView extends FrameLayout {

    private View mBg;
    private ImageView mIc;
    private TextView mTv;
    private ImageView mSelect;
    private String mType;

    private boolean isSelected;

    public PayItemView(@NonNull Context context) {
        this(context, null);
    }

    public PayItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PayItemView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void updateView(String type, int iv, int text) {
        this.mType = type;
        this.mIc.setImageResource(iv);
        this.mTv.setText(text);
    }

    private void initView() {
        View view = View.inflate(getContext(), R.layout.pay_item_layout, this);
        mBg = view;
        mBg.setBackgroundResource(R.drawable.pay_item_normal_bg);
        mIc = (ImageView) view.findViewById(R.id.pay_item_iv);
        mTv = (TextView) view.findViewById(R.id.pay_item_tx);
        mSelect = (ImageView) view.findViewById(R.id.pay_item_select);
    }

    public void setSelect(boolean select) {
        this.isSelected = select;
        if (select) {
            mBg.setBackgroundResource(R.drawable.pay_item_select_bg);
            mSelect.setImageResource(R.mipmap.pay_icon_check_s);
        } else {
            mBg.setBackgroundResource(R.drawable.pay_item_normal_bg);
            mSelect.setImageResource(R.mipmap.pay_icon_check);
        }
    }

    public String getType() {
        return mType;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }
}
