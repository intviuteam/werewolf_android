package orangelab.project.common.utils;

import android.content.Context;
import android.graphics.Rect;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;

import orangelab.project.common.manager.AppManager;


/**
 * game_werewolf
 * 2018/3/19 上午10:26
 * Mystery
 */

public class FitSystemUIConstraintLayout extends ConstraintLayout {

    private int[] mInsets = new int[4];

    public FitSystemUIConstraintLayout(Context context) {
        super(context);
    }

    public FitSystemUIConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FitSystemUIConstraintLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected final boolean fitSystemWindows(Rect insets) {
        if (AppManager.GreaterLollipop()) {
            // Intentionally do not modify the bottom inset. For some reason,
            // if the bottom inset is modified, window resizing stops working.
            // TODO: Figure out why.

            mInsets[0] = insets.left;
            mInsets[1] = insets.top;
            mInsets[2] = insets.right;

            insets.left = 0;
            insets.top = 0;
            insets.right = 0;
        }

        return super.fitSystemWindows(insets);
    }
}
