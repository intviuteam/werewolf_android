package orangelab.project.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.R;
import orangelab.project.game.view.ChooseTypeButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lloydfinch on 25/08/2017.
 */

public class ChooseTypeAdapter extends BaseAdapter {

    private Context mContext;
    private List<ChooseTypeButton.PopItem> popItems = new ArrayList<>();

    public ChooseTypeAdapter(Context mContext, List<ChooseTypeButton.PopItem> popItems) {
        this.mContext = mContext;
        this.popItems = popItems;
    }

    @Override
    public int getCount() {
        return popItems.size();
    }

    @Override
    public Object getItem(int position) {
        return popItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView == null
                ? LayoutInflater.from(mContext).inflate(R.layout.item_choose_type, null)
                : convertView;
        ViewHolder holder = (ViewHolder) itemView.getTag();
        if (holder == null) {
            holder = new ViewHolder(itemView);
            itemView.setTag(holder);
        }

        ChooseTypeButton.PopItem item = popItems.get(position);
        holder.tvChooseType.setText(item.msg);

        return itemView;
    }

    static class ViewHolder {
        TextView tvChooseType;

        ViewHolder(View itemView) {
            tvChooseType = (TextView) itemView.findViewById(R.id.tv_item_choose_type);
        }
    }

    public interface OnChooseTypeListener {
        void chooseType(int id);
    }
}
