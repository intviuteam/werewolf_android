package orangelab.project.common.utils;

import android.app.Activity;
import android.text.TextUtils;

import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * game_werewolf
 * 2017/6/13 下午3:58
 * Mystery
 */

public class GameShareFactory {


    public static class Builder {
        private String imageUrl;
        private String userName;
        private boolean isWin;
        private String gameType;
        private String gameMessage;

        public Builder() {
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public String getUserName() {
            return userName;
        }

        public boolean isWin() {
            return isWin;
        }

        public String getGameType() {
            return gameType;
        }

        public String getGameMessage() {
            return gameMessage;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public void setUserName(String userName) {
            if (TextUtils.isEmpty(userName)) {
                this.userName = "";
                return;
            }
            if (userName.length() > 4) {
                userName = userName.substring(0, 4);
                userName += "...";
            }
            this.userName = userName;
        }

        public void setWin(boolean win) {
            isWin = win;
        }

        public void setGameType(String gameType) {
            this.gameType = gameType;
        }

        public void setGameMessage(String gameMessage) {
            this.gameMessage = gameMessage;
        }
    }

    public static void shareGameOver(Activity activity, Builder builder, SHARE_MEDIA share_media) {
        ShareTools.shareGameOverImage(activity, builder, share_media);
    }


}
