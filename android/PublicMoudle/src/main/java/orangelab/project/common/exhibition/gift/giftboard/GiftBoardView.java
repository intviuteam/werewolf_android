package orangelab.project.common.exhibition.gift.giftboard;

import android.graphics.PointF;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.R;
import com.toolkit.action.Action;
import com.toolkit.action.Destroyable;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.SafeHandler;
import com.androidtoolkit.PicassoUtils;

import com.androidtoolkit.view.ScreenUtils;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * game_werewolf
 * 2017/11/9 下午5:25
 * Mystery
 */

public class GiftBoardView implements Destroyable {

    public static final int DEFAULT_WIDTH = ScreenUtils.dip2px(200);
    public static final int DEFAULT_HEIGHT = ScreenUtils.dip2px(60);
    public static final long DISMISS_SECOND = 3000;

    private View mView;

    private CircleImageView mFromIv;
    private CircleImageView mToIv;

    private TextView mFromName;
    private TextView mToName;

    private ImageView mFromNumber;
    private ImageView mToNumber;

    private ImageView mGift;
    private TextView mGiftCount;

    private int mCount = 0;
    private PointF mPoint = null;
    private PointF mLastPoint = null;
    private SafeHandler mSafeHandler = new SafeHandler();
    private boolean isShowing = false;

    private int mFromPosition = 0;
    private int mToPosition = 0;
    private String mGiftType = "";

    private Action<GiftBoardView> mDismissListener;
    private Runnable mDismissRunnable = new Runnable() {
        @Override
        public void run() {
            if (mDismissListener != null) {
                mDismissListener.func(GiftBoardView.this);
                isShowing = false;
            }
        }
    };


    public GiftBoardView(ViewGroup rootView, Action<GiftBoardView> dismissListener) {
        this.mDismissListener = dismissListener;
        mView = View.inflate(rootView.getContext(), R.layout.layout_giftboard, null);

        mFromIv = (CircleImageView) mView.findViewById(R.id.id_giftboard_left_iv);
        mToIv = (CircleImageView) mView.findViewById(R.id.id_giftboard_right_iv);

        mFromName = (TextView) mView.findViewById(R.id.id_giftboard_from_name);
        mToName = (TextView) mView.findViewById(R.id.id_giftboard_to_name);

        mFromNumber = (ImageView) mView.findViewById(R.id.id_giftboard_from_position);
        mToNumber = (ImageView) mView.findViewById(R.id.id_giftboard_to_position);

        mGift = (ImageView) mView.findViewById(R.id.id_giftboard_gift_iv);
        mGiftCount = (TextView) mView.findViewById(R.id.id_giftboard_gift_count);
    }

    public View getView() {
        return mView;
    }

    public boolean isShowing() {
        return isShowing;
    }

    public GiftBoardView updateFromIv(String url) {
        postSafely(() -> {
            if (mView != null) {
                PicassoUtils.loadImage(mView.getContext(), url, mFromIv, R.mipmap.default_head);
            }
        });
        return this;
    }

    public GiftBoardView updateFromName(String name) {
        postSafely(() -> {
            mFromName.setText(name);
        });
        return this;
    }

    public GiftBoardView updateToName(String name) {
        postSafely(() -> {
            mToName.setText(name);
        });
        return this;
    }

    public GiftBoardView updateToIv(String url) {
        postSafely(() -> {
            if (mView != null) {
                PicassoUtils.loadImage(mView.getContext(), url, mToIv, R.mipmap.default_head);
            }
        });
        return this;
    }

    public GiftBoardView updateFromPosition(int position) {
        postSafely(() -> {
            mFromPosition = position;
            mFromNumber.setImageResource(GiftBoardUtils.GetFromPositionImage(position));
        });
        return this;
    }

    public GiftBoardView updateToPosition(int position) {
        postSafely(() -> {
            mToPosition = position;
            mToNumber.setImageResource(GiftBoardUtils.GetToPositionImage(position));
        });
        return this;
    }

    public GiftBoardView updateGiftIv(String giftType) {
        postSafely(() -> {
            mGiftType = giftType;
            EffectsManager.FillGiftImageView(giftType, mGift);
        });
        return this;
    }

    public GiftBoardView addCount() {
        postSafely(() -> {
            mCount++;
            mGiftCount.setText(MessageUtils.getString(R.string.string_gift_board_gift_count, Integer.toString(mCount)));
        });
        return this;
    }

    public void addViewToRoot(ViewGroup rootView) {
        postSafely(() -> {
            if (rootView != null) {
                rootView.addView(mView);
            }
        });
    }

    public GiftBoardView updatePoint(PointF point) {
        if (mPoint == point) {
            /**
             * 坐标是一致的，不用处理
             */
            return this;
        }
        postSafely(() -> {
            mPoint = point;
        });
        return this;
    }

    public GiftBoardView moveTo() {
        postSafely(() -> {
            if (mLastPoint == null) {
                /**
                 * 说明是第一次出现
                 */
                if (mPoint != null) {
                    mView.setX(mPoint.x);
                    mView.setY(mPoint.y);
                    mLastPoint = mPoint;
                }
            } else {
                /**
                 * 需要移动
                 */
                if (mPoint == mLastPoint) {
                    /**
                     * 所需要的移动点相同，不管它
                     */
                    return;
                }
                GiftBoardAnimFactory.CreatePathAnimator(mView, mLastPoint, mPoint).start();
                /**
                 * 移动之后更换lastPoint
                 */
                mLastPoint = mPoint;
            }

        });
        return this;
    }

    public void show() {
        postSafely(() -> {
            isShowing = true;
            mSafeHandler.removeCallbacks(mDismissRunnable);
            mSafeHandler.postDelaySafely(mDismissRunnable, DISMISS_SECOND);
        });
    }

    public int getFromPosition() {
        return mFromPosition;
    }

    public int getToPosition() {
        return mToPosition;
    }

    public String getGiftType() {
        return mGiftType;
    }

    public boolean isEqual(int fromPosition, int toPosition, String giftType) {
        if (mFromPosition == fromPosition && mToPosition == toPosition && TextUtils.equals(mGiftType, giftType)) {
            return true;
        } else {
            return false;
        }
    }

    private void postSafely(Runnable runnable) {
        mSafeHandler.postSafely(runnable);
    }

    @Override
    public void destroy() {
        mSafeHandler.release();
    }
}
