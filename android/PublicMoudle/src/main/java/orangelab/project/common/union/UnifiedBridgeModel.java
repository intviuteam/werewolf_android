package orangelab.project.common.union;

import com.androidtoolkit.PLog;

import org.json.JSONObject;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2017/9/19 下午4:13
 * Mystery
 */

public class UnifiedBridgeModel {

    public String action;
    public String type;
    public JSONObject params;
    public UnifiedBridgeModelOption options;

    public static final UnifiedBridgeModelOption default_options_without_callback = new UnifiedBridgeModelOption(false, false, 0);
    public static final UnifiedBridgeModelOption default_options_with_callback = new UnifiedBridgeModelOption(false, true, 0);

    public static class UnifiedBridgeModelOption {

        public boolean sync;
        public boolean needcallback;
        public long timeout;

        public UnifiedBridgeModelOption() {
        }

        public UnifiedBridgeModelOption(boolean sync, boolean needcallback) {
            this.sync = sync;
            this.needcallback = needcallback;
        }

        public UnifiedBridgeModelOption(boolean sync, boolean needcallback, long timeout) {
            this.sync = sync;
            this.needcallback = needcallback;
            this.timeout = timeout;
        }

        @Override
        public String toString() {
            return "UnifiedBridgeModelOption{" +
                    "Sync=" + sync +
                    ", needcallback=" + needcallback +
                    ", timeout=" + timeout +
                    '}';
        }
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"action\":\"");
        sb.append(action + "\"");
        sb.append(",\"params\":");
        sb.append(params == null ? "{}" : params.toString());
        sb.append(",\"options\":");
        sb.append(GsonHelper.getGson().toJson(options));
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString() {
        return "UnifiedBridgeModel{" +
                "action='" + action + '\'' +
                ", type='" + type + '\'' +
                ", params=" + params +
                ", option=" + options +
                '}';
    }


    public UnifiedBridgeModel() {

    }

    public static UnifiedBridgeModel TurnToUnifiedBridgeModel(String json) {
        PLog.i("UnifiedBridge", "turn to mode [json=" + json + "]");
        UnifiedBridgeModel model = null;
        try {
            JSONObject j = new JSONObject(json);
            if (!j.has("action")) {
                return null;
            }
            model = new UnifiedBridgeModel();
            model.action = j.optString("action");
            if (!j.has("type")) {
                model.type = UnifiedConstant.RN_NATIVE;//默认为RN通信，兼容之前
            }
            model.type = j.optString("type");
            model.params = j.optJSONObject("params");
            if (j.has("options")) {
                try {
                    model.options = GsonHelper.getGson().fromJson(j.optJSONObject("options").toString(), UnifiedBridgeModelOption.class);
                } catch (Exception e) {
                    /**
                     * 这里如果报错了那么就走一个默认的options
                     */
                    model.options = default_options_without_callback;
                    e.printStackTrace();
                }
            } else {
                model.options = default_options_without_callback;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return model;
    }
}
