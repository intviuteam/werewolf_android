package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;
import orangelab.project.common.model.UserInfoResult;

/**
 * game_werewolf
 * 2017/10/19 下午2:22
 * Mystery
 */

public class FamilyMemberInfo implements Keepable {

    public int sex;
    public String name;
    public UserInfoResult.UserInfoGameResult game;
    public String image;
    public String id;
    public String status;
    public String title;
    public int active_point;

}
