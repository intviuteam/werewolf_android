package orangelab.project.common.engine.task;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.androidtoolkit.RxToolKit;
import com.R;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.ServerResponseEvent;
import orangelab.project.common.utils.MessageUtils;
import com.toolkit.action.Action;
import com.toolkit.action.ActionListener;

import org.json.JSONObject;

import java.util.List;

/**
 * game_werewolf
 * 2017/5/19 下午3:11
 * Mystery
 * 自处理任务
 */

public class SocketEngineTask {

    private static final String TAG = "SocketEngineTask";
    private List<SocketEngineTask> allTask;
    private static final int REQUEST_SUCCESS = 1000;
    private String taskId;
    private String mIntent;
    private SocketCallBack successCallBack;
    private SocketCallBack failedCallBack;
    private long timeOut;
    private Handler actionHandler;
    private boolean isDone = false;
    private boolean isRelease = false;
    private boolean isForceFilter = false;
    private boolean needTimeOutHint = true;
    private Runnable timeOutRunnable = new Runnable() {
        @Override
        public void run() {
            if (isDone == false) {
                Log.i(TAG, "Task is Time Out");
                isDone = true;
                if (needTimeOutHint) {
                    if (failedCallBack != null) {
                        failedCallBack.invoke("操作超时");
                        failedCallBack = null;
                    }
                }
                if (successCallBack != null) {
                    successCallBack = null;
                }
                release();
            }
        }
    };

    public void setNeedTimeOutHint(boolean needTimeOutHint) {
        this.needTimeOutHint = needTimeOutHint;
    }

    private SocketEngineTask() {

    }

    public void setIntent(String intent) {
        this.mIntent = intent;
    }

    private void bindHandler(Handler actionHandler) {
        if (actionHandler != null) {
            /**
             * 只能绑定一个执行器
             */
            this.actionHandler = actionHandler;
        }
    }

    public void timeOutCheck(long timeOut) {
        if (timeOut > 0) {
            setTimeOut(timeOut);
            if (actionHandler != null) {
                actionHandler.postDelayed(timeOutRunnable, timeOut);
            }
        }
    }

    private void bindTaskList(List<SocketEngineTask> list) {
        this.allTask = list;
    }

    private void setTaskId(String taskId) {
        if (TextUtils.isEmpty(taskId)) {
            throw new IllegalArgumentException();
        }
        this.taskId = taskId;
    }

    private void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    private void bindSuccessCallBack(SocketCallBack callBack) {
        successCallBack = callBack;
    }

    private void bindFailedCallBack(SocketCallBack callBack) {
        failedCallBack = callBack;
    }

    private void init(String careType) {
        RxToolKit.Build(taskId, ServerResponseEvent.class)
                .action(new Action<ServerResponseEvent>() {
                    private void execute(int code, String message, JSONObject payload) {
                        if (!isDone) {
                            if (code == REQUEST_SUCCESS) {
                                if (successCallBack != null) {
                                    if (payload != null) {
                                        successCallBack.invoke(payload.toString());
                                    } else {
                                        successCallBack.invoke("success!!");
                                    }
                                }
                            } else {
                                if (failedCallBack != null) {
                                    failedCallBack.invoke(message);
                                }
                            }
                            isDone = true;
                            release();
                        }
                    }

                    @Override
                    public void func(ServerResponseEvent value) {
                        if (!TextUtils.equals(value.getFromSocketType(), mIntent)) {
                            return;
                        }

                        if (isForceFilter) {
                            if (TextUtils.equals(value.type, careType) && TextUtils.equals(value.getRequestId(), taskId)) {
                                execute(value.code, value.message, value.payload);
                            }
                        } else {
                            if (TextUtils.equals(value.type, careType)) {
                                execute(value.code, value.message, value.payload);
                            }
                        }
                    }
                })
                .actionListener(new ActionListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onComplete() {
                        isDone = true;
                        release();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (failedCallBack != null) {
                            failedCallBack.invoke(MessageUtils.getString(R.string.network_is_unavailable));
                        }
                        isDone = true;
                        release();
                    }
                }).register();
        RxToolKit.Build(taskId, ServerMessageEvent.class)
                .action(new Action<ServerMessageEvent>() {
                    private void execute(int code, String message, JSONObject payload) {
                        if (!isDone) {
                            if (code == REQUEST_SUCCESS) {
                                if (successCallBack != null) {
                                    if (payload != null) {
                                        successCallBack.invoke(payload.toString());
                                    } else {
                                        successCallBack.invoke("success!!");
                                    }
                                }
                            } else {
                                if (failedCallBack != null) {
                                    failedCallBack.invoke(message);
                                }
                            }
                            isDone = true;
                            release();
                        }
                    }

                    @Override
                    public void func(ServerMessageEvent value) {
                        if (!TextUtils.equals(value.getFromSocketType(), mIntent)) {
                            return;
                        }
                        if (isForceFilter) {
                            if (TextUtils.equals(value.type, careType) && TextUtils.equals(value.getRequestId(), taskId)) {
                                execute(value.code, value.message, value.payload);
                            }
                        } else {
                            if (TextUtils.equals(value.type, careType)) {
                                execute(value.code, value.message, value.payload);
                            }
                        }
                    }
                })
                .actionListener(new ActionListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onComplete() {
                        isDone = true;
                        release();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (failedCallBack != null) {
                            failedCallBack.invoke(MessageUtils.getString(R.string.network_is_unavailable));
                        }
                        isDone = true;
                        release();
                    }
                }).register();
    }

    public void release() {
        if (!isRelease) {
            isRelease = true;
            successCallBack = null;
            failedCallBack = null;
            if (actionHandler != null) {
                actionHandler.removeCallbacks(timeOutRunnable);
                actionHandler = null;
            }
            if (allTask != null) {
                allTask.remove(this);
                allTask = null;
            }
            RxToolKit.UnRegister(taskId);
            Log.i(TAG, "Task is release");
        }
    }

    public boolean isForceFilter() {
        return isForceFilter;
    }

    public void setForceFilter(boolean forceFilter) {
        isForceFilter = forceFilter;
    }

    public static SocketEngineTask buildOne(
            String taskId,
            List<SocketEngineTask> allTask,
            String careType,
            SocketCallBack successCallBack,
            SocketCallBack failedCallBack,
            Handler actionHandler,
            boolean needIdFilter,
            String intent
    ) {
        SocketEngineTask task = new SocketEngineTask();
        task.setTaskId(taskId);
        task.bindHandler(actionHandler);
        task.bindSuccessCallBack(successCallBack);
        task.bindFailedCallBack(failedCallBack);
        task.setForceFilter(needIdFilter);
        task.init(careType);
        task.setIntent(intent);
        allTask.add(task);
        Log.i(TAG, "buildOne: create a Task Type is " + careType);
        return task;
    }

}
