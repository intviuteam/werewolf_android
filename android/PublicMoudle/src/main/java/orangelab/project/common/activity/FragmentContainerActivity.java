package orangelab.project.common.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.R;


/**
 * game_werewolf
 * 2018/8/6 上午9:56
 * Mystery
 */
public class FragmentContainerActivity extends SafeActivity {

    private static final String CLAZZ_DATA = "clazzData";

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        String clazzName = getIntent().getStringExtra(CLAZZ_DATA);

        if (TextUtils.isEmpty(clazzName)) {
            finish();
            return;
        }

        Fragment fragment = getFragmentInstance(clazzName);
        if (fragment == null) {
            finish();
            return;
        }

        getSupportFragmentManager().beginTransaction().add(R.id.root, fragment).commit();
    }

    private Fragment getFragmentInstance(String name) {
        Fragment result = null;
        try {
            result = (Fragment) Class.forName(name).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void Launch(Context context, String clazzName) {
        Intent intent = new Intent(context, FragmentContainerActivity.class);
        intent.putExtra(CLAZZ_DATA, clazzName);
        context.startActivity(intent);
    }

    public static void Launch(Context context, String clazzName, Bundle bundle) {
        Intent intent = new Intent(context, FragmentContainerActivity.class);
        intent.putExtras(bundle);
        intent.putExtra(CLAZZ_DATA, clazzName);
        context.startActivity(intent);
    }

}
