package orangelab.project.common.union;

import com.toolkit.action.Destroyable;
import com.androidtoolkit.PLog;
import com.androidtoolkit.UIActuator;

/**
 * game_werewolf
 * 2017/10/30 上午11:32
 * Mystery
 */

public abstract class UnifiedBridgeTask implements Destroyable {

    private static final String TAG = "UnifiedBridgeTask";

    private String mAction;
    private IUnifiedBridge mUnifiedBridge;
    private UnifiedBridgeModel.UnifiedBridgeModelOption mOption;
    private boolean isCancel = false;
    private boolean isExecuting = false;
    private boolean isDestroy = false;
    private Runnable mTimeOutRunnable = null;

    public UnifiedBridgeTask(String action, UnifiedBridgeModel.UnifiedBridgeModelOption option, IUnifiedBridge unifiedBridge) {
        PLog.i(TAG, "create a Task action is " + action);
        mAction = action;
        mUnifiedBridge = unifiedBridge;
        mOption = option;
        if (mOption != null && mOption.timeout > 0) {
            /**
             * 创建一个超时检测任务
             */
            mTimeOutRunnable = () -> {
                if (isExecuting && !isDestroy) {
                    PLog.i(TAG, "Task [" + action + "] is time out,so end it");
                    /**
                     * 此时任务还没有被终止
                     */
                    onTimeOut(mUnifiedBridge);
                    forceEndTask();
                }
            };
        }
    }

    public void execute() {
        /**
         * 执行任务
         */
        if (isExecuting) {
            return;
        }
        isExecuting = true;
        if (mTimeOutRunnable != null) {
            UIActuator.postDelay(mTimeOutRunnable, mOption.timeout);
        }
        onExecuting(mUnifiedBridge);
    }

    public void cancel() {
        /**
         * 任务取消
         */
        if (!isExecuting) {
            return;
        }
        isCancel = true;
        onCancel(mUnifiedBridge);
        forceEndTask();
    }

    public boolean isExecuting() {
        return isExecuting;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public UnifiedBridgeModel.UnifiedBridgeModelOption getOption() {
        return mOption;
    }

    protected abstract void onExecuting(IUnifiedBridge bridge);

    protected abstract void onTimeOut(IUnifiedBridge bridge);

    protected abstract void onCancel(IUnifiedBridge bridge);

    /**
     * 强制结束任务
     */
    public void forceEndTask() {
        PLog.i(TAG, "force to end task");
        destroy();
    }

    @Override
    public void destroy() {
        if (isDestroy) {
            return;
        }
        PLog.i(TAG, "Task[" + mAction + "has been destroy");
        isDestroy = true;
        isExecuting = false;
        isCancel = true;
        mUnifiedBridge = null;
        mOption = null;
        if (mTimeOutRunnable != null) {
            UIActuator.remove(mTimeOutRunnable);
            mTimeOutRunnable = null;
        }
        UnifiedBridgeTaskManager.getInstance().removeTask(mAction);
    }
}
