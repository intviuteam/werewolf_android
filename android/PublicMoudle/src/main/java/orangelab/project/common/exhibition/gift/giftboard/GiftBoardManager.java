package orangelab.project.common.exhibition.gift.giftboard;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;
import android.view.ViewGroup;

import com.androidtoolkit.RxToolKit;
import com.toolkit.action.Action;
import com.toolkit.action.Destroyable;
import orangelab.project.game.component.GameMember;
import orangelab.project.common.utils.SafeHandler;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/11/9 下午4:10
 * Mystery
 */

public class GiftBoardManager implements Destroyable, Action<GiftBoardView> {

    private static final String TAG = "GiftBoardManager";

    private final int ScreenWidth = ScreenUtils.getScreenWidth();

    private Context mContext;
    private ViewGroup mRootView;
    private SafeHandler mSafeHandler = new SafeHandler();

    private PointF baseLineCenterPoint = new PointF(ScreenWidth / 2, ScreenUtils.dip2px(130));

    private PointF mMidPointWhenThree;
    private PointF mTopPointWhenThree;
    private PointF mBottomPointWhenThree;
    private PointF mTopPointWhenTwo;
    private PointF mBottomPointWhenTwo;


    private List<GiftBoardView> mGiftBoardViews = new ArrayList<>();
    private int mMaxSize = 3;
    private int mCurCount = 0;

    public GiftBoardManager(Context context, ViewGroup rootView) {
        mContext = context;
        mRootView = rootView;
        initBaseData();
        initListener();
    }

    private void initBaseData() {
        float baseX = baseLineCenterPoint.x - GiftBoardView.DEFAULT_WIDTH / 2;
        float mMidPointY = baseLineCenterPoint.y - GiftBoardView.DEFAULT_HEIGHT / 2;
        mMidPointWhenThree = new PointF(baseX, mMidPointY);

        float mTopPointY = baseLineCenterPoint.y - (GiftBoardView.DEFAULT_HEIGHT / 2) * 3;
        mTopPointWhenThree = new PointF(baseX, mTopPointY);

        float mBottomPointY = baseLineCenterPoint.y + GiftBoardView.DEFAULT_HEIGHT / 2;
        mBottomPointWhenThree = new PointF(baseX, mBottomPointY);

        float mTopPointWhenTwoY = baseLineCenterPoint.y - GiftBoardView.DEFAULT_HEIGHT;
        mTopPointWhenTwo = new PointF(baseX, mTopPointWhenTwoY);

        float mBottomPointWhenTwoY = baseLineCenterPoint.y;
        mBottomPointWhenTwo = new PointF(baseX, mBottomPointWhenTwoY);
    }

    private void postSafely(Runnable runnable) {
        mSafeHandler.postSafely(runnable);
    }

    private void createAndAddView(GameMember.GameMemberData fromData, GameMember.GameMemberData toData, String giftType, PointF updatePoint) {
        if (fromData == null || toData == null) {
            return;
        }
        GiftBoardView tempBoardView = new GiftBoardView(mRootView, this)
                .updateGiftIv(giftType).updateFromIv(fromData.userImage).updateToIv(toData.userImage).updateFromName(fromData.userName).updateToName(toData.userName)
                .updateFromPosition(fromData.position).updateToPosition(toData.position).addCount();
        tempBoardView.updatePoint(updatePoint).moveTo();
        mGiftBoardViews.add(tempBoardView);
        tempBoardView.show();
        tempBoardView.addViewToRoot(mRootView);
    }


    private void initListener() {
        RxToolKit.Build(this, GiftBoardEvent.class).action(giftBoardEvent -> {
            postSafely(() -> {
                try {
                    GameMember.GameMemberData fromData = giftBoardEvent.getFromData();
                    GameMember.GameMemberData toData = giftBoardEvent.getToData();
                    String giftType = giftBoardEvent.getGiftType();

                    Log.i(TAG, "GiftBoardEvent: update Size when create [" + mGiftBoardViews.size() + "]");
                    tryClearData();
                    for (GiftBoardView temp : mGiftBoardViews) {
                        if (temp.isEqual(fromData.position, toData.position, giftType)) {
                            /**
                             * 找到了一个正在显示的
                             */
                            temp.addCount();//增加个数
                            temp.show();
                            updateViewPoint(mGiftBoardViews.size());
                            return;
                        }
                    }
                    Log.i(TAG, "initListener: begin create ");
                    switch (mGiftBoardViews.size()) {
                        case 0: {
                            /**
                             * 当前一个提示都没有直接加一个提示
                             */
                            createAndAddView(fromData, toData, giftType, mMidPointWhenThree);
                        }
                        break;
                        case 1:
                            /**
                             * 当前已经有了一个了，拿出第一个View做移动
                             */
                            GiftBoardView giftBoardView = mGiftBoardViews.get(0);
                            giftBoardView.updatePoint(mTopPointWhenTwo).moveTo();
                            /**
                             * 创建一个View
                             */
                            createAndAddView(fromData, toData, giftType, mBottomPointWhenTwo);
                            break;
                        case 2:
                            /**
                             * 当前已经有2个了，拿第一个View做移动
                             */
                            GiftBoardView temp1 = mGiftBoardViews.get(0);
                            temp1.updatePoint(mTopPointWhenThree).moveTo();
                            /**
                             * 当前已经有2个了，拿第二个View做移动
                             */
                            GiftBoardView temp2 = mGiftBoardViews.get(1);
                            temp2.updatePoint(mMidPointWhenThree).moveTo();
                            /**
                             * 创建一个View
                             */
                            createAndAddView(fromData, toData, giftType, mBottomPointWhenThree);
                            break;
                        case 3:
                        default:
                            /**
                             * 当前已经有3个View了，移除第一个View
                             */
                            GiftBoardView t1 = mGiftBoardViews.get(0);
                            t1.destroy();
                            mRootView.removeView(t1.getView());
                            mGiftBoardViews.remove(t1);

                            GiftBoardView t2 = mGiftBoardViews.get(0);
                            t2.updatePoint(mTopPointWhenThree).moveTo();

                            GiftBoardView t3 = mGiftBoardViews.get(1);
                            t3.updatePoint(mMidPointWhenThree).moveTo();

                            createAndAddView(fromData, toData, giftType, mBottomPointWhenThree);
                            break;
                    }

                    Log.i(TAG, "initListener: after request show" + mGiftBoardViews.size());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        }).register();
    }

    @Override
    public void destroy() {
        RxToolKit.UnRegister(this);
        mSafeHandler.release();
        for (GiftBoardView view : mGiftBoardViews) {
            view.destroy();
            mRootView.removeView(view.getView());
        }
        if (mContext != null) {
            mContext = null;
        }
        if (mRootView != null) {
            mRootView = null;
        }

    }

    @Override
    public void func(GiftBoardView value) {
        postSafely(() -> {
            try {
                Log.i(TAG, "func: dimiss " + value.getGiftType() + " [" + value.getFromPosition() + "]->[" + value.getToPosition() + "]");
                mRootView.removeView(value.getView());
                mGiftBoardViews.remove(value);
                value.destroy();
                Log.i(TAG, "func: update Size when dismiss [" + mGiftBoardViews.size() + "]");
                updateViewPoint(mGiftBoardViews.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void tryClearData() {
    }

    public void updateViewPoint(int size) {
        tryClearData();
        switch (size) {
            case 0:
                break;
            case 1:
                mGiftBoardViews.get(0).updatePoint(mMidPointWhenThree).moveTo();
                break;
            case 2:
                mGiftBoardViews.get(0).updatePoint(mTopPointWhenTwo).moveTo();
                mGiftBoardViews.get(1).updatePoint(mBottomPointWhenTwo).moveTo();
                break;
            case 3:
                mGiftBoardViews.get(0).updatePoint(mTopPointWhenThree).moveTo();
                mGiftBoardViews.get(1).updatePoint(mMidPointWhenThree).moveTo();
                mGiftBoardViews.get(2).updatePoint(mBottomPointWhenThree).moveTo();
                break;
        }
    }
}
