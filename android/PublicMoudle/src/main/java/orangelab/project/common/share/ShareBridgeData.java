package orangelab.project.common.share;

import android.graphics.Bitmap;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * 分享统一存放的数据
 */
public class ShareBridgeData implements Serializable, Keepable {

    public static final String SHARE_COMMON_SINGLE = "data";
    public static final String SHARE_COMMON_DEED = "deed";
    public static final String SHARE_ONLY_IMAGE = "share_only_image";

    public static final String SHARE_FROM_GANE = "share_from_game";
    public static final String SHARE_FROM_VOICE = "share_from_voice";
    public static final String SHARE_FROM_SPY_ROOM = "share_from_spy";
    public static final String SHARE_FROM_SETTING = "share_from_setting";
    public static final String SHARE_FROM_MINIGAME = "share_from_mini_game";
    public static final String SHARE_FROM_JS = "share_from_js";
    public static final String SHARE_FROM_DEED = "share_from_deed";
    public static final String SHARE_FROM_CUSTOM = "share_from_custom";
    public static final String SHARE_FROM_LOBBY = "share_from_lobby";

    public static final String $ROOMID = "$roomid";        //后台约定的roomid的占位符号
    public static final String $PASSWORD = "$password";    //后台约定的password的占位符号


    public String shareOrigin;
    public String userName;
    public String roomId;
    public String password;
    public String title;
    public String url;
    public String message;
    public String shareType;
    public Bitmap bitmap;
    public String avatar; //分享那个人的头像链接
    public String type;  //分享类型
    public String fromType="";

    public String json = ""; //支持传递json

    public ShareBridgeData() {
    }

    public ShareBridgeData(String shareOrigin) {
        this.shareOrigin = shareOrigin;
    }

    public ShareBridgeData(String shareOrigin, String userName) {
        this.shareOrigin = shareOrigin;
        this.userName = userName;
    }

    public ShareBridgeData(String shareOrigin, String title, String url, String message) {
        this.shareOrigin = shareOrigin;
        this.title = title;
        this.url = url;
        this.message = message;
    }

    public ShareBridgeData(String shareOrigin, String title, String url, String message, String type) {
        this.shareOrigin = shareOrigin;
        this.title = title;
        this.url = url;
        this.message = message;
        this.type = type;
    }

    public ShareBridgeData(String shareOrigin, String title, String url, String message, String type, String json) {
        this.shareOrigin = shareOrigin;
        this.title = title;
        this.url = url;
        this.message = message;
        this.type = type;
        this.json = json;
    }
}
