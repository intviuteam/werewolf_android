
package orangelab.project.common.upgrade;

public interface IUpgradeServiceDefines {

    String PROCESS = "process";
    String DOWNLOAD_PATH = "file_path";
    String ACTION_NOTIFICATION = "com.xiaobanhui.werewolf.notification";
    String EXTRA_NOTIFICATION_TYPE = "notification_type";
    String ERROR_CODE = "error_code";
    String UPDATE_DETAIL = "update_detail";

    int ACTION_BEGIN_DOWNLOAD = 1002;
    int ACTION_DOWNLOAD_PROCESS = 1003;
    int ACTION_FINISH_DOWNLOAD = 1004;
    int ACTION_DOWNLOAD_ERROR = 1005;
    int ACTION_CHECK_UPDATE = 1006;

    int ERROR_CODE_CHECK_FILE = 1;//校验MD5失败
    int ERROR_CDOE_NETWORK = 2;//下载出错

    String UPDATE_PARAM_CHANNEL="channel";

}
