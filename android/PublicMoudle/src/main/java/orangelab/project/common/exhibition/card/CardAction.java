package orangelab.project.common.exhibition.card;

import android.view.View;

/**
 * game_werewolf
 * 2017/7/10 上午10:05
 * Mystery
 */

public abstract class CardAction implements View.OnClickListener {

    private boolean isDisposable = true;

    public boolean isDisposable() {
        return isDisposable;
    }

    public void setDisposable(boolean disposable) {
        isDisposable = disposable;
    }

    public CardAction() {
    }
}
