package orangelab.project.common.db.entity;

import com.toolkit.action.Keepable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * game_werewolf
 * 2018/5/17 下午4:39
 * Mystery
 */
@Entity
public class MiniGameRecordEntity {

    @Id(autoincrement = true)
    private Long id;  //唯一Id 自增属性

    private String userId; //用户的ID

    private String userToken;//用户的Token

    private String gameRecordId; //小游戏recordId唯一表示

    private String gameType; //单机小游戏类型

    private long gameCostTime; //小游戏的时间 单位为ms

    private long gameScoreRecordValue;//单机小游戏分数 数值类型

    private String extraData1;//预留字段1

    private String extraData2;//预留字段2

    @Generated(hash = 1525550945)
    public MiniGameRecordEntity(Long id, String userId, String userToken,
            String gameRecordId, String gameType, long gameCostTime,
            long gameScoreRecordValue, String extraData1, String extraData2) {
        this.id = id;
        this.userId = userId;
        this.userToken = userToken;
        this.gameRecordId = gameRecordId;
        this.gameType = gameType;
        this.gameCostTime = gameCostTime;
        this.gameScoreRecordValue = gameScoreRecordValue;
        this.extraData1 = extraData1;
        this.extraData2 = extraData2;
    }

    @Generated(hash = 284189365)
    public MiniGameRecordEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserToken() {
        return this.userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getGameRecordId() {
        return this.gameRecordId;
    }

    public void setGameRecordId(String gameRecordId) {
        this.gameRecordId = gameRecordId;
    }

    public String getGameType() {
        return this.gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public long getGameCostTime() {
        return this.gameCostTime;
    }

    public void setGameCostTime(long gameCostTime) {
        this.gameCostTime = gameCostTime;
    }

    public long getGameScoreRecordValue() {
        return this.gameScoreRecordValue;
    }

    public void setGameScoreRecordValue(long gameScoreRecordValue) {
        this.gameScoreRecordValue = gameScoreRecordValue;
    }

    public String getExtraData1() {
        return this.extraData1;
    }

    public void setExtraData1(String extraData1) {
        this.extraData1 = extraData1;
    }

    public String getExtraData2() {
        return this.extraData2;
    }

    public void setExtraData2(String extraData2) {
        this.extraData2 = extraData2;
    }

    @Override
    public String toString() {
        return "MiniGameRecordEntity{" +
                "userId='" + userId + '\'' +
                ", userToken='" + userToken + '\'' +
                ", gameRecordId='" + gameRecordId + '\'' +
                ", gameType='" + gameType + '\'' +
                ", gameCostTime=" + gameCostTime +
                ", gameScoreRecordValue=" + gameScoreRecordValue +
                '}';
    }
}
