package orangelab.project.common.engine;

import android.content.Context;

import orangelab.project.common.model.EnterRoomResult;

import cn.intviu.sdk.model.User;

/**
 * game_werewolf
 * 2018/3/31 下午1:32
 * Mystery
 * <p>
 * OrbitEngine全局托管器，由于Orbit不会同时存在两个engine所以此管理器如果多次调用 start 会销毁之前创建的一个Orbit
 */

public enum OrbitEngineManager {

    INSTANCE;

    private IOrbitEngine mOrbitEngine = null;

    private OrbitEngineManager() {

    }

    public void start(Context context, String roomId, EnterRoomResult.RoomServerMessage message, User user, boolean defaultOpenSeaker) {
        if (mOrbitEngine != null) {
            /**
             * 如果要创建一个新的Orbit 要先销毁老的Orbit
             */
            mOrbitEngine.shutdown(false, () -> {
                createAndStart(context, roomId, message, user, defaultOpenSeaker);
            });
        } else {
            createAndStart(context, roomId, message, user, defaultOpenSeaker);
        }
    }

    public void start(Context context, String roomId, EnterRoomResult.RoomServerMessage message, User user) {
        start(context, roomId, message, user, false);
    }

    private void createAndStart(Context context, String roomId, EnterRoomResult.RoomServerMessage message, User user, boolean defaultOpenSeaker) {
        mOrbitEngine = new OrbitEngine(context);
        mOrbitEngine.setDefaultOpenSpeaker(defaultOpenSeaker);
        mOrbitEngine.start(roomId, message, user);
    }

    public void closeRecord() {
        if (mOrbitEngine != null) {
            mOrbitEngine.closeRecord();
        }
    }

    public void openRecord() {
        if (mOrbitEngine != null) {
            mOrbitEngine.openRecord();
        }
    }

    public void openSpeaker() {
        if (mOrbitEngine != null) {
            mOrbitEngine.openSpeaker();
        }
    }

    public void closeSpeaker() {
        if (mOrbitEngine != null) {
            mOrbitEngine.closeSpeaker();
        }
    }

    public void shutDown() {
        shutDown(null);
    }

    /**
     * 如果调用多次销毁，只会真实销毁一次，再之后的销毁会直接执行回调
     *
     * @param complete
     */
    public void shutDown(Runnable complete) {
        if (mOrbitEngine != null) {
            mOrbitEngine.shutdown(false, complete);
            mOrbitEngine = null;
        } else {
            /**
             * 已经被shutdown过一次
             */
            if (complete != null) {
                complete.run();
            }
        }
    }
}
