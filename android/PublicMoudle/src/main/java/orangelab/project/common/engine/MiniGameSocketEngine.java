package orangelab.project.common.engine;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.androidtoolkit.TimeZoneToolKit;
import com.androidtoolkit.UIActuator;
import com.datasource.GlobalUserState;
import com.BuildConfig;
import com.R;

import orangelab.project.common.engine.task.SocketCallBack;
import orangelab.project.minigame.MiniGameConstant;
import orangelab.project.minigame.model.MiniGameLaunch;
import orangelab.project.common.model.BaseServerEvent;
import orangelab.project.common.model.EnterRoomResult;
import orangelab.project.common.model.GameEnterRoom;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.Utils;

import com.networktoolkit.transport.Constant;
import com.networktoolkit.transport.TransportConfig;
import com.toolkit.action.ApiResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.intviu.support.GsonHelper;

/**
 * game_werewolf
 * 2018/1/23 下午6:07
 * Mystery
 */

public class MiniGameSocketEngine {

    private static final List<String> MINI_GAME_ARQ_TASK_MANIFEST = new ArrayList<>();

    static {
        MINI_GAME_ARQ_TASK_MANIFEST.add(MiniGameConstant.READY);
        MINI_GAME_ARQ_TASK_MANIFEST.add(MiniGameConstant.MOVE);
    }

    private static final String TAG = "MiniGameSocketEngine";

    private SocketEngine engine = null;

    public MiniGameSocketEngine() {

    }

    public SocketEngine getEngine() {
        return engine;
    }

    public boolean engineExist() {
        return getEngine() != null;
    }

    public void releaseEngine() {
        if (engine != null) {
            engine.release();
            engine = null;
        }
    }

    public void releaseDelay() {
        if (engine != null) {
            engine.releaseDelay();
            engine = null;
        }
    }

    public void nativeRequestMessage(final String type, JSONObject payload, final SocketCallBack success, final SocketCallBack failed) {
        if (engineExist()) {
            engine.nativeRequestMessage(type, payload, success, failed);
        } else {
            PLog.i(TAG, "sendMessage: Hell! Engine is null");
        }
    }

    public void startPollMessage() {
        if (getEngine() != null) {
            getEngine().startPollMessage();
        }
    }

    public void initEngineAndEnter(MiniGameLaunch launch, ApiResult<EnterRoomResult> apiResult) {
        releaseEngine();
        try {
            final GameEnterRoom.GameEnterRoomItem item = new GameEnterRoom.GameEnterRoomItem();
            item.id = GlobalUserState.getGlobalState().getUserId();
            item.name = GlobalUserState.getGlobalState().getUserName();
            item.sex = GlobalUserState.getGlobalState().getUserSex();
            item.avatar = GlobalUserState.getGlobalState().getUserIcon();

            final GameEnterRoom.GameExtra extra = new GameEnterRoom.GameExtra();
            extra.pt = "android";
            extra.sv = Integer.parseInt(TransportConfig.SV_VALUE);
            extra.tz = TimeZoneToolKit.GetCurrentTimeZone();
            extra.v = BuildConfig.VERSION_CODE;
            extra.lg = Locale.getDefault().getLanguage();
            GameEnterRoom room = new GameEnterRoom();
            room.room_id = launch.getRoomId();
            room.password = launch.getPassword();
            room.token = GlobalUserState.getGlobalState().getToken();
            room.type = launch.getType();
            room.user = item;
            room.extra = extra;

            if (TextUtils.isEmpty(launch.getServer())) {
                UIActuator.post(() -> {
                    if (apiResult != null) {
                        apiResult.onResult(null, new Exception(MessageUtils.getString(R.string.string_action_failed)));
                    }
                });
                return;
            }

            final JSONObject sendJson = new JSONObject(GsonHelper.getGson().toJson(room));
            final SocketEngine engine = new SocketEngine(TAG, SocketEngine.MINI_GAME);
            this.engine = engine;
            engine.setSocketMsgToken(launch.getExtraData());
            engine.registerARQTaskActions(MINI_GAME_ARQ_TASK_MANIFEST);
            engine.setSocketUrl(launch.getServer(), launch.getType(), launch.getRoomId());
            engine.setSocketEngineOpenListener(new SocketEngine.SocketEngineOpenListener() {
                @Override
                public void onOpen() {
                    UIActuator.post(() -> {
                        nativeRequestMessage(Constant.TYPE_ENTER, sendJson,
                                result -> {
                                    UIActuator.post(() -> {
                                        EnterRoomResult enterResult = GsonHelper.getGson().fromJson(result, EnterRoomResult.class);
                                        if (enterResult != null && apiResult != null) {
                                            apiResult.onResult(enterResult, null);
                                        }
                                    });
                                },
                                result -> {
                                    UIActuator.post(() -> {
                                        if (apiResult != null) {
                                            apiResult.onResult(null, new Exception(result));
                                        }
                                        releaseEngine();
                                    });
                                });
                    });
                }

                @Override
                public void onError() {
                    UIActuator.post(() -> {
                        if (apiResult != null) {
                            apiResult.onResult(null, new Exception(MessageUtils.getString(R.string.network_is_unavailable)));
                        }
                        releaseEngine();
                    });
                }
            });
            engine.openSocketEngine();
        } catch (JSONException e) {
            e.printStackTrace();
            UIActuator.post(() -> {
                if (apiResult != null) {
                    apiResult.onResult(null, new Exception(MessageUtils.getString(R.string.string_unkown_error)));
                }
            });
            return;
        }

    }

    public static String CreateChangeGame() {
        JSONObject payload = new JSONObject();
        try {
            payload.put(MiniGameConstant.CHANGE_GAME, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payload.toString();
    }

    public static String CreateRequireFriend() {
        return CreateGo(MiniGameConstant.REQUIRE);
    }

    public static String CreateGo(String type) {
        JSONObject payload = new JSONObject();
        try {
            payload.put(Constant.TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload.toString();
    }

    public static String CreateChangeOppo() {
        JSONObject payload = new JSONObject();
        try {
            payload.put(MiniGameConstant.CHANGE_OPPO, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payload.toString();
    }

    public static String CreateReplay() {
        JSONObject payload = new JSONObject();
        try {
            payload.put(MiniGameConstant.REPLAY, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payload.toString();
    }

}
