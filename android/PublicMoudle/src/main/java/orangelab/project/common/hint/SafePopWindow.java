package orangelab.project.common.hint;

import android.content.Context;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;

import orangelab.project.common.utils.SafeHandler;

/**
 * Created by lloydfinch on 25/08/2017.
 */

public abstract class SafePopWindow extends PopupWindow {

    private boolean isDestroy = false;
    private SafeHandler mSafeHandler;

    public SafePopWindow(Context context) {
        super(context);
        init();
    }

    public SafePopWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SafePopWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SafePopWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public SafePopWindow() {
        super();
        init();
    }

    public SafePopWindow(View contentView) {
        super(contentView);
        init();
    }

    public SafePopWindow(int width, int height) {
        super(width, height);
        init();
    }

    public SafePopWindow(View contentView, int width, int height) {
        super(contentView, width, height);
        init();
    }

    public SafePopWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);
        init();
    }

    private void init() {
        mSafeHandler = new SafeHandler(Looper.getMainLooper());
    }

    public void runSafely(Runnable runnable) {
        runSafelyDelayed(runnable, 0);
    }

    public void runSafelyDelayed(Runnable runnable, long delay) {
        mSafeHandler.postDelaySafely(runnable, delay);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        destroy();
    }

    protected void destroy() {
        if (!isDestroy) {
            if (mSafeHandler != null) {
                mSafeHandler.release();
                mSafeHandler = null;
            }
            release();
            isDestroy = true;
        }
    }

    protected abstract void release();
}
