package orangelab.project.common.utils;

import android.content.Context;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidtoolkit.view.MaterialButton;

/**
 * game_werewolf
 * 2017/5/16 下午1:50
 * Mystery
 */

public class ViewTools {

    public static void dismissEditTextKeyboard(EditText editText) {
        if (editText == null) {
            return;
        }
        InputMethodManager imm =
                (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void wapperTouristButton(boolean isTourist, Button button, String text, int bg) {
        if (isTourist) {
            if (button != null) {
                button.setText(text);
                button.setClickable(false);
                button.setBackgroundResource(bg);
            }
        }
    }

    public static void wapperTouristButton(boolean isTourist, MaterialButton button, String text) {
        if (isTourist) {
            if (button != null) {
                button.setText(text);
                button.setClickable(false);
            }
        }
    }

    public static void setTextViewSizeDip(TextView textView, int sp) {
        if (textView == null) {
            return;
        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, sp);
    }

    public static void setTextViewSize(TextView textView, int sp) {
        if (textView == null) {
            return;
        }
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp);
    }

    public static void boldTextView(TextView textView, boolean isBold) {
        if (textView == null) {
            return;
        }
        TextPaint tp = textView.getPaint();
        tp.setFakeBoldText(isBold);
    }
}
