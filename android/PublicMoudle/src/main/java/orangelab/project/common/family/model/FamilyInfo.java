package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;

import java.io.Serializable;

/**
 * game_werewolf
 * 2017/10/27 下午2:39
 * Mystery
 */

public class FamilyInfo implements Keepable,Serializable {

    public boolean is_owner;
    public String group_id;
    public String gid;
    public String name;
    public String short_name;
    public String image;
    public int member_count;
    public String level;
    public int level_val;
    public String level_image;
    public String lc_id;
}
