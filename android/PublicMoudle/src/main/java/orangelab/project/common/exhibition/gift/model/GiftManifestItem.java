package orangelab.project.common.exhibition.gift.model;

import android.text.TextUtils;

import com.androidtoolkit.ApkToolKit;
import com.toolkit.action.Keepable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import orangelab.project.common.effect.gifts.CustomGiftAnim;

/**
 * game_werewolf
 * 2017/11/9 上午10:54
 * Mystery
 * <p>
 * 礼物清单Model
 */

public class GiftManifestItem implements Keepable {

    public String gift_type;    //礼物类型
    public String name;    //礼物姓名
    public String image;    //礼物图片，用于礼物列表展示一个小图片
    public String localImage; //礼物的本地图片，用于礼物列表展示一个小图片
    public int status;//礼物状态
    public int popular = 1000;//礼物的人气值
    public CustomGiftAnim animate;  //礼物动画
    public List<GiftManifestItemPrice> price; //礼物价格
    public String rebateMsg; //礼物折扣
    public HashMap<String, String> name_i18n;//国际化礼物名称

    public static class GiftManifestItemPrice implements Keepable {
        public String type;
        public int count;
    }

    public String getName() {
        if (TextUtils.equals(ApkToolKit.GetApkLanguage(), "zh-CN")) {
            return name;
        }
        if (name_i18n != null && name_i18n.size() > 0) {
            if (ApkToolKit.GetApkLanguage().contains("en") && name_i18n.containsKey("en")) {
                /**
                 * 使用英文
                 */
                return name_i18n.get("en");
            }
            /**
             * 不使用英文
             */
            Iterator<Map.Entry<String, String>> iterator = name_i18n.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                String key = entry.getKey();
                String value = entry.getValue();
                if (TextUtils.equals(ApkToolKit.GetApkLanguage(), key)) {
                    return value;
                }
            }
            return name;
        } else {
            /**
             * 说明不支持国际化
             */
            return name;
        }
    }
}
