package orangelab.project.common.model;

import java.util.List;

/**
 * Author:1727
 * 2017/3/18
 */

public class VoteInfo {

    public List<Integer> alives;
    public String type;
    public int duration;
}
