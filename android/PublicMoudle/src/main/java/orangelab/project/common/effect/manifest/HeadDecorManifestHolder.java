package orangelab.project.common.effect.manifest;

import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.toolkit.ToolKit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.effect.ApkResourceManager;
import orangelab.project.common.effect.headdecor.HeadDecorAnimData;
import orangelab.project.common.effect.headdecor.HeadDecorManiFest;
import orangelab.project.common.tool.GsonTurnToolKit;

/**
 * game_werewolf
 * 2018/7/12 下午8:31
 * Mystery
 */
public class HeadDecorManifestHolder extends EffectsManifestHolder<HeadDecorManiFest> {

    private static final String TAG = "HeadDecorManifestHolder";


    public HeadDecorManifestHolder(ManifestManagerHelper helper, String cacheManifestFile, String cacheManifestVersionFile, String diskLruType) {
        super(helper, cacheManifestFile, cacheManifestVersionFile, diskLruType);
    }

    @Override
    public HeadDecorManiFest transformData(String data) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        return GsonTurnToolKit.turn(data, HeadDecorManiFest.class);
    }

    public HeadDecorManiFest.HeadDecorManiFestItem getManiFestItem(String type) {
        if (TextUtils.isEmpty(type)) {
            return null;
        }
        HeadDecorManiFest headDecorManiFest = getManifestData();
        if (headDecorManiFest == null) {
            return null;
        }
        return headDecorManiFest.getItem(type);
    }

    @Override
    public void saveManifest(HeadDecorManiFest data) {
        try {
            String temp = GsonHelper.getGson().toJson(data);
            mHelper.putString(mCacheManifestFile, temp);
            putVersion(data.rev);
            updateManifest(data);
            PLog.i(TAG, "saveHeadDecor: success");
        } catch (Exception e) {
            e.printStackTrace();
            PLog.i(TAG, "save HeadDecor failed");
        }
    }

    @Override
    public Set<String> verifyManifest(HeadDecorManiFest manifest) {
        Set<String> failedType = new HashSet<>();
        if (manifest == null) {
            return failedType;
        }
        List<HeadDecorManiFest.HeadDecorManiFestItem> data = manifest.avatar_boxs;
        if (ToolKit.isEmpty(data)) {
            return failedType;
        }
        for (HeadDecorManiFest.HeadDecorManiFestItem item : data) {
            HeadDecorAnimData decorAnimData = item.animate;
            if (decorAnimData != null) {

                List<HeadDecorAnimData.HeadDecorFrame> repeatFrames = decorAnimData.repeatFrames;
                if (!ToolKit.isEmpty(repeatFrames)) {
                    for (HeadDecorAnimData.HeadDecorFrame temp : repeatFrames) {
                        if (ApkResourceManager.GetApkImageResource(temp.local) > 0) {
                            /**
                             * 本地有就不进行远程校验
                             */
                        } else {
                            if (!verifyUrlData(temp.remote)) {
                                failedType.add(item.avatar_box_type);
                            }
                        }
                    }
                }

                List<HeadDecorAnimData.HeadDecorFrame> preRepeatFrames = decorAnimData.preRepeatFrames;
                if (!ToolKit.isEmpty(preRepeatFrames)) {
                    for (HeadDecorAnimData.HeadDecorFrame temp : preRepeatFrames) {
                        if (ApkResourceManager.GetApkImageResource(temp.local) > 0) {
                            /**
                             * 本地有就不进行远程校验
                             */
                        } else {
                            if (!verifyUrlData(temp.remote)) {
                                failedType.add(item.avatar_box_type);
                            }
                        }
                    }
                }
            }
        }
        PLog.i(TAG, "HeadDecor verify finish " + failedType);
        return failedType;
    }

    @Override
    public void downloadManifestResource(HeadDecorManiFest data, boolean forceRefresh) {
        if (data == null) {
            return;
        }
        List<HeadDecorManiFest.HeadDecorManiFestItem> items = data.avatar_boxs;
        if (items == null) {
            return;
        }
        for (HeadDecorManiFest.HeadDecorManiFestItem item : items) {

            /**
             * 下载所有的头像所需的图片数据
             */
            HeadDecorAnimData decorAnimData = item.animate;
            if (decorAnimData != null) {
                List<HeadDecorAnimData.HeadDecorFrame> repeatFrames = decorAnimData.repeatFrames;
                if (!ToolKit.isEmpty(repeatFrames)) {
                    for (HeadDecorAnimData.HeadDecorFrame temp : repeatFrames) {
                        downloadAndSaveIfNeed(temp.local, temp.remote, forceRefresh);
                    }
                }
                List<HeadDecorAnimData.HeadDecorFrame> preRepeatFrames = decorAnimData.preRepeatFrames;
                if (!ToolKit.isEmpty(preRepeatFrames)) {
                    for (HeadDecorAnimData.HeadDecorFrame temp : preRepeatFrames) {
                        downloadAndSaveIfNeed(temp.local, temp.remote, forceRefresh);
                    }
                }
            }
        }
    }
}
