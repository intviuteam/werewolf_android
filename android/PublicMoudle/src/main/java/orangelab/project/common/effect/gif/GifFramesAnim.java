package orangelab.project.common.effect.gif;

import android.util.Log;
import android.widget.ImageView;

import com.androidtoolkit.PLog;

import orangelab.project.common.effect.frames.ThreadFramesAnim;

/**
 * game_werewolf
 * 2018/7/20 下午6:16
 * Mystery
 * <p>
 * 重新封装了Gif动画，不仅仅支持Gif播放，还支持自定义Bitmap的播放
 */
public class GifFramesAnim extends ThreadFramesAnim {

    private static final String TAG = "GifFramesAnim";

    private GifDecoder gifDecoder;
    private Runnable mFinishListener;
    private FrameCreator mFrameCreator;

    public GifFramesAnim(ImageView imageView) {
        super(TAG, imageView);
    }

    public void setFinishListener(Runnable finishListener) {
        mFinishListener = finishListener;
    }

    public void setLastFrameCreator(FrameCreator frameCreatorWhenStop) {
        post(() -> {
            mFrameCreator = frameCreatorWhenStop;
        });
    }

    public void setBytes(final byte[] bytes) {
        post(() -> {
            if (isRunning()) {
                return;
            }
            gifDecoder = new GifDecoder();
            try {
                gifDecoder.read(bytes);
            } catch (final Exception e) {
                e.printStackTrace();
                gifDecoder = null;
                return;
            }
        });
    }

    @Override
    protected void onDraw() {
        if (gifDecoder == null) {
            PLog.i(TAG, "gifDecoder is null");
            return;
        }

        long frameStartTime = System.currentTimeMillis();

        boolean advance = gifDecoder.charge(mRepeatTime);

        if (!advance) {
            PLog.i(TAG, "advance is false");
            /**
             * gif播完了
             */
            if (mFrameCreator != null) {
                /**
                 * 需要绘制最后一帧
                 */
                drawLastFrame(mFrameCreator);
            } else {
                stop();
            }
            return;
        }

        mBitmap = gifDecoder.getNextFrame();


        long frameCost = System.currentTimeMillis() - frameStartTime;
        postUI(() -> {
            drawBitmap(mBitmap);
        });
        long delay = gifDecoder.getNextDelay() - frameCost;
        PLog.i(TAG, "delay " + delay);
        if (delay > 0) {
            post(this, delay);
        } else {
            post(this);
        }
    }


    private void drawLastFrame(FrameCreator frameCreator) {
        if (frameCreator == null) return;
        post(() -> {
            /**
             * 这里先将Gif设为暂定
             */
            mPause = true;
            mResume = false;

            if (!mShouldRun) return;

            mBitmap = frameCreator.createFrame();

            long duration = frameCreator.getDuration();

            postUI(() -> {
                clearImageView();
                drawBitmap(mBitmap);
            });

            /**
             * 清空图片
             */
            if (duration > 0) {
                postUI(() -> {
                    clearImageView();
                    stop();
                }, duration);
            } else {
                postUI(() -> {
                    stop();
                });
            }
            PLog.i(TAG, "drawLastFrame: ");
        });
    }

    @Override
    protected void onRelease() {
        if (gifDecoder != null) {
            gifDecoder.clear();
        }
    }


    @Override
    protected void onResume() {

    }

    @Override
    protected void onStart() {

    }

    @Override
    protected void onStop() {

    }

    @Override
    protected void onPause() {

    }

    @Override
    protected void onFinish() {
        if (mFinishListener != null) {
            mFinishListener.run();
        }
    }
}
