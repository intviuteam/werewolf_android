package orangelab.project.common.utils;

import android.os.Handler;
import android.os.Looper;

/**
 * game_werewolf
 * 2017/7/10 下午1:27
 * Mystery
 */

public class SafeHandler extends Handler {

    private boolean isDestroy = false;

    public SafeHandler() {
        super(Looper.getMainLooper());
    }

    public SafeHandler(Callback callback) {
        super(callback);
    }

    public SafeHandler(Looper looper) {
        super(looper);
    }

    public SafeHandler(Looper looper, Callback callback) {
        super(looper, callback);
    }

    public void postSafely(Runnable runnable) {
        if (!isDestroy) {
            post(runnable);
        }
    }

    public void postDelaySafely(Runnable runnable, long delay) {
        if (!isDestroy) {
            postDelayed(runnable, delay);
        }
    }

    public void release() {
        if (!isDestroy) {
            removeCallbacksAndMessages(null);
            isDestroy = true;
        }
    }
}
