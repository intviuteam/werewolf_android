package orangelab.project.common.db;

import android.content.Context;
import android.text.TextUtils;

import com.toolkit.action.Destroyable;
import orangelab.project.common.db.entity.BlackListLightEntity;
import orangelab.project.common.db.gen.BlackListLightEntityDao;
import orangelab.project.common.db.gen.DaoMaster;
import orangelab.project.common.db.gen.DaoSession;
import com.androidtoolkit.PLog;

import java.util.List;

/**
 * game_werewolf
 * 2017/9/21 下午2:25
 * Mystery
 */

public class BlackListLightDaoHelper implements Destroyable {

    private static final String TAG = "BlackListLightDaoHelper";
    private DaoMaster.DevOpenHelper mHelper;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    private BlackListLightEntityDao mDao;

    public BlackListLightDaoHelper(Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context, DBConstant.DB_NAME, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
        mDaoSession = mDaoMaster.newSession();
        mDao = mDaoSession.getBlackListLightEntityDao();
    }

    public void insert(String myUserId, BlackListLightEntity entity) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return;
        }
        if (entity == null) {
            return;
        }
        if (mDao == null) {
            return;
        }
        BlackListLightEntity temp = mDao.queryBuilder().where(BlackListLightEntityDao.Properties.FromUserId.eq(myUserId), BlackListLightEntityDao.Properties.UserId.eq(entity.getUserId())).build()
                .unique();
        if (temp != null) {
            /**
             * has add this personal,so skip it
             */
            return;
        }
        entity.setFromUserId(myUserId);
        mDao.insert(entity);
    }

    public void refresh(String myUserId, BlackListLightEntity entity, long updateTime) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return;
        }
        if (entity == null) {
            return;
        }
        if (mDao == null) {
            return;
        }
        /**
         * 先更新时间
         */
        entity.setUpdateTime(updateTime);
        BlackListLightEntity temp = mDao.queryBuilder().where(
                BlackListLightEntityDao.Properties.FromUserId.eq(myUserId),
                BlackListLightEntityDao.Properties.UserId.eq(entity.getUserId())).build().unique();
        if (temp != null) {
            /**
             * 说明之前数据库有这条数据，而且刷新之后还是有的，直接更新它的updateTime
             */
            temp.setUpdateTime(updateTime);
            mDao.update(temp);
            return;
        } else {
            /**
             * 说明之前没有这条数据,进行插入操作
             */
            entity.setFromUserId(myUserId);
            mDao.insert(entity);
        }
    }

    public void refreshAll(String myUserId, long updateTime) {
        List<BlackListLightEntity> all = getAllList(myUserId);
        if (all != null && all.size() > 0) {
            for (BlackListLightEntity entity : all) {
                if (entity.getUpdateTime() != updateTime) {
                    if (mDao != null) {
                        mDao.delete(entity);
                    }
                }
            }
        }
    }

    public List<BlackListLightEntity> getAllList(String myUserId) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return null;
        }
        if (mDao != null) {
            List<BlackListLightEntity> entities = mDao.queryBuilder().where(
                    BlackListLightEntityDao.Properties.FromUserId.eq(myUserId)).build().list();
            return entities;
        }
        return null;
    }

    public boolean hasEntity(String myUserId, String userId) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return false;
        }
        if (mDao == null) {
            return false;
        }
        BlackListLightEntity temp = mDao.queryBuilder().where(
                BlackListLightEntityDao.Properties.FromUserId.eq(myUserId),
                BlackListLightEntityDao.Properties.UserId.eq(userId)).build().unique();
        if (temp != null) {
            return true;
        } else {
            return false;
        }
    }

    public void deleteAll(String myUserId) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return;
        }
        if (mDao == null) {
            return;
        }
        List<BlackListLightEntity> temp = mDao.queryBuilder().where(BlackListLightEntityDao.Properties.FromUserId.eq(myUserId)).build().list();
        if (temp != null) {
            for (BlackListLightEntity entity : temp) {
                mDao.delete(entity);
            }
        }
    }

    public void delete(String myUserId, String userId) {
        if (TextUtils.isEmpty(myUserId)) {
            PLog.i(TAG, "myUserId is empty");
            return;
        }
        if (mDao == null) {
            return;
        }
        BlackListLightEntity temp = mDao.queryBuilder().where(
                BlackListLightEntityDao.Properties.FromUserId.eq(myUserId),
                BlackListLightEntityDao.Properties.UserId.eq(userId)).build().unique();
        if (temp != null) {
            mDao.delete(temp);
        }
    }


    @Override
    public void destroy() {

    }
}
