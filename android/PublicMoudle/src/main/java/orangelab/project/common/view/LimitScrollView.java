package orangelab.project.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/6/14 上午10:23
 * Mystery
 */

public class LimitScrollView extends ScrollView {

    private Context context;
    private int limitRate = 2;

    public LimitScrollView(Context context) {
        this(context, null);
    }

    public LimitScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LimitScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(
                ScreenUtils.getCurrentScreenHeight() / limitRate, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
