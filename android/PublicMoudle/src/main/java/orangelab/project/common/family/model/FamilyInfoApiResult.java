package orangelab.project.common.family.model;

import com.toolkit.action.Keepable;

import java.util.ArrayList;

/**
 * game_werewolf
 * 2017/10/26 下午4:04
 * Mystery
 */

public class FamilyInfoApiResult implements Keepable {

    public FamilyInfoApiResultGroup group;

    public static class FamilyInfoApiResultGroup implements Keepable {
        public String _id;
        public String gid;
        public String name;
        public String short_name;
        public String desc;
        public String level;
        public int level_val = 1;
        public String level_image;
        public String image;
        public String own_id;
        public int member_count;
        public long updated_at;
        public long created_at;
        public int active_point;
        public int max_members;
        public String lc_id;
        public Integer require_count;
        public ArrayList<FamilyInfoMember> members;

    }

    public static class FamilyInfoMember implements Keepable {
        public String user_id;
        public String title;
        public String _id;
        public String image;
        public String name;
    }
}
