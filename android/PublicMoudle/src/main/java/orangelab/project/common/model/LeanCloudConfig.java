package orangelab.project.common.model;

/**
 * game_werewolf
 * 2017/10/12 上午11:36
 * Mystery
 */

public class LeanCloudConfig {

    public String host;
    public boolean unmute_send;
    public LeanCloudSeverConfig leancloud;

    public static class LeanCloudSeverConfig {
        public String api;
        public String rtm;
    }
}
