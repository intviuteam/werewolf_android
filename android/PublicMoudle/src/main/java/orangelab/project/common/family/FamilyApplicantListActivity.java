package orangelab.project.common.family;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.androidtoolkit.RxToolKit;
import com.R;

import orangelab.project.common.activity.LockAbleActivity;
import orangelab.project.common.family.adapter.FamilyApplicantListAdapter;
import orangelab.project.common.family.event.FamilyEvent;
import orangelab.project.common.family.manager.FamilyApiManager;
import orangelab.project.common.family.model.FamilyApplicantApiResult;
import orangelab.project.common.tool.RecycleViewPageHelper;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.MessageUtils;
import orangelab.project.common.utils.RunnableFactory;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.RecycleViewDivider;
import orangelab.project.common.view.TitleView;

import com.networktoolkit.transport.RequestTask;
import com.networktoolkit.transport.RequestTaskCallBack;

import cn.intviu.support.GsonHelper;

import com.androidtoolkit.view.ScreenUtils;
import com.datasource.GlobalUserState;

/**
 * game_werewolf
 * 2017/10/19 下午7:02
 * Mystery
 */

public class FamilyApplicantListActivity extends LockAbleActivity implements SwipeRefreshLayout.OnRefreshListener, RecycleViewPageHelper.RecycleViewPageHelperAction, View.OnClickListener {

    private static final String TAG = "FamilyApplicantListActi";

    private TitleView mTitle;
    private Button mApproveAll;
    private Button mRejectAll;
    private SwipeRefreshLayout mListContainer;
    private RecyclerView mList;
    private RecycleViewPageHelper mRecycleViewPageHelper;
    private FamilyApplicantListAdapter mAdapter;

    private RequestTask mCurRequestTask = null;


    public static void Launch(Context context) {
        Intent intent = new Intent(context, FamilyApplicantListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        setContentView(R.layout.layout_activity_family_applicant_list);
        initView();
        executeActivityDrawFinish(() -> {
            addUnLockViewRect(mTitle);
            requestAllLock();
            onRefresh();
        });
    }

    private void initView() {
        mTitle = (TitleView) findViewById(R.id.id_family_applicant_list_title);
        mTitle.setTitle(R.string.string_family_apply_title);
        mTitle.setActionBack(v -> {
            finish();
        });
        mApproveAll = (Button) findViewById(R.id.id_family_applicant_approve_all);
        mRejectAll = (Button) findViewById(R.id.id_family_applicant_clear_all);
        mApproveAll.setOnClickListener(this);
        mRejectAll.setOnClickListener(this);
        mListContainer = (SwipeRefreshLayout) findViewById(R.id.id_family_applicant_list_container);
        mListContainer.setOnRefreshListener(this);
        mList = (RecyclerView) findViewById(R.id.id_family_applicant_list);
        mList.setLayoutManager(new WrapContentLinearLayoutManager(this));
        mAdapter = new FamilyApplicantListAdapter();
        mList.setAdapter(mAdapter);
        mList.addItemDecoration(new RecycleViewDivider(this, LinearLayoutManager.HORIZONTAL, ScreenUtils.dip2px(0.5f), Color.parseColor("#e5e5e5")));
        RecycleViewPageHelper.RecycleViewPageHelperConfig config = new RecycleViewPageHelper.RecycleViewPageHelperConfig.Builder().pageIndex(0).pageSize(20).nextPageEnable(true).action(this).build();
        mRecycleViewPageHelper = new RecycleViewPageHelper(mList, mAdapter, config);
    }


    @Override
    public void onRefresh() {
        mRecycleViewPageHelper.refreshPage();

        runOnUiThreadSafely(() -> {
            if (mCurRequestTask != null && mCurRequestTask.isExecuting()) {
                mCurRequestTask.cancel();
                mCurRequestTask = null;
            }
            mListContainer.setRefreshing(false);
        }, 15000);
    }

    @Override
    public void onRefresh(int pageIndex, int pageSize, RecyclerView.Adapter mAdapter) {
        createAndExecuteRequest(pageIndex, pageSize, mAdapter);
    }

    @Override
    public void onNextPage(int pageIndex, int pageSize, RecyclerView.Adapter mAdapter) {
        createAndExecuteRequest(pageIndex, pageSize, mAdapter);
    }

    public void createAndExecuteRequest(int skip, int limit, RecyclerView.Adapter adapter) {
        if (mCurRequestTask != null && mCurRequestTask.isExecuting()) {
            mCurRequestTask.cancel();
            mCurRequestTask = null;
        }
        mCurRequestTask = RequestTask.create().isGet(true).request("/group/member/require_list?skip=" + (skip * limit) + "&limit=" + limit)
                .withToken(GlobalUserState.getGlobalState().getToken())
                .action(new RequestTaskCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        runOnUiThreadSafely(() -> {
                            FamilyApplicantApiResult apiResult = GsonHelper.getGson().fromJson(response, FamilyApplicantApiResult.class);
                            if (skip == 0) {
                                mAdapter.updateData(apiResult.datas);
                            } else {
                                mAdapter.addData(apiResult.datas);
                            }
                            requestAllUnLock();
                            mRecycleViewPageHelper.loadMoreFinish();
                            mListContainer.setRefreshing(false);
                        });
                    }

                    @Override
                    public void onFailed(int code, String failed) {
                        runOnUiThreadSafely(() -> {
                            RunnableFactory.createMsgToastRunnable(failed).run();
                            mListContainer.setRefreshing(false);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        runOnUiThreadSafely(() -> {
                            RunnableFactory.createNetworkErrorRunnable().run();
                            mListContainer.setRefreshing(false);
                        });
                    }
                }).build();
        mCurRequestTask.execute();
    }

    @Override
    public void finish() {
        super.finish();
        if (mCurRequestTask != null && mCurRequestTask.isExecuting()) {
            mCurRequestTask.cancel();
        }
        if (mAdapter != null) {
            mAdapter.destroy();
        }
        RxToolKit.Emit(new FamilyEvent.RefreshFamilyInfoEvent());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.id_family_applicant_approve_all) {
            addLockViewRect(mApproveAll);
            FamilyApiManager.ApproveAllFamilyApply((s, e) -> {
                runOnUiThreadSafely(() -> {
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        /**
                         * 全部通过成功
                         */
                        RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_approve_all)).run();
                        mAdapter.clear();
                    }
                    removeLockViewRect(mApproveAll);
                });
            });
        } else if (id == R.id.id_family_applicant_clear_all) {
            addLockViewRect(mRejectAll);
            FamilyApiManager.RejectAllFamilyApply((s, e) -> {
                runOnUiThreadSafely(() -> {
                    if (e != null) {
                        RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
                    } else {
                        /**
                         * 全部清楚
                         */
                        RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_reject_all)).run();
                        mAdapter.clear();
                    }
                    removeLockViewRect(mRejectAll);
                });
            });
        }
//        switch (v.getId()) {
//            case R.id.id_family_applicant_approve_all: {
//                addLockViewRect(mApproveAll);
//                FamilyApiManager.ApproveAllFamilyApply((s, e) -> {
//                    runOnUiThreadSafely(() -> {
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            /**
//                             * 全部通过成功
//                             */
//                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_approve_all)).run();
//                            mAdapter.clear();
//                        }
//                        removeLockViewRect(mApproveAll);
//                    });
//                });
//            }
//            break;
//            case R.id.id_family_applicant_clear_all: {
//                addLockViewRect(mRejectAll);
//                FamilyApiManager.RejectAllFamilyApply((s, e) -> {
//                    runOnUiThreadSafely(() -> {
//                        if (e != null) {
//                            RunnableFactory.createMsgToastRunnable(e.getMessage()).run();
//                        } else {
//                            /**
//                             * 全部清楚
//                             */
//                            RunnableFactory.createMsgToastRunnable(MessageUtils.getString(R.string.string_reject_all)).run();
//                            mAdapter.clear();
//                        }
//                        removeLockViewRect(mRejectAll);
//                    });
//                });
//            }
//            break;
//        }
    }
}
