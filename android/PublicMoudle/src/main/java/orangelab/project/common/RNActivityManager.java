package orangelab.project.common;

import android.app.Activity;

/**
 * game_werewolf
 * 2018/4/8 下午7:45
 * Mystery
 */

public enum RNActivityManager {

    INSTANCE;

    private Activity mActivity;

    private RNActivityManager() {

    }

    public void BindRNActivity(Activity activity) {
        this.mActivity = activity;
    }

    public Activity GetRNActivity() {
        return mActivity;
    }

    public void UnRegisterRNActivity() {
        mActivity = null;
    }
}
