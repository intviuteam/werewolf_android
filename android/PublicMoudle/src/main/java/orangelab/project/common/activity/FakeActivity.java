package orangelab.project.common.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.datasource.GlobalUserState;

import orangelab.project.common.RNActivityManager;

/**
 * game_werewolf
 * 2018/4/3 上午11:00
 * Mystery
 * 一个假的Activity 主要用于唤醒应用
 */

public class FakeActivity extends SafeActivity {

    @Override
    protected void onActivityWindowInitFinish() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         *
         */
        if (TextUtils.isEmpty(GlobalUserState.getGlobalState().getToken())) {
            /**
             * 应用没有了,启动RN Acitivty
             */
            try {
                getIntent().setComponent(new ComponentName(this, RNActivityManager.INSTANCE.GetRNActivity().getClass()));
                getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(getIntent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        } else {
            /**
             * 应用启动过
             */
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        finish();
    }


}
