package orangelab.project.common.model;

import java.util.HashMap;

/**
 * Created by wangxu on 2017/3/9.
 */

public class KillInfoResult {
    public HashMap<Integer, Integer> kill_info;
    public boolean finished;
}
