package orangelab.project.common.context

import orangelab.project.common.context.ServerMessageProcess
import orangelab.project.common.model.action.*

/**
 * game_werewolf
 * 2018/3/19 下午5:56
 * Mystery
 */

open class EmptyServerMessageProcess : ServerMessageProcess {

    constructor()

    override fun handleServerActionAddFriend(serverActionAddFriend: ServerActionAddFriend) {

    }

    override fun handleServerSocketDisconnect() {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerSocketConnect() {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerSocketReconnectFailed(reason: String) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionAcceptFreeMode(serverActionAcceptFreeMode: ServerActionAcceptFreeMode) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionChangeUserState(serverActionChangeUserState: ServerActionChangeUserState) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionChat(serverActionChat: ServerActionChat) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionDownSeat(serverActionDownSeat: ServerActionDownSeat) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionForceSeat(serverActionForceSeat: ServerActionForceSeat) {

    }

    override fun handleServerActionHandOverMaster(serverActionHandOverMaster: ServerActionHandOverMaster) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionJoin(serverActionJoin: ServerActionJoin) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionKickOut(serverActionKickOut: ServerActionKickOut) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionLeave(serverActionLeave: ServerActionLeave) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionLikeRoom(serverActionLikeRoom: ServerActionLikeRoom) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionLock(serverActionLock: ServerActionLock) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionRejectFreeMode(serverActionRejectFreeMode: ServerActionRejectFreeMode) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionRequestFreeMode(serverActionRequestFreeMode: ServerActionRequestFreeMode) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionRestoreRoom(serverActionRestoreRoom: ServerActionRestoreRoom) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionSpeak(serverActionSpeak: ServerActionSpeak) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionSystemMsg(serverActionSystemMsg: ServerActionSystemMsg) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionUnSpeak(serverActionUnSpeak: ServerActionUnSpeak) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionUpdateConfig(serverActionUpdateConfig: ServerActionUpdateConfig) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionUpdateMaster(serverActionUpdateMaster: ServerActionUpdateMaster) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionUpdateTitle(serverActionUpdateTitle: ServerActionUpdateTitle) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionUpSeat(serverActionUpSeat: ServerActionUpSeat) {
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleServerActionChangePassword(serverActionChangePassword: ServerActionChangePassword) {

    }

    override fun handleServerActionShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion) {

    }

}
