package orangelab.project.common.engine;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.androidtoolkit.RxToolKit;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;

import orangelab.project.MainApplication;

import com.androidtoolkit.PLog;

import com.networktoolkit.transport.Constant;

import orangelab.project.common.engine.task.SocketCallBack;
import orangelab.project.common.model.BaseServerEvent;
import orangelab.project.common.model.CupidSelectData;
import orangelab.project.common.model.ServerMessageEvent;
import orangelab.project.common.model.StartResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.intviu.support.GsonHelper;
import orangelab.project.common.model.action.ClientActionSendPaperPlane;
import orangelab.project.common.utils.IntentDataHelper;
import orangelab.project.common.utils.Utils;
import orangelab.project.game.WereWolfConstant;
import orangelab.project.game.event.GameEvent;
import orangelab.project.game.service.BackGroundMediaService;
import orangelab.project.voice.constants.VoiceConstants;
import orangelab.project.voice.privateroom.model.PrivateRoomMemberData;

import com.datasource.GlobalUserState;
import com.toolkit.ToolKit;
import com.toolkit.action.TravelCollection;

/**
 * Author:1727
 * 2017/3/6
 */

public enum RoomSocketEngineHelper {

    INSTANCE;

    private static final String TAG = "RoomSocketEngineHelper";

    private RoomSocketEngineHelper() {

    }

    public long getServerTime() {
        return MainApplication.getInstance().getSocketEngineHelper().getServerTime();
    }

    public void nativeRequestMessage(final String type, JSONObject payload) {
        INSTANCE.nativeRequestMessage(type, payload, null, null);
    }

    public void nativeRequestMessage(final String type, JSONObject payload, final SocketCallBack
            success, final SocketCallBack failed) {
        MainApplication.getInstance().getSocketEngineHelper().nativeRequestMessage(type, payload, success, failed);
    }

    /**
     * 游戏开始
     */
    public static void sendGameStart(SocketCallBack successCallback, SocketCallBack
            failedCallback) {
        INSTANCE.nativeRequestMessage(Constant.START, null, successCallback, failedCallback);
    }

    public static long serverTime() {
        return INSTANCE.getServerTime();
    }

    public static void sendIsWolfEvent(int number, int king) {
        RxToolKit.Emit(new GameEvent.IsWolfEvent(number, king));
    }

    public static void startSpeak() {
        INSTANCE.nativeRequestMessage(Constant.SPEAK, null);
    }

    public static void endSpeak() {
        INSTANCE.nativeRequestMessage(Constant.UNSPEAK, null);
    }

    /**
     * 杀人
     */
    public static void sendKill(int position) {
        sendPositionMessage(Constant.KILL, position);
    }

    public static void savePeople(int position) {
        sendPositionMessage(Constant.SAVE, position);
    }

    public static void sendEmptyType(String type) {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(type, jsonObject);
    }

    public static void sendPositionMessage(String type, int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.POSITION, (position - 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(type, jsonObject);
    }

    public static void sendChangePassword(String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.PASSWORD, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.CHANGE_PASSWORD, jsonObject);
    }

    public static void sendChatMessage(String msg) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MESSAGE, msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.TYPE_CHAT, jsonObject);
    }

    public static void sendGiftChatMessage(String msg, int from, int to, String giftType, String ticket) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MESSAGE, msg);
            JSONObject gift = new JSONObject();
            gift.put(Constant.GIFT_FROM, from);
            gift.put(Constant.GIFT_TO, to);
            gift.put(Constant.GIFT_TYPE, giftType);
            gift.put(Constant.GIFT_TICKET, ticket);
            jsonObject.put(Constant.GIFT_CHAT, gift);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.TYPE_CHAT, jsonObject);
    }

    /**
     * 添加了一个displayType
     */
    public static void sendGiftChatMessageByType(String msg, int from, int to, String giftType, String ticket, String displayType) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MESSAGE, msg);
            JSONObject gift = new JSONObject();
            gift.put(Constant.GIFT_FROM, from);
            gift.put(Constant.GIFT_TO, to);
            gift.put(Constant.GIFT_TYPE, giftType);
            gift.put(Constant.GIFT_TICKET, ticket);
            gift.put(Constant.DISPLAY_TYPE, displayType);
            jsonObject.put(Constant.GIFT_CHAT, gift);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.TYPE_CHAT, jsonObject);
    }

    public static int getOptDurTime(JSONObject jsonObject) {
        int dur = jsonObject.optInt(Constant.DURATION);
        return dur / 1000;
    }

    public static boolean getLeaveByNormal(JSONObject jsonObject) {
        return jsonObject.optBoolean(Constant.REMOVE);
    }

    public static boolean getLeaveByDeath(JSONObject jsonObject) {
        return jsonObject.optBoolean(Constant.DEAD);
    }

    public static int getPositionFromJSON(JSONObject jsonObject) {
        return jsonObject.optInt(Constant.POSITION);
    }

    public static void sendCoupleMessage(int position1, int position2) {
        List<Integer> data = new ArrayList<>();
        data.add(position1);
        data.add(position2);
        CupidSelectData transport = new CupidSelectData();
        transport.positions = data;
        try {
            INSTANCE.nativeRequestMessage(Constant.LINK, new JSONObject(GsonHelper.getGson().toJson(transport)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static boolean isCupid(String role) {
        return TextUtils.equals(role, Constant.ROLE_CUPID);
    }

    public static void sendElection(boolean flag) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.APPLY, flag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.APPLY, jsonObject);
    }

    public static void playMedia(Context context, String type, int dur) {
        Intent intent = new Intent(context, BackGroundMediaService.class);
        IntentDataHelper.setMediaType(intent, type);
        IntentDataHelper.setMediaDurTime(intent, dur);
        context.startService(intent);
    }

    public static int getPositionJSONFrom(JSONObject payload) {
        return payload.optInt(Constant.FROM);
    }

    public static int getPositionJSONTo(JSONObject payload) {
        return payload.optInt(Constant.TO);
    }

    public static void openUserMainPage(String userId) {
        WritableMap params = Arguments.createMap();
        params.putString(Constant.USER_ID, userId);
        MainApplication.getInstance().getNativeJSModule().sendEvent(Constant.EVENT_USER_MAIN_PAGE, params);
    }

    public static void userLeaveReportRN() {
        WritableMap params = Arguments.createMap();
        sendEventToRn(Constant.EVENT_USER_LEAVE, params);
    }

    public static void userStartGameReportRn() {
        Utils.runSafely(() -> {
            WritableMap params = Arguments.createMap();
            params.putString(orangelab.thirdparty.leancloud.chatkit.utils.Constant.GAME_TYPE, GlobalUserState.getGlobalState().getCurGameType());
            sendEventToRn(Constant.EVENT_USER_START, params);
        });
    }

    public static void userLeaveFromWeb() {
        WritableMap params = Arguments.createMap();
        sendEventToRn(Constant.EVENT_USER_LEAVE_FROM_WEB, params);
    }


    public static void sendEventToRn(String event, WritableMap map) {
        if (MainApplication.getInstance().getNativeJSModule() != null) {
            PLog.i(TAG, "sendEventToRn: event=" + event + " map =" + map.toString());
            MainApplication.getInstance().getNativeJSModule().sendEvent(event, map);
        }
    }

    public static void resetMaster() {
        INSTANCE.nativeRequestMessage(Constant.RESET_MASTER, null);
    }

    public static void postMessageIntoDebug(String msg) {
        RxToolKit.Emit(new GameEvent.SendMessageEvent(msg));
    }

    public static ServerMessageEvent createUpdateMasterInner(int position) {
        JSONObject jsonObject = new JSONObject();
        JSONObject payload = new JSONObject();
        try {
            jsonObject.put(BaseServerEvent.TYPE, Constant.TYPE_UPDATE_MASTER);
            payload.put(Constant.POSITION, position);
            jsonObject.put(BaseServerEvent.PAYLOAD, payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PLog.i(TAG, "createUpdateMasterInner: " + jsonObject.toString());
        return new ServerMessageEvent(SocketEngine.WEREWOLF, jsonObject);
    }

    public static boolean hasWitch(JSONObject payload) {
        boolean returnValue = false;
        try {
            StartResult result = GsonHelper.getGson().fromJson(payload.toString(), StartResult.class);
            Map<String, Integer> roleMap = result.role_map;
            if (roleMap != null) {
                if (roleMap.containsKey(Constant.ROLE_WITCH)) {
                    returnValue = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static boolean hasSeer(JSONObject payload) {
        boolean returnValue = false;
        try {
            StartResult result = GsonHelper.getGson().fromJson(payload.toString(), StartResult.class);
            Map<String, Integer> roleMap = result.role_map;
            if (roleMap != null) {
                if (roleMap.containsKey(Constant.ROLE_SEER)) {
                    returnValue = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static void robCardApplyRole(String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.ROLE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.APPLY_ROLE, jsonObject);
    }

    public static void sendRequestFriendInGame(String toUserId, int toPosition) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.putOpt(Constant.USER_ID_LOWER, toUserId);
            jsonObject.putOpt(Constant.POSITION, toPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.TYPE_ADD_FRIEND, jsonObject);
    }


    //更新语音房标题
    public static void sendUpdateTitle(String title, String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.VOICE_TITLE, title);
            jsonObject.put(Constant.VOICE_TITLE_TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.UPDATE_TITLE, jsonObject);
    }

    //抱用户上麦
    public static void sendForceSeat(String userId, int position, int destPosition) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_LOWER, userId);
            jsonObject.put(Constant.POSITION, position);
            jsonObject.put(Constant.DEST_POSITION, destPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.FORCE_SEAT, jsonObject);
    }

    //上麦
    public static void sendUpSeat(int destPosition) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.DEST_POSITION, destPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.UP_SEAT, jsonObject);
    }

    //下麦
    public static void sendDownSeat(int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.DOWN_SEAT, jsonObject);
    }

    public static void sendExportMsg(String title, String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.TITLE, title);
            jsonObject.put(Constant.TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.EXPORT_PAY, jsonObject);
    }

    public static void sendExportMsgWithCallBack(String title, String type, SocketCallBack success, SocketCallBack failed) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.TITLE, title);
            jsonObject.put(Constant.TYPE, type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.EXPORT_PAY, jsonObject, success, failed);
    }

    public static void sendHandOverRoomOwner(int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.HAND_OVER_MASTER, jsonObject);
    }

    public static void sendSpeechDirection(String direction) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.DIRECTION, direction);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.SPEECH_DIRECTION, jsonObject);
    }

    public static void updateConfig(int level) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.GAME_LEVEL, level);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.UPDATE_CONFIG, jsonObject);
    }

    public static void ResetMasterAllowed(boolean isAllow) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.ALLOW, isAllow);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.RESET_MASTER_RESULT, jsonObject);
    }

    public static void sendLikeRoom() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(Constant.LIKE_ROOM, jsonObject);
    }

    public static void sendChangeUserState(String model, int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MODEL, model);
            jsonObject.put(Constant.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.CHANGE_USER_STATE, jsonObject);
    }

    public static void sendBanMic(int position) {
        sendChangeUserState(Constant.LIMIT, position);
    }

    public static void sendUnBanMic(int position) {
        sendChangeUserState(Constant.SPACE_STR, position);
    }

    public static void sendFreeStyle(int position) {
        sendChangeUserState(Constant.FREE, position);
    }

    public static void sendUnFreeStyle(int position) {
        sendChangeUserState(Constant.SPACE_STR, position);
    }

    public static void sendEmotion(String mark, String url, int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MARK, mark);
            jsonObject.put(Constant.URL, url);
            jsonObject.put(Constant.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.SHOW_EMOTION, jsonObject);
    }

    public static void sendGameEmotion(String mark, int position, int res) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.MARK, mark);
            jsonObject.put(Constant.POSITION, position);
            if (res >= 0) {
                JSONObject data = new JSONObject();
                data.put(Constant.RES, res);
                jsonObject.put(Constant.DATA, data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.SHOW_GAME, jsonObject);
    }

    //踢出
    public static void kickOut(int position, String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.POSITION, position);
            jsonObject.put(Constant.UID, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.KICK_OUT, jsonObject);
    }

    //获取语音房黑名单列表
    public static void getVoiceBlackList() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(Constant.VOICE_BLACK_LIST, jsonObject);
    }

    //添加房间黑名单
    public static void addVoiceBlack(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.UID, userId);
            jsonObject.put(Constant.ADD_BLACK, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.KICK_OUT, jsonObject);
    }

    //解除房间黑名单
    public static void removeVoiceBlack(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.USER_ID_CAMEL, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(Constant.UN_BLACK, jsonObject);
    }

    public static void SendGiftMessage(String peerId, String giftType, SocketCallBack success, SocketCallBack failed) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.SOCKET_SEND_GIFT_PEER_ID, peerId);
            jsonObject.put(Constant.SOCKET_SEND_GIFT_GIFT_TYPE, giftType);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        INSTANCE.nativeRequestMessage(Constant.SOCKET_SEND_GIFT, jsonObject, success, failed);
    }

    public static void SendCardMessage(String peerId, String giftType, SocketCallBack success, SocketCallBack failed) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.SOCKET_SEND_GIFT_PEER_ID, peerId);
            jsonObject.put(Constant.SOCKET_SEND_GIFT_CARD_TYPE, giftType);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        INSTANCE.nativeRequestMessage(Constant.SOCKET_SEND_CARD, jsonObject, success, failed);
    }


    public static void SendGiftMessageByType(String peerId, String giftType, String displayType, SocketCallBack success, SocketCallBack failed) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.SOCKET_SEND_GIFT_PEER_ID, peerId);
            jsonObject.put(Constant.SOCKET_SEND_GIFT_GIFT_TYPE, giftType);
            jsonObject.put(Constant.DISPLAY_TYPE, displayType);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        INSTANCE.nativeRequestMessage(Constant.SOCKET_SEND_GIFT, jsonObject, success, failed);
    }

    public static void SendCardMessageByType(String peerId, String giftType, String displayType, SocketCallBack success, SocketCallBack failed) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.SOCKET_SEND_GIFT_PEER_ID, peerId);
            jsonObject.put(Constant.SOCKET_SEND_GIFT_CARD_TYPE, giftType);
            jsonObject.put(Constant.DISPLAY_TYPE, displayType);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        INSTANCE.nativeRequestMessage(Constant.SOCKET_SEND_CARD, jsonObject, success, failed);
    }

    //wwq 2018-02-08 去观战//
    public static void sendToObserve(int position) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constant.POSITION, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.GAME_DOWN_SEAT, jsonObject);
    }

    //wwq 2018-02-08 上座//
    public static void sendGameUpSeat() {

        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(Constant.GAME_UP_SEAT, jsonObject);
    }

    public static void sendUpdateBackGround(String url, String type, String imageId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("url", url);
            jsonObject.put("type", type);
            jsonObject.put("image_id", imageId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.UPDATE_BACKGROUND, jsonObject);
    }

    public static void sendImageChatMessage(String key, String url, long time) {
        JSONObject jsonObject = new JSONObject();
        JSONObject imageJson = new JSONObject();
        try {
            imageJson.put("image_id", key);
            imageJson.put("url", url);
            imageJson.put("expire_time", time);
            jsonObject.put(Constant.TYPE, "image");
            jsonObject.put("image", imageJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(Constant.TYPE_CHAT, jsonObject);
    }

    public static boolean sendPaperPlane(PrivateRoomMemberData from, List<PrivateRoomMemberData> to, String message, boolean needSendUnSeat) {
        if (from == null) {
            return false;
        }
        if (to == null) {
            to = new ArrayList<>();
        }

        if (to.size() == 0 && !needSendUnSeat) {
            return false;
        }

        ClientActionSendPaperPlane actionSendPaperPlane = new ClientActionSendPaperPlane();
        ClientActionSendPaperPlane.ClientActionSendPaperPlaneTo planeTo = new ClientActionSendPaperPlane.ClientActionSendPaperPlaneTo();
        planeTo.unseat_users = needSendUnSeat;
        ArrayList<ClientActionSendPaperPlane.ClientActionSendPaperItem> tolist = new ArrayList<>();
        planeTo.users = tolist;
        ToolKit.travelCollection(to, value -> {
            ClientActionSendPaperPlane.ClientActionSendPaperItem item = new ClientActionSendPaperPlane.ClientActionSendPaperItem();
            item.id = value.getMemberId();
            item.position = value.getMemberPosition();
            tolist.add(item);
        });

        ClientActionSendPaperPlane.ClientActionSendPaperItem planeFrom = new ClientActionSendPaperPlane.ClientActionSendPaperItem();
        planeFrom.id = from.getMemberId();
        planeFrom.position = from.getMemberPosition();

        actionSendPaperPlane.to = planeTo;
        actionSendPaperPlane.from = planeFrom;
        actionSendPaperPlane.message = message;

        INSTANCE.nativeRequestMessage(Constant.TYPE_CHAT, actionSendPaperPlane.toJson());

        return true;
    }

    public static void sendUpdateTheme(String title, String description, String color, String tag, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
            jsonObject.put("description", description);
            jsonObject.put("tag_color", color);
            jsonObject.put("tag", tag);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage("update_theme", jsonObject);
    }

    public static void sendEnableUploadImage(boolean enable) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("enable", enable);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage("enable_share_image", jsonObject);
    }

    public static void sendFight() {
        INSTANCE.nativeRequestMessage(WereWolfConstant.FIGHT, new JSONObject());
    }

    //获取语音房管理员列表
    public static void getVoiceAdminList() {
        JSONObject jsonObject = new JSONObject();
        INSTANCE.nativeRequestMessage(VoiceConstants.VOICE_ADMIN_LIST, jsonObject);
    }

    //添加房间管理员
    public static void addVoiceAdmin(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.UID, userId);
            jsonObject.put(VoiceConstants.ADD_ADMIN, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        INSTANCE.nativeRequestMessage(VoiceConstants.KICK_OUT, jsonObject);
    }

    //解除房间管理员
    public static void removeVoiceAdmin(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(VoiceConstants.USER_ID_CAMEL, userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        INSTANCE.nativeRequestMessage(VoiceConstants.UN_ADMIN, jsonObject);
    }

}
