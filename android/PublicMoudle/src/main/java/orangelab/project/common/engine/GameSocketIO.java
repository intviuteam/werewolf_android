package orangelab.project.common.engine;


import cn.intviu.connector.BaseSocketIO;

/**
 * Author:1727
 * 2017/3/2
 */

public class GameSocketIO extends BaseSocketIO {

    public static final String GAME_SOCKET_WEREWOLF_PATH = "/werewolf";
    public static final String GAME_SOCKET_GLOBAL_PATH = "/connector";

    public GameSocketIO(String address) {
        super(address, GAME_SOCKET_WEREWOLF_PATH);
    }

    public GameSocketIO(String address, String path) {
        super(address, path, "");
    }

    public GameSocketIO(String address, String path, String query) {
        super(address, path, query);
    }

    @Override
    public void sendMessage(String message) {
        super.sendMessage("2" + message);
    }
}
