package orangelab.project.common.effect.frames;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import com.androidtoolkit.PicassoUtils;

import java.io.File;
import java.io.IOException;

import orangelab.project.common.effect.frames.model.Frame;

/**
 * game_werewolf
 * 2018/6/23 下午2:27
 * Mystery
 */
public class PngFramesAnim extends BitmapFramesAnim<Frame> {

    private static final String TAG = "PngFramesAnim";

    public PngFramesAnim(ImageView imageView) {
        super(imageView);
    }

    @Override
    protected Bitmap beforeFrameToBitmap(Frame frame) {
//        LruCache mLruCache = MainApplication.getInstance().getHeadDecorateLruCache();
//        if (mLruCache != null) {
//            return mLruCache.get(frame.getFrameKey());
//        }
        return null;
    }

    @Override
    protected Bitmap onFrameToBitmap(Context context, Frame frame) {
        /**
         * 根据帧的数据判断
         */
        if (frame.frameResource != -1) {
            /**
             * 说明使用的是本地资源
             */
            try {
                return PicassoUtils.getImageBitmap(context, frame.frameResource);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        if (!TextUtils.isEmpty(frame.frameUrl)) {
            /**
             * 说明使用的是远程资源
             */
            try {
                return PicassoUtils.getImageBitmap(context, frame.frameUrl);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        if (!TextUtils.isEmpty(frame.frameLocalPath)) {
            try {
                return PicassoUtils.getImageBitmap(context, new File(frame.frameLocalPath));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    @Override
    protected Bitmap afterFrameToBitmap(Frame frame, Bitmap bitmap) {
//        LruCache mLruCache = MainApplication.getInstance().getHeadDecorateLruCache();
//        if (mLruCache != null) {
//            mLruCache.set(frame.getFrameKey(), bitmap);
//            PLog.i(TAG, frame + " set into cache");
//        }
        return bitmap;
    }
}
