package orangelab.project.common;

/**
 * game_werewolf
 * 2018/5/23 下午4:01
 * Mystery
 */
public interface AppLifeCycle {
    /**
     * 应用进入到了前台
     */
    void appOnTop();

    /**
     * 应用进入到了后台
     */
    void appInBackGround();
}
