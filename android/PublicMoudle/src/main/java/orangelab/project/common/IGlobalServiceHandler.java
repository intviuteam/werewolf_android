package orangelab.project.common;

import com.toolkit.action.Destroyable;

/**
 * game_werewolf
 * 2018/1/30 下午2:30
 * Mystery
 */

public interface IGlobalServiceHandler extends Destroyable {

    boolean isCareType(String type);

    void handleMessage(String type, String payload);
}
