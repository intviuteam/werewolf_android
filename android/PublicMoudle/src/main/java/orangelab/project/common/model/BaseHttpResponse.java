package orangelab.project.common.model;

import org.json.JSONObject;

/**
 * Author:1727
 * 2017/3/31
 */

public class BaseHttpResponse {
    public int code;
    public String message;
    public JSONObject data;
}
