package orangelab.project.common.exhibition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.androidtoolkit.ToastToolKit;
import com.R;
import orangelab.project.common.activity.SafeActivity;
import orangelab.project.common.effect.EffectsManager;
import orangelab.project.common.manager.ApiManager;
import orangelab.project.common.model.GiftHistoryResult;
import orangelab.project.common.tool.WrapContentLinearLayoutManager;
import orangelab.project.common.utils.Utils;
import orangelab.project.common.view.TitleView;
import com.networktoolkit.transport.Constant;

import java.util.ArrayList;
import java.util.List;

import cn.intviu.support.NetworkHelpers;


/**
 * game_werewolf
 * 2017/7/3 下午2:55
 * Mystery
 */

public class ExhibitionsRecordActivity extends SafeActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "ExhibitionsRecordActivi";

    private static final int TIME_OUT = 10000;

    private int curPage = 0;
    private int lastPage = 0;
    private TitleView mTitleView;
    private String mTargetId;

    private SwipeRefreshLayout mRefreshView;
    private RecyclerView mRecord;
    private WrapContentLinearLayoutManager mLayoutManager;
    private ExhibitionsPresentRecordAdapter mAdapter;

    private boolean mTopIsRefreshing = false;
    private boolean noData = false;

    private List<ExhibitionsPresentRecordItem> mRecordItems = new ArrayList<>();
    private int lastVisibleItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitions_records);
        initView();
        initTitle();
        initData();
        curPage = 0;
        getPageData();
    }

    private void getPageData() {
        if (!noData) {
            if (NetworkHelpers.isNetworkAvailable(this)) {
                ApiManager.GiftHistory(mTargetId, curPage, true, (giftHistoryResult, e) -> {
                    runOnUiThreadSafely(() -> {
                        if (e == null) {
                            onSuccess(giftHistoryResult);
                        } else {
                            ToastToolKit.showShort(e.getMessage());
                        }
                    });
                });
            } else {
                Toast.makeText(this, R.string.network_is_unavailable, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView() {
        mTitleView = (TitleView) findViewById(R.id.exhibitions_title);
        mRefreshView = (SwipeRefreshLayout) findViewById(R.id.exhibitions_record_fresh);
        mRecord = (RecyclerView) findViewById(R.id.exhibitions_record);
        mLayoutManager = new WrapContentLinearLayoutManager(this);
        mRecord.setLayoutManager(mLayoutManager);
        mRecord.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (mTopIsRefreshing)
                    return;
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItem + 1 == mAdapter.getItemCount()) {
                    curPage++;
                    getPageData();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mTopIsRefreshing)
                    return;
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
            }
        });
        mAdapter = new ExhibitionsPresentRecordAdapter(this);
        mAdapter.updateRecord(mRecordItems);
        mRecord.setAdapter(mAdapter);
    }

    private void initData() {
        mTargetId = getIntent().getStringExtra(Constant.USER_ID);
        mRefreshView.setOnRefreshListener(this);
    }

    private void initTitle() {
        Utils.setWindowStatusBarColor(this, R.color.personal_info_back_color);
        mTitleView.setTitle(R.string.gifts_exhibitions_title_string);
        mTitleView.setActionBack(v -> finish());
    }

    @Override
    protected void onActivityWindowInitFinish() {

    }


    public static void enterExhibitionsRecord(Context context, String userId) {
        Intent intent = new Intent(context, ExhibitionsRecordActivity.class);
        intent.putExtra(Constant.USER_ID, userId);
        context.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRecordItems.clear();
        System.gc();
    }

    @Override
    public void onRefresh() {
        Log.i(TAG, "onRefresh: ");
        /**
         * 请求数据
         *
         * 1.将监听者标记为true
         * 2.执行超时任务
         */
        mTopIsRefreshing = true;
        noData = false;
        curPage = 0;
        getPageData();
//
//        runOnUiThreadSafely(() -> {
//            Log.i(TAG, "onRefresh:  time Out check");
//            if (mTopIsRefreshing && mRequestTask != null && mRequestTask.isExecuting()) {
//                /**
//                 * 如果此时还是在刷新状态，则执行超时
//                 */
//                mRequestTask.cancel();
//                Log.i(TAG, "onRefresh: time Out cancel The Task");
//                mTopIsRefreshing = false;
//                mRefreshView.setRefreshing(mTopIsRefreshing);
//            }
//        }, TIME_OUT);
    }

    public void onSuccess(GiftHistoryResult result) {
        runOnUiThreadSafely(() -> {
            try {
                if (result != null && result.histories != null && result.histories.size() > 0 && result.users != null) {
                    if (curPage == 0) {
                        mRecordItems.clear();
                    }
                    for (GiftHistoryResult.GiftHistoryItem temp : result.histories) {
                        if (EffectsManager.SupportThisGiftType(temp.type)) {
                            ExhibitionsPresentRecordItem item = null;
                            GiftHistoryResult.GiftHistoryUserDataItem userItem = result.users.get(temp.from);
                            if (userItem != null) {
                                item = new ExhibitionsPresentRecordItem(
                                        userItem.name,
                                        userItem.image,
                                        temp.type,
                                        temp.time
                                );
                                item.setFromId(userItem.id);
                                mRecordItems.add(item);
                            }
                        }
                    }
                    mAdapter.updateRecord(mRecordItems);
                    Log.i(TAG, "onSuccess: updateRecord Finish");
                    lastPage = curPage;
                    if (mTopIsRefreshing) {
                        mTopIsRefreshing = false;
                        mRefreshView.setRefreshing(false);
                        Log.i(TAG, "onSuccess: updateFresh false");
                    }
                } else {
                    //Toast.makeText(this, "没有数据了", Toast.LENGTH_SHORT).show();
                    noData = true;
                }
            } catch (Exception e) {
                /**
                 * 不再请求
                 */
                e.printStackTrace();
                return;
            }
        });
    }

    public void onFailed(int code, String failed) {
        Toast.makeText(this, failed, Toast.LENGTH_SHORT).show();
    }

    public void onError(Exception e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
