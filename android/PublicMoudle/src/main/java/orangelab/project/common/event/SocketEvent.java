package orangelab.project.common.event;

import com.toolkit.action.Action;

/**
 * Author:1727
 * 2017/3/27
 */

public class SocketEvent {

    public static final int SOCKET_DISCONNECT = 1;
    public static final int SOCKET_CONNECT = 2;

    public static class SocketConnectStateEvent {
        private String intent;
        private int action;

        public SocketConnectStateEvent(String intent, int action) {
            this.intent = intent;
            this.action = action;
        }

        public String getIntent() {
            return intent;
        }

        public int getAction() {
            return action;
        }
    }

    public static class ReConnectedFailedEvent {
        private String intent;
        private int code = 0;//用来标记是超时断开还是被踢出断开

        public ReConnectedFailedEvent(String intent, int code) {
            this.intent = intent;
            this.code = code;
        }

        public String getIntent() {
            return intent;
        }

        public int getCode() {
            return code;
        }
    }

    public static class BootFinishEvent {

        private String intentType;

        public BootFinishEvent(String intentType) {
            this.intentType = intentType;
        }

        public String getIntentType() {
            return intentType;
        }

        public void setIntentType(String intentType) {
            this.intentType = intentType;
        }
    }

    public static class SocketSendGiftEvent {
        private Action<String> successCallBack;
        private Action<String> failedCallBack;
        private String peer_id;
        private String gift_type;
        private String displayType;

        public SocketSendGiftEvent(String peer_id, String gift_type, Action<String> successCallBack, Action<String> failedCallBack) {
            this.successCallBack = successCallBack;
            this.failedCallBack = failedCallBack;
            this.peer_id = peer_id;
            this.gift_type = gift_type;
        }

        public SocketSendGiftEvent(String peer_id, String gift_type, String displayType, Action<String> successCallBack, Action<String> failedCallBack) {
            this.successCallBack = successCallBack;
            this.failedCallBack = failedCallBack;
            this.peer_id = peer_id;
            this.gift_type = gift_type;
            this.displayType = displayType;
        }

        public Action<String> getSuccessCallBack() {
            return successCallBack;
        }

        public Action<String> getFailedCallBack() {
            return failedCallBack;
        }

        public String getPeer_id() {
            return peer_id;
        }

        public String getGift_type() {
            return gift_type;
        }

        public String getDisplayType() {
            return displayType;
        }
    }

    public static class SocketSendCardEvent {
        private Action<String> successCallBack;
        private Action<String> failedCallBack;
        private String peer_id;
        private String gift_type;
        private String displayType;

        public SocketSendCardEvent(String peer_id, String gift_type, Action<String> successCallBack, Action<String> failedCallBack) {
            this.successCallBack = successCallBack;
            this.failedCallBack = failedCallBack;
            this.peer_id = peer_id;
            this.gift_type = gift_type;
        }

        public SocketSendCardEvent(String peer_id, String gift_type, String displayType, Action<String> successCallBack, Action<String> failedCallBack) {
            this.successCallBack = successCallBack;
            this.failedCallBack = failedCallBack;
            this.peer_id = peer_id;
            this.gift_type = gift_type;
            this.displayType = displayType;
        }

        public Action<String> getSuccessCallBack() {
            return successCallBack;
        }

        public Action<String> getFailedCallBack() {
            return failedCallBack;
        }

        public String getPeer_id() {
            return peer_id;
        }

        public String getGift_type() {
            return gift_type;
        }

        public String getDisplayType() {
            return displayType;
        }
    }


}
