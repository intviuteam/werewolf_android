package orangelab.project.common.effect.frames.model;

import android.text.TextUtils;

/**
 * game_werewolf
 * 2018/6/23 下午2:26
 * Mystery
 */
public class Frame {
    public String frameUrl;
    public String frameLocalPath;
    public int frameResource = -1;


    public String getFrameKey() {
        if (frameResource != -1) {
            return "Frame" + Integer.toString(frameResource);
        }
        if (!TextUtils.isEmpty(frameUrl)) {
            return "Frame" + frameUrl;
        }
        if (!TextUtils.isEmpty(frameLocalPath)) {
            return "Frame" + frameLocalPath;
        }
        return "";
    }

    @Override
    public String toString() {
        return getFrameKey();
    }
}