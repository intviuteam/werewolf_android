package orangelab.project.common.context

import orangelab.project.common.model.action.*

/**
 * game_werewolf
 * 2018/3/19 上午11:47
 * Mystery
 */

interface ServerMessageProcess {

    fun handleServerSocketDisconnect()

    fun handleServerSocketConnect()

    fun handleServerSocketReconnectFailed(reason: String)

    fun handleServerActionAcceptFreeMode(serverActionAcceptFreeMode: ServerActionAcceptFreeMode)

    fun handleServerActionChangeUserState(serverActionChangeUserState: ServerActionChangeUserState)

    fun handleServerActionChat(serverActionChat: ServerActionChat)

    fun handleServerActionDownSeat(serverActionDownSeat: ServerActionDownSeat)

    fun handleServerActionForceSeat(serverActionForceSeat: ServerActionForceSeat)

    fun handleServerActionHandOverMaster(serverActionHandOverMaster: ServerActionHandOverMaster)

    fun handleServerActionJoin(serverActionJoin: ServerActionJoin)

    fun handleServerActionKickOut(serverActionKickOut: ServerActionKickOut)

    fun handleServerActionLeave(serverActionLeave: ServerActionLeave)

    fun handleServerActionLikeRoom(serverActionLikeRoom: ServerActionLikeRoom)

    fun handleServerActionLock(serverActionLock: ServerActionLock)

    fun handleServerActionRejectFreeMode(serverActionRejectFreeMode: ServerActionRejectFreeMode)

    fun handleServerActionRequestFreeMode(serverActionRequestFreeMode: ServerActionRequestFreeMode)

    fun handleServerActionRestoreRoom(serverActionRestoreRoom: ServerActionRestoreRoom)

    fun handleServerActionShowEmoticon(serverActionShowEmoticon: ServerActionShowEmoticon)

    fun handleServerActionSpeak(serverActionSpeak: ServerActionSpeak)

    fun handleServerActionSystemMsg(serverActionSystemMsg: ServerActionSystemMsg)

    fun handleServerActionUnSpeak(serverActionUnSpeak: ServerActionUnSpeak)

    fun handleServerActionUpdateConfig(serverActionUpdateConfig: ServerActionUpdateConfig)

    fun handleServerActionUpdateMaster(serverActionUpdateMaster: ServerActionUpdateMaster)

    fun handleServerActionUpdateTitle(serverActionUpdateTitle: ServerActionUpdateTitle)

    fun handleServerActionUpSeat(serverActionUpSeat: ServerActionUpSeat)

    fun handleServerActionChangePassword(serverActionChangePassword: ServerActionChangePassword)

    fun handleServerActionAddFriend(serverActionAddFriend: ServerActionAddFriend)

    fun handleServerActionShowGameEmoticon(serverActionShowGameEmotion: ServerActionShowGameEmotion)

}
