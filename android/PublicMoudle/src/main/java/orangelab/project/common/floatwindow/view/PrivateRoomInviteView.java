package orangelab.project.common.floatwindow.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.R;
import com.androidtoolkit.AndroidToolKit;
import com.androidtoolkit.PicassoUtils;
import com.toolkit.action.Destroyable;

import de.hdodenhof.circleimageview.CircleImageView;
import orangelab.project.common.utils.MessageUtils;

/**
 * game_werewolf
 * 2018/5/16 下午6:28
 * Mystery
 * 私密房邀请悬浮View
 */
public class PrivateRoomInviteView extends FrameLayout implements View.OnClickListener, Destroyable {

    public interface PrivateRoomInviteViewObserver {

        void onClose();

        void onAccept(String roomId, String roomPassword, String roomType);

        void onRefuse();
    }

    private TextView mTextView;
    private Button mAccept;
    private Button mCancel;
    private CircleImageView mCircleImageView;
    private View mClose;

    private String roomId;
    private String roomPassword;
    private String roomType;
    private PrivateRoomInviteViewObserver mObserver;

    private boolean isShowing = false;

    public PrivateRoomInviteView(@NonNull Context context) {
        this(context, null);
    }

    public PrivateRoomInviteView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PrivateRoomInviteView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setObserver(PrivateRoomInviteViewObserver observer) {
        mObserver = observer;
    }

    public void init() {

        View view = View.inflate(getContext(), R.layout.layout_private_room_intviu_float, null);
        addView(view);

        mTextView = (TextView) view.findViewById(R.id.text);

        mAccept = (Button) view.findViewById(R.id.id_accept);
        mAccept.setOnClickListener(this);
        mCancel = (Button) view.findViewById(R.id.id_refuse);
        mCancel.setOnClickListener(this);

        mCircleImageView = (CircleImageView) view.findViewById(R.id.user_head);

        mClose = view.findViewById(R.id.close);
        mClose.setOnClickListener(this);

    }

    public void setUp(String userIcon, String userName, String roomId, String roomPassword, String roomType) {

        this.roomId = roomId;
        this.roomPassword = roomPassword;
        this.roomType = roomType;

        PicassoUtils.loadImage(getContext(), userIcon, mCircleImageView, R.mipmap.default_head);

        String showName = AndroidToolKit.getNameForShow(userName, 4);

        mTextView.setText(MessageUtils.getString(R.string.str_private_room_float_window_intviu, showName, roomId, roomPassword));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.id_accept) {
            /**
             * 接收了请求
             */
            if (mObserver != null) {
                mObserver.onAccept(roomId, roomPassword, roomType);
            }
        } else if (v.getId() == R.id.id_refuse) {
            /**
             * 拒绝了请求
             */
            if (mObserver != null) {
                mObserver.onRefuse();
            }
        } else if (v.getId() == R.id.close) {
            /**
             * 点击了X
             */
            if (mObserver != null) {
                mObserver.onClose();
            }
        }
    }

    public boolean isShowing() {
        return isShowing;
    }

    public void setShowing(boolean showing) {
        isShowing = showing;
    }

    @Override
    public void destroy() {
        mObserver = null;
    }
}
