package orangelab.project.common.effect;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.PointF;
import android.text.TextUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import orangelab.project.common.effect.gifts.GiftAnimConstant;
import orangelab.project.common.effect.gifts.CustomGiftAnim;
import orangelab.project.common.effect.gifts.CustomGiftAnimPayload;

import com.androidtoolkit.view.ScreenUtils;

/**
 * game_werewolf
 * 2017/11/7 上午11:51
 * Mystery
 */

public class EffectsFactory {

    public static Interpolator CreateInterpolator(String wanted) {
        if (TextUtils.isEmpty(wanted)) {
            return new LinearInterpolator();
        }
        switch (wanted) {
            case GiftAnimConstant.INTERPOLATOR_LINEAR:
                return new LinearInterpolator();
            case GiftAnimConstant.INTERPOLATOR_ACCELERATE:
                return new AccelerateInterpolator();
            case GiftAnimConstant.INTERPOLATOR_ACCELERATEDECLERATE:
                return new AccelerateDecelerateInterpolator();
            case GiftAnimConstant.INTERPOLATOR_DECLERATEACCELERATE:
                /**
                 * 先临时拼凑
                 */
                return new AccelerateDecelerateInterpolator();
            default:
                return new LinearInterpolator();
        }
    }

    public static void wrapPointOffSet(PointF point, int offSetX, int offSetY) {
        float x = point.x;
        float y = point.y;

        if (offSetX != 0) {
            if (offSetX > 0) {
                x = x + ScreenUtils.dip2px(offSetX);
            } else {
                x = x - ScreenUtils.dip2px(Math.abs(offSetX));
            }
        }

        if (offSetY != 0) {
            if (offSetY > 0) {
                y = y + ScreenUtils.dip2px(offSetY);
            } else {
                y = y - ScreenUtils.dip2px(Math.abs(offSetY));
            }
        }
        point.set(x, y);
    }

    public static PointF GetPointByGiftAnimPoint(
            String fromType,
            PointF defaultStart,
            PointF defaultEnd,
            CustomGiftAnim giftAnim,
            CustomGiftAnimPayload payload,
            CustomGiftAnim.CustomGiftAnimPoint giftAnimPoint) {
        final int startOrientation = payload.startOrientation;
        final int endOrientation = payload.endOrientation;

        wrapPointOffSet(defaultStart, giftAnimPoint.startOffSetX, giftAnimPoint.startOffSetY);
        wrapPointOffSet(defaultEnd, giftAnimPoint.endOffSetX, giftAnimPoint.endOffSetY);

        final float startX = defaultStart.x;
        final float startY = defaultStart.y;
        final float endX = defaultEnd.x;
        final float endY = defaultEnd.y;

        float defaultWidth = giftAnim.DEFAULT_WIDTH;
        if (payload.viewWidth == -1) {
            defaultWidth = giftAnim.DEFAULT_WIDTH;
        } else {
            defaultWidth = payload.viewWidth;
        }
        float defaultHeight = giftAnim.DEFAULT_HEIGHT;
        if (payload.viewHeight == -1) {
            defaultHeight = giftAnim.DEFAULT_HEIGHT;
        } else {
            defaultHeight = payload.viewHeight;
        }
        final float screenWidth = EffectsTaskFactory.INSTANCE.getScreenWidth();
        final float screenHeight = EffectsTaskFactory.INSTANCE.getScreenHeight();

        try {
            if (!TextUtils.isEmpty(
                    giftAnimPoint.location)
                    && TextUtils.equals(fromType, GiftAnimConstant.GIFT_FROM_TYPE_VOICE)
                    && (TextUtils.equals(giftAnimPoint.location, GiftAnimConstant.LOCATION_TOFRONT) || TextUtils.equals(giftAnimPoint.location, GiftAnimConstant.LOCATION_FROMFRONT))) {
                /**
                 * 语音房这种特效需要强制更换
                 */
                if (TextUtils.equals(giftAnimPoint.location, GiftAnimConstant.LOCATION_TOFRONT)) {
                    giftAnimPoint.location = GiftAnimConstant.LOCATION_TOABOVE;
                }
                if (TextUtils.equals(giftAnimPoint.location, GiftAnimConstant.LOCATION_FROMFRONT)) {
                    giftAnimPoint.location = GiftAnimConstant.LOCATION_FROMABOVE;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (giftAnimPoint.location) {
            case GiftAnimConstant.LOCATION_FROMABOVE: {
                return defaultStart;
            }
            case GiftAnimConstant.LOCATION_FROMFRONT: {
                float newWidth = defaultWidth;
                float newHeight = defaultHeight;
                if (startOrientation == GiftAnimConstant.MEMBER_ORIENTATION_LEFT) {
                    float newX = startX + defaultWidth + GiftAnimConstant.DEFAULT_GAUGE_SMALL;
                    float newY = startY;
                    return new PointF(newX, newY);
                } else {
                    float newX = startX - (newWidth + GiftAnimConstant.DEFAULT_GAUGE_SMALL);
                    float newY = startY;
                    return new PointF(newX, newY);
                }
            }
            case GiftAnimConstant.LOCATION_TOABOVE: {
                return defaultEnd;
            }
            case GiftAnimConstant.LOCATION_TOFRONT: {
                float newWidth = defaultWidth;
                float newHeight = defaultHeight;
                if (endOrientation == GiftAnimConstant.MEMBER_ORIENTATION_LEFT) {
                    float newX = endX + newWidth + GiftAnimConstant.DEFAULT_GAUGE_SMALL;
                    float newY = endY;
                    return new PointF(newX, newY);
                } else {
                    float newX = endX - (newWidth) - GiftAnimConstant.DEFAULT_GAUGE_SMALL;
                    float newY = endY;
                    return new PointF(newX, newY);
                }
            }
            case GiftAnimConstant.LOCATION_SCREENBOTTOMLEFT: {
                /**
                 * 屏幕底部左边,这里默认了所有的屏幕宽度超过300dp
                 */
                float newX = screenWidth / 4 - defaultWidth / 2;
                float newY = screenHeight + defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENBOTTOMMID: {
                float newX = screenWidth / 2 - defaultWidth / 2;
                float newY = screenHeight + defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENBOTTOMRIGHT: {
                float newX = (screenWidth / 4) * 3 - defaultWidth / 2;
                float newY = screenHeight + defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENLEFTTOP: {
                float newX = -defaultWidth;
                float newY = screenHeight / 4 - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENLEFTBOTTOM: {
                float newX = -defaultWidth;
                float newY = (screenHeight / 4) * 3 - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENLEFTMID: {
                float newX = -defaultWidth;
                float newY = (screenHeight / 2) - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENRIGHTMID: {
                float newX = screenWidth;
                float newY = (screenHeight / 2) - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENRIGHTTOP: {
                float newX = screenWidth;
                float newY = (screenHeight / 4) - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENRIGHTBOTTOM: {
                float newX = screenWidth;
                float newY = (screenHeight / 4) * 3 - defaultHeight / 2;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENTOPLEFT: {
                float newX = screenWidth / 4 - defaultWidth / 2;
                float newY = -defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENTOPMID: {
                float newX = screenWidth / 2 - defaultWidth / 2;
                float newY = -defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENTOPRIGHT: {
                float newX = (screenWidth / 4) * 3 - defaultWidth / 2;
                float newY = -defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENLEFTBOTTOMCORNER: {
                float newX = -defaultWidth;
                float newY = screenHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENLEFTTOPCORNER: {
                float newX = -defaultWidth;
                float newY = -defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENRIGHTBOTTOMCORNER: {
                float newX = screenWidth + defaultWidth;
                float newY = screenHeight + defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENRIGHTTOPCORNER: {
                float newX = screenWidth + defaultWidth;
                float newY = -defaultHeight;
                return new PointF(newX, newY);
            }
            case GiftAnimConstant.LOCATION_SCREENMID: {
                float newWidth = defaultWidth;
                float newHeight = defaultHeight;
                float centerX = EffectsTaskFactory.INSTANCE.getCenterPoint().x;
                float centerY = EffectsTaskFactory.INSTANCE.getCenterPoint().y;
                float newX = centerX - newWidth / 2;
                float newY = 0;
                if (TextUtils.equals(fromType, GiftAnimConstant.GIFT_FROM_TYPE_VOICE)) {
                    newY = centerY - newHeight / 2;
                } else {
                    newY = centerY - newHeight / 2 - ScreenUtils.dip2px(50);
                }
                return new PointF(newX, newY);
            }
            default:
                return null;

        }
    }


    public static PointF GetPointByGiftAnimPoint(
            PointF defaultStart,
            PointF defaultEnd,
            CustomGiftAnim giftAnim,
            CustomGiftAnimPayload payload,
            CustomGiftAnim.CustomGiftAnimPoint giftAnimPoint) throws Exception {
        /**
         * 默认为游戏房间的
         */
        return GetPointByGiftAnimPoint(GiftAnimConstant.GIFT_FROM_TYPE_GAME, defaultStart, defaultEnd, giftAnim, payload, giftAnimPoint);
    }


    public static class AnimatorAdapterWithFasterAnim extends AnimatorListenerAdapter {

        private IFasterAnim mFasterAnim;

        public AnimatorAdapterWithFasterAnim(IFasterAnim fasterAnim) {
            mFasterAnim = fasterAnim;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationCancel(Animator animation) {
            if (mFasterAnim != null) {
                mFasterAnim.stop();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationEnd(Animator animation) {
            if (mFasterAnim != null) {
                mFasterAnim.stop();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationRepeat(Animator animation) {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationStart(Animator animation) {
            if (mFasterAnim != null) {
                mFasterAnim.start();
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationPause(Animator animation) {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void onAnimationResume(Animator animation) {
        }


    }

}
