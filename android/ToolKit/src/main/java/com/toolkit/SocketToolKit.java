package com.toolkit;

import android.util.Log;

import java.io.IOException;
import java.net.Socket;
import java.util.Random;

/**
 * game_werewolf
 * 2018/4/19 上午11:20
 * Mystery
 */

public class SocketToolKit {

    private static final String TAG = "SocketToolKit";

    public static boolean portIsAvailable(int port) {
        Socket s = null;
        try {
            s = new Socket("localhost", port);
            // If the code makes it this far without an exception it means
            // something is using the port and has responded.
            Log.i(TAG, "portIsAvailable: " + port + " is not available");
            return false;
        } catch (IOException e) {
//            e.printStackTrace();
            Log.i(TAG, "portIsAvailable: " + port + " is available");
            return true;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (IOException e) {
                    throw new RuntimeException("You should handle this error.", e);
                }
            }
        }
    }

    public static int getAvailablePointRandom(int begin, int dur) {
        Random random = new Random();
        int point;
        do {
            point = random.nextInt(dur) + begin;
        } while (!portIsAvailable(point));
        return point;
    }
}
