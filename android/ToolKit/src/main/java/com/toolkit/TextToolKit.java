package com.toolkit;

import android.text.TextUtils;

/**
 * game_werewolf
 * 2018/4/11 下午12:02
 * Mystery
 */

public class TextToolKit {

    public static boolean isChinese(String input) {
        if (TextUtils.isEmpty(input)) return false;
        int n = 0;
        for (int i = 0; i < input.length(); i++) {
            n = (int) input.charAt(i);
            if (!(19968 <= n && n < 40869)) {
                return false;
            }
        }
        return true;
    }

}
