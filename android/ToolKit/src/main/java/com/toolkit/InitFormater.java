package com.toolkit;

/**
 * Created by lloydfinch on 24/11/2017.
 * <p>
 * View相关初始化格式化器
 */

public interface InitFormater {

    void init();

    void initView();

    void initWindow();

    void initListener();

    void initEvent();

    void initData();
}
