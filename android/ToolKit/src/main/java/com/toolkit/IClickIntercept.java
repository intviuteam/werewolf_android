package com.toolkit;

import android.view.View;

/**
 * MyApplication
 * 2017/11/3 下午6:08
 * Mystery
 */

public interface IClickIntercept {
    boolean isIntercept(View view);
}