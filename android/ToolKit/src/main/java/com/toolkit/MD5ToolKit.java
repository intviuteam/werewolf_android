package com.toolkit;

import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * game_werewolf
 * 2018/4/3 下午5:18
 * Mystery
 */
public class MD5ToolKit {

    public static String getFileMD5(File file) {
        if (!file.isFile()) {
            return "";
        }

        MessageDigest digest = null;
        FileInputStream in = null;
        byte buffer[] = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }

    public static String getStringMD5(String url) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
            byte[] bytes = url.getBytes();
            digest.update(bytes, 0, bytes.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }

}