package com.toolkit;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * game_werewolf
 * 2018/4/3 下午1:57
 * Mystery
 */

public class ZipFileToolKit {

    public static void unPackZip(String path, String unzipPath) throws Exception {
        InputStream is = null;
        ZipInputStream zis = null;
        try {
            String filename;

            is = new FileInputStream(path);
            zis = new ZipInputStream(new BufferedInputStream(is));

            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                // zapis do souboru
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(unzipPath + filename);
                    fmd.mkdirs();
                    continue;
                }

                File file = new File(unzipPath + filename);
                if (!file.exists()) {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }

                FileOutputStream fout = new FileOutputStream(file);
                try {
                    // cteni zipu a zapis
                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                } finally {
                    ToolKit.Close(fout);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (zis != null) {
                zis.closeEntry();
            }
            ToolKit.Close(zis);
        }
    }

    public static void UnZip(String zipFile, String targetDir) throws Exception {

        int BUFFER = 4096; //这里缓冲区我们使用4KB，
        String strEntry; //保存每个zip的条目名称

        ZipInputStream zis = null;
        try {
            BufferedOutputStream dest = null; //缓冲输出流
            FileInputStream fis = new FileInputStream(zipFile);
            zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry; //每个zip条目的实例

            while ((entry = zis.getNextEntry()) != null) {
                try {
                    int count;
                    byte data[] = new byte[BUFFER];
                    strEntry = entry.getName();

                    File entryFile = new File(targetDir + strEntry);

                    if (entryFile.getName().startsWith(".")) {
                        /**
                         * 忽略掉已 「 . 」开头的文件
                         */
                        continue;
                    }

                    if (entryFile.getName().contains("MACOSX")) {
                        /**
                         * 忽略掉Mac文件
                         */
                        continue;
                    }

                    File entryDir = new File(entryFile.getParent());

                    if (!entryDir.exists()) {
                        entryDir.mkdirs();
                    }

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(entryFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        /**
                         * 出错则直接跳过
                         */
                        Log.i("MiniGameManager", " error file " + entryFile.getName());
                        continue;
                    }
                    dest = new BufferedOutputStream(fos, BUFFER);
                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }
                    dest.flush();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw ex;
                } finally {
                    ToolKit.Close(dest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            ToolKit.Close(zis);
        }
    }
}
