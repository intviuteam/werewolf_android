package com.toolkit.action;

/**
 * game_werewolf
 * 2018/2/7 上午10:37
 * Mystery
 */

public interface ActionFilter<T> {

    boolean filter(T value);
}
