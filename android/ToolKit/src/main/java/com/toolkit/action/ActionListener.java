package com.toolkit.action;

/**
 * game_werewolf
 * 2017/11/25 下午1:26
 * Mystery
 */

public interface ActionListener {

    void onStart();

    void onComplete();

    void onError(Throwable e);
}
