package com.toolkit.action;

import java.util.Iterator;

/**
 * game_werewolf
 * 2018/4/27 上午10:21
 * Mystery
 */

public interface TravelCollectionByIterator<T> {
    void onTravel(Iterator<T> iterator, T data);
}
