package com.toolkit.action;

/**
 * game_werewolf
 * 2017/8/26 下午6:06
 * Mystery
 */

public interface Action<T> {

    void func(T value);
}
