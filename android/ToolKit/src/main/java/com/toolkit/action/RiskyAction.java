package com.toolkit.action;

/**
 * game_werewolf
 * 2018/4/3 下午5:11
 * Mystery
 */

public interface RiskyAction<T> {

    void func(T result, Exception e);
}
