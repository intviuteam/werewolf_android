package com.toolkit.action;

/**
 * game_werewolf
 * 2017/10/20 上午11:33
 * Mystery
 */

public interface ResultAction<T> {

    void func(int code, T result);
}
