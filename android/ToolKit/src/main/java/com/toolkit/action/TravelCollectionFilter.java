package com.toolkit.action;

/**
 * game_werewolf
 * 2017/9/23 上午10:38
 * Mystery
 */

public interface TravelCollectionFilter<T> {

    boolean filter(T value);
}
