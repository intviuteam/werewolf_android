package com.toolkit.action;

/**
 * game_werewolf
 * 2017/11/16 下午3:31
 * Mystery
 */

public interface TravelMapWithResult<K, V> {
    boolean onTravel(K key, V value);
}
