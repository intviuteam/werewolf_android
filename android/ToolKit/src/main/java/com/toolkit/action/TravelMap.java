package com.toolkit.action;

/**
 * game_werewolf
 * 2017/9/15 下午1:34
 * Mystery
 */

public interface TravelMap<K, V> {

    void onTravel(K key, V value);
}
