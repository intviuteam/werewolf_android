package com.toolkit.action;

import org.json.JSONObject;

/**
 * game_werewolf
 * 2018/4/10 上午11:16
 * Mystery
 */

public interface ActionToJson {
    JSONObject toJson();
}
