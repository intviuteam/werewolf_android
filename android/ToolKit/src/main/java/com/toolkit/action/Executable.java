package com.toolkit.action;

/**
 * game_werewolf
 * 2018/3/17 下午2:49
 * Mystery
 */

public interface Executable {

    void execute();

}
