package com.toolkit.action;

/**
 * game_werewolf
 * 2017/8/10 上午10:26
 * Mystery
 */

public interface Destroyable {

    public void destroy();
}
