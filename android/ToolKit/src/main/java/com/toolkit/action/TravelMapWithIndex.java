package com.toolkit.action;

/**
 * game_werewolf
 * 2018/6/4 上午11:55
 * Mystery
 */
public interface TravelMapWithIndex<K, V> {

    void onTravel(int index, K key, V value);
}
