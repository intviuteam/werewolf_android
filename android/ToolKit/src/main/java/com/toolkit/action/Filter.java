package com.toolkit.action;

/**
 * game_werewolf
 * 2017/9/13 下午1:23
 * Mystery
 */

public interface Filter {

    boolean filter();
}
