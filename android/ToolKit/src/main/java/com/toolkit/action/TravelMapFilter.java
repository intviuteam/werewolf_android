package com.toolkit.action;

/**
 * game_werewolf
 * 2017/9/23 上午10:36
 * Mystery
 */

public interface TravelMapFilter<K, V> {

    boolean filter(K key, V value);
}
