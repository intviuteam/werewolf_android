package com.toolkit.action;

/**
 * game_werewolf
 * 2017/10/26 下午3:59
 * Mystery
 */

public interface ApiResult<Result> {

    void onResult(Result result, Exception e);
}
