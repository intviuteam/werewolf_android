package com.toolkit.action;

/**
 * game_werewolf
 * 2018/4/16 上午11:13
 * Mystery
 */

public interface Printable<T> {

    void printInfo(T info);
}
