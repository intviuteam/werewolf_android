package com.toolkit.action;

/**
 * game_werewolf
 * 2017/9/23 上午10:30
 * Mystery
 */

public interface TravelCollection<T> {

    void onTravel(T value);
}
