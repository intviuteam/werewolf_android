package com.toolkit.action;

/**
 * game_werewolf
 * 2018/3/16 下午5:51
 * Mystery
 */

public interface Cancelable {

    void cancel();
}
