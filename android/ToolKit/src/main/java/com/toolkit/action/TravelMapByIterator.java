package com.toolkit.action;

import java.util.Iterator;
import java.util.Map;

/**
 * game_werewolf
 * 2018/7/5 下午7:06
 * Mystery
 */
public interface TravelMapByIterator<K, V> {
    void onTravel(Iterator<Map.Entry<K, V>> iterator, K key, V value);
}
