package com.toolkit;

import android.util.Log;

import com.toolkit.action.Cancelable;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2018/3/16 下午5:53
 * Mystery
 */

public class CancelableManager {

    private static final String TAG = "RequestTask";

    private static volatile CancelableManager mInstance;

    private Map<String, Cancelable> mCancelableUniqueMap;

    private CancelableManager() {

    }

    public static CancelableManager getInstance() {
        if (mInstance == null) {
            synchronized (CancelableManager.class) {
                if (mInstance == null) {
                    mInstance = new CancelableManager();
                }
            }
        }
        return mInstance;
    }

    public synchronized void recordUniqueCancelable(String key, Cancelable cancelable) {
        if (mCancelableUniqueMap == null) {
            mCancelableUniqueMap = new HashMap<>();
        }
        if (mCancelableUniqueMap.containsKey(key)) {
            Cancelable temp = mCancelableUniqueMap.remove(key);
            temp.cancel();
            Log.i(TAG, "recordUniqueCancelable: cancel a Cancelable from map");
        }
        mCancelableUniqueMap.put(key, cancelable);
        Log.i(TAG, "recordUniqueCancelable: add a cancel key:" + key);
    }

    public synchronized void removeUniqueCancelable(String key) {
        if (mCancelableUniqueMap == null) {
            return;
        }
        mCancelableUniqueMap.remove(key);
        Log.i(TAG, "removeUniqueCancelable: remove a cancel key:" + key);
    }


}
