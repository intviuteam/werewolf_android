package com.toolkit;

import android.text.TextUtils;
import android.util.Log;

import com.toolkit.action.TravelCollection;
import com.toolkit.action.TravelCollectionByIterator;
import com.toolkit.action.TravelCollectionFilter;
import com.toolkit.action.TravelMap;
import com.toolkit.action.TravelMapByIterator;
import com.toolkit.action.TravelMapFilter;
import com.toolkit.action.TravelMapWithIndex;
import com.toolkit.action.TravelMapWithResult;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class ToolKit {

    /**
     * 遍历 Map
     *
     * @param map
     * @param travelMap
     * @param <K>
     * @param <V>
     */
    public static <K, V> void travelMap(Map<K, V> map, TravelMap<K, V> travelMap) {
        travelMap(map, travelMap, null);
    }

    public static <K, V> void travelMap(Map<K, V> map, TravelMap<K, V> travelMap, TravelMapFilter<K, V> filter) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, V> entry = iterator.next();
            K key = entry.getKey();
            V value = entry.getValue();
            if (filter != null) {
                if (!filter.filter(key, value)) {
                    return;
                }
            }
            if (travelMap != null) {
                travelMap.onTravel(key, value);
            }
        }
    }

    public static <K, V> void travelMap(Map<K, V> map, TravelMapWithIndex<K, V> travelMap) {
        travelMap(map, travelMap, null);
    }

    public static <K, V> void travelMap(Map<K, V> map, TravelMapWithIndex<K, V> travelMap, TravelMapFilter<K, V> filter) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
        int index = -1;
        while (iterator.hasNext()) {
            Map.Entry<K, V> entry = iterator.next();
            K key = entry.getKey();
            V value = entry.getValue();
            index = index + 1;
            if (filter != null) {
                if (!filter.filter(key, value)) {
                    return;
                }
            }
            if (travelMap != null) {
                travelMap.onTravel(index, key, value);
            }
        }
    }

    public static <K, V> void travelMap(Map<K, V> map, TravelMapWithResult<K, V> travelMap) {
        travelMap(map, travelMap, null);
    }

    public static <K, V> void travelMap(Map<K, V> map, TravelMapWithResult<K, V> travelMap, TravelMapFilter<K, V> filter) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, V> entry = iterator.next();
            K key = entry.getKey();
            V value = entry.getValue();
            if (filter != null) {
                if (!filter.filter(key, value)) {
                    return;
                }
            }
            if (travelMap != null) {
                if (travelMap.onTravel(key, value)) {
                    break;
                }
            }
        }
    }

    public static <T> void travelCollection(Collection<T> collection, TravelCollection<T> travelCollection) {
        travelCollection(collection, travelCollection, null);
    }


    /**
     * 遍历 Collection
     *
     * @param collection
     * @param travelCollection
     * @param <T>
     */
    public static <T> void travelCollection(Collection<T> collection, TravelCollection<T> travelCollection, TravelCollectionFilter<T> filter) {
        if (isEmpty(collection)) {
            return;
        }
        for (T temp : collection) {
            if (filter != null) {
                if (!filter.filter(temp)) {
                    return;
                }
            }
            if (travelCollection != null) {
                travelCollection.onTravel(temp);
            }
        }
    }

    public static <K, V> void travelMapByIterator(Map<K, V> map, TravelMapByIterator<K, V> travelMapByIterator) {
        if (isEmpty(map)) {
            return;
        }
        Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<K, V> entry = iterator.next();
            if (travelMapByIterator != null) {
                travelMapByIterator.onTravel(iterator, entry.getKey(), entry.getValue());
            }
        }
    }

    public static <T> void travelCollectionByIterator(Collection<T> collection, TravelCollectionByIterator<T> travelCollection) {
        if (isEmpty(collection)) {
            return;
        }
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (travelCollection != null) {
                travelCollection.onTravel(iterator, next);
            }
        }
    }

    public static boolean isEmpty(Collection collection) {
        if (collection != null && collection.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmpty(Map map) {
        if (map != null && map.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static String Encryption(byte[] input, String type) {
        MessageDigest messageDigest = null;
        StringBuffer md5StrBuff = new StringBuffer();
        try {
            messageDigest = MessageDigest.getInstance(type);
            messageDigest.reset();
            messageDigest.update(input);
            byte[] byteArray = messageDigest.digest();
            for (int i = 0; i < byteArray.length; i++) {
                if (Integer.toHexString(0xFF & byteArray[i]).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
                } else {
                    md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
                }
            }
            return md5StrBuff.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String GetHashKey(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = BytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    public static String BytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static void ShowThreadInfo(String TAG) {
        Log.i(TAG, "Cur Thread info : " + Thread.currentThread().getName());
    }

    public static String GetThreadInfo() {
        return Thread.currentThread().getName();
    }

    public static void Close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double getDistanceBetweenPoint(float startX, float startY, float endX, float endY) {
        double _x = Math.abs(startX - endX);
        double _y = Math.abs(startY - endY);
        return Math.sqrt(_x * _x + _y * _y);
    }

    public static int getRotationBetweenPoint(float centerX, float centerY, float xInView, float yInView) {
        double rotation = 0;

        double k1 = (double) (centerY - centerY) / (centerX * 2 - centerX);
        double k2 = (double) (yInView - centerY) / (xInView - centerX);
        double tmpDegree = Math.atan((Math.abs(k1 - k2)) / (1 + k1 * k2)) / Math.PI * 180;

        if (xInView > centerX && yInView < centerY) {  //第一象限
            rotation = 90 - tmpDegree;
        } else if (xInView > centerX && yInView > centerY) //第二象限
        {
            rotation = 90 + tmpDegree;
        } else if (xInView < centerX && yInView > centerY) { //第三象限
            rotation = 270 - tmpDegree;
        } else if (xInView < centerX && yInView < centerY) { //第四象限
            rotation = 270 + tmpDegree;
        } else if (xInView == centerX && yInView < centerY) {
            rotation = 0;
        } else if (xInView == centerX && yInView > centerY) {
            rotation = 180;
        } else if (yInView == centerY && xInView < centerX) {
            rotation = 270;
        } else if (yInView == centerY && xInView > centerX) {
            rotation = 90;
        }
        return (int) rotation;
    }

    public static String urlEncode(String input) {
        if (TextUtils.isEmpty(input)) {
            return "";
        }
        try {
            return URLEncoder.encode(input, "utf-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String urlEncodeTwice(String input) {
        return urlEncode(urlEncode(input));
    }

    public static String urlDecodeTwice(String input) {
        return urlDecode(urlDecode(input));
    }

    public static String urlDecode(String input) {
        if (TextUtils.isEmpty(input)) {
            return "";
        }
        try {
            return URLDecoder.decode(input, "utf-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }
}
