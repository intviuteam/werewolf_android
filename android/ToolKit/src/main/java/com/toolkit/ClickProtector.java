package com.toolkit;

import android.util.SparseArray;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * game_werewolf
 * 2017/11/3 下午6:18
 * Mystery
 */

public abstract class ClickProtector implements View.OnClickListener, IClickIntercept {

    public static class ClickTimeIntercept implements IClickIntercept {
        private long protectTime;
        private SparseArray<Long> mClickMap;

        public ClickTimeIntercept(long protectTime) {
            this.protectTime = protectTime;
            mClickMap = new SparseArray<>();
        }

        @Override
        public boolean isIntercept(View view) {
            if (view != null) {
                long curClickTime = System.currentTimeMillis();  //当前的点击时间
                int curClickId = view.getId();//当前点击的View ID
                Long curLastClick = mClickMap.get(curClickId);  //这个ViewId 对应的上次点击时间
                if (curLastClick != null) {
                    mClickMap.put(curClickId, curClickTime);
                    if (curClickTime - curLastClick > protectTime) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    mClickMap.put(curClickId, curClickTime);
                    return false;
                }
            }
            return false;
        }
    }

    private List<IClickIntercept> mIntercepts = null;
    private boolean mDisableAllIntercept = false;

    public ClickProtector() {

    }

    public void disableAllIntercept() {
        mDisableAllIntercept = true;
    }

    public void enableAllIntercept() {
        mDisableAllIntercept = false;
    }

    public void addIntercept(IClickIntercept iClickIntercept) {
        if (mIntercepts == null) {
            mIntercepts = new ArrayList<>();
        }
        mIntercepts.add(iClickIntercept);
    }

    public abstract void onClickWithProtector(View v);


    @Override
    public boolean isIntercept(View view) {
        if (!mDisableAllIntercept) {
            if (mIntercepts != null) {
                for (IClickIntercept temp : mIntercepts) {
                    if (temp.isIntercept(view)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (isIntercept(v)) {
            return;
        }
        onClickWithProtector(v);
    }


    public static void testCode() {
        View view = null;

        ClickProtector mViewProtector = new ClickProtector() {
            @Override
            public void onClickWithProtector(View v) {
                /**
                 * 实际的点击任务
                 */
            }
        };
        /**
         * 添加限制时间间隔
         */
        mViewProtector.addIntercept(new ClickTimeIntercept(2000));
        /**
         * 添加限制条件1，例如 网络请求未完成
         */
        mViewProtector.addIntercept(view1 -> false);
        /**
         * 添加限制条件2，例如 异步任务未完成
         */
        mViewProtector.addIntercept(view2 -> false);

        view.setOnClickListener(mViewProtector);

        mViewProtector.disableAllIntercept();//可选择去掉所有限制条件
    }

}
