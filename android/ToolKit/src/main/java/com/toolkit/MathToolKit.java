package com.toolkit;

import java.math.BigDecimal;

/**
 * game_werewolf
 * 2018/6/29 下午5:53
 * Mystery
 */
public class MathToolKit {

    public static String twoDigitsAfterDecimalPoint(long input) {
        try {
            double a = (double) (input / 1000.0);
            BigDecimal bg = new BigDecimal(a).setScale(2, BigDecimal.ROUND_HALF_UP);
            return bg.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
