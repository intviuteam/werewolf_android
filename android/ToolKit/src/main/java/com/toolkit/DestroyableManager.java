package com.toolkit;

import android.text.TextUtils;
import android.util.Log;

import com.toolkit.action.Destroyable;

import java.util.HashMap;
import java.util.Map;

/**
 * game_werewolf
 * 2018/3/19 上午10:43
 * Mystery
 */

public class DestroyableManager {

    private static final String TAG = "DestroyableManager";

    private static volatile DestroyableManager mInstance;

    private Map<String, Destroyable> mDestroyableMap;

    private DestroyableManager() {

    }

    public static DestroyableManager getInstance() {
        if (mInstance == null) {
            synchronized (CancelableManager.class) {
                if (mInstance == null) {
                    mInstance = new DestroyableManager();
                }
            }
        }
        return mInstance;
    }

    public synchronized void addDestroyAble(String key, Destroyable destroyable) {
        if (mDestroyableMap == null) {
            mDestroyableMap = new HashMap<>();
        }
        if (mDestroyableMap.containsKey(key)) {
            Destroyable temp = mDestroyableMap.remove(key);
            temp.destroy();
        }
        mDestroyableMap.put(key, destroyable);
        Log.i(TAG, "addDestroyAble: add a destroyable key:" + key);
    }

    public synchronized boolean contains(String key) {
        if (mDestroyableMap == null) {
            return false;
        }
        if (TextUtils.isEmpty(key)) {
            return false;
        }
        return mDestroyableMap.containsKey(key);
    }

    public synchronized Destroyable removeDestroyAble(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        if (mDestroyableMap == null) {
            return null;
        }
        return mDestroyableMap.remove(key);
    }

    public synchronized void removeAndDestroyDestroyAble(String key) {
        Destroyable destroyable = removeDestroyAble(key);
        if (destroyable != null) {
            destroyable.destroy();
            Log.i(TAG, "removeAndDestroyDestroyAble: remove a destroyable key:" + key);
        }
    }
}
