
package android.graphics.drawable;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MaskDrawable extends Drawable implements Drawable.Callback {

    private static final String LOG_TAG = "MaskDrawable";

    private static final boolean DEFAULT_DITHER = true;
    private MaskState mState;
    private int mAlpha = 0xFF;
    private boolean mDither = DEFAULT_DITHER;
    private ColorFilter mColorFilter;

    private boolean mMutated;
    private final Rect mTempRect = new Rect();
    private boolean mWarning = false;

    public MaskDrawable(Drawable drawable, Drawable mask) {
        this(drawable, mask, PorterDuff.Mode.DST_IN);
    }
    
    public MaskDrawable(Drawable drawable, Drawable mask, PorterDuff.Mode mode) {
        this((MaskState) null, null);
        mState.mDrawable = drawable;
        mState.mMaskDrawable = mask;
        mState.mMode = mode;

        if (drawable != null) {
            drawable.setCallback(this);
        }
        if (mask != null) {
            mask.setCallback(this);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        RectF r = new RectF(mState.mDrawable.getBounds());

        int sc = canvas.saveLayer(r, null, Canvas.MATRIX_SAVE_FLAG
                | Canvas.CLIP_SAVE_FLAG | Canvas.HAS_ALPHA_LAYER_SAVE_FLAG
                | Canvas.FULL_COLOR_LAYER_SAVE_FLAG
                | Canvas.CLIP_TO_LAYER_SAVE_FLAG);
        if (mState.mDrawable != null) {
            mState.mDrawable.draw(canvas);
        }
        if (mState.mMaskDrawable != null) {
            Paint paint = getPaint(mState.mMaskDrawable);
            if (paint != null) {
                paint.setXfermode(new PorterDuffXfermode(mState.mMode));
                mState.mMaskDrawable.draw(canvas);
                // paint.setXfermode(null);
            }
        }
        canvas.restoreToCount(sc);
    }

    private Paint getPaint(Drawable drawable) {
        Paint result = null;
        try {
            Method method = drawable.getClass().getMethod("getPaint");
            result = (Paint) method.invoke(drawable);
        } catch (Exception e) {
            // ignore
        }

        if (result == null && !mWarning) {
            mWarning = true;
            Log.w(LOG_TAG,
                    "Failed get paint from mask drawable. The mask type: "
                            + drawable.getClass().getName());
        }
        return result;
    }

    @Override
    public int getChangingConfigurations() {
        return super.getChangingConfigurations()
                | mState.mChangingConfigurations
                | mState.mDrawable.getChangingConfigurations()
                | mState.mMaskDrawable.getChangingConfigurations();
    }

    @Override
    public boolean getPadding(Rect padding) {
        boolean result = false;
        Rect r = mTempRect;
        if (mState.mDrawable != null) {
            r.set(0, 0, 0, 0);
            if (mState.mDrawable.getPadding(r)) {
                padding.set(r);
                result = true;
            }
        }
        if (mState.mMaskDrawable != null) {
            r.set(0, 0, 0, 0);
            if (mState.mMaskDrawable.getPadding(r)) {
                padding.left = Math.max(padding.left, r.left);
                padding.top = Math.max(padding.top, r.top);
                padding.right = Math.max(padding.right, r.right);
                padding.bottom = Math.max(padding.bottom, r.bottom);
                result = true;
            }
        }

        return result || super.getPadding(padding);
    }

    @Override
    public void setAlpha(int alpha) {
        if (mAlpha != alpha) {
            mAlpha = alpha;
            if (mState.mDrawable != null) {
                mState.mDrawable.setAlpha(alpha);
            }
        }
    }

    @Override
    public void setDither(boolean dither) {
        super.setDither(dither);
        if (mDither != dither) {
            mDither = dither;
            if (mState.mDrawable != null) {
                mState.mDrawable.setDither(dither);
            }
        }
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        if (mColorFilter != cf) {
            mColorFilter = cf;
            if (mState.mDrawable != null) {
                mState.mDrawable.setColorFilter(cf);
            }
        }
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        if (mState.mDrawable != null) {
            mState.mDrawable.setBounds(bounds);
        }
        if (mState.mMaskDrawable != null) {
            mState.mMaskDrawable.setBounds(bounds);
        }
    }

    @Override
    public boolean isStateful() {
        boolean result = super.isStateful();
        if (mState.mDrawable != null) {
            result = mState.mDrawable.isStateful() || result;
        }
        if (mState.mMaskDrawable != null) {
            result = mState.mMaskDrawable.isStateful() || result;
        }

        return result;
    }

    @SuppressLint("NewApi")
    @Override
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        if (mState.mMaskDrawable != null) {
            DrawableCompat.jumpToCurrentState(mState.mMaskDrawable);
        }
        if (mState.mDrawable != null) {
            DrawableCompat.jumpToCurrentState(mState.mDrawable);
            mState.mDrawable.setAlpha(mAlpha);
        }
    }

    @Override
    protected boolean onStateChange(int[] state) {
        boolean result = super.onStateChange(state);
        if (mState.mMaskDrawable != null) {
            result = mState.mMaskDrawable.setState(state) || result;
        }
        if (mState.mDrawable != null) {
            result = mState.mDrawable.setState(state) || result;
        }
        return result;
    }

    @Override
    protected boolean onLevelChange(int level) {
        boolean result = super.onLevelChange(level);
        if (mState.mMaskDrawable != null) {
            result = mState.mMaskDrawable.onLevelChange(level) || result;
        }
        if (mState.mDrawable != null) {
            result = mState.mDrawable.onLevelChange(level) || result;
        }
        return result;
    }

    @Override
    public int getIntrinsicWidth() {
        int result = super.getIntrinsicWidth();
        if (mState.mDrawable != null) {
            result = Math.max(result, mState.mDrawable.getIntrinsicWidth());
        }
        if (mState.mMaskDrawable != null) {
            result = Math.max(result, mState.mMaskDrawable.getIntrinsicWidth());
        }
        return result;
    }

    @Override
    public int getIntrinsicHeight() {
        int result = super.getIntrinsicHeight();
        if (mState.mDrawable != null) {
            result = Math.max(result, mState.mDrawable.getIntrinsicHeight());
        }
        if (mState.mMaskDrawable != null) {
            result = Math
                    .max(result, mState.mMaskDrawable.getIntrinsicHeight());
        }
        return result;
    }

    @Override
    public int getMinimumWidth() {
        int result = super.getMinimumWidth();
        if (mState.mDrawable != null) {
            result = Math.max(result, mState.mDrawable.getMinimumWidth());
        }
        if (mState.mMaskDrawable != null) {
            result = Math.max(result, mState.mMaskDrawable.getMinimumWidth());
        }
        return result;
    }

    @Override
    public int getMinimumHeight() {
        int result = super.getMinimumHeight();
        if (mState.mDrawable != null) {
            result = Math.max(result, mState.mDrawable.getMinimumHeight());
        }
        if (mState.mMaskDrawable != null) {
            result = Math.max(result, mState.mMaskDrawable.getMinimumHeight());
        }
        return result;
    }

    public void invalidateDrawable(Drawable who) {
        Callback callback = _getCallback();
        if (callback != null
                && (who == mState.mDrawable || who == mState.mMaskDrawable)) {
            callback.invalidateDrawable(this);
        }
    }

    public void scheduleDrawable(Drawable who, Runnable what, long when) {
        Callback callback = _getCallback();
        if (callback != null
                && (who == mState.mDrawable || who == mState.mMaskDrawable)) {
            callback.scheduleDrawable(this, what, when);
        }
    }

    public void unscheduleDrawable(Drawable who, Runnable what) {
        Callback callback = _getCallback();
        if (callback != null
                && (who == mState.mDrawable || who == mState.mMaskDrawable)) {
            callback.unscheduleDrawable(this, what);
        }
    }

    @Override
    public boolean setVisible(boolean visible, boolean restart) {
        boolean changed = super.setVisible(visible, restart);
        if (mState.mMaskDrawable != null) {
            changed = mState.mMaskDrawable.setVisible(visible, restart)
                    || changed;
        }
        if (mState.mDrawable != null) {
            changed = mState.mDrawable.setVisible(visible, restart) || changed;
        }
        return changed;
    }

    @Override
    public int getOpacity() {
        return mState.mDrawable == null || !mState.mDrawable.isVisible() ? PixelFormat.TRANSPARENT
                : mState.mDrawable.getOpacity();
    }

    @Override
    public ConstantState getConstantState() {
        if (mState.canConstantState()) {
            mState.mChangingConfigurations = getChangingConfigurations();
            return mState;
        }
        return null;
    }

    @Override
    public Drawable mutate() {
        if (!mMutated && super.mutate() == this) {
            if (mState.mDrawable != null) {
                mState.mDrawable.mutate();
            }
            if (mState.mMaskDrawable != null) {
                mState.mMaskDrawable.mutate();
            }
            mMutated = true;
        }

        return this;
    }

    final static class MaskState extends ConstantState {
        Drawable mDrawable;
        Drawable mMaskDrawable;
        PorterDuff.Mode mMode = PorterDuff.Mode.DST_IN;
        
        int mChangingConfigurations;

        private boolean mCheckedConstantState;
        private boolean mCanConstantState;

        MaskState(MaskState orig, MaskDrawable owner, Resources res) {
            if (orig != null) {
                if (res != null) {
                    mDrawable = orig.mDrawable.getConstantState().newDrawable(
                            res);
                } else {
                    mDrawable = orig.mDrawable.getConstantState().newDrawable();
                }
                mDrawable.setCallback(owner);
                if (res != null) {
                    mMaskDrawable = orig.mMaskDrawable.getConstantState()
                            .newDrawable(res);
                } else {
                    mMaskDrawable = orig.mMaskDrawable.getConstantState()
                            .newDrawable();
                }
                mMode = orig.mMode;
                mCheckedConstantState = mCanConstantState = true;
            }
        }

        @Override
        public Drawable newDrawable() {
            return new MaskDrawable(this, null);
        }

        @Override
        public Drawable newDrawable(Resources res) {
            return new MaskDrawable(this, res);
        }

        @Override
        public int getChangingConfigurations() {
            return mChangingConfigurations;
        }

        boolean canConstantState() {
            if (!mCheckedConstantState) {
                mCanConstantState = mDrawable.getConstantState() != null;
                mCheckedConstantState = true;
            }

            return mCanConstantState;
        }
    }

    private MaskDrawable(MaskState state, Resources res) {
        mState = new MaskState(state, this, res);
    }

    private static final Method METHOD_getCallback;
    private static final Field FIELD_Callback;
    static {
        Method methodGetCallback = null;
        Field filedCallback = null;
        try {
            methodGetCallback = MaskDrawable.class.getMethod("getCallback");
        } catch (NoSuchMethodException e) {
        }

        try {
            filedCallback = MaskDrawable.class.getField("mCallback");
        } catch (NoSuchFieldException e) {
        }
        METHOD_getCallback = methodGetCallback;
        FIELD_Callback = filedCallback;
    }

    private Callback _getCallback() {
        Callback result = null;
        boolean handled = false;
        if (METHOD_getCallback != null) {
            try {
                result = (Callback) METHOD_getCallback.invoke(this);
                handled = true;
            } catch (Exception e) {
            }
        }

        if (!handled && FIELD_Callback != null) {
            try {
                result = (Callback) FIELD_Callback.get(this);
                handled = true;
            } catch (Exception e) {
                Log.w(LOG_TAG, "Failed getCallback.", e);
            }
        }

        if (!handled) {
            Log.e(LOG_TAG, "Failed getCallback.");
        }

        return result;
    }
}
