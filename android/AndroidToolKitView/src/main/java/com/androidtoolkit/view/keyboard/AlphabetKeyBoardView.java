package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.MaterialButton;
import cn.intviu.widget.R;

/**
 * Created by Mystery on 16/9/10.
 */
public class AlphabetKeyBoardView extends BasicKeyBoardView {

    private Context mContext;
    private boolean mShiftPressed = false;
    private List<MaterialButton> mAlphabetList = new ArrayList<MaterialButton>();
    private static final String alphabet = "qwertyuiopasdfghjklzxcvbnm";


    public AlphabetKeyBoardView(ViewGroup mMainKeyBoardView, IKeyBoardInputListener mListener) {
        super(mMainKeyBoardView, mListener);
        this.mContext = mMainKeyBoardView.getContext();
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        mKeyBoardView = linearLayout;
        initKeyBoardView();
    }

    //false lowerCase true upCase
    private void initTag(boolean flag) {
        for (int i = 0; i < mAlphabetList.size(); i++) {
            MaterialButton button = mAlphabetList.get(i);
            String tag;
            if (!flag) {
                tag = (alphabet.substring(i, i + 1)).toLowerCase();
            } else {
                tag = (alphabet.substring(i, i + 1)).toUpperCase();
            }
            button.setTag(tag);
        }
    }

    private void initText(boolean flag) {
        for (int i = 0; i < mAlphabetList.size(); i++) {
            MaterialButton button = mAlphabetList.get(i);
            String tag;
            if (!flag) {
                tag = (alphabet.substring(i, i + 1)).toLowerCase();
            } else {
                tag = (alphabet.substring(i, i + 1)).toUpperCase();
            }
            button.setText(tag);
        }
    }

    private void initListener() {
        for (MaterialButton button : mAlphabetList) {
            button.setOnClickListener(this);
        }
    }

    public void initKeyBoardView() {
        View view = initView();
        initListener();
        mKeyBoardView.addView(view, KeyBoardUtil.getLinearLayoutParams());
    }

    public View initView() {
        View view = View.inflate(mKeyBoardView.getContext(), R.layout.layout_alphabet_keyboard, null);
        MaterialButton button;
        MaterialButton imageView;
        button = (MaterialButton) view.findViewById(R.id.keyboard_q);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_w);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_e);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_r);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_t);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_y);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_u);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_i);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_o);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_p);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_a);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_s);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_d);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_f);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_g);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_h);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_j);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_k);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_l);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_z);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_x);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_c);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_v);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_b);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_n);
        mAlphabetList.add(button);
        button = (MaterialButton) view.findViewById(R.id.keyboard_m);
        mAlphabetList.add(button);

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_done);
        imageView.setTag(KeyEvent.KEYCODE_ENTER);
        imageView.setOnClickListener(this);

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_shift);
        imageView.setTag(KeyEvent.KEYCODE_SHIFT_LEFT);
        imageView.setOnClickListener(this);

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_delete);
        imageView.setTag(KeyEvent.KEYCODE_DEL);
        imageView.setOnClickListener(this);
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.deleteAll();
                return true;
            }
        });

        button = (MaterialButton) view.findViewById(R.id.keyboard_hide);
        button.setTag(KEYCODE_CANCEL);
        button.setOnClickListener(this);

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_space);
        imageView.setTag(KeyEvent.KEYCODE_SPACE);
        imageView.setOnClickListener(this);

        button = (MaterialButton) view.findViewById(R.id.keyboard_number);
        button.setTag(KeyEvent.KEYCODE_NUM);
        button.setOnClickListener(this);

        button = (MaterialButton) view.findViewById(R.id.keyboard_symbols);
        button.setTag(KeyEvent.KEYCODE_SYM);
        button.setOnClickListener(this);

        return view;
    }

    public void clickShift() {
        MaterialButton shift = (MaterialButton) mKeyBoardView.findViewWithTag(KeyEvent.KEYCODE_SHIFT_LEFT);
        mShiftPressed = !mShiftPressed;
        if (mShiftPressed) {
            shift.setBitmap(((BitmapDrawable) mKeyBoardView.getContext().getResources().getDrawable(R.drawable.keyboard_lowercase)).getBitmap());
        } else {
            shift.setBitmap(((BitmapDrawable) mKeyBoardView.getContext().getResources().getDrawable(R.drawable.keyboard_uppercase)).getBitmap());
        }
    }

    public void changeCase() {
        if (mShiftPressed) {
            initTag(mShiftPressed);
            initText(mShiftPressed);
        } else {
            initTag(mShiftPressed);
            initText(mShiftPressed);
        }
    }

    @Override
    public boolean handleClick(View view, int code) {
        if (!super.handleClick(view, code)) {
            switch (code) {
                case KeyEvent.KEYCODE_SHIFT_LEFT:
                    clickShift();
                    changeCase();
                    return true;
                case KeyEvent.KEYCODE_SYM:
                    mListener.switchType(FULLSYMBOL_KEYBOARD);
                    return true;
                case KeyEvent.KEYCODE_NUM:
                    mListener.switchType(SMALLNUMBER_KEYBOARD);
                    return true;
                default:
                    return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void originalState() {
        mShiftPressed = false;
        changeCase();
    }

    @Override
    public void release() {

    }
}
