package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.widget.LinearLayout;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Mystery on 16/9/10.
 */
public class KeyBoardView extends LinearLayout implements IKeyBoardInputListener, CustomKeyboardContants {

    private Context mContext;
    private IKeyBoardInputListener mListener;
    private final Map<Integer, BasicKeyBoardView> mKeyboardMap = new ConcurrentHashMap<Integer, BasicKeyBoardView>();
    private int mKeyBoardType = -1;
    private BasicKeyBoardView mBasicKeyBoardView;

    public KeyBoardView(IKeyBoardInputListener listener) {
        super(KeyBoardUtil.getContext());
        this.mListener = listener;
        this.mContext = KeyBoardUtil.getContext();
        initKeyBoardMap();
    }

    private void initKeyBoardMap() {
        mKeyboardMap.put(ALPHABET_KEYBOARD, new AlphabetKeyBoardView(this, this));
        mKeyboardMap.put(FULLSYMBOL_KEYBOARD, new SymbolsKeyBoardView(this, this));
        mKeyboardMap.put(SMALLNUMBER_KEYBOARD, new NumberKeyBoardView(this, this));
        setKeyBoardByType(SMALLNUMBER_KEYBOARD);
    }

    public void setKeyBoardByType(int type) {
        if (mKeyBoardType != type) {
            mKeyBoardType = type;
            removeAllViews();
            mBasicKeyBoardView = mKeyboardMap.get(type);
            addView(mBasicKeyBoardView.getKeyBoardView());
        }
        clearKeyboard();
    }

    public void clearKeyboard() {
        mKeyboardMap.get(mKeyBoardType).originalState();
    }

    @Override
    public void input(char input) {
        if (mListener != null)
            mListener.input(input);
    }

    @Override
    public void delete() {
        if (mListener != null)
            mListener.delete();
    }

    @Override
    public void done() {
        if (mListener != null)
            mListener.done();
    }

    @Override
    public void cancel() {
        if (mListener != null)
            mListener.cancel();
    }

    @Override
    public void switchType(int type) {
        if (mListener != null)
            mListener.switchType(type);
    }

    @Override
    public void deleteAll() {
        if (mListener != null)
            mListener.deleteAll();
    }
}
