package com.androidtoolkit.view.keyboard;

import android.widget.EditText;

/**
 * Created by Mystery on 16/9/10.
 */
public interface IKeyBoard {

    public void show(EditText text);

    public void hide();

    public boolean isShowing();

    public void clear();

    public void setInputFinsh(KeyBoard.IInputFinish inputFinsh);

}
