package com.androidtoolkit.view.viewpagergroup;

import android.view.View;

public interface GridViewPagerGroupHolderCreator<Data, Holder extends ViewPagerGroupHolder> {

    Holder createViewHolder(View view);

    void onBindViewHolder(Data data, Holder holder);

    void onDestroy();

    View createViewHolderView();
}
