package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;

import cn.intviu.widget.R;

/**
 * Created by Mystery on 16/9/10.
 */
public class KeyBoard implements IKeyBoard, CustomKeyboardContants {


    private PopupWindow mKeyBoardWindow;
    private KeyBoardView mKeyBoardView;
    private String mInput = "";
    private boolean isShowing = false;
    private IKeyBoardInputListener listener;
    private Context mContext;
    private int mKeyBoardHeight;
    private int mKeyBoardWidth;
    private View mCurrentView;
    private IKeyBoardInputListener mListener;

    public interface IInputFinish {
        public void inputFinish();
    }

    private IInputFinish mInputFinsh = null;

    @Override
    public void setInputFinsh(IInputFinish mInputFinsh) {
        this.mInputFinsh = mInputFinsh;
    }

    public KeyBoard() {
        mContext = KeyBoardUtil.getContext();
        initKeyBoardWindow();
        initKeyBoardListener();
    }


    private void initKeyBoardListener() {
        mListener = new IKeyBoardInputListener() {
            @Override
            public void input(char input) {
                handleInput(input);
            }

            @Override
            public void delete() {
                handleDelete();
            }

            @Override
            public void done() {
                hide();
                if (mInputFinsh != null)
                    mInputFinsh.inputFinish();
            }

            @Override
            public void cancel() {
                hide();
            }

            @Override
            public void switchType(int type) {
                mKeyBoardView.setKeyBoardByType(type);
            }

            @Override
            public void deleteAll() {
                handleDeleteAll();
            }
        };
        this.mKeyBoardView = new KeyBoardView(mListener);
        this.mKeyBoardWindow.setContentView(mKeyBoardView);
    }

    private void handleDeleteAll() {
        if (mCurrentView instanceof EditText) {
            EditText editText = (EditText) mCurrentView;
            mInput = editText.getText().toString();
            if (TextUtils.isEmpty(mInput)) {
                return;
            }
            int selectedStart = editText.getSelectionStart();
            int selectedEnd = editText.getSelectionEnd();
            if (selectedStart == selectedEnd && selectedEnd < mInput.length()) {
                mInput = mInput.substring(selectedEnd, mInput.length());
                editText.setText(mInput);
                editText.setSelection(0);
                return;
            }
            clear();
        }
    }

    private void handleDelete() {
        if (mCurrentView instanceof EditText) {
            EditText edittext = (EditText) mCurrentView;
            mInput = edittext.getText().toString();
            if (TextUtils.isEmpty(mInput))
                return;
            int selectedStart = edittext.getSelectionStart();
            int selectedEnd = edittext.getSelectionEnd();
            if (selectedStart == selectedEnd && selectedEnd == mInput.length()) {
                mInput = mInput.substring(0, mInput.length() - 1);
                edittext.setText(mInput);
                edittext.setSelection(mInput.length());
                return;
            }
            if (selectedEnd != selectedStart && selectedEnd <= mInput.length()) {
                String beforeSelected = mInput.substring(0, selectedStart);
                String afterSelected = mInput.substring(selectedEnd, mInput.length());
                mInput = beforeSelected + afterSelected;
                edittext.setText(mInput);
                edittext.setSelection(selectedStart);
                return;
            }
            if (selectedEnd == selectedStart && selectedEnd <= mInput.length()) {
                String beforeSelected = mInput.substring(0, selectedStart);
                if (TextUtils.isEmpty(beforeSelected)) {
                    return;
                }
                String afterSelected = mInput.substring(selectedEnd, mInput.length());
                mInput = beforeSelected.substring(0, beforeSelected.length() - 1) + afterSelected;
                edittext.setText(mInput);
                edittext.setSelection((selectedStart - 1) > 0 ? (selectedStart - 1) : 0);
                return;
            }

        }
    }

    private void handleInput(char input) {
        if (mCurrentView instanceof EditText) {
            EditText edittext = (EditText) mCurrentView;
            mInput = edittext.getText().toString();
            int selectedStart = edittext.getSelectionStart();
            int selectedEnd = edittext.getSelectionEnd();
            if (selectedStart == selectedEnd && selectedStart == mInput.length()) {
                mInput = mInput + input;
                edittext.setText(mInput);
                edittext.setSelection(mInput.length());
                return;
            }
            if (selectedStart != selectedEnd && selectedEnd <= mInput.length()) {
                String beforeStart = mInput.substring(0, selectedStart);
                String afterEnd = mInput.substring(selectedEnd, mInput.length());
                mInput = beforeStart + input + afterEnd;
                edittext.setText(mInput);
                edittext.setSelection(selectedStart + 1);
                return;
            }
            if (selectedStart == selectedEnd && selectedEnd <= mInput.length()) {
                String beforeStart = mInput.substring(0, selectedStart);
                String afterEnd = mInput.substring(selectedEnd, mInput.length());
                mInput = beforeStart + input + afterEnd;
                edittext.setText(mInput);
                edittext.setSelection(selectedStart + 1, selectedStart + 1);
                return;
            }
        }
    }

    private void initKeyBoardWindow() {
        this.mKeyBoardHeight = (int) (KeyBoardUtil.getScreenHeight() * 0.4f);
        this.mKeyBoardWidth = KeyBoardUtil.getScreenWidth();
        this.mKeyBoardWindow = new PopupWindow(mContext);
        mKeyBoardWindow.setBackgroundDrawable(new BitmapDrawable(mContext
                .getResources()));
        mKeyBoardWindow.setFocusable(false);
        mKeyBoardWindow.setTouchable(true);
        mKeyBoardWindow.setAnimationStyle(R.style.keyboard_popstyle);
        mKeyBoardWindow.setWidth(mKeyBoardWidth);
        mKeyBoardWindow.setHeight(mKeyBoardHeight);
    }

    @Override
    public void show(EditText text) {
        if (text.getWindowToken() != null) {
            mCurrentView = text;
            mKeyBoardWindow.setOutsideTouchable(false);
            mKeyBoardWindow.setTouchInterceptor(null);
            showKeyBoard();
        }
    }

    private void showKeyBoard() {
        KeyBoardUtil.execRunnableDelay(new Runnable() {
            @Override
            public void run() {
                if (isShowing()) {
                    hide();
                } else {
                    mKeyBoardView.setKeyBoardByType(SMALLNUMBER_KEYBOARD);
                    mKeyBoardWindow.showAtLocation(mCurrentView.getRootView(), Gravity.BOTTOM
                            , 0, 0);
                    if (mCurrentView instanceof EditText) {
                        EditText editText = (EditText) mCurrentView;
                        String text = editText.getText().toString();
                        if (TextUtils.isEmpty(text)) {
                            clear();
                        } else {
                            mInput = text;
                            editText.setSelection(mInput.length(), mInput.length());
                        }
                    }
                }
            }
        }, 400);
    }

    @Override
    public void hide() {
        if (mCurrentView != null && mCurrentView.getWindowToken() != null
                && mKeyBoardWindow != null && mKeyBoardWindow.isShowing()) {
            KeyBoardUtil.execRunnableDelay(new Runnable() {
                @Override
                public void run() {
                    mKeyBoardWindow.dismiss();
                }
            }, 400);
        }


    }

    @Override
    public boolean isShowing() {
        if (mKeyBoardWindow != null && mKeyBoardWindow.isShowing()) {
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        mInput = "";
        if (mCurrentView instanceof EditText) {
            ((EditText) mCurrentView).setText("");
        }
    }
}
