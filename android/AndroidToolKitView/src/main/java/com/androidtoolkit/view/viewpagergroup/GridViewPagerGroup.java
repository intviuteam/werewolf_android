package com.androidtoolkit.view.viewpagergroup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.DestroyFailedException;
import javax.security.auth.Destroyable;


public abstract class GridViewPagerGroup<GridViewItem, ViewHolder extends ViewPagerGroupHolder> extends ViewPagerGroup<GridView> implements Destroyable {


    private List<GridViewItem> data;
    private List<GridView> mGridViewList = new ArrayList<>();
    private int mItemMaxNum;
    private int mItemColumnNum;
    private int mGridViewId;
    protected GridViewPagerGroupHolderCreator<GridViewItem, ViewHolder> mViewPagerGroupHolderCreator;

    public GridViewPagerGroup(@NonNull Context context) {
        this(context, null);
    }

    public GridViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GridViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initResource(int id, int maxNum, int columnNum, int select, int unSelect) {
        mGridViewId = id;
        mItemMaxNum = maxNum;
        mItemColumnNum = columnNum;
        setIndicatorResID(select, unSelect);
    }

    public void setViewPagerGroupHolderCreator(GridViewPagerGroupHolderCreator<GridViewItem, ViewHolder> viewPagerGroupHolderCreator) {
        this.mViewPagerGroupHolderCreator = viewPagerGroupHolderCreator;
    }

    public void setData(List<GridViewItem> data) {
        if (data == null) return;

        setTotalPagers(data.size() / mItemMaxNum);
        for (int i = 0; i < mTotalPagers; i++) {
            final GridView gridView = (GridView) View.inflate(getContext(), mGridViewId, null);
            gridView.setNumColumns(mItemColumnNum);
            gridView.setAdapter(new ViewPagerAdapter<>(getContext(), data, i, mItemMaxNum, mViewPagerGroupHolderCreator));
            mGridViewList.add(gridView);
        }
        setPagersData(mGridViewList);
    }

    @Override
    public void destroy() {
        try {
            for (GridView gridView : mGridViewList) {
                ((ViewPagerAdapter) gridView.getAdapter()).destroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class ViewPagerAdapter<GridViewItem, ViewHolder extends ViewPagerGroupHolder> extends BaseAdapter implements Destroyable {

        private Context mContext;
        private int mCurIndex;
        private int mMaxSize;
        private List<GridViewItem> mData;
        GridViewPagerGroupHolderCreator<GridViewItem, ViewHolder> mGridViewGroupHolderCreator;


        private ViewPagerAdapter(Context context, List<GridViewItem> data, int curIndex, int maxSize, @NonNull GridViewPagerGroupHolderCreator<GridViewItem, ViewHolder> creator) {
            this.mContext = context;
            this.mData = data;
            this.mCurIndex = curIndex;
            this.mMaxSize = maxSize;
            this.mGridViewGroupHolderCreator = creator;
        }

        private int getCurIndexStartIndex() {
            return mCurIndex * mMaxSize;
        }

        @Override
        public int getCount() {
            return mData.size() > (mCurIndex + 1) * mMaxSize ?
                    mMaxSize : (mData.size() - getCurIndexStartIndex());
        }

        @Override
        public GridViewItem getItem(int position) {
            return mData.get(position + getCurIndexStartIndex());
        }

        @Override
        public long getItemId(int position) {
            return position + mCurIndex * mMaxSize;
        }

        public void destroy() {
            if (mGridViewGroupHolderCreator != null) {
                mGridViewGroupHolderCreator.onDestroy();
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mGridViewGroupHolderCreator.createViewHolderView();
                holder = mGridViewGroupHolderCreator.createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            mGridViewGroupHolderCreator.onBindViewHolder(getItem(position), holder);
            return convertView;
        }

    }

}
