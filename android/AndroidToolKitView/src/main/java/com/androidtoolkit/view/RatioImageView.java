package com.androidtoolkit.view;

/**
 * Created by Mystery on 16/9/25.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RatioImageView extends ImageView {
    private float ratio;
    private Context context;

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initRatio();
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRatio();
    }

    public RatioImageView(Context context) {
        super(context);
        this.context = context;
        initRatio();
    }

    public RatioImageView(Context context, int id) {
        super(context);
        this.context = context;
        initRatio();
    }

    @Override
    public void setBackgroundResource(int resid) {
        // TODO Auto-generated method stub
        super.setBackgroundResource(resid);
        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), resid);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        ratio = (float) width / height;
    }

    private void initRatio() {
        int width, height;
        width = this.getBackground().getIntrinsicWidth();
        height = this.getBackground().getIntrinsicHeight();
        ratio = (float) width / height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) { // 父容器传过来的宽度方向上的模式
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        // 父容器传过来的高度方向上的模式
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        // 父容器传过来的宽度的值
        int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        // 父容器传过来的高度的值
        int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingLeft() - getPaddingRight();
        if (widthMode == MeasureSpec.EXACTLY && heightMode != MeasureSpec.EXACTLY && ratio != 0.0f) { // 判断条件为，宽度模式为Exactly，也就是填充父窗体或者是指定宽度；
            // 且高度模式不是Exaclty，代表设置的既不是fill_parent也不是具体的值，于是需要具体测量
            // 且图片的宽高比已经赋值完毕，不再是0.0f
            // 表示宽度确定，要测量高度
            height = (int) (width / ratio + 0.5f);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        } else if (widthMode != MeasureSpec.EXACTLY && heightMode == MeasureSpec.EXACTLY && ratio != 0.0f) {
            // 判断条件跟上面的相反，宽度方向和高度方向的条件互换
            // 表示高度确定，要测量宽度
            width = (int) (height * ratio + 0.5f);
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

}
