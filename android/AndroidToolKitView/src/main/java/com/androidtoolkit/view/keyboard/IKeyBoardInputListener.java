package com.androidtoolkit.view.keyboard;

/**
 * Created by Mystery on 16/9/9.
 */
public interface IKeyBoardInputListener {

    public void input(char input);

    public void delete();

    public void done();

    public void cancel();

    public void switchType(int type);

    public void deleteAll();

}
