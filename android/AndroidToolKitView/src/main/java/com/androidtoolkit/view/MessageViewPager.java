package com.androidtoolkit.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by wx on 16-7-4.
 */
public class MessageViewPager extends ViewPager {
    private boolean isPagingEnabled = true;

    public MessageViewPager(Context context) {
        super(context);
    }

    public MessageViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return  this.isPagingEnabled;
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }

}
