package com.androidtoolkit.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wenjy on 2016/9/1.
 */
public class DragScaleViewGroup extends RelativeLayout {

    public static final String TAG = "DragScaleViewGroup";
    public View childView = null;
    private float mMidPntX, mMidPntY;
    private ScaleGestureDetector mScaleGestureDetector = null;
    private GestureDetector mGestureDetector = null;
    private int mWidth = 0;
    private int mHeight = 0;

    private int mMaxWidth = 0;
    private int mMaxHeight = 0;
    private int mMinWidth = 0;
    private int mMinHeight = 0;
    private int mCurWidth = 0;
    private int mCurHeight = 0;
    private int mLastWidth = 0;
    private int mLastHeight = 0;
    private boolean mInitHeight = false;
    private int mMaxScale = 3;

    private int mMarginLeft = 0;
    private int mMarginRight = 0;
    private int mMarginTop = 0;
    private int mMarginBottom = 0;
    private float mDistanceX = 0;
    private float mDistanceY = 0;

    private int mLastMarginLeft = 0;
    private int mLastMarginRight = 0;
    private int mLastMarginBottom = 0;
    private int mLastMarginTop = 0;

    private int mScaleMinDis = 20;
    private int mMoveMinDis = 5;

    private Object layoutLock = new Object();

    private boolean mSwitch = false;
    public List<IDecorateRequestLayout> mDecorateRequestLayouts = new ArrayList<IDecorateRequestLayout>(1);

    public DragScaleViewGroup(Context context) {
        this(context, null);
    }

    public DragScaleViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragScaleViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mScaleGestureDetector = new ScaleGestureDetector(context, new ScaleListener());
        mGestureDetector = new GestureDetector(getContext(), new GestureListener(), null, true);
    }

    public int getMaxScale() {
        return mMaxScale;
    }

    public void setMaxScale(int maxScale) {
        mMaxScale = maxScale;
    }

    public void setLayoutByForce(int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
        this.mMaxHeight = mMaxScale * this.mHeight;
        this.mMaxWidth = mMaxScale * this.mWidth;
        this.mMinHeight = this.mHeight;
        this.mMinWidth = this.mWidth;
        this.mCurHeight = this.mHeight;
        this.mCurWidth = this.mWidth;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (getChildCount() > 1) {
            throw new NullPointerException("not support child count > 1 for now");
        }
        if (!mInitHeight) {
            this.mWidth = Math.abs(r - l);
            this.mHeight = Math.abs(b - t);
            this.mMaxHeight = mMaxScale * this.mHeight;
            this.mMaxWidth = mMaxScale * this.mWidth;
            this.mMinHeight = this.mHeight;
            this.mMinWidth = this.mWidth;
            this.mCurHeight = this.mHeight;
            this.mCurWidth = this.mWidth;
            this.childView = getChildAt(0);
            mInitHeight = true;
        }

        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getChildCount() > 1) {
            throw new NullPointerException("not support child count > 1 for now");
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) {
            mMidPntX = (event.getX(0) + event.getX(1)) / 2;
            mMidPntY = (event.getY(0) + event.getY(1)) / 2;
        }
        if (mSwitch) {
            if (mScaleGestureDetector.onTouchEvent(event) | mGestureDetector.onTouchEvent(event)) {
                return true;
            }
        }
        return false;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scale = detector.getScaleFactor();
            handleScale(scale);
            computeScaleMargin(detector.getScaleFactor(), mMidPntX, mMidPntY);
            if (needScaleChange()) {
                setMargin(mMarginLeft, mMarginTop, mMarginRight, mMarginBottom);
            } else {
                return false;
            }
            return true;
        }
    }

    private void handleScale(float scale) {
        int width = (int) (mCurWidth * scale);
        int height = (int) (mCurHeight * scale);
        if (width >= mMinWidth && width <= mMaxWidth && height >= mMinHeight && height <= mMaxHeight) {
            mCurWidth = width;
            mCurHeight = height;
        }
        if (mCurHeight <= mMinHeight) {
            mCurHeight = mMinHeight;
        }
        if (mCurHeight >= mMaxHeight) {
            mCurHeight = mMaxHeight;
        }
        if (mCurWidth >= mMaxWidth) {
            mCurWidth = mMaxWidth;
        }
        if (mCurWidth <= mMinWidth) {
            mCurWidth = mMinWidth;
        }
    }

    private void computeScaleMargin(float scale, float x, float y) {
//        this.mMarginTop = -(int) (y * scale);
//        this.mMarginLeft = -(int) (x * scale);
//        this.mMarginRight = -((mCurWidth - mWidth - mMarginLeft) <= 0 ? 0 : (mCurWidth - mWidth - mMarginLeft));
//        this.mMarginBottom = -((mCurHeight - mHeight - mMarginTop) <= 0 ? 0 : (mCurHeight - mHeight - mMarginTop));

        this.mMarginLeft = -(mCurWidth - mWidth) / 2;
        this.mMarginTop = -(mCurHeight - mHeight) / 2;
        this.mMarginRight = -(mCurWidth - mWidth) / 2;
        this.mMarginBottom = -(mCurHeight - mHeight) / 2;
    }

    private boolean changeTooLittle(int last, int curt, int dur) {
        if (Math.abs(last - curt) <= dur) {
            return true;
        } else {
            return false;
        }
    }

    private boolean needScaleChange() {
        if (changeTooLittle(mLastHeight, mCurHeight, mScaleMinDis) || changeTooLittle(mLastWidth, mCurWidth, mScaleMinDis)) {
            return false;
        } else {
            mLastHeight = mCurHeight;
            mLastWidth = mCurWidth;
            return true;
        }
    }

    private boolean needMoveChange() {
        if (changeTooLittle(mLastMarginTop, mMarginTop, mMoveMinDis) ||
                changeTooLittle(mLastMarginRight, mMarginRight, mMoveMinDis) ||
                changeTooLittle(mLastMarginBottom, mMarginBottom, mMoveMinDis) ||
                changeTooLittle(mLastMarginLeft, mMarginLeft, mMoveMinDis)) {
            return false;
        } else {
            mLastMarginTop = mMarginTop;
            mLastMarginBottom = mMarginBottom;
            mLastMarginRight = mMarginRight;
            mLastMarginLeft = mMarginLeft;
            return true;
        }
    }

    private synchronized void setMargin(final int l, final int t, final int r, final int b) {
        post(new Runnable() {
            @Override
            public void run() {
                synchronized (layoutLock) {
                    LayoutParams layoutParams = (LayoutParams) childView.getLayoutParams();
                    layoutParams.setMargins(l, t, r, b);
                    childView.setLayoutParams(layoutParams);
                }
            }
        });

    }

    private int marginAdd(int margin, int d) {
        return -(Math.abs(margin) + Math.abs(d));
    }

    private void handleMove() {
        if (mDistanceX >= 0) {
            //right
            if (Math.abs(mMarginRight) <= Math.abs(mDistanceX)) {
                mMarginRight = 0;
                mMarginLeft = marginAdd(mMarginRight, mMarginLeft);
            } else {
                mMarginRight = (int) (mMarginRight + mDistanceX);
                mMarginLeft = marginAdd(mMarginLeft, (int) mDistanceX);
            }
        } else {
            if (Math.abs(mMarginLeft) <= Math.abs(mDistanceX)) {
                mMarginLeft = 0;
                mMarginRight = marginAdd(mMarginRight, mMarginLeft);
            } else {
                mMarginLeft = (int) (mMarginLeft + Math.abs(mDistanceX));
                mMarginRight = marginAdd(mMarginRight, (int) mDistanceX);
            }
        }

        if (mDistanceY >= 0) {
            if (Math.abs(mMarginBottom) <= Math.abs(mDistanceY)) {
                mMarginBottom = 0;
                mMarginTop = marginAdd(mMarginTop, mMarginBottom);
            } else {
                mMarginBottom = (int) (mMarginBottom + Math.abs(mDistanceY));
                mMarginTop = marginAdd(mMarginTop, (int) mDistanceY);
            }
        } else {
            if (Math.abs(mMarginTop) <= Math.abs(mDistanceY)) {
                mMarginTop = 0;
                mMarginBottom = marginAdd(mMarginBottom, mMarginTop);
            } else {
                mMarginTop = (int) (mMarginTop + Math.abs(mDistanceY));
                mMarginBottom = marginAdd(mMarginBottom, (int) mDistanceY);
            }
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return super.onDoubleTap(e);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

            mDistanceX = distanceX;
            mDistanceY = distanceY;
            handleMove();
            if (needMoveChange()) {
                setMargin(mMarginLeft, mMarginTop, mMarginRight, mMarginBottom);
                return true;
            } else {

            }
            return false;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    public void addDecorateRequestLayout(IDecorateRequestLayout layout) {
        if (!mDecorateRequestLayouts.contains(layout))
            mDecorateRequestLayouts.add(layout);
    }

    public void removeDecorateRequestLayout(IDecorateRequestLayout layout) {
        if (mDecorateRequestLayouts.contains(layout))
            mDecorateRequestLayouts.remove(layout);
    }

    public void dispatchBeforeRequestLayout() {
        if (mDecorateRequestLayouts.size() > 0) {
            for (IDecorateRequestLayout layout : mDecorateRequestLayouts) {
                layout.beforeRequestLayout();
            }
        }
    }

    public void dispatchAfterRequestLayout() {
        if (mDecorateRequestLayouts.size() > 0) {
            for (IDecorateRequestLayout layout : mDecorateRequestLayouts) {
                layout.afterRequestLayout();
            }
        }
    }


    public void switchAllOff() {
        mSwitch = false;
    }

    public void switchAllOn() {
        mSwitch = true;
    }

    public interface IDecorateRequestLayout {

        public void beforeRequestLayout();

        public void afterRequestLayout();
    }

}
