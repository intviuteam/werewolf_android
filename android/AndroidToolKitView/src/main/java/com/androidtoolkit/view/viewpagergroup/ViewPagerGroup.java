package com.androidtoolkit.view.viewpagergroup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import cn.intviu.widget.R;

public class ViewPagerGroup<PagerItem extends View> extends FrameLayout {

    protected ViewPager mViewPager;
    protected ViewPagerGroupAdapter<PagerItem> mapViewPagerAdapter;

    protected int mTotalPagers = 0;
    protected int mCurPager = 0;

    protected LinearLayout mPagerIndex;
    protected ImageView[] mIndexPoints;

    private int mSelectIndicator;
    private int mUnSelectIndicator;


    public ViewPagerGroup(@NonNull Context context) {

        this(context, null);
    }

    public ViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewPagerGroup(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = View.inflate(context, R.layout.layout_default_popwindow, null);
        addView(view, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mViewPager = findViewById(R.id.viewpager);
        mPagerIndex = findViewById(R.id.indicator);
        mapViewPagerAdapter = new ViewPagerGroupAdapter<PagerItem>();
        mViewPager.setAdapter(mapViewPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mTotalPagers; i++) {
                    if (i == position) {
                        mIndexPoints[i].setImageResource(mSelectIndicator);
                    } else {
                        mIndexPoints[i].setImageResource(mUnSelectIndicator);
                    }
                }
                mCurPager = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    protected void setPagersData(List<PagerItem> pagersData) {
        mapViewPagerAdapter.setData(pagersData);
    }

    protected void setIndicatorResID(int selectIndicator, int unSelectIndicator) {
        mSelectIndicator = selectIndicator;
        mUnSelectIndicator = unSelectIndicator;
    }

    protected void setTotalPagers(int pagers) {
        if (pagers < 0) {
            return;
        } else if (pagers == 0) { //不足一页算一页
            mTotalPagers = 1;
        } else {
            mTotalPagers = pagers;
        }
        mPagerIndex.removeAllViews();
        mIndexPoints = new ImageView[mTotalPagers];
        for (int i = 0; i < pagers; i++) {
            mIndexPoints[i] = new ImageView(getContext());
            if (i == mCurPager) {
                mIndexPoints[i].setImageResource(mSelectIndicator);
            } else {
                mIndexPoints[i].setImageResource(mUnSelectIndicator);
            }
            mIndexPoints[i].setPadding(3, 3, 3, 3);
            mPagerIndex.addView(mIndexPoints[i]);
        }
    }

    protected void turnIntoLoading() {
        /**
         * .......
         */
    }

    protected void turnIntoLoadFinish() {

    }


}
