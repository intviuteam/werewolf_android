package com.androidtoolkit.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class FloatView extends View {

    private static final long DURATION_CLICK = 200;
    private OnClickListener mClickListener;

    private long mTouchTime;
    private int mLastX;
    private int mLastY;

    private View mParent;

    private int mHeight = 0;
    private int mWidht = 0;
    private WindowManager manager;
    private int mScreenWidth = 0;
    private int mScreenHeight = 0;

    public FloatView(Context context, AttributeSet attrs) {
        super(context, attrs);
        manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    public void setParent(View parent) {
        mParent = parent;
    }

    public void initHeightAndWidth() {
        DisplayMetrics dm = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            mHeight = Math.abs(bottom - top);
            mWidht = Math.abs(right - left);
        }
        Log.e("FloatView", "mHeight=" + mHeight + ",mWidth=" + mWidht);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastX = (int) event.getRawX();
                mLastY = (int) event.getRawY();
                mTouchTime = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_MOVE:
                initHeightAndWidth();
                if (mParent == null) {
                    break;
                }
                int dx = (int) event.getRawX() - mLastX;
                int dy = (int) event.getRawY() - mLastY;
                int top = mParent.getTop() + dy;
                int left = mParent.getLeft() + dx;
                Log.e("FloatView", "Top=" + top + ",Left=" + left);
                Log.e("FloatView", "mScreenHeight=" + mScreenHeight + ",mScreenWidth=" + mScreenWidth);
                if (top <= 0) {
                    top = 0;
                }
                if (left <= 0) {
                    left = 0;
                }
                if (top + mHeight >= mScreenHeight) {
                    top = mScreenHeight - mHeight;
                }
                if (left + mWidht >= mScreenWidth) {
                    left = mScreenWidth - mWidht;
                }
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mParent.getLayoutParams();
                params.setMargins(left, top, 0, 0);
                mParent.setLayoutParams(params);
                mLastX = (int) event.getRawX();
                mLastY = (int) event.getRawY();
                break;
            case MotionEvent.ACTION_UP:
                if (mClickListener != null && System.currentTimeMillis() - mTouchTime < DURATION_CLICK) {
                    mClickListener.onClick(this);
                }
                break;
        }
        return true;
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        this.mClickListener = l;
    }

}