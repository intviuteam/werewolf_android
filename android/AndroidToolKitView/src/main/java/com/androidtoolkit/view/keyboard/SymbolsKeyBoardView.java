package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.androidtoolkit.view.MaterialButton;
import cn.intviu.widget.R;

/**
 * Created by Mystery on 16/9/10.
 */
public class SymbolsKeyBoardView extends BasicKeyBoardView {

    private Context mContext;
    private static final String SYMBOLSPAGES = "()$&@-/:;.,\"?!`{}[]%^*+=_#\\|~<>·?!'";
    private List<MaterialButton> mSymbols = new ArrayList<MaterialButton>();

    public SymbolsKeyBoardView(ViewGroup mMainKeyBoardView, IKeyBoardInputListener mListener) {
        super(mMainKeyBoardView, mListener);
        this.mContext = mMainKeyBoardView.getContext();
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        mKeyBoardView = linearLayout;
        initKeyBoardView();
    }

    private void initKeyBoardView() {
        View view = View.inflate(mKeyBoardView.getContext(), R.layout.layout_symbols_keyboard, null);
        initViews(view);
        initData();
        mKeyBoardView.addView(view, KeyBoardUtil.getLinearLayoutParams());
    }

    private void initData() {
        for (int i = 0; i < mSymbols.size(); i++) {
            MaterialButton temp = mSymbols.get(i);
            String value = SYMBOLSPAGES.substring(i, i + 1);
            temp.setText(value);
            temp.setTag(value);
            temp.setOnClickListener(this);
        }
    }

    private void initViews(View view) {
        MaterialButton imageView;
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_1));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_2));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_3));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_4));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_5));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_6));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_7));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_8));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_9));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_10));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_11));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_12));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_13));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_14));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_15));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_16));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_17));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_18));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_19));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_20));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_21));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_22));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_23));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_24));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_25));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_26));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_27));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_28));
//        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_29));
//        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_30));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_31));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_32));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_33));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_34));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_35));
        mSymbols.add((MaterialButton) view.findViewById(R.id.symbols_36));

        MaterialButton button = (MaterialButton) view.findViewById(R.id.keyboard_alphabet);
        button.setTag(KeyEvent.KEYCODE_SHIFT_LEFT);
        button.setOnClickListener(this);

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_delete);
        imageView.setTag(KeyEvent.KEYCODE_DEL);
        imageView.setOnClickListener(this);
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.deleteAll();
                return true;
            }
        });

        imageView = (MaterialButton) view.findViewById(R.id.keyboard_done);
        imageView.setTag(KeyEvent.KEYCODE_ENTER);
        imageView.setOnClickListener(this);

        button = (MaterialButton) view.findViewById(R.id.keyboard_hide);
        button.setTag(KEYCODE_CANCEL);
        button.setOnClickListener(this);

    }

    @Override
    public boolean handleClick(View view, int code) {
        if (!super.handleClick(view, code)) {
            switch (code) {
                case KeyEvent.KEYCODE_SHIFT_LEFT:
                    mListener.switchType(ALPHABET_KEYBOARD);
                    return true;
                default:
                    return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void release() {

    }

    @Override
    public void originalState() {
        initData();
    }
}
