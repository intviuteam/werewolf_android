package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.androidtoolkit.view.MaterialButton;
import cn.intviu.widget.R;

/**
 * Created by Mystery on 16/9/10.
 */
public class NumberKeyBoardView extends BasicKeyBoardView {

    private Context mContext;

    public NumberKeyBoardView(ViewGroup mMainKeyBoardView, IKeyBoardInputListener mListener) {
        super(mMainKeyBoardView, mListener);
        mContext = mMainKeyBoardView.getContext();
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        mKeyBoardView = linearLayout;
        initKeyBoardView();
    }

    private void initKeyBoardView() {
        View view = View.inflate(mKeyBoardView.getContext(), R.layout.layout_number_keyboard, null);
        MaterialButton button;
        button = (MaterialButton) view.findViewById(R.id.keyboard_0);
        button.setTag("0");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_1);
        button.setTag("1");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_2);
        button.setTag("2");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_3);
        button.setTag("3");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_4);
        button.setTag("4");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_5);
        button.setTag("5");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_6);
        button.setTag("6");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_7);
        button.setTag("7");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_8);
        button.setTag("8");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_9);
        button.setTag("9");
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_abc);
        button.setTag(KeyEvent.KEYCODE_SYM);
        button.setOnClickListener(this);
        button = (MaterialButton) view.findViewById(R.id.keyboard_hide);
        button.setTag(KEYCODE_CANCEL);
        button.setOnClickListener(this);
        MaterialButton imageButton = (MaterialButton) view.findViewById(R.id.keyboard_delete);
        imageButton.setTag(KeyEvent.KEYCODE_DEL);
        imageButton.setOnClickListener(this);
        imageButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.deleteAll();
                return true;
            }
        });

        imageButton = (MaterialButton) view.findViewById(R.id.keyboard_done);
        imageButton.setTag(KeyEvent.KEYCODE_ENTER);
        imageButton.setOnClickListener(this);

        mKeyBoardView.addView(view, KeyBoardUtil.getLinearLayoutParams());
    }

    @Override
    public boolean handleClick(View view, int code) {
        if (!super.handleClick(view, code)) {
            switch (code) {
                case KeyEvent.KEYCODE_SYM:
                    mListener.switchType(ALPHABET_KEYBOARD);
                    return true;
                case KeyEvent.KEYCODE_DEL:
                    mListener.delete();
                    return true;
                default:
                    return false;
            }

        } else {
            return true;
        }
    }

    @Override
    public void release() {

    }

    @Override
    public void originalState() {

    }
}
