package com.androidtoolkit.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

import cn.intviu.widget.R;

/**
 * 自定义View: 快速索引
 *
 * @author 王旭
 */
public class QuickIndexView extends View {
    private float itemWidth;
    private float itemHeight;
    private ArrayList<String> indexArr = new ArrayList<String>();
    private Paint paint;// 定义画笔
    private int textColor = getResources().getColor(R.color.quick_index);
    private int textSize = ScreenUtils.dip2px(14);
    private int textColorTouch = Color.LTGRAY;
    private int textSizeTouch = ScreenUtils.dip2px(14);
    private int start;

    public QuickIndexView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 初始化画笔
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        paint.setAntiAlias(true);

    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public void setTextColorTouch(int textColorTouch) {
        this.textColorTouch = textColorTouch;
    }

    public void setTextSizeTouch(int textSizeTouch) {
        this.textSizeTouch = textSizeTouch;
    }

    public void setItemHeight(float height) {
        itemHeight = height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        // 给item的宽度和高度赋值
        itemWidth = getMeasuredWidth();
        itemHeight = (itemHeight != 0) ? itemHeight : getResources().getDimension(R.dimen.quick_index_view_item_height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);// 父类的onDraw没有任何实现
        int singleHeight = (int) itemHeight;
        int l = indexArr.size();
        int height = getMeasuredHeight();
        int dis = ((height - (singleHeight * l)) / 2);
        if (dis > 0) {
            start = dis;
        } else {
            start = 0;
        }
        for (int i = 0; i < indexArr.size(); i++) {
            String word = indexArr.get(i);
            // 设置文本的颜色
            if (i == touchIndex) {
                paint.setColor(textColorTouch);
                paint.setTextSize(textSizeTouch);
            } else {
                paint.setColor(textColor);
                paint.setTextSize(textSize);
            }
            // 设置文字的宽高
            Rect bounds = new Rect();
            paint.getTextBounds(word, 0, word.length(), bounds);
            // 得到word的宽高
            int wordWidth = bounds.width();
            int wordHeight = bounds.height();
            // 计算word左下角的坐标
            float x = itemWidth / 2 - wordWidth / 2;
            float y = start + itemHeight / 2 + wordHeight / 2 + i * itemHeight;
            // 绘制word
            canvas.drawText(word, x, y, paint);

        }
    }

    private int touchIndex = -1;// 设置
    private OnIndexChangedListener onIndexChangedListener;

    /**
     *
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 得到事件的坐标
        float eventY = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                // 计算下标
                int index = (int) ((eventY - start) / itemHeight);
                // 强制重绘
                if (touchIndex != index) {
                    // 更新touchIndex
                    touchIndex = index;
                    // 强制重绘
                    invalidate();
                    //通知Activity更新TextView
                    if (onIndexChangedListener != null && indexArr.size() > 0) {
                        if (index >= indexArr.size() - 1) {
                            index = indexArr.size() - 1;
                        } else if (index <= 0) {
                            index = 0;
                        }
                        onIndexChangedListener.onIndexChanged(indexArr.get(index));
                    }
                }

                break;
            case MotionEvent.ACTION_UP:
                touchIndex = -1;
                // 强制重绘
                invalidate();
                //通知Activity更新TextView
                if (onIndexChangedListener != null) {
                    onIndexChangedListener.onUp();
                }
                break;

            default:
                break;
        }
        return true;// 让item自己消费
    }


    public void setIndexArr(ArrayList<String> quickIndex) {
        indexArr.clear();
        indexArr.addAll(quickIndex);
        postInvalidate();
    }

    /*
     * 设置监听对象的方法 这个方法一般是Activity调用
     */
    public void setOnIndexChangedListener(
            OnIndexChangedListener onIndexChangedListener) {
        this.onIndexChangedListener = onIndexChangedListener;
    }

    public interface OnIndexChangedListener {
        // 当操作的下标改变时自动调用
        public void onIndexChanged(String word);

        // 当up时调用
        public void onUp();
    }

    public float getTotalHeight() {
        return itemHeight * indexArr.size();
    }

}
