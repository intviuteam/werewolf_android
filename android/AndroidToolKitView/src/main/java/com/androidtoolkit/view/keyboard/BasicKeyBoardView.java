package com.androidtoolkit.view.keyboard;

import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Mystery on 16/9/9.
 */
public abstract class BasicKeyBoardView implements View.OnClickListener, CustomKeyboardContants {

    protected ViewGroup mKeyBoardView;
    protected ViewGroup mMainKeyBoardView;
    protected IKeyBoardInputListener mListener;

    public BasicKeyBoardView(ViewGroup mMainKeyBoardView, IKeyBoardInputListener mListener) {
        this.mMainKeyBoardView = mMainKeyBoardView;
        this.mListener = mListener;
    }

    public ViewGroup getKeyBoardView() {
        return mKeyBoardView;
    }

    public ViewGroup getMainKeyBoardView() {
        return mMainKeyBoardView;
    }

    @Override
    public void onClick(View view) {
        Object object = view.getTag();
        if (object instanceof String) {
            mListener.input(((String) object).charAt(0));
        } else if (object instanceof Integer) {
            handleClick(view, (Integer) object);
        }
    }

    public boolean handleClick(View view, int code) {
        switch (code) {
            case KeyEvent.KEYCODE_SPACE:
                mListener.input(' ');
                return true;
            case KeyEvent.KEYCODE_ENTER:
                mListener.done();
                return true;
            case KeyEvent.KEYCODE_DEL:
                mListener.delete();
                return true;
            case KEYCODE_CANCEL:
                mListener.cancel();
            default:
                return false;
        }
    }

    public abstract void release();

    public abstract void originalState();
}
