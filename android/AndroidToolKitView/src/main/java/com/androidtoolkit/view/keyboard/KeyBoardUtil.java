package com.androidtoolkit.view.keyboard;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.lang.reflect.Method;

import com.androidtoolkit.view.MaterialButton;

/**
 * Created by Mystery on 16/9/9.
 */
public class KeyBoardUtil {

    private static Context context;
    private static double screenSize;
    private static float screenDensity;
    private static int screenWidth;
    private static int screenHeight;
    private static Handler mMainHandler;
    private static int keyBoardHeight;
    private static float keyBoardScale = 0.4f;
    private static IKeyBoard mKeyBoard = null;

    public static void initialization(Context c) {
        mMainHandler = new Handler(Looper.getMainLooper());
        context = c;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);
        screenSize = Math.sqrt(dm.widthPixels * dm.widthPixels + dm.heightPixels * dm.heightPixels) / (dm.density * 160);
        screenDensity = dm.density;
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;

        if (screenWidth > screenHeight) {
            int temp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = temp;
        }
        keyBoardHeight = (int) (screenHeight * keyBoardScale);
        Log.e("Screen", screenWidth + "," + screenHeight);
    }

    public static Context getContext() {
        return context;
    }

    public static void execRunable(Runnable runnable) {
        mMainHandler.post(runnable);
    }

    public static void execRunnableDelay(Runnable runnable, long t) {
        mMainHandler.postDelayed(runnable, t);
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static int dip2px(float dip) {
        return (int) (dip * screenDensity + 0.5f);
    }

    public static int sp2px(float sp) {
        return (int) (sp * screenDensity + 0.5f);
    }

    public static LinearLayout.LayoutParams getButtonLayoutParams(Button button) {
        return (LinearLayout.LayoutParams) button.getLayoutParams();
    }

    public static LinearLayout.LayoutParams getButtonLayoutParams(MaterialButton button) {
        return (LinearLayout.LayoutParams) button.getLayoutParams();
    }

    public static LinearLayout.LayoutParams getRowLinearLayout() {
        return new LinearLayout.LayoutParams(screenWidth, (int) ((keyBoardHeight - dip2px(8)) / 4));
    }

    public static LinearLayout.LayoutParams getRowLinearLayout(int height) {
        return new LinearLayout.LayoutParams(screenWidth, height);
    }

    public static LinearLayout.LayoutParams getLinearLayoutParams() {
        return new LinearLayout.LayoutParams(screenWidth, keyBoardHeight);
    }

    public static int getKeyBoardHeight() {
        return keyBoardHeight;
    }

    public static float getKeyBoardScale() {
        return keyBoardScale;
    }

    public static void disableEditTextShowSoftInput(EditText editText) {
        if (android.os.Build.VERSION.SDK_INT <= 10) {
            editText.setInputType(InputType.TYPE_NULL);
        } else {
            Class<EditText> cls = EditText.class;
            Method method;
            try {
                method = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                method = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
                method.setAccessible(true);
                method.invoke(editText, false);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    public static void setKeyBoard(IKeyBoard keyBoard) {
        mKeyBoard = keyBoard;
    }

    public static void hideKeyBoard() {
        if (mKeyBoard != null) {
            if (mKeyBoard.isShowing()) {
                mKeyBoard.hide();
            }
        }
    }

    public static boolean keyBoardIsShown() {
        if (mKeyBoard != null) {
            return mKeyBoard.isShowing();
        } else {
            return false;
        }
    }

    public static void showKeyBoard(EditText editText) {
        if (mKeyBoard != null) {
            if (!mKeyBoard.isShowing()) {
                mKeyBoard.show(editText);
            }
        }
    }

}
