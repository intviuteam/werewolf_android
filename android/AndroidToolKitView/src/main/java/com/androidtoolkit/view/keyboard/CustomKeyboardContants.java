package com.androidtoolkit.view.keyboard;

/**
 * Created by Mystery on 16/9/9.
 */
public interface CustomKeyboardContants {

    int ALPHABET_KEYBOARD = 0;

    int FULLSYMBOL_KEYBOARD = 1;

    int SMALLNUMBER_KEYBOARD = 2;

    int NUMBER_KEYBOARD = 3;

    int KEYCODE_CANCEL = 4;

    String[] ALPHABETPAGES = new String[]{"qwertyuiopasdfghjklzxcvbnm", "QWERTYUIOPASDFGHJKLZXCVBNM"};

    String[] SYMBOLSPAGES = new String[]{"1234567890()$&@-/:;.,\"?!`", "{}[]%^*+=_#\\|~<>€£¥·.,?!'"};

    String[] NUMBERPAGES = new String[]{"1234567890"};
}
