package com.androidtoolkit.view.viewpagergroup;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class ViewPagerGroupAdapter<T extends View> extends PagerAdapter {

    private List<T> items;

    public ViewPagerGroupAdapter() {

    }


    public void setData(List<T> data)
    {
        this.items=data;
        notifyDataSetChanged();
    }

    public void removeLastPage() {
        if (items.size() > 1) {
            items.remove(items.size() - 1);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(items.get(position));
        return items.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}

