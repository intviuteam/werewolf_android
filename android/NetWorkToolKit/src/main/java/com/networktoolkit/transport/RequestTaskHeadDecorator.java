package com.networktoolkit.transport;

import okhttp3.Request;

/**
 * game_werewolf
 * 2018/6/4 下午2:06
 * Mystery
 */
public interface RequestTaskHeadDecorator {

    Request.Builder decorateHead(Request.Builder builder);
}
