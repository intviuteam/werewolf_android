package com.networktoolkit.transport;

import android.text.TextUtils;

/**
 * game_werewolf
 * 2017/9/18 上午11:48
 * Mystery
 */

public class URLManager {

    private static final String EMPTY_INTENT = "";

    private static volatile String curIntent = EMPTY_INTENT;

    private static String getCurIntent() {
        return curIntent;
    }

    public static void updateCurIntent(String curIntent) {
        URLManager.curIntent = curIntent;
    }

    public static String GetRequestURL(String intent) {
        if (TextUtils.isEmpty(intent)) {
            return Constant.HTTP_URL;
        }
        switch (intent) {
            case Constant.URL_TYPE_AUDIO:
                return Constant.AUDIO_HTTP_URL;
            case Constant.URL_TYPE_DEFAULT:
                return Constant.HTTP_URL;
            default:
                break;
        }
        return Constant.HTTP_URL;
    }

    public static String GetDefaultURL() {
        return GetRequestURL(curIntent);
    }

    public static String GetBlackListURL(String userID) {
        return "/block/add/" + userID;
    }

    public static String GetStandingsURL(String token) {
        return "/game_record?access_token=" + token + TransportConfig.getUrlAttrs1();
    }

    public static String GetDisConnectInfoURL() {
        return "/server/disconnectInfo";
    }

    public static String GetEscapeURL() {
        return "/server/escape";
    }


}
