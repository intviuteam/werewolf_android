package com.networktoolkit.transport;

import android.content.Context;
import android.text.TextUtils;

import cn.intviu.support.SharePreStoreUtils;

/**
 * Author:1727
 * 2017/3/6
 */

public class Constant {

    public static final String REQUEST_NAME = "REQUEST_NAME";
    public static final String REQUEST_URL_KEY = "REQUEST_URL";
    public static final String REQUEST_AUDIO_URL_KEY = "REQUEST_AUDIO_URL";

    public static String HTTP_URL = "";

    public static void setHttpUrl(String httpUrl) {
        HTTP_URL = httpUrl;
        AUDIO_HTTP_URL = HTTP_URL;
    }

    public static String AUDIO_HTTP_URL = HTTP_URL;

    public static final String URL_TYPE_AUDIO = "type_audio";
    public static final String URL_TYPE_DEFAULT = "type_default";
    public static final String URL_VOICE_EMOTIONS = "/config/audio_emoticon";
    public static final String PUBLIC_SHARE_CASE = "/share_case/get_share_case_from_client_V2?type=werewolf&version=";  //获取分享文案的接口
    public static final String PUBLIC_SHARE_CASE_CN = "/share_case/get_share_case_from_client_V2?type=kuaiwan&version=";  //获取分享文案的接口

    //语音房分享
    public static String SHARE_AUDIO_URL = "/share/?roomid=%s&type=audio&password=%s";

    public static void init(Context context) {
        String tempurl = SharePreStoreUtils.getString(REQUEST_NAME, context, REQUEST_URL_KEY);
        if (TextUtils.isEmpty(tempurl)) {
            return;
        }
        HTTP_URL = tempurl;
        String tempAudioUrl = SharePreStoreUtils.getString(REQUEST_NAME, context, REQUEST_AUDIO_URL_KEY);
        if (TextUtils.isEmpty(tempAudioUrl)) {
            return;
        }
        AUDIO_HTTP_URL = tempAudioUrl;
    }

    public static final int SECONDS = 1000;

    public static final int CODE_1000 = 1000;//正常重连
    public static final int CODE_1032 = 1032;//断网期间被踢出重连返回此code


    public static final String EMPTY = "";
    public static final String CODE = "code";
    public static final String DATA = "data";
    public static final String ADD_FRIEND_REQUEST = "/friend/add";
    public static final String USER_DETAIL_REQUEST = "/user/info";
    public static final String URI_APP_UPDATE = "/upgrade";
    public static final String URI_TASK_DO = "/task/do";
    public static final String GET_CARD_LIST = "/wealth/unusedCards";
    public static final String REPORT_URL = "/account/report";
    public static final String GOOGLE_PAY = "/pay/googlePay";
    public static final String GOOGLE_SUBS_PAY = "/vip/werewolf/active_android";
    public static final String IS_NOPLAY = "/isnoplay";
    public static final String FRIEND_DELETE = "/friend/delete";
    public static final String ACCEPT_FRIEND = "/friend/accept?friend_id=";
    public static final String REJECT_FRIEND = "/friend/reject?friend_id=";
    public static final String FACEBOO_ADV_CONFIG = "/config/gg?type=";

    public static final String GAME_TYPE_SIMPLE = "simple";                 //simple局目前未知是否可以使用
    public static final String GAME_TYPE_PRIVACY = "privacy";               //私密房
    public static final String GAME_TYPE_NORMAL = "normal";                 //12人爱神局
    public static final String GAME_TYPE_PRE_SIMPLE = "pre_simple";         //6-10人新手局
    public static final String GAME_TYPE_PRE_SIMPLE_NEW = "pre_simple_new"; //6人猎人局
    public static final String GAME_TYPE_AUDIO = "audio";                   //语音房
    public static final String GAME_TYPE_SIMPLE_6 = "simple_6";             //6人守卫局
    public static final String GAME_TYPE_SIMPLE_9 = "simple_9";             //9人屠边局
    public static final String GAME_TYPE_SIMPLE_10 = "simple_10";           //10人屠城局
    public static final String GAME_TYPE_NORMAL_GUARD = "normal_guard";     //12人守卫局
    public static final String GAME_TYPE_HIGH_KING = "high_king";           //12人高级场(白狼王局)
    public static final String GAME_TYPE_SPY = "undercover";                //谁是卧底房
    public static final String GAME_TYPE_PAIR = "pair";                     //游戏大厅
    public static final String GAME_TYPE_KTV = "ktv";                       //ktv房

    public static final String FRIEND_ID = "friend_id";


    public static final String ALIVES = "alives";
    public static final String TYPE_ENTER = "enter";
    public static final String TYPE_ENTER_SERVER = "enter_server";
    public static final String TYPE_JOIN = "join";
    public static final String TYPE_UPDATE_MASTER = "update_master";
    public static final String TYPE_PREPARE = "prepare";
    public static final String TYPE_UNPREPARE = "unprepare";
    public static final String ID = "id";
    public static final String DURATION = "duration";
    public static final String START = "start";
    public static final String PREPARE_TIME = "prepare_time";
    public static final String KILL_INFO = "kill_info";
    public static final String DEATH = "death";
    public static final String POSITIONS = "positions";
    public static final String PREPARE = "prepare";
    public static final String UNPREPARE = "unprepare";
    public static final String NAME_LOWER = "name";
    public static final String AVATAR_LOWER = "avatar";
    public static final String SPEECH_DIRECTION = "speech_direction";
    public static final String DIRECTION = "direction";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";

    public static final String SIGNATURE = "signature";
    public static final String SPEAK = "speak";
    public static final String UNSPEAK = "unspeak";
    public static final String POSITION = "position";
    public static final String DEST_POSITION = "dest_position";
    public static final String KILL = "kill";

    public static final String CHECKRESULT = "check_result";
    public static final String ROLE = "role";
    public static final String CARD_TYPE = "card_type";
    public static final String CHECK = "check";
    public static final String DEATHINFO = "death_info";

    public static final String ROLE_WOLF = "werewolf";  //狼人
    public static final String ROLE_PEOPLE = "people";  //村民
    public static final String ROLE_SEER = "seer";      //预言家
    public static final String ROLE_WITCH = "witch";    //女巫
    public static final String ROLE_HUNTER = "hunter";  //猎人
    public static final String ROLE_CUPID = "cupid";    //爱神
    public static final String ROLE_GUARD = "guard";    //守卫
    public static final String ROLE_WEREWOLF_KING = "werewolf_king"; //白狼王

    public static final String ASSIGNED_ROLE = "assigned_role";
    public static final String WAKE_TO_KILL = "wake_to_kill";
    public static final String SEER_CHECK = "seer_check";
    public static final String VOTE_RESULT = "vote_result";

    public static final String POISON = "poison";
    public static final String POISON_RESULT = "poison_result";
    public static final String SAVE = "save";
    public static final String SAVE_RESULT = "save_result";
    public static final String TAKEAWAY = "take_away";

    public static final String SPEECH = "speech";
    public static final String SELF_SPEECH = "SELF_SPEECH";
    public static final String SELF_SPEECH_DANGER = "SELF_SPEECH_DANGER";
    public static final String VOTE = "vote";

    public static final String SUNSET = "sunset";
    public static final String SUNSET_WITHOUT_SEER = "SUNSET_WITHOUT_SEER";
    public static final String SUNSET_ONLY_SEER = "SUNSET_ONLY_SEER";
    public static final String SUNSET_WITH_SEER = "SUNSET_WITH_SEER";
    public static final String SUNSET_NORMAL_PEOPLE = "SUNSET_NORMAL_PEOPLE";
    public static final String SUNUP = "sunup";
    public static final String SUNUP_WITH_SHERIFF = "sunup_sheriff";
    public static final String GAMEOVER_PEOPLE_WIN = "GAMEOVER_PEOPLE_WIN";
    public static final String GAMEOVER_WOLF_WIN = "GAMEOVER_WOLF_WIN";
    public static final String GAMEOVER_CUPID_WIN = "GAMEOVER_CUPID_WIN";
    public static final String SPEECH_DANGER_TIME = "SPEECH_DANGER_TIME";
    public static final String STOP_MEDIA = "STOP_MEDIA";

    public static final String ENDSPEECH = "end_speech";
    public static final String GAMEOVER = "game_over";
    public static final String DAYINDEX = "day_index";

    public static final String TYPE_VOTE_RESULT_DEATH = "death";
    public static final String TYPE_VOTE_RESULT_NO_DEATH = "no_death";
    public static final String TYPE_VOTE_RESULT_EQUAL = "equal";

    public static final String WIN_PEOPELE = "people";
    public static final String WIN_Wolf = "werewolf";
    public static final String WIN_THIRD = "third_party";

    public static final String STATE_VOTED = "voted";
    public static final String STATE_KILLED = "killed";
    public static final String STATE_ALIVE = "alive";
    public static final String STATE_POISON = "poisoned";
    public static final String STATE_TOKEN_AWAY = "token_away";
    public static final String STATE_LINKED = "linked";
    public static final String STATE_LEAVED = "leaved";
    public static final String STATE_BOOM = "boom";
    public static final String STATE_BOOM_AWAY = "boom_away";

    public static final String SYSTEM_MSG = "system_msg";
    public static final String CONTENT = "content";
    public static final String RESET_MASTER = "reset_master";

    public static final String LEAVE = "leave";
    public static final String REMOVE = "remove";
    public static final String DEAD = "dead";

    public static final String KILL_RESULT = "kill_result";
    public static final String TYPE_CHAT = "chat";
    public static final String MESSAGE = "message";
    public static final String TAKE_AWAY_RESULT = "take_away_result";

    public static final String MUTE_ALL = "mute_all";
    public static final String UNMUTE_ALL = "unmute_all";

    public static final String APPLY_ROLE = "apply_role";
    public static final String APPLY_ROLE_RESULT = "apply_role_result";
    public static final String NOT_ENOUGH_CARD = "not_enough_card";
    public static final String NOT_ENOUGH_ROLES = "not_enough_roles";
    public static final String ILLEGAL_STATE = "illegal_state";
    public static final String FAILED = "failed";
    public static final String NONE = "none";
    public static final String SUCCESS = "success";
    public static final String CARD_CHECK = "card_check";
    public static final String CARD_CHECK_RESULT = "card_check_result";
    public static final String APPEND_TIME = "append_time";
    public static final String APPEND_TIME_RESULT = "append_time_result";
    public static final String LEFT_TIME = "left_time";
    public static final String APPEND_DURATION = "append_duration";
    public static final String TYPE_ADD_FRIEND = "add_friend";

    /**
     * 标准模式
     */
    public static final String LINK = "link";
    public static final String SKIPED = "skiped";
    public static final String LINK_RESULT = "link_result";
    public static final String GIVE_UP_APPLY = "give_up_apply";
    public static final String APPLY = "apply";
    public static final String APPLY_RESULT = "apply_result";
    public static final String SHERIFF_RESULT = "sheriff_result";
    public static final String HAND_OVER = "hand_over";
    public static final String VOTE_TYPE = "type";
    public static final String VOTE_TYPE_APPLY = "apply";
    public static final String VOTE_TYPE_KILL = "kill";
    public static final String HAND_OVER_RESULT = "hand_over_result";
    public static final String LOCK = "lock";
    public static final String UNLOCK = "unlock";
    public static final String KICK_OUT = "kick_out";
    public static final String TO = "to";
    public static final String FROM = "from";
    public static final String PING = "ping";
    public static final String MSG_ID = "msg_id";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String PASSWORD = "password";
    public static final String RESTORE_ROOM = "restore_room";
    public static final String VOICE_TITLE = "title";
    public static final String VOICE_TITLE_TYPE = "type";
    public static final String UPDATE_TITLE = "update_title";
    public static final String TITLE = "title";

    public static final String FORCE_SEAT = "force_seat";
    public static final String UP_SEAT = "up_seat";
    public static final String DOWN_SEAT = "down_seat";
    public static final String HAND_OVER_MASTER = "handover_master";//语音房转让房主
    public static final int MASTER_POSITION = 0;//房主座位号
    public static final int MAX_CHAIR = 8;//最后一个麦号
    public static final int OVER_POSITION = 10086;//退出房间就视为去到这个位置
    public static final String LIKE_ROOM = "like_room";
    public static final int TEMPORARY_POSITION = -101;//上麦前不下麦的临时麦位
    public static final String CHANGE_USER_STATE = "change_user_state";//语音房更新用户状态
    public static final String SHOW_EMOTION = "show_emoticon";//语音房发表情
    public static final String MODEL = "state";
    public static final String MARK = "mark";
    public static final String URL = "url";
    public static final String THUMBNAIL = "thumbnail";
    public static final String SHOW_GAME = "show_game_emoticon";
    public static final String STAY_TIME = "stay_time";
    public static final String SOUND = "sound";
    public static final String RES = "res";
    public static final int NO_RES = -1;
    public static final String MSG = "msg";
    public static final int BALANCE_NO_ENOUGH = 1039;//余额不足
    public static final int NO_RECOGNIZED = 1020;//无法识别
    public static final String CHILD_TYPE = "child_type";
    public static final String SOUND_APPLAUSE = "play_applause";//鼓掌音乐id
    public static final String VOICE_BLACK_LIST = "block_user_list";
    public static final String ADD_BLACK = "block";
    public static final String UN_BLACK = "unblock_user";
    public static final String USERS = "users";

    public static final String LIMIT = "limit";
    public static final String FREE = "free";
    public static final String SPACE_STR = "";
    public static final String BACKGROUND = "background";
    public static final String THEME = "theme";
    public static final String VOICE_COUNT = "count";


    public static final String USER_ID = "USER_ID";
    public static final String USER_ID_LOWER = "user_id";
    public static final String USER_ID_CAMEL = "userId";
    public static final String UID = "uid";
    public static final String EVENT_USER_MAIN_PAGE = "EVENT_USER_MAIN_PAGE";
    public static final String EVENT_USER_LEAVE = "EVENT_USER_LEAVE";
    public static final String EVENT_USER_START = "EVENT_USER_START";
    public static final String EVENT_USER_LEAVE_FROM_WEB = "EVENT_USER_LEAVE_FROM_WEB";
    public static final String GAME_LEVEL = "level";

    public static final String PROTECT = "protect";
    public static final String PROTECT_RESULT = "protect_result";
    public static final String BOOM = "boom";
    public static final String BOOM_AWAY = "boom_away";
    public static final String BOOM_AWAY_RESULT = "boom_away_result";
    public static final String BOOM_STATE = "boom_state";
    public static final String BOOM_ABLE = "can_boom";
    public static final String LAST_PROTECTED = "last_protected";

    /**
     * 应用更新
     */
    public static final String CHECK_UPGRADE = "check_upgrade";
    public static final String DOWNLOAD_APK = "download_apk";
    public static final String INITIATIVE_CHECK = "initiative_check";

    public static final String UNACTIVE_WARNING = "unactive_warning";
    public static final String ACTION_CONTINUE = "continue";

    /**
     * 分享页面页面跳转进应用
     */
    public static final String ROOM_CODE = "room_code";
    public static final String ROOM_PASSWORD = "password";
    public static final String EVENT_START_BROWSABLE = "EVENT_START_BROWSABLE";
    public static final String EVENT_CHAT = "EVENT_CHAT"; //聊天事件
    public static final String EVENT_LEAVE_CHAT = "LEAVE_EVENT_CHAT"; //离开聊天事件
    public static final String USER_NAME = "USER_NAME";  //用户名
    public static final String USER_ICON = "USER_ICON";  //用户头像
    public static final String IS_FRIEND = "IS_FRIEND";//是否是好友
    public static final String USER_SEX = "USER_SEX";    //用户性别
    public static final String USER_CHAT_TYPE = "USER_CHAT_TYPE"; //消息类型
    public static final String CHAT_TIME = "CHAT_TIME";  //消息时间
    public static final String CHAT_MESSAGE = "CHAT_MESSAGE";//聊天的消息
    public static final String PARAMETER_USER_ID = "user_id";
    public static final String PARAMETER_ROOM_ID = "room_id";
    public static final String PARAMETER_TITLE = "title";
    public static final String PARAMETER_FRIEND_ID = "friend_id";
    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";
    public static final String MESSAGE_TYPE_CHAT = "MESSAGE_TYPE_CHAT";
    public static final int RX_BACK_PRESSURE = 500;//rx 背压

    /**
     * 兼容IOS添加的消息字段
     */
    public static final String CONVERSATION_TYPE = "conversationType";//
    public static final String USERNAME = "username";//
    public static final String ACTION_ROBOT = "action_Robot";
    public static final int ACTION_CHECK_TIME = 35;
    public static final int DANGEROUS_TIME = 5;

    /**
     * 日常任务分享／邀请
     */
    public static final String TYPE = "type";
    public static final String COUNT = "count";
    public static final String SHARE_TO_WEIXIN = "share_to_weixin_qq";
    public static final String SHARE_TO_WEIXIN_CIRCLE = "share_to_wxwall";
    public static final String SHARE_TO_QQ = "share_to_weixin_qq";
    public static final String INVITE_TO_QQ = "invite";
    public static final String INVITE_TO_WEIXIN = "invite";
    public static final String INVITE_TO_WEIXIN_CIRCLE = "invite";
    public static final String INVITE_TO_FRIEND = "invite";
    public static final String INVITE = "invite";
    public static final String SHARE = "share";


    public static final String GIFT_FROM = "from";
    public static final String GIFT_TO = "to";
    public static final String GIFT_TYPE = "type";
    public static final String GIFT_CHAT = "gift";
    public static final String GIFT_TICKET = "ticket";

    /**
     * LeanCloud节点问题
     */
    public static final String LEANCLOUD_NODE = "LEANCLOUD_NODE";
    public static final String LEANCLOUD_NODE_CONFIG = "LEANCLOUD_NODE_CONFIG";
    public static final String LEANCLOUD_CN = "cn";
    public static final String LEANCLOUD_US = "us";
    public static final String HOST = "host";
    public static final String LEANCLOUD_NODE_RN = "LEANCLOUD_NODE_RN";
    public static final String LEANCLOUD_NODE_RN_CONFIG = "LEANCLOUD_NODE_RN_CONFIG";

    public static final String LEANCLOUD_SERVER_API = "LEANCLOUD_SERVER_API";
    public static final String LEANCLOUD_SERVER_RTM = "LEANCLOUD_SERVER_RTM";
    public static final String LEANCLOUD_SERVER_NONE = "None";

    public static final String LEANCLOUD_SERVER_API_DEFAULT = "https://intviu-api.leancloud.cn";
    public static final String LEANCLOUD_SERVER_RTM_DEFAULT = "wss://intviu-rtm.leancloud.cn";

    public static final String UNMUTE_SEND = "unmute_send";
    public static final String OLIVE_CHANGE = "olive_changed";
    public static final String ENGINE_ORBIT = "orbit";
    public static final String ENGINE_GVOICE = "gvoice";

    public static final String CALL_TYPE_GAME = "game";
    public static final String CALL_TYPE_LANGUAGE = "language";
    public static final String CALL_TYPE_JOY = "joy";
    public static final String CALL_TYPE_HIGH_LEVEL = "high_level";
    public static final String EXPORT = "export";
    public static final String EXPORT_PAY = "export_pay";

    /**
     * 权限
     */
    public static final String CONFIG_DATA = "config_data";
    public static final String PREMISSION_KEY = "premission_key";
    /**
     * 广告flag
     */
    public static final String ADV_STATUS = "adv_status";
    public static final String ADV_FAIL_COUNT = "adv_fail_count";
    public static final String ADV_FAIL = "adv_fail";
    public static final String ADV_SUCCESS = "adv_success";

    public static final String REPORT_ESCAPE = "escape";
    public static final String REPORT_AD = "ad";
    public static final String REPORT_SEX = "sex";
    public static final String REPORT_CHEAT = "cheat";
    public static final String REPORT_ABUSE = "abuse";
    public static final String REPORT_GANG_UP = "gang_up";
    public static final String REPORT_TROUBLE = "trouble";

    public static final String SERVER_GET_LOCATION = "lc";
    public static final String CAN_SAVE = "can_save";

    /**
     * 连续登录
     */
    public static final String CONTINUE_LOGIN = "CONTINUE_LOGIN";

    /**
     * 领取奖品
     */
    public static final String MEDIA_RECEIVE_GOLD = "media_receive_gold";
    public static final String OPEN_BOX = "open_box";
    public static final int TYPE_ENTER_GAME_ADV = 1;//进入游戏广告展
    public static final int TYPE_DIE_ADV = 2;//游戏内死亡后广告展示
    public static final int TYPE_AWARD_ADV = 3;//领取奖励后弹出的广告
    public static final int TYPE_GAME_OVER_ADV = 4;//游戏结束时，胜负板上的广告
    public static final int TYPE_BANNER_ADV = 5;//转盘抽奖，悬浮的广告位
    public static final int TYPE_DISK_ADV = 6;//转盘抽奖，领奖的广告

    public static void init() {

    }

    public static final int ERROR_CODE_NOT_ENOUGH_MONEY = 1008;
    public static final int ERROR_CODE_BUY_ERROR = 1001;
    public static final int MAX_RETRY = 6;

    public static final String LEVEL_LIMIT_NONE = "0";
    public static final String LEVEL_LIMIT_15 = "15";
    public static final String LEVEL_LIMIT_30 = "30";
    public static final String LEVEL_LIMIT_50 = "50";

    public static final String MEDIA_SEER_WOLF = "MEDIA_SEER_WOLF";
    public static final String MEDIA_SEER_WITCH_WOLF = "MEDIA_SEER_WITCH_WOLF";
    public static final String MEDIA_SEER_GUARD_WOLF = "MEDIA_SEER_GUARD_WOLF";
    public static final String MEDIA_SEER_GUARD_WITCH_WOLF = "MEDIA_SEER_GUARD_WITCH_WOLF";

    public static final String SPEECH_DIRECTION_RESULT = "speech_direction_result";
    public static final String SYSTEM_MSG_TYPE_DIALOG = "dialog";
    public static final String SYSTEM_MSG_TYPE_TOAST = "toast";
    public static final String SYSTEM_MSG_TYPE_TEXT = "text";

    public static final String UPDATE_CONFIG = "update_config";

    public static final String RESET_MASTER_RESULT = "reset_master_result";
    public static final String REQUEST_RESET_MASTER = "request_reset_master";
    public static final String REJECT_RESET_MASTER = "reject_reset_master";
    public static final String ALLOW = "allow";

    public static final String SOCKET_SEND_GIFT = "send_gift";
    public static final String SOCKET_SEND_CARD = "send_card";
    public static final String SOCKET_SEND_GIFT_PEER_ID = "peer_id";
    public static final String SOCKET_SEND_GIFT_GIFT_TYPE = "gift_type";
    public static final String SOCKET_SEND_GIFT_CARD_TYPE = "card_type";

    public static final String CAN_SPEAK = "can_speak";
    //
    public static final String APP_TYPE_WEREWOLF = "werewolf";
    public static final String APP_TYPE_XIAOYU = "xiaoyu";

    //wwq 2018-02-09 start//
    public static final String GAME_UP_SEAT = "up_seat";//上座
    public static final String GAME_DOWN_SEAT = "down_seat";//去观战

    public static final String MESSAGE_CHANNEL = "channel";//聊天频道
    public static final String MESSAGE_ALL = "all";//玩家消息标识
    public static final String MESSAGE_DEATH = "death";//观战消息标识
    //wwq 2018-02-09 end//

    public static final String UPDATE_BACKGROUND = "update_background";

    public static final String DISPLAY_TYPE = "display_type";  //礼物展现类型
    public static final String DISPLAY_TYPE_SEAT = "seat";  //一般送礼：终点在玩家头像上
    public static final String DISPLAY_TYPE_SING = "sing";  //唱歌送礼：终点在播放器头像上

}
