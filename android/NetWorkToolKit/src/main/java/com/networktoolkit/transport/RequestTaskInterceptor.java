package com.networktoolkit.transport;

public interface RequestTaskInterceptor{

    boolean onInterceptor(String data);

}
