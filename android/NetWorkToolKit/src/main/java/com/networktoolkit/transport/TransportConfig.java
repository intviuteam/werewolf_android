package com.networktoolkit.transport;

import com.androidtoolkit.ApkToolKit;
import com.androidtoolkit.TimeZoneToolKit;
import com.support.BuildConfig;

import java.util.Locale;

import cn.intviu.service.IntviuApplication;
import cn.intviu.support.ConstInfo;

/**
 * game_werewolf
 * 2017/9/14 上午11:14
 * Mystery
 */

public class TransportConfig {
    public static final String SV_KEY = "sv";
    public static final String SV_VALUE = "19";
    public static String APP_TYPE = "";

    //webView url 需要添加app标示 TODO wx
    public static void initAppType(String type) {
        APP_TYPE = type;
    }

    public static final String getUrlAttrs1() {
        StringBuilder sb = new StringBuilder();
        sb.append("&lg=" + Locale.getDefault().getLanguage());
        sb.append("&v=" + Integer.toString(BuildConfig.VERSION_CODE));
        sb.append("&" + TransportConfig.SV_KEY + "=" + TransportConfig.SV_VALUE);
        sb.append("&tz=" + TimeZoneToolKit.GetCurrentTimeZone());
        sb.append("&pt=android");
        sb.append("&sign=" + ApkToolKit.GetApkVerifyStringByMD5());
        sb.append("&app=" + APP_TYPE);
        try {
            sb.append("&did=" + ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static final String getUrlAttrs2() {
        StringBuilder sb = new StringBuilder();
        sb.append("?lg=" + Locale.getDefault().getLanguage());
        sb.append("&v=" + Integer.toString(BuildConfig.VERSION_CODE));
        sb.append("&" + TransportConfig.SV_KEY + "=" + TransportConfig.SV_VALUE);
        sb.append("&tz=" + TimeZoneToolKit.GetCurrentTimeZone());
        sb.append("&pt=android");
        sb.append("&sign=" + ApkToolKit.GetApkVerifyStringByMD5());
        sb.append("&app=" + APP_TYPE);
        try {
            sb.append("&did=" + ConstInfo.getDeviceIdOnPhone(IntviuApplication.getInstance()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
