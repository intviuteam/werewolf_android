package com.networktoolkit.transport;

import android.text.TextUtils;

import com.androidtoolkit.ThreadToolKit;
import com.androidtoolkit.PLog;
import com.androidtoolkit.transport.HttpTransportUtils;
import com.toolkit.CancelableManager;
import com.toolkit.action.Cancelable;
import com.toolkit.action.Executable;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 * game_werewolf
 * 2017/7/3 下午3:54
 * Mystery
 * <p>
 * 请求任务
 */

public class RequestTask implements Cancelable, Executable, Runnable {

    public static class RequestTaskCallBackAdapter implements RequestTaskCallBack{

        @Override
        public void onSuccess(String response) {

        }

        @Override
        public void onFailed(int code, String failed) {

        }

        @Override
        public void onError(Exception e) {

        }
    }

    private static final String TAG = "RequestTask";

    private boolean mIsExecuting = false;
    private boolean mIsCancel = false;

    private boolean isUnique = false;

    private Object stateLock = new Object();

    private Call mRealTask;

    private String mRequestUrl;

    private RequestTaskCallBack mCallBack;
    private RequestTaskInterceptor mInterceptor;

    private RequestTask(RequestTaskBuilder builder, Call realCall) {
        mRequestUrl = builder.requestUrl;
        mRealTask = realCall;
        mCallBack = builder.callBack;
        isUnique = builder.unique;
        mInterceptor = builder.interceptor;
    }

    public void executeInCurThread() {
        if (isExecuting()) {
            return;
        }
        ThreadToolKit.ExecuteInCurThread(this);
    }

    public void execute() {
        if (isExecuting()) {
            return;
        }
        ThreadToolKit.Async(this);
    }

    @Override
    public void run() {
        setExecuting(true);

        if (isUnique) {
            reportRecord();
        }

        Response response = null;

        try {

            PLog.i(TAG, "Begin RequestTask Url:" + mRealTask.request().url().toString());

            response = HttpTransportUtils.executeCall(mRealTask);

            if (isCancel()) {
                PLog.i(TAG, "RequestTask has been Canceled");
                return;
            }

            if (response.isSuccessful()) {
                String responseString = response.body().string();
                PLog.i(TAG, "RequestTask receive Response:" + responseString);

                if (mInterceptor != null && mInterceptor.onInterceptor(responseString)) {
                    /**
                     * 执行阻断器，如果阻断器进行阻断，则不继续往下进行，而是直接认为结束
                     */
                } else {
                    /**
                     * 数据开始进行解析
                     */

                    JSONObject responseJson = new JSONObject(responseString);

                    int code = responseJson.optInt(Constant.CODE);
                    String message = responseJson.optString(Constant.MESSAGE);
                    JSONObject data = responseJson.optJSONObject(Constant.DATA);

                    if (code == 1000) {
                        if (data != null) {
                            if (mCallBack != null) {
                                mCallBack.onSuccess(data.toString());
                            }
                        } else {
                            if (mCallBack != null) {
                                mCallBack.onSuccess(message);
                            }
                        }
                    } else {
                        if (mCallBack != null) {
                            PLog.i(TAG, "onFailed " + message);
                            mCallBack.onFailed(code, message);
                        }
                    }

                }
            } else {
                if (mCallBack != null) {
                    PLog.i(TAG, "onFailed Request Failed");
                    mCallBack.onFailed(RequestTaskCallBack.NONE_CODE, "请求失败");
                }
            }
            /**
             * 至此请求的处理已经结束
             */
            response.close();

            /**
             * 完成后从Manager中移除
             */
            if (isUnique) {
                removeRecord();
            }

            /**
             * 完成了一个RequestTask 任务,清空所有的接口
             */
            mCallBack = null;
            mInterceptor = null;

            setExecuting(false);
            PLog.i(TAG, "RequestTask Execute Successfully");
        } catch (Throwable e) {
            e.printStackTrace();

            PLog.i(TAG, "RequestTask Error");

            if (isUnique) {
                removeRecord();
            }

            try {

                setExecuting(false);

                if (mCallBack != null) {
                    mCallBack.onError(new Exception("请求失败"));
                }

                if (response != null) {
                    response.close();
                }

                cancel();

            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }
    }

    public void cancel() {

        if (isCancel()) {
            return;
        }

        if (!isExecuting()) {
            return;
        }

        setCancel(true);

        synchronized (stateLock) {
            try {
                if (mRealTask != null && mRealTask.isExecuted() && !mRealTask.isCanceled()) {
                    mRealTask.cancel();
                    mRealTask = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        setExecuting(false);

        if (mCallBack != null) {
            mCallBack = null;
        }

        PLog.i(TAG, "Cancel a RequestTask successfully");
    }

    public boolean isExecuting() {
        boolean result = false;
        synchronized (stateLock) {
            result = mIsExecuting;
        }
        return result;
    }

    public boolean isCancel() {
        boolean result = false;
        synchronized (stateLock) {
            result = mIsCancel;
        }
        return result;
    }

    private void setCancel(boolean flag) {
        synchronized (stateLock) {
            mIsCancel = flag;
            PLog.i(TAG, "setCancel:" + flag);
        }
    }

    private void setExecuting(boolean flag) {
        synchronized (stateLock) {
            mIsExecuting = flag;
            PLog.i(TAG, "setExecuting:" + flag);
        }
    }

    private void reportRecord() {
        CancelableManager.getInstance().recordUniqueCancelable(mRequestUrl, this);
    }

    private void removeRecord() {
        CancelableManager.getInstance().removeUniqueCancelable(mRequestUrl);
    }

    @Deprecated
    public static RequestTask create(boolean isGet, String url, String token, JSONObject json, RequestTaskCallBack callback) {
        RequestTaskBuilder builder = create().isGet(isGet).request(url).intent(Constant.URL_TYPE_DEFAULT).withToken(token).load(json).action(callback);
        return builder.build();
    }

    public static RequestTaskBuilder create() {
        return new RequestTaskBuilder();
    }

    /**
     * 网络请求任务的Builder
     */
    public static class RequestTaskBuilder {

        public static final String GET = "get";
        public static final String POST = "post";

        public static final String URL_CUSTOM = "url_custom";  //完全自定义url
        public static final String URL_PARENT = "url_parent";  //使用parent路径，parent路径即是通过intent判断的路径
        /**
         * 默认的类别
         */
        private String mode = POST;
        private boolean isGet = false;

        private String urlMode = URL_PARENT;

        /**
         * url意图类型
         */
        private String intent = Constant.URL_TYPE_DEFAULT;
        /**
         * url子路径
         */
        private String requestUrl = "";
        /**
         * 自定义的url 当且仅当 为 url_custom 模式才可以使用
         */
        private String customUrl = "";
        private String token = "";

        /**
         * post的数据
         */
        private JSONObject postPayload = null;
        /**
         * get的数据
         */
        private Map<String, String> getPayload = null;

        /**
         * 任务回调
         */
        private RequestTaskCallBack callBack = null;

        /**
         * 头装饰器
         */
        private RequestTaskHeadDecorator mHeadDecorator = null;

        /**
         * 任务唯一性
         */
        private boolean unique = false;
        private RequestTaskInterceptor interceptor;

        public RequestTaskBuilder interceptor(RequestTaskInterceptor interceptor) {
            this.interceptor = interceptor;
            return this;
        }


        public RequestTaskBuilder mode(String mode) {
            if (TextUtils.equals(mode, GET)) {
                isGet = true;
                return this;
            }
            if (TextUtils.equals(mode, POST)) {
                isGet = false;
                return this;
            }
            throw new IllegalArgumentException("not support mode = " + mode);
        }

        public RequestTaskBuilder urlMode(String mode) {
            urlMode = mode;
            return this;
        }

        public RequestTaskBuilder withGetPayload(Map<String, String> payload) {
            if (isGet) {
                /**
                 * 当且仅当mode为Get模式，才会生效
                 */
                getPayload = payload;
            }
            return this;
        }

        public boolean urlModeIsCustom() {
            return TextUtils.equals(urlMode, URL_CUSTOM);
        }

        public boolean urlModeIsParent() {
            return TextUtils.equals(urlMode, URL_PARENT);
        }

        public RequestTaskBuilder customUrl(String url) {
            if (urlModeIsCustom()) {
                this.customUrl = url;
                return this;
            }
            throw new IllegalStateException("you should set urlmode before you invoke this method");
        }

        /**
         * 此方法为失败的一次开放，绝不推荐使用
         */
        @Deprecated
        public RequestTaskBuilder unique(boolean unique) {
            this.unique = unique;
            return this;
        }

        public RequestTaskBuilder isGet(boolean isGet) {
            this.isGet = isGet;
            return this;
        }

        @Deprecated
        public RequestTaskBuilder intent(String intent) {
            this.intent = intent;
            return this;
        }

        public RequestTaskBuilder setHeadDecorator(RequestTaskHeadDecorator headDecorator) {
            mHeadDecorator = headDecorator;
            return this;
        }

        public RequestTaskBuilder request(String url) {
            this.requestUrl = url;
            return this;
        }

        public RequestTaskBuilder load(JSONObject jsonObject) {
            this.postPayload = jsonObject;
            return this;
        }

        public RequestTaskBuilder withToken(String token) {
            this.token = token;
            return this;
        }

        public RequestTaskBuilder action(RequestTaskCallBack callBack) {
            this.callBack = callBack;
            return this;
        }

        public boolean isGet() {
            return isGet;
        }

        public String getIntent() {
            return intent;
        }

        public String getRequestUrl() {
            return requestUrl;
        }

        public String getToken() {
            return token;
        }

        public Map<String, String> getGetPayload() {
            return getPayload;
        }

        public JSONObject getPostPayload() {
            return postPayload;
        }

        public String getCustomUrl() {
            return customUrl;
        }

        public RequestTaskHeadDecorator getHeadDecorator() {
            return mHeadDecorator;
        }

        public RequestTask build() {
            Request request = RequestTaskUtils.createRequest(this);
            Call call = RequestTaskUtils.createCallByRequest(request);
            return new RequestTask(this, call);
        }

    }
}
