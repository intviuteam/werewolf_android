package com.networktoolkit.transport;


import android.text.TextUtils;

import com.androidtoolkit.PLog;
import com.androidtoolkit.transport.HttpTransportUtils;
import com.toolkit.ToolKit;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Author:1727
 * 2017/3/31
 */

public class RequestTaskUtils {

    public static String APP_TYPE = "";

    public static void initAppType(String type) {
        APP_TYPE = type;
        TransportConfig.initAppType(type);
    }

    private static final String TAG = "RequestTask";

    public static Request createRequest(RequestTask.RequestTaskBuilder requestTaskBuilder) {
        if (requestTaskBuilder == null) return null;

        Request request = null;
        Request.Builder requestBuilder = null;
        JSONObject postJson = null;

        RequestTaskHeadDecorator headDecorator = requestTaskBuilder.getHeadDecorator();

        String requestUrl = "";

        /**
         * 处理url
         */
        if (requestTaskBuilder.urlModeIsCustom()) {
            requestUrl = requestTaskBuilder.getCustomUrl();
        }

        if (requestTaskBuilder.urlModeIsParent()) {
            requestUrl = URLManager.GetRequestURL(requestTaskBuilder.getIntent()) + requestTaskBuilder.getRequestUrl();
        }

        if (TextUtils.isEmpty(requestUrl)) {
            throw new IllegalArgumentException("url is empty");
        }

        /**
         * 处理参数
         */
        if (requestTaskBuilder.isGet()) {
            Map<String, String> map = requestTaskBuilder.getGetPayload();
            if (!ToolKit.isEmpty(map)) {
                /**
                 * 拼接get参数
                 */
                StringBuilder stringBuilder = new StringBuilder(requestUrl);

                stringBuilder.append("?");

                ToolKit.travelMap(map, (index, key, value) -> {
                    if (index == 0) {
                        /**
                         * 第一个
                         */
                        stringBuilder.append(key);
                        stringBuilder.append("=");
                        stringBuilder.append(value);
                    } else {
                        /**
                         * 不是第一个
                         */
                        stringBuilder.append("&");
                        stringBuilder.append(key);
                        stringBuilder.append("=");
                        stringBuilder.append(value);
                    }
                });

                requestUrl = stringBuilder.toString();
            }
            requestBuilder = new Request.Builder().url(
                    requestUrl).addHeader("x-access-token", requestTaskBuilder.getToken()).addHeader("app", APP_TYPE);
        } else {
            requestBuilder = new Request.Builder().url(
                    requestUrl).addHeader("x-access-token", requestTaskBuilder.getToken()).addHeader("app", APP_TYPE);
            postJson = requestTaskBuilder.getPostPayload();
            if (postJson == null) {
                postJson = new JSONObject();
            }
            RequestBody body = RequestBody.create(HttpTransportUtils.JSON, postJson.toString());
            requestBuilder.post(body);
        }
        /**
         * 处理Header的处理
         */
        if (GlobalDecorateManager.INSTANCE.getHeaderDecorate() != null) {
            requestBuilder = GlobalDecorateManager.INSTANCE.getHeaderDecorate().decorateHttpRequestHeader(requestBuilder);
        }
        /**
         * 自定义Header的处理
         */
        if (headDecorator != null) {
            requestBuilder = headDecorator.decorateHead(requestBuilder);
        }

        request = requestBuilder.build();

        try {
            PLog.i(TAG, "request Url " + request.url().toString());
            PLog.i(TAG, "request Header " + request.headers().toString());
            PLog.i(TAG, "request body " + postJson);
        } catch (Exception e) {
        }
        return request;
    }

    public static Call createCallByRequest(Request request) {
        if (request != null) {
            return HttpTransportUtils.getCall(request);
        } else {
            return null;
        }
    }
}
