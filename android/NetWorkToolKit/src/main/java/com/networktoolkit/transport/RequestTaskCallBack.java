package com.networktoolkit.transport;

/**
 * Author:1727
 * 2017/3/31
 */

public interface RequestTaskCallBack {

    int NONE_CODE = 0;

    void onSuccess(String response);

    void onFailed(int code, String failed);

    void onError(Exception e);
}


