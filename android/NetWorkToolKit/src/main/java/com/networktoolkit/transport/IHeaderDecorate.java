package com.networktoolkit.transport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;

/**
 * game_werewolf
 * 2018/3/1 上午11:53
 * Mystery
 */

public interface IHeaderDecorate {

    Request.Builder decorateHttpRequestHeader(Request.Builder builder);

    Map<String, List<String>> decorateSocketHeader(Map<String, List<String>> headers);

    Map<String, String> decorateWebViewHeader(Map<String, String> extraHeaders);

    String decorateUrl(String url);

}
