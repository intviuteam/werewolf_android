package com.networktoolkit.transport;

/**
 * game_werewolf
 * 2018/3/1 上午11:52
 * Mystery
 */

public enum GlobalDecorateManager {

    INSTANCE;

    private IHeaderDecorate mIHeaderDecorate;

    private GlobalDecorateManager() {

    }

    public IHeaderDecorate getHeaderDecorate() {
        return mIHeaderDecorate;
    }

    public void setHeaderDecorate(IHeaderDecorate IHeaderDecorate) {
        mIHeaderDecorate = IHeaderDecorate;
    }
}
